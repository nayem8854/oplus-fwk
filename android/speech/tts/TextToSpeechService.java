package android.speech.tts;

import android.app.Service;
import android.content.Intent;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Message;
import android.os.MessageQueue;
import android.os.ParcelFileDescriptor;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.Set;

public abstract class TextToSpeechService extends Service {
  private final Object mVoicesInfoLock = new Object();
  
  private SynthHandler mSynthHandler;
  
  private String mPackageName;
  
  private TtsEngines mEngineHelper;
  
  private CallbackMap mCallbacks;
  
  public void onCreate() {
    super.onCreate();
    SynthThread synthThread = new SynthThread();
    synthThread.start();
    this.mSynthHandler = new SynthHandler(synthThread.getLooper());
    AudioPlaybackHandler audioPlaybackHandler = new AudioPlaybackHandler();
    audioPlaybackHandler.start();
    this.mEngineHelper = new TtsEngines(this);
    this.mCallbacks = new CallbackMap();
    this.mPackageName = (getApplicationInfo()).packageName;
    String[] arrayOfString = getSettingsLocale();
    onLoadLanguage(arrayOfString[0], arrayOfString[1], arrayOfString[2]);
  }
  
  public void onDestroy() {
    this.mSynthHandler.quit();
    this.mAudioPlaybackHandler.quit();
    this.mCallbacks.kill();
    super.onDestroy();
  }
  
  protected Set<String> onGetFeaturesForLanguage(String paramString1, String paramString2, String paramString3) {
    return new HashSet<>();
  }
  
  private int getExpectedLanguageAvailableStatus(Locale paramLocale) {
    byte b = 2;
    if (paramLocale.getVariant().isEmpty())
      if (paramLocale.getCountry().isEmpty()) {
        b = 0;
      } else {
        b = 1;
      }  
    return b;
  }
  
  public List<Voice> onGetVoices() {
    ArrayList<Voice> arrayList = new ArrayList();
    for (Locale locale : Locale.getAvailableLocales()) {
      int i = getExpectedLanguageAvailableStatus(locale);
      try {
        String str1 = locale.getISO3Language();
        String str2 = locale.getISO3Country(), str3 = locale.getVariant();
        int j = onIsLanguageAvailable(str1, str2, str3);
        if (j == i) {
          str1 = locale.getISO3Language();
          str3 = locale.getISO3Country();
          str2 = locale.getVariant();
          Set<String> set = onGetFeaturesForLanguage(str1, str3, str2);
          String str = locale.getISO3Language();
          str2 = locale.getISO3Country();
          str3 = locale.getVariant();
          str2 = onGetDefaultVoiceNameFor(str, str2, str3);
          arrayList.add(new Voice(str2, locale, 300, 300, false, set));
        } 
      } catch (MissingResourceException missingResourceException) {}
    } 
    return arrayList;
  }
  
  public String onGetDefaultVoiceNameFor(String paramString1, String paramString2, String paramString3) {
    int i = onIsLanguageAvailable(paramString1, paramString2, paramString3);
    if (i != 0) {
      if (i != 1) {
        if (i != 2)
          return null; 
        locale = new Locale(paramString1, paramString2, paramString3);
      } else {
        locale = new Locale((String)locale, paramString2);
      } 
    } else {
      locale = new Locale((String)locale);
    } 
    Locale locale = TtsEngines.normalizeTTSLocale(locale);
    String str = locale.toLanguageTag();
    if (onIsValidVoiceName(str) == 0)
      return str; 
    return null;
  }
  
  public int onLoadVoice(String paramString) {
    Locale locale = Locale.forLanguageTag(paramString);
    if (locale == null)
      return -1; 
    int i = getExpectedLanguageAvailableStatus(locale);
    try {
      String str2 = locale.getISO3Language();
      String str3 = locale.getISO3Country(), str4 = locale.getVariant();
      int j = onIsLanguageAvailable(str2, str3, str4);
      if (j != i)
        return -1; 
      str3 = locale.getISO3Language();
      str2 = locale.getISO3Country();
      String str1 = locale.getVariant();
      onLoadLanguage(str3, str2, str1);
      return 0;
    } catch (MissingResourceException missingResourceException) {
      return -1;
    } 
  }
  
  public int onIsValidVoiceName(String paramString) {
    Locale locale = Locale.forLanguageTag(paramString);
    if (locale == null)
      return -1; 
    int i = getExpectedLanguageAvailableStatus(locale);
    try {
      String str2 = locale.getISO3Language();
      paramString = locale.getISO3Country();
      String str1 = locale.getVariant();
      int j = onIsLanguageAvailable(str2, paramString, str1);
      if (j != i)
        return -1; 
      return 0;
    } catch (MissingResourceException missingResourceException) {
      return -1;
    } 
  }
  
  private int getDefaultSpeechRate() {
    return getSecureSettingInt("tts_default_rate", 100);
  }
  
  private int getDefaultPitch() {
    return getSecureSettingInt("tts_default_pitch", 100);
  }
  
  private String[] getSettingsLocale() {
    Locale locale = this.mEngineHelper.getLocalePrefForEngine(this.mPackageName);
    return TtsEngines.toOldLocaleStringFormat(locale);
  }
  
  private int getSecureSettingInt(String paramString, int paramInt) {
    return Settings.Secure.getInt(getContentResolver(), paramString, paramInt);
  }
  
  class SynthThread extends HandlerThread implements MessageQueue.IdleHandler {
    private boolean mFirstIdle = true;
    
    final TextToSpeechService this$0;
    
    public SynthThread() {
      super("SynthThread", 0);
    }
    
    protected void onLooperPrepared() {
      getLooper().getQueue().addIdleHandler(this);
    }
    
    public boolean queueIdle() {
      if (this.mFirstIdle) {
        this.mFirstIdle = false;
      } else {
        broadcastTtsQueueProcessingCompleted();
      } 
      return true;
    }
    
    private void broadcastTtsQueueProcessingCompleted() {
      Intent intent = new Intent("android.speech.tts.TTS_QUEUE_PROCESSING_COMPLETED");
      TextToSpeechService.this.sendBroadcast(intent);
    }
  }
  
  class SynthHandler extends Handler {
    private TextToSpeechService.SpeechItem mCurrentSpeechItem = null;
    
    private List<Object> mFlushedObjects = new ArrayList();
    
    private int mFlushAll = 0;
    
    final TextToSpeechService this$0;
    
    public SynthHandler(Looper param1Looper) {
      super(param1Looper);
    }
    
    private void startFlushingSpeechItems(Object param1Object) {
      // Byte code:
      //   0: aload_0
      //   1: getfield mFlushedObjects : Ljava/util/List;
      //   4: astore_2
      //   5: aload_2
      //   6: monitorenter
      //   7: aload_1
      //   8: ifnonnull -> 24
      //   11: aload_0
      //   12: aload_0
      //   13: getfield mFlushAll : I
      //   16: iconst_1
      //   17: iadd
      //   18: putfield mFlushAll : I
      //   21: goto -> 35
      //   24: aload_0
      //   25: getfield mFlushedObjects : Ljava/util/List;
      //   28: aload_1
      //   29: invokeinterface add : (Ljava/lang/Object;)Z
      //   34: pop
      //   35: aload_2
      //   36: monitorexit
      //   37: return
      //   38: astore_1
      //   39: aload_2
      //   40: monitorexit
      //   41: aload_1
      //   42: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #478	-> 0
      //   #479	-> 7
      //   #480	-> 11
      //   #482	-> 24
      //   #484	-> 35
      //   #485	-> 37
      //   #484	-> 38
      // Exception table:
      //   from	to	target	type
      //   11	21	38	finally
      //   24	35	38	finally
      //   35	37	38	finally
      //   39	41	38	finally
    }
    
    private void endFlushingSpeechItems(Object param1Object) {
      // Byte code:
      //   0: aload_0
      //   1: getfield mFlushedObjects : Ljava/util/List;
      //   4: astore_2
      //   5: aload_2
      //   6: monitorenter
      //   7: aload_1
      //   8: ifnonnull -> 24
      //   11: aload_0
      //   12: aload_0
      //   13: getfield mFlushAll : I
      //   16: iconst_1
      //   17: isub
      //   18: putfield mFlushAll : I
      //   21: goto -> 35
      //   24: aload_0
      //   25: getfield mFlushedObjects : Ljava/util/List;
      //   28: aload_1
      //   29: invokeinterface remove : (Ljava/lang/Object;)Z
      //   34: pop
      //   35: aload_2
      //   36: monitorexit
      //   37: return
      //   38: astore_1
      //   39: aload_2
      //   40: monitorexit
      //   41: aload_1
      //   42: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #487	-> 0
      //   #488	-> 7
      //   #489	-> 11
      //   #491	-> 24
      //   #493	-> 35
      //   #494	-> 37
      //   #493	-> 38
      // Exception table:
      //   from	to	target	type
      //   11	21	38	finally
      //   24	35	38	finally
      //   35	37	38	finally
      //   39	41	38	finally
    }
    
    private boolean isFlushed(TextToSpeechService.SpeechItem param1SpeechItem) {
      synchronized (this.mFlushedObjects) {
        if (this.mFlushAll > 0 || this.mFlushedObjects.contains(param1SpeechItem.getCallerIdentity()))
          return true; 
        return false;
      } 
    }
    
    private TextToSpeechService.SpeechItem getCurrentSpeechItem() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mCurrentSpeechItem : Landroid/speech/tts/TextToSpeechService$SpeechItem;
      //   6: astore_1
      //   7: aload_0
      //   8: monitorexit
      //   9: aload_1
      //   10: areturn
      //   11: astore_1
      //   12: aload_0
      //   13: monitorexit
      //   14: aload_1
      //   15: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #502	-> 2
      //   #502	-> 11
      // Exception table:
      //   from	to	target	type
      //   2	7	11	finally
    }
    
    private boolean setCurrentSpeechItem(TextToSpeechService.SpeechItem param1SpeechItem) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_1
      //   3: ifnull -> 20
      //   6: aload_0
      //   7: aload_1
      //   8: invokespecial isFlushed : (Landroid/speech/tts/TextToSpeechService$SpeechItem;)Z
      //   11: istore_2
      //   12: iload_2
      //   13: ifeq -> 20
      //   16: aload_0
      //   17: monitorexit
      //   18: iconst_0
      //   19: ireturn
      //   20: aload_0
      //   21: aload_1
      //   22: putfield mCurrentSpeechItem : Landroid/speech/tts/TextToSpeechService$SpeechItem;
      //   25: aload_0
      //   26: monitorexit
      //   27: iconst_1
      //   28: ireturn
      //   29: astore_1
      //   30: aload_0
      //   31: monitorexit
      //   32: aload_1
      //   33: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #516	-> 2
      //   #517	-> 16
      //   #519	-> 20
      //   #520	-> 25
      //   #515	-> 29
      // Exception table:
      //   from	to	target	type
      //   6	12	29	finally
      //   20	25	29	finally
    }
    
    private TextToSpeechService.SpeechItem removeCurrentSpeechItem() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mCurrentSpeechItem : Landroid/speech/tts/TextToSpeechService$SpeechItem;
      //   6: astore_1
      //   7: aload_0
      //   8: aconst_null
      //   9: putfield mCurrentSpeechItem : Landroid/speech/tts/TextToSpeechService$SpeechItem;
      //   12: aload_0
      //   13: monitorexit
      //   14: aload_1
      //   15: areturn
      //   16: astore_1
      //   17: aload_0
      //   18: monitorexit
      //   19: aload_1
      //   20: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #524	-> 2
      //   #525	-> 7
      //   #526	-> 12
      //   #523	-> 16
      // Exception table:
      //   from	to	target	type
      //   2	7	16	finally
      //   7	12	16	finally
    }
    
    private TextToSpeechService.SpeechItem maybeRemoveCurrentSpeechItem(Object param1Object) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mCurrentSpeechItem : Landroid/speech/tts/TextToSpeechService$SpeechItem;
      //   6: ifnull -> 36
      //   9: aload_0
      //   10: getfield mCurrentSpeechItem : Landroid/speech/tts/TextToSpeechService$SpeechItem;
      //   13: astore_2
      //   14: aload_2
      //   15: invokevirtual getCallerIdentity : ()Ljava/lang/Object;
      //   18: aload_1
      //   19: if_acmpne -> 36
      //   22: aload_0
      //   23: getfield mCurrentSpeechItem : Landroid/speech/tts/TextToSpeechService$SpeechItem;
      //   26: astore_1
      //   27: aload_0
      //   28: aconst_null
      //   29: putfield mCurrentSpeechItem : Landroid/speech/tts/TextToSpeechService$SpeechItem;
      //   32: aload_0
      //   33: monitorexit
      //   34: aload_1
      //   35: areturn
      //   36: aload_0
      //   37: monitorexit
      //   38: aconst_null
      //   39: areturn
      //   40: astore_1
      //   41: aload_0
      //   42: monitorexit
      //   43: aload_1
      //   44: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #530	-> 2
      //   #531	-> 14
      //   #532	-> 22
      //   #533	-> 27
      //   #534	-> 32
      //   #537	-> 36
      //   #529	-> 40
      // Exception table:
      //   from	to	target	type
      //   2	14	40	finally
      //   14	22	40	finally
      //   22	27	40	finally
      //   27	32	40	finally
    }
    
    public boolean isSpeaking() {
      boolean bool;
      if (getCurrentSpeechItem() != null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public void quit() {
      getLooper().quit();
      TextToSpeechService.SpeechItem speechItem = removeCurrentSpeechItem();
      if (speechItem != null)
        speechItem.stop(); 
    }
    
    public int enqueueSpeechItem(int param1Int, TextToSpeechService.SpeechItem param1SpeechItem) {
      TextToSpeechService.UtteranceProgressDispatcher utteranceProgressDispatcher = null;
      if (param1SpeechItem instanceof TextToSpeechService.UtteranceProgressDispatcher)
        utteranceProgressDispatcher = (TextToSpeechService.UtteranceProgressDispatcher)param1SpeechItem; 
      if (!param1SpeechItem.isValid()) {
        if (utteranceProgressDispatcher != null)
          utteranceProgressDispatcher.dispatchOnError(-8); 
        return -1;
      } 
      if (param1Int == 0) {
        stopForApp(param1SpeechItem.getCallerIdentity());
      } else if (param1Int == 2) {
        stopAll();
      } 
      Object object = new Object(this, param1SpeechItem);
      object = Message.obtain(this, (Runnable)object);
      ((Message)object).obj = param1SpeechItem.getCallerIdentity();
      if (sendMessage((Message)object))
        return 0; 
      Log.w("TextToSpeechService", "SynthThread has quit");
      if (utteranceProgressDispatcher != null)
        utteranceProgressDispatcher.dispatchOnError(-4); 
      return -1;
    }
    
    public int stopForApp(Object param1Object) {
      if (param1Object == null)
        return -1; 
      startFlushingSpeechItems(param1Object);
      TextToSpeechService.SpeechItem speechItem = maybeRemoveCurrentSpeechItem(param1Object);
      if (speechItem != null)
        speechItem.stop(); 
      TextToSpeechService.this.mAudioPlaybackHandler.stopForApp(param1Object);
      param1Object = new Object(this, param1Object);
      sendMessage(Message.obtain(this, (Runnable)param1Object));
      return 0;
    }
    
    public int stopAll() {
      startFlushingSpeechItems((Object)null);
      TextToSpeechService.SpeechItem speechItem = removeCurrentSpeechItem();
      if (speechItem != null)
        speechItem.stop(); 
      TextToSpeechService.this.mAudioPlaybackHandler.stop();
      Object object = new Object(this);
      sendMessage(Message.obtain(this, (Runnable)object));
      return 0;
    }
  }
  
  class AudioOutputParams {
    public final AudioAttributes mAudioAttributes;
    
    public final float mPan;
    
    public final int mSessionId;
    
    public final float mVolume;
    
    AudioOutputParams() {
      this.mSessionId = 0;
      this.mVolume = 1.0F;
      this.mPan = 0.0F;
      this.mAudioAttributes = null;
    }
    
    AudioOutputParams(TextToSpeechService this$0, float param1Float1, float param1Float2, AudioAttributes param1AudioAttributes) {
      this.mSessionId = this$0;
      this.mVolume = param1Float1;
      this.mPan = param1Float2;
      this.mAudioAttributes = param1AudioAttributes;
    }
    
    static AudioOutputParams createFromParamsBundle(Bundle param1Bundle, boolean param1Boolean) {
      if (param1Bundle == null)
        return new AudioOutputParams(); 
      AudioAttributes audioAttributes1 = (AudioAttributes)param1Bundle.getParcelable("audioAttributes");
      AudioAttributes audioAttributes2 = audioAttributes1;
      if (audioAttributes1 == null) {
        int j = param1Bundle.getInt("streamType", 3);
        AudioAttributes.Builder builder = new AudioAttributes.Builder();
        builder = builder.setLegacyStreamType(j);
        if (param1Boolean) {
          j = 1;
        } else {
          j = 4;
        } 
        builder = builder.setContentType(j);
        audioAttributes2 = builder.build();
      } 
      int i = param1Bundle.getInt("sessionId", 0);
      float f = param1Bundle.getFloat("volume", 1.0F);
      return new AudioOutputParams(i, f, param1Bundle.getFloat("pan", 0.0F), audioAttributes2);
    }
  }
  
  class SpeechItem {
    private final Object mCallerIdentity;
    
    private final int mCallerPid;
    
    private final int mCallerUid;
    
    private boolean mStarted = false;
    
    private boolean mStopped = false;
    
    final TextToSpeechService this$0;
    
    public SpeechItem(Object param1Object, int param1Int1, int param1Int2) {
      this.mCallerIdentity = param1Object;
      this.mCallerUid = param1Int1;
      this.mCallerPid = param1Int2;
    }
    
    public Object getCallerIdentity() {
      return this.mCallerIdentity;
    }
    
    public int getCallerUid() {
      return this.mCallerUid;
    }
    
    public int getCallerPid() {
      return this.mCallerPid;
    }
    
    public void play() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mStarted : Z
      //   6: ifne -> 21
      //   9: aload_0
      //   10: iconst_1
      //   11: putfield mStarted : Z
      //   14: aload_0
      //   15: monitorexit
      //   16: aload_0
      //   17: invokevirtual playImpl : ()V
      //   20: return
      //   21: new java/lang/IllegalStateException
      //   24: astore_1
      //   25: aload_1
      //   26: ldc 'play() called twice'
      //   28: invokespecial <init> : (Ljava/lang/String;)V
      //   31: aload_1
      //   32: athrow
      //   33: astore_1
      //   34: aload_0
      //   35: monitorexit
      //   36: aload_1
      //   37: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #813	-> 0
      //   #814	-> 2
      //   #817	-> 9
      //   #818	-> 14
      //   #819	-> 16
      //   #820	-> 20
      //   #815	-> 21
      //   #818	-> 33
      // Exception table:
      //   from	to	target	type
      //   2	9	33	finally
      //   9	14	33	finally
      //   14	16	33	finally
      //   21	33	33	finally
      //   34	36	33	finally
    }
    
    public void stop() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mStopped : Z
      //   6: ifne -> 21
      //   9: aload_0
      //   10: iconst_1
      //   11: putfield mStopped : Z
      //   14: aload_0
      //   15: monitorexit
      //   16: aload_0
      //   17: invokevirtual stopImpl : ()V
      //   20: return
      //   21: new java/lang/IllegalStateException
      //   24: astore_1
      //   25: aload_1
      //   26: ldc 'stop() called twice'
      //   28: invokespecial <init> : (Ljava/lang/String;)V
      //   31: aload_1
      //   32: athrow
      //   33: astore_1
      //   34: aload_0
      //   35: monitorexit
      //   36: aload_1
      //   37: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #831	-> 0
      //   #832	-> 2
      //   #835	-> 9
      //   #836	-> 14
      //   #837	-> 16
      //   #838	-> 20
      //   #833	-> 21
      //   #836	-> 33
      // Exception table:
      //   from	to	target	type
      //   2	9	33	finally
      //   9	14	33	finally
      //   14	16	33	finally
      //   21	33	33	finally
      //   34	36	33	finally
    }
    
    protected boolean isStopped() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mStopped : Z
      //   6: istore_1
      //   7: aload_0
      //   8: monitorexit
      //   9: iload_1
      //   10: ireturn
      //   11: astore_2
      //   12: aload_0
      //   13: monitorexit
      //   14: aload_2
      //   15: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #843	-> 2
      //   #843	-> 11
      // Exception table:
      //   from	to	target	type
      //   2	7	11	finally
    }
    
    protected boolean isStarted() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mStarted : Z
      //   6: istore_1
      //   7: aload_0
      //   8: monitorexit
      //   9: iload_1
      //   10: ireturn
      //   11: astore_2
      //   12: aload_0
      //   13: monitorexit
      //   14: aload_2
      //   15: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #847	-> 2
      //   #847	-> 11
      // Exception table:
      //   from	to	target	type
      //   2	7	11	finally
    }
    
    public abstract boolean isValid();
    
    protected abstract void playImpl();
    
    protected abstract void stopImpl();
  }
  
  class UtteranceSpeechItem extends SpeechItem implements UtteranceProgressDispatcher {
    final TextToSpeechService this$0;
    
    public UtteranceSpeechItem(Object param1Object, int param1Int1, int param1Int2) {
      super(param1Object, param1Int1, param1Int2);
    }
    
    public void dispatchOnSuccess() {
      String str = getUtteranceId();
      if (str != null)
        TextToSpeechService.this.mCallbacks.dispatchOnSuccess(getCallerIdentity(), str); 
    }
    
    public void dispatchOnStop() {
      String str = getUtteranceId();
      if (str != null)
        TextToSpeechService.this.mCallbacks.dispatchOnStop(getCallerIdentity(), str, isStarted()); 
    }
    
    public void dispatchOnStart() {
      String str = getUtteranceId();
      if (str != null)
        TextToSpeechService.this.mCallbacks.dispatchOnStart(getCallerIdentity(), str); 
    }
    
    public void dispatchOnError(int param1Int) {
      String str = getUtteranceId();
      if (str != null)
        TextToSpeechService.this.mCallbacks.dispatchOnError(getCallerIdentity(), str, param1Int); 
    }
    
    public void dispatchOnBeginSynthesis(int param1Int1, int param1Int2, int param1Int3) {
      String str = getUtteranceId();
      if (str != null)
        TextToSpeechService.this.mCallbacks.dispatchOnBeginSynthesis(getCallerIdentity(), str, param1Int1, param1Int2, param1Int3); 
    }
    
    public void dispatchOnAudioAvailable(byte[] param1ArrayOfbyte) {
      String str = getUtteranceId();
      if (str != null)
        TextToSpeechService.this.mCallbacks.dispatchOnAudioAvailable(getCallerIdentity(), str, param1ArrayOfbyte); 
    }
    
    public void dispatchOnRangeStart(int param1Int1, int param1Int2, int param1Int3) {
      String str = getUtteranceId();
      if (str != null) {
        TextToSpeechService.CallbackMap callbackMap = TextToSpeechService.this.mCallbacks;
        Object object = getCallerIdentity();
        callbackMap.dispatchOnRangeStart(object, str, param1Int1, param1Int2, param1Int3);
      } 
    }
    
    String getStringParam(Bundle param1Bundle, String param1String1, String param1String2) {
      String str;
      if (param1Bundle == null) {
        str = param1String2;
      } else {
        str = str.getString(param1String1, param1String2);
      } 
      return str;
    }
    
    int getIntParam(Bundle param1Bundle, String param1String, int param1Int) {
      if (param1Bundle != null)
        param1Int = param1Bundle.getInt(param1String, param1Int); 
      return param1Int;
    }
    
    float getFloatParam(Bundle param1Bundle, String param1String, float param1Float) {
      if (param1Bundle != null)
        param1Float = param1Bundle.getFloat(param1String, param1Float); 
      return param1Float;
    }
    
    public abstract String getUtteranceId();
  }
  
  class UtteranceSpeechItemWithParams extends UtteranceSpeechItem {
    protected final Bundle mParams;
    
    protected final String mUtteranceId;
    
    final TextToSpeechService this$0;
    
    UtteranceSpeechItemWithParams(Object param1Object, int param1Int1, int param1Int2, Bundle param1Bundle, String param1String) {
      super(param1Object, param1Int1, param1Int2);
      this.mParams = param1Bundle;
      this.mUtteranceId = param1String;
    }
    
    boolean hasLanguage() {
      return TextUtils.isEmpty(getStringParam(this.mParams, "language", (String)null)) ^ true;
    }
    
    int getSpeechRate() {
      return getIntParam(this.mParams, "rate", TextToSpeechService.this.getDefaultSpeechRate());
    }
    
    int getPitch() {
      return getIntParam(this.mParams, "pitch", TextToSpeechService.this.getDefaultPitch());
    }
    
    public String getUtteranceId() {
      return this.mUtteranceId;
    }
    
    TextToSpeechService.AudioOutputParams getAudioParams() {
      return TextToSpeechService.AudioOutputParams.createFromParamsBundle(this.mParams, true);
    }
  }
  
  class SynthesisSpeechItem extends UtteranceSpeechItemWithParams {
    private final int mCallerUid;
    
    private final String[] mDefaultLocale;
    
    private final EventLogger mEventLogger;
    
    private AbstractSynthesisCallback mSynthesisCallback;
    
    private final SynthesisRequest mSynthesisRequest;
    
    private final CharSequence mText;
    
    final TextToSpeechService this$0;
    
    public SynthesisSpeechItem(Object param1Object, int param1Int1, int param1Int2, Bundle param1Bundle, String param1String, CharSequence param1CharSequence) {
      super(param1Object, param1Int1, param1Int2, param1Bundle, param1String);
      this.mText = param1CharSequence;
      this.mCallerUid = param1Int1;
      this.mSynthesisRequest = new SynthesisRequest(param1CharSequence, this.mParams);
      this.mDefaultLocale = TextToSpeechService.this.getSettingsLocale();
      setRequestParams(this.mSynthesisRequest);
      this.mEventLogger = new EventLogger(this.mSynthesisRequest, param1Int1, param1Int2, TextToSpeechService.this.mPackageName);
    }
    
    public CharSequence getText() {
      return this.mText;
    }
    
    public boolean isValid() {
      CharSequence charSequence = this.mText;
      if (charSequence == null) {
        Log.e("TextToSpeechService", "null synthesis text");
        return false;
      } 
      if (charSequence.length() > TextToSpeech.getMaxSpeechInputLength()) {
        charSequence = new StringBuilder();
        charSequence.append("Text too long: ");
        charSequence.append(this.mText.length());
        charSequence.append(" chars");
        Log.w("TextToSpeechService", charSequence.toString());
        return false;
      } 
      return true;
    }
    
    protected void playImpl() {
      // Byte code:
      //   0: aload_0
      //   1: getfield mEventLogger : Landroid/speech/tts/EventLogger;
      //   4: invokevirtual onRequestProcessingStart : ()V
      //   7: aload_0
      //   8: monitorenter
      //   9: aload_0
      //   10: invokevirtual isStopped : ()Z
      //   13: ifeq -> 19
      //   16: aload_0
      //   17: monitorexit
      //   18: return
      //   19: aload_0
      //   20: invokevirtual createSynthesisCallback : ()Landroid/speech/tts/AbstractSynthesisCallback;
      //   23: astore_1
      //   24: aload_0
      //   25: aload_1
      //   26: putfield mSynthesisCallback : Landroid/speech/tts/AbstractSynthesisCallback;
      //   29: aload_0
      //   30: monitorexit
      //   31: aload_0
      //   32: getfield this$0 : Landroid/speech/tts/TextToSpeechService;
      //   35: aload_0
      //   36: getfield mSynthesisRequest : Landroid/speech/tts/SynthesisRequest;
      //   39: aload_1
      //   40: invokevirtual onSynthesizeText : (Landroid/speech/tts/SynthesisRequest;Landroid/speech/tts/SynthesisCallback;)V
      //   43: aload_1
      //   44: invokevirtual hasStarted : ()Z
      //   47: ifeq -> 62
      //   50: aload_1
      //   51: invokevirtual hasFinished : ()Z
      //   54: ifne -> 62
      //   57: aload_1
      //   58: invokevirtual done : ()I
      //   61: pop
      //   62: return
      //   63: astore_1
      //   64: aload_0
      //   65: monitorexit
      //   66: aload_1
      //   67: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1022	-> 0
      //   #1023	-> 7
      //   #1026	-> 9
      //   #1027	-> 16
      //   #1029	-> 19
      //   #1030	-> 29
      //   #1031	-> 29
      //   #1033	-> 31
      //   #1036	-> 43
      //   #1037	-> 57
      //   #1039	-> 62
      //   #1031	-> 63
      // Exception table:
      //   from	to	target	type
      //   9	16	63	finally
      //   16	18	63	finally
      //   19	29	63	finally
      //   29	31	63	finally
      //   64	66	63	finally
    }
    
    protected AbstractSynthesisCallback createSynthesisCallback() {
      TextToSpeechService.AudioOutputParams audioOutputParams = getAudioParams();
      TextToSpeechService textToSpeechService = TextToSpeechService.this;
      return 
        new PlaybackSynthesisCallback(audioOutputParams, textToSpeechService.mAudioPlaybackHandler, this, getCallerIdentity(), this.mEventLogger, false);
    }
    
    private void setRequestParams(SynthesisRequest param1SynthesisRequest) {
      String str = getVoiceName();
      param1SynthesisRequest.setLanguage(getLanguage(), getCountry(), getVariant());
      if (!TextUtils.isEmpty(str))
        param1SynthesisRequest.setVoiceName(getVoiceName()); 
      param1SynthesisRequest.setSpeechRate(getSpeechRate());
      param1SynthesisRequest.setCallerUid(this.mCallerUid);
      param1SynthesisRequest.setPitch(getPitch());
    }
    
    protected void stopImpl() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mSynthesisCallback : Landroid/speech/tts/AbstractSynthesisCallback;
      //   6: astore_1
      //   7: aload_0
      //   8: monitorexit
      //   9: aload_1
      //   10: ifnull -> 27
      //   13: aload_1
      //   14: invokevirtual stop : ()V
      //   17: aload_0
      //   18: getfield this$0 : Landroid/speech/tts/TextToSpeechService;
      //   21: invokevirtual onStop : ()V
      //   24: goto -> 31
      //   27: aload_0
      //   28: invokevirtual dispatchOnStop : ()V
      //   31: return
      //   32: astore_1
      //   33: aload_0
      //   34: monitorexit
      //   35: aload_1
      //   36: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1060	-> 0
      //   #1061	-> 2
      //   #1062	-> 7
      //   #1063	-> 9
      //   #1067	-> 13
      //   #1068	-> 17
      //   #1070	-> 27
      //   #1072	-> 31
      //   #1062	-> 32
      // Exception table:
      //   from	to	target	type
      //   2	7	32	finally
      //   7	9	32	finally
      //   33	35	32	finally
    }
    
    private String getCountry() {
      if (!hasLanguage())
        return this.mDefaultLocale[1]; 
      return getStringParam(this.mParams, "country", "");
    }
    
    private String getVariant() {
      if (!hasLanguage())
        return this.mDefaultLocale[2]; 
      return getStringParam(this.mParams, "variant", "");
    }
    
    public String getLanguage() {
      return getStringParam(this.mParams, "language", this.mDefaultLocale[0]);
    }
    
    public String getVoiceName() {
      return getStringParam(this.mParams, "voiceName", "");
    }
  }
  
  private class SynthesisToFileOutputStreamSpeechItem extends SynthesisSpeechItem {
    private final FileOutputStream mFileOutputStream;
    
    final TextToSpeechService this$0;
    
    public SynthesisToFileOutputStreamSpeechItem(Object param1Object, int param1Int1, int param1Int2, Bundle param1Bundle, String param1String, CharSequence param1CharSequence, FileOutputStream param1FileOutputStream) {
      super(param1Object, param1Int1, param1Int2, param1Bundle, param1String, param1CharSequence);
      this.mFileOutputStream = param1FileOutputStream;
    }
    
    protected AbstractSynthesisCallback createSynthesisCallback() {
      return new FileSynthesisCallback(this.mFileOutputStream.getChannel(), this, false);
    }
    
    protected void playImpl() {
      super.playImpl();
      try {
        this.mFileOutputStream.close();
      } catch (IOException iOException) {
        Log.w("TextToSpeechService", "Failed to close output file", iOException);
      } 
    }
  }
  
  private class AudioSpeechItem extends UtteranceSpeechItemWithParams {
    private final AudioPlaybackQueueItem mItem;
    
    final TextToSpeechService this$0;
    
    public AudioSpeechItem(Object param1Object, int param1Int1, int param1Int2, Bundle param1Bundle, String param1String, Uri param1Uri) {
      super(param1Object, param1Int1, param1Int2, param1Bundle, param1String);
      param1Object = getCallerIdentity();
      this.mItem = new AudioPlaybackQueueItem(this, param1Object, TextToSpeechService.this, param1Uri, getAudioParams());
    }
    
    public boolean isValid() {
      return true;
    }
    
    protected void playImpl() {
      TextToSpeechService.this.mAudioPlaybackHandler.enqueue(this.mItem);
    }
    
    protected void stopImpl() {}
    
    public String getUtteranceId() {
      return getStringParam(this.mParams, "utteranceId", (String)null);
    }
    
    TextToSpeechService.AudioOutputParams getAudioParams() {
      return TextToSpeechService.AudioOutputParams.createFromParamsBundle(this.mParams, false);
    }
  }
  
  class SilenceSpeechItem extends UtteranceSpeechItem {
    private final long mDuration;
    
    private final String mUtteranceId;
    
    final TextToSpeechService this$0;
    
    public SilenceSpeechItem(Object param1Object, int param1Int1, int param1Int2, String param1String, long param1Long) {
      super(param1Object, param1Int1, param1Int2);
      this.mUtteranceId = param1String;
      this.mDuration = param1Long;
    }
    
    public boolean isValid() {
      return true;
    }
    
    protected void playImpl() {
      AudioPlaybackHandler audioPlaybackHandler = TextToSpeechService.this.mAudioPlaybackHandler;
      SilencePlaybackQueueItem silencePlaybackQueueItem = new SilencePlaybackQueueItem(this, getCallerIdentity(), this.mDuration);
      audioPlaybackHandler.enqueue(silencePlaybackQueueItem);
    }
    
    protected void stopImpl() {}
    
    public String getUtteranceId() {
      return this.mUtteranceId;
    }
  }
  
  class LoadLanguageItem extends SpeechItem {
    private final String mCountry;
    
    private final String mLanguage;
    
    private final String mVariant;
    
    final TextToSpeechService this$0;
    
    public LoadLanguageItem(Object param1Object, int param1Int1, int param1Int2, String param1String1, String param1String2, String param1String3) {
      super(param1Object, param1Int1, param1Int2);
      this.mLanguage = param1String1;
      this.mCountry = param1String2;
      this.mVariant = param1String3;
    }
    
    public boolean isValid() {
      return true;
    }
    
    protected void playImpl() {
      TextToSpeechService.this.onLoadLanguage(this.mLanguage, this.mCountry, this.mVariant);
    }
    
    protected void stopImpl() {}
  }
  
  class LoadVoiceItem extends SpeechItem {
    private final String mVoiceName;
    
    final TextToSpeechService this$0;
    
    public LoadVoiceItem(Object param1Object, int param1Int1, int param1Int2, String param1String) {
      super(param1Object, param1Int1, param1Int2);
      this.mVoiceName = param1String;
    }
    
    public boolean isValid() {
      return true;
    }
    
    protected void playImpl() {
      TextToSpeechService.this.onLoadVoice(this.mVoiceName);
    }
    
    protected void stopImpl() {}
  }
  
  public IBinder onBind(Intent paramIntent) {
    if ("android.intent.action.TTS_SERVICE".equals(paramIntent.getAction())) {
      Binder.allowBlocking(this.mBinder.asBinder());
      return (IBinder)this.mBinder;
    } 
    return null;
  }
  
  private final ITextToSpeechService.Stub mBinder = new ITextToSpeechService.Stub() {
      final TextToSpeechService this$0;
      
      public int speak(IBinder param1IBinder, CharSequence param1CharSequence, int param1Int, Bundle param1Bundle, String param1String) {
        if (!checkNonNull(new Object[] { param1IBinder, param1CharSequence, param1Bundle }))
          return -1; 
        TextToSpeechService textToSpeechService = TextToSpeechService.this;
        int i = Binder.getCallingUid();
        TextToSpeechService.SynthesisSpeechItem synthesisSpeechItem = new TextToSpeechService.SynthesisSpeechItem(param1IBinder, i, Binder.getCallingPid(), param1Bundle, param1String, param1CharSequence);
        return TextToSpeechService.this.mSynthHandler.enqueueSpeechItem(param1Int, synthesisSpeechItem);
      }
      
      public int synthesizeToFileDescriptor(IBinder param1IBinder, CharSequence param1CharSequence, ParcelFileDescriptor param1ParcelFileDescriptor, Bundle param1Bundle, String param1String) {
        if (!checkNonNull(new Object[] { param1IBinder, param1CharSequence, param1ParcelFileDescriptor, param1Bundle }))
          return -1; 
        param1ParcelFileDescriptor = ParcelFileDescriptor.adoptFd(param1ParcelFileDescriptor.detachFd());
        TextToSpeechService textToSpeechService = TextToSpeechService.this;
        int i = Binder.getCallingUid();
        TextToSpeechService.SynthesisToFileOutputStreamSpeechItem synthesisToFileOutputStreamSpeechItem = new TextToSpeechService.SynthesisToFileOutputStreamSpeechItem(param1IBinder, i, Binder.getCallingPid(), param1Bundle, param1String, param1CharSequence, (FileOutputStream)new ParcelFileDescriptor.AutoCloseOutputStream(param1ParcelFileDescriptor));
        return TextToSpeechService.this.mSynthHandler.enqueueSpeechItem(1, synthesisToFileOutputStreamSpeechItem);
      }
      
      public int playAudio(IBinder param1IBinder, Uri param1Uri, int param1Int, Bundle param1Bundle, String param1String) {
        if (!checkNonNull(new Object[] { param1IBinder, param1Uri, param1Bundle }))
          return -1; 
        TextToSpeechService textToSpeechService = TextToSpeechService.this;
        int i = Binder.getCallingUid();
        TextToSpeechService.AudioSpeechItem audioSpeechItem = new TextToSpeechService.AudioSpeechItem(param1IBinder, i, Binder.getCallingPid(), param1Bundle, param1String, param1Uri);
        return TextToSpeechService.this.mSynthHandler.enqueueSpeechItem(param1Int, audioSpeechItem);
      }
      
      public int playSilence(IBinder param1IBinder, long param1Long, int param1Int, String param1String) {
        if (!checkNonNull(new Object[] { param1IBinder }))
          return -1; 
        TextToSpeechService textToSpeechService = TextToSpeechService.this;
        int i = Binder.getCallingUid();
        TextToSpeechService.SilenceSpeechItem silenceSpeechItem = new TextToSpeechService.SilenceSpeechItem(param1IBinder, i, Binder.getCallingPid(), param1String, param1Long);
        return TextToSpeechService.this.mSynthHandler.enqueueSpeechItem(param1Int, silenceSpeechItem);
      }
      
      public boolean isSpeaking() {
        return (TextToSpeechService.this.mSynthHandler.isSpeaking() || TextToSpeechService.this.mAudioPlaybackHandler.isSpeaking());
      }
      
      public int stop(IBinder param1IBinder) {
        if (!checkNonNull(new Object[] { param1IBinder }))
          return -1; 
        return TextToSpeechService.this.mSynthHandler.stopForApp(param1IBinder);
      }
      
      public String[] getLanguage() {
        return TextToSpeechService.this.onGetLanguage();
      }
      
      public String[] getClientDefaultLanguage() {
        return TextToSpeechService.this.getSettingsLocale();
      }
      
      public int isLanguageAvailable(String param1String1, String param1String2, String param1String3) {
        if (!checkNonNull(new Object[] { param1String1 }))
          return -1; 
        return TextToSpeechService.this.onIsLanguageAvailable(param1String1, param1String2, param1String3);
      }
      
      public String[] getFeaturesForLanguage(String param1String1, String param1String2, String param1String3) {
        String[] arrayOfString;
        Set<String> set = TextToSpeechService.this.onGetFeaturesForLanguage(param1String1, param1String2, param1String3);
        if (set != null) {
          arrayOfString = new String[set.size()];
          set.toArray(arrayOfString);
        } else {
          arrayOfString = new String[0];
        } 
        return arrayOfString;
      }
      
      public int loadLanguage(IBinder param1IBinder, String param1String1, String param1String2, String param1String3) {
        if (!checkNonNull(new Object[] { param1String1 }))
          return -1; 
        int i = TextToSpeechService.this.onIsLanguageAvailable(param1String1, param1String2, param1String3);
        if (i == 0 || i == 1 || i == 2) {
          TextToSpeechService textToSpeechService = TextToSpeechService.this;
          int j = Binder.getCallingUid();
          TextToSpeechService.LoadLanguageItem loadLanguageItem = new TextToSpeechService.LoadLanguageItem(param1IBinder, j, Binder.getCallingPid(), param1String1, param1String2, param1String3);
          if (TextToSpeechService.this.mSynthHandler.enqueueSpeechItem(1, loadLanguageItem) != 0)
            return -1; 
        } 
        return i;
      }
      
      public List<Voice> getVoices() {
        return TextToSpeechService.this.onGetVoices();
      }
      
      public int loadVoice(IBinder param1IBinder, String param1String) {
        if (!checkNonNull(new Object[] { param1String }))
          return -1; 
        int i = TextToSpeechService.this.onIsValidVoiceName(param1String);
        if (i == 0) {
          TextToSpeechService textToSpeechService = TextToSpeechService.this;
          int j = Binder.getCallingUid();
          TextToSpeechService.LoadVoiceItem loadVoiceItem = new TextToSpeechService.LoadVoiceItem(param1IBinder, j, Binder.getCallingPid(), param1String);
          if (TextToSpeechService.this.mSynthHandler.enqueueSpeechItem(1, loadVoiceItem) != 0)
            return -1; 
        } 
        return i;
      }
      
      public String getDefaultVoiceNameFor(String param1String1, String param1String2, String param1String3) {
        if (!checkNonNull(new Object[] { param1String1 }))
          return null; 
        int i = TextToSpeechService.this.onIsLanguageAvailable(param1String1, param1String2, param1String3);
        if (i == 0 || i == 1 || i == 2)
          return TextToSpeechService.this.onGetDefaultVoiceNameFor(param1String1, param1String2, param1String3); 
        return null;
      }
      
      public void setCallback(IBinder param1IBinder, ITextToSpeechCallback param1ITextToSpeechCallback) {
        if (!checkNonNull(new Object[] { param1IBinder }))
          return; 
        TextToSpeechService.this.mCallbacks.setCallback(param1IBinder, param1ITextToSpeechCallback);
      }
      
      private String intern(String param1String) {
        return param1String.intern();
      }
      
      private boolean checkNonNull(Object... param1VarArgs) {
        int i;
        byte b;
        for (i = param1VarArgs.length, b = 0; b < i; ) {
          Object object = param1VarArgs[b];
          if (object == null)
            return false; 
          b++;
        } 
        return true;
      }
    };
  
  private AudioPlaybackHandler mAudioPlaybackHandler;
  
  private static final String TAG = "TextToSpeechService";
  
  private static final String SYNTH_THREAD_NAME = "SynthThread";
  
  private static final boolean DBG = false;
  
  protected abstract String[] onGetLanguage();
  
  protected abstract int onIsLanguageAvailable(String paramString1, String paramString2, String paramString3);
  
  protected abstract int onLoadLanguage(String paramString1, String paramString2, String paramString3);
  
  protected abstract void onStop();
  
  protected abstract void onSynthesizeText(SynthesisRequest paramSynthesisRequest, SynthesisCallback paramSynthesisCallback);
  
  class CallbackMap extends RemoteCallbackList<ITextToSpeechCallback> {
    private final HashMap<IBinder, ITextToSpeechCallback> mCallerToCallback;
    
    final TextToSpeechService this$0;
    
    private CallbackMap() {
      this.mCallerToCallback = new HashMap<>();
    }
    
    public void setCallback(IBinder param1IBinder, ITextToSpeechCallback param1ITextToSpeechCallback) {
      // Byte code:
      //   0: aload_0
      //   1: getfield mCallerToCallback : Ljava/util/HashMap;
      //   4: astore_3
      //   5: aload_3
      //   6: monitorenter
      //   7: aload_2
      //   8: ifnull -> 34
      //   11: aload_0
      //   12: aload_2
      //   13: aload_1
      //   14: invokevirtual register : (Landroid/os/IInterface;Ljava/lang/Object;)Z
      //   17: pop
      //   18: aload_0
      //   19: getfield mCallerToCallback : Ljava/util/HashMap;
      //   22: aload_1
      //   23: aload_2
      //   24: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
      //   27: checkcast android/speech/tts/ITextToSpeechCallback
      //   30: astore_1
      //   31: goto -> 46
      //   34: aload_0
      //   35: getfield mCallerToCallback : Ljava/util/HashMap;
      //   38: aload_1
      //   39: invokevirtual remove : (Ljava/lang/Object;)Ljava/lang/Object;
      //   42: checkcast android/speech/tts/ITextToSpeechCallback
      //   45: astore_1
      //   46: aload_1
      //   47: ifnull -> 61
      //   50: aload_1
      //   51: aload_2
      //   52: if_acmpeq -> 61
      //   55: aload_0
      //   56: aload_1
      //   57: invokevirtual unregister : (Landroid/os/IInterface;)Z
      //   60: pop
      //   61: aload_3
      //   62: monitorexit
      //   63: return
      //   64: astore_1
      //   65: aload_3
      //   66: monitorexit
      //   67: aload_1
      //   68: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1521	-> 0
      //   #1523	-> 7
      //   #1524	-> 11
      //   #1525	-> 18
      //   #1527	-> 34
      //   #1529	-> 46
      //   #1530	-> 55
      //   #1532	-> 61
      //   #1533	-> 63
      //   #1532	-> 64
      // Exception table:
      //   from	to	target	type
      //   11	18	64	finally
      //   18	31	64	finally
      //   34	46	64	finally
      //   55	61	64	finally
      //   61	63	64	finally
      //   65	67	64	finally
    }
    
    public void dispatchOnStop(Object param1Object, String param1String, boolean param1Boolean) {
      param1Object = getCallbackFor(param1Object);
      if (param1Object == null)
        return; 
      try {
        param1Object.onStop(param1String, param1Boolean);
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Callback onStop failed: ");
        stringBuilder.append(remoteException);
        Log.e("TextToSpeechService", stringBuilder.toString());
      } 
    }
    
    public void dispatchOnSuccess(Object param1Object, String param1String) {
      param1Object = getCallbackFor(param1Object);
      if (param1Object == null)
        return; 
      try {
        param1Object.onSuccess(param1String);
      } catch (RemoteException remoteException) {
        param1Object = new StringBuilder();
        param1Object.append("Callback onDone failed: ");
        param1Object.append(remoteException);
        Log.e("TextToSpeechService", param1Object.toString());
      } 
    }
    
    public void dispatchOnStart(Object param1Object, String param1String) {
      param1Object = getCallbackFor(param1Object);
      if (param1Object == null)
        return; 
      try {
        param1Object.onStart(param1String);
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Callback onStart failed: ");
        stringBuilder.append(remoteException);
        Log.e("TextToSpeechService", stringBuilder.toString());
      } 
    }
    
    public void dispatchOnError(Object param1Object, String param1String, int param1Int) {
      param1Object = getCallbackFor(param1Object);
      if (param1Object == null)
        return; 
      try {
        param1Object.onError(param1String, param1Int);
      } catch (RemoteException remoteException) {
        param1Object = new StringBuilder();
        param1Object.append("Callback onError failed: ");
        param1Object.append(remoteException);
        Log.e("TextToSpeechService", param1Object.toString());
      } 
    }
    
    public void dispatchOnBeginSynthesis(Object param1Object, String param1String, int param1Int1, int param1Int2, int param1Int3) {
      param1Object = getCallbackFor(param1Object);
      if (param1Object == null)
        return; 
      try {
        param1Object.onBeginSynthesis(param1String, param1Int1, param1Int2, param1Int3);
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Callback dispatchOnBeginSynthesis(String, int, int, int) failed: ");
        stringBuilder.append(remoteException);
        Log.e("TextToSpeechService", stringBuilder.toString());
      } 
    }
    
    public void dispatchOnAudioAvailable(Object param1Object, String param1String, byte[] param1ArrayOfbyte) {
      param1Object = getCallbackFor(param1Object);
      if (param1Object == null)
        return; 
      try {
        param1Object.onAudioAvailable(param1String, param1ArrayOfbyte);
      } catch (RemoteException remoteException) {
        param1Object = new StringBuilder();
        param1Object.append("Callback dispatchOnAudioAvailable(String, byte[]) failed: ");
        param1Object.append(remoteException);
        Log.e("TextToSpeechService", param1Object.toString());
      } 
    }
    
    public void dispatchOnRangeStart(Object param1Object, String param1String, int param1Int1, int param1Int2, int param1Int3) {
      param1Object = getCallbackFor(param1Object);
      if (param1Object == null)
        return; 
      try {
        param1Object.onRangeStart(param1String, param1Int1, param1Int2, param1Int3);
      } catch (RemoteException remoteException) {
        param1Object = new StringBuilder();
        param1Object.append("Callback dispatchOnRangeStart(String, int, int, int) failed: ");
        param1Object.append(remoteException);
        Log.e("TextToSpeechService", param1Object.toString());
      } 
    }
    
    public void onCallbackDied(ITextToSpeechCallback param1ITextToSpeechCallback, Object param1Object) {
      param1Object = param1Object;
      synchronized (this.mCallerToCallback) {
        this.mCallerToCallback.remove(param1Object);
        TextToSpeechService.this.mSynthHandler.stopForApp(param1Object);
        return;
      } 
    }
    
    public void kill() {
      synchronized (this.mCallerToCallback) {
        this.mCallerToCallback.clear();
        super.kill();
        return;
      } 
    }
    
    private ITextToSpeechCallback getCallbackFor(Object<IBinder, ITextToSpeechCallback> param1Object) {
      null = (IBinder)param1Object;
      synchronized (this.mCallerToCallback) {
        return this.mCallerToCallback.get(null);
      } 
    }
  }
  
  class UtteranceProgressDispatcher {
    public abstract void dispatchOnAudioAvailable(byte[] param1ArrayOfbyte);
    
    public abstract void dispatchOnBeginSynthesis(int param1Int1, int param1Int2, int param1Int3);
    
    public abstract void dispatchOnError(int param1Int);
    
    public abstract void dispatchOnRangeStart(int param1Int1, int param1Int2, int param1Int3);
    
    public abstract void dispatchOnStart();
    
    public abstract void dispatchOnStop();
    
    public abstract void dispatchOnSuccess();
  }
}
