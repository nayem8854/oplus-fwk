package android.speech.tts;

import android.os.Bundle;

public final class SynthesisRequest {
  private int mCallerUid;
  
  private String mCountry;
  
  private String mLanguage;
  
  private final Bundle mParams;
  
  private int mPitch;
  
  private int mSpeechRate;
  
  private final CharSequence mText;
  
  private String mVariant;
  
  private String mVoiceName;
  
  public SynthesisRequest(String paramString, Bundle paramBundle) {
    this.mText = paramString;
    this.mParams = new Bundle(paramBundle);
  }
  
  public SynthesisRequest(CharSequence paramCharSequence, Bundle paramBundle) {
    this.mText = paramCharSequence;
    this.mParams = new Bundle(paramBundle);
  }
  
  @Deprecated
  public String getText() {
    return this.mText.toString();
  }
  
  public CharSequence getCharSequenceText() {
    return this.mText;
  }
  
  public String getVoiceName() {
    return this.mVoiceName;
  }
  
  public String getLanguage() {
    return this.mLanguage;
  }
  
  public String getCountry() {
    return this.mCountry;
  }
  
  public String getVariant() {
    return this.mVariant;
  }
  
  public int getSpeechRate() {
    return this.mSpeechRate;
  }
  
  public int getPitch() {
    return this.mPitch;
  }
  
  public Bundle getParams() {
    return this.mParams;
  }
  
  public int getCallerUid() {
    return this.mCallerUid;
  }
  
  void setLanguage(String paramString1, String paramString2, String paramString3) {
    this.mLanguage = paramString1;
    this.mCountry = paramString2;
    this.mVariant = paramString3;
  }
  
  void setVoiceName(String paramString) {
    this.mVoiceName = paramString;
  }
  
  void setSpeechRate(int paramInt) {
    this.mSpeechRate = paramInt;
  }
  
  void setPitch(int paramInt) {
    this.mPitch = paramInt;
  }
  
  void setCallerUid(int paramInt) {
    this.mCallerUid = paramInt;
  }
}
