package android.speech.tts;

import android.media.AudioTrack;
import android.util.Log;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

final class SynthesisPlaybackQueueItem extends PlaybackQueueItem implements AudioTrack.OnPlaybackPositionUpdateListener {
  private static final boolean DBG = false;
  
  private static final long MAX_UNCONSUMED_AUDIO_MS = 500L;
  
  private static final int NOT_RUN = 0;
  
  private static final int RUN_CALLED = 1;
  
  private static final int STOP_CALLED = 2;
  
  private static final String TAG = "TTS.SynthQueueItem";
  
  private final BlockingAudioTrack mAudioTrack;
  
  private final LinkedList<ListEntry> mDataBufferList;
  
  private volatile boolean mDone;
  
  private final Lock mListLock;
  
  private final AbstractEventLogger mLogger;
  
  private final Condition mNotFull;
  
  private final Condition mReadReady;
  
  private final AtomicInteger mRunState;
  
  private volatile int mStatusCode;
  
  private volatile boolean mStopped;
  
  private int mUnconsumedBytes;
  
  private ConcurrentLinkedQueue<ProgressMarker> markerList;
  
  SynthesisPlaybackQueueItem(TextToSpeechService.AudioOutputParams paramAudioOutputParams, int paramInt1, int paramInt2, int paramInt3, TextToSpeechService.UtteranceProgressDispatcher paramUtteranceProgressDispatcher, Object paramObject, AbstractEventLogger paramAbstractEventLogger) {
    super(paramUtteranceProgressDispatcher, paramObject);
    ReentrantLock reentrantLock = new ReentrantLock();
    this.mReadReady = reentrantLock.newCondition();
    this.mNotFull = this.mListLock.newCondition();
    this.mDataBufferList = new LinkedList<>();
    this.markerList = new ConcurrentLinkedQueue<>();
    this.mRunState = new AtomicInteger(0);
    this.mUnconsumedBytes = 0;
    this.mStopped = false;
    this.mDone = false;
    this.mStatusCode = 0;
    this.mAudioTrack = new BlockingAudioTrack(paramAudioOutputParams, paramInt1, paramInt2, paramInt3);
    this.mLogger = paramAbstractEventLogger;
  }
  
  public void run() {
    if (!this.mRunState.compareAndSet(0, 1))
      return; 
    TextToSpeechService.UtteranceProgressDispatcher utteranceProgressDispatcher = getDispatcher();
    utteranceProgressDispatcher.dispatchOnStart();
    if (!this.mAudioTrack.init()) {
      utteranceProgressDispatcher.dispatchOnError(-5);
      return;
    } 
    this.mAudioTrack.setPlaybackPositionUpdateListener(this);
    updateMarker();
    try {
      while (true) {
        byte[] arrayOfByte = take();
        if (arrayOfByte != null) {
          this.mAudioTrack.write(arrayOfByte);
          this.mLogger.onAudioDataWritten();
          continue;
        } 
        break;
      } 
    } catch (InterruptedException interruptedException) {}
    this.mAudioTrack.waitAndRelease();
    dispatchEndStatus();
  }
  
  private void dispatchEndStatus() {
    TextToSpeechService.UtteranceProgressDispatcher utteranceProgressDispatcher = getDispatcher();
    if (this.mStatusCode == 0) {
      utteranceProgressDispatcher.dispatchOnSuccess();
    } else if (this.mStatusCode == -2) {
      utteranceProgressDispatcher.dispatchOnStop();
    } else {
      utteranceProgressDispatcher.dispatchOnError(this.mStatusCode);
    } 
    this.mLogger.onCompleted(this.mStatusCode);
  }
  
  void stop(int paramInt) {
    try {
      this.mListLock.lock();
      this.mStopped = true;
      this.mStatusCode = paramInt;
      this.mNotFull.signal();
      if (this.mRunState.getAndSet(2) == 0) {
        dispatchEndStatus();
        return;
      } 
      this.mReadReady.signal();
      this.mListLock.unlock();
      return;
    } finally {
      this.mListLock.unlock();
    } 
  }
  
  void done() {
    try {
      this.mListLock.lock();
      this.mDone = true;
      this.mReadReady.signal();
      this.mNotFull.signal();
      return;
    } finally {
      this.mListLock.unlock();
    } 
  }
  
  class ProgressMarker {
    public final int end;
    
    public final int frames;
    
    public final int start;
    
    final SynthesisPlaybackQueueItem this$0;
    
    public ProgressMarker(int param1Int1, int param1Int2, int param1Int3) {
      this.frames = param1Int1;
      this.start = param1Int2;
      this.end = param1Int3;
    }
  }
  
  void updateMarker() {
    ProgressMarker progressMarker = this.markerList.peek();
    if (progressMarker != null) {
      int i;
      if (progressMarker.frames == 0) {
        i = 1;
      } else {
        i = progressMarker.frames;
      } 
      this.mAudioTrack.setNotificationMarkerPosition(i);
    } 
  }
  
  void rangeStart(int paramInt1, int paramInt2, int paramInt3) {
    this.markerList.add(new ProgressMarker(paramInt1, paramInt2, paramInt3));
    updateMarker();
  }
  
  public void onMarkerReached(AudioTrack paramAudioTrack) {
    ProgressMarker progressMarker = this.markerList.poll();
    if (progressMarker == null) {
      Log.e("TTS.SynthQueueItem", "onMarkerReached reached called but no marker in queue");
      return;
    } 
    getDispatcher().dispatchOnRangeStart(progressMarker.start, progressMarker.end, progressMarker.frames);
    updateMarker();
  }
  
  public void onPeriodicNotification(AudioTrack paramAudioTrack) {}
  
  void put(byte[] paramArrayOfbyte) throws InterruptedException {
    try {
      this.mListLock.lock();
      while (this.mAudioTrack.getAudioLengthMs(this.mUnconsumedBytes) > 500L && !this.mStopped)
        this.mNotFull.await(); 
      boolean bool = this.mStopped;
      if (bool)
        return; 
      LinkedList<ListEntry> linkedList = this.mDataBufferList;
      ListEntry listEntry = new ListEntry();
      this(paramArrayOfbyte);
      linkedList.add(listEntry);
      this.mUnconsumedBytes += paramArrayOfbyte.length;
      this.mReadReady.signal();
      return;
    } finally {
      this.mListLock.unlock();
    } 
  }
  
  private byte[] take() throws InterruptedException {
    try {
      this.mListLock.lock();
      while (this.mDataBufferList.size() == 0 && !this.mStopped && !this.mDone)
        this.mReadReady.await(); 
      boolean bool = this.mStopped;
      if (bool)
        return null; 
      ListEntry listEntry = this.mDataBufferList.poll();
      if (listEntry == null)
        return null; 
      this.mUnconsumedBytes -= listEntry.mBytes.length;
      this.mNotFull.signal();
      return listEntry.mBytes;
    } finally {
      this.mListLock.unlock();
    } 
  }
  
  class ListEntry {
    final byte[] mBytes;
    
    ListEntry(SynthesisPlaybackQueueItem this$0) {
      this.mBytes = (byte[])this$0;
    }
  }
}
