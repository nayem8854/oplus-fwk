package android.speech.tts;

import android.os.ConditionVariable;

class SilencePlaybackQueueItem extends PlaybackQueueItem {
  private final ConditionVariable mCondVar = new ConditionVariable();
  
  private final long mSilenceDurationMs;
  
  SilencePlaybackQueueItem(TextToSpeechService.UtteranceProgressDispatcher paramUtteranceProgressDispatcher, Object paramObject, long paramLong) {
    super(paramUtteranceProgressDispatcher, paramObject);
    this.mSilenceDurationMs = paramLong;
  }
  
  public void run() {
    getDispatcher().dispatchOnStart();
    boolean bool = false;
    long l = this.mSilenceDurationMs;
    if (l > 0L)
      bool = this.mCondVar.block(l); 
    if (bool) {
      getDispatcher().dispatchOnStop();
    } else {
      getDispatcher().dispatchOnSuccess();
    } 
  }
  
  void stop(int paramInt) {
    this.mCondVar.open();
  }
}
