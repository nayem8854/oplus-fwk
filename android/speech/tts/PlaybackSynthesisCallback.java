package android.speech.tts;

import android.util.Log;

class PlaybackSynthesisCallback extends AbstractSynthesisCallback {
  protected int mStatusCode;
  
  private final Object mStateLock = new Object();
  
  private final AbstractEventLogger mLogger;
  
  private SynthesisPlaybackQueueItem mItem = null;
  
  private volatile boolean mDone = false;
  
  private final TextToSpeechService.UtteranceProgressDispatcher mDispatcher;
  
  private final Object mCallerIdentity;
  
  private final AudioPlaybackHandler mAudioTrackHandler;
  
  private final TextToSpeechService.AudioOutputParams mAudioParams;
  
  private static final String TAG = "PlaybackSynthesisRequest";
  
  private static final int MIN_AUDIO_BUFFER_SIZE = 8192;
  
  private static final boolean DBG = false;
  
  PlaybackSynthesisCallback(TextToSpeechService.AudioOutputParams paramAudioOutputParams, AudioPlaybackHandler paramAudioPlaybackHandler, TextToSpeechService.UtteranceProgressDispatcher paramUtteranceProgressDispatcher, Object paramObject, AbstractEventLogger paramAbstractEventLogger, boolean paramBoolean) {
    super(paramBoolean);
    this.mAudioParams = paramAudioOutputParams;
    this.mAudioTrackHandler = paramAudioPlaybackHandler;
    this.mDispatcher = paramUtteranceProgressDispatcher;
    this.mCallerIdentity = paramObject;
    this.mLogger = paramAbstractEventLogger;
    this.mStatusCode = 0;
  }
  
  void stop() {
    synchronized (this.mStateLock) {
      if (this.mDone)
        return; 
      if (this.mStatusCode == -2) {
        Log.w("PlaybackSynthesisRequest", "stop() called twice");
        return;
      } 
      SynthesisPlaybackQueueItem synthesisPlaybackQueueItem = this.mItem;
      this.mStatusCode = -2;
      if (synthesisPlaybackQueueItem != null) {
        synthesisPlaybackQueueItem.stop(-2);
      } else {
        this.mLogger.onCompleted(-2);
        this.mDispatcher.dispatchOnStop();
      } 
      return;
    } 
  }
  
  public int getMaxBufferSize() {
    return 8192;
  }
  
  public boolean hasStarted() {
    synchronized (this.mStateLock) {
      boolean bool;
      if (this.mItem != null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    } 
  }
  
  public boolean hasFinished() {
    synchronized (this.mStateLock) {
      return this.mDone;
    } 
  }
  
  public int start(int paramInt1, int paramInt2, int paramInt3) {
    // Byte code:
    //   0: iload_2
    //   1: iconst_3
    //   2: if_icmpeq -> 58
    //   5: iload_2
    //   6: iconst_2
    //   7: if_icmpeq -> 58
    //   10: iload_2
    //   11: iconst_4
    //   12: if_icmpeq -> 58
    //   15: new java/lang/StringBuilder
    //   18: dup
    //   19: invokespecial <init> : ()V
    //   22: astore #4
    //   24: aload #4
    //   26: ldc 'Audio format encoding '
    //   28: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   31: pop
    //   32: aload #4
    //   34: iload_2
    //   35: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   38: pop
    //   39: aload #4
    //   41: ldc ' not supported. Please use one of AudioFormat.ENCODING_PCM_8BIT, AudioFormat.ENCODING_PCM_16BIT or AudioFormat.ENCODING_PCM_FLOAT'
    //   43: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   46: pop
    //   47: ldc 'PlaybackSynthesisRequest'
    //   49: aload #4
    //   51: invokevirtual toString : ()Ljava/lang/String;
    //   54: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   57: pop
    //   58: aload_0
    //   59: getfield mDispatcher : Landroid/speech/tts/TextToSpeechService$UtteranceProgressDispatcher;
    //   62: iload_1
    //   63: iload_2
    //   64: iload_3
    //   65: invokeinterface dispatchOnBeginSynthesis : (III)V
    //   70: iload_3
    //   71: invokestatic getChannelConfig : (I)I
    //   74: istore #5
    //   76: aload_0
    //   77: getfield mStateLock : Ljava/lang/Object;
    //   80: astore #4
    //   82: aload #4
    //   84: monitorenter
    //   85: iload #5
    //   87: ifne -> 137
    //   90: new java/lang/StringBuilder
    //   93: astore #6
    //   95: aload #6
    //   97: invokespecial <init> : ()V
    //   100: aload #6
    //   102: ldc 'Unsupported number of channels :'
    //   104: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   107: pop
    //   108: aload #6
    //   110: iload_3
    //   111: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   114: pop
    //   115: ldc 'PlaybackSynthesisRequest'
    //   117: aload #6
    //   119: invokevirtual toString : ()Ljava/lang/String;
    //   122: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   125: pop
    //   126: aload_0
    //   127: bipush #-5
    //   129: putfield mStatusCode : I
    //   132: aload #4
    //   134: monitorexit
    //   135: iconst_m1
    //   136: ireturn
    //   137: aload_0
    //   138: getfield mStatusCode : I
    //   141: bipush #-2
    //   143: if_icmpne -> 156
    //   146: aload_0
    //   147: invokevirtual errorCodeOnStop : ()I
    //   150: istore_1
    //   151: aload #4
    //   153: monitorexit
    //   154: iload_1
    //   155: ireturn
    //   156: aload_0
    //   157: getfield mStatusCode : I
    //   160: ifeq -> 168
    //   163: aload #4
    //   165: monitorexit
    //   166: iconst_m1
    //   167: ireturn
    //   168: aload_0
    //   169: getfield mItem : Landroid/speech/tts/SynthesisPlaybackQueueItem;
    //   172: ifnull -> 188
    //   175: ldc 'PlaybackSynthesisRequest'
    //   177: ldc 'Start called twice'
    //   179: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   182: pop
    //   183: aload #4
    //   185: monitorexit
    //   186: iconst_m1
    //   187: ireturn
    //   188: new android/speech/tts/SynthesisPlaybackQueueItem
    //   191: astore #6
    //   193: aload #6
    //   195: aload_0
    //   196: getfield mAudioParams : Landroid/speech/tts/TextToSpeechService$AudioOutputParams;
    //   199: iload_1
    //   200: iload_2
    //   201: iload_3
    //   202: aload_0
    //   203: getfield mDispatcher : Landroid/speech/tts/TextToSpeechService$UtteranceProgressDispatcher;
    //   206: aload_0
    //   207: getfield mCallerIdentity : Ljava/lang/Object;
    //   210: aload_0
    //   211: getfield mLogger : Landroid/speech/tts/AbstractEventLogger;
    //   214: invokespecial <init> : (Landroid/speech/tts/TextToSpeechService$AudioOutputParams;IIILandroid/speech/tts/TextToSpeechService$UtteranceProgressDispatcher;Ljava/lang/Object;Landroid/speech/tts/AbstractEventLogger;)V
    //   217: aload_0
    //   218: getfield mAudioTrackHandler : Landroid/speech/tts/AudioPlaybackHandler;
    //   221: aload #6
    //   223: invokevirtual enqueue : (Landroid/speech/tts/PlaybackQueueItem;)V
    //   226: aload_0
    //   227: aload #6
    //   229: putfield mItem : Landroid/speech/tts/SynthesisPlaybackQueueItem;
    //   232: aload #4
    //   234: monitorexit
    //   235: iconst_0
    //   236: ireturn
    //   237: astore #6
    //   239: aload #4
    //   241: monitorexit
    //   242: aload #6
    //   244: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #128	-> 0
    //   #131	-> 15
    //   #135	-> 58
    //   #137	-> 70
    //   #139	-> 76
    //   #140	-> 85
    //   #141	-> 90
    //   #142	-> 126
    //   #143	-> 132
    //   #145	-> 137
    //   #147	-> 146
    //   #149	-> 156
    //   #151	-> 163
    //   #153	-> 168
    //   #154	-> 175
    //   #155	-> 183
    //   #157	-> 188
    //   #160	-> 217
    //   #161	-> 226
    //   #162	-> 232
    //   #164	-> 235
    //   #162	-> 237
    // Exception table:
    //   from	to	target	type
    //   90	126	237	finally
    //   126	132	237	finally
    //   132	135	237	finally
    //   137	146	237	finally
    //   146	154	237	finally
    //   156	163	237	finally
    //   163	166	237	finally
    //   168	175	237	finally
    //   175	183	237	finally
    //   183	186	237	finally
    //   188	217	237	finally
    //   217	226	237	finally
    //   226	232	237	finally
    //   232	235	237	finally
    //   239	242	237	finally
  }
  
  public int audioAvailable(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    if (paramInt2 <= getMaxBufferSize() && paramInt2 > 0)
      synchronized (this.mStateLock) {
        if (this.mItem == null) {
          this.mStatusCode = -5;
          return -1;
        } 
        if (this.mStatusCode != 0)
          return -1; 
        if (this.mStatusCode == -2) {
          paramInt1 = errorCodeOnStop();
          return paramInt1;
        } 
        SynthesisPlaybackQueueItem synthesisPlaybackQueueItem = this.mItem;
        null = new byte[paramInt2];
        System.arraycopy(paramArrayOfbyte, paramInt1, null, 0, paramInt2);
        this.mDispatcher.dispatchOnAudioAvailable((byte[])null);
        try {
          synthesisPlaybackQueueItem.put((byte[])null);
          this.mLogger.onEngineDataReceived();
          return 0;
        } catch (InterruptedException null) {
          synchronized (this.mStateLock) {
            this.mStatusCode = -5;
            return -1;
          } 
        } 
      }  
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("buffer is too large or of zero length (");
    stringBuilder.append(paramInt2);
    stringBuilder.append(" bytes)");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public int done() {
    synchronized (this.mStateLock) {
      if (this.mDone) {
        Log.w("PlaybackSynthesisRequest", "Duplicate call to done()");
        return -1;
      } 
      if (this.mStatusCode == -2)
        return errorCodeOnStop(); 
      this.mDone = true;
      if (this.mItem == null) {
        Log.w("PlaybackSynthesisRequest", "done() was called before start() call");
        if (this.mStatusCode == 0) {
          this.mDispatcher.dispatchOnSuccess();
        } else {
          this.mDispatcher.dispatchOnError(this.mStatusCode);
        } 
        this.mLogger.onEngineComplete();
        return -1;
      } 
      SynthesisPlaybackQueueItem synthesisPlaybackQueueItem = this.mItem;
      int i = this.mStatusCode;
      if (i == 0) {
        synthesisPlaybackQueueItem.done();
      } else {
        synthesisPlaybackQueueItem.stop(i);
      } 
      this.mLogger.onEngineComplete();
      return 0;
    } 
  }
  
  public void error() {
    error(-3);
  }
  
  public void error(int paramInt) {
    synchronized (this.mStateLock) {
      if (this.mDone)
        return; 
      this.mStatusCode = paramInt;
      return;
    } 
  }
  
  public void rangeStart(int paramInt1, int paramInt2, int paramInt3) {
    SynthesisPlaybackQueueItem synthesisPlaybackQueueItem = this.mItem;
    if (synthesisPlaybackQueueItem == null) {
      Log.e("PlaybackSynthesisRequest", "mItem is null");
      return;
    } 
    synthesisPlaybackQueueItem.rangeStart(paramInt1, paramInt2, paramInt3);
  }
}
