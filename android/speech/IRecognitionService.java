package android.speech;

import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IRecognitionService extends IInterface {
  void cancel(IRecognitionListener paramIRecognitionListener, String paramString1, String paramString2) throws RemoteException;
  
  void startListening(Intent paramIntent, IRecognitionListener paramIRecognitionListener, String paramString1, String paramString2) throws RemoteException;
  
  void stopListening(IRecognitionListener paramIRecognitionListener, String paramString1, String paramString2) throws RemoteException;
  
  class Default implements IRecognitionService {
    public void startListening(Intent param1Intent, IRecognitionListener param1IRecognitionListener, String param1String1, String param1String2) throws RemoteException {}
    
    public void stopListening(IRecognitionListener param1IRecognitionListener, String param1String1, String param1String2) throws RemoteException {}
    
    public void cancel(IRecognitionListener param1IRecognitionListener, String param1String1, String param1String2) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IRecognitionService {
    private static final String DESCRIPTOR = "android.speech.IRecognitionService";
    
    static final int TRANSACTION_cancel = 3;
    
    static final int TRANSACTION_startListening = 1;
    
    static final int TRANSACTION_stopListening = 2;
    
    public Stub() {
      attachInterface(this, "android.speech.IRecognitionService");
    }
    
    public static IRecognitionService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.speech.IRecognitionService");
      if (iInterface != null && iInterface instanceof IRecognitionService)
        return (IRecognitionService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "cancel";
        } 
        return "stopListening";
      } 
      return "startListening";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.speech.IRecognitionService");
            return true;
          } 
          param1Parcel1.enforceInterface("android.speech.IRecognitionService");
          IRecognitionListener iRecognitionListener2 = IRecognitionListener.Stub.asInterface(param1Parcel1.readStrongBinder());
          String str3 = param1Parcel1.readString();
          str1 = param1Parcel1.readString();
          cancel(iRecognitionListener2, str3, str1);
          return true;
        } 
        str1.enforceInterface("android.speech.IRecognitionService");
        IRecognitionListener iRecognitionListener1 = IRecognitionListener.Stub.asInterface(str1.readStrongBinder());
        String str = str1.readString();
        str1 = str1.readString();
        stopListening(iRecognitionListener1, str, str1);
        return true;
      } 
      str1.enforceInterface("android.speech.IRecognitionService");
      if (str1.readInt() != 0) {
        Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)str1);
      } else {
        param1Parcel2 = null;
      } 
      IRecognitionListener iRecognitionListener = IRecognitionListener.Stub.asInterface(str1.readStrongBinder());
      String str2 = str1.readString();
      String str1 = str1.readString();
      startListening((Intent)param1Parcel2, iRecognitionListener, str2, str1);
      return true;
    }
    
    private static class Proxy implements IRecognitionService {
      public static IRecognitionService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.speech.IRecognitionService";
      }
      
      public void startListening(Intent param2Intent, IRecognitionListener param2IRecognitionListener, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.speech.IRecognitionService");
          if (param2Intent != null) {
            parcel.writeInt(1);
            param2Intent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2IRecognitionListener != null) {
            iBinder = param2IRecognitionListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IRecognitionService.Stub.getDefaultImpl() != null) {
            IRecognitionService.Stub.getDefaultImpl().startListening(param2Intent, param2IRecognitionListener, param2String1, param2String2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void stopListening(IRecognitionListener param2IRecognitionListener, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.speech.IRecognitionService");
          if (param2IRecognitionListener != null) {
            iBinder = param2IRecognitionListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IRecognitionService.Stub.getDefaultImpl() != null) {
            IRecognitionService.Stub.getDefaultImpl().stopListening(param2IRecognitionListener, param2String1, param2String2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void cancel(IRecognitionListener param2IRecognitionListener, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.speech.IRecognitionService");
          if (param2IRecognitionListener != null) {
            iBinder = param2IRecognitionListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IRecognitionService.Stub.getDefaultImpl() != null) {
            IRecognitionService.Stub.getDefaultImpl().cancel(param2IRecognitionListener, param2String1, param2String2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IRecognitionService param1IRecognitionService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IRecognitionService != null) {
          Proxy.sDefaultImpl = param1IRecognitionService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IRecognitionService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
