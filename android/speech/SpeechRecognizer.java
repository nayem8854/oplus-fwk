package android.speech;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.util.SeempLog;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class SpeechRecognizer {
  private Handler mHandler = (Handler)new Object(this);
  
  private final Queue<Message> mPendingTasks = new LinkedList<>();
  
  private final InternalListener mListener = new InternalListener();
  
  public static final String CONFIDENCE_SCORES = "confidence_scores";
  
  private static final boolean DBG = false;
  
  public static final int ERROR_AUDIO = 3;
  
  public static final int ERROR_CLIENT = 5;
  
  public static final int ERROR_INSUFFICIENT_PERMISSIONS = 9;
  
  public static final int ERROR_NETWORK = 2;
  
  public static final int ERROR_NETWORK_TIMEOUT = 1;
  
  public static final int ERROR_NO_MATCH = 7;
  
  public static final int ERROR_RECOGNIZER_BUSY = 8;
  
  public static final int ERROR_SERVER = 4;
  
  public static final int ERROR_SPEECH_TIMEOUT = 6;
  
  private static final int MSG_CANCEL = 3;
  
  private static final int MSG_CHANGE_LISTENER = 4;
  
  private static final int MSG_START = 1;
  
  private static final int MSG_STOP = 2;
  
  public static final String RESULTS_RECOGNITION = "results_recognition";
  
  private static final String TAG = "SpeechRecognizer";
  
  private Connection mConnection;
  
  private final Context mContext;
  
  private IRecognitionService mService;
  
  private final ComponentName mServiceComponent;
  
  private SpeechRecognizer(Context paramContext, ComponentName paramComponentName) {
    this.mContext = paramContext;
    this.mServiceComponent = paramComponentName;
  }
  
  class Connection implements ServiceConnection {
    final SpeechRecognizer this$0;
    
    private Connection() {}
    
    public void onServiceConnected(ComponentName param1ComponentName, IBinder param1IBinder) {
      SpeechRecognizer.access$502(SpeechRecognizer.this, IRecognitionService.Stub.asInterface(param1IBinder));
      while (!SpeechRecognizer.this.mPendingTasks.isEmpty())
        SpeechRecognizer.this.mHandler.sendMessage(SpeechRecognizer.this.mPendingTasks.poll()); 
    }
    
    public void onServiceDisconnected(ComponentName param1ComponentName) {
      SpeechRecognizer.access$502(SpeechRecognizer.this, null);
      SpeechRecognizer.access$802(SpeechRecognizer.this, null);
      SpeechRecognizer.this.mPendingTasks.clear();
    }
  }
  
  public static boolean isRecognitionAvailable(Context paramContext) {
    PackageManager packageManager = paramContext.getPackageManager();
    Intent intent = new Intent("android.speech.RecognitionService");
    boolean bool1 = false;
    List<ResolveInfo> list = packageManager.queryIntentServices(intent, 0);
    boolean bool2 = bool1;
    if (list != null) {
      bool2 = bool1;
      if (list.size() != 0)
        bool2 = true; 
    } 
    return bool2;
  }
  
  public static SpeechRecognizer createSpeechRecognizer(Context paramContext) {
    return createSpeechRecognizer(paramContext, null);
  }
  
  public static SpeechRecognizer createSpeechRecognizer(Context paramContext, ComponentName paramComponentName) {
    if (paramContext != null) {
      checkIsCalledFromMainThread();
      return new SpeechRecognizer(paramContext, paramComponentName);
    } 
    throw new IllegalArgumentException("Context cannot be null)");
  }
  
  public void setRecognitionListener(RecognitionListener paramRecognitionListener) {
    checkIsCalledFromMainThread();
    putMessage(Message.obtain(this.mHandler, 4, paramRecognitionListener));
  }
  
  public void startListening(Intent paramIntent) {
    SeempLog.record(72);
    if (paramIntent != null) {
      checkIsCalledFromMainThread();
      if (this.mConnection == null) {
        String str;
        this.mConnection = new Connection();
        Intent intent = new Intent("android.speech.RecognitionService");
        ComponentName componentName = this.mServiceComponent;
        if (componentName == null) {
          str = Settings.Secure.getString(this.mContext.getContentResolver(), "voice_recognition_service");
          if (TextUtils.isEmpty(str)) {
            Log.e("SpeechRecognizer", "no selected voice recognition service");
            this.mListener.onError(5);
            return;
          } 
          intent.setComponent(ComponentName.unflattenFromString(str));
        } else {
          intent.setComponent((ComponentName)str);
        } 
        if (!this.mContext.bindService(intent, this.mConnection, 4097)) {
          Log.e("SpeechRecognizer", "bind to recognition service failed");
          this.mConnection = null;
          this.mService = null;
          this.mListener.onError(5);
          return;
        } 
      } 
      putMessage(Message.obtain(this.mHandler, 1, paramIntent));
      return;
    } 
    throw new IllegalArgumentException("intent must not be null");
  }
  
  public void stopListening() {
    checkIsCalledFromMainThread();
    putMessage(Message.obtain(this.mHandler, 2));
  }
  
  public void cancel() {
    checkIsCalledFromMainThread();
    putMessage(Message.obtain(this.mHandler, 3));
  }
  
  private static void checkIsCalledFromMainThread() {
    if (Looper.myLooper() == Looper.getMainLooper())
      return; 
    throw new RuntimeException("SpeechRecognizer should be used only from the application's main thread");
  }
  
  private void putMessage(Message paramMessage) {
    if (this.mService == null) {
      this.mPendingTasks.offer(paramMessage);
    } else {
      this.mHandler.sendMessage(paramMessage);
    } 
  }
  
  private void handleStartListening(Intent paramIntent) {
    if (!checkOpenConnection())
      return; 
    try {
      IRecognitionService iRecognitionService = this.mService;
      InternalListener internalListener = this.mListener;
      String str1 = this.mContext.getOpPackageName();
      Context context = this.mContext;
      String str2 = context.getAttributionTag();
      iRecognitionService.startListening(paramIntent, internalListener, str1, str2);
    } catch (RemoteException remoteException) {
      Log.e("SpeechRecognizer", "startListening() failed", (Throwable)remoteException);
      this.mListener.onError(5);
    } 
  }
  
  private void handleStopMessage() {
    if (!checkOpenConnection())
      return; 
    try {
      IRecognitionService iRecognitionService = this.mService;
      InternalListener internalListener = this.mListener;
      String str1 = this.mContext.getOpPackageName();
      Context context = this.mContext;
      String str2 = context.getAttributionTag();
      iRecognitionService.stopListening(internalListener, str1, str2);
    } catch (RemoteException remoteException) {
      Log.e("SpeechRecognizer", "stopListening() failed", (Throwable)remoteException);
      this.mListener.onError(5);
    } 
  }
  
  private void handleCancelMessage() {
    if (!checkOpenConnection())
      return; 
    try {
      this.mService.cancel(this.mListener, this.mContext.getOpPackageName(), this.mContext.getAttributionTag());
    } catch (RemoteException remoteException) {
      Log.e("SpeechRecognizer", "cancel() failed", (Throwable)remoteException);
      this.mListener.onError(5);
    } 
  }
  
  private boolean checkOpenConnection() {
    if (this.mService != null)
      return true; 
    this.mListener.onError(5);
    Log.e("SpeechRecognizer", "not connected to the recognition service");
    return false;
  }
  
  private void handleChangeListener(RecognitionListener paramRecognitionListener) {
    InternalListener.access$1002(this.mListener, paramRecognitionListener);
  }
  
  public void destroy() {
    IRecognitionService iRecognitionService = this.mService;
    if (iRecognitionService != null)
      try {
        InternalListener internalListener = this.mListener;
        String str1 = this.mContext.getOpPackageName();
        Context context = this.mContext;
        String str2 = context.getAttributionTag();
        iRecognitionService.cancel(internalListener, str1, str2);
      } catch (RemoteException remoteException) {} 
    Connection connection = this.mConnection;
    if (connection != null)
      this.mContext.unbindService(connection); 
    this.mPendingTasks.clear();
    this.mService = null;
    this.mConnection = null;
    InternalListener.access$1002(this.mListener, null);
  }
  
  class InternalListener extends IRecognitionListener.Stub {
    private static final int MSG_BEGINNING_OF_SPEECH = 1;
    
    private static final int MSG_BUFFER_RECEIVED = 2;
    
    private static final int MSG_END_OF_SPEECH = 3;
    
    private static final int MSG_ERROR = 4;
    
    private static final int MSG_ON_EVENT = 9;
    
    private static final int MSG_PARTIAL_RESULTS = 7;
    
    private static final int MSG_READY_FOR_SPEECH = 5;
    
    private static final int MSG_RESULTS = 6;
    
    private static final int MSG_RMS_CHANGED = 8;
    
    private final Handler mInternalHandler;
    
    private RecognitionListener mInternalListener;
    
    private InternalListener() {
      this.mInternalHandler = (Handler)new Object(this);
    }
    
    public void onBeginningOfSpeech() {
      Message.obtain(this.mInternalHandler, 1).sendToTarget();
    }
    
    public void onBufferReceived(byte[] param1ArrayOfbyte) {
      Message.obtain(this.mInternalHandler, 2, param1ArrayOfbyte).sendToTarget();
    }
    
    public void onEndOfSpeech() {
      Message.obtain(this.mInternalHandler, 3).sendToTarget();
    }
    
    public void onError(int param1Int) {
      Message.obtain(this.mInternalHandler, 4, Integer.valueOf(param1Int)).sendToTarget();
    }
    
    public void onReadyForSpeech(Bundle param1Bundle) {
      Message.obtain(this.mInternalHandler, 5, param1Bundle).sendToTarget();
    }
    
    public void onResults(Bundle param1Bundle) {
      Message.obtain(this.mInternalHandler, 6, param1Bundle).sendToTarget();
    }
    
    public void onPartialResults(Bundle param1Bundle) {
      Message.obtain(this.mInternalHandler, 7, param1Bundle).sendToTarget();
    }
    
    public void onRmsChanged(float param1Float) {
      Message.obtain(this.mInternalHandler, 8, Float.valueOf(param1Float)).sendToTarget();
    }
    
    public void onEvent(int param1Int, Bundle param1Bundle) {
      Message message = Message.obtain(this.mInternalHandler, 9, param1Int, param1Int, param1Bundle);
      message.sendToTarget();
    }
  }
}
