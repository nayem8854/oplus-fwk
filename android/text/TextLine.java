package android.text;

import android.common.OplusFeatureCache;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.text.style.CharacterStyle;
import android.text.style.MetricAffectingSpan;
import android.text.style.ReplacementSpan;
import com.android.internal.util.ArrayUtils;
import java.util.ArrayList;

public class TextLine {
  private final TextPaint mWorkPaint = new TextPaint();
  
  private final TextPaint mActivePaint = new TextPaint();
  
  private final SpanSet<MetricAffectingSpan> mMetricAffectingSpanSpanSet = new SpanSet<>(MetricAffectingSpan.class);
  
  private final SpanSet<CharacterStyle> mCharacterStyleSpanSet = new SpanSet<>(CharacterStyle.class);
  
  private final SpanSet<ReplacementSpan> mReplacementSpanSpanSet = new SpanSet<>(ReplacementSpan.class);
  
  private final DecorationInfo mDecorationInfo = new DecorationInfo();
  
  private final ArrayList<DecorationInfo> mDecorations = new ArrayList<>();
  
  private static final TextLine[] sCached = new TextLine[3];
  
  private static final boolean DEBUG = false;
  
  private static final char TAB_CHAR = '\t';
  
  private static final int TAB_INCREMENT = 20;
  
  private float mAddedWidthForJustify;
  
  private char[] mChars;
  
  private boolean mCharsValid;
  
  private PrecomputedText mComputed;
  
  private int mDir;
  
  private Layout.Directions mDirections;
  
  private int mEllipsisEnd;
  
  private int mEllipsisStart;
  
  private boolean mHasTabs;
  
  private boolean mIsJustifying;
  
  private int mLen;
  
  private TextPaint mPaint;
  
  private Spanned mSpanned;
  
  private int mStart;
  
  private Layout.TabStops mTabs;
  
  private CharSequence mText;
  
  public static TextLine obtain() {
    TextLine[] arrayOfTextLine;
    TextLine textLine;
    synchronized (sCached) {
      int i = sCached.length;
      while (true) {
        int j = i - 1;
        if (j >= 0) {
          i = j;
          if (sCached[j] != null) {
            TextLine textLine1 = sCached[j];
            sCached[j] = null;
            return textLine1;
          } 
          continue;
        } 
        break;
      } 
      textLine = new TextLine();
      return textLine;
    } 
  }
  
  public static TextLine recycle(TextLine paramTextLine) {
    // Byte code:
    //   0: aload_0
    //   1: aconst_null
    //   2: putfield mText : Ljava/lang/CharSequence;
    //   5: aload_0
    //   6: aconst_null
    //   7: putfield mPaint : Landroid/text/TextPaint;
    //   10: aload_0
    //   11: aconst_null
    //   12: putfield mDirections : Landroid/text/Layout$Directions;
    //   15: aload_0
    //   16: aconst_null
    //   17: putfield mSpanned : Landroid/text/Spanned;
    //   20: aload_0
    //   21: aconst_null
    //   22: putfield mTabs : Landroid/text/Layout$TabStops;
    //   25: aload_0
    //   26: aconst_null
    //   27: putfield mChars : [C
    //   30: aload_0
    //   31: aconst_null
    //   32: putfield mComputed : Landroid/text/PrecomputedText;
    //   35: aload_0
    //   36: getfield mMetricAffectingSpanSpanSet : Landroid/text/SpanSet;
    //   39: invokevirtual recycle : ()V
    //   42: aload_0
    //   43: getfield mCharacterStyleSpanSet : Landroid/text/SpanSet;
    //   46: invokevirtual recycle : ()V
    //   49: aload_0
    //   50: getfield mReplacementSpanSpanSet : Landroid/text/SpanSet;
    //   53: invokevirtual recycle : ()V
    //   56: getstatic android/text/TextLine.sCached : [Landroid/text/TextLine;
    //   59: astore_1
    //   60: aload_1
    //   61: monitorenter
    //   62: iconst_0
    //   63: istore_2
    //   64: iload_2
    //   65: getstatic android/text/TextLine.sCached : [Landroid/text/TextLine;
    //   68: arraylength
    //   69: if_icmpge -> 95
    //   72: getstatic android/text/TextLine.sCached : [Landroid/text/TextLine;
    //   75: iload_2
    //   76: aaload
    //   77: ifnonnull -> 89
    //   80: getstatic android/text/TextLine.sCached : [Landroid/text/TextLine;
    //   83: iload_2
    //   84: aload_0
    //   85: aastore
    //   86: goto -> 95
    //   89: iinc #2, 1
    //   92: goto -> 64
    //   95: aload_1
    //   96: monitorexit
    //   97: aconst_null
    //   98: areturn
    //   99: astore_0
    //   100: aload_1
    //   101: monitorexit
    //   102: aload_0
    //   103: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #139	-> 0
    //   #140	-> 5
    //   #141	-> 10
    //   #142	-> 15
    //   #143	-> 20
    //   #144	-> 25
    //   #145	-> 30
    //   #147	-> 35
    //   #148	-> 42
    //   #149	-> 49
    //   #151	-> 56
    //   #152	-> 62
    //   #153	-> 72
    //   #154	-> 80
    //   #155	-> 86
    //   #152	-> 89
    //   #158	-> 95
    //   #159	-> 97
    //   #158	-> 99
    // Exception table:
    //   from	to	target	type
    //   64	72	99	finally
    //   72	80	99	finally
    //   80	86	99	finally
    //   95	97	99	finally
    //   100	102	99	finally
  }
  
  public void set(TextPaint paramTextPaint, CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3, Layout.Directions paramDirections, boolean paramBoolean, Layout.TabStops paramTabStops, int paramInt4, int paramInt5) {
    this.mPaint = paramTextPaint;
    this.mText = paramCharSequence;
    this.mStart = paramInt1;
    this.mLen = paramInt2 - paramInt1;
    this.mDir = paramInt3;
    this.mDirections = paramDirections;
    if (paramDirections != null) {
      this.mHasTabs = paramBoolean;
      this.mSpanned = null;
      paramBoolean = false;
      if (paramCharSequence instanceof Spanned) {
        Spanned spanned = (Spanned)paramCharSequence;
        this.mReplacementSpanSpanSet.init(spanned, paramInt1, paramInt2);
        if (this.mReplacementSpanSpanSet.numberOfSpans > 0) {
          paramBoolean = true;
        } else {
          paramBoolean = false;
        } 
      } 
      this.mComputed = null;
      if (paramCharSequence instanceof PrecomputedText) {
        PrecomputedText precomputedText = (PrecomputedText)paramCharSequence;
        if (!precomputedText.getParams().getTextPaint().equalsForTextMeasurement(paramTextPaint))
          this.mComputed = null; 
      } 
      this.mCharsValid = paramBoolean;
      if (paramBoolean) {
        char[] arrayOfChar = this.mChars;
        if (arrayOfChar == null || arrayOfChar.length < this.mLen)
          this.mChars = ArrayUtils.newUnpaddedCharArray(this.mLen); 
        TextUtils.getChars(paramCharSequence, paramInt1, paramInt2, this.mChars, 0);
        if (paramBoolean) {
          arrayOfChar = this.mChars;
          for (paramInt3 = paramInt1; paramInt3 < paramInt2; paramInt3 = i) {
            int i = this.mReplacementSpanSpanSet.getNextTransition(paramInt3, paramInt2);
            if (this.mReplacementSpanSpanSet.hasSpansIntersecting(paramInt3, i) && (paramInt3 - paramInt1 >= paramInt5 || i - paramInt1 <= paramInt4)) {
              arrayOfChar[paramInt3 - paramInt1] = '￼';
              for (paramInt3 = paramInt3 - paramInt1 + 1; paramInt3 < i - paramInt1; paramInt3++)
                arrayOfChar[paramInt3] = '﻿'; 
            } 
          } 
        } 
      } 
      this.mTabs = paramTabStops;
      this.mAddedWidthForJustify = 0.0F;
      this.mIsJustifying = false;
      if (paramInt4 != paramInt5) {
        paramInt1 = paramInt4;
      } else {
        paramInt1 = 0;
      } 
      this.mEllipsisStart = paramInt1;
      if (paramInt4 == paramInt5)
        paramInt5 = 0; 
      this.mEllipsisEnd = paramInt5;
      return;
    } 
    throw new IllegalArgumentException("Directions cannot be null");
  }
  
  private char charAt(int paramInt) {
    char c;
    if (this.mCharsValid) {
      c = this.mChars[paramInt];
    } else {
      c = this.mText.charAt(this.mStart + paramInt);
    } 
    return c;
  }
  
  public void justify(float paramFloat) {
    int i = this.mLen;
    while (i > 0 && isLineEndSpace(this.mText.charAt(this.mStart + i - 1)))
      i--; 
    int j = countStretchableSpaces(0, i);
    float f1 = Math.abs(measure(i, false, null));
    float f2 = ((ITextJustificationHooks)OplusFeatureCache.getOrCreate(ITextJustificationHooks.DEFAULT, new Object[0])).calculateAddedWidth(paramFloat, f1, j, 0, i, this.mCharsValid, this.mChars, this.mText, this.mStart);
    if (f2 > 0.0F) {
      this.mAddedWidthForJustify = f2;
      this.mIsJustifying = true;
      return;
    } 
    if (j == 0)
      return; 
    this.mAddedWidthForJustify = (paramFloat - f1) / j;
    this.mIsJustifying = true;
  }
  
  void draw(Canvas paramCanvas, float paramFloat, int paramInt1, int paramInt2, int paramInt3) {
    float f = 0.0F;
    int i = this.mDirections.getRunCount();
    for (byte b = 0; b < i; b++) {
      int m, j = this.mDirections.getRunStart(b);
      if (j > this.mLen)
        break; 
      int k = Math.min(this.mDirections.getRunLength(b) + j, this.mLen);
      boolean bool = this.mDirections.isRunRtl(b);
      if (this.mHasTabs) {
        m = j;
      } else {
        m = k;
      } 
      for (; m <= k; m++) {
        if (m == k || charAt(m) == '\t') {
          boolean bool1;
          if (b != i - 1 || m != this.mLen) {
            bool1 = true;
          } else {
            bool1 = false;
          } 
          float f1 = f + drawRun(paramCanvas, j, m, bool, paramFloat + f, paramInt1, paramInt2, paramInt3, bool1);
          f = f1;
          if (m != k) {
            j = this.mDir;
            f = j * nextTab(j * f1);
          } 
          j = m + 1;
        } 
      } 
    } 
  }
  
  public float metrics(Paint.FontMetricsInt paramFontMetricsInt) {
    return measure(this.mLen, false, paramFontMetricsInt);
  }
  
  public float measure(int paramInt, boolean paramBoolean, Paint.FontMetricsInt paramFontMetricsInt) {
    if (paramInt <= this.mLen) {
      int i;
      Object object;
      if (paramBoolean) {
        i = paramInt - 1;
      } else {
        i = paramInt;
      } 
      if (i < 0)
        return 0.0F; 
      float f = 0.0F;
      for (byte b = 0; b < this.mDirections.getRunCount(); b++) {
        int m, j = this.mDirections.getRunStart(b);
        if (j > this.mLen)
          break; 
        int k = Math.min(this.mDirections.getRunLength(b) + j, this.mLen);
        boolean bool = this.mDirections.isRunRtl(b);
        if (this.mHasTabs) {
          m = j;
        } else {
          m = k;
        } 
        while (true) {
          m++;
          object = SYNTHETIC_LOCAL_VARIABLE_11;
          object1 = SYNTHETIC_LOCAL_VARIABLE_12;
        } 
        continue;
      } 
      return object;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("offset(");
    stringBuilder.append(paramInt);
    stringBuilder.append(") should be less than line limit(");
    stringBuilder.append(this.mLen);
    stringBuilder.append(")");
    throw new IndexOutOfBoundsException(stringBuilder.toString());
  }
  
  public float[] measureAllOffsets(boolean[] paramArrayOfboolean, Paint.FontMetricsInt paramFontMetricsInt) {
    int i = this.mLen;
    float[] arrayOfFloat = new float[i + 1];
    int[] arrayOfInt = new int[i + 1];
    for (i = 0; i < arrayOfInt.length; i++) {
      int j;
      if (paramArrayOfboolean[i]) {
        j = i - 1;
      } else {
        j = i;
      } 
      arrayOfInt[i] = j;
    } 
    if (arrayOfInt[0] < 0)
      arrayOfFloat[0] = 0.0F; 
    float f = 0.0F;
    for (byte b = 0; b < this.mDirections.getRunCount(); b++) {
      int j = this.mDirections.getRunStart(b);
      if (j > this.mLen)
        break; 
      int k = Math.min(this.mDirections.getRunLength(b) + j, this.mLen);
      boolean bool = this.mDirections.isRunRtl(b);
      if (this.mHasTabs) {
        i = j;
      } else {
        i = k;
      } 
      for (int m = i; m <= k; m++) {
        if (m == k || charAt(m) == '\t') {
          boolean bool1;
          boolean bool2;
          if (this.mDir == -1) {
            bool1 = true;
          } else {
            bool1 = false;
          } 
          if (bool1 == bool) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          j = i;
          float f1 = measureRun(i, m, m, bool, paramFontMetricsInt);
          if (bool2) {
            f2 = f1;
          } else {
            f2 = -f1;
          } 
          float f2 = f + f2;
          if (!bool2)
            f = f2; 
          if (bool2) {
            Paint.FontMetricsInt fontMetricsInt = paramFontMetricsInt;
          } else {
            paramArrayOfboolean = null;
          } 
          for (i = j; i <= m && i <= this.mLen; i++) {
            if (arrayOfInt[i] >= j && arrayOfInt[i] < m)
              arrayOfFloat[i] = f + measureRun(j, i, m, bool, (Paint.FontMetricsInt)paramArrayOfboolean); 
          } 
          f = f2;
          if (m != k) {
            if (arrayOfInt[m] == m)
              arrayOfFloat[m] = f2; 
            i = this.mDir;
            f = i * nextTab(i * f2);
            if (arrayOfInt[m + 1] == m)
              arrayOfFloat[m + 1] = f; 
          } 
          i = m + 1;
        } 
      } 
    } 
    i = this.mLen;
    if (arrayOfInt[i] == i)
      arrayOfFloat[i] = f; 
    return arrayOfFloat;
  }
  
  private float drawRun(Canvas paramCanvas, int paramInt1, int paramInt2, boolean paramBoolean1, float paramFloat, int paramInt3, int paramInt4, int paramInt5, boolean paramBoolean2) {
    int i = this.mDir;
    boolean bool = true;
    if (i != 1)
      bool = false; 
    if (bool == paramBoolean1) {
      float f = -measureRun(paramInt1, paramInt2, paramInt2, paramBoolean1, null);
      handleRun(paramInt1, paramInt2, paramInt2, paramBoolean1, paramCanvas, paramFloat + f, paramInt3, paramInt4, paramInt5, null, false);
      return f;
    } 
    return handleRun(paramInt1, paramInt2, paramInt2, paramBoolean1, paramCanvas, paramFloat, paramInt3, paramInt4, paramInt5, null, paramBoolean2);
  }
  
  private float measureRun(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean, Paint.FontMetricsInt paramFontMetricsInt) {
    return handleRun(paramInt1, paramInt2, paramInt3, paramBoolean, null, 0.0F, 0, 0, 0, paramFontMetricsInt, true);
  }
  
  int getOffsetToLeftRightOf(int paramInt, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mLen : I
    //   4: istore_3
    //   5: aload_0
    //   6: getfield mDir : I
    //   9: iconst_m1
    //   10: if_icmpne -> 19
    //   13: iconst_1
    //   14: istore #4
    //   16: goto -> 22
    //   19: iconst_0
    //   20: istore #4
    //   22: aload_0
    //   23: getfield mDirections : Landroid/text/Layout$Directions;
    //   26: getfield mDirections : [I
    //   29: astore #5
    //   31: iconst_0
    //   32: istore #6
    //   34: iload_3
    //   35: istore #7
    //   37: iconst_0
    //   38: istore #8
    //   40: iload_1
    //   41: ifne -> 56
    //   44: iconst_0
    //   45: istore #9
    //   47: bipush #-2
    //   49: istore #10
    //   51: iconst_m1
    //   52: istore_1
    //   53: goto -> 440
    //   56: iload_1
    //   57: iload_3
    //   58: if_icmpne -> 74
    //   61: aload #5
    //   63: arraylength
    //   64: istore #10
    //   66: iconst_0
    //   67: istore #9
    //   69: iconst_m1
    //   70: istore_1
    //   71: goto -> 440
    //   74: iconst_0
    //   75: istore #10
    //   77: iload #10
    //   79: aload #5
    //   81: arraylength
    //   82: if_icmpge -> 311
    //   85: iconst_0
    //   86: aload #5
    //   88: iload #10
    //   90: iaload
    //   91: iadd
    //   92: istore #6
    //   94: iload_1
    //   95: iload #6
    //   97: if_icmplt -> 305
    //   100: aload #5
    //   102: iload #10
    //   104: iconst_1
    //   105: iadd
    //   106: iaload
    //   107: ldc_w 67108863
    //   110: iand
    //   111: iload #6
    //   113: iadd
    //   114: istore #9
    //   116: iload #9
    //   118: istore #7
    //   120: iload #9
    //   122: iload_3
    //   123: if_icmple -> 129
    //   126: iload_3
    //   127: istore #7
    //   129: iload_1
    //   130: iload #7
    //   132: if_icmpge -> 302
    //   135: aload #5
    //   137: iload #10
    //   139: iconst_1
    //   140: iadd
    //   141: iaload
    //   142: bipush #26
    //   144: iushr
    //   145: bipush #63
    //   147: iand
    //   148: istore #11
    //   150: iload_1
    //   151: iload #6
    //   153: if_icmpne -> 292
    //   156: iload_1
    //   157: iconst_1
    //   158: isub
    //   159: istore #12
    //   161: iconst_0
    //   162: istore #13
    //   164: iload #13
    //   166: aload #5
    //   168: arraylength
    //   169: if_icmpge -> 285
    //   172: aload #5
    //   174: iload #13
    //   176: iaload
    //   177: iconst_0
    //   178: iadd
    //   179: istore #14
    //   181: iload #12
    //   183: iload #14
    //   185: if_icmplt -> 279
    //   188: iload #14
    //   190: aload #5
    //   192: iload #13
    //   194: iconst_1
    //   195: iadd
    //   196: iaload
    //   197: ldc_w 67108863
    //   200: iand
    //   201: iadd
    //   202: istore #15
    //   204: iload #15
    //   206: istore #9
    //   208: iload #15
    //   210: iload_3
    //   211: if_icmple -> 217
    //   214: iload_3
    //   215: istore #9
    //   217: iload #12
    //   219: iload #9
    //   221: if_icmpge -> 276
    //   224: aload #5
    //   226: iload #13
    //   228: iconst_1
    //   229: iadd
    //   230: iaload
    //   231: bipush #26
    //   233: iushr
    //   234: bipush #63
    //   236: iand
    //   237: istore #15
    //   239: iload #15
    //   241: iload #11
    //   243: if_icmpge -> 279
    //   246: iload #13
    //   248: istore #10
    //   250: iload #15
    //   252: istore #6
    //   254: iload #9
    //   256: istore #7
    //   258: iconst_1
    //   259: istore #8
    //   261: iload #14
    //   263: istore #13
    //   265: iload #6
    //   267: istore #9
    //   269: iload #13
    //   271: istore #6
    //   273: goto -> 289
    //   276: goto -> 279
    //   279: iinc #13, 2
    //   282: goto -> 164
    //   285: iload #11
    //   287: istore #9
    //   289: goto -> 317
    //   292: iload #11
    //   294: istore #9
    //   296: iconst_0
    //   297: istore #8
    //   299: goto -> 317
    //   302: goto -> 305
    //   305: iinc #10, 2
    //   308: goto -> 77
    //   311: iconst_0
    //   312: istore #9
    //   314: iconst_0
    //   315: istore #8
    //   317: iload #10
    //   319: aload #5
    //   321: arraylength
    //   322: if_icmpeq -> 438
    //   325: iload #9
    //   327: iconst_1
    //   328: iand
    //   329: ifeq -> 338
    //   332: iconst_1
    //   333: istore #16
    //   335: goto -> 341
    //   338: iconst_0
    //   339: istore #16
    //   341: iload_2
    //   342: iload #16
    //   344: if_icmpne -> 353
    //   347: iconst_1
    //   348: istore #17
    //   350: goto -> 356
    //   353: iconst_0
    //   354: istore #17
    //   356: iload #17
    //   358: ifeq -> 368
    //   361: iload #7
    //   363: istore #13
    //   365: goto -> 372
    //   368: iload #6
    //   370: istore #13
    //   372: iload_1
    //   373: iload #13
    //   375: if_icmpne -> 391
    //   378: iload #17
    //   380: iload #8
    //   382: if_icmpeq -> 388
    //   385: goto -> 391
    //   388: goto -> 438
    //   391: iload #10
    //   393: istore #13
    //   395: aload_0
    //   396: iload #10
    //   398: iload #6
    //   400: iload #7
    //   402: iload #16
    //   404: iload_1
    //   405: iload #17
    //   407: invokespecial getOffsetBeforeAfter : (IIIZIZ)I
    //   410: istore_1
    //   411: iload #17
    //   413: ifeq -> 419
    //   416: goto -> 423
    //   419: iload #6
    //   421: istore #7
    //   423: iload_1
    //   424: iload #7
    //   426: if_icmpeq -> 431
    //   429: iload_1
    //   430: ireturn
    //   431: iload #13
    //   433: istore #10
    //   435: goto -> 440
    //   438: iconst_m1
    //   439: istore_1
    //   440: iload_2
    //   441: iload #4
    //   443: if_icmpne -> 452
    //   446: iconst_1
    //   447: istore #7
    //   449: goto -> 455
    //   452: iconst_0
    //   453: istore #7
    //   455: iload #7
    //   457: ifeq -> 466
    //   460: iconst_2
    //   461: istore #6
    //   463: goto -> 470
    //   466: bipush #-2
    //   468: istore #6
    //   470: iload #10
    //   472: iload #6
    //   474: iadd
    //   475: istore #13
    //   477: iload #13
    //   479: iflt -> 669
    //   482: iload #13
    //   484: aload #5
    //   486: arraylength
    //   487: if_icmpge -> 669
    //   490: iconst_0
    //   491: aload #5
    //   493: iload #13
    //   495: iaload
    //   496: iadd
    //   497: istore #7
    //   499: iload #7
    //   501: aload #5
    //   503: iload #13
    //   505: iconst_1
    //   506: iadd
    //   507: iaload
    //   508: ldc_w 67108863
    //   511: iand
    //   512: iadd
    //   513: istore #10
    //   515: iload #10
    //   517: iload_3
    //   518: if_icmple -> 527
    //   521: iload_3
    //   522: istore #10
    //   524: goto -> 527
    //   527: aload #5
    //   529: iload #13
    //   531: iconst_1
    //   532: iadd
    //   533: iaload
    //   534: bipush #26
    //   536: iushr
    //   537: bipush #63
    //   539: iand
    //   540: istore #11
    //   542: iload #11
    //   544: iconst_1
    //   545: iand
    //   546: ifeq -> 555
    //   549: iconst_1
    //   550: istore #8
    //   552: goto -> 558
    //   555: iconst_0
    //   556: istore #8
    //   558: iload_2
    //   559: iload #8
    //   561: if_icmpne -> 570
    //   564: iconst_1
    //   565: istore #16
    //   567: goto -> 573
    //   570: iconst_0
    //   571: istore #16
    //   573: iload_1
    //   574: iconst_m1
    //   575: if_icmpne -> 645
    //   578: iload #16
    //   580: ifeq -> 589
    //   583: iload #7
    //   585: istore_1
    //   586: goto -> 592
    //   589: iload #10
    //   591: istore_1
    //   592: aload_0
    //   593: iload #13
    //   595: iload #7
    //   597: iload #10
    //   599: iload #8
    //   601: iload_1
    //   602: iload #16
    //   604: invokespecial getOffsetBeforeAfter : (IIIZIZ)I
    //   607: istore #6
    //   609: iload #16
    //   611: ifeq -> 617
    //   614: goto -> 621
    //   617: iload #7
    //   619: istore #10
    //   621: iload #6
    //   623: istore_1
    //   624: iload #6
    //   626: iload #10
    //   628: if_icmpne -> 715
    //   631: iload #13
    //   633: istore #10
    //   635: iload #11
    //   637: istore #9
    //   639: iload #6
    //   641: istore_1
    //   642: goto -> 440
    //   645: iload #11
    //   647: iload #9
    //   649: if_icmpge -> 715
    //   652: iload #16
    //   654: ifeq -> 663
    //   657: iload #7
    //   659: istore_1
    //   660: goto -> 666
    //   663: iload #10
    //   665: istore_1
    //   666: goto -> 715
    //   669: iconst_m1
    //   670: istore #10
    //   672: iload_1
    //   673: iconst_m1
    //   674: if_icmpne -> 695
    //   677: iload #10
    //   679: istore_1
    //   680: iload #7
    //   682: ifeq -> 692
    //   685: aload_0
    //   686: getfield mLen : I
    //   689: iconst_1
    //   690: iadd
    //   691: istore_1
    //   692: goto -> 715
    //   695: iload_1
    //   696: iload_3
    //   697: if_icmpgt -> 715
    //   700: iload #7
    //   702: ifeq -> 710
    //   705: iload_3
    //   706: istore_1
    //   707: goto -> 712
    //   710: iconst_0
    //   711: istore_1
    //   712: goto -> 715
    //   715: iload_1
    //   716: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #562	-> 0
    //   #563	-> 0
    //   #564	-> 5
    //   #565	-> 22
    //   #567	-> 31
    //   #568	-> 37
    //   #570	-> 40
    //   #571	-> 44
    //   #572	-> 56
    //   #573	-> 61
    //   #577	-> 74
    //   #578	-> 85
    //   #579	-> 94
    //   #580	-> 100
    //   #581	-> 116
    //   #582	-> 126
    //   #584	-> 129
    //   #585	-> 135
    //   #587	-> 150
    //   #592	-> 156
    //   #593	-> 161
    //   #594	-> 172
    //   #595	-> 181
    //   #596	-> 188
    //   #598	-> 204
    //   #599	-> 214
    //   #601	-> 217
    //   #602	-> 224
    //   #604	-> 239
    //   #606	-> 246
    //   #607	-> 250
    //   #608	-> 254
    //   #609	-> 254
    //   #610	-> 258
    //   #611	-> 261
    //   #601	-> 276
    //   #595	-> 279
    //   #593	-> 279
    //   #616	-> 289
    //   #587	-> 292
    //   #584	-> 302
    //   #579	-> 305
    //   #577	-> 305
    //   #627	-> 317
    //   #628	-> 325
    //   #629	-> 341
    //   #630	-> 356
    //   #632	-> 391
    //   #636	-> 411
    //   #637	-> 429
    //   #636	-> 431
    //   #627	-> 438
    //   #649	-> 438
    //   #650	-> 455
    //   #651	-> 477
    //   #652	-> 490
    //   #653	-> 499
    //   #655	-> 515
    //   #656	-> 521
    //   #655	-> 527
    //   #658	-> 527
    //   #660	-> 542
    //   #662	-> 558
    //   #663	-> 573
    //   #664	-> 578
    //   #666	-> 578
    //   #664	-> 592
    //   #667	-> 609
    //   #670	-> 631
    //   #671	-> 635
    //   #672	-> 639
    //   #678	-> 645
    //   #680	-> 652
    //   #651	-> 669
    //   #685	-> 669
    //   #689	-> 677
    //   #690	-> 692
    //   #701	-> 695
    //   #702	-> 700
    //   #707	-> 715
  }
  
  private int getOffsetBeforeAfter(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean1, int paramInt4, boolean paramBoolean2) {
    // Byte code:
    //   0: iload_1
    //   1: iflt -> 336
    //   4: iconst_0
    //   5: istore #7
    //   7: iload #6
    //   9: ifeq -> 20
    //   12: aload_0
    //   13: getfield mLen : I
    //   16: istore_1
    //   17: goto -> 22
    //   20: iconst_0
    //   21: istore_1
    //   22: iload #5
    //   24: iload_1
    //   25: if_icmpne -> 31
    //   28: goto -> 336
    //   31: aload_0
    //   32: getfield mWorkPaint : Landroid/text/TextPaint;
    //   35: astore #8
    //   37: aload #8
    //   39: aload_0
    //   40: getfield mPaint : Landroid/text/TextPaint;
    //   43: invokevirtual set : (Landroid/text/TextPaint;)V
    //   46: aload_0
    //   47: getfield mIsJustifying : Z
    //   50: ifeq -> 62
    //   53: aload #8
    //   55: aload_0
    //   56: getfield mAddedWidthForJustify : F
    //   59: invokevirtual setWordSpacing : (F)V
    //   62: iload_2
    //   63: istore_1
    //   64: aload_0
    //   65: getfield mSpanned : Landroid/text/Spanned;
    //   68: ifnonnull -> 76
    //   71: iload_3
    //   72: istore_2
    //   73: goto -> 251
    //   76: iload #6
    //   78: ifeq -> 90
    //   81: iload #5
    //   83: iconst_1
    //   84: iadd
    //   85: istore #9
    //   87: goto -> 94
    //   90: iload #5
    //   92: istore #9
    //   94: aload_0
    //   95: getfield mStart : I
    //   98: istore #10
    //   100: aload_0
    //   101: getfield mSpanned : Landroid/text/Spanned;
    //   104: aload_0
    //   105: getfield mStart : I
    //   108: iload_1
    //   109: iadd
    //   110: iload #10
    //   112: iload_3
    //   113: iadd
    //   114: ldc android/text/style/MetricAffectingSpan
    //   116: invokeinterface nextSpanTransition : (IILjava/lang/Class;)I
    //   121: istore_2
    //   122: aload_0
    //   123: getfield mStart : I
    //   126: istore #11
    //   128: iload_2
    //   129: iload #11
    //   131: isub
    //   132: istore_2
    //   133: iload_2
    //   134: iload #9
    //   136: if_icmplt -> 331
    //   139: aload_0
    //   140: getfield mSpanned : Landroid/text/Spanned;
    //   143: iload #11
    //   145: iload_1
    //   146: iadd
    //   147: iload #11
    //   149: iload_2
    //   150: iadd
    //   151: ldc android/text/style/MetricAffectingSpan
    //   153: invokeinterface getSpans : (IILjava/lang/Class;)[Ljava/lang/Object;
    //   158: checkcast [Landroid/text/style/MetricAffectingSpan;
    //   161: astore #12
    //   163: aload #12
    //   165: aload_0
    //   166: getfield mSpanned : Landroid/text/Spanned;
    //   169: ldc android/text/style/MetricAffectingSpan
    //   171: invokestatic removeEmptySpans : ([Ljava/lang/Object;Landroid/text/Spanned;Ljava/lang/Class;)[Ljava/lang/Object;
    //   174: checkcast [Landroid/text/style/MetricAffectingSpan;
    //   177: astore #13
    //   179: aload #13
    //   181: arraylength
    //   182: ifle -> 251
    //   185: aconst_null
    //   186: astore #12
    //   188: iconst_0
    //   189: istore_3
    //   190: iload_3
    //   191: aload #13
    //   193: arraylength
    //   194: if_icmpge -> 234
    //   197: aload #13
    //   199: iload_3
    //   200: aaload
    //   201: astore #14
    //   203: aload #14
    //   205: instanceof android/text/style/ReplacementSpan
    //   208: ifeq -> 221
    //   211: aload #14
    //   213: checkcast android/text/style/ReplacementSpan
    //   216: astore #12
    //   218: goto -> 228
    //   221: aload #14
    //   223: aload #8
    //   225: invokevirtual updateMeasureState : (Landroid/text/TextPaint;)V
    //   228: iinc #3, 1
    //   231: goto -> 190
    //   234: aload #12
    //   236: ifnull -> 251
    //   239: iload #6
    //   241: ifeq -> 249
    //   244: iload_2
    //   245: istore_1
    //   246: goto -> 249
    //   249: iload_1
    //   250: ireturn
    //   251: iload #6
    //   253: ifeq -> 262
    //   256: iload #7
    //   258: istore_3
    //   259: goto -> 264
    //   262: iconst_2
    //   263: istore_3
    //   264: aload_0
    //   265: getfield mCharsValid : Z
    //   268: ifeq -> 290
    //   271: aload #8
    //   273: aload_0
    //   274: getfield mChars : [C
    //   277: iload_1
    //   278: iload_2
    //   279: iload_1
    //   280: isub
    //   281: iload #4
    //   283: iload #5
    //   285: iload_3
    //   286: invokevirtual getTextRunCursor : ([CIIZII)I
    //   289: ireturn
    //   290: aload_0
    //   291: getfield mText : Ljava/lang/CharSequence;
    //   294: astore #12
    //   296: aload_0
    //   297: getfield mStart : I
    //   300: istore #9
    //   302: aload #8
    //   304: aload #12
    //   306: iload #9
    //   308: iload_1
    //   309: iadd
    //   310: iload #9
    //   312: iload_2
    //   313: iadd
    //   314: iload #4
    //   316: iload #9
    //   318: iload #5
    //   320: iadd
    //   321: iload_3
    //   322: invokevirtual getTextRunCursor : (Ljava/lang/CharSequence;IIZII)I
    //   325: aload_0
    //   326: getfield mStart : I
    //   329: isub
    //   330: ireturn
    //   331: iload_2
    //   332: istore_1
    //   333: goto -> 100
    //   336: iload #6
    //   338: ifeq -> 361
    //   341: aload_0
    //   342: getfield mText : Ljava/lang/CharSequence;
    //   345: aload_0
    //   346: getfield mStart : I
    //   349: iload #5
    //   351: iadd
    //   352: invokestatic getOffsetAfter : (Ljava/lang/CharSequence;I)I
    //   355: aload_0
    //   356: getfield mStart : I
    //   359: isub
    //   360: ireturn
    //   361: aload_0
    //   362: getfield mText : Ljava/lang/CharSequence;
    //   365: aload_0
    //   366: getfield mStart : I
    //   369: iload #5
    //   371: iadd
    //   372: invokestatic getOffsetBefore : (Ljava/lang/CharSequence;I)I
    //   375: aload_0
    //   376: getfield mStart : I
    //   379: isub
    //   380: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #730	-> 0
    //   #740	-> 31
    //   #741	-> 37
    //   #742	-> 46
    //   #743	-> 53
    //   #746	-> 62
    //   #748	-> 64
    //   #749	-> 71
    //   #751	-> 76
    //   #752	-> 94
    //   #754	-> 100
    //   #756	-> 133
    //   #757	-> 139
    //   #762	-> 139
    //   #764	-> 163
    //   #766	-> 179
    //   #767	-> 185
    //   #768	-> 188
    //   #769	-> 197
    //   #770	-> 203
    //   #771	-> 211
    //   #773	-> 221
    //   #768	-> 228
    //   #777	-> 234
    //   #780	-> 239
    //   #785	-> 251
    //   #786	-> 264
    //   #787	-> 271
    //   #790	-> 290
    //   #759	-> 331
    //   #734	-> 336
    //   #735	-> 341
    //   #737	-> 361
  }
  
  private static void expandMetricsFromPaint(Paint.FontMetricsInt paramFontMetricsInt, TextPaint paramTextPaint) {
    int i = paramFontMetricsInt.top;
    int j = paramFontMetricsInt.ascent;
    int k = paramFontMetricsInt.descent;
    int m = paramFontMetricsInt.bottom;
    int n = paramFontMetricsInt.leading;
    paramTextPaint.getFontMetricsInt(paramFontMetricsInt);
    updateMetrics(paramFontMetricsInt, i, j, k, m, n);
  }
  
  static void updateMetrics(Paint.FontMetricsInt paramFontMetricsInt, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    paramFontMetricsInt.top = Math.min(paramFontMetricsInt.top, paramInt1);
    paramFontMetricsInt.ascent = Math.min(paramFontMetricsInt.ascent, paramInt2);
    paramFontMetricsInt.descent = Math.max(paramFontMetricsInt.descent, paramInt3);
    paramFontMetricsInt.bottom = Math.max(paramFontMetricsInt.bottom, paramInt4);
    paramFontMetricsInt.leading = Math.max(paramFontMetricsInt.leading, paramInt5);
  }
  
  private static void drawStroke(TextPaint paramTextPaint, Canvas paramCanvas, int paramInt, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5) {
    paramFloat1 = paramFloat5 + paramTextPaint.baselineShift + paramFloat1;
    int i = paramTextPaint.getColor();
    Paint.Style style = paramTextPaint.getStyle();
    boolean bool = paramTextPaint.isAntiAlias();
    paramTextPaint.setStyle(Paint.Style.FILL);
    paramTextPaint.setAntiAlias(true);
    paramTextPaint.setColor(paramInt);
    paramCanvas.drawRect(paramFloat3, paramFloat1, paramFloat4, paramFloat1 + paramFloat2, paramTextPaint);
    paramTextPaint.setStyle(style);
    paramTextPaint.setColor(i);
    paramTextPaint.setAntiAlias(bool);
  }
  
  private float getRunAdvance(TextPaint paramTextPaint, int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean, int paramInt5) {
    if (this.mCharsValid)
      return paramTextPaint.getRunAdvance(this.mChars, paramInt1, paramInt2, paramInt3, paramInt4, paramBoolean, paramInt5); 
    int i = this.mStart;
    PrecomputedText precomputedText = this.mComputed;
    if (precomputedText == null)
      return paramTextPaint.getRunAdvance(this.mText, i + paramInt1, i + paramInt2, i + paramInt3, i + paramInt4, paramBoolean, i + paramInt5); 
    return precomputedText.getWidth(paramInt1 + i, paramInt2 + i);
  }
  
  private float handleText(TextPaint paramTextPaint, int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean1, Canvas paramCanvas, float paramFloat, int paramInt5, int paramInt6, int paramInt7, Paint.FontMetricsInt paramFontMetricsInt, boolean paramBoolean2, int paramInt8, ArrayList<DecorationInfo> paramArrayList) {
    int i;
    if (this.mIsJustifying)
      paramTextPaint.setWordSpacing(this.mAddedWidthForJustify); 
    if (paramFontMetricsInt != null)
      expandMetricsFromPaint(paramFontMetricsInt, paramTextPaint); 
    if (paramInt2 == paramInt1)
      return 0.0F; 
    float f = 0.0F;
    if (paramArrayList == null) {
      i = 0;
    } else {
      i = paramArrayList.size();
    } 
    if (paramBoolean2 || (paramCanvas != null && (paramTextPaint.bgColor != 0 || i != 0 || paramBoolean1)))
      f = getRunAdvance(paramTextPaint, paramInt1, paramInt2, paramInt3, paramInt4, paramBoolean1, paramInt8); 
    if (paramCanvas != null) {
      float f1;
      if (paramBoolean1) {
        float f2 = paramFloat - f;
        f1 = paramFloat;
        paramFloat = f2;
      } else {
        float f2 = paramFloat;
        f1 = paramFloat + f;
        paramFloat = f2;
      } 
      if (paramTextPaint.bgColor != 0) {
        int j = paramTextPaint.getColor();
        Paint.Style style = paramTextPaint.getStyle();
        paramTextPaint.setColor(paramTextPaint.bgColor);
        paramTextPaint.setStyle(Paint.Style.FILL);
        paramCanvas.drawRect(paramFloat, paramInt5, f1, paramInt7, paramTextPaint);
        paramTextPaint.setStyle(style);
        paramTextPaint.setColor(j);
      } 
      drawTextRun(paramCanvas, paramTextPaint, paramInt1, paramInt2, paramInt3, paramInt4, paramBoolean1, paramFloat, paramInt6 + paramTextPaint.baselineShift);
      if (i != 0)
        for (paramInt7 = 0, paramInt5 = i; paramInt7 < paramInt5; paramInt7++) {
          float f4;
          DecorationInfo decorationInfo = paramArrayList.get(paramInt7);
          i = Math.max(decorationInfo.start, paramInt1);
          int j = Math.min(decorationInfo.end, paramInt8);
          float f2 = getRunAdvance(paramTextPaint, paramInt1, paramInt2, paramInt3, paramInt4, paramBoolean1, i);
          float f3 = getRunAdvance(paramTextPaint, paramInt1, paramInt2, paramInt3, paramInt4, paramBoolean1, j);
          if (paramBoolean1) {
            f4 = f1 - f3;
            f2 = f1 - f2;
          } else {
            f4 = paramFloat + f2;
            f2 = paramFloat + f3;
          } 
          if (decorationInfo.underlineColor != 0)
            drawStroke(paramTextPaint, paramCanvas, decorationInfo.underlineColor, paramTextPaint.getUnderlinePosition(), decorationInfo.underlineThickness, f4, f2, paramInt6); 
          if (decorationInfo.isUnderlineText) {
            f3 = Math.max(paramTextPaint.getUnderlineThickness(), 1.0F);
            drawStroke(paramTextPaint, paramCanvas, paramTextPaint.getColor(), paramTextPaint.getUnderlinePosition(), f3, f4, f2, paramInt6);
          } 
          if (decorationInfo.isStrikeThruText) {
            f3 = Math.max(paramTextPaint.getStrikeThruThickness(), 1.0F);
            drawStroke(paramTextPaint, paramCanvas, paramTextPaint.getColor(), paramTextPaint.getStrikeThruPosition(), f3, f4, f2, paramInt6);
          } 
        }  
    } 
    if (paramBoolean1)
      f = -f; 
    return f;
  }
  
  private float handleReplacement(ReplacementSpan paramReplacementSpan, TextPaint paramTextPaint, int paramInt1, int paramInt2, boolean paramBoolean1, Canvas paramCanvas, float paramFloat, int paramInt3, int paramInt4, int paramInt5, Paint.FontMetricsInt paramFontMetricsInt, boolean paramBoolean2) {
    // Byte code:
    //   0: fconst_0
    //   1: fstore #13
    //   3: aload_0
    //   4: getfield mStart : I
    //   7: istore #14
    //   9: iload #14
    //   11: iload_3
    //   12: iadd
    //   13: istore #15
    //   15: iload #14
    //   17: iload #4
    //   19: iadd
    //   20: istore #16
    //   22: iload #12
    //   24: ifne -> 51
    //   27: fload #13
    //   29: fstore #17
    //   31: aload #6
    //   33: ifnull -> 48
    //   36: fload #13
    //   38: fstore #17
    //   40: iload #5
    //   42: ifeq -> 48
    //   45: goto -> 51
    //   48: goto -> 176
    //   51: aload #11
    //   53: ifnull -> 61
    //   56: iconst_1
    //   57: istore_3
    //   58: goto -> 63
    //   61: iconst_0
    //   62: istore_3
    //   63: iload_3
    //   64: ifeq -> 113
    //   67: aload #11
    //   69: getfield top : I
    //   72: istore #4
    //   74: aload #11
    //   76: getfield ascent : I
    //   79: istore #18
    //   81: aload #11
    //   83: getfield descent : I
    //   86: istore #19
    //   88: aload #11
    //   90: getfield bottom : I
    //   93: istore #20
    //   95: aload #11
    //   97: getfield leading : I
    //   100: istore #14
    //   102: iload #19
    //   104: istore #21
    //   106: iload #20
    //   108: istore #19
    //   110: goto -> 128
    //   113: iconst_0
    //   114: istore #4
    //   116: iconst_0
    //   117: istore #18
    //   119: iconst_0
    //   120: istore #21
    //   122: iconst_0
    //   123: istore #19
    //   125: iconst_0
    //   126: istore #14
    //   128: aload_1
    //   129: aload_2
    //   130: aload_0
    //   131: getfield mText : Ljava/lang/CharSequence;
    //   134: iload #15
    //   136: iload #16
    //   138: aload #11
    //   140: invokevirtual getSize : (Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I
    //   143: i2f
    //   144: fstore #13
    //   146: fload #13
    //   148: fstore #17
    //   150: iload_3
    //   151: ifeq -> 48
    //   154: aload #11
    //   156: iload #4
    //   158: iload #18
    //   160: iload #21
    //   162: iload #19
    //   164: iload #14
    //   166: invokestatic updateMetrics : (Landroid/graphics/Paint$FontMetricsInt;IIIII)V
    //   169: fload #13
    //   171: fstore #17
    //   173: goto -> 48
    //   176: aload #6
    //   178: ifnull -> 222
    //   181: iload #5
    //   183: ifeq -> 196
    //   186: fload #7
    //   188: fload #17
    //   190: fsub
    //   191: fstore #7
    //   193: goto -> 196
    //   196: aload_1
    //   197: aload #6
    //   199: aload_0
    //   200: getfield mText : Ljava/lang/CharSequence;
    //   203: iload #15
    //   205: iload #16
    //   207: fload #7
    //   209: iload #8
    //   211: iload #9
    //   213: iload #10
    //   215: aload_2
    //   216: invokevirtual draw : (Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V
    //   219: goto -> 222
    //   222: iload #5
    //   224: ifeq -> 235
    //   227: fload #17
    //   229: fneg
    //   230: fstore #7
    //   232: goto -> 239
    //   235: fload #17
    //   237: fstore #7
    //   239: fload #7
    //   241: freturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #996	-> 0
    //   #998	-> 3
    //   #999	-> 15
    //   #1001	-> 22
    //   #1026	-> 48
    //   #1002	-> 51
    //   #1003	-> 51
    //   #1004	-> 51
    //   #1005	-> 51
    //   #1006	-> 51
    //   #1008	-> 51
    //   #1010	-> 63
    //   #1011	-> 67
    //   #1012	-> 74
    //   #1013	-> 81
    //   #1014	-> 88
    //   #1015	-> 95
    //   #1010	-> 113
    //   #1018	-> 128
    //   #1020	-> 146
    //   #1021	-> 154
    //   #1026	-> 176
    //   #1027	-> 181
    //   #1028	-> 186
    //   #1027	-> 196
    //   #1030	-> 196
    //   #1026	-> 222
    //   #1034	-> 222
  }
  
  private int adjustStartHyphenEdit(int paramInt1, int paramInt2) {
    if (paramInt1 > 0)
      paramInt2 = 0; 
    return paramInt2;
  }
  
  private int adjustEndHyphenEdit(int paramInt1, int paramInt2) {
    if (paramInt1 < this.mLen)
      paramInt2 = 0; 
    return paramInt2;
  }
  
  private static final class DecorationInfo {
    public float underlineThickness;
    
    public int underlineColor;
    
    public int start = -1;
    
    public boolean isUnderlineText;
    
    public boolean isStrikeThruText;
    
    public int end = -1;
    
    public boolean hasDecoration() {
      return (this.isStrikeThruText || this.isUnderlineText || this.underlineColor != 0);
    }
    
    public DecorationInfo copyInfo() {
      DecorationInfo decorationInfo = new DecorationInfo();
      decorationInfo.isStrikeThruText = this.isStrikeThruText;
      decorationInfo.isUnderlineText = this.isUnderlineText;
      decorationInfo.underlineColor = this.underlineColor;
      decorationInfo.underlineThickness = this.underlineThickness;
      return decorationInfo;
    }
    
    private DecorationInfo() {}
  }
  
  private void extractDecorationInfo(TextPaint paramTextPaint, DecorationInfo paramDecorationInfo) {
    paramDecorationInfo.isStrikeThruText = paramTextPaint.isStrikeThruText();
    if (paramDecorationInfo.isStrikeThruText)
      paramTextPaint.setStrikeThruText(false); 
    paramDecorationInfo.isUnderlineText = paramTextPaint.isUnderlineText();
    if (paramDecorationInfo.isUnderlineText)
      paramTextPaint.setUnderlineText(false); 
    paramDecorationInfo.underlineColor = paramTextPaint.underlineColor;
    paramDecorationInfo.underlineThickness = paramTextPaint.underlineThickness;
    paramTextPaint.setUnderlineText(0, 0.0F);
  }
  
  private float handleRun(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean1, Canvas paramCanvas, float paramFloat, int paramInt4, int paramInt5, int paramInt6, Paint.FontMetricsInt paramFontMetricsInt, boolean paramBoolean2) {
    // Byte code:
    //   0: iload_2
    //   1: iload_1
    //   2: if_icmplt -> 1175
    //   5: iload_2
    //   6: iload_3
    //   7: if_icmpgt -> 1175
    //   10: iload_1
    //   11: iload_2
    //   12: if_icmpne -> 44
    //   15: aload_0
    //   16: getfield mWorkPaint : Landroid/text/TextPaint;
    //   19: astore #5
    //   21: aload #5
    //   23: aload_0
    //   24: getfield mPaint : Landroid/text/TextPaint;
    //   27: invokevirtual set : (Landroid/text/TextPaint;)V
    //   30: aload #10
    //   32: ifnull -> 42
    //   35: aload #10
    //   37: aload #5
    //   39: invokestatic expandMetricsFromPaint : (Landroid/graphics/Paint$FontMetricsInt;Landroid/text/TextPaint;)V
    //   42: fconst_0
    //   43: freturn
    //   44: aload_0
    //   45: getfield mSpanned : Landroid/text/Spanned;
    //   48: astore #12
    //   50: aload #12
    //   52: ifnonnull -> 61
    //   55: iconst_0
    //   56: istore #13
    //   58: goto -> 153
    //   61: aload_0
    //   62: getfield mMetricAffectingSpanSpanSet : Landroid/text/SpanSet;
    //   65: astore #14
    //   67: aload_0
    //   68: getfield mStart : I
    //   71: istore #13
    //   73: aload #14
    //   75: aload #12
    //   77: iload #13
    //   79: iload_1
    //   80: iadd
    //   81: iload #13
    //   83: iload_3
    //   84: iadd
    //   85: invokevirtual init : (Landroid/text/Spanned;II)V
    //   88: aload_0
    //   89: getfield mCharacterStyleSpanSet : Landroid/text/SpanSet;
    //   92: astore #12
    //   94: aload_0
    //   95: getfield mSpanned : Landroid/text/Spanned;
    //   98: astore #14
    //   100: aload_0
    //   101: getfield mStart : I
    //   104: istore #13
    //   106: aload #12
    //   108: aload #14
    //   110: iload #13
    //   112: iload_1
    //   113: iadd
    //   114: iload #13
    //   116: iload_3
    //   117: iadd
    //   118: invokevirtual init : (Landroid/text/Spanned;II)V
    //   121: aload_0
    //   122: getfield mMetricAffectingSpanSpanSet : Landroid/text/SpanSet;
    //   125: getfield numberOfSpans : I
    //   128: ifne -> 150
    //   131: aload_0
    //   132: getfield mCharacterStyleSpanSet : Landroid/text/SpanSet;
    //   135: getfield numberOfSpans : I
    //   138: ifeq -> 144
    //   141: goto -> 150
    //   144: iconst_0
    //   145: istore #13
    //   147: goto -> 153
    //   150: iconst_1
    //   151: istore #13
    //   153: iload #13
    //   155: ifne -> 232
    //   158: aload_0
    //   159: getfield mWorkPaint : Landroid/text/TextPaint;
    //   162: astore #14
    //   164: aload #14
    //   166: aload_0
    //   167: getfield mPaint : Landroid/text/TextPaint;
    //   170: invokevirtual set : (Landroid/text/TextPaint;)V
    //   173: aload #14
    //   175: aload_0
    //   176: iload_1
    //   177: aload #14
    //   179: invokevirtual getStartHyphenEdit : ()I
    //   182: invokespecial adjustStartHyphenEdit : (II)I
    //   185: invokevirtual setStartHyphenEdit : (I)V
    //   188: aload #14
    //   190: aload_0
    //   191: iload_3
    //   192: aload #14
    //   194: invokevirtual getEndHyphenEdit : ()I
    //   197: invokespecial adjustEndHyphenEdit : (II)I
    //   200: invokevirtual setEndHyphenEdit : (I)V
    //   203: aload_0
    //   204: aload #14
    //   206: iload_1
    //   207: iload_3
    //   208: iload_1
    //   209: iload_3
    //   210: iload #4
    //   212: aload #5
    //   214: fload #6
    //   216: iload #7
    //   218: iload #8
    //   220: iload #9
    //   222: aload #10
    //   224: iload #11
    //   226: iload_2
    //   227: aconst_null
    //   228: invokespecial handleText : (Landroid/text/TextPaint;IIIIZLandroid/graphics/Canvas;FIIILandroid/graphics/Paint$FontMetricsInt;ZILjava/util/ArrayList;)F
    //   231: freturn
    //   232: fload #6
    //   234: fstore #15
    //   236: iload_1
    //   237: iload_2
    //   238: if_icmpge -> 1169
    //   241: aload_0
    //   242: getfield mWorkPaint : Landroid/text/TextPaint;
    //   245: astore #16
    //   247: aload #16
    //   249: aload_0
    //   250: getfield mPaint : Landroid/text/TextPaint;
    //   253: invokevirtual set : (Landroid/text/TextPaint;)V
    //   256: aload_0
    //   257: getfield mMetricAffectingSpanSpanSet : Landroid/text/SpanSet;
    //   260: astore #14
    //   262: aload_0
    //   263: getfield mStart : I
    //   266: istore #13
    //   268: aload #14
    //   270: iload #13
    //   272: iload_1
    //   273: iadd
    //   274: iload #13
    //   276: iload_3
    //   277: iadd
    //   278: invokevirtual getNextTransition : (II)I
    //   281: aload_0
    //   282: getfield mStart : I
    //   285: isub
    //   286: istore #17
    //   288: iload #17
    //   290: iload_2
    //   291: invokestatic min : (II)I
    //   294: istore #18
    //   296: iconst_0
    //   297: istore #13
    //   299: aconst_null
    //   300: astore #14
    //   302: iload #13
    //   304: aload_0
    //   305: getfield mMetricAffectingSpanSpanSet : Landroid/text/SpanSet;
    //   308: getfield numberOfSpans : I
    //   311: if_icmpge -> 492
    //   314: aload #14
    //   316: astore #12
    //   318: aload_0
    //   319: getfield mMetricAffectingSpanSpanSet : Landroid/text/SpanSet;
    //   322: getfield spanStarts : [I
    //   325: iload #13
    //   327: iaload
    //   328: aload_0
    //   329: getfield mStart : I
    //   332: iload #18
    //   334: iadd
    //   335: if_icmpge -> 482
    //   338: aload_0
    //   339: getfield mMetricAffectingSpanSpanSet : Landroid/text/SpanSet;
    //   342: getfield spanEnds : [I
    //   345: iload #13
    //   347: iaload
    //   348: istore #19
    //   350: aload_0
    //   351: getfield mStart : I
    //   354: istore #20
    //   356: iload #19
    //   358: iload #20
    //   360: iload_1
    //   361: iadd
    //   362: if_icmpgt -> 372
    //   365: aload #14
    //   367: astore #12
    //   369: goto -> 482
    //   372: iload #20
    //   374: aload_0
    //   375: getfield mEllipsisStart : I
    //   378: iadd
    //   379: aload_0
    //   380: getfield mMetricAffectingSpanSpanSet : Landroid/text/SpanSet;
    //   383: getfield spanStarts : [I
    //   386: iload #13
    //   388: iaload
    //   389: if_icmpgt -> 420
    //   392: aload_0
    //   393: getfield mMetricAffectingSpanSpanSet : Landroid/text/SpanSet;
    //   396: getfield spanEnds : [I
    //   399: iload #13
    //   401: iaload
    //   402: aload_0
    //   403: getfield mStart : I
    //   406: aload_0
    //   407: getfield mEllipsisEnd : I
    //   410: iadd
    //   411: if_icmpgt -> 420
    //   414: iconst_1
    //   415: istore #20
    //   417: goto -> 423
    //   420: iconst_0
    //   421: istore #20
    //   423: aload_0
    //   424: getfield mMetricAffectingSpanSpanSet : Landroid/text/SpanSet;
    //   427: getfield spans : [Ljava/lang/Object;
    //   430: checkcast [Landroid/text/style/MetricAffectingSpan;
    //   433: iload #13
    //   435: aaload
    //   436: astore #12
    //   438: aload #12
    //   440: instanceof android/text/style/ReplacementSpan
    //   443: ifeq -> 471
    //   446: iload #20
    //   448: ifne -> 461
    //   451: aload #12
    //   453: checkcast android/text/style/ReplacementSpan
    //   456: astore #14
    //   458: goto -> 464
    //   461: aconst_null
    //   462: astore #14
    //   464: aload #14
    //   466: astore #12
    //   468: goto -> 482
    //   471: aload #12
    //   473: aload #16
    //   475: invokevirtual updateDrawState : (Landroid/text/TextPaint;)V
    //   478: aload #14
    //   480: astore #12
    //   482: iinc #13, 1
    //   485: aload #12
    //   487: astore #14
    //   489: goto -> 302
    //   492: aload #14
    //   494: ifnull -> 555
    //   497: iload #11
    //   499: ifne -> 517
    //   502: iload #18
    //   504: iload_2
    //   505: if_icmpge -> 511
    //   508: goto -> 517
    //   511: iconst_0
    //   512: istore #21
    //   514: goto -> 520
    //   517: iconst_1
    //   518: istore #21
    //   520: fload #15
    //   522: aload_0
    //   523: aload #14
    //   525: aload #16
    //   527: iload_1
    //   528: iload #18
    //   530: iload #4
    //   532: aload #5
    //   534: fload #15
    //   536: iload #7
    //   538: iload #8
    //   540: iload #9
    //   542: aload #10
    //   544: iload #21
    //   546: invokespecial handleReplacement : (Landroid/text/style/ReplacementSpan;Landroid/text/TextPaint;IIZLandroid/graphics/Canvas;FIIILandroid/graphics/Paint$FontMetricsInt;Z)F
    //   549: fadd
    //   550: fstore #15
    //   552: goto -> 1163
    //   555: iload #18
    //   557: istore #13
    //   559: aload #16
    //   561: astore #14
    //   563: aload_0
    //   564: astore #12
    //   566: aload #12
    //   568: getfield mActivePaint : Landroid/text/TextPaint;
    //   571: astore #16
    //   573: aload #16
    //   575: aload #12
    //   577: getfield mPaint : Landroid/text/TextPaint;
    //   580: invokevirtual set : (Landroid/text/TextPaint;)V
    //   583: aload #12
    //   585: getfield mDecorationInfo : Landroid/text/TextLine$DecorationInfo;
    //   588: astore #22
    //   590: aload #12
    //   592: getfield mDecorations : Ljava/util/ArrayList;
    //   595: invokevirtual clear : ()V
    //   598: iload #13
    //   600: istore #19
    //   602: iload_1
    //   603: istore #23
    //   605: iload_1
    //   606: istore #24
    //   608: iload #13
    //   610: istore #18
    //   612: iload_1
    //   613: istore #20
    //   615: iload #24
    //   617: istore_1
    //   618: iload #23
    //   620: istore #13
    //   622: iload #13
    //   624: iload #18
    //   626: if_icmpge -> 1031
    //   629: aload #12
    //   631: getfield mCharacterStyleSpanSet : Landroid/text/SpanSet;
    //   634: astore #25
    //   636: aload #12
    //   638: getfield mStart : I
    //   641: istore #23
    //   643: aload #25
    //   645: iload #23
    //   647: iload #13
    //   649: iadd
    //   650: iload #23
    //   652: iload #17
    //   654: iadd
    //   655: invokevirtual getNextTransition : (II)I
    //   658: aload #12
    //   660: getfield mStart : I
    //   663: isub
    //   664: istore #23
    //   666: iload #23
    //   668: iload #18
    //   670: invokestatic min : (II)I
    //   673: istore #26
    //   675: aload #14
    //   677: aload #12
    //   679: getfield mPaint : Landroid/text/TextPaint;
    //   682: invokevirtual set : (Landroid/text/TextPaint;)V
    //   685: iconst_0
    //   686: istore #24
    //   688: iload #24
    //   690: aload #12
    //   692: getfield mCharacterStyleSpanSet : Landroid/text/SpanSet;
    //   695: getfield numberOfSpans : I
    //   698: if_icmpge -> 777
    //   701: aload #12
    //   703: getfield mCharacterStyleSpanSet : Landroid/text/SpanSet;
    //   706: getfield spanStarts : [I
    //   709: iload #24
    //   711: iaload
    //   712: aload #12
    //   714: getfield mStart : I
    //   717: iload #26
    //   719: iadd
    //   720: if_icmpge -> 771
    //   723: aload #12
    //   725: getfield mCharacterStyleSpanSet : Landroid/text/SpanSet;
    //   728: getfield spanEnds : [I
    //   731: iload #24
    //   733: iaload
    //   734: aload #12
    //   736: getfield mStart : I
    //   739: iload #13
    //   741: iadd
    //   742: if_icmpgt -> 748
    //   745: goto -> 771
    //   748: aload #12
    //   750: getfield mCharacterStyleSpanSet : Landroid/text/SpanSet;
    //   753: getfield spans : [Ljava/lang/Object;
    //   756: checkcast [Landroid/text/style/CharacterStyle;
    //   759: iload #24
    //   761: aaload
    //   762: astore #25
    //   764: aload #25
    //   766: aload #14
    //   768: invokevirtual updateDrawState : (Landroid/text/TextPaint;)V
    //   771: iinc #24, 1
    //   774: goto -> 688
    //   777: aload #12
    //   779: aload #14
    //   781: aload #22
    //   783: invokespecial extractDecorationInfo : (Landroid/text/TextPaint;Landroid/text/TextLine$DecorationInfo;)V
    //   786: iload #13
    //   788: iload #20
    //   790: if_icmpne -> 803
    //   793: aload #16
    //   795: aload #14
    //   797: invokevirtual set : (Landroid/text/TextPaint;)V
    //   800: goto -> 973
    //   803: aload #14
    //   805: aload #16
    //   807: invokestatic equalAttributes : (Landroid/text/TextPaint;Landroid/text/TextPaint;)Z
    //   810: ifne -> 973
    //   813: aload #12
    //   815: getfield mPaint : Landroid/text/TextPaint;
    //   818: astore #25
    //   820: aload #12
    //   822: iload_1
    //   823: aload #25
    //   825: invokevirtual getStartHyphenEdit : ()I
    //   828: invokespecial adjustStartHyphenEdit : (II)I
    //   831: istore #24
    //   833: aload #16
    //   835: iload #24
    //   837: invokevirtual setStartHyphenEdit : (I)V
    //   840: aload #12
    //   842: getfield mPaint : Landroid/text/TextPaint;
    //   845: astore #25
    //   847: aload #12
    //   849: iload #19
    //   851: aload #25
    //   853: invokevirtual getEndHyphenEdit : ()I
    //   856: invokespecial adjustEndHyphenEdit : (II)I
    //   859: istore #24
    //   861: aload #16
    //   863: iload #24
    //   865: invokevirtual setEndHyphenEdit : (I)V
    //   868: iload #11
    //   870: ifne -> 888
    //   873: iload #19
    //   875: iload_2
    //   876: if_icmpge -> 882
    //   879: goto -> 888
    //   882: iconst_0
    //   883: istore #21
    //   885: goto -> 891
    //   888: iconst_1
    //   889: istore #21
    //   891: iload #19
    //   893: iload #18
    //   895: invokestatic min : (II)I
    //   898: istore #24
    //   900: aload #12
    //   902: getfield mDecorations : Ljava/util/ArrayList;
    //   905: astore #12
    //   907: fload #15
    //   909: aload_0
    //   910: aload #16
    //   912: iload_1
    //   913: iload #19
    //   915: iload #20
    //   917: iload #17
    //   919: iload #4
    //   921: aload #5
    //   923: fload #15
    //   925: iload #7
    //   927: iload #8
    //   929: iload #9
    //   931: aload #10
    //   933: iload #21
    //   935: iload #24
    //   937: aload #12
    //   939: invokespecial handleText : (Landroid/text/TextPaint;IIIIZLandroid/graphics/Canvas;FIIILandroid/graphics/Paint$FontMetricsInt;ZILjava/util/ArrayList;)F
    //   942: fadd
    //   943: fstore #15
    //   945: iload #13
    //   947: istore_1
    //   948: aload #14
    //   950: astore #12
    //   952: aload #16
    //   954: aload #12
    //   956: invokevirtual set : (Landroid/text/TextPaint;)V
    //   959: aload_0
    //   960: astore #12
    //   962: aload #12
    //   964: getfield mDecorations : Ljava/util/ArrayList;
    //   967: invokevirtual clear : ()V
    //   970: goto -> 977
    //   973: aload #14
    //   975: astore #25
    //   977: iload #23
    //   979: istore #19
    //   981: aload #22
    //   983: invokevirtual hasDecoration : ()Z
    //   986: ifeq -> 1024
    //   989: aload #22
    //   991: invokevirtual copyInfo : ()Landroid/text/TextLine$DecorationInfo;
    //   994: astore #25
    //   996: aload #25
    //   998: iload #13
    //   1000: putfield start : I
    //   1003: aload #25
    //   1005: iload #23
    //   1007: putfield end : I
    //   1010: aload #12
    //   1012: getfield mDecorations : Ljava/util/ArrayList;
    //   1015: aload #25
    //   1017: invokevirtual add : (Ljava/lang/Object;)Z
    //   1020: pop
    //   1021: goto -> 1024
    //   1024: iload #23
    //   1026: istore #13
    //   1028: goto -> 622
    //   1031: aload #12
    //   1033: getfield mPaint : Landroid/text/TextPaint;
    //   1036: astore #14
    //   1038: aload #12
    //   1040: iload_1
    //   1041: aload #14
    //   1043: invokevirtual getStartHyphenEdit : ()I
    //   1046: invokespecial adjustStartHyphenEdit : (II)I
    //   1049: istore #13
    //   1051: aload #16
    //   1053: iload #13
    //   1055: invokevirtual setStartHyphenEdit : (I)V
    //   1058: aload #12
    //   1060: getfield mPaint : Landroid/text/TextPaint;
    //   1063: astore #14
    //   1065: aload #12
    //   1067: iload #19
    //   1069: aload #14
    //   1071: invokevirtual getEndHyphenEdit : ()I
    //   1074: invokespecial adjustEndHyphenEdit : (II)I
    //   1077: istore #13
    //   1079: aload #16
    //   1081: iload #13
    //   1083: invokevirtual setEndHyphenEdit : (I)V
    //   1086: iload #11
    //   1088: ifne -> 1106
    //   1091: iload #19
    //   1093: iload_2
    //   1094: if_icmpge -> 1100
    //   1097: goto -> 1106
    //   1100: iconst_0
    //   1101: istore #21
    //   1103: goto -> 1109
    //   1106: iconst_1
    //   1107: istore #21
    //   1109: iload #19
    //   1111: iload #18
    //   1113: invokestatic min : (II)I
    //   1116: istore #13
    //   1118: aload #12
    //   1120: getfield mDecorations : Ljava/util/ArrayList;
    //   1123: astore #14
    //   1125: fload #15
    //   1127: aload_0
    //   1128: aload #16
    //   1130: iload_1
    //   1131: iload #19
    //   1133: iload #20
    //   1135: iload #17
    //   1137: iload #4
    //   1139: aload #5
    //   1141: fload #15
    //   1143: iload #7
    //   1145: iload #8
    //   1147: iload #9
    //   1149: aload #10
    //   1151: iload #21
    //   1153: iload #13
    //   1155: aload #14
    //   1157: invokespecial handleText : (Landroid/text/TextPaint;IIIIZLandroid/graphics/Canvas;FIIILandroid/graphics/Paint$FontMetricsInt;ZILjava/util/ArrayList;)F
    //   1160: fadd
    //   1161: fstore #15
    //   1163: iload #17
    //   1165: istore_1
    //   1166: goto -> 236
    //   1169: fload #15
    //   1171: fload #6
    //   1173: fsub
    //   1174: freturn
    //   1175: new java/lang/StringBuilder
    //   1178: dup
    //   1179: invokespecial <init> : ()V
    //   1182: astore #5
    //   1184: aload #5
    //   1186: ldc_w 'measureLimit ('
    //   1189: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1192: pop
    //   1193: aload #5
    //   1195: iload_2
    //   1196: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1199: pop
    //   1200: aload #5
    //   1202: ldc_w ') is out of start ('
    //   1205: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1208: pop
    //   1209: aload #5
    //   1211: iload_1
    //   1212: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1215: pop
    //   1216: aload #5
    //   1218: ldc_w ') and limit ('
    //   1221: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1224: pop
    //   1225: aload #5
    //   1227: iload_3
    //   1228: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1231: pop
    //   1232: aload #5
    //   1234: ldc_w ') bounds'
    //   1237: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1240: pop
    //   1241: new java/lang/IndexOutOfBoundsException
    //   1244: dup
    //   1245: aload #5
    //   1247: invokevirtual toString : ()Ljava/lang/String;
    //   1250: invokespecial <init> : (Ljava/lang/String;)V
    //   1253: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1107	-> 0
    //   #1113	-> 10
    //   #1114	-> 15
    //   #1115	-> 21
    //   #1116	-> 30
    //   #1117	-> 35
    //   #1119	-> 42
    //   #1123	-> 44
    //   #1124	-> 55
    //   #1126	-> 61
    //   #1127	-> 88
    //   #1128	-> 121
    //   #1132	-> 153
    //   #1133	-> 158
    //   #1134	-> 164
    //   #1135	-> 173
    //   #1136	-> 188
    //   #1137	-> 203
    //   #1146	-> 232
    //   #1147	-> 232
    //   #1148	-> 241
    //   #1149	-> 247
    //   #1151	-> 256
    //   #1153	-> 288
    //   #1155	-> 296
    //   #1157	-> 296
    //   #1160	-> 314
    //   #1161	-> 365
    //   #1163	-> 372
    //   #1166	-> 423
    //   #1167	-> 438
    //   #1168	-> 446
    //   #1172	-> 471
    //   #1157	-> 482
    //   #1176	-> 492
    //   #1177	-> 497
    //   #1179	-> 552
    //   #1182	-> 555
    //   #1183	-> 573
    //   #1184	-> 583
    //   #1185	-> 583
    //   #1186	-> 583
    //   #1187	-> 590
    //   #1188	-> 598
    //   #1189	-> 629
    //   #1192	-> 666
    //   #1193	-> 675
    //   #1194	-> 685
    //   #1196	-> 701
    //   #1197	-> 745
    //   #1199	-> 748
    //   #1200	-> 764
    //   #1194	-> 771
    //   #1203	-> 777
    //   #1205	-> 786
    //   #1209	-> 793
    //   #1210	-> 803
    //   #1214	-> 813
    //   #1215	-> 820
    //   #1214	-> 833
    //   #1216	-> 840
    //   #1217	-> 847
    //   #1216	-> 861
    //   #1218	-> 868
    //   #1220	-> 891
    //   #1218	-> 907
    //   #1222	-> 945
    //   #1223	-> 948
    //   #1224	-> 959
    //   #1210	-> 973
    //   #1232	-> 973
    //   #1233	-> 981
    //   #1234	-> 989
    //   #1235	-> 996
    //   #1236	-> 1003
    //   #1237	-> 1010
    //   #1233	-> 1024
    //   #1188	-> 1024
    //   #1241	-> 1031
    //   #1242	-> 1038
    //   #1241	-> 1051
    //   #1243	-> 1058
    //   #1244	-> 1065
    //   #1243	-> 1079
    //   #1245	-> 1086
    //   #1247	-> 1109
    //   #1245	-> 1125
    //   #1147	-> 1163
    //   #1250	-> 1169
    //   #1108	-> 1175
  }
  
  private void drawTextRun(Canvas paramCanvas, TextPaint paramTextPaint, int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean, float paramFloat, int paramInt5) {
    if (this.mCharsValid) {
      paramCanvas.drawTextRun(this.mChars, paramInt1, paramInt2 - paramInt1, paramInt3, paramInt4 - paramInt3, paramFloat, paramInt5, paramBoolean, paramTextPaint);
    } else {
      int i = this.mStart;
      paramCanvas.drawTextRun(this.mText, i + paramInt1, i + paramInt2, i + paramInt3, i + paramInt4, paramFloat, paramInt5, paramBoolean, paramTextPaint);
    } 
  }
  
  float nextTab(float paramFloat) {
    Layout.TabStops tabStops = this.mTabs;
    if (tabStops != null)
      return tabStops.nextTab(paramFloat); 
    return Layout.TabStops.nextDefaultStop(paramFloat, 20.0F);
  }
  
  private boolean isStretchableWhitespace(int paramInt) {
    boolean bool;
    if (paramInt == 32) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private int countStretchableSpaces(int paramInt1, int paramInt2) {
    int i = 0;
    for (; paramInt1 < paramInt2; paramInt1++, i = j) {
      char c;
      if (this.mCharsValid) {
        c = this.mChars[paramInt1];
      } else {
        c = this.mText.charAt(this.mStart + paramInt1);
      } 
      int j = i;
      if (isStretchableWhitespace(c))
        j = i + 1; 
    } 
    return i;
  }
  
  public static boolean isLineEndSpace(char paramChar) {
    return (paramChar == ' ' || paramChar == '\t' || paramChar == ' ' || (' ' <= paramChar && paramChar <= ' ' && paramChar != ' ') || paramChar == ' ' || paramChar == '　');
  }
  
  private static boolean equalAttributes(TextPaint paramTextPaint1, TextPaint paramTextPaint2) {
    boolean bool;
    if (paramTextPaint1.getColorFilter() == paramTextPaint2.getColorFilter() && 
      paramTextPaint1.getMaskFilter() == paramTextPaint2.getMaskFilter() && 
      paramTextPaint1.getShader() == paramTextPaint2.getShader() && 
      paramTextPaint1.getTypeface() == paramTextPaint2.getTypeface() && 
      paramTextPaint1.getXfermode() == paramTextPaint2.getXfermode() && 
      paramTextPaint1.getTextLocales().equals(paramTextPaint2.getTextLocales()) && 
      TextUtils.equals(paramTextPaint1.getFontFeatureSettings(), paramTextPaint2.getFontFeatureSettings()) && 
      TextUtils.equals(paramTextPaint1.getFontVariationSettings(), paramTextPaint2.getFontVariationSettings()) && 
      paramTextPaint1.getShadowLayerRadius() == paramTextPaint2.getShadowLayerRadius() && 
      paramTextPaint1.getShadowLayerDx() == paramTextPaint2.getShadowLayerDx() && 
      paramTextPaint1.getShadowLayerDy() == paramTextPaint2.getShadowLayerDy() && 
      paramTextPaint1.getShadowLayerColor() == paramTextPaint2.getShadowLayerColor() && 
      paramTextPaint1.getFlags() == paramTextPaint2.getFlags() && 
      paramTextPaint1.getHinting() == paramTextPaint2.getHinting() && 
      paramTextPaint1.getStyle() == paramTextPaint2.getStyle() && 
      paramTextPaint1.getColor() == paramTextPaint2.getColor() && 
      paramTextPaint1.getStrokeWidth() == paramTextPaint2.getStrokeWidth() && 
      paramTextPaint1.getStrokeMiter() == paramTextPaint2.getStrokeMiter() && 
      paramTextPaint1.getStrokeCap() == paramTextPaint2.getStrokeCap() && 
      paramTextPaint1.getStrokeJoin() == paramTextPaint2.getStrokeJoin() && 
      paramTextPaint1.getTextAlign() == paramTextPaint2.getTextAlign() && 
      paramTextPaint1.isElegantTextHeight() == paramTextPaint2.isElegantTextHeight() && 
      paramTextPaint1.getTextSize() == paramTextPaint2.getTextSize() && 
      paramTextPaint1.getTextScaleX() == paramTextPaint2.getTextScaleX() && 
      paramTextPaint1.getTextSkewX() == paramTextPaint2.getTextSkewX() && 
      paramTextPaint1.getLetterSpacing() == paramTextPaint2.getLetterSpacing() && 
      paramTextPaint1.getWordSpacing() == paramTextPaint2.getWordSpacing() && 
      paramTextPaint1.getStartHyphenEdit() == paramTextPaint2.getStartHyphenEdit() && 
      paramTextPaint1.getEndHyphenEdit() == paramTextPaint2.getEndHyphenEdit() && paramTextPaint1.bgColor == paramTextPaint2.bgColor && paramTextPaint1.baselineShift == paramTextPaint2.baselineShift && paramTextPaint1.linkColor == paramTextPaint2.linkColor && paramTextPaint1.drawableState == paramTextPaint2.drawableState && paramTextPaint1.density == paramTextPaint2.density && paramTextPaint1.underlineColor == paramTextPaint2.underlineColor && paramTextPaint1.underlineThickness == paramTextPaint2.underlineThickness) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
}
