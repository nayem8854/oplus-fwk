package android.text;

import java.nio.CharBuffer;
import java.util.Locale;

public class TextDirectionHeuristics {
  public static final TextDirectionHeuristic ANYRTL_LTR;
  
  public static final TextDirectionHeuristic FIRSTSTRONG_LTR;
  
  public static final TextDirectionHeuristic FIRSTSTRONG_RTL;
  
  public static final TextDirectionHeuristic LOCALE;
  
  public static final TextDirectionHeuristic LTR = new TextDirectionHeuristicInternal(false);
  
  public static final TextDirectionHeuristic RTL = new TextDirectionHeuristicInternal(true);
  
  private static final int STATE_FALSE = 1;
  
  private static final int STATE_TRUE = 0;
  
  private static final int STATE_UNKNOWN = 2;
  
  static {
    FIRSTSTRONG_LTR = new TextDirectionHeuristicInternal(false);
    FIRSTSTRONG_RTL = new TextDirectionHeuristicInternal(true);
    ANYRTL_LTR = new TextDirectionHeuristicInternal(false);
    LOCALE = TextDirectionHeuristicLocale.INSTANCE;
  }
  
  private static int isRtlCodePoint(int paramInt) {
    byte b = Character.getDirectionality(paramInt);
    if (b != -1) {
      if (b != 0) {
        if (b != 1 && b != 2)
          return 2; 
        return 0;
      } 
      return 1;
    } 
    if ((1424 <= paramInt && paramInt <= 2303) || (64285 <= paramInt && paramInt <= 64975) || (65008 <= paramInt && paramInt <= 65023) || (65136 <= paramInt && paramInt <= 65279) || (67584 <= paramInt && paramInt <= 69631) || (124928 <= paramInt && paramInt <= 126975))
      return 0; 
    if ((8293 <= paramInt && paramInt <= 8297) || (65520 <= paramInt && paramInt <= 65528) || (917504 <= paramInt && paramInt <= 921599) || (64976 <= paramInt && paramInt <= 65007) || (paramInt & 0xFFFE) == 65534 || (8352 <= paramInt && paramInt <= 8399) || (55296 <= paramInt && paramInt <= 57343))
      return 2; 
    return 1;
  }
  
  class TextDirectionHeuristicImpl implements TextDirectionHeuristic {
    private final TextDirectionHeuristics.TextDirectionAlgorithm mAlgorithm;
    
    public TextDirectionHeuristicImpl(TextDirectionHeuristics this$0) {
      this.mAlgorithm = (TextDirectionHeuristics.TextDirectionAlgorithm)this$0;
    }
    
    public boolean isRtl(char[] param1ArrayOfchar, int param1Int1, int param1Int2) {
      return isRtl(CharBuffer.wrap(param1ArrayOfchar), param1Int1, param1Int2);
    }
    
    public boolean isRtl(CharSequence param1CharSequence, int param1Int1, int param1Int2) {
      if (param1CharSequence != null && param1Int1 >= 0 && param1Int2 >= 0 && param1CharSequence.length() - param1Int2 >= param1Int1) {
        if (this.mAlgorithm == null)
          return defaultIsRtl(); 
        return doCheck(param1CharSequence, param1Int1, param1Int2);
      } 
      throw new IllegalArgumentException();
    }
    
    private boolean doCheck(CharSequence param1CharSequence, int param1Int1, int param1Int2) {
      param1Int1 = this.mAlgorithm.checkRtl(param1CharSequence, param1Int1, param1Int2);
      if (param1Int1 != 0) {
        if (param1Int1 != 1)
          return defaultIsRtl(); 
        return false;
      } 
      return true;
    }
    
    protected abstract boolean defaultIsRtl();
  }
  
  class TextDirectionHeuristicInternal extends TextDirectionHeuristicImpl {
    private final boolean mDefaultIsRtl;
    
    private TextDirectionHeuristicInternal(TextDirectionHeuristics this$0, boolean param1Boolean) {
      this.mDefaultIsRtl = param1Boolean;
    }
    
    protected boolean defaultIsRtl() {
      return this.mDefaultIsRtl;
    }
  }
  
  class FirstStrong implements TextDirectionAlgorithm {
    public int checkRtl(CharSequence param1CharSequence, int param1Int1, int param1Int2) {
      int i = 2;
      byte b = 0;
      int j = param1Int1;
      for (; j < param1Int1 + param1Int2 && i == 2; 
        j += Character.charCount(k), i = m, b = b1) {
        byte b1;
        int m, k = Character.codePointAt(param1CharSequence, j);
        if (8294 <= k && k <= 8296) {
          b1 = b + 1;
          m = i;
        } else if (k == 8297) {
          m = i;
          b1 = b;
          if (b > 0) {
            b1 = b - 1;
            m = i;
          } 
        } else {
          m = i;
          b1 = b;
          if (b == 0) {
            m = TextDirectionHeuristics.isRtlCodePoint(k);
            b1 = b;
          } 
        } 
      } 
      return i;
    }
    
    public static final FirstStrong INSTANCE = new FirstStrong();
  }
  
  class AnyStrong implements TextDirectionAlgorithm {
    public static final AnyStrong INSTANCE_LTR = new AnyStrong(false);
    
    public int checkRtl(CharSequence param1CharSequence, int param1Int1, int param1Int2) {
      int i = 0;
      byte b = 0;
      for (int j = param1Int1; j < param1Int1 + param1Int2; j += Character.charCount(k), i = m, b = b1) {
        byte b1;
        int m, k = Character.codePointAt(param1CharSequence, j);
        if (8294 <= k && k <= 8296) {
          b1 = b + 1;
          m = i;
        } else if (k == 8297) {
          m = i;
          b1 = b;
          if (b > 0) {
            b1 = b - 1;
            m = i;
          } 
        } else {
          m = i;
          b1 = b;
          if (b == 0) {
            m = TextDirectionHeuristics.isRtlCodePoint(k);
            if (m != 0) {
              if (m != 1) {
                m = i;
                b1 = b;
              } else {
                if (!this.mLookForRtl)
                  return 1; 
                m = 1;
                b1 = b;
              } 
            } else {
              if (this.mLookForRtl)
                return 0; 
              m = 1;
              b1 = b;
            } 
          } 
        } 
      } 
      if (i != 0)
        return this.mLookForRtl; 
      return 2;
    }
    
    private AnyStrong(TextDirectionHeuristics this$0) {
      this.mLookForRtl = this$0;
    }
    
    public static final AnyStrong INSTANCE_RTL = new AnyStrong(true);
    
    private final boolean mLookForRtl;
    
    static {
    
    }
  }
  
  class TextDirectionHeuristicLocale extends TextDirectionHeuristicImpl {
    public TextDirectionHeuristicLocale() {
      super(null);
    }
    
    protected boolean defaultIsRtl() {
      int i = TextUtils.getLayoutDirectionFromLocale(Locale.getDefault());
      boolean bool = true;
      if (i != 1)
        bool = false; 
      return bool;
    }
    
    public static final TextDirectionHeuristicLocale INSTANCE = new TextDirectionHeuristicLocale();
  }
  
  private static interface TextDirectionAlgorithm {
    int checkRtl(CharSequence param1CharSequence, int param1Int1, int param1Int2);
  }
}
