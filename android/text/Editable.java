package android.text;

import java.io.IOException;

public interface Editable extends CharSequence, GetChars, Spannable, Appendable {
  Editable append(char paramChar);
  
  Editable append(CharSequence paramCharSequence);
  
  Editable append(CharSequence paramCharSequence, int paramInt1, int paramInt2);
  
  void clear();
  
  void clearSpans();
  
  Editable delete(int paramInt1, int paramInt2);
  
  InputFilter[] getFilters();
  
  Editable insert(int paramInt, CharSequence paramCharSequence);
  
  Editable insert(int paramInt1, CharSequence paramCharSequence, int paramInt2, int paramInt3);
  
  Editable replace(int paramInt1, int paramInt2, CharSequence paramCharSequence);
  
  Editable replace(int paramInt1, int paramInt2, CharSequence paramCharSequence, int paramInt3, int paramInt4);
  
  void setFilters(InputFilter[] paramArrayOfInputFilter);
  
  class Factory {
    private static Factory sInstance = new Factory();
    
    public static Factory getInstance() {
      return sInstance;
    }
    
    public Editable newEditable(CharSequence param1CharSequence) {
      return new SpannableStringBuilder(param1CharSequence);
    }
  }
}
