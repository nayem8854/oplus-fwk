package android.text;

import android.graphics.Paint;
import android.graphics.Rect;
import android.text.style.ReplacementSpan;
import android.util.ArraySet;
import android.util.Pools;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.GrowingArrayUtils;
import java.lang.ref.WeakReference;

public class DynamicLayout extends Layout {
  private static final int BLOCK_MINIMUM_CHARACTER_LENGTH = 400;
  
  private static final int COLUMNS_ELLIPSIZE = 7;
  
  private static final int COLUMNS_NORMAL = 5;
  
  private static final int DESCENT = 2;
  
  private static final int DIR = 0;
  
  private static final int DIR_SHIFT = 30;
  
  private static final int ELLIPSIS_COUNT = 6;
  
  private static final int ELLIPSIS_START = 5;
  
  private static final int ELLIPSIS_UNDEFINED = -2147483648;
  
  private static final int EXTRA = 3;
  
  private static final int HYPHEN = 4;
  
  private static final int HYPHEN_MASK = 255;
  
  public static final int INVALID_BLOCK_INDEX = -1;
  
  private static final int MAY_PROTRUDE_FROM_TOP_OR_BOTTOM = 4;
  
  private static final int MAY_PROTRUDE_FROM_TOP_OR_BOTTOM_MASK = 256;
  
  private static final int PRIORITY = 128;
  
  private static final int START = 0;
  
  private static final int START_MASK = 536870911;
  
  private static final int TAB = 0;
  
  private static final int TAB_MASK = 536870912;
  
  private static final int TOP = 1;
  
  private static StaticLayout.Builder sBuilder;
  
  private static final Object[] sLock;
  
  class Builder {
    private int mWidth;
    
    private TextDirectionHeuristic mTextDir;
    
    private float mSpacingMult;
    
    private float mSpacingAdd;
    
    private TextPaint mPaint;
    
    private int mJustificationMode;
    
    private boolean mIncludePad;
    
    private int mHyphenationFrequency;
    
    public static Builder obtain(CharSequence param1CharSequence, TextPaint param1TextPaint, int param1Int) {
      Builder builder1 = sPool.acquire();
      Builder builder2 = builder1;
      if (builder1 == null)
        builder2 = new Builder(); 
      builder2.mBase = param1CharSequence;
      builder2.mDisplay = param1CharSequence;
      builder2.mPaint = param1TextPaint;
      builder2.mWidth = param1Int;
      builder2.mAlignment = Layout.Alignment.ALIGN_NORMAL;
      builder2.mTextDir = TextDirectionHeuristics.FIRSTSTRONG_LTR;
      builder2.mSpacingMult = 1.0F;
      builder2.mSpacingAdd = 0.0F;
      builder2.mIncludePad = true;
      builder2.mFallbackLineSpacing = false;
      builder2.mEllipsizedWidth = param1Int;
      builder2.mEllipsize = null;
      builder2.mBreakStrategy = 0;
      builder2.mHyphenationFrequency = 0;
      builder2.mJustificationMode = 0;
      return builder2;
    }
    
    private static void recycle(Builder param1Builder) {
      param1Builder.mBase = null;
      param1Builder.mDisplay = null;
      param1Builder.mPaint = null;
      sPool.release(param1Builder);
    }
    
    public Builder setDisplayText(CharSequence param1CharSequence) {
      this.mDisplay = param1CharSequence;
      return this;
    }
    
    public Builder setAlignment(Layout.Alignment param1Alignment) {
      this.mAlignment = param1Alignment;
      return this;
    }
    
    public Builder setTextDirection(TextDirectionHeuristic param1TextDirectionHeuristic) {
      this.mTextDir = param1TextDirectionHeuristic;
      return this;
    }
    
    public Builder setLineSpacing(float param1Float1, float param1Float2) {
      this.mSpacingAdd = param1Float1;
      this.mSpacingMult = param1Float2;
      return this;
    }
    
    public Builder setIncludePad(boolean param1Boolean) {
      this.mIncludePad = param1Boolean;
      return this;
    }
    
    public Builder setUseLineSpacingFromFallbacks(boolean param1Boolean) {
      this.mFallbackLineSpacing = param1Boolean;
      return this;
    }
    
    public Builder setEllipsizedWidth(int param1Int) {
      this.mEllipsizedWidth = param1Int;
      return this;
    }
    
    public Builder setEllipsize(TextUtils.TruncateAt param1TruncateAt) {
      this.mEllipsize = param1TruncateAt;
      return this;
    }
    
    public Builder setBreakStrategy(int param1Int) {
      this.mBreakStrategy = param1Int;
      return this;
    }
    
    public Builder setHyphenationFrequency(int param1Int) {
      this.mHyphenationFrequency = param1Int;
      return this;
    }
    
    public Builder setJustificationMode(int param1Int) {
      this.mJustificationMode = param1Int;
      return this;
    }
    
    public DynamicLayout build() {
      DynamicLayout dynamicLayout = new DynamicLayout(this);
      recycle(this);
      return dynamicLayout;
    }
    
    private final Paint.FontMetricsInt mFontMetricsInt = new Paint.FontMetricsInt();
    
    private boolean mFallbackLineSpacing;
    
    private int mEllipsizedWidth;
    
    private TextUtils.TruncateAt mEllipsize;
    
    private CharSequence mDisplay;
    
    private int mBreakStrategy;
    
    private CharSequence mBase;
    
    private Layout.Alignment mAlignment;
    
    private static final Pools.SynchronizedPool<Builder> sPool = new Pools.SynchronizedPool<>(3);
  }
  
  @Deprecated
  public DynamicLayout(CharSequence paramCharSequence, TextPaint paramTextPaint, int paramInt, Layout.Alignment paramAlignment, float paramFloat1, float paramFloat2, boolean paramBoolean) {
    this(paramCharSequence, paramCharSequence, paramTextPaint, paramInt, paramAlignment, paramFloat1, paramFloat2, paramBoolean);
  }
  
  @Deprecated
  public DynamicLayout(CharSequence paramCharSequence1, CharSequence paramCharSequence2, TextPaint paramTextPaint, int paramInt, Layout.Alignment paramAlignment, float paramFloat1, float paramFloat2, boolean paramBoolean) {
    this(paramCharSequence1, paramCharSequence2, paramTextPaint, paramInt, paramAlignment, paramFloat1, paramFloat2, paramBoolean, (TextUtils.TruncateAt)null, 0);
  }
  
  @Deprecated
  public DynamicLayout(CharSequence paramCharSequence1, CharSequence paramCharSequence2, TextPaint paramTextPaint, int paramInt1, Layout.Alignment paramAlignment, float paramFloat1, float paramFloat2, boolean paramBoolean, TextUtils.TruncateAt paramTruncateAt, int paramInt2) {
    this(paramCharSequence1, paramCharSequence2, paramTextPaint, paramInt1, paramAlignment, TextDirectionHeuristics.FIRSTSTRONG_LTR, paramFloat1, paramFloat2, paramBoolean, 0, 0, 0, paramTruncateAt, paramInt2);
  }
  
  @Deprecated
  public DynamicLayout(CharSequence paramCharSequence1, CharSequence paramCharSequence2, TextPaint paramTextPaint, int paramInt1, Layout.Alignment paramAlignment, TextDirectionHeuristic paramTextDirectionHeuristic, float paramFloat1, float paramFloat2, boolean paramBoolean, int paramInt2, int paramInt3, int paramInt4, TextUtils.TruncateAt paramTruncateAt, int paramInt5) {
    super(createEllipsizer(paramTruncateAt, paramCharSequence2), paramTextPaint, paramInt1, paramAlignment, paramTextDirectionHeuristic, paramFloat1, paramFloat2);
    this.mTempRect = new Rect();
    Builder builder = Builder.obtain(paramCharSequence1, paramTextPaint, paramInt1);
    builder = builder.setAlignment(paramAlignment);
    builder = builder.setTextDirection(paramTextDirectionHeuristic);
    builder = builder.setLineSpacing(paramFloat2, paramFloat1);
    builder = builder.setEllipsizedWidth(paramInt5);
    builder = builder.setEllipsize(paramTruncateAt);
    this.mDisplay = paramCharSequence2;
    this.mIncludePad = paramBoolean;
    this.mBreakStrategy = paramInt2;
    this.mJustificationMode = paramInt4;
    this.mHyphenationFrequency = paramInt3;
    generate(builder);
    Builder.recycle(builder);
  }
  
  private DynamicLayout(Builder paramBuilder) {
    super(charSequence, textPaint, i, alignment, textDirectionHeuristic, f1, f2);
    this.mTempRect = new Rect();
    this.mDisplay = paramBuilder.mDisplay;
    this.mIncludePad = paramBuilder.mIncludePad;
    this.mBreakStrategy = paramBuilder.mBreakStrategy;
    this.mJustificationMode = paramBuilder.mJustificationMode;
    this.mHyphenationFrequency = paramBuilder.mHyphenationFrequency;
    generate(paramBuilder);
  }
  
  private static CharSequence createEllipsizer(TextUtils.TruncateAt paramTruncateAt, CharSequence paramCharSequence) {
    if (paramTruncateAt == null)
      return paramCharSequence; 
    if (paramCharSequence instanceof Spanned)
      return new Layout.SpannedEllipsizer(paramCharSequence); 
    return new Layout.Ellipsizer(paramCharSequence);
  }
  
  private void generate(Builder paramBuilder) {
    int[] arrayOfInt;
    this.mBase = paramBuilder.mBase;
    this.mFallbackLineSpacing = paramBuilder.mFallbackLineSpacing;
    if (paramBuilder.mEllipsize != null) {
      this.mInts = new PackedIntVector(7);
      this.mEllipsizedWidth = paramBuilder.mEllipsizedWidth;
      this.mEllipsizeAt = paramBuilder.mEllipsize;
      Layout.Ellipsizer ellipsizer = (Layout.Ellipsizer)getText();
      ellipsizer.mLayout = this;
      ellipsizer.mWidth = paramBuilder.mEllipsizedWidth;
      ellipsizer.mMethod = paramBuilder.mEllipsize;
      this.mEllipsize = true;
    } else {
      this.mInts = new PackedIntVector(5);
      this.mEllipsizedWidth = paramBuilder.mWidth;
      this.mEllipsizeAt = null;
    } 
    this.mObjects = new PackedObjectVector<>(1);
    if (paramBuilder.mEllipsize != null) {
      arrayOfInt = new int[7];
      arrayOfInt[5] = Integer.MIN_VALUE;
    } else {
      arrayOfInt = new int[5];
    } 
    Layout.Directions directions = DIRS_ALL_LEFT_TO_RIGHT;
    Paint.FontMetricsInt fontMetricsInt = paramBuilder.mFontMetricsInt;
    paramBuilder.mPaint.getFontMetricsInt(fontMetricsInt);
    int i = fontMetricsInt.ascent;
    int j = fontMetricsInt.descent;
    arrayOfInt[0] = 1073741824;
    arrayOfInt[1] = 0;
    arrayOfInt[2] = j;
    this.mInts.insertAt(0, arrayOfInt);
    arrayOfInt[1] = j - i;
    this.mInts.insertAt(1, arrayOfInt);
    this.mObjects.insertAt(0, new Layout.Directions[] { directions });
    i = this.mBase.length();
    reflow(this.mBase, 0, 0, i);
    if (this.mBase instanceof Spannable) {
      if (this.mWatcher == null)
        this.mWatcher = new ChangeWatcher(this); 
      Spannable spannable = (Spannable)this.mBase;
      ChangeWatcher[] arrayOfChangeWatcher = spannable.<ChangeWatcher>getSpans(0, i, ChangeWatcher.class);
      for (j = 0; j < arrayOfChangeWatcher.length; j++)
        spannable.removeSpan(arrayOfChangeWatcher[j]); 
      spannable.setSpan(this.mWatcher, 0, i, 8388626);
    } 
  }
  
  public void reflow(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {
    // Byte code:
    //   0: aload_1
    //   1: aload_0
    //   2: getfield mBase : Ljava/lang/CharSequence;
    //   5: if_acmpeq -> 9
    //   8: return
    //   9: aload_0
    //   10: getfield mDisplay : Ljava/lang/CharSequence;
    //   13: astore #5
    //   15: aload #5
    //   17: invokeinterface length : ()I
    //   22: istore #6
    //   24: aload #5
    //   26: bipush #10
    //   28: iload_2
    //   29: iconst_1
    //   30: isub
    //   31: invokestatic lastIndexOf : (Ljava/lang/CharSequence;CI)I
    //   34: istore #7
    //   36: iload #7
    //   38: ifge -> 47
    //   41: iconst_0
    //   42: istore #7
    //   44: goto -> 50
    //   47: iinc #7, 1
    //   50: iload_2
    //   51: iload #7
    //   53: isub
    //   54: istore #8
    //   56: iload #4
    //   58: iload #8
    //   60: iadd
    //   61: istore #7
    //   63: iload_2
    //   64: iload #8
    //   66: isub
    //   67: istore_2
    //   68: aload #5
    //   70: bipush #10
    //   72: iload_2
    //   73: iload #7
    //   75: iadd
    //   76: invokestatic indexOf : (Ljava/lang/CharSequence;CI)I
    //   79: istore #4
    //   81: iload #4
    //   83: ifge -> 93
    //   86: iload #6
    //   88: istore #4
    //   90: goto -> 96
    //   93: iinc #4, 1
    //   96: iload #4
    //   98: iload_2
    //   99: iload #7
    //   101: iadd
    //   102: isub
    //   103: istore #9
    //   105: iload_3
    //   106: iload #8
    //   108: iadd
    //   109: iload #9
    //   111: iadd
    //   112: istore #4
    //   114: iload #7
    //   116: iload #9
    //   118: iadd
    //   119: istore #7
    //   121: aload #5
    //   123: instanceof android/text/Spanned
    //   126: ifeq -> 324
    //   129: aload #5
    //   131: checkcast android/text/Spanned
    //   134: astore #10
    //   136: iload #7
    //   138: istore_3
    //   139: iload_2
    //   140: istore #7
    //   142: iconst_0
    //   143: istore #9
    //   145: aload #10
    //   147: iload #7
    //   149: iload #7
    //   151: iload_3
    //   152: iadd
    //   153: ldc_w android/text/style/WrapTogetherSpan
    //   156: invokeinterface getSpans : (IILjava/lang/Class;)[Ljava/lang/Object;
    //   161: astore_1
    //   162: iconst_0
    //   163: istore #11
    //   165: iload_3
    //   166: istore_2
    //   167: iload #4
    //   169: istore_3
    //   170: iload #7
    //   172: istore #4
    //   174: iload #11
    //   176: aload_1
    //   177: arraylength
    //   178: if_icmpge -> 301
    //   181: aload #10
    //   183: aload_1
    //   184: iload #11
    //   186: aaload
    //   187: invokeinterface getSpanStart : (Ljava/lang/Object;)I
    //   192: istore #12
    //   194: aload #10
    //   196: aload_1
    //   197: iload #11
    //   199: aaload
    //   200: invokeinterface getSpanEnd : (Ljava/lang/Object;)I
    //   205: istore #13
    //   207: iload #4
    //   209: istore #14
    //   211: iload_3
    //   212: istore #8
    //   214: iload_2
    //   215: istore #7
    //   217: iload #12
    //   219: iload #4
    //   221: if_icmpge -> 253
    //   224: iconst_1
    //   225: istore #9
    //   227: iload #4
    //   229: iload #12
    //   231: isub
    //   232: istore #14
    //   234: iload_3
    //   235: iload #14
    //   237: iadd
    //   238: istore #8
    //   240: iload_2
    //   241: iload #14
    //   243: iadd
    //   244: istore #7
    //   246: iload #4
    //   248: iload #14
    //   250: isub
    //   251: istore #14
    //   253: iload #8
    //   255: istore_3
    //   256: iload #7
    //   258: istore_2
    //   259: iload #13
    //   261: iload #14
    //   263: iload #7
    //   265: iadd
    //   266: if_icmple -> 291
    //   269: iload #13
    //   271: iload #14
    //   273: iload #7
    //   275: iadd
    //   276: isub
    //   277: istore_2
    //   278: iload #8
    //   280: iload_2
    //   281: iadd
    //   282: istore_3
    //   283: iload #7
    //   285: iload_2
    //   286: iadd
    //   287: istore_2
    //   288: iconst_1
    //   289: istore #9
    //   291: iinc #11, 1
    //   294: iload #14
    //   296: istore #4
    //   298: goto -> 174
    //   301: iload #9
    //   303: ifne -> 312
    //   306: iload_3
    //   307: istore #14
    //   309: goto -> 336
    //   312: iload #4
    //   314: istore #7
    //   316: iload_3
    //   317: istore #4
    //   319: iload_2
    //   320: istore_3
    //   321: goto -> 142
    //   324: iload_2
    //   325: istore_3
    //   326: iload #7
    //   328: istore_2
    //   329: iload #4
    //   331: istore #14
    //   333: iload_3
    //   334: istore #4
    //   336: aload_0
    //   337: iload #4
    //   339: invokevirtual getLineForOffset : (I)I
    //   342: istore #15
    //   344: aload_0
    //   345: iload #15
    //   347: invokevirtual getLineTop : (I)I
    //   350: istore #12
    //   352: aload_0
    //   353: iload #4
    //   355: iload #14
    //   357: iadd
    //   358: invokevirtual getLineForOffset : (I)I
    //   361: istore #8
    //   363: iload #4
    //   365: iload_2
    //   366: iadd
    //   367: iload #6
    //   369: if_icmpne -> 381
    //   372: aload_0
    //   373: invokevirtual getLineCount : ()I
    //   376: istore #8
    //   378: goto -> 381
    //   381: aload_0
    //   382: iload #8
    //   384: invokevirtual getLineTop : (I)I
    //   387: istore #16
    //   389: iload #8
    //   391: aload_0
    //   392: invokevirtual getLineCount : ()I
    //   395: if_icmpne -> 404
    //   398: iconst_1
    //   399: istore #11
    //   401: goto -> 407
    //   404: iconst_0
    //   405: istore #11
    //   407: getstatic android/text/DynamicLayout.sLock : [Ljava/lang/Object;
    //   410: astore #17
    //   412: aload #17
    //   414: monitorenter
    //   415: getstatic android/text/DynamicLayout.sStaticLayout : Landroid/text/StaticLayout;
    //   418: astore #10
    //   420: getstatic android/text/DynamicLayout.sBuilder : Landroid/text/StaticLayout$Builder;
    //   423: astore_1
    //   424: aconst_null
    //   425: putstatic android/text/DynamicLayout.sStaticLayout : Landroid/text/StaticLayout;
    //   428: aconst_null
    //   429: putstatic android/text/DynamicLayout.sBuilder : Landroid/text/StaticLayout$Builder;
    //   432: aload #17
    //   434: monitorexit
    //   435: aload #10
    //   437: ifnonnull -> 473
    //   440: new android/text/StaticLayout
    //   443: dup
    //   444: aconst_null
    //   445: invokespecial <init> : (Ljava/lang/CharSequence;)V
    //   448: astore #10
    //   450: aload #5
    //   452: iload #4
    //   454: iload #4
    //   456: iload_2
    //   457: iadd
    //   458: aload_0
    //   459: invokevirtual getPaint : ()Landroid/text/TextPaint;
    //   462: aload_0
    //   463: invokevirtual getWidth : ()I
    //   466: invokestatic obtain : (Ljava/lang/CharSequence;IILandroid/text/TextPaint;I)Landroid/text/StaticLayout$Builder;
    //   469: astore_1
    //   470: goto -> 473
    //   473: aload_1
    //   474: aload #5
    //   476: iload #4
    //   478: iload #4
    //   480: iload_2
    //   481: iadd
    //   482: invokevirtual setText : (Ljava/lang/CharSequence;II)Landroid/text/StaticLayout$Builder;
    //   485: astore #17
    //   487: aload #17
    //   489: aload_0
    //   490: invokevirtual getPaint : ()Landroid/text/TextPaint;
    //   493: invokevirtual setPaint : (Landroid/text/TextPaint;)Landroid/text/StaticLayout$Builder;
    //   496: astore #17
    //   498: aload #17
    //   500: aload_0
    //   501: invokevirtual getWidth : ()I
    //   504: invokevirtual setWidth : (I)Landroid/text/StaticLayout$Builder;
    //   507: astore #17
    //   509: aload #17
    //   511: aload_0
    //   512: invokevirtual getTextDirectionHeuristic : ()Landroid/text/TextDirectionHeuristic;
    //   515: invokevirtual setTextDirection : (Landroid/text/TextDirectionHeuristic;)Landroid/text/StaticLayout$Builder;
    //   518: astore #17
    //   520: aload #17
    //   522: aload_0
    //   523: invokevirtual getSpacingAdd : ()F
    //   526: aload_0
    //   527: invokevirtual getSpacingMultiplier : ()F
    //   530: invokevirtual setLineSpacing : (FF)Landroid/text/StaticLayout$Builder;
    //   533: astore #17
    //   535: aload_0
    //   536: getfield mFallbackLineSpacing : Z
    //   539: istore #18
    //   541: aload #17
    //   543: iload #18
    //   545: invokevirtual setUseLineSpacingFromFallbacks : (Z)Landroid/text/StaticLayout$Builder;
    //   548: astore #17
    //   550: aload_0
    //   551: getfield mEllipsizedWidth : I
    //   554: istore_3
    //   555: aload #17
    //   557: iload_3
    //   558: invokevirtual setEllipsizedWidth : (I)Landroid/text/StaticLayout$Builder;
    //   561: astore #17
    //   563: aload_0
    //   564: getfield mEllipsizeAt : Landroid/text/TextUtils$TruncateAt;
    //   567: astore #19
    //   569: aload #17
    //   571: aload #19
    //   573: invokevirtual setEllipsize : (Landroid/text/TextUtils$TruncateAt;)Landroid/text/StaticLayout$Builder;
    //   576: astore #17
    //   578: aload_0
    //   579: getfield mBreakStrategy : I
    //   582: istore_3
    //   583: aload #17
    //   585: iload_3
    //   586: invokevirtual setBreakStrategy : (I)Landroid/text/StaticLayout$Builder;
    //   589: astore #17
    //   591: aload_0
    //   592: getfield mHyphenationFrequency : I
    //   595: istore_3
    //   596: aload #17
    //   598: iload_3
    //   599: invokevirtual setHyphenationFrequency : (I)Landroid/text/StaticLayout$Builder;
    //   602: astore #17
    //   604: aload_0
    //   605: getfield mJustificationMode : I
    //   608: istore_3
    //   609: aload #17
    //   611: iload_3
    //   612: invokevirtual setJustificationMode : (I)Landroid/text/StaticLayout$Builder;
    //   615: astore #17
    //   617: iload #11
    //   619: ifne -> 628
    //   622: iconst_1
    //   623: istore #18
    //   625: goto -> 631
    //   628: iconst_0
    //   629: istore #18
    //   631: aload #17
    //   633: iload #18
    //   635: invokevirtual setAddLastLineLineSpacing : (Z)Landroid/text/StaticLayout$Builder;
    //   638: pop
    //   639: aload #10
    //   641: aload_1
    //   642: iconst_0
    //   643: iconst_1
    //   644: invokevirtual generate : (Landroid/text/StaticLayout$Builder;ZZ)V
    //   647: aload #10
    //   649: invokevirtual getLineCount : ()I
    //   652: istore #9
    //   654: iload #4
    //   656: iload_2
    //   657: iadd
    //   658: iload #6
    //   660: if_icmpeq -> 685
    //   663: aload #10
    //   665: iload #9
    //   667: iconst_1
    //   668: isub
    //   669: invokevirtual getLineStart : (I)I
    //   672: iload #4
    //   674: iload_2
    //   675: iadd
    //   676: if_icmpne -> 685
    //   679: iinc #9, -1
    //   682: goto -> 685
    //   685: aload_0
    //   686: getfield mInts : Landroid/text/PackedIntVector;
    //   689: iload #15
    //   691: iload #8
    //   693: iload #15
    //   695: isub
    //   696: invokevirtual deleteAt : (II)V
    //   699: aload_0
    //   700: getfield mObjects : Landroid/text/PackedObjectVector;
    //   703: iload #15
    //   705: iload #8
    //   707: iload #15
    //   709: isub
    //   710: invokevirtual deleteAt : (II)V
    //   713: aload #10
    //   715: iload #9
    //   717: invokevirtual getLineTop : (I)I
    //   720: istore #6
    //   722: iconst_0
    //   723: istore #13
    //   725: iload #6
    //   727: istore #7
    //   729: iload #13
    //   731: istore_3
    //   732: aload_0
    //   733: getfield mIncludePad : Z
    //   736: ifeq -> 768
    //   739: iload #6
    //   741: istore #7
    //   743: iload #13
    //   745: istore_3
    //   746: iload #15
    //   748: ifne -> 768
    //   751: aload #10
    //   753: invokevirtual getTopPadding : ()I
    //   756: istore_3
    //   757: aload_0
    //   758: iload_3
    //   759: putfield mTopPadding : I
    //   762: iload #6
    //   764: iload_3
    //   765: isub
    //   766: istore #7
    //   768: aload_0
    //   769: getfield mIncludePad : Z
    //   772: ifeq -> 807
    //   775: iload #11
    //   777: ifeq -> 807
    //   780: aload #10
    //   782: invokevirtual getBottomPadding : ()I
    //   785: istore #6
    //   787: aload_0
    //   788: iload #6
    //   790: putfield mBottomPadding : I
    //   793: iload #6
    //   795: istore #11
    //   797: iload #7
    //   799: iload #6
    //   801: iadd
    //   802: istore #7
    //   804: goto -> 810
    //   807: iconst_0
    //   808: istore #11
    //   810: aload_0
    //   811: getfield mInts : Landroid/text/PackedIntVector;
    //   814: iload #15
    //   816: iconst_0
    //   817: iload_2
    //   818: iload #14
    //   820: isub
    //   821: invokevirtual adjustValuesBelow : (III)V
    //   824: aload_0
    //   825: getfield mInts : Landroid/text/PackedIntVector;
    //   828: iload #15
    //   830: iconst_1
    //   831: iload #12
    //   833: iload #16
    //   835: isub
    //   836: iload #7
    //   838: iadd
    //   839: invokevirtual adjustValuesBelow : (III)V
    //   842: aload_0
    //   843: getfield mEllipsize : Z
    //   846: ifeq -> 864
    //   849: bipush #7
    //   851: newarray int
    //   853: astore #17
    //   855: aload #17
    //   857: iconst_5
    //   858: ldc -2147483648
    //   860: iastore
    //   861: goto -> 869
    //   864: iconst_5
    //   865: newarray int
    //   867: astore #17
    //   869: iconst_1
    //   870: anewarray android/text/Layout$Directions
    //   873: astore #19
    //   875: iconst_0
    //   876: istore #14
    //   878: iload #14
    //   880: iload #9
    //   882: if_icmpge -> 1208
    //   885: aload #10
    //   887: iload #14
    //   889: invokevirtual getLineStart : (I)I
    //   892: istore #16
    //   894: aload #17
    //   896: iconst_0
    //   897: iload #16
    //   899: iastore
    //   900: aload #17
    //   902: iconst_0
    //   903: aload #17
    //   905: iconst_0
    //   906: iaload
    //   907: aload #10
    //   909: iload #14
    //   911: invokevirtual getParagraphDirection : (I)I
    //   914: bipush #30
    //   916: ishl
    //   917: ior
    //   918: iastore
    //   919: aload #17
    //   921: iconst_0
    //   922: iaload
    //   923: istore #13
    //   925: aload #10
    //   927: iload #14
    //   929: invokevirtual getLineContainsTab : (I)Z
    //   932: ifeq -> 942
    //   935: ldc 536870912
    //   937: istore #6
    //   939: goto -> 945
    //   942: iconst_0
    //   943: istore #6
    //   945: aload #17
    //   947: iconst_0
    //   948: iload #13
    //   950: iload #6
    //   952: ior
    //   953: iastore
    //   954: aload #10
    //   956: iload #14
    //   958: invokevirtual getLineTop : (I)I
    //   961: iload #12
    //   963: iadd
    //   964: istore #13
    //   966: iload #13
    //   968: istore #6
    //   970: iload #14
    //   972: ifle -> 981
    //   975: iload #13
    //   977: iload_3
    //   978: isub
    //   979: istore #6
    //   981: aload #17
    //   983: iconst_1
    //   984: iload #6
    //   986: iastore
    //   987: aload #10
    //   989: iload #14
    //   991: invokevirtual getLineDescent : (I)I
    //   994: istore #13
    //   996: iload #13
    //   998: istore #6
    //   1000: iload #14
    //   1002: iload #9
    //   1004: iconst_1
    //   1005: isub
    //   1006: if_icmpne -> 1016
    //   1009: iload #13
    //   1011: iload #11
    //   1013: iadd
    //   1014: istore #6
    //   1016: aload #17
    //   1018: iconst_2
    //   1019: iload #6
    //   1021: iastore
    //   1022: aload #17
    //   1024: iconst_3
    //   1025: aload #10
    //   1027: iload #14
    //   1029: invokevirtual getLineExtra : (I)I
    //   1032: iastore
    //   1033: aload #19
    //   1035: iconst_0
    //   1036: aload #10
    //   1038: iload #14
    //   1040: invokevirtual getLineDirections : (I)Landroid/text/Layout$Directions;
    //   1043: aastore
    //   1044: iload #14
    //   1046: iload #9
    //   1048: iconst_1
    //   1049: isub
    //   1050: if_icmpne -> 1062
    //   1053: iload #4
    //   1055: iload_2
    //   1056: iadd
    //   1057: istore #6
    //   1059: goto -> 1073
    //   1062: aload #10
    //   1064: iload #14
    //   1066: iconst_1
    //   1067: iadd
    //   1068: invokevirtual getLineStart : (I)I
    //   1071: istore #6
    //   1073: aload #10
    //   1075: iload #14
    //   1077: invokevirtual getStartHyphenEdit : (I)I
    //   1080: istore #13
    //   1082: aload #10
    //   1084: iload #14
    //   1086: invokevirtual getEndHyphenEdit : (I)I
    //   1089: istore #20
    //   1091: aload #17
    //   1093: iconst_4
    //   1094: iload #13
    //   1096: iload #20
    //   1098: invokestatic packHyphenEdit : (II)I
    //   1101: iastore
    //   1102: aload #17
    //   1104: iconst_4
    //   1105: iaload
    //   1106: istore #13
    //   1108: aload_0
    //   1109: aload #5
    //   1111: iload #16
    //   1113: iload #6
    //   1115: invokespecial contentMayProtrudeFromLineTopOrBottom : (Ljava/lang/CharSequence;II)Z
    //   1118: ifeq -> 1129
    //   1121: sipush #256
    //   1124: istore #6
    //   1126: goto -> 1132
    //   1129: iconst_0
    //   1130: istore #6
    //   1132: aload #17
    //   1134: iconst_4
    //   1135: iload #13
    //   1137: iload #6
    //   1139: ior
    //   1140: iastore
    //   1141: aload_0
    //   1142: getfield mEllipsize : Z
    //   1145: ifeq -> 1174
    //   1148: aload #17
    //   1150: iconst_5
    //   1151: aload #10
    //   1153: iload #14
    //   1155: invokevirtual getEllipsisStart : (I)I
    //   1158: iastore
    //   1159: aload #17
    //   1161: bipush #6
    //   1163: aload #10
    //   1165: iload #14
    //   1167: invokevirtual getEllipsisCount : (I)I
    //   1170: iastore
    //   1171: goto -> 1174
    //   1174: aload_0
    //   1175: getfield mInts : Landroid/text/PackedIntVector;
    //   1178: iload #15
    //   1180: iload #14
    //   1182: iadd
    //   1183: aload #17
    //   1185: invokevirtual insertAt : (I[I)V
    //   1188: aload_0
    //   1189: getfield mObjects : Landroid/text/PackedObjectVector;
    //   1192: iload #15
    //   1194: iload #14
    //   1196: iadd
    //   1197: aload #19
    //   1199: invokevirtual insertAt : (I[Ljava/lang/Object;)V
    //   1202: iinc #14, 1
    //   1205: goto -> 878
    //   1208: aload_0
    //   1209: iload #15
    //   1211: iload #8
    //   1213: iconst_1
    //   1214: isub
    //   1215: iload #9
    //   1217: invokevirtual updateBlocks : (III)V
    //   1220: aload_1
    //   1221: invokevirtual finish : ()V
    //   1224: getstatic android/text/DynamicLayout.sLock : [Ljava/lang/Object;
    //   1227: astore #17
    //   1229: aload #17
    //   1231: monitorenter
    //   1232: aload #10
    //   1234: putstatic android/text/DynamicLayout.sStaticLayout : Landroid/text/StaticLayout;
    //   1237: aload_1
    //   1238: putstatic android/text/DynamicLayout.sBuilder : Landroid/text/StaticLayout$Builder;
    //   1241: aload #17
    //   1243: monitorexit
    //   1244: return
    //   1245: astore_1
    //   1246: aload #17
    //   1248: monitorexit
    //   1249: aload_1
    //   1250: athrow
    //   1251: astore_1
    //   1252: aload #17
    //   1254: monitorexit
    //   1255: aload_1
    //   1256: athrow
    //   1257: astore_1
    //   1258: goto -> 1252
    // Line number table:
    //   Java source line number -> byte code offset
    //   #502	-> 0
    //   #503	-> 8
    //   #505	-> 9
    //   #506	-> 15
    //   #510	-> 24
    //   #511	-> 36
    //   #512	-> 41
    //   #514	-> 47
    //   #517	-> 50
    //   #518	-> 56
    //   #519	-> 56
    //   #520	-> 63
    //   #525	-> 68
    //   #526	-> 81
    //   #527	-> 86
    //   #529	-> 93
    //   #531	-> 96
    //   #532	-> 105
    //   #533	-> 114
    //   #537	-> 121
    //   #538	-> 129
    //   #542	-> 142
    //   #544	-> 145
    //   #547	-> 162
    //   #548	-> 181
    //   #549	-> 194
    //   #551	-> 207
    //   #552	-> 224
    //   #554	-> 227
    //   #555	-> 234
    //   #556	-> 240
    //   #557	-> 246
    //   #560	-> 253
    //   #561	-> 269
    //   #563	-> 269
    //   #564	-> 278
    //   #565	-> 283
    //   #547	-> 291
    //   #568	-> 301
    //   #537	-> 324
    //   #573	-> 336
    //   #574	-> 344
    //   #576	-> 352
    //   #577	-> 363
    //   #578	-> 372
    //   #577	-> 381
    //   #579	-> 381
    //   #580	-> 389
    //   #587	-> 407
    //   #588	-> 415
    //   #589	-> 420
    //   #590	-> 424
    //   #591	-> 428
    //   #592	-> 432
    //   #594	-> 435
    //   #595	-> 440
    //   #596	-> 450
    //   #594	-> 473
    //   #599	-> 473
    //   #600	-> 487
    //   #601	-> 498
    //   #602	-> 509
    //   #603	-> 520
    //   #604	-> 541
    //   #605	-> 555
    //   #606	-> 569
    //   #607	-> 583
    //   #608	-> 596
    //   #609	-> 609
    //   #610	-> 631
    //   #612	-> 639
    //   #613	-> 647
    //   #618	-> 654
    //   #619	-> 679
    //   #622	-> 685
    //   #623	-> 699
    //   #627	-> 713
    //   #628	-> 722
    //   #630	-> 725
    //   #631	-> 751
    //   #632	-> 757
    //   #633	-> 762
    //   #635	-> 768
    //   #636	-> 780
    //   #637	-> 787
    //   #638	-> 793
    //   #641	-> 807
    //   #642	-> 824
    //   #648	-> 842
    //   #649	-> 849
    //   #650	-> 855
    //   #652	-> 864
    //   #655	-> 869
    //   #657	-> 875
    //   #658	-> 885
    //   #659	-> 894
    //   #660	-> 900
    //   #661	-> 919
    //   #663	-> 954
    //   #664	-> 966
    //   #665	-> 975
    //   #666	-> 981
    //   #668	-> 987
    //   #669	-> 996
    //   #670	-> 1009
    //   #672	-> 1016
    //   #673	-> 1022
    //   #674	-> 1033
    //   #676	-> 1044
    //   #677	-> 1073
    //   #678	-> 1073
    //   #677	-> 1091
    //   #679	-> 1102
    //   #680	-> 1108
    //   #681	-> 1121
    //   #683	-> 1141
    //   #684	-> 1148
    //   #685	-> 1159
    //   #683	-> 1174
    //   #688	-> 1174
    //   #689	-> 1188
    //   #657	-> 1202
    //   #692	-> 1208
    //   #694	-> 1220
    //   #695	-> 1224
    //   #696	-> 1232
    //   #697	-> 1237
    //   #698	-> 1241
    //   #699	-> 1244
    //   #698	-> 1245
    //   #592	-> 1251
    // Exception table:
    //   from	to	target	type
    //   415	420	1251	finally
    //   420	424	1251	finally
    //   424	428	1251	finally
    //   428	432	1251	finally
    //   432	435	1251	finally
    //   1232	1237	1245	finally
    //   1237	1241	1245	finally
    //   1241	1244	1245	finally
    //   1246	1249	1245	finally
    //   1252	1255	1257	finally
  }
  
  private boolean contentMayProtrudeFromLineTopOrBottom(CharSequence paramCharSequence, int paramInt1, int paramInt2) {
    boolean bool = paramCharSequence instanceof Spanned;
    boolean bool1 = true;
    if (bool) {
      Spanned spanned = (Spanned)paramCharSequence;
      if (((ReplacementSpan[])spanned.getSpans(paramInt1, paramInt2, (Class)ReplacementSpan.class)).length > 0)
        return true; 
    } 
    TextPaint textPaint = getPaint();
    if (paramCharSequence instanceof PrecomputedText) {
      paramCharSequence = paramCharSequence;
      paramCharSequence.getBounds(paramInt1, paramInt2, this.mTempRect);
    } else {
      textPaint.getTextBounds(paramCharSequence, paramInt1, paramInt2, this.mTempRect);
    } 
    Paint.FontMetricsInt fontMetricsInt = textPaint.getFontMetricsInt();
    bool = bool1;
    if (this.mTempRect.top >= fontMetricsInt.top)
      if (this.mTempRect.bottom > fontMetricsInt.bottom) {
        bool = bool1;
      } else {
        bool = false;
      }  
    return bool;
  }
  
  private void createBlocks() {
    int i = 400;
    this.mNumberOfBlocks = 0;
    CharSequence charSequence = this.mDisplay;
    while (true) {
      i = TextUtils.indexOf(charSequence, '\n', i);
      if (i < 0) {
        addBlockAtOffset(charSequence.length());
        this.mBlockIndices = new int[this.mBlockEndLines.length];
        for (i = 0; i < this.mBlockEndLines.length; i++)
          this.mBlockIndices[i] = -1; 
        return;
      } 
      addBlockAtOffset(i);
      i += 400;
    } 
  }
  
  public ArraySet<Integer> getBlocksAlwaysNeedToBeRedrawn() {
    return this.mBlocksAlwaysNeedToBeRedrawn;
  }
  
  private void updateAlwaysNeedsToBeRedrawn(int paramInt) {
    int i;
    if (paramInt == 0) {
      i = 0;
    } else {
      i = this.mBlockEndLines[paramInt - 1] + 1;
    } 
    int j = this.mBlockEndLines[paramInt];
    for (; i <= j; i++) {
      if (getContentMayProtrudeFromTopOrBottom(i)) {
        if (this.mBlocksAlwaysNeedToBeRedrawn == null)
          this.mBlocksAlwaysNeedToBeRedrawn = new ArraySet<>(); 
        this.mBlocksAlwaysNeedToBeRedrawn.add(Integer.valueOf(paramInt));
        return;
      } 
    } 
    ArraySet<Integer> arraySet = this.mBlocksAlwaysNeedToBeRedrawn;
    if (arraySet != null)
      arraySet.remove(Integer.valueOf(paramInt)); 
  }
  
  private void addBlockAtOffset(int paramInt) {
    paramInt = getLineForOffset(paramInt);
    int[] arrayOfInt = this.mBlockEndLines;
    if (arrayOfInt == null) {
      this.mBlockEndLines = arrayOfInt = ArrayUtils.newUnpaddedIntArray(1);
      int k = this.mNumberOfBlocks;
      arrayOfInt[k] = paramInt;
      updateAlwaysNeedsToBeRedrawn(k);
      this.mNumberOfBlocks++;
      return;
    } 
    int j = this.mNumberOfBlocks, i = arrayOfInt[j - 1];
    if (paramInt > i) {
      this.mBlockEndLines = GrowingArrayUtils.append(arrayOfInt, j, paramInt);
      updateAlwaysNeedsToBeRedrawn(this.mNumberOfBlocks);
      this.mNumberOfBlocks++;
    } 
  }
  
  public void updateBlocks(int paramInt1, int paramInt2, int paramInt3) {
    boolean bool1, bool2;
    if (this.mBlockEndLines == null) {
      createBlocks();
      return;
    } 
    byte b = -1;
    int i = -1;
    int j = 0;
    while (true) {
      k = b;
      if (j < this.mNumberOfBlocks) {
        if (this.mBlockEndLines[j] >= paramInt1) {
          k = j;
          break;
        } 
        j++;
        continue;
      } 
      break;
    } 
    j = k;
    while (true) {
      m = i;
      if (j < this.mNumberOfBlocks) {
        if (this.mBlockEndLines[j] >= paramInt2) {
          m = j;
          break;
        } 
        j++;
        continue;
      } 
      break;
    } 
    int arrayOfInt[] = this.mBlockEndLines, n = arrayOfInt[m];
    if (k == 0) {
      j = 0;
    } else {
      j = arrayOfInt[k - 1] + 1;
    } 
    if (paramInt1 > j) {
      b = 1;
    } else {
      b = 0;
    } 
    if (paramInt3 > 0) {
      bool1 = true;
    } else {
      bool1 = false;
    } 
    if (paramInt2 < this.mBlockEndLines[m]) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    i = 0;
    if (b != 0)
      i = 0 + 1; 
    j = i;
    if (bool1)
      j = i + 1; 
    i = j;
    if (bool2)
      i = j + 1; 
    int i1 = m - k + 1;
    j = this.mNumberOfBlocks;
    int i2 = j + i - i1;
    if (i2 == 0) {
      this.mBlockEndLines[0] = 0;
      this.mBlockIndices[0] = -1;
      this.mNumberOfBlocks = 1;
      return;
    } 
    arrayOfInt = this.mBlockEndLines;
    if (i2 > arrayOfInt.length) {
      j = arrayOfInt.length;
      j = Math.max(j * 2, i2);
      int[] arrayOfInt1 = ArrayUtils.newUnpaddedIntArray(j);
      arrayOfInt = new int[arrayOfInt1.length];
      System.arraycopy(this.mBlockEndLines, 0, arrayOfInt1, 0, k);
      System.arraycopy(this.mBlockIndices, 0, arrayOfInt, 0, k);
      System.arraycopy(this.mBlockEndLines, m + 1, arrayOfInt1, k + i, this.mNumberOfBlocks - m - 1);
      System.arraycopy(this.mBlockIndices, m + 1, arrayOfInt, k + i, this.mNumberOfBlocks - m - 1);
      this.mBlockEndLines = arrayOfInt1;
      this.mBlockIndices = arrayOfInt;
    } else if (i + i1 != 0) {
      System.arraycopy(arrayOfInt, m + 1, arrayOfInt, k + i, j - m - 1);
      arrayOfInt = this.mBlockIndices;
      System.arraycopy(arrayOfInt, m + 1, arrayOfInt, k + i, this.mNumberOfBlocks - m - 1);
    } 
    if (i + i1 != 0 && this.mBlocksAlwaysNeedToBeRedrawn != null) {
      ArraySet<Integer> arraySet = new ArraySet();
      for (j = 0; j < this.mBlocksAlwaysNeedToBeRedrawn.size(); j++) {
        Integer integer = this.mBlocksAlwaysNeedToBeRedrawn.valueAt(j);
        if (integer.intValue() < k)
          arraySet.add(integer); 
        if (integer.intValue() > m) {
          int i3 = integer.intValue();
          arraySet.add(Integer.valueOf(i3 + i - i1));
        } 
      } 
      this.mBlocksAlwaysNeedToBeRedrawn = arraySet;
    } 
    this.mNumberOfBlocks = i2;
    int m = paramInt3 - paramInt2 - paramInt1 + 1;
    if (m != 0) {
      paramInt2 = k + i;
      for (j = paramInt2; j < this.mNumberOfBlocks; j++) {
        arrayOfInt = this.mBlockEndLines;
        arrayOfInt[j] = arrayOfInt[j] + m;
      } 
    } else {
      paramInt2 = this.mNumberOfBlocks;
    } 
    this.mIndexFirstChangedBlock = Math.min(this.mIndexFirstChangedBlock, paramInt2);
    paramInt2 = k;
    if (b != 0) {
      this.mBlockEndLines[k] = paramInt1 - 1;
      updateAlwaysNeedsToBeRedrawn(k);
      this.mBlockIndices[k] = -1;
      paramInt2 = k + 1;
    } 
    int k = paramInt2;
    if (bool1) {
      this.mBlockEndLines[paramInt2] = paramInt1 + paramInt3 - 1;
      updateAlwaysNeedsToBeRedrawn(paramInt2);
      this.mBlockIndices[paramInt2] = -1;
      k = paramInt2 + 1;
    } 
    if (bool2) {
      this.mBlockEndLines[k] = n + m;
      updateAlwaysNeedsToBeRedrawn(k);
      this.mBlockIndices[k] = -1;
    } 
  }
  
  public void setBlocksDataForTest(int[] paramArrayOfint1, int[] paramArrayOfint2, int paramInt1, int paramInt2) {
    int[] arrayOfInt = new int[paramArrayOfint1.length];
    this.mBlockIndices = new int[paramArrayOfint2.length];
    System.arraycopy(paramArrayOfint1, 0, arrayOfInt, 0, paramArrayOfint1.length);
    System.arraycopy(paramArrayOfint2, 0, this.mBlockIndices, 0, paramArrayOfint2.length);
    this.mNumberOfBlocks = paramInt1;
    while (this.mInts.size() < paramInt2) {
      PackedIntVector packedIntVector = this.mInts;
      packedIntVector.insertAt(packedIntVector.size(), new int[5]);
    } 
  }
  
  public int[] getBlockEndLines() {
    return this.mBlockEndLines;
  }
  
  public int[] getBlockIndices() {
    return this.mBlockIndices;
  }
  
  public int getBlockIndex(int paramInt) {
    return this.mBlockIndices[paramInt];
  }
  
  public void setBlockIndex(int paramInt1, int paramInt2) {
    this.mBlockIndices[paramInt1] = paramInt2;
  }
  
  public int getNumberOfBlocks() {
    return this.mNumberOfBlocks;
  }
  
  public int getIndexFirstChangedBlock() {
    return this.mIndexFirstChangedBlock;
  }
  
  public void setIndexFirstChangedBlock(int paramInt) {
    this.mIndexFirstChangedBlock = paramInt;
  }
  
  public int getLineCount() {
    return this.mInts.size() - 1;
  }
  
  public int getLineTop(int paramInt) {
    return this.mInts.getValue(paramInt, 1);
  }
  
  public int getLineDescent(int paramInt) {
    return this.mInts.getValue(paramInt, 2);
  }
  
  public int getLineExtra(int paramInt) {
    return this.mInts.getValue(paramInt, 3);
  }
  
  public int getLineStart(int paramInt) {
    return this.mInts.getValue(paramInt, 0) & 0x1FFFFFFF;
  }
  
  public boolean getLineContainsTab(int paramInt) {
    PackedIntVector packedIntVector = this.mInts;
    boolean bool = false;
    if ((packedIntVector.getValue(paramInt, 0) & 0x20000000) != 0)
      bool = true; 
    return bool;
  }
  
  public int getParagraphDirection(int paramInt) {
    return this.mInts.getValue(paramInt, 0) >> 30;
  }
  
  public final Layout.Directions getLineDirections(int paramInt) {
    return this.mObjects.getValue(paramInt, 0);
  }
  
  public int getTopPadding() {
    return this.mTopPadding;
  }
  
  public int getBottomPadding() {
    return this.mBottomPadding;
  }
  
  public int getStartHyphenEdit(int paramInt) {
    return StaticLayout.unpackStartHyphenEdit(this.mInts.getValue(paramInt, 4) & 0xFF);
  }
  
  public int getEndHyphenEdit(int paramInt) {
    return StaticLayout.unpackEndHyphenEdit(this.mInts.getValue(paramInt, 4) & 0xFF);
  }
  
  private boolean getContentMayProtrudeFromTopOrBottom(int paramInt) {
    boolean bool;
    if ((this.mInts.getValue(paramInt, 4) & 0x100) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int getEllipsizedWidth() {
    return this.mEllipsizedWidth;
  }
  
  class ChangeWatcher implements TextWatcher, SpanWatcher {
    private WeakReference<DynamicLayout> mLayout;
    
    public ChangeWatcher(DynamicLayout this$0) {
      this.mLayout = new WeakReference<>(this$0);
    }
    
    private void reflow(CharSequence param1CharSequence, int param1Int1, int param1Int2, int param1Int3) {
      DynamicLayout dynamicLayout = this.mLayout.get();
      if (dynamicLayout != null) {
        dynamicLayout.reflow(param1CharSequence, param1Int1, param1Int2, param1Int3);
      } else if (param1CharSequence instanceof Spannable) {
        ((Spannable)param1CharSequence).removeSpan(this);
      } 
    }
    
    public void beforeTextChanged(CharSequence param1CharSequence, int param1Int1, int param1Int2, int param1Int3) {}
    
    public void onTextChanged(CharSequence param1CharSequence, int param1Int1, int param1Int2, int param1Int3) {
      reflow(param1CharSequence, param1Int1, param1Int2, param1Int3);
    }
    
    public void afterTextChanged(Editable param1Editable) {}
    
    public void onSpanAdded(Spannable param1Spannable, Object param1Object, int param1Int1, int param1Int2) {
      if (param1Object instanceof android.text.style.UpdateLayout)
        reflow(param1Spannable, param1Int1, param1Int2 - param1Int1, param1Int2 - param1Int1); 
    }
    
    public void onSpanRemoved(Spannable param1Spannable, Object param1Object, int param1Int1, int param1Int2) {
      if (param1Object instanceof android.text.style.UpdateLayout)
        reflow(param1Spannable, param1Int1, param1Int2 - param1Int1, param1Int2 - param1Int1); 
    }
    
    public void onSpanChanged(Spannable param1Spannable, Object param1Object, int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      if (param1Object instanceof android.text.style.UpdateLayout) {
        int i = param1Int1;
        if (param1Int1 > param1Int2)
          i = 0; 
        reflow(param1Spannable, i, param1Int2 - i, param1Int2 - i);
        reflow(param1Spannable, param1Int3, param1Int4 - param1Int3, param1Int4 - param1Int3);
      } 
    }
  }
  
  public int getEllipsisStart(int paramInt) {
    if (this.mEllipsizeAt == null)
      return 0; 
    return this.mInts.getValue(paramInt, 5);
  }
  
  public int getEllipsisCount(int paramInt) {
    if (this.mEllipsizeAt == null)
      return 0; 
    return this.mInts.getValue(paramInt, 6);
  }
  
  private static StaticLayout sStaticLayout = null;
  
  private CharSequence mBase;
  
  private int[] mBlockEndLines;
  
  private int[] mBlockIndices;
  
  private ArraySet<Integer> mBlocksAlwaysNeedToBeRedrawn;
  
  private int mBottomPadding;
  
  private int mBreakStrategy;
  
  private CharSequence mDisplay;
  
  private boolean mEllipsize;
  
  private TextUtils.TruncateAt mEllipsizeAt;
  
  private int mEllipsizedWidth;
  
  private boolean mFallbackLineSpacing;
  
  private int mHyphenationFrequency;
  
  private boolean mIncludePad;
  
  private int mIndexFirstChangedBlock;
  
  private PackedIntVector mInts;
  
  private int mJustificationMode;
  
  private int mNumberOfBlocks;
  
  private PackedObjectVector<Layout.Directions> mObjects;
  
  private Rect mTempRect;
  
  private int mTopPadding;
  
  private ChangeWatcher mWatcher;
  
  static {
    sBuilder = null;
    sLock = new Object[0];
  }
}
