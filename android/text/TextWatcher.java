package android.text;

public interface TextWatcher extends NoCopySpan {
  void afterTextChanged(Editable paramEditable);
  
  void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3);
  
  void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3);
}
