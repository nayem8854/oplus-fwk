package android.text;

import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.LocaleList;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Objects;

public class PrecomputedText implements Spannable {
  private static final char LINE_FEED = '\n';
  
  private final int mEnd;
  
  private final ParagraphInfo[] mParagraphInfo;
  
  private final Params mParams;
  
  private final int mStart;
  
  private final SpannableString mText;
  
  class Params {
    public static final int NEED_RECOMPUTE = 1;
    
    public static final int UNUSABLE = 0;
    
    public static final int USABLE = 2;
    
    private final int mBreakStrategy;
    
    private final int mHyphenationFrequency;
    
    private final TextPaint mPaint;
    
    private final TextDirectionHeuristic mTextDir;
    
    public static class Builder {
      private TextDirectionHeuristic mTextDir = TextDirectionHeuristics.FIRSTSTRONG_LTR;
      
      private int mBreakStrategy = 1;
      
      private int mHyphenationFrequency = 1;
      
      private final TextPaint mPaint;
      
      public Builder(TextPaint param2TextPaint) {
        this.mPaint = param2TextPaint;
      }
      
      public Builder(PrecomputedText.Params param2Params) {
        this.mPaint = param2Params.mPaint;
        this.mTextDir = param2Params.mTextDir;
        this.mBreakStrategy = param2Params.mBreakStrategy;
        this.mHyphenationFrequency = param2Params.mHyphenationFrequency;
      }
      
      public Builder setBreakStrategy(int param2Int) {
        this.mBreakStrategy = param2Int;
        return this;
      }
      
      public Builder setHyphenationFrequency(int param2Int) {
        this.mHyphenationFrequency = param2Int;
        return this;
      }
      
      public Builder setTextDirection(TextDirectionHeuristic param2TextDirectionHeuristic) {
        this.mTextDir = param2TextDirectionHeuristic;
        return this;
      }
      
      public PrecomputedText.Params build() {
        return new PrecomputedText.Params(this.mPaint, this.mTextDir, this.mBreakStrategy, this.mHyphenationFrequency);
      }
    }
    
    public Params(PrecomputedText this$0, TextDirectionHeuristic param1TextDirectionHeuristic, int param1Int1, int param1Int2) {
      this.mPaint = (TextPaint)this$0;
      this.mTextDir = param1TextDirectionHeuristic;
      this.mBreakStrategy = param1Int1;
      this.mHyphenationFrequency = param1Int2;
    }
    
    public TextPaint getTextPaint() {
      return this.mPaint;
    }
    
    public TextDirectionHeuristic getTextDirection() {
      return this.mTextDir;
    }
    
    public int getBreakStrategy() {
      return this.mBreakStrategy;
    }
    
    public int getHyphenationFrequency() {
      return this.mHyphenationFrequency;
    }
    
    public int checkResultUsable(TextPaint param1TextPaint, TextDirectionHeuristic param1TextDirectionHeuristic, int param1Int1, int param1Int2) {
      if (this.mBreakStrategy == param1Int1 && this.mHyphenationFrequency == param1Int2) {
        TextPaint textPaint = this.mPaint;
        if (textPaint.equalsForTextMeasurement(param1TextPaint)) {
          if (this.mTextDir == param1TextDirectionHeuristic) {
            param1Int1 = 2;
          } else {
            param1Int1 = 1;
          } 
          return param1Int1;
        } 
      } 
      return 0;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = true;
      if (param1Object == this)
        return true; 
      if (param1Object == null || !(param1Object instanceof Params))
        return false; 
      param1Object = param1Object;
      if (checkResultUsable(((Params)param1Object).mPaint, ((Params)param1Object).mTextDir, ((Params)param1Object).mBreakStrategy, ((Params)param1Object).mHyphenationFrequency) != 2)
        bool = false; 
      return bool;
    }
    
    public int hashCode() {
      float f1 = this.mPaint.getTextSize(), f2 = this.mPaint.getTextScaleX(), f3 = this.mPaint.getTextSkewX();
      TextPaint textPaint1 = this.mPaint;
      float f4 = textPaint1.getLetterSpacing(), f5 = this.mPaint.getWordSpacing();
      int i = this.mPaint.getFlags();
      textPaint1 = this.mPaint;
      LocaleList localeList = textPaint1.getTextLocales();
      Typeface typeface = this.mPaint.getTypeface();
      TextPaint textPaint2 = this.mPaint;
      String str = textPaint2.getFontVariationSettings();
      boolean bool = this.mPaint.isElegantTextHeight();
      TextDirectionHeuristic textDirectionHeuristic = this.mTextDir;
      int j = this.mBreakStrategy;
      int k = this.mHyphenationFrequency;
      return Objects.hash(new Object[] { 
            Float.valueOf(f1), Float.valueOf(f2), Float.valueOf(f3), Float.valueOf(f4), Float.valueOf(f5), Integer.valueOf(i), localeList, typeface, str, Boolean.valueOf(bool), 
            textDirectionHeuristic, Integer.valueOf(j), Integer.valueOf(k) });
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("{textSize=");
      TextPaint textPaint = this.mPaint;
      stringBuilder.append(textPaint.getTextSize());
      stringBuilder.append(", textScaleX=");
      textPaint = this.mPaint;
      stringBuilder.append(textPaint.getTextScaleX());
      stringBuilder.append(", textSkewX=");
      textPaint = this.mPaint;
      stringBuilder.append(textPaint.getTextSkewX());
      stringBuilder.append(", letterSpacing=");
      textPaint = this.mPaint;
      stringBuilder.append(textPaint.getLetterSpacing());
      stringBuilder.append(", textLocale=");
      textPaint = this.mPaint;
      stringBuilder.append(textPaint.getTextLocales());
      stringBuilder.append(", typeface=");
      textPaint = this.mPaint;
      stringBuilder.append(textPaint.getTypeface());
      stringBuilder.append(", variationSettings=");
      textPaint = this.mPaint;
      stringBuilder.append(textPaint.getFontVariationSettings());
      stringBuilder.append(", elegantTextHeight=");
      textPaint = this.mPaint;
      stringBuilder.append(textPaint.isElegantTextHeight());
      stringBuilder.append(", textDir=");
      stringBuilder.append(this.mTextDir);
      stringBuilder.append(", breakStrategy=");
      stringBuilder.append(this.mBreakStrategy);
      stringBuilder.append(", hyphenationFrequency=");
      stringBuilder.append(this.mHyphenationFrequency);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public static @interface CheckResultUsableResult {}
  }
  
  class ParagraphInfo {
    public final MeasuredParagraph measured;
    
    public final int paragraphEnd;
    
    public ParagraphInfo(PrecomputedText this$0, MeasuredParagraph param1MeasuredParagraph) {
      this.paragraphEnd = this$0;
      this.measured = param1MeasuredParagraph;
    }
  }
  
  public static PrecomputedText create(CharSequence paramCharSequence, Params paramParams) {
    ParagraphInfo[] arrayOfParagraphInfo2;
    TextPaint textPaint1 = null;
    TextPaint textPaint2 = textPaint1;
    if (paramCharSequence instanceof PrecomputedText) {
      PrecomputedText precomputedText = (PrecomputedText)paramCharSequence;
      Params params = precomputedText.getParams();
      textPaint2 = paramParams.mPaint;
      TextDirectionHeuristic textDirectionHeuristic = paramParams.mTextDir;
      int i = paramParams.mBreakStrategy, j = paramParams.mHyphenationFrequency;
      i = params.checkResultUsable(textPaint2, textDirectionHeuristic, i, j);
      if (i != 1) {
        if (i != 2) {
          textPaint2 = textPaint1;
        } else {
          return precomputedText;
        } 
      } else {
        textPaint2 = textPaint1;
        if (paramParams.getBreakStrategy() == params.getBreakStrategy()) {
          i = paramParams.getHyphenationFrequency();
          textPaint2 = textPaint1;
          if (i == params.getHyphenationFrequency())
            arrayOfParagraphInfo2 = createMeasuredParagraphsFromPrecomputedText(precomputedText, paramParams, true); 
        } 
      } 
    } 
    ParagraphInfo[] arrayOfParagraphInfo1 = arrayOfParagraphInfo2;
    if (arrayOfParagraphInfo2 == null) {
      int i = paramCharSequence.length();
      arrayOfParagraphInfo1 = createMeasuredParagraphs(paramCharSequence, paramParams, 0, i, true);
    } 
    return new PrecomputedText(paramCharSequence, 0, paramCharSequence.length(), paramParams, arrayOfParagraphInfo1);
  }
  
  private static ParagraphInfo[] createMeasuredParagraphsFromPrecomputedText(PrecomputedText paramPrecomputedText, Params paramParams, boolean paramBoolean) {
    boolean bool;
    if (paramParams.getBreakStrategy() != 0 && 
      paramParams.getHyphenationFrequency() != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    ArrayList<ParagraphInfo> arrayList = new ArrayList();
    for (byte b = 0; b < paramPrecomputedText.getParagraphCount(); b++) {
      int i = paramPrecomputedText.getParagraphStart(b);
      int j = paramPrecomputedText.getParagraphEnd(b);
      TextPaint textPaint = paramParams.getTextPaint();
      TextDirectionHeuristic textDirectionHeuristic = paramParams.getTextDirection();
      MeasuredParagraph measuredParagraph = paramPrecomputedText.getMeasuredParagraph(b);
      arrayList.add(new ParagraphInfo(j, MeasuredParagraph.buildForStaticLayout(textPaint, paramPrecomputedText, i, j, textDirectionHeuristic, bool, paramBoolean, measuredParagraph, null)));
    } 
    return arrayList.<ParagraphInfo>toArray(new ParagraphInfo[arrayList.size()]);
  }
  
  public static ParagraphInfo[] createMeasuredParagraphs(CharSequence paramCharSequence, Params paramParams, int paramInt1, int paramInt2, boolean paramBoolean) {
    boolean bool;
    ArrayList<ParagraphInfo> arrayList = new ArrayList();
    Preconditions.checkNotNull(paramCharSequence);
    Preconditions.checkNotNull(paramParams);
    if (paramParams.getBreakStrategy() != 0 && 
      paramParams.getHyphenationFrequency() != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    int i;
    for (i = paramInt1; i < paramInt2; i = paramInt1) {
      paramInt1 = TextUtils.indexOf(paramCharSequence, '\n', i, paramInt2);
      if (paramInt1 < 0) {
        paramInt1 = paramInt2;
      } else {
        paramInt1++;
      } 
      TextPaint textPaint = paramParams.getTextPaint();
      TextDirectionHeuristic textDirectionHeuristic = paramParams.getTextDirection();
      arrayList.add(new ParagraphInfo(paramInt1, MeasuredParagraph.buildForStaticLayout(textPaint, paramCharSequence, i, paramInt1, textDirectionHeuristic, bool, paramBoolean, null, null)));
    } 
    return arrayList.<ParagraphInfo>toArray(new ParagraphInfo[arrayList.size()]);
  }
  
  private PrecomputedText(CharSequence paramCharSequence, int paramInt1, int paramInt2, Params paramParams, ParagraphInfo[] paramArrayOfParagraphInfo) {
    this.mText = new SpannableString(paramCharSequence, true);
    this.mStart = paramInt1;
    this.mEnd = paramInt2;
    this.mParams = paramParams;
    this.mParagraphInfo = paramArrayOfParagraphInfo;
  }
  
  public CharSequence getText() {
    return this.mText;
  }
  
  public int getStart() {
    return this.mStart;
  }
  
  public int getEnd() {
    return this.mEnd;
  }
  
  public Params getParams() {
    return this.mParams;
  }
  
  public int getParagraphCount() {
    return this.mParagraphInfo.length;
  }
  
  public int getParagraphStart(int paramInt) {
    Preconditions.checkArgumentInRange(paramInt, 0, getParagraphCount(), "paraIndex");
    if (paramInt == 0) {
      paramInt = this.mStart;
    } else {
      paramInt = getParagraphEnd(paramInt - 1);
    } 
    return paramInt;
  }
  
  public int getParagraphEnd(int paramInt) {
    Preconditions.checkArgumentInRange(paramInt, 0, getParagraphCount(), "paraIndex");
    return (this.mParagraphInfo[paramInt]).paragraphEnd;
  }
  
  public MeasuredParagraph getMeasuredParagraph(int paramInt) {
    return (this.mParagraphInfo[paramInt]).measured;
  }
  
  public ParagraphInfo[] getParagraphInfo() {
    return this.mParagraphInfo;
  }
  
  public int checkResultUsable(int paramInt1, int paramInt2, TextDirectionHeuristic paramTextDirectionHeuristic, TextPaint paramTextPaint, int paramInt3, int paramInt4) {
    if (this.mStart != paramInt1 || this.mEnd != paramInt2)
      return 0; 
    return this.mParams.checkResultUsable(paramTextPaint, paramTextDirectionHeuristic, paramInt3, paramInt4);
  }
  
  public int findParaIndex(int paramInt) {
    byte b = 0;
    while (true) {
      ParagraphInfo[] arrayOfParagraphInfo1 = this.mParagraphInfo;
      if (b < arrayOfParagraphInfo1.length) {
        if (paramInt < (arrayOfParagraphInfo1[b]).paragraphEnd)
          return b; 
        b++;
        continue;
      } 
      break;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("pos must be less than ");
    ParagraphInfo[] arrayOfParagraphInfo = this.mParagraphInfo;
    stringBuilder.append((arrayOfParagraphInfo[arrayOfParagraphInfo.length - 1]).paragraphEnd);
    stringBuilder.append(", gave ");
    stringBuilder.append(paramInt);
    throw new IndexOutOfBoundsException(stringBuilder.toString());
  }
  
  public float getWidth(int paramInt1, int paramInt2) {
    boolean bool2, bool1 = true;
    if (paramInt1 >= 0 && paramInt1 <= this.mText.length()) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    Preconditions.checkArgument(bool2, "invalid start offset");
    if (paramInt2 >= 0 && paramInt2 <= this.mText.length()) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    Preconditions.checkArgument(bool2, "invalid end offset");
    if (paramInt1 <= paramInt2) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    Preconditions.checkArgument(bool2, "start offset can not be larger than end offset");
    if (paramInt1 == paramInt2)
      return 0.0F; 
    int i = findParaIndex(paramInt1);
    int j = getParagraphStart(i);
    int k = getParagraphEnd(i);
    if (paramInt1 >= j && k >= paramInt2)
      return getMeasuredParagraph(i).getWidth(paramInt1 - j, paramInt2 - j); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Cannot measured across the paragraph:para: (");
    stringBuilder.append(j);
    stringBuilder.append(", ");
    stringBuilder.append(k);
    stringBuilder.append("), request: (");
    stringBuilder.append(paramInt1);
    stringBuilder.append(", ");
    stringBuilder.append(paramInt2);
    stringBuilder.append(")");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public void getBounds(int paramInt1, int paramInt2, Rect paramRect) {
    boolean bool2, bool1 = true;
    if (paramInt1 >= 0 && paramInt1 <= this.mText.length()) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    Preconditions.checkArgument(bool2, "invalid start offset");
    if (paramInt2 >= 0 && paramInt2 <= this.mText.length()) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    Preconditions.checkArgument(bool2, "invalid end offset");
    if (paramInt1 <= paramInt2) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    Preconditions.checkArgument(bool2, "start offset can not be larger than end offset");
    Preconditions.checkNotNull(paramRect);
    if (paramInt1 == paramInt2) {
      paramRect.set(0, 0, 0, 0);
      return;
    } 
    int i = findParaIndex(paramInt1);
    int j = getParagraphStart(i);
    int k = getParagraphEnd(i);
    if (paramInt1 >= j && k >= paramInt2) {
      getMeasuredParagraph(i).getBounds(paramInt1 - j, paramInt2 - j, paramRect);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Cannot measured across the paragraph:para: (");
    stringBuilder.append(j);
    stringBuilder.append(", ");
    stringBuilder.append(k);
    stringBuilder.append("), request: (");
    stringBuilder.append(paramInt1);
    stringBuilder.append(", ");
    stringBuilder.append(paramInt2);
    stringBuilder.append(")");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public float getCharWidthAt(int paramInt) {
    boolean bool;
    if (paramInt >= 0 && paramInt < this.mText.length()) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool, "invalid offset");
    int i = findParaIndex(paramInt);
    int j = getParagraphStart(i);
    getParagraphEnd(i);
    return getMeasuredParagraph(i).getCharWidthAt(paramInt - j);
  }
  
  public int getMemoryUsage() {
    int i = 0;
    for (byte b = 0; b < getParagraphCount(); b++)
      i += getMeasuredParagraph(b).getMemoryUsage(); 
    return i;
  }
  
  public void setSpan(Object paramObject, int paramInt1, int paramInt2, int paramInt3) {
    if (!(paramObject instanceof android.text.style.MetricAffectingSpan)) {
      this.mText.setSpan(paramObject, paramInt1, paramInt2, paramInt3);
      return;
    } 
    throw new IllegalArgumentException("MetricAffectingSpan can not be set to PrecomputedText.");
  }
  
  public void removeSpan(Object paramObject) {
    if (!(paramObject instanceof android.text.style.MetricAffectingSpan)) {
      this.mText.removeSpan(paramObject);
      return;
    } 
    throw new IllegalArgumentException("MetricAffectingSpan can not be removed from PrecomputedText.");
  }
  
  public <T> T[] getSpans(int paramInt1, int paramInt2, Class<T> paramClass) {
    return (T[])this.mText.getSpans(paramInt1, paramInt2, paramClass);
  }
  
  public int getSpanStart(Object paramObject) {
    return this.mText.getSpanStart(paramObject);
  }
  
  public int getSpanEnd(Object paramObject) {
    return this.mText.getSpanEnd(paramObject);
  }
  
  public int getSpanFlags(Object paramObject) {
    return this.mText.getSpanFlags(paramObject);
  }
  
  public int nextSpanTransition(int paramInt1, int paramInt2, Class paramClass) {
    return this.mText.nextSpanTransition(paramInt1, paramInt2, paramClass);
  }
  
  public int length() {
    return this.mText.length();
  }
  
  public char charAt(int paramInt) {
    return this.mText.charAt(paramInt);
  }
  
  public CharSequence subSequence(int paramInt1, int paramInt2) {
    return create(this.mText.subSequence(paramInt1, paramInt2), this.mParams);
  }
  
  public String toString() {
    return this.mText.toString();
  }
}
