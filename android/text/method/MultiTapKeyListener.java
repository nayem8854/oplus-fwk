package android.text.method;

import android.os.Handler;
import android.os.SystemClock;
import android.text.Editable;
import android.text.Selection;
import android.text.SpanWatcher;
import android.text.Spannable;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.View;

public class MultiTapKeyListener extends BaseKeyListener implements SpanWatcher {
  private static MultiTapKeyListener[] sInstance = new MultiTapKeyListener[(TextKeyListener.Capitalize.values()).length * 2];
  
  private static final SparseArray<String> sRecs;
  
  private boolean mAutoText;
  
  private TextKeyListener.Capitalize mCapitalize;
  
  static {
    SparseArray<String> sparseArray = new SparseArray();
    sparseArray.put(8, ".,1!@#$%^&*:/?'=()");
    sRecs.put(9, "abc2ABC");
    sRecs.put(10, "def3DEF");
    sRecs.put(11, "ghi4GHI");
    sRecs.put(12, "jkl5JKL");
    sRecs.put(13, "mno6MNO");
    sRecs.put(14, "pqrs7PQRS");
    sRecs.put(15, "tuv8TUV");
    sRecs.put(16, "wxyz9WXYZ");
    sRecs.put(7, "0+");
    sRecs.put(18, " ");
  }
  
  public MultiTapKeyListener(TextKeyListener.Capitalize paramCapitalize, boolean paramBoolean) {
    this.mCapitalize = paramCapitalize;
    this.mAutoText = paramBoolean;
  }
  
  public static MultiTapKeyListener getInstance(boolean paramBoolean, TextKeyListener.Capitalize paramCapitalize) {
    int i = paramCapitalize.ordinal() * 2 + paramBoolean;
    MultiTapKeyListener[] arrayOfMultiTapKeyListener = sInstance;
    if (arrayOfMultiTapKeyListener[i] == null)
      arrayOfMultiTapKeyListener[i] = new MultiTapKeyListener(paramCapitalize, paramBoolean); 
    return sInstance[i];
  }
  
  public int getInputType() {
    return makeTextContentType(this.mCapitalize, this.mAutoText);
  }
  
  public boolean onKeyDown(View paramView, Editable paramEditable, int paramInt, KeyEvent paramKeyEvent) {
    // Byte code:
    //   0: aload_1
    //   1: ifnull -> 19
    //   4: invokestatic getInstance : ()Landroid/text/method/TextKeyListener;
    //   7: aload_1
    //   8: invokevirtual getContext : ()Landroid/content/Context;
    //   11: invokevirtual getPrefs : (Landroid/content/Context;)I
    //   14: istore #5
    //   16: goto -> 22
    //   19: iconst_0
    //   20: istore #5
    //   22: aload_2
    //   23: invokestatic getSelectionStart : (Ljava/lang/CharSequence;)I
    //   26: istore #6
    //   28: aload_2
    //   29: invokestatic getSelectionEnd : (Ljava/lang/CharSequence;)I
    //   32: istore #7
    //   34: iload #6
    //   36: iload #7
    //   38: invokestatic min : (II)I
    //   41: istore #8
    //   43: iload #6
    //   45: iload #7
    //   47: invokestatic max : (II)I
    //   50: istore #9
    //   52: aload_2
    //   53: getstatic android/text/method/TextKeyListener.ACTIVE : Ljava/lang/Object;
    //   56: invokeinterface getSpanStart : (Ljava/lang/Object;)I
    //   61: istore #6
    //   63: aload_2
    //   64: getstatic android/text/method/TextKeyListener.ACTIVE : Ljava/lang/Object;
    //   67: invokeinterface getSpanEnd : (Ljava/lang/Object;)I
    //   72: istore #7
    //   74: aload_2
    //   75: getstatic android/text/method/TextKeyListener.ACTIVE : Ljava/lang/Object;
    //   78: invokeinterface getSpanFlags : (Ljava/lang/Object;)I
    //   83: ldc -16777216
    //   85: iand
    //   86: bipush #24
    //   88: iushr
    //   89: istore #10
    //   91: iload #6
    //   93: iload #8
    //   95: if_icmpne -> 377
    //   98: iload #7
    //   100: iload #9
    //   102: if_icmpne -> 377
    //   105: iload #9
    //   107: iload #8
    //   109: isub
    //   110: iconst_1
    //   111: if_icmpne -> 377
    //   114: iload #10
    //   116: iflt -> 377
    //   119: getstatic android/text/method/MultiTapKeyListener.sRecs : Landroid/util/SparseArray;
    //   122: astore #11
    //   124: iload #10
    //   126: aload #11
    //   128: invokevirtual size : ()I
    //   131: if_icmpge -> 377
    //   134: iload_3
    //   135: bipush #17
    //   137: if_icmpne -> 240
    //   140: aload_2
    //   141: iload #8
    //   143: invokeinterface charAt : (I)C
    //   148: istore #12
    //   150: iload #12
    //   152: invokestatic isLowerCase : (C)Z
    //   155: ifeq -> 195
    //   158: iload #12
    //   160: invokestatic valueOf : (C)Ljava/lang/String;
    //   163: invokevirtual toUpperCase : ()Ljava/lang/String;
    //   166: astore_1
    //   167: aload_2
    //   168: iload #8
    //   170: iload #9
    //   172: aload_1
    //   173: invokeinterface replace : (IILjava/lang/CharSequence;)Landroid/text/Editable;
    //   178: pop
    //   179: aload_2
    //   180: invokestatic removeTimeouts : (Landroid/text/Spannable;)V
    //   183: new android/text/method/MultiTapKeyListener$Timeout
    //   186: dup
    //   187: aload_0
    //   188: aload_2
    //   189: invokespecial <init> : (Landroid/text/method/MultiTapKeyListener;Landroid/text/Editable;)V
    //   192: pop
    //   193: iconst_1
    //   194: ireturn
    //   195: iload #12
    //   197: invokestatic isUpperCase : (C)Z
    //   200: ifeq -> 240
    //   203: iload #12
    //   205: invokestatic valueOf : (C)Ljava/lang/String;
    //   208: invokevirtual toLowerCase : ()Ljava/lang/String;
    //   211: astore_1
    //   212: aload_2
    //   213: iload #8
    //   215: iload #9
    //   217: aload_1
    //   218: invokeinterface replace : (IILjava/lang/CharSequence;)Landroid/text/Editable;
    //   223: pop
    //   224: aload_2
    //   225: invokestatic removeTimeouts : (Landroid/text/Spannable;)V
    //   228: new android/text/method/MultiTapKeyListener$Timeout
    //   231: dup
    //   232: aload_0
    //   233: aload_2
    //   234: invokespecial <init> : (Landroid/text/method/MultiTapKeyListener;Landroid/text/Editable;)V
    //   237: pop
    //   238: iconst_1
    //   239: ireturn
    //   240: getstatic android/text/method/MultiTapKeyListener.sRecs : Landroid/util/SparseArray;
    //   243: iload_3
    //   244: invokevirtual indexOfKey : (I)I
    //   247: iload #10
    //   249: if_icmpne -> 333
    //   252: getstatic android/text/method/MultiTapKeyListener.sRecs : Landroid/util/SparseArray;
    //   255: iload #10
    //   257: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   260: checkcast java/lang/String
    //   263: astore #11
    //   265: aload_2
    //   266: iload #8
    //   268: invokeinterface charAt : (I)C
    //   273: istore #6
    //   275: aload #11
    //   277: iload #6
    //   279: invokevirtual indexOf : (I)I
    //   282: istore #6
    //   284: iload #6
    //   286: iflt -> 333
    //   289: iload #6
    //   291: iconst_1
    //   292: iadd
    //   293: aload #11
    //   295: invokevirtual length : ()I
    //   298: irem
    //   299: istore_3
    //   300: aload_2
    //   301: iload #8
    //   303: iload #9
    //   305: aload #11
    //   307: iload_3
    //   308: iload_3
    //   309: iconst_1
    //   310: iadd
    //   311: invokeinterface replace : (IILjava/lang/CharSequence;II)Landroid/text/Editable;
    //   316: pop
    //   317: aload_2
    //   318: invokestatic removeTimeouts : (Landroid/text/Spannable;)V
    //   321: new android/text/method/MultiTapKeyListener$Timeout
    //   324: dup
    //   325: aload_0
    //   326: aload_2
    //   327: invokespecial <init> : (Landroid/text/method/MultiTapKeyListener;Landroid/text/Editable;)V
    //   330: pop
    //   331: iconst_1
    //   332: ireturn
    //   333: getstatic android/text/method/MultiTapKeyListener.sRecs : Landroid/util/SparseArray;
    //   336: iload_3
    //   337: invokevirtual indexOfKey : (I)I
    //   340: istore #7
    //   342: iload #7
    //   344: iflt -> 366
    //   347: aload_2
    //   348: iload #9
    //   350: iload #9
    //   352: invokestatic setSelection : (Landroid/text/Spannable;II)V
    //   355: iload #9
    //   357: istore #6
    //   359: iload #7
    //   361: istore #8
    //   363: goto -> 394
    //   366: iload #8
    //   368: istore #6
    //   370: iload #7
    //   372: istore #8
    //   374: goto -> 394
    //   377: getstatic android/text/method/MultiTapKeyListener.sRecs : Landroid/util/SparseArray;
    //   380: iload_3
    //   381: invokevirtual indexOfKey : (I)I
    //   384: istore #7
    //   386: iload #8
    //   388: istore #6
    //   390: iload #7
    //   392: istore #8
    //   394: iload #8
    //   396: iflt -> 668
    //   399: getstatic android/text/method/MultiTapKeyListener.sRecs : Landroid/util/SparseArray;
    //   402: iload #8
    //   404: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   407: checkcast java/lang/String
    //   410: astore #4
    //   412: iload #5
    //   414: iconst_1
    //   415: iand
    //   416: ifeq -> 466
    //   419: aload_0
    //   420: getfield mCapitalize : Landroid/text/method/TextKeyListener$Capitalize;
    //   423: astore_1
    //   424: aload_1
    //   425: aload_2
    //   426: iload #6
    //   428: invokestatic shouldCap : (Landroid/text/method/TextKeyListener$Capitalize;Ljava/lang/CharSequence;I)Z
    //   431: ifeq -> 466
    //   434: iconst_0
    //   435: istore_3
    //   436: iload_3
    //   437: aload #4
    //   439: invokevirtual length : ()I
    //   442: if_icmpge -> 466
    //   445: aload #4
    //   447: iload_3
    //   448: invokevirtual charAt : (I)C
    //   451: invokestatic isUpperCase : (C)Z
    //   454: ifeq -> 460
    //   457: goto -> 468
    //   460: iinc #3, 1
    //   463: goto -> 436
    //   466: iconst_0
    //   467: istore_3
    //   468: iload #6
    //   470: iload #9
    //   472: if_icmpeq -> 481
    //   475: aload_2
    //   476: iload #9
    //   478: invokestatic setSelection : (Landroid/text/Spannable;I)V
    //   481: aload_2
    //   482: getstatic android/text/method/MultiTapKeyListener.OLD_SEL_START : Ljava/lang/Object;
    //   485: iload #6
    //   487: iload #6
    //   489: bipush #17
    //   491: invokeinterface setSpan : (Ljava/lang/Object;III)V
    //   496: aload_2
    //   497: iload #6
    //   499: iload #9
    //   501: aload #4
    //   503: iload_3
    //   504: iload_3
    //   505: iconst_1
    //   506: iadd
    //   507: invokeinterface replace : (IILjava/lang/CharSequence;II)Landroid/text/Editable;
    //   512: pop
    //   513: aload_2
    //   514: getstatic android/text/method/MultiTapKeyListener.OLD_SEL_START : Ljava/lang/Object;
    //   517: invokeinterface getSpanStart : (Ljava/lang/Object;)I
    //   522: istore #6
    //   524: aload_2
    //   525: invokestatic getSelectionEnd : (Ljava/lang/CharSequence;)I
    //   528: istore_3
    //   529: iload_3
    //   530: iload #6
    //   532: if_icmpeq -> 576
    //   535: aload_2
    //   536: iload #6
    //   538: iload_3
    //   539: invokestatic setSelection : (Landroid/text/Spannable;II)V
    //   542: aload_2
    //   543: getstatic android/text/method/TextKeyListener.LAST_TYPED : Ljava/lang/Object;
    //   546: iload #6
    //   548: iload_3
    //   549: bipush #33
    //   551: invokeinterface setSpan : (Ljava/lang/Object;III)V
    //   556: aload_2
    //   557: getstatic android/text/method/TextKeyListener.ACTIVE : Ljava/lang/Object;
    //   560: iload #6
    //   562: iload_3
    //   563: bipush #33
    //   565: iload #8
    //   567: bipush #24
    //   569: ishl
    //   570: ior
    //   571: invokeinterface setSpan : (Ljava/lang/Object;III)V
    //   576: aload_2
    //   577: invokestatic removeTimeouts : (Landroid/text/Spannable;)V
    //   580: new android/text/method/MultiTapKeyListener$Timeout
    //   583: dup
    //   584: aload_0
    //   585: aload_2
    //   586: invokespecial <init> : (Landroid/text/method/MultiTapKeyListener;Landroid/text/Editable;)V
    //   589: pop
    //   590: aload_2
    //   591: aload_0
    //   592: invokeinterface getSpanStart : (Ljava/lang/Object;)I
    //   597: ifge -> 666
    //   600: aload_2
    //   601: iconst_0
    //   602: aload_2
    //   603: invokeinterface length : ()I
    //   608: ldc android/text/method/KeyListener
    //   610: invokeinterface getSpans : (IILjava/lang/Class;)[Ljava/lang/Object;
    //   615: checkcast [Landroid/text/method/KeyListener;
    //   618: astore_1
    //   619: aload_1
    //   620: arraylength
    //   621: istore #8
    //   623: iconst_0
    //   624: istore_3
    //   625: iload_3
    //   626: iload #8
    //   628: if_icmpge -> 650
    //   631: aload_1
    //   632: iload_3
    //   633: aaload
    //   634: astore #4
    //   636: aload_2
    //   637: aload #4
    //   639: invokeinterface removeSpan : (Ljava/lang/Object;)V
    //   644: iinc #3, 1
    //   647: goto -> 625
    //   650: aload_2
    //   651: aload_0
    //   652: iconst_0
    //   653: aload_2
    //   654: invokeinterface length : ()I
    //   659: bipush #18
    //   661: invokeinterface setSpan : (Ljava/lang/Object;III)V
    //   666: iconst_1
    //   667: ireturn
    //   668: aload_0
    //   669: aload_1
    //   670: aload_2
    //   671: iload_3
    //   672: aload #4
    //   674: invokespecial onKeyDown : (Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z
    //   677: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #91	-> 0
    //   #93	-> 0
    //   #94	-> 4
    //   #93	-> 19
    //   #98	-> 22
    //   #99	-> 28
    //   #101	-> 34
    //   #102	-> 43
    //   #105	-> 52
    //   #106	-> 63
    //   #113	-> 74
    //   #116	-> 91
    //   #118	-> 124
    //   #119	-> 134
    //   #120	-> 140
    //   #122	-> 150
    //   #123	-> 158
    //   #124	-> 158
    //   #123	-> 167
    //   #125	-> 179
    //   #126	-> 183
    //   #128	-> 193
    //   #130	-> 195
    //   #131	-> 203
    //   #132	-> 203
    //   #131	-> 212
    //   #133	-> 224
    //   #134	-> 228
    //   #136	-> 238
    //   #140	-> 240
    //   #141	-> 252
    //   #142	-> 265
    //   #143	-> 275
    //   #145	-> 284
    //   #146	-> 289
    //   #148	-> 300
    //   #149	-> 317
    //   #150	-> 321
    //   #152	-> 331
    //   #145	-> 333
    //   #161	-> 333
    //   #163	-> 342
    //   #164	-> 347
    //   #165	-> 355
    //   #163	-> 366
    //   #168	-> 377
    //   #171	-> 394
    //   #176	-> 399
    //   #178	-> 412
    //   #179	-> 412
    //   #180	-> 424
    //   #181	-> 434
    //   #182	-> 445
    //   #183	-> 457
    //   #184	-> 457
    //   #181	-> 460
    //   #189	-> 466
    //   #190	-> 475
    //   #193	-> 481
    //   #196	-> 496
    //   #198	-> 513
    //   #199	-> 524
    //   #201	-> 529
    //   #202	-> 535
    //   #204	-> 542
    //   #208	-> 556
    //   #215	-> 576
    //   #216	-> 580
    //   #221	-> 590
    //   #222	-> 600
    //   #224	-> 619
    //   #225	-> 636
    //   #224	-> 644
    //   #227	-> 650
    //   #231	-> 666
    //   #234	-> 668
  }
  
  public void onSpanChanged(Spannable paramSpannable, Object paramObject, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (paramObject == Selection.SELECTION_END) {
      paramSpannable.removeSpan(TextKeyListener.ACTIVE);
      removeTimeouts(paramSpannable);
    } 
  }
  
  private static void removeTimeouts(Spannable paramSpannable) {
    Timeout[] arrayOfTimeout = paramSpannable.<Timeout>getSpans(0, paramSpannable.length(), Timeout.class);
    for (byte b = 0; b < arrayOfTimeout.length; b++) {
      Timeout timeout = arrayOfTimeout[b];
      timeout.removeCallbacks(timeout);
      Timeout.access$002(timeout, (Editable)null);
      paramSpannable.removeSpan(timeout);
    } 
  }
  
  class Timeout extends Handler implements Runnable {
    private Editable mBuffer;
    
    final MultiTapKeyListener this$0;
    
    public Timeout(Editable param1Editable) {
      this.mBuffer = param1Editable;
      param1Editable.setSpan(this, 0, param1Editable.length(), 18);
      postAtTime(this, SystemClock.uptimeMillis() + 2000L);
    }
    
    public void run() {
      Editable editable = this.mBuffer;
      if (editable != null) {
        int i = Selection.getSelectionStart(editable);
        int j = Selection.getSelectionEnd(editable);
        int k = editable.getSpanStart(TextKeyListener.ACTIVE);
        int m = editable.getSpanEnd(TextKeyListener.ACTIVE);
        if (i == k && j == m)
          Selection.setSelection(editable, Selection.getSelectionEnd(editable)); 
        editable.removeSpan(this);
      } 
    }
  }
  
  public void onSpanAdded(Spannable paramSpannable, Object paramObject, int paramInt1, int paramInt2) {}
  
  public void onSpanRemoved(Spannable paramSpannable, Object paramObject, int paramInt1, int paramInt2) {}
}
