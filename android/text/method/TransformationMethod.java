package android.text.method;

import android.graphics.Rect;
import android.view.View;

public interface TransformationMethod {
  CharSequence getTransformation(CharSequence paramCharSequence, View paramView);
  
  void onFocusChanged(View paramView, CharSequence paramCharSequence, boolean paramBoolean, int paramInt, Rect paramRect);
}
