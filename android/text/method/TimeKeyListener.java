package android.text.method;

import java.util.HashMap;
import java.util.Locale;

public class TimeKeyListener extends NumberKeyListener {
  public int getInputType() {
    if (this.mNeedsAdvancedInput)
      return 1; 
    return 36;
  }
  
  protected char[] getAcceptedChars() {
    return this.mCharacters;
  }
  
  @Deprecated
  public TimeKeyListener() {
    this(null);
  }
  
  public TimeKeyListener(Locale paramLocale) {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial <init> : ()V
    //   4: new java/util/LinkedHashSet
    //   7: dup
    //   8: invokespecial <init> : ()V
    //   11: astore_2
    //   12: aload_2
    //   13: aload_1
    //   14: invokestatic addDigits : (Ljava/util/Collection;Ljava/util/Locale;)Z
    //   17: ifeq -> 57
    //   20: aload_2
    //   21: aload_1
    //   22: invokestatic addAmPmChars : (Ljava/util/Collection;Ljava/util/Locale;)Z
    //   25: ifeq -> 57
    //   28: aload_2
    //   29: aload_1
    //   30: ldc 'hms'
    //   32: ldc 'ahHKkms'
    //   34: invokestatic addFormatCharsFromSkeleton : (Ljava/util/Collection;Ljava/util/Locale;Ljava/lang/String;Ljava/lang/String;)Z
    //   37: ifeq -> 57
    //   40: aload_2
    //   41: aload_1
    //   42: ldc 'Hms'
    //   44: ldc 'ahHKkms'
    //   46: invokestatic addFormatCharsFromSkeleton : (Ljava/util/Collection;Ljava/util/Locale;Ljava/lang/String;Ljava/lang/String;)Z
    //   49: ifeq -> 57
    //   52: iconst_1
    //   53: istore_3
    //   54: goto -> 59
    //   57: iconst_0
    //   58: istore_3
    //   59: iload_3
    //   60: ifeq -> 114
    //   63: aload_0
    //   64: aload_2
    //   65: invokestatic collectionToArray : (Ljava/util/Collection;)[C
    //   68: putfield mCharacters : [C
    //   71: aload_1
    //   72: ifnull -> 95
    //   75: ldc 'en'
    //   77: aload_1
    //   78: invokevirtual getLanguage : ()Ljava/lang/String;
    //   81: invokevirtual equals : (Ljava/lang/Object;)Z
    //   84: ifeq -> 95
    //   87: aload_0
    //   88: iconst_0
    //   89: putfield mNeedsAdvancedInput : Z
    //   92: goto -> 126
    //   95: aload_0
    //   96: iconst_1
    //   97: getstatic android/text/method/TimeKeyListener.CHARACTERS : [C
    //   100: aload_0
    //   101: getfield mCharacters : [C
    //   104: invokestatic containsAll : ([C[C)Z
    //   107: ixor
    //   108: putfield mNeedsAdvancedInput : Z
    //   111: goto -> 126
    //   114: aload_0
    //   115: getstatic android/text/method/TimeKeyListener.CHARACTERS : [C
    //   118: putfield mCharacters : [C
    //   121: aload_0
    //   122: iconst_0
    //   123: putfield mNeedsAdvancedInput : Z
    //   126: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #67	-> 0
    //   #68	-> 4
    //   #71	-> 12
    //   #72	-> 20
    //   #73	-> 28
    //   #75	-> 40
    //   #77	-> 59
    //   #78	-> 63
    //   #79	-> 71
    //   #83	-> 87
    //   #85	-> 95
    //   #88	-> 114
    //   #89	-> 121
    //   #91	-> 126
  }
  
  @Deprecated
  public static TimeKeyListener getInstance() {
    return getInstance(null);
  }
  
  public static TimeKeyListener getInstance(Locale paramLocale) {
    synchronized (sLock) {
      TimeKeyListener timeKeyListener1 = sInstanceCache.get(paramLocale);
      TimeKeyListener timeKeyListener2 = timeKeyListener1;
      if (timeKeyListener1 == null) {
        timeKeyListener2 = new TimeKeyListener();
        this(paramLocale);
        sInstanceCache.put(paramLocale, timeKeyListener2);
      } 
      return timeKeyListener2;
    } 
  }
  
  public static final char[] CHARACTERS = new char[] { 
      '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
      'a', 'm', 'p', ':' };
  
  private static final String SKELETON_12HOUR = "hms";
  
  private static final String SKELETON_24HOUR = "Hms";
  
  private static final String SYMBOLS_TO_IGNORE = "ahHKkms";
  
  private static final HashMap<Locale, TimeKeyListener> sInstanceCache;
  
  private static final Object sLock = new Object();
  
  private final char[] mCharacters;
  
  private final boolean mNeedsAdvancedInput;
  
  static {
    sInstanceCache = new HashMap<>();
  }
}
