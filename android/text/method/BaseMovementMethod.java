package android.text.method;

import android.text.Layout;
import android.text.Spannable;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.TextView;

public class BaseMovementMethod implements MovementMethod {
  public boolean canSelectArbitrarily() {
    return false;
  }
  
  public void initialize(TextView paramTextView, Spannable paramSpannable) {}
  
  public boolean onKeyDown(TextView paramTextView, Spannable paramSpannable, int paramInt, KeyEvent paramKeyEvent) {
    int i = getMovementMetaState(paramSpannable, paramKeyEvent);
    boolean bool = handleMovementKey(paramTextView, paramSpannable, paramInt, i, paramKeyEvent);
    if (bool) {
      MetaKeyKeyListener.adjustMetaAfterKeypress(paramSpannable);
      MetaKeyKeyListener.resetLockedMeta(paramSpannable);
    } 
    return bool;
  }
  
  public boolean onKeyOther(TextView paramTextView, Spannable paramSpannable, KeyEvent paramKeyEvent) {
    int i = getMovementMetaState(paramSpannable, paramKeyEvent);
    int j = paramKeyEvent.getKeyCode();
    if (j != 0 && 
      paramKeyEvent.getAction() == 2) {
      int k = paramKeyEvent.getRepeatCount();
      boolean bool;
      byte b;
      for (bool = false, b = 0; b < k && 
        handleMovementKey(paramTextView, paramSpannable, j, i, paramKeyEvent); b++)
        bool = true; 
      if (bool) {
        MetaKeyKeyListener.adjustMetaAfterKeypress(paramSpannable);
        MetaKeyKeyListener.resetLockedMeta(paramSpannable);
      } 
      return bool;
    } 
    return false;
  }
  
  public boolean onKeyUp(TextView paramTextView, Spannable paramSpannable, int paramInt, KeyEvent paramKeyEvent) {
    return false;
  }
  
  public void onTakeFocus(TextView paramTextView, Spannable paramSpannable, int paramInt) {}
  
  public boolean onTouchEvent(TextView paramTextView, Spannable paramSpannable, MotionEvent paramMotionEvent) {
    return false;
  }
  
  public boolean onTrackballEvent(TextView paramTextView, Spannable paramSpannable, MotionEvent paramMotionEvent) {
    return false;
  }
  
  public boolean onGenericMotionEvent(TextView paramTextView, Spannable paramSpannable, MotionEvent paramMotionEvent) {
    float f1, f2;
    boolean bool;
    if ((paramMotionEvent.getSource() & 0x2) == 0 || 
      paramMotionEvent.getAction() != 8)
      return false; 
    if ((paramMotionEvent.getMetaState() & 0x1) != 0) {
      f1 = 0.0F;
      f2 = paramMotionEvent.getAxisValue(9);
    } else {
      f1 = -paramMotionEvent.getAxisValue(9);
      f2 = paramMotionEvent.getAxisValue(10);
    } 
    int i = 0;
    if (f2 < 0.0F) {
      i = false | scrollLeft(paramTextView, paramSpannable, (int)Math.ceil(-f2));
    } else if (f2 > 0.0F) {
      i = false | scrollRight(paramTextView, paramSpannable, (int)Math.ceil(f2));
    } 
    if (f1 < 0.0F) {
      bool = i | scrollUp(paramTextView, paramSpannable, (int)Math.ceil(-f1));
    } else {
      int j = i;
      if (f1 > 0.0F)
        bool = i | scrollDown(paramTextView, paramSpannable, (int)Math.ceil(f1)); 
    } 
    return bool;
  }
  
  protected int getMovementMetaState(Spannable paramSpannable, KeyEvent paramKeyEvent) {
    int i = MetaKeyKeyListener.getMetaState(paramSpannable, paramKeyEvent);
    return KeyEvent.normalizeMetaState(i & 0xFFFFF9FF) & 0xFFFFFF3E;
  }
  
  protected boolean handleMovementKey(TextView paramTextView, Spannable paramSpannable, int paramInt1, int paramInt2, KeyEvent paramKeyEvent) {
    if (paramInt1 != 92) {
      if (paramInt1 != 93) {
        if (paramInt1 != 122) {
          if (paramInt1 != 123) {
            switch (paramInt1) {
              default:
                return false;
              case 22:
                if (KeyEvent.metaStateHasNoModifiers(paramInt2))
                  return right(paramTextView, paramSpannable); 
                if (KeyEvent.metaStateHasModifiers(paramInt2, 4096))
                  return rightWord(paramTextView, paramSpannable); 
                if (KeyEvent.metaStateHasModifiers(paramInt2, 2))
                  return lineEnd(paramTextView, paramSpannable); 
              case 21:
                if (KeyEvent.metaStateHasNoModifiers(paramInt2))
                  return left(paramTextView, paramSpannable); 
                if (KeyEvent.metaStateHasModifiers(paramInt2, 4096))
                  return leftWord(paramTextView, paramSpannable); 
                if (KeyEvent.metaStateHasModifiers(paramInt2, 2))
                  return lineStart(paramTextView, paramSpannable); 
              case 20:
                if (KeyEvent.metaStateHasNoModifiers(paramInt2))
                  return down(paramTextView, paramSpannable); 
                if (KeyEvent.metaStateHasModifiers(paramInt2, 2))
                  return bottom(paramTextView, paramSpannable); 
              case 19:
                break;
            } 
            if (KeyEvent.metaStateHasNoModifiers(paramInt2))
              return up(paramTextView, paramSpannable); 
            if (KeyEvent.metaStateHasModifiers(paramInt2, 2))
              return top(paramTextView, paramSpannable); 
          } 
          if (KeyEvent.metaStateHasNoModifiers(paramInt2))
            return end(paramTextView, paramSpannable); 
          if (KeyEvent.metaStateHasModifiers(paramInt2, 4096))
            return bottom(paramTextView, paramSpannable); 
        } 
        if (KeyEvent.metaStateHasNoModifiers(paramInt2))
          return home(paramTextView, paramSpannable); 
        if (KeyEvent.metaStateHasModifiers(paramInt2, 4096))
          return top(paramTextView, paramSpannable); 
      } 
      if (KeyEvent.metaStateHasNoModifiers(paramInt2))
        return pageDown(paramTextView, paramSpannable); 
      if (KeyEvent.metaStateHasModifiers(paramInt2, 2))
        return bottom(paramTextView, paramSpannable); 
    } 
    if (KeyEvent.metaStateHasNoModifiers(paramInt2))
      return pageUp(paramTextView, paramSpannable); 
    if (KeyEvent.metaStateHasModifiers(paramInt2, 2))
      return top(paramTextView, paramSpannable); 
  }
  
  protected boolean left(TextView paramTextView, Spannable paramSpannable) {
    return false;
  }
  
  protected boolean right(TextView paramTextView, Spannable paramSpannable) {
    return false;
  }
  
  protected boolean up(TextView paramTextView, Spannable paramSpannable) {
    return false;
  }
  
  protected boolean down(TextView paramTextView, Spannable paramSpannable) {
    return false;
  }
  
  protected boolean pageUp(TextView paramTextView, Spannable paramSpannable) {
    return false;
  }
  
  protected boolean pageDown(TextView paramTextView, Spannable paramSpannable) {
    return false;
  }
  
  protected boolean top(TextView paramTextView, Spannable paramSpannable) {
    return false;
  }
  
  protected boolean bottom(TextView paramTextView, Spannable paramSpannable) {
    return false;
  }
  
  protected boolean lineStart(TextView paramTextView, Spannable paramSpannable) {
    return false;
  }
  
  protected boolean lineEnd(TextView paramTextView, Spannable paramSpannable) {
    return false;
  }
  
  protected boolean leftWord(TextView paramTextView, Spannable paramSpannable) {
    return false;
  }
  
  protected boolean rightWord(TextView paramTextView, Spannable paramSpannable) {
    return false;
  }
  
  protected boolean home(TextView paramTextView, Spannable paramSpannable) {
    return false;
  }
  
  protected boolean end(TextView paramTextView, Spannable paramSpannable) {
    return false;
  }
  
  private int getTopLine(TextView paramTextView) {
    return paramTextView.getLayout().getLineForVertical(paramTextView.getScrollY());
  }
  
  private int getBottomLine(TextView paramTextView) {
    return paramTextView.getLayout().getLineForVertical(paramTextView.getScrollY() + getInnerHeight(paramTextView));
  }
  
  private int getInnerWidth(TextView paramTextView) {
    return paramTextView.getWidth() - paramTextView.getTotalPaddingLeft() - paramTextView.getTotalPaddingRight();
  }
  
  private int getInnerHeight(TextView paramTextView) {
    return paramTextView.getHeight() - paramTextView.getTotalPaddingTop() - paramTextView.getTotalPaddingBottom();
  }
  
  private int getCharacterWidth(TextView paramTextView) {
    return (int)Math.ceil(paramTextView.getPaint().getFontSpacing());
  }
  
  private int getScrollBoundsLeft(TextView paramTextView) {
    Layout layout = paramTextView.getLayout();
    int i = getTopLine(paramTextView);
    int j = getBottomLine(paramTextView);
    if (i > j)
      return 0; 
    int k = Integer.MAX_VALUE;
    for (; i <= j; i++, k = n) {
      int m = (int)Math.floor(layout.getLineLeft(i));
      int n = k;
      if (m < k)
        n = m; 
    } 
    return k;
  }
  
  private int getScrollBoundsRight(TextView paramTextView) {
    Layout layout = paramTextView.getLayout();
    int i = getTopLine(paramTextView);
    int j = getBottomLine(paramTextView);
    if (i > j)
      return 0; 
    int k = Integer.MIN_VALUE;
    for (; i <= j; i++, k = n) {
      int m = (int)Math.ceil(layout.getLineRight(i));
      int n = k;
      if (m > k)
        n = m; 
    } 
    return k;
  }
  
  protected boolean scrollLeft(TextView paramTextView, Spannable paramSpannable, int paramInt) {
    int i = getScrollBoundsLeft(paramTextView);
    int j = paramTextView.getScrollX();
    if (j > i) {
      paramInt = Math.max(j - getCharacterWidth(paramTextView) * paramInt, i);
      paramTextView.scrollTo(paramInt, paramTextView.getScrollY());
      return true;
    } 
    return false;
  }
  
  protected boolean scrollRight(TextView paramTextView, Spannable paramSpannable, int paramInt) {
    int i = getScrollBoundsRight(paramTextView) - getInnerWidth(paramTextView);
    int j = paramTextView.getScrollX();
    if (j < i) {
      paramInt = Math.min(getCharacterWidth(paramTextView) * paramInt + j, i);
      paramTextView.scrollTo(paramInt, paramTextView.getScrollY());
      return true;
    } 
    return false;
  }
  
  protected boolean scrollUp(TextView paramTextView, Spannable paramSpannable, int paramInt) {
    Layout layout = paramTextView.getLayout();
    int i = paramTextView.getScrollY();
    int j = layout.getLineForVertical(i);
    int k = j;
    if (layout.getLineTop(j) == i)
      k = j - 1; 
    if (k >= 0) {
      paramInt = Math.max(k - paramInt + 1, 0);
      Touch.scrollTo(paramTextView, layout, paramTextView.getScrollX(), layout.getLineTop(paramInt));
      return true;
    } 
    return false;
  }
  
  protected boolean scrollDown(TextView paramTextView, Spannable paramSpannable, int paramInt) {
    Layout layout = paramTextView.getLayout();
    int i = getInnerHeight(paramTextView);
    int j = paramTextView.getScrollY() + i;
    int k = layout.getLineForVertical(j);
    int m = k;
    if (layout.getLineTop(k + 1) < j + 1)
      m = k + 1; 
    k = layout.getLineCount() - 1;
    if (m <= k) {
      m = Math.min(m + paramInt - 1, k);
      paramInt = paramTextView.getScrollX();
      m = layout.getLineTop(m + 1);
      Touch.scrollTo(paramTextView, layout, paramInt, m - i);
      return true;
    } 
    return false;
  }
  
  protected boolean scrollPageUp(TextView paramTextView, Spannable paramSpannable) {
    Layout layout = paramTextView.getLayout();
    int i = paramTextView.getScrollY(), j = getInnerHeight(paramTextView);
    i = layout.getLineForVertical(i - j);
    if (i >= 0) {
      Touch.scrollTo(paramTextView, layout, paramTextView.getScrollX(), layout.getLineTop(i));
      return true;
    } 
    return false;
  }
  
  protected boolean scrollPageDown(TextView paramTextView, Spannable paramSpannable) {
    Layout layout = paramTextView.getLayout();
    int i = getInnerHeight(paramTextView);
    int j = paramTextView.getScrollY();
    int k = layout.getLineForVertical(j + i + i);
    if (k <= layout.getLineCount() - 1) {
      j = paramTextView.getScrollX();
      k = layout.getLineTop(k + 1);
      Touch.scrollTo(paramTextView, layout, j, k - i);
      return true;
    } 
    return false;
  }
  
  protected boolean scrollTop(TextView paramTextView, Spannable paramSpannable) {
    Layout layout = paramTextView.getLayout();
    if (getTopLine(paramTextView) >= 0) {
      Touch.scrollTo(paramTextView, layout, paramTextView.getScrollX(), layout.getLineTop(0));
      return true;
    } 
    return false;
  }
  
  protected boolean scrollBottom(TextView paramTextView, Spannable paramSpannable) {
    Layout layout = paramTextView.getLayout();
    int i = layout.getLineCount();
    if (getBottomLine(paramTextView) <= i - 1) {
      int j = paramTextView.getScrollX();
      int k = layout.getLineTop(i);
      i = getInnerHeight(paramTextView);
      Touch.scrollTo(paramTextView, layout, j, k - i);
      return true;
    } 
    return false;
  }
  
  protected boolean scrollLineStart(TextView paramTextView, Spannable paramSpannable) {
    int i = getScrollBoundsLeft(paramTextView);
    int j = paramTextView.getScrollX();
    if (j > i) {
      paramTextView.scrollTo(i, paramTextView.getScrollY());
      return true;
    } 
    return false;
  }
  
  protected boolean scrollLineEnd(TextView paramTextView, Spannable paramSpannable) {
    int i = getScrollBoundsRight(paramTextView) - getInnerWidth(paramTextView);
    int j = paramTextView.getScrollX();
    if (j < i) {
      paramTextView.scrollTo(i, paramTextView.getScrollY());
      return true;
    } 
    return false;
  }
}
