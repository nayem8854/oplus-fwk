package android.text.method;

import android.content.Context;
import android.graphics.Rect;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import java.util.Locale;

public class AllCapsTransformationMethod implements TransformationMethod2 {
  private static final String TAG = "AllCapsTransformationMethod";
  
  private boolean mEnabled;
  
  private Locale mLocale;
  
  public AllCapsTransformationMethod(Context paramContext) {
    this.mLocale = paramContext.getResources().getConfiguration().getLocales().get(0);
  }
  
  public CharSequence getTransformation(CharSequence paramCharSequence, View paramView) {
    if (!this.mEnabled) {
      Log.w("AllCapsTransformationMethod", "Caller did not enable length changes; not transforming text");
      return paramCharSequence;
    } 
    if (paramCharSequence == null)
      return null; 
    Locale locale2 = null;
    if (paramView instanceof TextView)
      locale2 = ((TextView)paramView).getTextLocale(); 
    Locale locale1 = locale2;
    if (locale2 == null)
      locale1 = this.mLocale; 
    boolean bool = paramCharSequence instanceof android.text.Spanned;
    return TextUtils.toUpperCase(locale1, paramCharSequence, bool);
  }
  
  public void onFocusChanged(View paramView, CharSequence paramCharSequence, boolean paramBoolean, int paramInt, Rect paramRect) {}
  
  public void setLengthChangesAllowed(boolean paramBoolean) {
    this.mEnabled = paramBoolean;
  }
}
