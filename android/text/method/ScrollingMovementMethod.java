package android.text.method;

import android.text.Layout;
import android.text.Spannable;
import android.view.MotionEvent;
import android.widget.TextView;

public class ScrollingMovementMethod extends BaseMovementMethod implements MovementMethod {
  private static ScrollingMovementMethod sInstance;
  
  protected boolean left(TextView paramTextView, Spannable paramSpannable) {
    return scrollLeft(paramTextView, paramSpannable, 1);
  }
  
  protected boolean right(TextView paramTextView, Spannable paramSpannable) {
    return scrollRight(paramTextView, paramSpannable, 1);
  }
  
  protected boolean up(TextView paramTextView, Spannable paramSpannable) {
    return scrollUp(paramTextView, paramSpannable, 1);
  }
  
  protected boolean down(TextView paramTextView, Spannable paramSpannable) {
    return scrollDown(paramTextView, paramSpannable, 1);
  }
  
  protected boolean pageUp(TextView paramTextView, Spannable paramSpannable) {
    return scrollPageUp(paramTextView, paramSpannable);
  }
  
  protected boolean pageDown(TextView paramTextView, Spannable paramSpannable) {
    return scrollPageDown(paramTextView, paramSpannable);
  }
  
  protected boolean top(TextView paramTextView, Spannable paramSpannable) {
    return scrollTop(paramTextView, paramSpannable);
  }
  
  protected boolean bottom(TextView paramTextView, Spannable paramSpannable) {
    return scrollBottom(paramTextView, paramSpannable);
  }
  
  protected boolean lineStart(TextView paramTextView, Spannable paramSpannable) {
    return scrollLineStart(paramTextView, paramSpannable);
  }
  
  protected boolean lineEnd(TextView paramTextView, Spannable paramSpannable) {
    return scrollLineEnd(paramTextView, paramSpannable);
  }
  
  protected boolean home(TextView paramTextView, Spannable paramSpannable) {
    return top(paramTextView, paramSpannable);
  }
  
  protected boolean end(TextView paramTextView, Spannable paramSpannable) {
    return bottom(paramTextView, paramSpannable);
  }
  
  public boolean onTouchEvent(TextView paramTextView, Spannable paramSpannable, MotionEvent paramMotionEvent) {
    return Touch.onTouchEvent(paramTextView, paramSpannable, paramMotionEvent);
  }
  
  public void onTakeFocus(TextView paramTextView, Spannable paramSpannable, int paramInt) {
    Layout layout = paramTextView.getLayout();
    if (layout != null && (paramInt & 0x2) != 0) {
      int i = paramTextView.getScrollX();
      int j = layout.getLineTop(0);
      paramTextView.scrollTo(i, j);
    } 
    if (layout != null && (paramInt & 0x1) != 0) {
      int i = paramTextView.getTotalPaddingTop();
      int j = paramTextView.getTotalPaddingBottom();
      int k = layout.getLineCount();
      paramInt = paramTextView.getScrollX();
      int m = layout.getLineTop(k - 1 + 1);
      k = paramTextView.getHeight();
      paramTextView.scrollTo(paramInt, m - k - i + j);
    } 
  }
  
  public static MovementMethod getInstance() {
    if (sInstance == null)
      sInstance = new ScrollingMovementMethod(); 
    return sInstance;
  }
}
