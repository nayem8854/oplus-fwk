package android.text.method;

import android.icu.lang.UCharacter;
import android.icu.text.DecimalFormatSymbols;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import com.android.internal.util.ArrayUtils;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Locale;

public class DigitsKeyListener extends NumberKeyListener {
  private String mDecimalPointChars = ".";
  
  private String mSignChars = "-+";
  
  protected char[] getAcceptedChars() {
    return this.mAccepted;
  }
  
  static {
    char[] arrayOfChar1 = { 
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
        '-', '+' }, arrayOfChar2 = { 
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
        '-', '+', '.' };
    COMPATIBILITY_CHARACTERS = new char[][] { { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' }, arrayOfChar1, { 
          '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
          '.' }, arrayOfChar2 };
  }
  
  private boolean isSignChar(char paramChar) {
    boolean bool;
    if (this.mSignChars.indexOf(paramChar) != -1) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean isDecimalPointChar(char paramChar) {
    boolean bool;
    if (this.mDecimalPointChars.indexOf(paramChar) != -1) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  @Deprecated
  public DigitsKeyListener() {
    this(null, false, false);
  }
  
  @Deprecated
  public DigitsKeyListener(boolean paramBoolean1, boolean paramBoolean2) {
    this(null, paramBoolean1, paramBoolean2);
  }
  
  public DigitsKeyListener(Locale paramLocale) {
    this(paramLocale, false, false);
  }
  
  private void setToCompat() {
    boolean bool2;
    this.mDecimalPointChars = ".";
    this.mSignChars = "-+";
    boolean bool1 = this.mSign;
    if (this.mDecimal) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mAccepted = COMPATIBILITY_CHARACTERS[bool1 | bool2];
    this.mNeedsAdvancedInput = false;
  }
  
  private void calculateNeedForAdvancedInput() {
    boolean bool2, bool1 = this.mSign;
    if (this.mDecimal) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mNeedsAdvancedInput = ArrayUtils.containsAll(COMPATIBILITY_CHARACTERS[bool1 | bool2], this.mAccepted) ^ true;
  }
  
  private static String stripBidiControls(String paramString) {
    String str = "";
    for (byte b = 0; b < paramString.length(); b++, str = str1) {
      char c = paramString.charAt(b);
      String str1 = str;
      if (!UCharacter.hasBinaryProperty(c, 2))
        if (str.isEmpty()) {
          str1 = String.valueOf(c);
        } else {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(str);
          stringBuilder.append(c);
          str1 = stringBuilder.toString();
        }  
    } 
    return str;
  }
  
  public DigitsKeyListener(Locale paramLocale, boolean paramBoolean1, boolean paramBoolean2) {
    this.mSign = paramBoolean1;
    this.mDecimal = paramBoolean2;
    this.mStringMode = false;
    this.mLocale = paramLocale;
    if (paramLocale == null) {
      setToCompat();
      return;
    } 
    LinkedHashSet<Character> linkedHashSet = new LinkedHashSet();
    boolean bool = NumberKeyListener.addDigits(linkedHashSet, paramLocale);
    if (!bool) {
      setToCompat();
      return;
    } 
    if (paramBoolean1 || paramBoolean2) {
      DecimalFormatSymbols decimalFormatSymbols = DecimalFormatSymbols.getInstance(paramLocale);
      if (paramBoolean1) {
        String str1 = stripBidiControls(decimalFormatSymbols.getMinusSignString());
        String str2 = stripBidiControls(decimalFormatSymbols.getPlusSignString());
        if (str1.length() > 1 || str2.length() > 1) {
          setToCompat();
          return;
        } 
        char c1 = str1.charAt(0);
        char c2 = str2.charAt(0);
        linkedHashSet.add(Character.valueOf(c1));
        linkedHashSet.add(Character.valueOf(c2));
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("");
        stringBuilder.append(c1);
        stringBuilder.append(c2);
        this.mSignChars = stringBuilder.toString();
        if (c1 == '−' || c1 == '–') {
          linkedHashSet.add(Character.valueOf('-'));
          stringBuilder = new StringBuilder();
          stringBuilder.append(this.mSignChars);
          stringBuilder.append('-');
          this.mSignChars = stringBuilder.toString();
        } 
      } 
      if (paramBoolean2) {
        String str = decimalFormatSymbols.getDecimalSeparatorString();
        if (str.length() > 1) {
          setToCompat();
          return;
        } 
        Character character = Character.valueOf(str.charAt(0));
        linkedHashSet.add(character);
        this.mDecimalPointChars = character.toString();
      } 
    } 
    this.mAccepted = NumberKeyListener.collectionToArray(linkedHashSet);
    calculateNeedForAdvancedInput();
  }
  
  private DigitsKeyListener(String paramString) {
    this.mSign = false;
    this.mDecimal = false;
    this.mStringMode = true;
    this.mLocale = null;
    this.mAccepted = new char[paramString.length()];
    paramString.getChars(0, paramString.length(), this.mAccepted, 0);
    this.mNeedsAdvancedInput = false;
  }
  
  @Deprecated
  public static DigitsKeyListener getInstance() {
    return getInstance(false, false);
  }
  
  @Deprecated
  public static DigitsKeyListener getInstance(boolean paramBoolean1, boolean paramBoolean2) {
    return getInstance(null, paramBoolean1, paramBoolean2);
  }
  
  public static DigitsKeyListener getInstance(Locale paramLocale) {
    return getInstance(paramLocale, false, false);
  }
  
  private static final Object sLocaleCacheLock = new Object();
  
  private static final HashMap<Locale, DigitsKeyListener[]> sLocaleInstanceCache = (HashMap)new HashMap<>();
  
  public static DigitsKeyListener getInstance(Locale paramLocale, boolean paramBoolean1, boolean paramBoolean2) {
    boolean bool;
    if (paramBoolean2) {
      bool = true;
    } else {
      bool = false;
    } 
    bool |= paramBoolean1;
    synchronized (sLocaleCacheLock) {
      DigitsKeyListener digitsKeyListener1, arrayOfDigitsKeyListener1[] = sLocaleInstanceCache.get(paramLocale);
      if (arrayOfDigitsKeyListener1 != null && arrayOfDigitsKeyListener1[bool] != null) {
        digitsKeyListener1 = arrayOfDigitsKeyListener1[bool];
        return digitsKeyListener1;
      } 
      DigitsKeyListener[] arrayOfDigitsKeyListener2 = arrayOfDigitsKeyListener1;
      if (arrayOfDigitsKeyListener1 == null) {
        arrayOfDigitsKeyListener2 = new DigitsKeyListener[4];
        sLocaleInstanceCache.put(digitsKeyListener1, arrayOfDigitsKeyListener2);
      } 
      DigitsKeyListener digitsKeyListener2 = new DigitsKeyListener();
      this((Locale)digitsKeyListener1, paramBoolean1, paramBoolean2);
      arrayOfDigitsKeyListener2[bool] = digitsKeyListener2;
      return digitsKeyListener2;
    } 
  }
  
  private static final Object sStringCacheLock = new Object();
  
  private static final HashMap<String, DigitsKeyListener> sStringInstanceCache = new HashMap<>();
  
  private static final char[][] COMPATIBILITY_CHARACTERS;
  
  private static final int DECIMAL = 2;
  
  private static final String DEFAULT_DECIMAL_POINT_CHARS = ".";
  
  private static final String DEFAULT_SIGN_CHARS = "-+";
  
  private static final char EN_DASH = '–';
  
  private static final char HYPHEN_MINUS = '-';
  
  private static final char MINUS_SIGN = '−';
  
  private static final int SIGN = 1;
  
  private char[] mAccepted;
  
  private final boolean mDecimal;
  
  private final Locale mLocale;
  
  private boolean mNeedsAdvancedInput;
  
  private final boolean mSign;
  
  private final boolean mStringMode;
  
  public static DigitsKeyListener getInstance(String paramString) {
    synchronized (sStringCacheLock) {
      DigitsKeyListener digitsKeyListener1 = sStringInstanceCache.get(paramString);
      DigitsKeyListener digitsKeyListener2 = digitsKeyListener1;
      if (digitsKeyListener1 == null) {
        digitsKeyListener2 = new DigitsKeyListener();
        this(paramString);
        sStringInstanceCache.put(paramString, digitsKeyListener2);
      } 
      return digitsKeyListener2;
    } 
  }
  
  public static DigitsKeyListener getInstance(Locale paramLocale, DigitsKeyListener paramDigitsKeyListener) {
    if (paramDigitsKeyListener.mStringMode)
      return paramDigitsKeyListener; 
    return getInstance(paramLocale, paramDigitsKeyListener.mSign, paramDigitsKeyListener.mDecimal);
  }
  
  public int getInputType() {
    int i;
    if (this.mNeedsAdvancedInput) {
      i = 1;
    } else {
      int j = 2;
      if (this.mSign)
        j = 0x2 | 0x1000; 
      i = j;
      if (this.mDecimal)
        i = j | 0x2000; 
    } 
    return i;
  }
  
  public CharSequence filter(CharSequence paramCharSequence, int paramInt1, int paramInt2, Spanned paramSpanned, int paramInt3, int paramInt4) {
    CharSequence charSequence2;
    int i, j;
    CharSequence charSequence1 = super.filter(paramCharSequence, paramInt1, paramInt2, paramSpanned, paramInt3, paramInt4);
    if (!this.mSign && !this.mDecimal)
      return charSequence1; 
    if (charSequence1 != null) {
      charSequence2 = charSequence1;
      i = 0;
      j = charSequence1.length();
    } else {
      j = paramInt2;
      i = paramInt1;
      charSequence2 = paramCharSequence;
    } 
    paramInt2 = -1;
    int k = -1;
    int m = paramSpanned.length();
    for (paramInt1 = 0; paramInt1 < paramInt3; paramInt1++, paramInt2 = n) {
      int n;
      char c = paramSpanned.charAt(paramInt1);
      if (isSignChar(c)) {
        n = paramInt1;
      } else {
        n = paramInt2;
        if (isDecimalPointChar(c)) {
          k = paramInt1;
          n = paramInt2;
        } 
      } 
    } 
    for (paramInt1 = paramInt4; paramInt1 < m; paramInt1++) {
      char c = paramSpanned.charAt(paramInt1);
      if (isSignChar(c))
        return ""; 
      if (isDecimalPointChar(c))
        k = paramInt1; 
    } 
    paramCharSequence = null;
    for (paramInt1 = j - 1, paramInt4 = paramInt2; paramInt1 >= i; paramInt1--, paramInt4 = m, k = n, paramCharSequence = charSequence) {
      int n;
      char c = charSequence2.charAt(paramInt1);
      boolean bool = false;
      if (isSignChar(c)) {
        if (paramInt1 != i || paramInt3 != 0) {
          paramInt2 = 1;
          m = paramInt4;
          n = k;
        } else if (paramInt4 >= 0) {
          paramInt2 = 1;
          m = paramInt4;
          n = k;
        } else {
          m = paramInt1;
          n = k;
          paramInt2 = bool;
        } 
      } else {
        m = paramInt4;
        n = k;
        paramInt2 = bool;
        if (isDecimalPointChar(c))
          if (k >= 0) {
            paramInt2 = 1;
            m = paramInt4;
            n = k;
          } else {
            n = paramInt1;
            paramInt2 = bool;
            m = paramInt4;
          }  
      } 
      CharSequence charSequence = paramCharSequence;
      if (paramInt2 != 0) {
        if (j == i + 1)
          return ""; 
        charSequence = paramCharSequence;
        if (paramCharSequence == null)
          charSequence = new SpannableStringBuilder(charSequence2, i, j); 
        charSequence.delete(paramInt1 - i, paramInt1 + 1 - i);
      } 
    } 
    if (paramCharSequence != null)
      return paramCharSequence; 
    if (charSequence1 != null)
      return charSequence1; 
    return null;
  }
}
