package android.text.method;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.Selection;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;

public class CharacterPickerDialog extends Dialog implements AdapterView.OnItemClickListener, View.OnClickListener {
  private Button mCancelButton;
  
  private LayoutInflater mInflater;
  
  private boolean mInsert;
  
  private String mOptions;
  
  private Editable mText;
  
  private View mView;
  
  public CharacterPickerDialog(Context paramContext, View paramView, Editable paramEditable, String paramString, boolean paramBoolean) {
    super(paramContext, 16973913);
    this.mView = paramView;
    this.mText = paramEditable;
    this.mOptions = paramString;
    this.mInsert = paramBoolean;
    this.mInflater = LayoutInflater.from(paramContext);
  }
  
  protected void onCreate(Bundle paramBundle) {
    super.onCreate(paramBundle);
    WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
    layoutParams.token = this.mView.getApplicationWindowToken();
    layoutParams.type = 1003;
    layoutParams.flags |= 0x1;
    setContentView(17367117);
    GridView gridView = (GridView)findViewById(16908833);
    gridView.setAdapter(new OptionsAdapter(getContext()));
    gridView.setOnItemClickListener(this);
    Button button = (Button)findViewById(16908824);
    button.setOnClickListener(this);
  }
  
  public void onItemClick(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong) {
    char c = this.mOptions.charAt(paramInt);
    replaceCharacterAndClose(String.valueOf(c));
  }
  
  private void replaceCharacterAndClose(CharSequence paramCharSequence) {
    int i = Selection.getSelectionEnd(this.mText);
    if (this.mInsert || i == 0) {
      this.mText.insert(i, paramCharSequence);
    } else {
      this.mText.replace(i - 1, i, paramCharSequence);
    } 
    dismiss();
  }
  
  public void onClick(View paramView) {
    if (paramView == this.mCancelButton) {
      dismiss();
    } else if (paramView instanceof Button) {
      CharSequence charSequence = ((Button)paramView).getText();
      replaceCharacterAndClose(charSequence);
    } 
  }
  
  private class OptionsAdapter extends BaseAdapter {
    final CharacterPickerDialog this$0;
    
    public OptionsAdapter(Context param1Context) {}
    
    public View getView(int param1Int, View param1View, ViewGroup param1ViewGroup) {
      CharacterPickerDialog characterPickerDialog = CharacterPickerDialog.this;
      Button button = (Button)characterPickerDialog.mInflater.inflate(17367118, (ViewGroup)null);
      button.setText(String.valueOf(CharacterPickerDialog.this.mOptions.charAt(param1Int)));
      button.setOnClickListener(CharacterPickerDialog.this);
      return button;
    }
    
    public final int getCount() {
      return CharacterPickerDialog.this.mOptions.length();
    }
    
    public final Object getItem(int param1Int) {
      return String.valueOf(CharacterPickerDialog.this.mOptions.charAt(param1Int));
    }
    
    public final long getItemId(int param1Int) {
      return param1Int;
    }
  }
}
