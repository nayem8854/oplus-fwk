package android.text.method;

import android.icu.lang.UCharacter;
import android.icu.text.BreakIterator;
import android.text.CharSequenceCharacterIterator;
import android.text.Selection;
import java.util.Locale;

public class WordIterator implements Selection.PositionIterator {
  private static final int WINDOW_WIDTH = 50;
  
  private CharSequence mCharSeq;
  
  private int mEnd;
  
  private final BreakIterator mIterator;
  
  private int mStart;
  
  public WordIterator() {
    this(Locale.getDefault());
  }
  
  public WordIterator(Locale paramLocale) {
    this.mIterator = BreakIterator.getWordInstance(paramLocale);
  }
  
  public void setCharSequence(CharSequence paramCharSequence, int paramInt1, int paramInt2) {
    if (paramInt1 >= 0 && paramInt2 <= paramCharSequence.length()) {
      this.mCharSeq = paramCharSequence;
      this.mStart = Math.max(0, paramInt1 - 50);
      this.mEnd = paramInt1 = Math.min(paramCharSequence.length(), paramInt2 + 50);
      this.mIterator.setText(new CharSequenceCharacterIterator(paramCharSequence, this.mStart, paramInt1));
      return;
    } 
    throw new IndexOutOfBoundsException("input indexes are outside the CharSequence");
  }
  
  public int preceding(int paramInt) {
    int i;
    checkOffsetIsValid(paramInt);
    while (true) {
      i = this.mIterator.preceding(paramInt);
      if (i != -1) {
        paramInt = i;
        if (isOnLetterOrDigit(i))
          break; 
        continue;
      } 
      break;
    } 
    return i;
  }
  
  public int following(int paramInt) {
    int i;
    checkOffsetIsValid(paramInt);
    while (true) {
      i = this.mIterator.following(paramInt);
      if (i != -1) {
        paramInt = i;
        if (isAfterLetterOrDigit(i))
          break; 
        continue;
      } 
      break;
    } 
    return i;
  }
  
  public boolean isBoundary(int paramInt) {
    checkOffsetIsValid(paramInt);
    return this.mIterator.isBoundary(paramInt);
  }
  
  public int nextBoundary(int paramInt) {
    checkOffsetIsValid(paramInt);
    return this.mIterator.following(paramInt);
  }
  
  public int prevBoundary(int paramInt) {
    checkOffsetIsValid(paramInt);
    return this.mIterator.preceding(paramInt);
  }
  
  public int getBeginning(int paramInt) {
    return getBeginning(paramInt, false);
  }
  
  public int getEnd(int paramInt) {
    return getEnd(paramInt, false);
  }
  
  public int getPrevWordBeginningOnTwoWordsBoundary(int paramInt) {
    return getBeginning(paramInt, true);
  }
  
  public int getNextWordEndOnTwoWordBoundary(int paramInt) {
    return getEnd(paramInt, true);
  }
  
  private int getBeginning(int paramInt, boolean paramBoolean) {
    checkOffsetIsValid(paramInt);
    if (isOnLetterOrDigit(paramInt)) {
      if (this.mIterator.isBoundary(paramInt) && (
        !isAfterLetterOrDigit(paramInt) || !paramBoolean))
        return paramInt; 
      return this.mIterator.preceding(paramInt);
    } 
    if (isAfterLetterOrDigit(paramInt))
      return this.mIterator.preceding(paramInt); 
    return -1;
  }
  
  private int getEnd(int paramInt, boolean paramBoolean) {
    checkOffsetIsValid(paramInt);
    if (isAfterLetterOrDigit(paramInt)) {
      if (this.mIterator.isBoundary(paramInt) && (
        !isOnLetterOrDigit(paramInt) || !paramBoolean))
        return paramInt; 
      return this.mIterator.following(paramInt);
    } 
    if (isOnLetterOrDigit(paramInt))
      return this.mIterator.following(paramInt); 
    return -1;
  }
  
  public int getPunctuationBeginning(int paramInt) {
    checkOffsetIsValid(paramInt);
    while (paramInt != -1 && !isPunctuationStartBoundary(paramInt))
      paramInt = prevBoundary(paramInt); 
    return paramInt;
  }
  
  public int getPunctuationEnd(int paramInt) {
    checkOffsetIsValid(paramInt);
    while (paramInt != -1 && !isPunctuationEndBoundary(paramInt))
      paramInt = nextBoundary(paramInt); 
    return paramInt;
  }
  
  public boolean isAfterPunctuation(int paramInt) {
    if (this.mStart < paramInt && paramInt <= this.mEnd) {
      paramInt = Character.codePointBefore(this.mCharSeq, paramInt);
      return isPunctuation(paramInt);
    } 
    return false;
  }
  
  public boolean isOnPunctuation(int paramInt) {
    if (this.mStart <= paramInt && paramInt < this.mEnd) {
      paramInt = Character.codePointAt(this.mCharSeq, paramInt);
      return isPunctuation(paramInt);
    } 
    return false;
  }
  
  public static boolean isMidWordPunctuation(Locale paramLocale, int paramInt) {
    paramInt = UCharacter.getIntPropertyValue(paramInt, 4116);
    return (paramInt == 4 || paramInt == 11 || paramInt == 15);
  }
  
  private boolean isPunctuationStartBoundary(int paramInt) {
    boolean bool;
    if (isOnPunctuation(paramInt) && !isAfterPunctuation(paramInt)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean isPunctuationEndBoundary(int paramInt) {
    boolean bool;
    if (!isOnPunctuation(paramInt) && isAfterPunctuation(paramInt)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private static boolean isPunctuation(int paramInt) {
    paramInt = Character.getType(paramInt);
    return (paramInt == 23 || paramInt == 20 || paramInt == 22 || paramInt == 30 || paramInt == 29 || paramInt == 24 || paramInt == 21);
  }
  
  private boolean isAfterLetterOrDigit(int paramInt) {
    if (this.mStart < paramInt && paramInt <= this.mEnd) {
      paramInt = Character.codePointBefore(this.mCharSeq, paramInt);
      if (Character.isLetterOrDigit(paramInt))
        return true; 
    } 
    return false;
  }
  
  private boolean isOnLetterOrDigit(int paramInt) {
    if (this.mStart <= paramInt && paramInt < this.mEnd) {
      paramInt = Character.codePointAt(this.mCharSeq, paramInt);
      if (Character.isLetterOrDigit(paramInt))
        return true; 
    } 
    return false;
  }
  
  private void checkOffsetIsValid(int paramInt) {
    if (this.mStart <= paramInt && paramInt <= this.mEnd)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid offset: ");
    stringBuilder.append(paramInt);
    stringBuilder.append(". Valid range is [");
    stringBuilder.append(this.mStart);
    stringBuilder.append(", ");
    stringBuilder.append(this.mEnd);
    stringBuilder.append("]");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
}
