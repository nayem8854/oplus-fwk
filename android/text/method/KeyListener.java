package android.text.method;

import android.text.Editable;
import android.view.KeyEvent;
import android.view.View;

public interface KeyListener {
  void clearMetaKeyState(View paramView, Editable paramEditable, int paramInt);
  
  int getInputType();
  
  boolean onKeyDown(View paramView, Editable paramEditable, int paramInt, KeyEvent paramKeyEvent);
  
  boolean onKeyOther(View paramView, Editable paramEditable, KeyEvent paramKeyEvent);
  
  boolean onKeyUp(View paramView, Editable paramEditable, int paramInt, KeyEvent paramKeyEvent);
}
