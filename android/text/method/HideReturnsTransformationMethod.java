package android.text.method;

public class HideReturnsTransformationMethod extends ReplacementTransformationMethod {
  private static char[] ORIGINAL = new char[] { '\r' };
  
  private static char[] REPLACEMENT = new char[] { '﻿' };
  
  private static HideReturnsTransformationMethod sInstance;
  
  protected char[] getOriginal() {
    return ORIGINAL;
  }
  
  protected char[] getReplacement() {
    return REPLACEMENT;
  }
  
  public static HideReturnsTransformationMethod getInstance() {
    HideReturnsTransformationMethod hideReturnsTransformationMethod = sInstance;
    if (hideReturnsTransformationMethod != null)
      return hideReturnsTransformationMethod; 
    sInstance = hideReturnsTransformationMethod = new HideReturnsTransformationMethod();
    return hideReturnsTransformationMethod;
  }
}
