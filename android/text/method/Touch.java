package android.text.method;

import android.text.Layout;
import android.text.NoCopySpan;
import android.text.Spannable;
import android.view.MotionEvent;
import android.widget.TextView;

public class Touch {
  public static void scrollTo(TextView paramTextView, Layout paramLayout, int paramInt1, int paramInt2) {
    boolean bool;
    int i = paramTextView.getTotalPaddingLeft(), j = paramTextView.getTotalPaddingRight();
    int k = paramTextView.getWidth() - i + j;
    int m = paramLayout.getLineForVertical(paramInt2);
    Layout.Alignment alignment = paramLayout.getParagraphAlignment(m);
    if (paramLayout.getParagraphDirection(m) > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    if (paramTextView.getHorizontallyScrolling()) {
      i = paramTextView.getTotalPaddingTop();
      j = paramTextView.getTotalPaddingBottom();
      int n = paramLayout.getLineForVertical(paramTextView.getHeight() + paramInt2 - i + j);
      j = Integer.MAX_VALUE;
      i = 0;
      for (; m <= n; m++) {
        j = (int)Math.min(j, paramLayout.getLineLeft(m));
        i = (int)Math.max(i, paramLayout.getLineRight(m));
      } 
    } else {
      j = 0;
      i = k;
    } 
    m = i - j;
    if (m < k) {
      if (alignment == Layout.Alignment.ALIGN_CENTER) {
        paramInt1 = j - (k - m) / 2;
      } else if ((bool && alignment == Layout.Alignment.ALIGN_OPPOSITE) || (!bool && alignment == Layout.Alignment.ALIGN_NORMAL) || alignment == Layout.Alignment.ALIGN_RIGHT) {
        paramInt1 = j - k - m;
      } else {
        paramInt1 = j;
      } 
    } else {
      paramInt1 = Math.min(paramInt1, i - k);
      paramInt1 = Math.max(paramInt1, j);
    } 
    paramTextView.scrollTo(paramInt1, paramInt2);
  }
  
  public static boolean onTouchEvent(TextView paramTextView, Spannable paramSpannable, MotionEvent paramMotionEvent) {
    // Byte code:
    //   0: aload_2
    //   1: invokevirtual getActionMasked : ()I
    //   4: istore_3
    //   5: iload_3
    //   6: ifeq -> 440
    //   9: iload_3
    //   10: iconst_1
    //   11: if_icmpeq -> 380
    //   14: iload_3
    //   15: iconst_2
    //   16: if_icmpeq -> 22
    //   19: goto -> 378
    //   22: aload_1
    //   23: iconst_0
    //   24: aload_1
    //   25: invokeinterface length : ()I
    //   30: ldc android/text/method/Touch$DragState
    //   32: invokeinterface getSpans : (IILjava/lang/Class;)[Ljava/lang/Object;
    //   37: checkcast [Landroid/text/method/Touch$DragState;
    //   40: astore #4
    //   42: aload #4
    //   44: arraylength
    //   45: ifle -> 378
    //   48: aload #4
    //   50: iconst_0
    //   51: aaload
    //   52: getfield mFarEnough : Z
    //   55: ifne -> 119
    //   58: aload_0
    //   59: invokevirtual getContext : ()Landroid/content/Context;
    //   62: invokestatic get : (Landroid/content/Context;)Landroid/view/ViewConfiguration;
    //   65: invokevirtual getScaledTouchSlop : ()I
    //   68: istore_3
    //   69: aload_2
    //   70: invokevirtual getX : ()F
    //   73: aload #4
    //   75: iconst_0
    //   76: aaload
    //   77: getfield mX : F
    //   80: fsub
    //   81: invokestatic abs : (F)F
    //   84: iload_3
    //   85: i2f
    //   86: fcmpl
    //   87: ifge -> 111
    //   90: aload_2
    //   91: invokevirtual getY : ()F
    //   94: aload #4
    //   96: iconst_0
    //   97: aaload
    //   98: getfield mY : F
    //   101: fsub
    //   102: invokestatic abs : (F)F
    //   105: iload_3
    //   106: i2f
    //   107: fcmpl
    //   108: iflt -> 119
    //   111: aload #4
    //   113: iconst_0
    //   114: aaload
    //   115: iconst_1
    //   116: putfield mFarEnough : Z
    //   119: aload #4
    //   121: iconst_0
    //   122: aaload
    //   123: getfield mFarEnough : Z
    //   126: ifeq -> 378
    //   129: aload #4
    //   131: iconst_0
    //   132: aaload
    //   133: iconst_1
    //   134: putfield mUsed : Z
    //   137: aload_2
    //   138: invokevirtual getMetaState : ()I
    //   141: iconst_1
    //   142: iand
    //   143: ifne -> 173
    //   146: aload_1
    //   147: iconst_1
    //   148: invokestatic getMetaState : (Ljava/lang/CharSequence;I)I
    //   151: iconst_1
    //   152: if_icmpeq -> 173
    //   155: aload_1
    //   156: sipush #2048
    //   159: invokestatic getMetaState : (Ljava/lang/CharSequence;I)I
    //   162: ifeq -> 168
    //   165: goto -> 173
    //   168: iconst_0
    //   169: istore_3
    //   170: goto -> 175
    //   173: iconst_1
    //   174: istore_3
    //   175: iload_3
    //   176: ifeq -> 210
    //   179: aload_2
    //   180: invokevirtual getX : ()F
    //   183: aload #4
    //   185: iconst_0
    //   186: aaload
    //   187: getfield mX : F
    //   190: fsub
    //   191: fstore #5
    //   193: aload_2
    //   194: invokevirtual getY : ()F
    //   197: aload #4
    //   199: iconst_0
    //   200: aaload
    //   201: getfield mY : F
    //   204: fsub
    //   205: fstore #6
    //   207: goto -> 238
    //   210: aload #4
    //   212: iconst_0
    //   213: aaload
    //   214: getfield mX : F
    //   217: aload_2
    //   218: invokevirtual getX : ()F
    //   221: fsub
    //   222: fstore #5
    //   224: aload #4
    //   226: iconst_0
    //   227: aaload
    //   228: getfield mY : F
    //   231: aload_2
    //   232: invokevirtual getY : ()F
    //   235: fsub
    //   236: fstore #6
    //   238: aload #4
    //   240: iconst_0
    //   241: aaload
    //   242: aload_2
    //   243: invokevirtual getX : ()F
    //   246: putfield mX : F
    //   249: aload #4
    //   251: iconst_0
    //   252: aaload
    //   253: aload_2
    //   254: invokevirtual getY : ()F
    //   257: putfield mY : F
    //   260: aload_0
    //   261: invokevirtual getScrollX : ()I
    //   264: istore_3
    //   265: fload #5
    //   267: f2i
    //   268: istore #7
    //   270: aload_0
    //   271: invokevirtual getScrollY : ()I
    //   274: istore #8
    //   276: fload #6
    //   278: f2i
    //   279: istore #9
    //   281: aload_0
    //   282: invokevirtual getTotalPaddingTop : ()I
    //   285: istore #10
    //   287: aload_0
    //   288: invokevirtual getTotalPaddingBottom : ()I
    //   291: istore #11
    //   293: aload_0
    //   294: invokevirtual getLayout : ()Landroid/text/Layout;
    //   297: astore_1
    //   298: iload #8
    //   300: iload #9
    //   302: iadd
    //   303: aload_1
    //   304: invokevirtual getHeight : ()I
    //   307: aload_0
    //   308: invokevirtual getHeight : ()I
    //   311: iload #10
    //   313: iload #11
    //   315: iadd
    //   316: isub
    //   317: isub
    //   318: invokestatic min : (II)I
    //   321: istore #11
    //   323: iload #11
    //   325: iconst_0
    //   326: invokestatic max : (II)I
    //   329: istore #11
    //   331: aload_0
    //   332: invokevirtual getScrollX : ()I
    //   335: istore #9
    //   337: aload_0
    //   338: invokevirtual getScrollY : ()I
    //   341: istore #10
    //   343: aload_0
    //   344: aload_1
    //   345: iload_3
    //   346: iload #7
    //   348: iadd
    //   349: iload #11
    //   351: invokestatic scrollTo : (Landroid/widget/TextView;Landroid/text/Layout;II)V
    //   354: iload #9
    //   356: aload_0
    //   357: invokevirtual getScrollX : ()I
    //   360: if_icmpne -> 372
    //   363: iload #10
    //   365: aload_0
    //   366: invokevirtual getScrollY : ()I
    //   369: if_icmpeq -> 376
    //   372: aload_0
    //   373: invokevirtual cancelLongPress : ()V
    //   376: iconst_1
    //   377: ireturn
    //   378: iconst_0
    //   379: ireturn
    //   380: aload_1
    //   381: iconst_0
    //   382: aload_1
    //   383: invokeinterface length : ()I
    //   388: ldc android/text/method/Touch$DragState
    //   390: invokeinterface getSpans : (IILjava/lang/Class;)[Ljava/lang/Object;
    //   395: checkcast [Landroid/text/method/Touch$DragState;
    //   398: astore_0
    //   399: iconst_0
    //   400: istore_3
    //   401: iload_3
    //   402: aload_0
    //   403: arraylength
    //   404: if_icmpge -> 422
    //   407: aload_1
    //   408: aload_0
    //   409: iload_3
    //   410: aaload
    //   411: invokeinterface removeSpan : (Ljava/lang/Object;)V
    //   416: iinc #3, 1
    //   419: goto -> 401
    //   422: aload_0
    //   423: arraylength
    //   424: ifle -> 438
    //   427: aload_0
    //   428: iconst_0
    //   429: aaload
    //   430: getfield mUsed : Z
    //   433: ifeq -> 438
    //   436: iconst_1
    //   437: ireturn
    //   438: iconst_0
    //   439: ireturn
    //   440: aload_1
    //   441: iconst_0
    //   442: aload_1
    //   443: invokeinterface length : ()I
    //   448: ldc android/text/method/Touch$DragState
    //   450: invokeinterface getSpans : (IILjava/lang/Class;)[Ljava/lang/Object;
    //   455: checkcast [Landroid/text/method/Touch$DragState;
    //   458: astore #4
    //   460: iconst_0
    //   461: istore_3
    //   462: iload_3
    //   463: aload #4
    //   465: arraylength
    //   466: if_icmpge -> 485
    //   469: aload_1
    //   470: aload #4
    //   472: iload_3
    //   473: aaload
    //   474: invokeinterface removeSpan : (Ljava/lang/Object;)V
    //   479: iinc #3, 1
    //   482: goto -> 462
    //   485: aload_2
    //   486: invokevirtual getX : ()F
    //   489: fstore #6
    //   491: aload_2
    //   492: invokevirtual getY : ()F
    //   495: fstore #5
    //   497: new android/text/method/Touch$DragState
    //   500: dup
    //   501: fload #6
    //   503: fload #5
    //   505: aload_0
    //   506: invokevirtual getScrollX : ()I
    //   509: aload_0
    //   510: invokevirtual getScrollY : ()I
    //   513: invokespecial <init> : (FFII)V
    //   516: astore_0
    //   517: aload_1
    //   518: aload_0
    //   519: iconst_0
    //   520: iconst_0
    //   521: bipush #17
    //   523: invokeinterface setSpan : (Ljava/lang/Object;III)V
    //   528: iconst_1
    //   529: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #92	-> 0
    //   #119	-> 22
    //   #121	-> 42
    //   #122	-> 48
    //   #123	-> 58
    //   #125	-> 69
    //   #126	-> 90
    //   #127	-> 111
    //   #131	-> 119
    //   #132	-> 129
    //   #133	-> 137
    //   #134	-> 146
    //   #136	-> 155
    //   #141	-> 175
    //   #144	-> 179
    //   #145	-> 193
    //   #147	-> 210
    //   #148	-> 224
    //   #150	-> 238
    //   #151	-> 249
    //   #153	-> 260
    //   #154	-> 270
    //   #156	-> 281
    //   #157	-> 293
    //   #159	-> 298
    //   #160	-> 323
    //   #162	-> 331
    //   #163	-> 337
    //   #165	-> 343
    //   #168	-> 354
    //   #169	-> 372
    //   #172	-> 376
    //   #177	-> 378
    //   #106	-> 380
    //   #108	-> 399
    //   #109	-> 407
    //   #108	-> 416
    //   #112	-> 422
    //   #113	-> 436
    //   #115	-> 438
    //   #94	-> 440
    //   #96	-> 460
    //   #97	-> 469
    //   #96	-> 479
    //   #100	-> 485
    //   #101	-> 497
    //   #100	-> 517
    //   #103	-> 528
  }
  
  public static int getInitialScrollX(TextView paramTextView, Spannable paramSpannable) {
    byte b;
    DragState[] arrayOfDragState = paramSpannable.<DragState>getSpans(0, paramSpannable.length(), DragState.class);
    if (arrayOfDragState.length > 0) {
      b = (arrayOfDragState[0]).mScrollX;
    } else {
      b = -1;
    } 
    return b;
  }
  
  public static int getInitialScrollY(TextView paramTextView, Spannable paramSpannable) {
    byte b;
    DragState[] arrayOfDragState = paramSpannable.<DragState>getSpans(0, paramSpannable.length(), DragState.class);
    if (arrayOfDragState.length > 0) {
      b = (arrayOfDragState[0]).mScrollY;
    } else {
      b = -1;
    } 
    return b;
  }
  
  class DragState implements NoCopySpan {
    public boolean mFarEnough;
    
    public int mScrollX;
    
    public int mScrollY;
    
    public boolean mUsed;
    
    public float mX;
    
    public float mY;
    
    public DragState(Touch this$0, float param1Float1, int param1Int1, int param1Int2) {
      this.mX = this$0;
      this.mY = param1Float1;
      this.mScrollX = param1Int1;
      this.mScrollY = param1Int2;
    }
  }
}
