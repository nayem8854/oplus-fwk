package android.text.method;

import android.graphics.Rect;
import android.text.Layout;
import android.text.Selection;
import android.text.Spannable;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.TextView;

public class ArrowKeyMovementMethod extends BaseMovementMethod implements MovementMethod {
  private static boolean isSelecting(Spannable paramSpannable) {
    boolean bool = true;
    if (MetaKeyKeyListener.getMetaState(paramSpannable, 1) != 1 && 
      MetaKeyKeyListener.getMetaState(paramSpannable, 2048) == 0)
      bool = false; 
    return bool;
  }
  
  private static int getCurrentLineTop(Spannable paramSpannable, Layout paramLayout) {
    return paramLayout.getLineTop(paramLayout.getLineForOffset(Selection.getSelectionEnd(paramSpannable)));
  }
  
  private static int getPageHeight(TextView paramTextView) {
    boolean bool;
    Rect rect = new Rect();
    if (paramTextView.getGlobalVisibleRect(rect)) {
      bool = rect.height();
    } else {
      bool = false;
    } 
    return bool;
  }
  
  protected boolean handleMovementKey(TextView paramTextView, Spannable paramSpannable, int paramInt1, int paramInt2, KeyEvent paramKeyEvent) {
    if (paramInt1 == 23)
      if (KeyEvent.metaStateHasNoModifiers(paramInt2) && 
        paramKeyEvent.getAction() == 0 && 
        paramKeyEvent.getRepeatCount() == 0 && 
        MetaKeyKeyListener.getMetaState(paramSpannable, 2048, paramKeyEvent) != 0)
        return paramTextView.showContextMenu();  
    return super.handleMovementKey(paramTextView, paramSpannable, paramInt1, paramInt2, paramKeyEvent);
  }
  
  protected boolean left(TextView paramTextView, Spannable paramSpannable) {
    Layout layout = paramTextView.getLayout();
    if (isSelecting(paramSpannable))
      return Selection.extendLeft(paramSpannable, layout); 
    return Selection.moveLeft(paramSpannable, layout);
  }
  
  protected boolean right(TextView paramTextView, Spannable paramSpannable) {
    Layout layout = paramTextView.getLayout();
    if (isSelecting(paramSpannable))
      return Selection.extendRight(paramSpannable, layout); 
    return Selection.moveRight(paramSpannable, layout);
  }
  
  protected boolean up(TextView paramTextView, Spannable paramSpannable) {
    Layout layout = paramTextView.getLayout();
    if (isSelecting(paramSpannable))
      return Selection.extendUp(paramSpannable, layout); 
    return Selection.moveUp(paramSpannable, layout);
  }
  
  protected boolean down(TextView paramTextView, Spannable paramSpannable) {
    Layout layout = paramTextView.getLayout();
    if (isSelecting(paramSpannable))
      return Selection.extendDown(paramSpannable, layout); 
    return Selection.moveDown(paramSpannable, layout);
  }
  
  protected boolean pageUp(TextView paramTextView, Spannable paramSpannable) {
    Layout layout = paramTextView.getLayout();
    boolean bool = isSelecting(paramSpannable);
    int i = getCurrentLineTop(paramSpannable, layout), j = getPageHeight(paramTextView);
    boolean bool1 = false;
    while (true) {
      int k = Selection.getSelectionEnd(paramSpannable);
      if (bool) {
        Selection.extendUp(paramSpannable, layout);
      } else {
        Selection.moveUp(paramSpannable, layout);
      } 
      if (Selection.getSelectionEnd(paramSpannable) != k) {
        bool1 = true;
        boolean bool2 = true;
        if (getCurrentLineTop(paramSpannable, layout) > i - j) {
          bool1 = bool2;
          continue;
        } 
      } 
      return bool1;
    } 
  }
  
  protected boolean pageDown(TextView paramTextView, Spannable paramSpannable) {
    Layout layout = paramTextView.getLayout();
    boolean bool = isSelecting(paramSpannable);
    int i = getCurrentLineTop(paramSpannable, layout), j = getPageHeight(paramTextView);
    boolean bool1 = false;
    while (true) {
      int k = Selection.getSelectionEnd(paramSpannable);
      if (bool) {
        Selection.extendDown(paramSpannable, layout);
      } else {
        Selection.moveDown(paramSpannable, layout);
      } 
      if (Selection.getSelectionEnd(paramSpannable) == k)
        break; 
      boolean bool2 = true;
      bool1 = true;
      if (getCurrentLineTop(paramSpannable, layout) >= i + j) {
        bool1 = bool2;
        break;
      } 
    } 
    return bool1;
  }
  
  protected boolean top(TextView paramTextView, Spannable paramSpannable) {
    if (isSelecting(paramSpannable)) {
      Selection.extendSelection(paramSpannable, 0);
    } else {
      Selection.setSelection(paramSpannable, 0);
    } 
    return true;
  }
  
  protected boolean bottom(TextView paramTextView, Spannable paramSpannable) {
    if (isSelecting(paramSpannable)) {
      Selection.extendSelection(paramSpannable, paramSpannable.length());
    } else {
      Selection.setSelection(paramSpannable, paramSpannable.length());
    } 
    return true;
  }
  
  protected boolean lineStart(TextView paramTextView, Spannable paramSpannable) {
    Layout layout = paramTextView.getLayout();
    if (isSelecting(paramSpannable))
      return Selection.extendToLeftEdge(paramSpannable, layout); 
    return Selection.moveToLeftEdge(paramSpannable, layout);
  }
  
  protected boolean lineEnd(TextView paramTextView, Spannable paramSpannable) {
    Layout layout = paramTextView.getLayout();
    if (isSelecting(paramSpannable))
      return Selection.extendToRightEdge(paramSpannable, layout); 
    return Selection.moveToRightEdge(paramSpannable, layout);
  }
  
  protected boolean leftWord(TextView paramTextView, Spannable paramSpannable) {
    int i = paramTextView.getSelectionEnd();
    WordIterator wordIterator = paramTextView.getWordIterator();
    wordIterator.setCharSequence(paramSpannable, i, i);
    return Selection.moveToPreceding(paramSpannable, wordIterator, isSelecting(paramSpannable));
  }
  
  protected boolean rightWord(TextView paramTextView, Spannable paramSpannable) {
    int i = paramTextView.getSelectionEnd();
    WordIterator wordIterator = paramTextView.getWordIterator();
    wordIterator.setCharSequence(paramSpannable, i, i);
    return Selection.moveToFollowing(paramSpannable, wordIterator, isSelecting(paramSpannable));
  }
  
  protected boolean home(TextView paramTextView, Spannable paramSpannable) {
    return lineStart(paramTextView, paramSpannable);
  }
  
  protected boolean end(TextView paramTextView, Spannable paramSpannable) {
    return lineEnd(paramTextView, paramSpannable);
  }
  
  public boolean onTouchEvent(TextView paramTextView, Spannable paramSpannable, MotionEvent paramMotionEvent) {
    int i = -1;
    int j = -1;
    int k = paramMotionEvent.getAction();
    if (k == 1) {
      i = Touch.getInitialScrollX(paramTextView, paramSpannable);
      j = Touch.getInitialScrollY(paramTextView, paramSpannable);
    } 
    boolean bool1 = isSelecting(paramSpannable);
    boolean bool2 = Touch.onTouchEvent(paramTextView, paramSpannable, paramMotionEvent);
    if (paramTextView.didTouchFocusSelect())
      return bool2; 
    if (k == 0) {
      if (isSelecting(paramSpannable)) {
        if (!paramTextView.isFocused() && 
          !paramTextView.requestFocus())
          return bool2; 
        j = paramTextView.getOffsetForPosition(paramMotionEvent.getX(), paramMotionEvent.getY());
        paramSpannable.setSpan(LAST_TAP_DOWN, j, j, 34);
        paramTextView.getParent().requestDisallowInterceptTouchEvent(true);
      } 
    } else if (paramTextView.isFocused()) {
      if (k == 2) {
        if (isSelecting(paramSpannable) && bool2) {
          k = paramSpannable.getSpanStart(LAST_TAP_DOWN);
          paramTextView.cancelLongPress();
          i = paramTextView.getOffsetForPosition(paramMotionEvent.getX(), paramMotionEvent.getY());
          j = Math.min(k, i);
          i = Math.max(k, i);
          Selection.setSelection(paramSpannable, j, i);
          return true;
        } 
      } else if (k == 1) {
        if ((j >= 0 && j != paramTextView.getScrollY()) || (i >= 0 && 
          i != paramTextView.getScrollX())) {
          paramTextView.moveCursorToVisibleOffset();
          return true;
        } 
        if (bool1) {
          i = paramSpannable.getSpanStart(LAST_TAP_DOWN);
          k = paramTextView.getOffsetForPosition(paramMotionEvent.getX(), paramMotionEvent.getY());
          j = Math.min(i, k);
          i = Math.max(i, k);
          Selection.setSelection(paramSpannable, j, i);
          paramSpannable.removeSpan(LAST_TAP_DOWN);
        } 
        MetaKeyKeyListener.adjustMetaAfterKeypress(paramSpannable);
        MetaKeyKeyListener.resetLockedMeta(paramSpannable);
        return true;
      } 
    } 
    return bool2;
  }
  
  public boolean canSelectArbitrarily() {
    return true;
  }
  
  public void initialize(TextView paramTextView, Spannable paramSpannable) {
    Selection.setSelection(paramSpannable, 0);
  }
  
  public void onTakeFocus(TextView paramTextView, Spannable paramSpannable, int paramInt) {
    if ((paramInt & 0x82) != 0) {
      if (paramTextView.getLayout() == null)
        Selection.setSelection(paramSpannable, paramSpannable.length()); 
    } else {
      Selection.setSelection(paramSpannable, paramSpannable.length());
    } 
  }
  
  public static MovementMethod getInstance() {
    if (sInstance == null)
      sInstance = new ArrowKeyMovementMethod(); 
    return sInstance;
  }
  
  private static final Object LAST_TAP_DOWN = new Object();
  
  private static ArrowKeyMovementMethod sInstance;
}
