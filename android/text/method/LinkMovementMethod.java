package android.text.method;

import android.text.Layout;
import android.text.NoCopySpan;
import android.text.Selection;
import android.text.Spannable;
import android.text.style.ClickableSpan;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.textclassifier.TextLinks;
import android.widget.TextView;

public class LinkMovementMethod extends ScrollingMovementMethod {
  private static final int CLICK = 1;
  
  private static final int DOWN = 3;
  
  public boolean canSelectArbitrarily() {
    return true;
  }
  
  protected boolean handleMovementKey(TextView paramTextView, Spannable paramSpannable, int paramInt1, int paramInt2, KeyEvent paramKeyEvent) {
    if (paramInt1 == 23 || paramInt1 == 66)
      if (KeyEvent.metaStateHasNoModifiers(paramInt2) && 
        paramKeyEvent.getAction() == 0 && 
        paramKeyEvent.getRepeatCount() == 0 && action(1, paramTextView, paramSpannable))
        return true;  
    return super.handleMovementKey(paramTextView, paramSpannable, paramInt1, paramInt2, paramKeyEvent);
  }
  
  protected boolean up(TextView paramTextView, Spannable paramSpannable) {
    if (action(2, paramTextView, paramSpannable))
      return true; 
    return super.up(paramTextView, paramSpannable);
  }
  
  protected boolean down(TextView paramTextView, Spannable paramSpannable) {
    if (action(3, paramTextView, paramSpannable))
      return true; 
    return super.down(paramTextView, paramSpannable);
  }
  
  protected boolean left(TextView paramTextView, Spannable paramSpannable) {
    if (action(2, paramTextView, paramSpannable))
      return true; 
    return super.left(paramTextView, paramSpannable);
  }
  
  protected boolean right(TextView paramTextView, Spannable paramSpannable) {
    if (action(3, paramTextView, paramSpannable))
      return true; 
    return super.right(paramTextView, paramSpannable);
  }
  
  private boolean action(int paramInt, TextView paramTextView, Spannable paramSpannable) {
    // Byte code:
    //   0: aload_2
    //   1: invokevirtual getLayout : ()Landroid/text/Layout;
    //   4: astore #4
    //   6: aload_2
    //   7: invokevirtual getTotalPaddingTop : ()I
    //   10: istore #5
    //   12: aload_2
    //   13: invokevirtual getTotalPaddingBottom : ()I
    //   16: istore #6
    //   18: aload_2
    //   19: invokevirtual getScrollY : ()I
    //   22: istore #7
    //   24: aload_2
    //   25: invokevirtual getHeight : ()I
    //   28: iload #7
    //   30: iadd
    //   31: iload #5
    //   33: iload #6
    //   35: iadd
    //   36: isub
    //   37: istore #8
    //   39: aload #4
    //   41: iload #7
    //   43: invokevirtual getLineForVertical : (I)I
    //   46: istore #5
    //   48: aload #4
    //   50: iload #8
    //   52: invokevirtual getLineForVertical : (I)I
    //   55: istore #6
    //   57: aload #4
    //   59: iload #5
    //   61: invokevirtual getLineStart : (I)I
    //   64: istore #9
    //   66: aload #4
    //   68: iload #6
    //   70: invokevirtual getLineEnd : (I)I
    //   73: istore #10
    //   75: aload_3
    //   76: iload #9
    //   78: iload #10
    //   80: ldc android/text/style/ClickableSpan
    //   82: invokeinterface getSpans : (IILjava/lang/Class;)[Ljava/lang/Object;
    //   87: checkcast [Landroid/text/style/ClickableSpan;
    //   90: astore #4
    //   92: aload_3
    //   93: invokestatic getSelectionStart : (Ljava/lang/CharSequence;)I
    //   96: istore #11
    //   98: aload_3
    //   99: invokestatic getSelectionEnd : (Ljava/lang/CharSequence;)I
    //   102: istore #6
    //   104: iload #11
    //   106: iload #6
    //   108: invokestatic min : (II)I
    //   111: istore #5
    //   113: iload #11
    //   115: iload #6
    //   117: invokestatic max : (II)I
    //   120: istore #6
    //   122: iload #5
    //   124: ifge -> 154
    //   127: aload_3
    //   128: getstatic android/text/method/LinkMovementMethod.FROM_BELOW : Ljava/lang/Object;
    //   131: invokeinterface getSpanStart : (Ljava/lang/Object;)I
    //   136: iflt -> 154
    //   139: aload_3
    //   140: invokeinterface length : ()I
    //   145: istore #5
    //   147: iload #5
    //   149: istore #6
    //   151: goto -> 154
    //   154: iload #5
    //   156: istore #11
    //   158: iload #5
    //   160: iload #10
    //   162: if_icmple -> 173
    //   165: ldc 2147483647
    //   167: istore #6
    //   169: ldc 2147483647
    //   171: istore #11
    //   173: iload #6
    //   175: istore #5
    //   177: iload #6
    //   179: iload #9
    //   181: if_icmpge -> 190
    //   184: iconst_m1
    //   185: istore #5
    //   187: iconst_m1
    //   188: istore #11
    //   190: iload_1
    //   191: iconst_1
    //   192: if_icmpeq -> 440
    //   195: iload_1
    //   196: iconst_2
    //   197: if_icmpeq -> 335
    //   200: iload_1
    //   201: iconst_3
    //   202: if_icmpeq -> 208
    //   205: goto -> 501
    //   208: ldc 2147483647
    //   210: istore #8
    //   212: ldc 2147483647
    //   214: istore #10
    //   216: iconst_0
    //   217: istore #6
    //   219: iload #7
    //   221: istore_1
    //   222: iload #8
    //   224: istore #7
    //   226: iload #6
    //   228: aload #4
    //   230: arraylength
    //   231: if_icmpge -> 315
    //   234: aload_3
    //   235: aload #4
    //   237: iload #6
    //   239: aaload
    //   240: invokeinterface getSpanStart : (Ljava/lang/Object;)I
    //   245: istore #12
    //   247: iload #12
    //   249: iload #11
    //   251: if_icmpgt -> 269
    //   254: iload #10
    //   256: istore #9
    //   258: iload #7
    //   260: istore #8
    //   262: iload #11
    //   264: iload #5
    //   266: if_icmpne -> 301
    //   269: iload #10
    //   271: istore #9
    //   273: iload #7
    //   275: istore #8
    //   277: iload #12
    //   279: iload #7
    //   281: if_icmpge -> 301
    //   284: aload_3
    //   285: aload #4
    //   287: iload #6
    //   289: aaload
    //   290: invokeinterface getSpanEnd : (Ljava/lang/Object;)I
    //   295: istore #9
    //   297: iload #12
    //   299: istore #8
    //   301: iinc #6, 1
    //   304: iload #9
    //   306: istore #10
    //   308: iload #8
    //   310: istore #7
    //   312: goto -> 226
    //   315: iload #10
    //   317: ldc 2147483647
    //   319: if_icmpge -> 332
    //   322: aload_3
    //   323: iload #7
    //   325: iload #10
    //   327: invokestatic setSelection : (Landroid/text/Spannable;II)V
    //   330: iconst_1
    //   331: ireturn
    //   332: goto -> 501
    //   335: iconst_m1
    //   336: istore #9
    //   338: iconst_m1
    //   339: istore #7
    //   341: iconst_0
    //   342: istore #6
    //   344: iload #8
    //   346: istore_1
    //   347: iload #9
    //   349: istore #8
    //   351: iload #6
    //   353: aload #4
    //   355: arraylength
    //   356: if_icmpge -> 425
    //   359: aload_3
    //   360: aload #4
    //   362: iload #6
    //   364: aaload
    //   365: invokeinterface getSpanEnd : (Ljava/lang/Object;)I
    //   370: istore #9
    //   372: iload #9
    //   374: iload #5
    //   376: if_icmplt -> 392
    //   379: iload #11
    //   381: iload #5
    //   383: if_icmpne -> 389
    //   386: goto -> 392
    //   389: goto -> 419
    //   392: iload #9
    //   394: iload #7
    //   396: if_icmple -> 419
    //   399: aload_3
    //   400: aload #4
    //   402: iload #6
    //   404: aaload
    //   405: invokeinterface getSpanStart : (Ljava/lang/Object;)I
    //   410: istore #8
    //   412: iload #9
    //   414: istore #7
    //   416: goto -> 419
    //   419: iinc #6, 1
    //   422: goto -> 351
    //   425: iload #8
    //   427: iflt -> 501
    //   430: aload_3
    //   431: iload #7
    //   433: iload #8
    //   435: invokestatic setSelection : (Landroid/text/Spannable;II)V
    //   438: iconst_1
    //   439: ireturn
    //   440: iload #11
    //   442: iload #5
    //   444: if_icmpne -> 449
    //   447: iconst_0
    //   448: ireturn
    //   449: aload_3
    //   450: iload #11
    //   452: iload #5
    //   454: ldc android/text/style/ClickableSpan
    //   456: invokeinterface getSpans : (IILjava/lang/Class;)[Ljava/lang/Object;
    //   461: checkcast [Landroid/text/style/ClickableSpan;
    //   464: astore_3
    //   465: aload_3
    //   466: arraylength
    //   467: iconst_1
    //   468: if_icmpeq -> 473
    //   471: iconst_0
    //   472: ireturn
    //   473: aload_3
    //   474: iconst_0
    //   475: aaload
    //   476: astore_3
    //   477: aload_3
    //   478: instanceof android/view/textclassifier/TextLinks$TextLinkSpan
    //   481: ifeq -> 496
    //   484: aload_3
    //   485: checkcast android/view/textclassifier/TextLinks$TextLinkSpan
    //   488: aload_2
    //   489: iconst_1
    //   490: invokevirtual onClick : (Landroid/view/View;I)V
    //   493: goto -> 501
    //   496: aload_3
    //   497: aload_2
    //   498: invokevirtual onClick : (Landroid/view/View;)V
    //   501: iconst_0
    //   502: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #102	-> 0
    //   #104	-> 6
    //   #105	-> 12
    //   #106	-> 18
    //   #107	-> 24
    //   #109	-> 39
    //   #110	-> 48
    //   #112	-> 57
    //   #113	-> 66
    //   #115	-> 75
    //   #117	-> 92
    //   #118	-> 98
    //   #120	-> 104
    //   #121	-> 113
    //   #123	-> 122
    //   #124	-> 127
    //   #125	-> 139
    //   #123	-> 154
    //   #129	-> 154
    //   #130	-> 165
    //   #131	-> 173
    //   #132	-> 184
    //   #134	-> 190
    //   #179	-> 208
    //   #180	-> 212
    //   #182	-> 212
    //   #183	-> 234
    //   #185	-> 247
    //   #186	-> 269
    //   #187	-> 284
    //   #188	-> 284
    //   #182	-> 301
    //   #193	-> 315
    //   #194	-> 322
    //   #195	-> 330
    //   #193	-> 332
    //   #157	-> 335
    //   #158	-> 338
    //   #160	-> 341
    //   #161	-> 359
    //   #163	-> 372
    //   #164	-> 392
    //   #165	-> 399
    //   #166	-> 412
    //   #164	-> 419
    //   #160	-> 419
    //   #171	-> 425
    //   #172	-> 430
    //   #173	-> 438
    //   #136	-> 440
    //   #137	-> 447
    //   #140	-> 449
    //   #142	-> 465
    //   #143	-> 471
    //   #146	-> 473
    //   #147	-> 477
    //   #148	-> 484
    //   #150	-> 496
    //   #152	-> 501
    //   #201	-> 501
  }
  
  public boolean onTouchEvent(TextView paramTextView, Spannable paramSpannable, MotionEvent paramMotionEvent) {
    ClickableSpan clickableSpan;
    int i = paramMotionEvent.getAction();
    if (i == 1 || i == 0) {
      int j = (int)paramMotionEvent.getX();
      int k = (int)paramMotionEvent.getY();
      int m = paramTextView.getTotalPaddingLeft();
      int n = paramTextView.getTotalPaddingTop();
      int i1 = paramTextView.getScrollX();
      int i2 = paramTextView.getScrollY();
      Layout layout = paramTextView.getLayout();
      i2 = layout.getLineForVertical(k - n + i2);
      i1 = layout.getOffsetForHorizontal(i2, (j - m + i1));
      ClickableSpan[] arrayOfClickableSpan = paramSpannable.<ClickableSpan>getSpans(i1, i1, ClickableSpan.class);
      if (arrayOfClickableSpan.length != 0) {
        clickableSpan = arrayOfClickableSpan[0];
        if (i == 1) {
          if (clickableSpan instanceof TextLinks.TextLinkSpan) {
            ((TextLinks.TextLinkSpan)clickableSpan).onClick(paramTextView, 0);
          } else {
            clickableSpan.onClick(paramTextView);
          } 
        } else if (i == 0) {
          if ((paramTextView.getContext().getApplicationInfo()).targetSdkVersion >= 28)
            paramTextView.hideFloatingToolbar(200); 
          i1 = paramSpannable.getSpanStart(clickableSpan);
          i = paramSpannable.getSpanEnd(clickableSpan);
          Selection.setSelection(paramSpannable, i1, i);
        } 
        return true;
      } 
      Selection.removeSelection(paramSpannable);
    } 
    return super.onTouchEvent(paramTextView, paramSpannable, (MotionEvent)clickableSpan);
  }
  
  public void initialize(TextView paramTextView, Spannable paramSpannable) {
    Selection.removeSelection(paramSpannable);
    paramSpannable.removeSpan(FROM_BELOW);
  }
  
  public void onTakeFocus(TextView paramTextView, Spannable paramSpannable, int paramInt) {
    Selection.removeSelection(paramSpannable);
    if ((paramInt & 0x1) != 0) {
      paramSpannable.setSpan(FROM_BELOW, 0, 0, 34);
    } else {
      paramSpannable.removeSpan(FROM_BELOW);
    } 
  }
  
  public static MovementMethod getInstance() {
    if (sInstance == null)
      sInstance = new LinkMovementMethod(); 
    return sInstance;
  }
  
  private static Object FROM_BELOW = new NoCopySpan.Concrete();
  
  private static final int HIDE_FLOATING_TOOLBAR_DELAY_MS = 200;
  
  private static final int UP = 2;
  
  private static LinkMovementMethod sInstance;
}
