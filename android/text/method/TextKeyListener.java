package android.text.method;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.os.Handler;
import android.provider.Settings;
import android.text.Editable;
import android.text.NoCopySpan;
import android.text.Selection;
import android.text.SpanWatcher;
import android.text.Spannable;
import android.text.TextUtils;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.View;
import java.lang.ref.WeakReference;

public class TextKeyListener extends BaseKeyListener implements SpanWatcher {
  static final Object ACTIVE;
  
  static final int AUTO_CAP = 1;
  
  static final int AUTO_PERIOD = 4;
  
  static final int AUTO_TEXT = 2;
  
  static final Object CAPPED;
  
  static final Object INHIBIT_REPLACEMENT;
  
  static final Object LAST_TYPED;
  
  static final int SHOW_PASSWORD = 8;
  
  private static TextKeyListener[] sInstance = new TextKeyListener[(Capitalize.values()).length * 2];
  
  private Capitalize mAutoCap;
  
  private boolean mAutoText;
  
  private SettingsObserver mObserver;
  
  private int mPrefs;
  
  private boolean mPrefsInited;
  
  private WeakReference<ContentResolver> mResolver;
  
  static {
    ACTIVE = new NoCopySpan.Concrete();
    CAPPED = new NoCopySpan.Concrete();
    INHIBIT_REPLACEMENT = new NoCopySpan.Concrete();
    LAST_TYPED = new NoCopySpan.Concrete();
  }
  
  public TextKeyListener(Capitalize paramCapitalize, boolean paramBoolean) {
    this.mAutoCap = paramCapitalize;
    this.mAutoText = paramBoolean;
  }
  
  public static TextKeyListener getInstance(boolean paramBoolean, Capitalize paramCapitalize) {
    int i = paramCapitalize.ordinal() * 2 + paramBoolean;
    TextKeyListener[] arrayOfTextKeyListener = sInstance;
    if (arrayOfTextKeyListener[i] == null)
      arrayOfTextKeyListener[i] = new TextKeyListener(paramCapitalize, paramBoolean); 
    return sInstance[i];
  }
  
  public static TextKeyListener getInstance() {
    return getInstance(false, Capitalize.NONE);
  }
  
  public static boolean shouldCap(Capitalize paramCapitalize, CharSequence paramCharSequence, int paramInt) {
    char c;
    Capitalize capitalize = Capitalize.NONE;
    boolean bool = false;
    if (paramCapitalize == capitalize)
      return false; 
    if (paramCapitalize == Capitalize.CHARACTERS)
      return true; 
    if (paramCapitalize == Capitalize.WORDS) {
      c = ' ';
    } else {
      c = '䀀';
    } 
    if (TextUtils.getCapsMode(paramCharSequence, paramInt, c) != 0)
      bool = true; 
    return bool;
  }
  
  public int getInputType() {
    return makeTextContentType(this.mAutoCap, this.mAutoText);
  }
  
  public boolean onKeyDown(View paramView, Editable paramEditable, int paramInt, KeyEvent paramKeyEvent) {
    KeyListener keyListener = getKeyListener(paramKeyEvent);
    return keyListener.onKeyDown(paramView, paramEditable, paramInt, paramKeyEvent);
  }
  
  public boolean onKeyUp(View paramView, Editable paramEditable, int paramInt, KeyEvent paramKeyEvent) {
    KeyListener keyListener = getKeyListener(paramKeyEvent);
    return keyListener.onKeyUp(paramView, paramEditable, paramInt, paramKeyEvent);
  }
  
  public boolean onKeyOther(View paramView, Editable paramEditable, KeyEvent paramKeyEvent) {
    KeyListener keyListener = getKeyListener(paramKeyEvent);
    return keyListener.onKeyOther(paramView, paramEditable, paramKeyEvent);
  }
  
  public static void clear(Editable paramEditable) {
    paramEditable.clear();
    paramEditable.removeSpan(ACTIVE);
    paramEditable.removeSpan(CAPPED);
    paramEditable.removeSpan(INHIBIT_REPLACEMENT);
    paramEditable.removeSpan(LAST_TYPED);
    QwertyKeyListener.Replaced[] arrayOfReplaced = paramEditable.<QwertyKeyListener.Replaced>getSpans(0, paramEditable.length(), QwertyKeyListener.Replaced.class);
    int i = arrayOfReplaced.length;
    for (byte b = 0; b < i; b++)
      paramEditable.removeSpan(arrayOfReplaced[b]); 
  }
  
  public void onSpanAdded(Spannable paramSpannable, Object paramObject, int paramInt1, int paramInt2) {}
  
  public void onSpanRemoved(Spannable paramSpannable, Object paramObject, int paramInt1, int paramInt2) {}
  
  public void onSpanChanged(Spannable paramSpannable, Object paramObject, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (paramObject == Selection.SELECTION_END)
      paramSpannable.removeSpan(ACTIVE); 
  }
  
  private KeyListener getKeyListener(KeyEvent paramKeyEvent) {
    KeyCharacterMap keyCharacterMap = paramKeyEvent.getKeyCharacterMap();
    int i = keyCharacterMap.getKeyboardType();
    if (i == 3)
      return QwertyKeyListener.getInstance(this.mAutoText, this.mAutoCap); 
    if (i == 1)
      return MultiTapKeyListener.getInstance(this.mAutoText, this.mAutoCap); 
    if (i == 4 || i == 5)
      return QwertyKeyListener.getInstanceForFullKeyboard(); 
    return NullKeyListener.getInstance();
  }
  
  class Capitalize extends Enum<Capitalize> {
    private static final Capitalize[] $VALUES;
    
    public static final Capitalize CHARACTERS;
    
    public static Capitalize[] values() {
      return (Capitalize[])$VALUES.clone();
    }
    
    public static Capitalize valueOf(String param1String) {
      return Enum.<Capitalize>valueOf(Capitalize.class, param1String);
    }
    
    private Capitalize(TextKeyListener this$0, int param1Int) {
      super((String)this$0, param1Int);
    }
    
    public static final Capitalize NONE = new Capitalize("NONE", 0), SENTENCES = new Capitalize("SENTENCES", 1), WORDS = new Capitalize("WORDS", 2);
    
    static {
      Capitalize capitalize = new Capitalize("CHARACTERS", 3);
      $VALUES = new Capitalize[] { NONE, SENTENCES, WORDS, capitalize };
    }
  }
  
  class NullKeyListener implements KeyListener {
    private static NullKeyListener sInstance;
    
    public int getInputType() {
      return 0;
    }
    
    public boolean onKeyDown(View param1View, Editable param1Editable, int param1Int, KeyEvent param1KeyEvent) {
      return false;
    }
    
    public boolean onKeyUp(View param1View, Editable param1Editable, int param1Int, KeyEvent param1KeyEvent) {
      return false;
    }
    
    public boolean onKeyOther(View param1View, Editable param1Editable, KeyEvent param1KeyEvent) {
      return false;
    }
    
    public void clearMetaKeyState(View param1View, Editable param1Editable, int param1Int) {}
    
    public static NullKeyListener getInstance() {
      NullKeyListener nullKeyListener = sInstance;
      if (nullKeyListener != null)
        return nullKeyListener; 
      sInstance = nullKeyListener = new NullKeyListener();
      return nullKeyListener;
    }
  }
  
  public void release() {
    WeakReference<ContentResolver> weakReference = this.mResolver;
    if (weakReference != null) {
      ContentResolver contentResolver = weakReference.get();
      if (contentResolver != null) {
        contentResolver.unregisterContentObserver(this.mObserver);
        this.mResolver.clear();
      } 
      this.mObserver = null;
      this.mResolver = null;
      this.mPrefsInited = false;
    } 
  }
  
  private void initPrefs(Context paramContext) {
    ContentResolver contentResolver = paramContext.getContentResolver();
    this.mResolver = new WeakReference<>(contentResolver);
    if (this.mObserver == null) {
      this.mObserver = new SettingsObserver();
      contentResolver.registerContentObserver(Settings.System.CONTENT_URI, true, this.mObserver);
    } 
    updatePrefs(contentResolver);
    this.mPrefsInited = true;
  }
  
  class SettingsObserver extends ContentObserver {
    final TextKeyListener this$0;
    
    public SettingsObserver() {
      super(new Handler());
    }
    
    public void onChange(boolean param1Boolean) {
      if (TextKeyListener.this.mResolver != null) {
        ContentResolver contentResolver = TextKeyListener.this.mResolver.get();
        if (contentResolver == null) {
          TextKeyListener.access$102(TextKeyListener.this, false);
        } else {
          TextKeyListener.this.updatePrefs(contentResolver);
        } 
      } else {
        TextKeyListener.access$102(TextKeyListener.this, false);
      } 
    }
  }
  
  private void updatePrefs(ContentResolver paramContentResolver) {
    byte b3, b4, b5, b1 = 1;
    int i = Settings.System.getInt(paramContentResolver, "auto_caps", 1);
    byte b2 = 0;
    if (i > 0) {
      b3 = 1;
    } else {
      b3 = 0;
    } 
    if (Settings.System.getInt(paramContentResolver, "auto_replace", 1) > 0) {
      b4 = 1;
    } else {
      b4 = 0;
    } 
    if (Settings.System.getInt(paramContentResolver, "auto_punctuate", 1) > 0) {
      b5 = 1;
    } else {
      b5 = 0;
    } 
    if (Settings.System.getInt(paramContentResolver, "show_password", 1) > 0) {
      i = 1;
    } else {
      i = 0;
    } 
    if (b3) {
      b3 = b1;
    } else {
      b3 = 0;
    } 
    if (b4) {
      b4 = 2;
    } else {
      b4 = 0;
    } 
    if (b5) {
      b5 = 4;
    } else {
      b5 = 0;
    } 
    if (i != 0)
      b2 = 8; 
    this.mPrefs = b3 | b4 | b5 | b2;
  }
  
  int getPrefs(Context paramContext) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mPrefsInited : Z
    //   6: ifeq -> 19
    //   9: aload_0
    //   10: getfield mResolver : Ljava/lang/ref/WeakReference;
    //   13: invokevirtual get : ()Ljava/lang/Object;
    //   16: ifnonnull -> 24
    //   19: aload_0
    //   20: aload_1
    //   21: invokespecial initPrefs : (Landroid/content/Context;)V
    //   24: aload_0
    //   25: monitorexit
    //   26: aload_0
    //   27: getfield mPrefs : I
    //   30: ireturn
    //   31: astore_1
    //   32: aload_0
    //   33: monitorexit
    //   34: aload_1
    //   35: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #308	-> 0
    //   #309	-> 2
    //   #310	-> 19
    //   #312	-> 24
    //   #314	-> 26
    //   #312	-> 31
    // Exception table:
    //   from	to	target	type
    //   2	19	31	finally
    //   19	24	31	finally
    //   24	26	31	finally
    //   32	34	31	finally
  }
}
