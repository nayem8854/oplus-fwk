package android.text.method;

import android.icu.text.DecimalFormatSymbols;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.format.DateFormat;
import android.view.KeyEvent;
import android.view.View;
import java.util.Collection;
import java.util.Locale;
import libcore.icu.LocaleData;

public abstract class NumberKeyListener extends BaseKeyListener implements InputFilter {
  private static final String DATE_TIME_FORMAT_SYMBOLS = "GyYuUrQqMLlwWdDFgEecabBhHKkjJCmsSAzZOvVXx";
  
  private static final char SINGLE_QUOTE = '\'';
  
  protected int lookup(KeyEvent paramKeyEvent, Spannable paramSpannable) {
    return paramKeyEvent.getMatch(getAcceptedChars(), getMetaState(paramSpannable, paramKeyEvent));
  }
  
  public CharSequence filter(CharSequence paramCharSequence, int paramInt1, int paramInt2, Spanned paramSpanned, int paramInt3, int paramInt4) {
    char[] arrayOfChar = getAcceptedChars();
    for (paramInt3 = paramInt1; paramInt3 < paramInt2 && 
      ok(arrayOfChar, paramCharSequence.charAt(paramInt3)); paramInt3++);
    if (paramInt3 == paramInt2)
      return null; 
    if (paramInt2 - paramInt1 == 1)
      return ""; 
    paramSpanned = new SpannableStringBuilder(paramCharSequence, paramInt1, paramInt2);
    paramInt2 -= paramInt1;
    for (; --paramInt2 >= paramInt3 - paramInt1; paramInt2--) {
      if (!ok(arrayOfChar, paramCharSequence.charAt(paramInt2)))
        paramSpanned.delete(paramInt2, paramInt2 + 1); 
    } 
    return paramSpanned;
  }
  
  protected static boolean ok(char[] paramArrayOfchar, char paramChar) {
    for (int i = paramArrayOfchar.length - 1; i >= 0; i--) {
      if (paramArrayOfchar[i] == paramChar)
        return true; 
    } 
    return false;
  }
  
  public boolean onKeyDown(View paramView, Editable paramEditable, int paramInt, KeyEvent paramKeyEvent) {
    // Byte code:
    //   0: aload_2
    //   1: invokestatic getSelectionStart : (Ljava/lang/CharSequence;)I
    //   4: istore #5
    //   6: aload_2
    //   7: invokestatic getSelectionEnd : (Ljava/lang/CharSequence;)I
    //   10: istore #6
    //   12: iload #5
    //   14: iload #6
    //   16: invokestatic min : (II)I
    //   19: istore #7
    //   21: iload #5
    //   23: iload #6
    //   25: invokestatic max : (II)I
    //   28: istore #5
    //   30: iconst_0
    //   31: istore #8
    //   33: iload #7
    //   35: iflt -> 47
    //   38: iload #5
    //   40: istore #6
    //   42: iload #5
    //   44: ifge -> 58
    //   47: iconst_0
    //   48: istore #6
    //   50: iconst_0
    //   51: istore #7
    //   53: aload_2
    //   54: iconst_0
    //   55: invokestatic setSelection : (Landroid/text/Spannable;I)V
    //   58: aload #4
    //   60: ifnull -> 75
    //   63: aload_0
    //   64: aload #4
    //   66: aload_2
    //   67: invokevirtual lookup : (Landroid/view/KeyEvent;Landroid/text/Spannable;)I
    //   70: istore #5
    //   72: goto -> 78
    //   75: iconst_0
    //   76: istore #5
    //   78: aload #4
    //   80: ifnull -> 90
    //   83: aload #4
    //   85: invokevirtual getRepeatCount : ()I
    //   88: istore #8
    //   90: iload #8
    //   92: ifne -> 136
    //   95: iload #5
    //   97: ifeq -> 200
    //   100: iload #7
    //   102: iload #6
    //   104: if_icmpeq -> 113
    //   107: aload_2
    //   108: iload #6
    //   110: invokestatic setSelection : (Landroid/text/Spannable;I)V
    //   113: aload_2
    //   114: iload #7
    //   116: iload #6
    //   118: iload #5
    //   120: i2c
    //   121: invokestatic valueOf : (C)Ljava/lang/String;
    //   124: invokeinterface replace : (IILjava/lang/CharSequence;)Landroid/text/Editable;
    //   129: pop
    //   130: aload_2
    //   131: invokestatic adjustMetaAfterKeypress : (Landroid/text/Spannable;)V
    //   134: iconst_1
    //   135: ireturn
    //   136: iload #5
    //   138: bipush #48
    //   140: if_icmpne -> 200
    //   143: iload #8
    //   145: iconst_1
    //   146: if_icmpne -> 200
    //   149: iload #7
    //   151: iload #6
    //   153: if_icmpne -> 200
    //   156: iload #6
    //   158: ifle -> 200
    //   161: aload_2
    //   162: iload #7
    //   164: iconst_1
    //   165: isub
    //   166: invokeinterface charAt : (I)C
    //   171: bipush #48
    //   173: if_icmpne -> 200
    //   176: aload_2
    //   177: iload #7
    //   179: iconst_1
    //   180: isub
    //   181: iload #6
    //   183: bipush #43
    //   185: invokestatic valueOf : (C)Ljava/lang/String;
    //   188: invokeinterface replace : (IILjava/lang/CharSequence;)Landroid/text/Editable;
    //   193: pop
    //   194: aload_2
    //   195: invokestatic adjustMetaAfterKeypress : (Landroid/text/Spannable;)V
    //   198: iconst_1
    //   199: ireturn
    //   200: aload_2
    //   201: invokestatic adjustMetaAfterKeypress : (Landroid/text/Spannable;)V
    //   204: aload_0
    //   205: aload_1
    //   206: aload_2
    //   207: iload_3
    //   208: aload #4
    //   210: invokespecial onKeyDown : (Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z
    //   213: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #111	-> 0
    //   #112	-> 6
    //   #114	-> 12
    //   #115	-> 21
    //   #118	-> 30
    //   #119	-> 47
    //   #120	-> 53
    //   #123	-> 58
    //   #124	-> 78
    //   #125	-> 90
    //   #126	-> 95
    //   #127	-> 100
    //   #128	-> 107
    //   #131	-> 113
    //   #133	-> 130
    //   #134	-> 134
    //   #136	-> 136
    //   #139	-> 149
    //   #140	-> 161
    //   #141	-> 176
    //   #142	-> 194
    //   #143	-> 198
    //   #147	-> 200
    //   #148	-> 204
  }
  
  static boolean addDigits(Collection<Character> paramCollection, Locale paramLocale) {
    if (paramLocale == null)
      return false; 
    String[] arrayOfString = DecimalFormatSymbols.getInstance(paramLocale).getDigitStrings();
    for (byte b = 0; b < 10; b++) {
      if (arrayOfString[b].length() > 1)
        return false; 
      paramCollection.add(Character.valueOf(arrayOfString[b].charAt(0)));
    } 
    return true;
  }
  
  static boolean addFormatCharsFromSkeleton(Collection<Character> paramCollection, Locale paramLocale, String paramString1, String paramString2) {
    if (paramLocale == null)
      return false; 
    String str = DateFormat.getBestDateTimePattern(paramLocale, paramString1);
    int i = 1;
    byte b = 0;
    while (true) {
      char c;
      int j = str.length();
      byte b1 = 1;
      if (b < j) {
        c = str.charAt(b);
        if (Character.isSurrogate(c))
          return false; 
        j = i;
        if (c == '\'') {
          if (i)
            b1 = 0; 
          i = b1;
          if (b != 0) {
            j = b1;
            if (str.charAt(b - 1) != '\'') {
              i = b1;
              continue;
            } 
          } else {
            continue;
          } 
        } 
        if (j != 0) {
          if (paramString2.indexOf(c) != -1) {
            i = j;
          } else {
            if ("GyYuUrQqMLlwWdDFgEecabBhHKkjJCmsSAzZOvVXx".indexOf(c) != -1)
              return false; 
            paramCollection.add(Character.valueOf(c));
            i = j;
          } 
          continue;
        } 
      } else {
        break;
      } 
      paramCollection.add(Character.valueOf(c));
      i = j;
      b++;
    } 
    return true;
  }
  
  static boolean addFormatCharsFromSkeletons(Collection<Character> paramCollection, Locale paramLocale, String[] paramArrayOfString, String paramString) {
    for (byte b = 0; b < paramArrayOfString.length; b++) {
      boolean bool = addFormatCharsFromSkeleton(paramCollection, paramLocale, paramArrayOfString[b], paramString);
      if (!bool)
        return false; 
    } 
    return true;
  }
  
  static boolean addAmPmChars(Collection<Character> paramCollection, Locale paramLocale) {
    if (paramLocale == null)
      return false; 
    String[] arrayOfString = (LocaleData.get(paramLocale)).amPm;
    for (byte b = 0; b < arrayOfString.length; b++) {
      for (byte b1 = 0; b1 < arrayOfString[b].length(); ) {
        char c = arrayOfString[b].charAt(b1);
        if (Character.isBmpCodePoint(c)) {
          paramCollection.add(Character.valueOf(c));
          b1++;
        } 
        return false;
      } 
    } 
    return true;
  }
  
  static char[] collectionToArray(Collection<Character> paramCollection) {
    char[] arrayOfChar = new char[paramCollection.size()];
    byte b = 0;
    for (Character character : paramCollection) {
      arrayOfChar[b] = character.charValue();
      b++;
    } 
    return arrayOfChar;
  }
  
  protected abstract char[] getAcceptedChars();
}
