package android.text.method;

import android.text.AutoText;
import android.text.Editable;
import android.text.NoCopySpan;
import android.text.Spannable;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.View;

public class QwertyKeyListener extends BaseKeyListener {
  private static SparseArray<String> PICKER_SETS;
  
  private static QwertyKeyListener sFullKeyboardInstance;
  
  private static QwertyKeyListener[] sInstance = new QwertyKeyListener[(TextKeyListener.Capitalize.values()).length * 2];
  
  private TextKeyListener.Capitalize mAutoCap;
  
  private boolean mAutoText;
  
  private boolean mFullKeyboard;
  
  private QwertyKeyListener(TextKeyListener.Capitalize paramCapitalize, boolean paramBoolean1, boolean paramBoolean2) {
    this.mAutoCap = paramCapitalize;
    this.mAutoText = paramBoolean1;
    this.mFullKeyboard = paramBoolean2;
  }
  
  public QwertyKeyListener(TextKeyListener.Capitalize paramCapitalize, boolean paramBoolean) {
    this(paramCapitalize, paramBoolean, false);
  }
  
  public static QwertyKeyListener getInstance(boolean paramBoolean, TextKeyListener.Capitalize paramCapitalize) {
    int i = paramCapitalize.ordinal() * 2 + paramBoolean;
    QwertyKeyListener[] arrayOfQwertyKeyListener = sInstance;
    if (arrayOfQwertyKeyListener[i] == null)
      arrayOfQwertyKeyListener[i] = new QwertyKeyListener(paramCapitalize, paramBoolean); 
    return sInstance[i];
  }
  
  public static QwertyKeyListener getInstanceForFullKeyboard() {
    if (sFullKeyboardInstance == null)
      sFullKeyboardInstance = new QwertyKeyListener(TextKeyListener.Capitalize.NONE, false, true); 
    return sFullKeyboardInstance;
  }
  
  public int getInputType() {
    return makeTextContentType(this.mAutoCap, this.mAutoText);
  }
  
  public boolean onKeyDown(View paramView, Editable paramEditable, int paramInt, KeyEvent paramKeyEvent) {
    // Byte code:
    //   0: aload_1
    //   1: ifnull -> 19
    //   4: invokestatic getInstance : ()Landroid/text/method/TextKeyListener;
    //   7: aload_1
    //   8: invokevirtual getContext : ()Landroid/content/Context;
    //   11: invokevirtual getPrefs : (Landroid/content/Context;)I
    //   14: istore #5
    //   16: goto -> 22
    //   19: iconst_0
    //   20: istore #5
    //   22: aload_2
    //   23: invokestatic getSelectionStart : (Ljava/lang/CharSequence;)I
    //   26: istore #6
    //   28: aload_2
    //   29: invokestatic getSelectionEnd : (Ljava/lang/CharSequence;)I
    //   32: istore #7
    //   34: iload #6
    //   36: iload #7
    //   38: invokestatic min : (II)I
    //   41: istore #8
    //   43: iload #6
    //   45: iload #7
    //   47: invokestatic max : (II)I
    //   50: istore #6
    //   52: iload #8
    //   54: iflt -> 68
    //   57: iload #6
    //   59: ifge -> 65
    //   62: goto -> 68
    //   65: goto -> 80
    //   68: aload_2
    //   69: iconst_0
    //   70: iconst_0
    //   71: invokestatic setSelection : (Landroid/text/Spannable;II)V
    //   74: iconst_0
    //   75: istore #8
    //   77: iconst_0
    //   78: istore #6
    //   80: aload_2
    //   81: getstatic android/text/method/TextKeyListener.ACTIVE : Ljava/lang/Object;
    //   84: invokeinterface getSpanStart : (Ljava/lang/Object;)I
    //   89: istore #9
    //   91: aload_2
    //   92: getstatic android/text/method/TextKeyListener.ACTIVE : Ljava/lang/Object;
    //   95: invokeinterface getSpanEnd : (Ljava/lang/Object;)I
    //   100: istore #10
    //   102: aload #4
    //   104: aload_2
    //   105: aload #4
    //   107: invokestatic getMetaState : (Ljava/lang/CharSequence;Landroid/view/KeyEvent;)I
    //   110: invokevirtual getUnicodeChar : (I)I
    //   113: istore #7
    //   115: aload_0
    //   116: getfield mFullKeyboard : Z
    //   119: ifne -> 211
    //   122: aload #4
    //   124: invokevirtual getRepeatCount : ()I
    //   127: istore #11
    //   129: iload #11
    //   131: ifle -> 208
    //   134: iload #8
    //   136: iload #6
    //   138: if_icmpne -> 208
    //   141: iload #8
    //   143: ifle -> 208
    //   146: aload_2
    //   147: iload #8
    //   149: iconst_1
    //   150: isub
    //   151: invokeinterface charAt : (I)C
    //   156: istore #12
    //   158: iload #12
    //   160: iload #7
    //   162: if_icmpeq -> 181
    //   165: iload #12
    //   167: iload #7
    //   169: invokestatic toUpperCase : (I)I
    //   172: if_icmpne -> 178
    //   175: goto -> 181
    //   178: goto -> 211
    //   181: aload_1
    //   182: ifnull -> 205
    //   185: aload_0
    //   186: aload_1
    //   187: aload_2
    //   188: iload #12
    //   190: iconst_0
    //   191: iload #11
    //   193: invokespecial showCharacterPicker : (Landroid/view/View;Landroid/text/Editable;CZI)Z
    //   196: ifeq -> 211
    //   199: aload_2
    //   200: invokestatic resetMetaState : (Landroid/text/Spannable;)V
    //   203: iconst_1
    //   204: ireturn
    //   205: goto -> 211
    //   208: goto -> 211
    //   211: iload #7
    //   213: ldc 61185
    //   215: if_icmpne -> 239
    //   218: aload_1
    //   219: ifnull -> 233
    //   222: aload_0
    //   223: aload_1
    //   224: aload_2
    //   225: ldc 61185
    //   227: iconst_1
    //   228: iconst_1
    //   229: invokespecial showCharacterPicker : (Landroid/view/View;Landroid/text/Editable;CZI)Z
    //   232: pop
    //   233: aload_2
    //   234: invokestatic resetMetaState : (Landroid/text/Spannable;)V
    //   237: iconst_1
    //   238: ireturn
    //   239: iload #7
    //   241: ldc_w 61184
    //   244: if_icmpne -> 381
    //   247: iload #8
    //   249: iload #6
    //   251: if_icmpne -> 308
    //   254: iload #6
    //   256: istore #11
    //   258: iload #11
    //   260: istore #7
    //   262: iload #11
    //   264: ifle -> 312
    //   267: iload #11
    //   269: istore #7
    //   271: iload #6
    //   273: iload #11
    //   275: isub
    //   276: iconst_4
    //   277: if_icmpge -> 312
    //   280: iload #11
    //   282: istore #7
    //   284: aload_2
    //   285: iload #11
    //   287: iconst_1
    //   288: isub
    //   289: invokeinterface charAt : (I)C
    //   294: bipush #16
    //   296: invokestatic digit : (CI)I
    //   299: iflt -> 312
    //   302: iinc #11, -1
    //   305: goto -> 258
    //   308: iload #8
    //   310: istore #7
    //   312: iconst_m1
    //   313: istore #11
    //   315: aload_2
    //   316: iload #7
    //   318: iload #6
    //   320: invokestatic substring : (Ljava/lang/CharSequence;II)Ljava/lang/String;
    //   323: astore #13
    //   325: aload #13
    //   327: bipush #16
    //   329: invokestatic parseInt : (Ljava/lang/String;I)I
    //   332: istore #14
    //   334: iload #14
    //   336: istore #11
    //   338: goto -> 343
    //   341: astore #13
    //   343: iload #11
    //   345: iflt -> 371
    //   348: iload #7
    //   350: istore #8
    //   352: aload_2
    //   353: iload #8
    //   355: iload #6
    //   357: invokestatic setSelection : (Landroid/text/Spannable;II)V
    //   360: iload #11
    //   362: istore #7
    //   364: iload #8
    //   366: istore #11
    //   368: goto -> 385
    //   371: iconst_0
    //   372: istore #7
    //   374: iload #8
    //   376: istore #11
    //   378: goto -> 385
    //   381: iload #8
    //   383: istore #11
    //   385: iload #7
    //   387: ifeq -> 1243
    //   390: iconst_0
    //   391: istore #8
    //   393: iload #7
    //   395: istore_3
    //   396: ldc_w -2147483648
    //   399: iload #7
    //   401: iand
    //   402: ifeq -> 415
    //   405: iconst_1
    //   406: istore #8
    //   408: iload #7
    //   410: ldc_w 2147483647
    //   413: iand
    //   414: istore_3
    //   415: iload #8
    //   417: istore #15
    //   419: iload_3
    //   420: istore #7
    //   422: iload #11
    //   424: istore #16
    //   426: iload #9
    //   428: iload #11
    //   430: if_icmpne -> 563
    //   433: iload #8
    //   435: istore #15
    //   437: iload_3
    //   438: istore #7
    //   440: iload #11
    //   442: istore #16
    //   444: iload #10
    //   446: iload #6
    //   448: if_icmpne -> 563
    //   451: iconst_0
    //   452: istore #7
    //   454: iload #8
    //   456: istore #9
    //   458: iload #7
    //   460: istore #10
    //   462: iload_3
    //   463: istore #14
    //   465: iload #6
    //   467: iload #11
    //   469: isub
    //   470: iconst_1
    //   471: isub
    //   472: ifne -> 519
    //   475: aload_2
    //   476: iload #11
    //   478: invokeinterface charAt : (I)C
    //   483: istore #14
    //   485: iload #14
    //   487: iload_3
    //   488: invokestatic getDeadChar : (II)I
    //   491: istore #15
    //   493: iload #8
    //   495: istore #9
    //   497: iload #7
    //   499: istore #10
    //   501: iload_3
    //   502: istore #14
    //   504: iload #15
    //   506: ifeq -> 519
    //   509: iload #15
    //   511: istore #14
    //   513: iconst_1
    //   514: istore #10
    //   516: iconst_0
    //   517: istore #9
    //   519: iload #9
    //   521: istore #15
    //   523: iload #14
    //   525: istore #7
    //   527: iload #11
    //   529: istore #16
    //   531: iload #10
    //   533: ifne -> 563
    //   536: aload_2
    //   537: iload #6
    //   539: invokestatic setSelection : (Landroid/text/Spannable;I)V
    //   542: aload_2
    //   543: getstatic android/text/method/TextKeyListener.ACTIVE : Ljava/lang/Object;
    //   546: invokeinterface removeSpan : (Ljava/lang/Object;)V
    //   551: iload #6
    //   553: istore #16
    //   555: iload #14
    //   557: istore #7
    //   559: iload #9
    //   561: istore #15
    //   563: iload #7
    //   565: istore_3
    //   566: iload #5
    //   568: iconst_1
    //   569: iand
    //   570: ifeq -> 717
    //   573: iload #7
    //   575: istore_3
    //   576: iload #7
    //   578: invokestatic isLowerCase : (I)Z
    //   581: ifeq -> 717
    //   584: aload_0
    //   585: getfield mAutoCap : Landroid/text/method/TextKeyListener$Capitalize;
    //   588: astore #4
    //   590: iload #7
    //   592: istore_3
    //   593: aload #4
    //   595: aload_2
    //   596: iload #16
    //   598: invokestatic shouldCap : (Landroid/text/method/TextKeyListener$Capitalize;Ljava/lang/CharSequence;I)Z
    //   601: ifeq -> 717
    //   604: aload_2
    //   605: getstatic android/text/method/TextKeyListener.CAPPED : Ljava/lang/Object;
    //   608: invokeinterface getSpanEnd : (Ljava/lang/Object;)I
    //   613: istore #8
    //   615: aload_2
    //   616: getstatic android/text/method/TextKeyListener.CAPPED : Ljava/lang/Object;
    //   619: invokeinterface getSpanFlags : (Ljava/lang/Object;)I
    //   624: istore_3
    //   625: iload #8
    //   627: iload #16
    //   629: if_icmpne -> 660
    //   632: iload_3
    //   633: bipush #16
    //   635: ishr
    //   636: ldc_w 65535
    //   639: iand
    //   640: iload #7
    //   642: if_icmpne -> 660
    //   645: aload_2
    //   646: getstatic android/text/method/TextKeyListener.CAPPED : Ljava/lang/Object;
    //   649: invokeinterface removeSpan : (Ljava/lang/Object;)V
    //   654: iload #7
    //   656: istore_3
    //   657: goto -> 717
    //   660: iload #7
    //   662: bipush #16
    //   664: ishl
    //   665: istore #8
    //   667: iload #7
    //   669: invokestatic toUpperCase : (I)I
    //   672: istore_3
    //   673: iload #16
    //   675: ifne -> 697
    //   678: aload_2
    //   679: getstatic android/text/method/TextKeyListener.CAPPED : Ljava/lang/Object;
    //   682: iconst_0
    //   683: iconst_0
    //   684: iload #8
    //   686: bipush #17
    //   688: ior
    //   689: invokeinterface setSpan : (Ljava/lang/Object;III)V
    //   694: goto -> 717
    //   697: aload_2
    //   698: getstatic android/text/method/TextKeyListener.CAPPED : Ljava/lang/Object;
    //   701: iload #16
    //   703: iconst_1
    //   704: isub
    //   705: iload #16
    //   707: iload #8
    //   709: bipush #33
    //   711: ior
    //   712: invokeinterface setSpan : (Ljava/lang/Object;III)V
    //   717: iload #16
    //   719: iload #6
    //   721: if_icmpeq -> 730
    //   724: aload_2
    //   725: iload #6
    //   727: invokestatic setSelection : (Landroid/text/Spannable;I)V
    //   730: aload_2
    //   731: getstatic android/text/method/QwertyKeyListener.OLD_SEL_START : Ljava/lang/Object;
    //   734: iload #16
    //   736: iload #16
    //   738: bipush #17
    //   740: invokeinterface setSpan : (Ljava/lang/Object;III)V
    //   745: aload_2
    //   746: iload #16
    //   748: iload #6
    //   750: iload_3
    //   751: i2c
    //   752: invokestatic valueOf : (C)Ljava/lang/String;
    //   755: invokeinterface replace : (IILjava/lang/CharSequence;)Landroid/text/Editable;
    //   760: pop
    //   761: aload_2
    //   762: getstatic android/text/method/QwertyKeyListener.OLD_SEL_START : Ljava/lang/Object;
    //   765: invokeinterface getSpanStart : (Ljava/lang/Object;)I
    //   770: istore #8
    //   772: aload_2
    //   773: invokestatic getSelectionEnd : (Ljava/lang/CharSequence;)I
    //   776: istore #6
    //   778: iload #8
    //   780: iload #6
    //   782: if_icmpge -> 828
    //   785: aload_2
    //   786: getstatic android/text/method/TextKeyListener.LAST_TYPED : Ljava/lang/Object;
    //   789: iload #8
    //   791: iload #6
    //   793: bipush #33
    //   795: invokeinterface setSpan : (Ljava/lang/Object;III)V
    //   800: iload #15
    //   802: ifeq -> 828
    //   805: aload_2
    //   806: iload #8
    //   808: iload #6
    //   810: invokestatic setSelection : (Landroid/text/Spannable;II)V
    //   813: aload_2
    //   814: getstatic android/text/method/TextKeyListener.ACTIVE : Ljava/lang/Object;
    //   817: iload #8
    //   819: iload #6
    //   821: bipush #33
    //   823: invokeinterface setSpan : (Ljava/lang/Object;III)V
    //   828: aload_2
    //   829: invokestatic adjustMetaAfterKeypress : (Landroid/text/Spannable;)V
    //   832: iload #5
    //   834: iconst_2
    //   835: iand
    //   836: ifeq -> 1089
    //   839: aload_0
    //   840: getfield mAutoText : Z
    //   843: ifeq -> 1089
    //   846: iload_3
    //   847: bipush #32
    //   849: if_icmpeq -> 909
    //   852: iload_3
    //   853: bipush #9
    //   855: if_icmpeq -> 909
    //   858: iload_3
    //   859: bipush #10
    //   861: if_icmpeq -> 909
    //   864: iload_3
    //   865: bipush #44
    //   867: if_icmpeq -> 909
    //   870: iload_3
    //   871: bipush #46
    //   873: if_icmpeq -> 909
    //   876: iload_3
    //   877: bipush #33
    //   879: if_icmpeq -> 909
    //   882: iload_3
    //   883: bipush #63
    //   885: if_icmpeq -> 909
    //   888: iload_3
    //   889: bipush #34
    //   891: if_icmpeq -> 909
    //   894: iload_3
    //   895: invokestatic getType : (I)I
    //   898: bipush #22
    //   900: if_icmpne -> 906
    //   903: goto -> 909
    //   906: goto -> 1089
    //   909: getstatic android/text/method/TextKeyListener.INHIBIT_REPLACEMENT : Ljava/lang/Object;
    //   912: astore #4
    //   914: aload_2
    //   915: aload #4
    //   917: invokeinterface getSpanEnd : (Ljava/lang/Object;)I
    //   922: iload #8
    //   924: if_icmpeq -> 1086
    //   927: iload #8
    //   929: istore_3
    //   930: iload_3
    //   931: ifle -> 969
    //   934: aload_2
    //   935: iload_3
    //   936: iconst_1
    //   937: isub
    //   938: invokeinterface charAt : (I)C
    //   943: istore #12
    //   945: iload #12
    //   947: bipush #39
    //   949: if_icmpeq -> 963
    //   952: iload #12
    //   954: invokestatic isLetter : (C)Z
    //   957: ifne -> 963
    //   960: goto -> 969
    //   963: iinc #3, -1
    //   966: goto -> 930
    //   969: aload_0
    //   970: aload_2
    //   971: iload_3
    //   972: iload #8
    //   974: aload_1
    //   975: invokespecial getReplacement : (Ljava/lang/CharSequence;IILandroid/view/View;)Ljava/lang/String;
    //   978: astore_1
    //   979: aload_1
    //   980: ifnull -> 1083
    //   983: aload_2
    //   984: iconst_0
    //   985: aload_2
    //   986: invokeinterface length : ()I
    //   991: ldc android/text/method/QwertyKeyListener$Replaced
    //   993: invokeinterface getSpans : (IILjava/lang/Class;)[Ljava/lang/Object;
    //   998: checkcast [Landroid/text/method/QwertyKeyListener$Replaced;
    //   1001: astore #4
    //   1003: iconst_0
    //   1004: istore #6
    //   1006: iload #6
    //   1008: aload #4
    //   1010: arraylength
    //   1011: if_icmpge -> 1031
    //   1014: aload_2
    //   1015: aload #4
    //   1017: iload #6
    //   1019: aaload
    //   1020: invokeinterface removeSpan : (Ljava/lang/Object;)V
    //   1025: iinc #6, 1
    //   1028: goto -> 1006
    //   1031: iload #8
    //   1033: iload_3
    //   1034: isub
    //   1035: newarray char
    //   1037: astore #4
    //   1039: aload_2
    //   1040: iload_3
    //   1041: iload #8
    //   1043: aload #4
    //   1045: iconst_0
    //   1046: invokestatic getChars : (Ljava/lang/CharSequence;II[CI)V
    //   1049: aload_2
    //   1050: new android/text/method/QwertyKeyListener$Replaced
    //   1053: dup
    //   1054: aload #4
    //   1056: invokespecial <init> : ([C)V
    //   1059: iload_3
    //   1060: iload #8
    //   1062: bipush #33
    //   1064: invokeinterface setSpan : (Ljava/lang/Object;III)V
    //   1069: aload_2
    //   1070: iload_3
    //   1071: iload #8
    //   1073: aload_1
    //   1074: invokeinterface replace : (IILjava/lang/CharSequence;)Landroid/text/Editable;
    //   1079: pop
    //   1080: goto -> 1089
    //   1083: goto -> 1089
    //   1086: goto -> 1089
    //   1089: iload #5
    //   1091: iconst_4
    //   1092: iand
    //   1093: ifeq -> 1241
    //   1096: aload_0
    //   1097: getfield mAutoText : Z
    //   1100: ifeq -> 1241
    //   1103: aload_2
    //   1104: invokestatic getSelectionEnd : (Ljava/lang/CharSequence;)I
    //   1107: istore #6
    //   1109: iload #6
    //   1111: iconst_3
    //   1112: isub
    //   1113: iflt -> 1241
    //   1116: aload_2
    //   1117: iload #6
    //   1119: iconst_1
    //   1120: isub
    //   1121: invokeinterface charAt : (I)C
    //   1126: bipush #32
    //   1128: if_icmpne -> 1241
    //   1131: aload_2
    //   1132: iload #6
    //   1134: iconst_2
    //   1135: isub
    //   1136: invokeinterface charAt : (I)C
    //   1141: bipush #32
    //   1143: if_icmpne -> 1241
    //   1146: aload_2
    //   1147: iload #6
    //   1149: iconst_3
    //   1150: isub
    //   1151: invokeinterface charAt : (I)C
    //   1156: istore #12
    //   1158: iload #6
    //   1160: iconst_3
    //   1161: isub
    //   1162: istore_3
    //   1163: iload_3
    //   1164: ifle -> 1204
    //   1167: iload #12
    //   1169: bipush #34
    //   1171: if_icmpeq -> 1187
    //   1174: iload #12
    //   1176: invokestatic getType : (C)I
    //   1179: bipush #22
    //   1181: if_icmpne -> 1204
    //   1184: goto -> 1187
    //   1187: aload_2
    //   1188: iload_3
    //   1189: iconst_1
    //   1190: isub
    //   1191: invokeinterface charAt : (I)C
    //   1196: istore #12
    //   1198: iinc #3, -1
    //   1201: goto -> 1163
    //   1204: iload #12
    //   1206: invokestatic isLetter : (C)Z
    //   1209: ifne -> 1220
    //   1212: iload #12
    //   1214: invokestatic isDigit : (C)Z
    //   1217: ifeq -> 1241
    //   1220: aload_2
    //   1221: iload #6
    //   1223: iconst_2
    //   1224: isub
    //   1225: iload #6
    //   1227: iconst_1
    //   1228: isub
    //   1229: ldc_w '.'
    //   1232: invokeinterface replace : (IILjava/lang/CharSequence;)Landroid/text/Editable;
    //   1237: pop
    //   1238: goto -> 1241
    //   1241: iconst_1
    //   1242: ireturn
    //   1243: iload_3
    //   1244: bipush #67
    //   1246: if_icmpne -> 1496
    //   1249: aload #4
    //   1251: invokevirtual hasNoModifiers : ()Z
    //   1254: ifne -> 1269
    //   1257: aload #4
    //   1259: iconst_2
    //   1260: invokevirtual hasModifiers : (I)Z
    //   1263: ifeq -> 1496
    //   1266: goto -> 1269
    //   1269: iload #11
    //   1271: iload #6
    //   1273: if_icmpne -> 1496
    //   1276: iconst_1
    //   1277: istore #8
    //   1279: iload #8
    //   1281: istore #6
    //   1283: aload_2
    //   1284: getstatic android/text/method/TextKeyListener.LAST_TYPED : Ljava/lang/Object;
    //   1287: invokeinterface getSpanEnd : (Ljava/lang/Object;)I
    //   1292: iload #11
    //   1294: if_icmpne -> 1319
    //   1297: iload #8
    //   1299: istore #6
    //   1301: aload_2
    //   1302: iload #11
    //   1304: iconst_1
    //   1305: isub
    //   1306: invokeinterface charAt : (I)C
    //   1311: bipush #10
    //   1313: if_icmpeq -> 1319
    //   1316: iconst_2
    //   1317: istore #6
    //   1319: aload_2
    //   1320: iload #11
    //   1322: iload #6
    //   1324: isub
    //   1325: iload #11
    //   1327: ldc android/text/method/QwertyKeyListener$Replaced
    //   1329: invokeinterface getSpans : (IILjava/lang/Class;)[Ljava/lang/Object;
    //   1334: checkcast [Landroid/text/method/QwertyKeyListener$Replaced;
    //   1337: astore #13
    //   1339: aload #13
    //   1341: arraylength
    //   1342: ifle -> 1493
    //   1345: aload_2
    //   1346: aload #13
    //   1348: iconst_0
    //   1349: aaload
    //   1350: invokeinterface getSpanStart : (Ljava/lang/Object;)I
    //   1355: istore #6
    //   1357: aload_2
    //   1358: aload #13
    //   1360: iconst_0
    //   1361: aaload
    //   1362: invokeinterface getSpanEnd : (Ljava/lang/Object;)I
    //   1367: istore #8
    //   1369: new java/lang/String
    //   1372: dup
    //   1373: aload #13
    //   1375: iconst_0
    //   1376: aaload
    //   1377: invokestatic access$000 : (Landroid/text/method/QwertyKeyListener$Replaced;)[C
    //   1380: invokespecial <init> : ([C)V
    //   1383: astore #17
    //   1385: aload_2
    //   1386: aload #13
    //   1388: iconst_0
    //   1389: aaload
    //   1390: invokeinterface removeSpan : (Ljava/lang/Object;)V
    //   1395: iload #11
    //   1397: iload #8
    //   1399: if_icmplt -> 1479
    //   1402: aload_2
    //   1403: getstatic android/text/method/TextKeyListener.INHIBIT_REPLACEMENT : Ljava/lang/Object;
    //   1406: iload #8
    //   1408: iload #8
    //   1410: bipush #34
    //   1412: invokeinterface setSpan : (Ljava/lang/Object;III)V
    //   1417: aload_2
    //   1418: iload #6
    //   1420: iload #8
    //   1422: aload #17
    //   1424: invokeinterface replace : (IILjava/lang/CharSequence;)Landroid/text/Editable;
    //   1429: pop
    //   1430: aload_2
    //   1431: getstatic android/text/method/TextKeyListener.INHIBIT_REPLACEMENT : Ljava/lang/Object;
    //   1434: invokeinterface getSpanStart : (Ljava/lang/Object;)I
    //   1439: istore_3
    //   1440: iload_3
    //   1441: iconst_1
    //   1442: isub
    //   1443: iflt -> 1464
    //   1446: aload_2
    //   1447: getstatic android/text/method/TextKeyListener.INHIBIT_REPLACEMENT : Ljava/lang/Object;
    //   1450: iload_3
    //   1451: iconst_1
    //   1452: isub
    //   1453: iload_3
    //   1454: bipush #33
    //   1456: invokeinterface setSpan : (Ljava/lang/Object;III)V
    //   1461: goto -> 1473
    //   1464: aload_2
    //   1465: getstatic android/text/method/TextKeyListener.INHIBIT_REPLACEMENT : Ljava/lang/Object;
    //   1468: invokeinterface removeSpan : (Ljava/lang/Object;)V
    //   1473: aload_2
    //   1474: invokestatic adjustMetaAfterKeypress : (Landroid/text/Spannable;)V
    //   1477: iconst_1
    //   1478: ireturn
    //   1479: aload_2
    //   1480: invokestatic adjustMetaAfterKeypress : (Landroid/text/Spannable;)V
    //   1483: aload_0
    //   1484: aload_1
    //   1485: aload_2
    //   1486: iload_3
    //   1487: aload #4
    //   1489: invokespecial onKeyDown : (Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z
    //   1492: ireturn
    //   1493: goto -> 1496
    //   1496: aload_0
    //   1497: aload_1
    //   1498: aload_2
    //   1499: iload_3
    //   1500: aload #4
    //   1502: invokespecial onKeyDown : (Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z
    //   1505: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #92	-> 0
    //   #94	-> 0
    //   #95	-> 4
    //   #94	-> 19
    //   #99	-> 22
    //   #100	-> 28
    //   #102	-> 34
    //   #103	-> 43
    //   #105	-> 52
    //   #106	-> 68
    //   #107	-> 68
    //   #111	-> 80
    //   #112	-> 91
    //   #116	-> 102
    //   #118	-> 115
    //   #119	-> 122
    //   #120	-> 129
    //   #121	-> 146
    //   #123	-> 158
    //   #124	-> 185
    //   #125	-> 199
    //   #126	-> 203
    //   #123	-> 205
    //   #120	-> 208
    //   #118	-> 211
    //   #132	-> 211
    //   #133	-> 218
    //   #134	-> 222
    //   #137	-> 233
    //   #138	-> 237
    //   #141	-> 239
    //   #144	-> 247
    //   #145	-> 254
    //   #147	-> 258
    //   #148	-> 280
    //   #149	-> 302
    //   #152	-> 308
    //   #155	-> 312
    //   #157	-> 315
    //   #158	-> 325
    //   #159	-> 341
    //   #161	-> 343
    //   #162	-> 348
    //   #163	-> 352
    //   #164	-> 360
    //   #166	-> 371
    //   #141	-> 381
    //   #170	-> 385
    //   #171	-> 390
    //   #173	-> 393
    //   #174	-> 405
    //   #175	-> 408
    //   #178	-> 415
    //   #179	-> 451
    //   #181	-> 454
    //   #182	-> 475
    //   #183	-> 485
    //   #185	-> 493
    //   #186	-> 509
    //   #187	-> 513
    //   #188	-> 516
    //   #192	-> 519
    //   #193	-> 536
    //   #194	-> 542
    //   #195	-> 551
    //   #199	-> 563
    //   #200	-> 573
    //   #201	-> 590
    //   #202	-> 604
    //   #203	-> 615
    //   #205	-> 625
    //   #206	-> 645
    //   #208	-> 660
    //   #209	-> 667
    //   #211	-> 673
    //   #212	-> 678
    //   #215	-> 697
    //   #222	-> 717
    //   #223	-> 724
    //   #225	-> 730
    //   #228	-> 745
    //   #230	-> 761
    //   #231	-> 772
    //   #233	-> 778
    //   #234	-> 785
    //   #238	-> 800
    //   #239	-> 805
    //   #240	-> 813
    //   #245	-> 828
    //   #250	-> 832
    //   #253	-> 894
    //   #254	-> 914
    //   #258	-> 927
    //   #259	-> 934
    //   #260	-> 945
    //   #261	-> 960
    //   #258	-> 963
    //   #265	-> 969
    //   #267	-> 979
    //   #268	-> 983
    //   #270	-> 1003
    //   #271	-> 1014
    //   #270	-> 1025
    //   #273	-> 1031
    //   #274	-> 1039
    //   #276	-> 1049
    //   #278	-> 1069
    //   #267	-> 1083
    //   #254	-> 1086
    //   #250	-> 1089
    //   #284	-> 1089
    //   #285	-> 1103
    //   #286	-> 1109
    //   #287	-> 1116
    //   #288	-> 1131
    //   #289	-> 1146
    //   #291	-> 1158
    //   #292	-> 1167
    //   #293	-> 1174
    //   #292	-> 1187
    //   #294	-> 1187
    //   #291	-> 1198
    //   #300	-> 1204
    //   #301	-> 1220
    //   #307	-> 1241
    //   #308	-> 1243
    //   #309	-> 1249
    //   #313	-> 1276
    //   #320	-> 1279
    //   #321	-> 1297
    //   #322	-> 1316
    //   #325	-> 1319
    //   #328	-> 1339
    //   #329	-> 1345
    //   #330	-> 1357
    //   #331	-> 1369
    //   #333	-> 1385
    //   #339	-> 1395
    //   #340	-> 1402
    //   #342	-> 1417
    //   #344	-> 1430
    //   #345	-> 1440
    //   #346	-> 1446
    //   #350	-> 1464
    //   #352	-> 1473
    //   #358	-> 1477
    //   #354	-> 1479
    //   #355	-> 1483
    //   #328	-> 1493
    //   #308	-> 1496
    //   #362	-> 1496
    // Exception table:
    //   from	to	target	type
    //   315	325	341	java/lang/NumberFormatException
    //   325	334	341	java/lang/NumberFormatException
  }
  
  private String getReplacement(CharSequence paramCharSequence, int paramInt1, int paramInt2, View paramView) {
    String str1;
    int i = paramInt2 - paramInt1;
    int j = 0;
    String str2 = AutoText.get(paramCharSequence, paramInt1, paramInt2, paramView);
    String str3 = str2;
    if (str2 == null) {
      str3 = TextUtils.substring(paramCharSequence, paramInt1, paramInt2).toLowerCase();
      str1 = AutoText.get(str3, 0, paramInt2 - paramInt1, paramView);
      j = 1;
      str3 = str1;
      if (str1 == null)
        return null; 
    } 
    int k = 0;
    byte b = 0;
    if (j) {
      int m = paramInt1;
      j = b;
      while (true) {
        k = j;
        if (m < paramInt2) {
          k = j;
          if (Character.isUpperCase(paramCharSequence.charAt(m)))
            k = j + 1; 
          m++;
          j = k;
          continue;
        } 
        break;
      } 
    } 
    if (k == 0) {
      str1 = str3;
    } else if (k == 1) {
      str1 = toTitleCase(str3);
    } else if (k == i) {
      str1 = str3.toUpperCase();
    } else {
      str1 = toTitleCase(str3);
    } 
    if (str1.length() == i && 
      TextUtils.regionMatches(paramCharSequence, paramInt1, str1, 0, i))
      return null; 
    return str1;
  }
  
  public static void markAsReplaced(Spannable paramSpannable, int paramInt1, int paramInt2, String paramString) {
    Replaced[] arrayOfReplaced = paramSpannable.<Replaced>getSpans(0, paramSpannable.length(), Replaced.class);
    int i;
    for (i = 0; i < arrayOfReplaced.length; i++)
      paramSpannable.removeSpan(arrayOfReplaced[i]); 
    i = paramString.length();
    char[] arrayOfChar = new char[i];
    paramString.getChars(0, i, arrayOfChar, 0);
    paramSpannable.setSpan(new Replaced(arrayOfChar), paramInt1, paramInt2, 33);
  }
  
  static {
    SparseArray<String> sparseArray = new SparseArray();
    sparseArray.put(65, "ÀÁÂÄÆÃÅĄĀ");
    PICKER_SETS.put(67, "ÇĆČ");
    PICKER_SETS.put(68, "Ď");
    PICKER_SETS.put(69, "ÈÉÊËĘĚĒ");
    PICKER_SETS.put(71, "Ğ");
    PICKER_SETS.put(76, "Ł");
    PICKER_SETS.put(73, "ÌÍÎÏĪİ");
    PICKER_SETS.put(78, "ÑŃŇ");
    PICKER_SETS.put(79, "ØŒÕÒÓÔÖŌ");
    PICKER_SETS.put(82, "Ř");
    PICKER_SETS.put(83, "ŚŠŞ");
    PICKER_SETS.put(84, "Ť");
    PICKER_SETS.put(85, "ÙÚÛÜŮŪ");
    PICKER_SETS.put(89, "ÝŸ");
    PICKER_SETS.put(90, "ŹŻŽ");
    PICKER_SETS.put(97, "àáâäæãåąā");
    PICKER_SETS.put(99, "çćč");
    PICKER_SETS.put(100, "ď");
    PICKER_SETS.put(101, "èéêëęěē");
    PICKER_SETS.put(103, "ğ");
    PICKER_SETS.put(105, "ìíîïīı");
    PICKER_SETS.put(108, "ł");
    PICKER_SETS.put(110, "ñńň");
    PICKER_SETS.put(111, "øœõòóôöō");
    PICKER_SETS.put(114, "ř");
    PICKER_SETS.put(115, "§ßśšş");
    PICKER_SETS.put(116, "ť");
    PICKER_SETS.put(117, "ùúûüůū");
    PICKER_SETS.put(121, "ýÿ");
    PICKER_SETS.put(122, "źżž");
    PICKER_SETS.put(61185, "…¥•®©±[]{}\\|");
    PICKER_SETS.put(47, "\\");
    PICKER_SETS.put(49, "¹½⅓¼⅛");
    PICKER_SETS.put(50, "²⅔");
    PICKER_SETS.put(51, "³¾⅜");
    PICKER_SETS.put(52, "⁴");
    PICKER_SETS.put(53, "⅝");
    PICKER_SETS.put(55, "⅞");
    PICKER_SETS.put(48, "ⁿ∅");
    PICKER_SETS.put(36, "¢£€¥₣₤₱");
    PICKER_SETS.put(37, "‰");
    PICKER_SETS.put(42, "†‡");
    PICKER_SETS.put(45, "–—");
    PICKER_SETS.put(43, "±");
    PICKER_SETS.put(40, "[{<");
    PICKER_SETS.put(41, "]}>");
    PICKER_SETS.put(33, "¡");
    PICKER_SETS.put(34, "“”«»˝");
    PICKER_SETS.put(63, "¿");
    PICKER_SETS.put(44, "‚„");
    PICKER_SETS.put(61, "≠≈∞");
    PICKER_SETS.put(60, "≤«‹");
    PICKER_SETS.put(62, "≥»›");
  }
  
  private boolean showCharacterPicker(View paramView, Editable paramEditable, char paramChar, boolean paramBoolean, int paramInt) {
    String str = PICKER_SETS.get(paramChar);
    if (str == null)
      return false; 
    if (paramInt == 1) {
      CharacterPickerDialog characterPickerDialog = new CharacterPickerDialog(paramView.getContext(), paramView, paramEditable, str, paramBoolean);
      characterPickerDialog.show();
    } 
    return true;
  }
  
  private static String toTitleCase(String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(Character.toUpperCase(paramString.charAt(0)));
    stringBuilder.append(paramString.substring(1));
    return stringBuilder.toString();
  }
  
  class Replaced implements NoCopySpan {
    private char[] mText;
    
    public Replaced(QwertyKeyListener this$0) {
      this.mText = (char[])this$0;
    }
  }
}
