package android.text.method;

public class SingleLineTransformationMethod extends ReplacementTransformationMethod {
  private static char[] ORIGINAL = new char[] { '\n', '\r' };
  
  private static char[] REPLACEMENT = new char[] { ' ', -257 };
  
  private static SingleLineTransformationMethod sInstance;
  
  protected char[] getOriginal() {
    return ORIGINAL;
  }
  
  protected char[] getReplacement() {
    return REPLACEMENT;
  }
  
  public static SingleLineTransformationMethod getInstance() {
    SingleLineTransformationMethod singleLineTransformationMethod = sInstance;
    if (singleLineTransformationMethod != null)
      return singleLineTransformationMethod; 
    sInstance = singleLineTransformationMethod = new SingleLineTransformationMethod();
    return singleLineTransformationMethod;
  }
}
