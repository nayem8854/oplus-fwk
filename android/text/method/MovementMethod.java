package android.text.method;

import android.text.Spannable;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.TextView;

public interface MovementMethod {
  boolean canSelectArbitrarily();
  
  void initialize(TextView paramTextView, Spannable paramSpannable);
  
  boolean onGenericMotionEvent(TextView paramTextView, Spannable paramSpannable, MotionEvent paramMotionEvent);
  
  boolean onKeyDown(TextView paramTextView, Spannable paramSpannable, int paramInt, KeyEvent paramKeyEvent);
  
  boolean onKeyOther(TextView paramTextView, Spannable paramSpannable, KeyEvent paramKeyEvent);
  
  boolean onKeyUp(TextView paramTextView, Spannable paramSpannable, int paramInt, KeyEvent paramKeyEvent);
  
  void onTakeFocus(TextView paramTextView, Spannable paramSpannable, int paramInt);
  
  boolean onTouchEvent(TextView paramTextView, Spannable paramSpannable, MotionEvent paramMotionEvent);
  
  boolean onTrackballEvent(TextView paramTextView, Spannable paramSpannable, MotionEvent paramMotionEvent);
}
