package android.text.method;

import android.text.Spannable;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;

public class DialerKeyListener extends NumberKeyListener {
  protected char[] getAcceptedChars() {
    return CHARACTERS;
  }
  
  public static DialerKeyListener getInstance() {
    DialerKeyListener dialerKeyListener = sInstance;
    if (dialerKeyListener != null)
      return dialerKeyListener; 
    sInstance = dialerKeyListener = new DialerKeyListener();
    return dialerKeyListener;
  }
  
  public int getInputType() {
    return 3;
  }
  
  protected int lookup(KeyEvent paramKeyEvent, Spannable paramSpannable) {
    int i = getMetaState(paramSpannable, paramKeyEvent);
    char c = paramKeyEvent.getNumber();
    if ((i & 0x3) == 0 && 
      c != '\000')
      return c; 
    int j = super.lookup(paramKeyEvent, paramSpannable);
    if (j != 0)
      return j; 
    if (i != 0) {
      KeyCharacterMap.KeyData keyData = new KeyCharacterMap.KeyData();
      char[] arrayOfChar = getAcceptedChars();
      if (paramKeyEvent.getKeyData(keyData))
        for (i = 1; i < keyData.meta.length; i++) {
          if (ok(arrayOfChar, keyData.meta[i]))
            return keyData.meta[i]; 
        }  
    } 
    return c;
  }
  
  public static final char[] CHARACTERS = new char[] { 
      '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
      '#', '*', '+', '-', '(', ')', ',', '/', 'N', '.', 
      ' ', ';' };
  
  private static DialerKeyListener sInstance;
}
