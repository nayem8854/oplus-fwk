package android.text.method;

import java.util.HashMap;
import java.util.Locale;

public class DateKeyListener extends NumberKeyListener {
  @Deprecated
  public static final char[] CHARACTERS;
  
  public int getInputType() {
    if (this.mNeedsAdvancedInput)
      return 1; 
    return 20;
  }
  
  protected char[] getAcceptedChars() {
    return this.mCharacters;
  }
  
  @Deprecated
  public DateKeyListener() {
    this(null);
  }
  
  private static final String[] SKELETONS = new String[] { "yMd", "yM", "Md" };
  
  private static final String SYMBOLS_TO_IGNORE = "yMLd";
  
  private static final HashMap<Locale, DateKeyListener> sInstanceCache;
  
  private static final Object sLock;
  
  private final char[] mCharacters;
  
  private final boolean mNeedsAdvancedInput;
  
  public DateKeyListener(Locale paramLocale) {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial <init> : ()V
    //   4: new java/util/LinkedHashSet
    //   7: dup
    //   8: invokespecial <init> : ()V
    //   11: astore_2
    //   12: aload_2
    //   13: aload_1
    //   14: invokestatic addDigits : (Ljava/util/Collection;Ljava/util/Locale;)Z
    //   17: ifeq -> 41
    //   20: getstatic android/text/method/DateKeyListener.SKELETONS : [Ljava/lang/String;
    //   23: astore_3
    //   24: aload_2
    //   25: aload_1
    //   26: aload_3
    //   27: ldc 'yMLd'
    //   29: invokestatic addFormatCharsFromSkeletons : (Ljava/util/Collection;Ljava/util/Locale;[Ljava/lang/String;Ljava/lang/String;)Z
    //   32: ifeq -> 41
    //   35: iconst_1
    //   36: istore #4
    //   38: goto -> 44
    //   41: iconst_0
    //   42: istore #4
    //   44: iload #4
    //   46: ifeq -> 75
    //   49: aload_2
    //   50: invokestatic collectionToArray : (Ljava/util/Collection;)[C
    //   53: astore_1
    //   54: aload_0
    //   55: aload_1
    //   56: putfield mCharacters : [C
    //   59: aload_0
    //   60: iconst_1
    //   61: getstatic android/text/method/DateKeyListener.CHARACTERS : [C
    //   64: aload_1
    //   65: invokestatic containsAll : ([C[C)Z
    //   68: ixor
    //   69: putfield mNeedsAdvancedInput : Z
    //   72: goto -> 87
    //   75: aload_0
    //   76: getstatic android/text/method/DateKeyListener.CHARACTERS : [C
    //   79: putfield mCharacters : [C
    //   82: aload_0
    //   83: iconst_0
    //   84: putfield mNeedsAdvancedInput : Z
    //   87: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #65	-> 0
    //   #66	-> 4
    //   #69	-> 12
    //   #70	-> 24
    //   #72	-> 44
    //   #73	-> 49
    //   #74	-> 59
    //   #76	-> 75
    //   #77	-> 82
    //   #79	-> 87
  }
  
  @Deprecated
  public static DateKeyListener getInstance() {
    return getInstance(null);
  }
  
  public static DateKeyListener getInstance(Locale paramLocale) {
    synchronized (sLock) {
      DateKeyListener dateKeyListener1 = sInstanceCache.get(paramLocale);
      DateKeyListener dateKeyListener2 = dateKeyListener1;
      if (dateKeyListener1 == null) {
        dateKeyListener2 = new DateKeyListener();
        this(paramLocale);
        sInstanceCache.put(paramLocale, dateKeyListener2);
      } 
      return dateKeyListener2;
    } 
  }
  
  static {
    CHARACTERS = new char[] { 
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
        '/', '-', '.' };
    sLock = new Object();
    sInstanceCache = new HashMap<>();
  }
}
