package android.text.method;

import android.graphics.Rect;
import android.text.GetChars;
import android.text.Spanned;
import android.text.SpannedString;
import android.text.TextUtils;
import android.view.View;

public abstract class ReplacementTransformationMethod implements TransformationMethod {
  protected abstract char[] getOriginal();
  
  protected abstract char[] getReplacement();
  
  public CharSequence getTransformation(CharSequence paramCharSequence, View paramView) {
    char[] arrayOfChar2 = getOriginal();
    char[] arrayOfChar1 = getReplacement();
    if (!(paramCharSequence instanceof android.text.Editable)) {
      boolean bool2, bool1 = true;
      int i = arrayOfChar2.length;
      byte b = 0;
      while (true) {
        bool2 = bool1;
        if (b < i) {
          if (TextUtils.indexOf(paramCharSequence, arrayOfChar2[b]) >= 0) {
            bool2 = false;
            break;
          } 
          b++;
          continue;
        } 
        break;
      } 
      if (bool2)
        return paramCharSequence; 
      if (!(paramCharSequence instanceof android.text.Spannable)) {
        if (paramCharSequence instanceof Spanned)
          return new SpannedString(new SpannedReplacementCharSequence((Spanned)paramCharSequence, arrayOfChar2, arrayOfChar1)); 
        paramCharSequence = new ReplacementCharSequence(paramCharSequence, arrayOfChar2, arrayOfChar1);
        return 
          
          paramCharSequence.toString();
      } 
    } 
    if (paramCharSequence instanceof Spanned)
      return new SpannedReplacementCharSequence((Spanned)paramCharSequence, arrayOfChar2, arrayOfChar1); 
    return new ReplacementCharSequence(paramCharSequence, arrayOfChar2, arrayOfChar1);
  }
  
  public void onFocusChanged(View paramView, CharSequence paramCharSequence, boolean paramBoolean, int paramInt, Rect paramRect) {}
  
  private static class ReplacementCharSequence implements CharSequence, GetChars {
    private char[] mOriginal;
    
    private char[] mReplacement;
    
    private CharSequence mSource;
    
    public ReplacementCharSequence(CharSequence param1CharSequence, char[] param1ArrayOfchar1, char[] param1ArrayOfchar2) {
      this.mSource = param1CharSequence;
      this.mOriginal = param1ArrayOfchar1;
      this.mReplacement = param1ArrayOfchar2;
    }
    
    public int length() {
      return this.mSource.length();
    }
    
    public char charAt(int param1Int) {
      char c = this.mSource.charAt(param1Int);
      int i = this.mOriginal.length;
      for (param1Int = 0; param1Int < i; param1Int++, c = c1) {
        char c1 = c;
        if (c == this.mOriginal[param1Int])
          c1 = this.mReplacement[param1Int]; 
      } 
      return c;
    }
    
    public CharSequence subSequence(int param1Int1, int param1Int2) {
      char[] arrayOfChar = new char[param1Int2 - param1Int1];
      getChars(param1Int1, param1Int2, arrayOfChar, 0);
      return new String(arrayOfChar);
    }
    
    public String toString() {
      char[] arrayOfChar = new char[length()];
      getChars(0, length(), arrayOfChar, 0);
      return new String(arrayOfChar);
    }
    
    public void getChars(int param1Int1, int param1Int2, char[] param1ArrayOfchar, int param1Int3) {
      TextUtils.getChars(this.mSource, param1Int1, param1Int2, param1ArrayOfchar, param1Int3);
      int i = this.mOriginal.length;
      for (int j = param1Int3; j < param1Int2 - param1Int1 + param1Int3; j++) {
        char c = param1ArrayOfchar[j];
        for (byte b = 0; b < i; b++) {
          if (c == this.mOriginal[b])
            param1ArrayOfchar[j] = this.mReplacement[b]; 
        } 
      } 
    }
  }
  
  class SpannedReplacementCharSequence extends ReplacementCharSequence implements Spanned {
    private Spanned mSpanned;
    
    public SpannedReplacementCharSequence(ReplacementTransformationMethod this$0, char[] param1ArrayOfchar1, char[] param1ArrayOfchar2) {
      super((CharSequence)this$0, param1ArrayOfchar1, param1ArrayOfchar2);
      this.mSpanned = (Spanned)this$0;
    }
    
    public CharSequence subSequence(int param1Int1, int param1Int2) {
      return (new SpannedString(this)).subSequence(param1Int1, param1Int2);
    }
    
    public <T> T[] getSpans(int param1Int1, int param1Int2, Class<T> param1Class) {
      return this.mSpanned.getSpans(param1Int1, param1Int2, param1Class);
    }
    
    public int getSpanStart(Object param1Object) {
      return this.mSpanned.getSpanStart(param1Object);
    }
    
    public int getSpanEnd(Object param1Object) {
      return this.mSpanned.getSpanEnd(param1Object);
    }
    
    public int getSpanFlags(Object param1Object) {
      return this.mSpanned.getSpanFlags(param1Object);
    }
    
    public int nextSpanTransition(int param1Int1, int param1Int2, Class param1Class) {
      return this.mSpanned.nextSpanTransition(param1Int1, param1Int2, param1Class);
    }
  }
}
