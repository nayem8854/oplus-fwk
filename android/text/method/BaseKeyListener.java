package android.text.method;

import android.graphics.Paint;
import android.icu.lang.UCharacter;
import android.text.Editable;
import android.text.Emoji;
import android.text.Layout;
import android.text.NoCopySpan;
import android.text.Selection;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.ReplacementSpan;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

public abstract class BaseKeyListener extends MetaKeyKeyListener implements KeyListener {
  static final Object OLD_SEL_START = new NoCopySpan.Concrete();
  
  private final Object mLock = new Object();
  
  static Paint sCachedPaint = null;
  
  private static final int CARRIAGE_RETURN = 13;
  
  private static final int LINE_FEED = 10;
  
  public boolean backspace(View paramView, Editable paramEditable, int paramInt, KeyEvent paramKeyEvent) {
    return backspaceOrForwardDelete(paramView, paramEditable, paramInt, paramKeyEvent, false);
  }
  
  public boolean forwardDelete(View paramView, Editable paramEditable, int paramInt, KeyEvent paramKeyEvent) {
    return backspaceOrForwardDelete(paramView, paramEditable, paramInt, paramKeyEvent, true);
  }
  
  private static boolean isVariationSelector(int paramInt) {
    return UCharacter.hasBinaryProperty(paramInt, 36);
  }
  
  private static int adjustReplacementSpan(CharSequence paramCharSequence, int paramInt, boolean paramBoolean) {
    if (!(paramCharSequence instanceof Spanned))
      return paramInt; 
    ReplacementSpan[] arrayOfReplacementSpan = ((Spanned)paramCharSequence).<ReplacementSpan>getSpans(paramInt, paramInt, ReplacementSpan.class);
    int i;
    for (byte b = 0; b < arrayOfReplacementSpan.length; b++, i = paramInt) {
      int j = ((Spanned)paramCharSequence).getSpanStart(arrayOfReplacementSpan[b]);
      int k = ((Spanned)paramCharSequence).getSpanEnd(arrayOfReplacementSpan[b]);
      paramInt = i;
      if (j < i) {
        paramInt = i;
        if (k > i)
          if (paramBoolean) {
            paramInt = j;
          } else {
            paramInt = k;
          }  
      } 
    } 
    return i;
  }
  
  private static int getOffsetForBackspaceKey(CharSequence paramCharSequence, int paramInt) {
    if (paramInt <= 1)
      return 0; 
    boolean bool = true;
    byte b1 = 2;
    int i = 0;
    int j = 0;
    byte b2 = 0;
    int k = paramInt;
    while (true) {
      int n, m = Character.codePointBefore(paramCharSequence, k);
      k -= Character.charCount(m);
      switch (b2) {
        default:
          paramCharSequence = new StringBuilder();
          paramCharSequence.append("state ");
          paramCharSequence.append(b2);
          paramCharSequence.append(" is unknown");
          throw new IllegalArgumentException(paramCharSequence.toString());
        case true:
          if (Emoji.isTagSpecChar(m)) {
            i += true;
            int i1 = j;
            break;
          } 
          if (Emoji.isEmoji(m)) {
            i += Character.charCount(m);
            b2 = 13;
            int i1 = j;
            break;
          } 
          i = 2;
          b2 = 13;
          n = j;
          break;
        case true:
          if (Emoji.isRegionalIndicatorSymbol(m)) {
            i -= 2;
            b2 = 10;
            n = j;
            break;
          } 
          b2 = 13;
          n = j;
          break;
        case true:
          if (Emoji.isRegionalIndicatorSymbol(m)) {
            i += 2;
            b2 = 11;
            n = j;
            break;
          } 
          b2 = 13;
          n = j;
          break;
        case true:
          if (Emoji.isEmoji(m)) {
            i += j + 1 + Character.charCount(m);
            n = 0;
            b2 = 7;
            break;
          } 
          b2 = 13;
          n = j;
          break;
        case true:
          if (Emoji.isEmoji(m)) {
            i += Character.charCount(m) + 1;
            if (Emoji.isEmojiModifier(m)) {
              b2 = 4;
            } else {
              b2 = 7;
            } 
            n = j;
            break;
          } 
          if (isVariationSelector(m)) {
            n = Character.charCount(m);
            b2 = 9;
            break;
          } 
          b2 = 13;
          n = j;
          break;
        case true:
          if (m == Emoji.ZERO_WIDTH_JOINER) {
            b2 = 8;
            n = j;
            break;
          } 
          b2 = 13;
          n = j;
          break;
        case true:
          if (Emoji.isEmoji(m)) {
            i += Character.charCount(m);
            b2 = 7;
            n = j;
            break;
          } 
          n = i;
          if (!isVariationSelector(m)) {
            n = i;
            if (UCharacter.getCombiningClass(m) == 0)
              n = i + Character.charCount(m); 
          } 
          b2 = 13;
          i = n;
          n = j;
          break;
        case true:
          n = i;
          if (Emoji.isEmojiModifierBase(m))
            n = i + j + Character.charCount(m); 
          b2 = 13;
          i = n;
          n = j;
          break;
        case true:
          if (isVariationSelector(m)) {
            n = Character.charCount(m);
            b2 = 5;
            break;
          } 
          n = i;
          if (Emoji.isEmojiModifierBase(m))
            n = i + Character.charCount(m); 
          b2 = 13;
          i = n;
          n = j;
          break;
        case true:
          n = i;
          if (Emoji.isKeycapBase(m))
            n = i + j + Character.charCount(m); 
          b2 = 13;
          i = n;
          n = j;
          break;
        case true:
          if (isVariationSelector(m)) {
            n = Character.charCount(m);
            b2 = 3;
            break;
          } 
          n = i;
          if (Emoji.isKeycapBase(m))
            n = i + Character.charCount(m); 
          b2 = 13;
          i = n;
          n = j;
          break;
        case true:
          n = i;
          if (m == 13)
            n = i + 1; 
          b2 = 13;
          i = n;
          n = j;
          break;
        case false:
          i = Character.charCount(m);
          if (m == 10) {
            b2 = 1;
            n = j;
            break;
          } 
          if (isVariationSelector(m)) {
            b2 = 6;
            n = j;
            break;
          } 
          if (Emoji.isRegionalIndicatorSymbol(m)) {
            b2 = 10;
            n = j;
            break;
          } 
          if (Emoji.isEmojiModifier(m)) {
            b2 = 4;
            n = j;
            break;
          } 
          if (m == Emoji.COMBINING_ENCLOSING_KEYCAP) {
            b2 = 2;
            n = j;
            break;
          } 
          if (Emoji.isEmoji(m)) {
            b2 = 7;
            n = j;
            break;
          } 
          if (m == Emoji.CANCEL_TAG) {
            b2 = 12;
            n = j;
            break;
          } 
          b2 = 13;
          n = j;
          break;
      } 
      if (k <= 0 || b2 == 13)
        break; 
      j = n;
    } 
    return adjustReplacementSpan(paramCharSequence, paramInt - i, true);
  }
  
  private static int getOffsetForForwardDeleteKey(CharSequence paramCharSequence, int paramInt, Paint paramPaint) {
    int i = paramCharSequence.length();
    if (paramInt >= i - 1)
      return i; 
    paramInt = paramPaint.getTextRunCursor(paramCharSequence, paramInt, i, false, paramInt, 0);
    return adjustReplacementSpan(paramCharSequence, paramInt, false);
  }
  
  private boolean backspaceOrForwardDelete(View paramView, Editable paramEditable, int paramInt, KeyEvent paramKeyEvent, boolean paramBoolean) {
    boolean bool;
    if (!KeyEvent.metaStateHasNoModifiers(paramKeyEvent.getMetaState() & 0xFFFF8F0C))
      return false; 
    if (deleteSelection(paramView, paramEditable))
      return true; 
    if ((paramKeyEvent.getMetaState() & 0x1000) != 0) {
      paramInt = 1;
    } else {
      paramInt = 0;
    } 
    if (getMetaState(paramEditable, 1, paramKeyEvent) == 1) {
      i = 1;
    } else {
      i = 0;
    } 
    if (getMetaState(paramEditable, 2, paramKeyEvent) == 1) {
      bool = true;
    } else {
      bool = false;
    } 
    if (paramInt != 0) {
      if (bool || i)
        return false; 
      return deleteUntilWordBoundary(paramView, paramEditable, paramBoolean);
    } 
    if (bool && deleteLine(paramView, paramEditable))
      return true; 
    int i = Selection.getSelectionEnd(paramEditable);
    if (paramBoolean) {
      if (paramView instanceof TextView) {
        TextPaint textPaint = ((TextView)paramView).getPaint();
      } else {
        synchronized (this.mLock) {
          if (sCachedPaint == null) {
            Paint paint1 = new Paint();
            this();
            sCachedPaint = paint1;
          } 
          Paint paint = sCachedPaint;
          paramInt = getOffsetForForwardDeleteKey(paramEditable, i, paint);
        } 
        if (i != paramInt) {
          paramEditable.delete(Math.min(i, paramInt), Math.max(i, paramInt));
          return true;
        } 
      } 
    } else {
      paramInt = getOffsetForBackspaceKey(paramEditable, i);
      if (i != paramInt) {
        paramEditable.delete(Math.min(i, paramInt), Math.max(i, paramInt));
        return true;
      } 
    } 
    paramInt = getOffsetForForwardDeleteKey(paramEditable, i, (Paint)paramView);
  }
  
  private boolean deleteUntilWordBoundary(View paramView, Editable paramEditable, boolean paramBoolean) {
    int j, i = Selection.getSelectionStart(paramEditable);
    if (i != Selection.getSelectionEnd(paramEditable))
      return false; 
    if ((!paramBoolean && i == 0) || (paramBoolean && 
      i == paramEditable.length()))
      return false; 
    WordIterator wordIterator2 = null;
    if (paramView instanceof TextView)
      wordIterator2 = ((TextView)paramView).getWordIterator(); 
    WordIterator wordIterator1 = wordIterator2;
    if (wordIterator2 == null)
      wordIterator1 = new WordIterator(); 
    if (paramBoolean) {
      int k = i;
      wordIterator1.setCharSequence(paramEditable, k, paramEditable.length());
      int m = wordIterator1.following(i);
      i = k;
      j = m;
      if (m == -1) {
        j = paramEditable.length();
        i = k;
      } 
    } else {
      int k = i;
      wordIterator1.setCharSequence(paramEditable, 0, k);
      int m = wordIterator1.preceding(i);
      i = m;
      j = k;
      if (m == -1) {
        i = 0;
        j = k;
      } 
    } 
    paramEditable.delete(i, j);
    return true;
  }
  
  private boolean deleteSelection(View paramView, Editable paramEditable) {
    int i = Selection.getSelectionStart(paramEditable);
    int j = Selection.getSelectionEnd(paramEditable);
    int k = i, m = j;
    if (j < i) {
      m = i;
      k = j;
    } 
    if (k != m) {
      paramEditable.delete(k, m);
      return true;
    } 
    return false;
  }
  
  private boolean deleteLine(View paramView, Editable paramEditable) {
    if (paramView instanceof TextView) {
      Layout layout = ((TextView)paramView).getLayout();
      if (layout != null) {
        int i = layout.getLineForOffset(Selection.getSelectionStart(paramEditable));
        int j = layout.getLineStart(i);
        i = layout.getLineEnd(i);
        if (i != j) {
          paramEditable.delete(j, i);
          return true;
        } 
      } 
    } 
    return false;
  }
  
  static int makeTextContentType(TextKeyListener.Capitalize paramCapitalize, boolean paramBoolean) {
    int i = 1;
    int j = null.$SwitchMap$android$text$method$TextKeyListener$Capitalize[paramCapitalize.ordinal()];
    if (j != 1) {
      if (j != 2) {
        if (j == 3)
          i = 0x1 | 0x4000; 
      } else {
        i = 0x1 | 0x2000;
      } 
    } else {
      i = 0x1 | 0x1000;
    } 
    j = i;
    if (paramBoolean)
      j = i | 0x8000; 
    return j;
  }
  
  public boolean onKeyDown(View paramView, Editable paramEditable, int paramInt, KeyEvent paramKeyEvent) {
    boolean bool;
    if (paramInt != 67) {
      if (paramInt != 112) {
        bool = false;
      } else {
        bool = forwardDelete(paramView, paramEditable, paramInt, paramKeyEvent);
      } 
    } else {
      bool = backspace(paramView, paramEditable, paramInt, paramKeyEvent);
    } 
    if (bool) {
      adjustMetaAfterKeypress(paramEditable);
      return true;
    } 
    return super.onKeyDown(paramView, paramEditable, paramInt, paramKeyEvent);
  }
  
  public boolean onKeyOther(View paramView, Editable paramEditable, KeyEvent paramKeyEvent) {
    if (paramKeyEvent.getAction() != 2 || 
      paramKeyEvent.getKeyCode() != 0)
      return false; 
    int i = Selection.getSelectionStart(paramEditable);
    int j = Selection.getSelectionEnd(paramEditable);
    int k = i, m = j;
    if (j < i) {
      m = i;
      k = j;
    } 
    String str = paramKeyEvent.getCharacters();
    if (str == null)
      return false; 
    paramEditable.replace(k, m, str);
    return true;
  }
}
