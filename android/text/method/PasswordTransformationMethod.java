package android.text.method;

import android.graphics.Rect;
import android.os.Handler;
import android.os.SystemClock;
import android.text.Editable;
import android.text.GetChars;
import android.text.NoCopySpan;
import android.text.Spannable;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.UpdateLayout;
import android.view.View;
import java.lang.ref.WeakReference;

public class PasswordTransformationMethod implements TransformationMethod, TextWatcher {
  public CharSequence getTransformation(CharSequence paramCharSequence, View paramView) {
    if (paramCharSequence instanceof Spannable) {
      Spannable spannable = (Spannable)paramCharSequence;
      ViewReference[] arrayOfViewReference = spannable.<ViewReference>getSpans(0, spannable.length(), ViewReference.class);
      for (byte b = 0; b < arrayOfViewReference.length; b++)
        spannable.removeSpan(arrayOfViewReference[b]); 
      removeVisibleSpans(spannable);
      spannable.setSpan(new ViewReference(paramView), 0, 0, 34);
    } 
    return new PasswordCharSequence(paramCharSequence);
  }
  
  public static PasswordTransformationMethod getInstance() {
    PasswordTransformationMethod passwordTransformationMethod = sInstance;
    if (passwordTransformationMethod != null)
      return passwordTransformationMethod; 
    sInstance = passwordTransformationMethod = new PasswordTransformationMethod();
    return passwordTransformationMethod;
  }
  
  public void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
  
  public void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {
    if (paramCharSequence instanceof Spannable) {
      View view;
      Spannable spannable = (Spannable)paramCharSequence;
      ViewReference[] arrayOfViewReference = spannable.<ViewReference>getSpans(0, paramCharSequence.length(), ViewReference.class);
      if (arrayOfViewReference.length == 0)
        return; 
      paramCharSequence = null;
      for (paramInt2 = 0; paramCharSequence == null && paramInt2 < arrayOfViewReference.length; paramInt2++)
        view = arrayOfViewReference[paramInt2].get(); 
      if (view == null)
        return; 
      paramInt2 = TextKeyListener.getInstance().getPrefs(view.getContext());
      if ((paramInt2 & 0x8) != 0 && 
        paramInt3 > 0) {
        removeVisibleSpans(spannable);
        if (paramInt3 == 1)
          spannable.setSpan(new Visible(spannable, this), paramInt1, paramInt1 + paramInt3, 33); 
      } 
    } 
  }
  
  public void afterTextChanged(Editable paramEditable) {}
  
  public void onFocusChanged(View paramView, CharSequence paramCharSequence, boolean paramBoolean, int paramInt, Rect paramRect) {
    if (!paramBoolean && 
      paramCharSequence instanceof Spannable) {
      Spannable spannable = (Spannable)paramCharSequence;
      removeVisibleSpans(spannable);
    } 
  }
  
  private static void removeVisibleSpans(Spannable paramSpannable) {
    Visible[] arrayOfVisible = paramSpannable.<Visible>getSpans(0, paramSpannable.length(), Visible.class);
    for (byte b = 0; b < arrayOfVisible.length; b++)
      paramSpannable.removeSpan(arrayOfVisible[b]); 
  }
  
  class PasswordCharSequence implements CharSequence, GetChars {
    private CharSequence mSource;
    
    public PasswordCharSequence(PasswordTransformationMethod this$0) {
      this.mSource = (CharSequence)this$0;
    }
    
    public int length() {
      return this.mSource.length();
    }
    
    public char charAt(int param1Int) {
      CharSequence charSequence = this.mSource;
      if (charSequence instanceof Spanned) {
        charSequence = charSequence;
        int i = charSequence.getSpanStart(TextKeyListener.ACTIVE);
        int j = charSequence.getSpanEnd(TextKeyListener.ACTIVE);
        if (param1Int >= i && param1Int < j)
          return this.mSource.charAt(param1Int); 
        PasswordTransformationMethod.Visible[] arrayOfVisible = charSequence.<PasswordTransformationMethod.Visible>getSpans(0, charSequence.length(), PasswordTransformationMethod.Visible.class);
        for (i = 0; i < arrayOfVisible.length; i++) {
          if (charSequence.getSpanStart((arrayOfVisible[i]).mTransformer) >= 0) {
            j = charSequence.getSpanStart(arrayOfVisible[i]);
            int k = charSequence.getSpanEnd(arrayOfVisible[i]);
            if (param1Int >= j && param1Int < k)
              return this.mSource.charAt(param1Int); 
          } 
        } 
      } 
      return PasswordTransformationMethod.DOT;
    }
    
    public CharSequence subSequence(int param1Int1, int param1Int2) {
      char[] arrayOfChar = new char[param1Int2 - param1Int1];
      getChars(param1Int1, param1Int2, arrayOfChar, 0);
      return new String(arrayOfChar);
    }
    
    public String toString() {
      return subSequence(0, length()).toString();
    }
    
    public void getChars(int param1Int1, int param1Int2, char[] param1ArrayOfchar, int param1Int3) {
      TextUtils.getChars(this.mSource, param1Int1, param1Int2, param1ArrayOfchar, param1Int3);
      int i = -1, j = -1;
      int k = 0;
      int[] arrayOfInt1 = null, arrayOfInt2 = null;
      CharSequence charSequence = this.mSource;
      if (charSequence instanceof Spanned) {
        Spanned spanned = (Spanned)charSequence;
        int n = spanned.getSpanStart(TextKeyListener.ACTIVE);
        int i1 = spanned.getSpanEnd(TextKeyListener.ACTIVE);
        PasswordTransformationMethod.Visible[] arrayOfVisible = spanned.<PasswordTransformationMethod.Visible>getSpans(0, spanned.length(), PasswordTransformationMethod.Visible.class);
        int i2 = arrayOfVisible.length;
        int[] arrayOfInt3 = new int[i2];
        int[] arrayOfInt4 = new int[i2];
        byte b = 0;
        while (true) {
          i = n;
          j = i1;
          k = i2;
          arrayOfInt1 = arrayOfInt3;
          arrayOfInt2 = arrayOfInt4;
          if (b < i2) {
            if (spanned.getSpanStart((arrayOfVisible[b]).mTransformer) >= 0) {
              arrayOfInt3[b] = spanned.getSpanStart(arrayOfVisible[b]);
              arrayOfInt4[b] = spanned.getSpanEnd(arrayOfVisible[b]);
            } 
            b++;
            continue;
          } 
          break;
        } 
      } 
      for (int m = param1Int1; m < param1Int2; m++) {
        if (m < i || m >= j) {
          boolean bool2, bool1 = false;
          byte b = 0;
          while (true) {
            bool2 = bool1;
            if (b < k) {
              if (m >= arrayOfInt1[b] && m < arrayOfInt2[b]) {
                bool2 = true;
                break;
              } 
              b++;
              continue;
            } 
            break;
          } 
          if (!bool2)
            param1ArrayOfchar[m - param1Int1 + param1Int3] = PasswordTransformationMethod.DOT; 
        } 
      } 
    }
  }
  
  private static class Visible extends Handler implements UpdateLayout, Runnable {
    private Spannable mText;
    
    private PasswordTransformationMethod mTransformer;
    
    public Visible(Spannable param1Spannable, PasswordTransformationMethod param1PasswordTransformationMethod) {
      this.mText = param1Spannable;
      this.mTransformer = param1PasswordTransformationMethod;
      postAtTime(this, SystemClock.uptimeMillis() + 1500L);
    }
    
    public void run() {
      this.mText.removeSpan(this);
    }
  }
  
  class ViewReference extends WeakReference<View> implements NoCopySpan {
    public ViewReference(PasswordTransformationMethod this$0) {
      super(this$0);
    }
  }
  
  private static char DOT = '•';
  
  private static PasswordTransformationMethod sInstance;
}
