package android.text;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;

public class BoringLayout extends Layout implements TextUtils.EllipsizeCallback {
  int mBottom;
  
  private int mBottomPadding;
  
  int mDesc;
  
  private String mDirect;
  
  private int mEllipsizedCount;
  
  private int mEllipsizedStart;
  
  private int mEllipsizedWidth;
  
  private float mMax;
  
  private Paint mPaint;
  
  private int mTopPadding;
  
  public static BoringLayout make(CharSequence paramCharSequence, TextPaint paramTextPaint, int paramInt, Layout.Alignment paramAlignment, float paramFloat1, float paramFloat2, Metrics paramMetrics, boolean paramBoolean) {
    return new BoringLayout(paramCharSequence, paramTextPaint, paramInt, paramAlignment, paramFloat1, paramFloat2, paramMetrics, paramBoolean);
  }
  
  public static BoringLayout make(CharSequence paramCharSequence, TextPaint paramTextPaint, int paramInt1, Layout.Alignment paramAlignment, float paramFloat1, float paramFloat2, Metrics paramMetrics, boolean paramBoolean, TextUtils.TruncateAt paramTruncateAt, int paramInt2) {
    return new BoringLayout(paramCharSequence, paramTextPaint, paramInt1, paramAlignment, paramFloat1, paramFloat2, paramMetrics, paramBoolean, paramTruncateAt, paramInt2);
  }
  
  public BoringLayout replaceOrMake(CharSequence paramCharSequence, TextPaint paramTextPaint, int paramInt, Layout.Alignment paramAlignment, float paramFloat1, float paramFloat2, Metrics paramMetrics, boolean paramBoolean) {
    replaceWith(paramCharSequence, paramTextPaint, paramInt, paramAlignment, paramFloat1, paramFloat2);
    this.mEllipsizedWidth = paramInt;
    this.mEllipsizedStart = 0;
    this.mEllipsizedCount = 0;
    init(paramCharSequence, paramTextPaint, paramAlignment, paramMetrics, paramBoolean, true);
    return this;
  }
  
  public BoringLayout replaceOrMake(CharSequence paramCharSequence, TextPaint paramTextPaint, int paramInt1, Layout.Alignment paramAlignment, float paramFloat1, float paramFloat2, Metrics paramMetrics, boolean paramBoolean, TextUtils.TruncateAt paramTruncateAt, int paramInt2) {
    if (paramTruncateAt == null || paramTruncateAt == TextUtils.TruncateAt.MARQUEE) {
      replaceWith(paramCharSequence, paramTextPaint, paramInt1, paramAlignment, paramFloat1, paramFloat2);
      this.mEllipsizedWidth = paramInt1;
      this.mEllipsizedStart = 0;
      this.mEllipsizedCount = 0;
      boolean bool1 = true;
      init(getText(), paramTextPaint, paramAlignment, paramMetrics, paramBoolean, bool1);
      return this;
    } 
    replaceWith(TextUtils.ellipsize(paramCharSequence, paramTextPaint, paramInt2, paramTruncateAt, true, this), paramTextPaint, paramInt1, paramAlignment, paramFloat1, paramFloat2);
    this.mEllipsizedWidth = paramInt2;
    boolean bool = false;
    init(getText(), paramTextPaint, paramAlignment, paramMetrics, paramBoolean, bool);
    return this;
  }
  
  public BoringLayout(CharSequence paramCharSequence, TextPaint paramTextPaint, int paramInt, Layout.Alignment paramAlignment, float paramFloat1, float paramFloat2, Metrics paramMetrics, boolean paramBoolean) {
    super(paramCharSequence, paramTextPaint, paramInt, paramAlignment, paramFloat1, paramFloat2);
    this.mEllipsizedWidth = paramInt;
    this.mEllipsizedStart = 0;
    this.mEllipsizedCount = 0;
    init(paramCharSequence, paramTextPaint, paramAlignment, paramMetrics, paramBoolean, true);
  }
  
  public BoringLayout(CharSequence paramCharSequence, TextPaint paramTextPaint, int paramInt1, Layout.Alignment paramAlignment, float paramFloat1, float paramFloat2, Metrics paramMetrics, boolean paramBoolean, TextUtils.TruncateAt paramTruncateAt, int paramInt2) {
    super(paramCharSequence, paramTextPaint, paramInt1, paramAlignment, paramFloat1, paramFloat2);
    boolean bool;
    if (paramTruncateAt == null || paramTruncateAt == TextUtils.TruncateAt.MARQUEE) {
      this.mEllipsizedWidth = paramInt1;
      this.mEllipsizedStart = 0;
      this.mEllipsizedCount = 0;
      bool = true;
    } else {
      replaceWith(TextUtils.ellipsize(paramCharSequence, paramTextPaint, paramInt2, paramTruncateAt, true, this), paramTextPaint, paramInt1, paramAlignment, paramFloat1, paramFloat2);
      this.mEllipsizedWidth = paramInt2;
      bool = false;
    } 
    init(getText(), paramTextPaint, paramAlignment, paramMetrics, paramBoolean, bool);
  }
  
  void init(CharSequence paramCharSequence, TextPaint paramTextPaint, Layout.Alignment paramAlignment, Metrics paramMetrics, boolean paramBoolean1, boolean paramBoolean2) {
    int i;
    if (paramCharSequence instanceof String && paramAlignment == Layout.Alignment.ALIGN_NORMAL) {
      this.mDirect = paramCharSequence.toString();
    } else {
      this.mDirect = null;
    } 
    this.mPaint = paramTextPaint;
    if (paramBoolean1) {
      i = paramMetrics.bottom;
      int j = paramMetrics.top;
      this.mDesc = paramMetrics.bottom;
      i -= j;
    } else {
      int j = paramMetrics.descent;
      i = paramMetrics.ascent;
      this.mDesc = paramMetrics.descent;
      i = j - i;
    } 
    this.mBottom = i;
    if (paramBoolean2) {
      this.mMax = paramMetrics.width;
    } else {
      TextLine textLine = TextLine.obtain();
      int j = paramCharSequence.length();
      Layout.Directions directions = Layout.DIRS_ALL_LEFT_TO_RIGHT;
      i = this.mEllipsizedStart;
      textLine.set(paramTextPaint, paramCharSequence, 0, j, 1, directions, false, null, i, i + this.mEllipsizedCount);
      this.mMax = (int)Math.ceil(textLine.metrics(null));
      TextLine.recycle(textLine);
    } 
    if (paramBoolean1) {
      this.mTopPadding = paramMetrics.top - paramMetrics.ascent;
      this.mBottomPadding = paramMetrics.bottom - paramMetrics.descent;
    } 
  }
  
  public static Metrics isBoring(CharSequence paramCharSequence, TextPaint paramTextPaint) {
    return isBoring(paramCharSequence, paramTextPaint, TextDirectionHeuristics.FIRSTSTRONG_LTR, (Metrics)null);
  }
  
  public static Metrics isBoring(CharSequence paramCharSequence, TextPaint paramTextPaint, Metrics paramMetrics) {
    return isBoring(paramCharSequence, paramTextPaint, TextDirectionHeuristics.FIRSTSTRONG_LTR, paramMetrics);
  }
  
  private static boolean hasAnyInterestingChars(CharSequence paramCharSequence, int paramInt) {
    char[] arrayOfChar = TextUtils.obtain(500);
    for (byte b = 0; b < paramInt;) {
      try {
        int i = Math.min(b + 500, paramInt);
        TextUtils.getChars(paramCharSequence, b, i, arrayOfChar, 0);
        for (byte b1 = 0; b1 < i - b; ) {
          char c = arrayOfChar[b1];
          if (c != '\n' && c != '\t') {
            boolean bool = TextUtils.couldAffectRtl(c);
            if (!bool) {
              b1++;
              continue;
            } 
          } 
          return true;
        } 
      } finally {
        TextUtils.recycle(arrayOfChar);
      } 
    } 
    TextUtils.recycle(arrayOfChar);
    return false;
  }
  
  public static Metrics isBoring(CharSequence paramCharSequence, TextPaint paramTextPaint, TextDirectionHeuristic paramTextDirectionHeuristic, Metrics paramMetrics) {
    // Byte code:
    //   0: aload_0
    //   1: invokeinterface length : ()I
    //   6: istore #4
    //   8: aload_0
    //   9: iload #4
    //   11: invokestatic hasAnyInterestingChars : (Ljava/lang/CharSequence;I)Z
    //   14: ifeq -> 19
    //   17: aconst_null
    //   18: areturn
    //   19: aload_2
    //   20: ifnull -> 38
    //   23: aload_2
    //   24: aload_0
    //   25: iconst_0
    //   26: iload #4
    //   28: invokeinterface isRtl : (Ljava/lang/CharSequence;II)Z
    //   33: ifeq -> 38
    //   36: aconst_null
    //   37: areturn
    //   38: aload_0
    //   39: instanceof android/text/Spanned
    //   42: ifeq -> 69
    //   45: aload_0
    //   46: checkcast android/text/Spanned
    //   49: astore_2
    //   50: aload_2
    //   51: iconst_0
    //   52: iload #4
    //   54: ldc android/text/style/ParagraphStyle
    //   56: invokeinterface getSpans : (IILjava/lang/Class;)[Ljava/lang/Object;
    //   61: astore_2
    //   62: aload_2
    //   63: arraylength
    //   64: ifle -> 69
    //   67: aconst_null
    //   68: areturn
    //   69: aload_3
    //   70: ifnonnull -> 84
    //   73: new android/text/BoringLayout$Metrics
    //   76: dup
    //   77: invokespecial <init> : ()V
    //   80: astore_2
    //   81: goto -> 90
    //   84: aload_3
    //   85: invokestatic access$000 : (Landroid/text/BoringLayout$Metrics;)V
    //   88: aload_3
    //   89: astore_2
    //   90: invokestatic obtain : ()Landroid/text/TextLine;
    //   93: astore_3
    //   94: aload_3
    //   95: aload_1
    //   96: aload_0
    //   97: iconst_0
    //   98: iload #4
    //   100: iconst_1
    //   101: getstatic android/text/Layout.DIRS_ALL_LEFT_TO_RIGHT : Landroid/text/Layout$Directions;
    //   104: iconst_0
    //   105: aconst_null
    //   106: iconst_0
    //   107: iconst_0
    //   108: invokevirtual set : (Landroid/text/TextPaint;Ljava/lang/CharSequence;IIILandroid/text/Layout$Directions;ZLandroid/text/Layout$TabStops;II)V
    //   111: aload_2
    //   112: aload_3
    //   113: aload_2
    //   114: invokevirtual metrics : (Landroid/graphics/Paint$FontMetricsInt;)F
    //   117: f2d
    //   118: invokestatic ceil : (D)D
    //   121: d2i
    //   122: putfield width : I
    //   125: aload_3
    //   126: invokestatic recycle : (Landroid/text/TextLine;)Landroid/text/TextLine;
    //   129: pop
    //   130: aload_2
    //   131: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #339	-> 0
    //   #340	-> 8
    //   #341	-> 17
    //   #343	-> 19
    //   #344	-> 36
    //   #346	-> 38
    //   #347	-> 45
    //   #348	-> 50
    //   #349	-> 62
    //   #350	-> 67
    //   #354	-> 69
    //   #355	-> 69
    //   #356	-> 73
    //   #358	-> 84
    //   #361	-> 90
    //   #362	-> 94
    //   #366	-> 111
    //   #367	-> 125
    //   #369	-> 130
  }
  
  public int getHeight() {
    return this.mBottom;
  }
  
  public int getLineCount() {
    return 1;
  }
  
  public int getLineTop(int paramInt) {
    if (paramInt == 0)
      return 0; 
    return this.mBottom;
  }
  
  public int getLineDescent(int paramInt) {
    return this.mDesc;
  }
  
  public int getLineStart(int paramInt) {
    if (paramInt == 0)
      return 0; 
    return getText().length();
  }
  
  public int getParagraphDirection(int paramInt) {
    return 1;
  }
  
  public boolean getLineContainsTab(int paramInt) {
    return false;
  }
  
  public float getLineMax(int paramInt) {
    return this.mMax;
  }
  
  public float getLineWidth(int paramInt) {
    float f;
    if (paramInt == 0) {
      f = this.mMax;
    } else {
      f = 0.0F;
    } 
    return f;
  }
  
  public final Layout.Directions getLineDirections(int paramInt) {
    return Layout.DIRS_ALL_LEFT_TO_RIGHT;
  }
  
  public int getTopPadding() {
    return this.mTopPadding;
  }
  
  public int getBottomPadding() {
    return this.mBottomPadding;
  }
  
  public int getEllipsisCount(int paramInt) {
    return this.mEllipsizedCount;
  }
  
  public int getEllipsisStart(int paramInt) {
    return this.mEllipsizedStart;
  }
  
  public int getEllipsizedWidth() {
    return this.mEllipsizedWidth;
  }
  
  public void draw(Canvas paramCanvas, Path paramPath, Paint paramPaint, int paramInt) {
    String str = this.mDirect;
    if (str != null && paramPath == null) {
      paramCanvas.drawText(str, 0.0F, (this.mBottom - this.mDesc), this.mPaint);
    } else {
      super.draw(paramCanvas, paramPath, paramPaint, paramInt);
    } 
  }
  
  public void ellipsized(int paramInt1, int paramInt2) {
    this.mEllipsizedStart = paramInt1;
    this.mEllipsizedCount = paramInt2 - paramInt1;
  }
  
  public static class Metrics extends Paint.FontMetricsInt {
    public int width;
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(super.toString());
      stringBuilder.append(" width=");
      stringBuilder.append(this.width);
      return stringBuilder.toString();
    }
    
    private void reset() {
      this.top = 0;
      this.bottom = 0;
      this.ascent = 0;
      this.descent = 0;
      this.width = 0;
      this.leading = 0;
    }
  }
}
