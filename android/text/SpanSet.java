package android.text;

import java.lang.reflect.Array;
import java.util.Arrays;

public class SpanSet<E> {
  private final Class<? extends E> classType;
  
  int numberOfSpans;
  
  int[] spanEnds;
  
  int[] spanFlags;
  
  int[] spanStarts;
  
  E[] spans;
  
  SpanSet(Class<? extends E> paramClass) {
    this.classType = paramClass;
    this.numberOfSpans = 0;
  }
  
  public void init(Spanned paramSpanned, int paramInt1, int paramInt2) {
    Object[] arrayOfObject = paramSpanned.getSpans(paramInt1, paramInt2, (Class)this.classType);
    int i = arrayOfObject.length;
    if (i > 0) {
      E[] arrayOfE = this.spans;
      if (arrayOfE == null || arrayOfE.length < i) {
        this.spans = (E[])Array.newInstance(this.classType, i);
        this.spanStarts = new int[i];
        this.spanEnds = new int[i];
        this.spanFlags = new int[i];
      } 
    } 
    paramInt2 = this.numberOfSpans;
    this.numberOfSpans = 0;
    for (paramInt1 = 0; paramInt1 < i; paramInt1++) {
      Object object = arrayOfObject[paramInt1];
      int j = paramSpanned.getSpanStart(object);
      int k = paramSpanned.getSpanEnd(object);
      if (j != k) {
        int m = paramSpanned.getSpanFlags(object);
        E[] arrayOfE = this.spans;
        int n = this.numberOfSpans;
        arrayOfE[n] = (E)object;
        this.spanStarts[n] = j;
        this.spanEnds[n] = k;
        this.spanFlags[n] = m;
        this.numberOfSpans = n + 1;
      } 
    } 
    paramInt1 = this.numberOfSpans;
    if (paramInt1 < paramInt2)
      Arrays.fill((Object[])this.spans, paramInt1, paramInt2, (Object)null); 
  }
  
  public boolean hasSpansIntersecting(int paramInt1, int paramInt2) {
    for (byte b = 0; b < this.numberOfSpans; ) {
      if (this.spanStarts[b] >= paramInt2 || this.spanEnds[b] <= paramInt1) {
        b++;
        continue;
      } 
      return true;
    } 
    return false;
  }
  
  int getNextTransition(int paramInt1, int paramInt2) {
    int i;
    for (byte b = 0; b < this.numberOfSpans; b++) {
      int j = this.spanStarts[b];
      int k = this.spanEnds[b];
      paramInt2 = i;
      if (j > paramInt1) {
        paramInt2 = i;
        if (j < i)
          paramInt2 = j; 
      } 
      i = paramInt2;
      if (k > paramInt1) {
        i = paramInt2;
        if (k < paramInt2)
          i = k; 
      } 
    } 
    return i;
  }
  
  public void recycle() {
    E[] arrayOfE = this.spans;
    if (arrayOfE != null)
      Arrays.fill((Object[])arrayOfE, 0, this.numberOfSpans, (Object)null); 
  }
}
