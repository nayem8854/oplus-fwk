package android.text.util;

import android.app.ActivityThread;
import android.app.Application;
import android.content.Context;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.text.style.URLSpan;
import android.util.EventLog;
import android.util.Log;
import android.util.Patterns;
import android.webkit.WebView;
import android.widget.TextView;
import com.android.i18n.phonenumbers.PhoneNumberMatch;
import com.android.i18n.phonenumbers.PhoneNumberUtil;
import java.io.UnsupportedEncodingException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Linkify {
  public static final int ALL = 15;
  
  private static final Function<String, URLSpan> DEFAULT_SPAN_FACTORY;
  
  public static final int EMAIL_ADDRESSES = 2;
  
  private static final String LOG_TAG = "Linkify";
  
  @Deprecated
  public static final int MAP_ADDRESSES = 8;
  
  public static final int PHONE_NUMBERS = 4;
  
  private static final int PHONE_NUMBER_MINIMUM_DIGITS = 5;
  
  public static final int WEB_URLS = 1;
  
  public static final MatchFilter sPhoneNumberMatchFilter;
  
  public static final TransformFilter sPhoneNumberTransformFilter;
  
  public static final MatchFilter sUrlMatchFilter = (MatchFilter)new Object();
  
  static {
    sPhoneNumberMatchFilter = (MatchFilter)new Object();
    sPhoneNumberTransformFilter = (TransformFilter)new Object();
    DEFAULT_SPAN_FACTORY = (Function<String, URLSpan>)_$$Lambda$Linkify$7J__cMhIF2bcttjkxA2jDFP8sKw.INSTANCE;
  }
  
  public static final boolean addLinks(Spannable paramSpannable, int paramInt) {
    return addLinks(paramSpannable, paramInt, null, null);
  }
  
  public static final boolean addLinks(Spannable paramSpannable, int paramInt, Function<String, URLSpan> paramFunction) {
    return addLinks(paramSpannable, paramInt, null, paramFunction);
  }
  
  private static boolean addLinks(Spannable paramSpannable, int paramInt, Context paramContext, Function<String, URLSpan> paramFunction) {
    if (paramSpannable != null && containsUnsupportedCharacters(paramSpannable.toString())) {
      EventLog.writeEvent(1397638484, new Object[] { "116321860", Integer.valueOf(-1), "" });
      return false;
    } 
    if (paramInt == 0)
      return false; 
    URLSpan[] arrayOfURLSpan = paramSpannable.<URLSpan>getSpans(0, paramSpannable.length(), URLSpan.class);
    for (int i = arrayOfURLSpan.length - 1; i >= 0; i--)
      paramSpannable.removeSpan(arrayOfURLSpan[i]); 
    ArrayList<LinkSpec> arrayList = new ArrayList();
    if ((paramInt & 0x1) != 0) {
      Pattern pattern = Patterns.AUTOLINK_WEB_URL;
      MatchFilter matchFilter = sUrlMatchFilter;
      gatherLinks(arrayList, paramSpannable, pattern, new String[] { "http://", "https://", "rtsp://" }, matchFilter, null);
    } 
    if ((paramInt & 0x2) != 0)
      gatherLinks(arrayList, paramSpannable, Patterns.AUTOLINK_EMAIL_ADDRESS, new String[] { "mailto:" }, null, null); 
    if ((paramInt & 0x4) != 0)
      gatherTelLinks(arrayList, paramSpannable, paramContext); 
    if ((paramInt & 0x8) != 0)
      gatherMapLinks(arrayList, paramSpannable); 
    pruneOverlaps(arrayList);
    if (arrayList.size() == 0)
      return false; 
    for (LinkSpec linkSpec : arrayList)
      applyLink(linkSpec.url, linkSpec.start, linkSpec.end, paramSpannable, paramFunction); 
    return true;
  }
  
  public static boolean containsUnsupportedCharacters(String paramString) {
    if (paramString.contains("‬")) {
      Log.e("Linkify", "Unsupported character for applying links: u202C");
      return true;
    } 
    if (paramString.contains("‭")) {
      Log.e("Linkify", "Unsupported character for applying links: u202D");
      return true;
    } 
    if (paramString.contains("‮")) {
      Log.e("Linkify", "Unsupported character for applying links: u202E");
      return true;
    } 
    return false;
  }
  
  public static final boolean addLinks(TextView paramTextView, int paramInt) {
    if (paramInt == 0)
      return false; 
    Context context = paramTextView.getContext();
    CharSequence charSequence = paramTextView.getText();
    if (charSequence instanceof Spannable) {
      if (addLinks((Spannable)charSequence, paramInt, context, null)) {
        addLinkMovementMethod(paramTextView);
        return true;
      } 
      return false;
    } 
    charSequence = SpannableString.valueOf(charSequence);
    if (addLinks((Spannable)charSequence, paramInt, context, null)) {
      addLinkMovementMethod(paramTextView);
      paramTextView.setText(charSequence);
      return true;
    } 
    return false;
  }
  
  private static final void addLinkMovementMethod(TextView paramTextView) {
    MovementMethod movementMethod = paramTextView.getMovementMethod();
    if ((movementMethod == null || !(movementMethod instanceof LinkMovementMethod)) && paramTextView.getLinksClickable())
      paramTextView.setMovementMethod(LinkMovementMethod.getInstance()); 
  }
  
  public static final void addLinks(TextView paramTextView, Pattern paramPattern, String paramString) {
    addLinks(paramTextView, paramPattern, paramString, (String[])null, (MatchFilter)null, (TransformFilter)null);
  }
  
  public static final void addLinks(TextView paramTextView, Pattern paramPattern, String paramString, MatchFilter paramMatchFilter, TransformFilter paramTransformFilter) {
    addLinks(paramTextView, paramPattern, paramString, (String[])null, paramMatchFilter, paramTransformFilter);
  }
  
  public static final void addLinks(TextView paramTextView, Pattern paramPattern, String paramString, String[] paramArrayOfString, MatchFilter paramMatchFilter, TransformFilter paramTransformFilter) {
    SpannableString spannableString = SpannableString.valueOf(paramTextView.getText());
    boolean bool = addLinks(spannableString, paramPattern, paramString, paramArrayOfString, paramMatchFilter, paramTransformFilter);
    if (bool) {
      paramTextView.setText(spannableString);
      addLinkMovementMethod(paramTextView);
    } 
  }
  
  public static final boolean addLinks(Spannable paramSpannable, Pattern paramPattern, String paramString) {
    return addLinks(paramSpannable, paramPattern, paramString, (String[])null, (MatchFilter)null, (TransformFilter)null);
  }
  
  public static final boolean addLinks(Spannable paramSpannable, Pattern paramPattern, String paramString, MatchFilter paramMatchFilter, TransformFilter paramTransformFilter) {
    return addLinks(paramSpannable, paramPattern, paramString, (String[])null, paramMatchFilter, paramTransformFilter);
  }
  
  public static final boolean addLinks(Spannable paramSpannable, Pattern paramPattern, String paramString, String[] paramArrayOfString, MatchFilter paramMatchFilter, TransformFilter paramTransformFilter) {
    return addLinks(paramSpannable, paramPattern, paramString, paramArrayOfString, paramMatchFilter, paramTransformFilter, null);
  }
  
  public static final boolean addLinks(Spannable paramSpannable, Pattern paramPattern, String paramString, String[] paramArrayOfString, MatchFilter paramMatchFilter, TransformFilter paramTransformFilter, Function<String, URLSpan> paramFunction) {
    // Byte code:
    //   0: aload_0
    //   1: ifnull -> 43
    //   4: aload_0
    //   5: invokevirtual toString : ()Ljava/lang/String;
    //   8: invokestatic containsUnsupportedCharacters : (Ljava/lang/String;)Z
    //   11: ifeq -> 43
    //   14: ldc 1397638484
    //   16: iconst_3
    //   17: anewarray java/lang/Object
    //   20: dup
    //   21: iconst_0
    //   22: ldc '116321860'
    //   24: aastore
    //   25: dup
    //   26: iconst_1
    //   27: iconst_m1
    //   28: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   31: aastore
    //   32: dup
    //   33: iconst_2
    //   34: ldc ''
    //   36: aastore
    //   37: invokestatic writeEvent : (I[Ljava/lang/Object;)I
    //   40: pop
    //   41: iconst_0
    //   42: ireturn
    //   43: aload_2
    //   44: astore #7
    //   46: aload_2
    //   47: ifnonnull -> 54
    //   50: ldc ''
    //   52: astore #7
    //   54: aload_3
    //   55: ifnull -> 66
    //   58: aload_3
    //   59: astore_2
    //   60: aload_3
    //   61: arraylength
    //   62: iconst_1
    //   63: if_icmpge -> 70
    //   66: getstatic libcore/util/EmptyArray.STRING : [Ljava/lang/String;
    //   69: astore_2
    //   70: aload_2
    //   71: arraylength
    //   72: iconst_1
    //   73: iadd
    //   74: anewarray java/lang/String
    //   77: astore #8
    //   79: aload #8
    //   81: iconst_0
    //   82: aload #7
    //   84: getstatic java/util/Locale.ROOT : Ljava/util/Locale;
    //   87: invokevirtual toLowerCase : (Ljava/util/Locale;)Ljava/lang/String;
    //   90: aastore
    //   91: iconst_0
    //   92: istore #9
    //   94: iload #9
    //   96: aload_2
    //   97: arraylength
    //   98: if_icmpge -> 138
    //   101: aload_2
    //   102: iload #9
    //   104: aaload
    //   105: astore_3
    //   106: aload_3
    //   107: ifnonnull -> 116
    //   110: ldc ''
    //   112: astore_3
    //   113: goto -> 124
    //   116: aload_3
    //   117: getstatic java/util/Locale.ROOT : Ljava/util/Locale;
    //   120: invokevirtual toLowerCase : (Ljava/util/Locale;)Ljava/lang/String;
    //   123: astore_3
    //   124: aload #8
    //   126: iload #9
    //   128: iconst_1
    //   129: iadd
    //   130: aload_3
    //   131: aastore
    //   132: iinc #9, 1
    //   135: goto -> 94
    //   138: iconst_0
    //   139: istore #10
    //   141: aload_1
    //   142: aload_0
    //   143: invokevirtual matcher : (Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
    //   146: astore_2
    //   147: aload_2
    //   148: invokevirtual find : ()Z
    //   151: ifeq -> 224
    //   154: aload_2
    //   155: invokevirtual start : ()I
    //   158: istore #11
    //   160: aload_2
    //   161: invokevirtual end : ()I
    //   164: istore #9
    //   166: iconst_1
    //   167: istore #12
    //   169: aload #4
    //   171: ifnull -> 188
    //   174: aload #4
    //   176: aload_0
    //   177: iload #11
    //   179: iload #9
    //   181: invokeinterface acceptMatch : (Ljava/lang/CharSequence;II)Z
    //   186: istore #12
    //   188: iload #12
    //   190: ifeq -> 221
    //   193: aload_2
    //   194: iconst_0
    //   195: invokevirtual group : (I)Ljava/lang/String;
    //   198: aload #8
    //   200: aload_2
    //   201: aload #5
    //   203: invokestatic makeUrl : (Ljava/lang/String;[Ljava/lang/String;Ljava/util/regex/Matcher;Landroid/text/util/Linkify$TransformFilter;)Ljava/lang/String;
    //   206: astore_1
    //   207: aload_1
    //   208: iload #11
    //   210: iload #9
    //   212: aload_0
    //   213: aload #6
    //   215: invokestatic applyLink : (Ljava/lang/String;IILandroid/text/Spannable;Ljava/util/function/Function;)V
    //   218: iconst_1
    //   219: istore #10
    //   221: goto -> 147
    //   224: iload #10
    //   226: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #563	-> 0
    //   #564	-> 14
    //   #565	-> 41
    //   #569	-> 43
    //   #570	-> 54
    //   #571	-> 66
    //   #574	-> 70
    //   #575	-> 79
    //   #576	-> 91
    //   #577	-> 101
    //   #578	-> 106
    //   #576	-> 132
    //   #581	-> 138
    //   #582	-> 141
    //   #584	-> 147
    //   #585	-> 154
    //   #586	-> 160
    //   #587	-> 166
    //   #589	-> 169
    //   #590	-> 174
    //   #593	-> 188
    //   #594	-> 193
    //   #596	-> 207
    //   #597	-> 218
    //   #599	-> 221
    //   #601	-> 224
  }
  
  private static void applyLink(String paramString, int paramInt1, int paramInt2, Spannable paramSpannable, Function<String, URLSpan> paramFunction) {
    Function<String, URLSpan> function = paramFunction;
    if (paramFunction == null)
      function = DEFAULT_SPAN_FACTORY; 
    URLSpan uRLSpan = function.apply(paramString);
    paramSpannable.setSpan(uRLSpan, paramInt1, paramInt2, 33);
  }
  
  private static final String makeUrl(String paramString, String[] paramArrayOfString, Matcher paramMatcher, TransformFilter paramTransformFilter) {
    String str1;
    boolean bool2;
    String str3 = paramString;
    if (paramTransformFilter != null)
      str3 = paramTransformFilter.transformUrl(paramMatcher, paramString); 
    boolean bool1 = false;
    byte b = 0;
    while (true) {
      bool2 = bool1;
      paramString = str3;
      if (b < paramArrayOfString.length) {
        if (str3.regionMatches(true, 0, paramArrayOfString[b], 0, paramArrayOfString[b].length())) {
          bool1 = true;
          bool2 = bool1;
          paramString = str3;
          if (!str3.regionMatches(false, 0, paramArrayOfString[b], 0, paramArrayOfString[b].length())) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(paramArrayOfString[b]);
            stringBuilder.append(str3.substring(paramArrayOfString[b].length()));
            str1 = stringBuilder.toString();
            bool2 = bool1;
          } 
          break;
        } 
        b++;
        continue;
      } 
      break;
    } 
    String str2 = str1;
    if (!bool2) {
      str2 = str1;
      if (paramArrayOfString.length > 0) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(paramArrayOfString[0]);
        stringBuilder.append(str1);
        str2 = stringBuilder.toString();
      } 
    } 
    return str2;
  }
  
  private static final void gatherLinks(ArrayList<LinkSpec> paramArrayList, Spannable paramSpannable, Pattern paramPattern, String[] paramArrayOfString, MatchFilter paramMatchFilter, TransformFilter paramTransformFilter) {
    Matcher matcher = paramPattern.matcher(paramSpannable);
    while (matcher.find()) {
      int i = matcher.start();
      int j = matcher.end();
      if (paramMatchFilter == null || paramMatchFilter.acceptMatch(paramSpannable, i, j)) {
        LinkSpec linkSpec = new LinkSpec();
        String str = makeUrl(matcher.group(0), paramArrayOfString, matcher, paramTransformFilter);
        linkSpec.url = str;
        linkSpec.start = i;
        linkSpec.end = j;
        paramArrayList.add(linkSpec);
      } 
    } 
  }
  
  private static void gatherTelLinks(ArrayList<LinkSpec> paramArrayList, Spannable paramSpannable, Context paramContext) {
    Application application;
    String str;
    PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
    if (paramContext == null)
      application = ActivityThread.currentApplication(); 
    if (application != null) {
      TelephonyManager telephonyManager = (TelephonyManager)application.getSystemService(TelephonyManager.class);
      str = telephonyManager.getSimCountryIso().toUpperCase(Locale.US);
    } else {
      str = Locale.getDefault().getCountry();
    } 
    Iterable iterable = phoneNumberUtil.findNumbers(paramSpannable.toString(), str, PhoneNumberUtil.Leniency.POSSIBLE, Long.MAX_VALUE);
    for (PhoneNumberMatch phoneNumberMatch : iterable) {
      LinkSpec linkSpec = new LinkSpec();
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("tel:");
      stringBuilder.append(PhoneNumberUtils.normalizeNumber(phoneNumberMatch.rawString()));
      linkSpec.url = stringBuilder.toString();
      linkSpec.start = phoneNumberMatch.start();
      linkSpec.end = phoneNumberMatch.end();
      paramArrayList.add(linkSpec);
    } 
  }
  
  private static final void gatherMapLinks(ArrayList<LinkSpec> paramArrayList, Spannable paramSpannable) {
    String str = paramSpannable.toString();
    int i = 0;
    try {
      while (true) {
        String str1 = WebView.findAddress(str);
        if (str1 != null) {
          int j = str.indexOf(str1);
          if (j < 0)
            break; 
          LinkSpec linkSpec = new LinkSpec();
          this();
          int k = str1.length();
          k = j + k;
          linkSpec.start = i + j;
          linkSpec.end = i + k;
          str = str.substring(k);
          i += k;
          try {
            String str2 = URLEncoder.encode(str1, "UTF-8");
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("geo:0,0?q=");
            stringBuilder.append(str2);
            linkSpec.url = stringBuilder.toString();
            paramArrayList.add(linkSpec);
          } catch (UnsupportedEncodingException unsupportedEncodingException) {}
          continue;
        } 
        break;
      } 
      return;
    } catch (UnsupportedOperationException unsupportedOperationException) {
      return;
    } 
  }
  
  private static final void pruneOverlaps(ArrayList<LinkSpec> paramArrayList) {
    Comparator<LinkSpec> comparator = new Comparator<LinkSpec>() {
        public final int compare(LinkSpec param1LinkSpec1, LinkSpec param1LinkSpec2) {
          if (param1LinkSpec1.start < param1LinkSpec2.start)
            return -1; 
          if (param1LinkSpec1.start > param1LinkSpec2.start)
            return 1; 
          if (param1LinkSpec1.end < param1LinkSpec2.end)
            return 1; 
          if (param1LinkSpec1.end > param1LinkSpec2.end)
            return -1; 
          return 0;
        }
      };
    Collections.sort(paramArrayList, comparator);
    int i = paramArrayList.size();
    byte b = 0;
    while (b < i - 1) {
      LinkSpec linkSpec2 = paramArrayList.get(b);
      LinkSpec linkSpec1 = paramArrayList.get(b + 1);
      int j = -1;
      if (linkSpec2.start <= linkSpec1.start && linkSpec2.end > linkSpec1.start) {
        if (linkSpec1.end <= linkSpec2.end) {
          j = b + 1;
        } else if (linkSpec2.end - linkSpec2.start > linkSpec1.end - linkSpec1.start) {
          j = b + 1;
        } else if (linkSpec2.end - linkSpec2.start < linkSpec1.end - linkSpec1.start) {
          j = b;
        } 
        if (j != -1) {
          paramArrayList.remove(j);
          i--;
          continue;
        } 
      } 
      b++;
    } 
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface LinkifyMask {}
  
  public static interface MatchFilter {
    boolean acceptMatch(CharSequence param1CharSequence, int param1Int1, int param1Int2);
  }
  
  public static interface TransformFilter {
    String transformUrl(Matcher param1Matcher, String param1String);
  }
}
