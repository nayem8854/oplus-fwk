package android.text.util;

public class Rfc822Token {
  private String mAddress;
  
  private String mComment;
  
  private String mName;
  
  public Rfc822Token(String paramString1, String paramString2, String paramString3) {
    this.mName = paramString1;
    this.mAddress = paramString2;
    this.mComment = paramString3;
  }
  
  public String getName() {
    return this.mName;
  }
  
  public String getAddress() {
    return this.mAddress;
  }
  
  public String getComment() {
    return this.mComment;
  }
  
  public void setName(String paramString) {
    this.mName = paramString;
  }
  
  public void setAddress(String paramString) {
    this.mAddress = paramString;
  }
  
  public void setComment(String paramString) {
    this.mComment = paramString;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    String str = this.mName;
    if (str != null && str.length() != 0) {
      stringBuilder.append(quoteNameIfNecessary(this.mName));
      stringBuilder.append(' ');
    } 
    str = this.mComment;
    if (str != null && str.length() != 0) {
      stringBuilder.append('(');
      stringBuilder.append(quoteComment(this.mComment));
      stringBuilder.append(") ");
    } 
    str = this.mAddress;
    if (str != null && str.length() != 0) {
      stringBuilder.append('<');
      stringBuilder.append(this.mAddress);
      stringBuilder.append('>');
    } 
    return stringBuilder.toString();
  }
  
  public static String quoteNameIfNecessary(String paramString) {
    int i = paramString.length();
    for (byte b = 0; b < i; b++) {
      char c = paramString.charAt(b);
      if ((c < 'A' || c > 'Z') && (c < 'a' || c > 'z') && c != ' ' && (c < '0' || c > '9')) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append('"');
        stringBuilder.append(quoteName(paramString));
        stringBuilder.append('"');
        return stringBuilder.toString();
      } 
    } 
    return paramString;
  }
  
  public static String quoteName(String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    int i = paramString.length();
    for (byte b = 0; b < i; b++) {
      char c = paramString.charAt(b);
      if (c == '\\' || c == '"')
        stringBuilder.append('\\'); 
      stringBuilder.append(c);
    } 
    return stringBuilder.toString();
  }
  
  public static String quoteComment(String paramString) {
    int i = paramString.length();
    StringBuilder stringBuilder = new StringBuilder();
    for (byte b = 0; b < i; b++) {
      char c = paramString.charAt(b);
      if (c == '(' || c == ')' || c == '\\')
        stringBuilder.append('\\'); 
      stringBuilder.append(c);
    } 
    return stringBuilder.toString();
  }
  
  public int hashCode() {
    int i = 17;
    String str = this.mName;
    if (str != null)
      i = 17 * 31 + str.hashCode(); 
    str = this.mAddress;
    int j = i;
    if (str != null)
      j = i * 31 + str.hashCode(); 
    str = this.mComment;
    i = j;
    if (str != null)
      i = j * 31 + str.hashCode(); 
    return i;
  }
  
  private static boolean stringEquals(String paramString1, String paramString2) {
    if (paramString1 == null) {
      boolean bool;
      if (paramString2 == null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    } 
    return paramString1.equals(paramString2);
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof Rfc822Token;
    boolean bool1 = false;
    if (!bool)
      return false; 
    paramObject = paramObject;
    if (stringEquals(this.mName, ((Rfc822Token)paramObject).mName)) {
      String str1 = this.mAddress, str2 = ((Rfc822Token)paramObject).mAddress;
      if (stringEquals(str1, str2)) {
        str1 = this.mComment;
        paramObject = ((Rfc822Token)paramObject).mComment;
        if (stringEquals(str1, (String)paramObject))
          bool1 = true; 
      } 
    } 
    return bool1;
  }
}
