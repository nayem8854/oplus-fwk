package android.text.util;

import android.widget.MultiAutoCompleteTextView;
import java.util.ArrayList;
import java.util.Collection;

public class Rfc822Tokenizer implements MultiAutoCompleteTextView.Tokenizer {
  public static void tokenize(CharSequence paramCharSequence, Collection<Rfc822Token> paramCollection) {
    StringBuilder stringBuilder1 = new StringBuilder();
    StringBuilder stringBuilder2 = new StringBuilder();
    StringBuilder stringBuilder3 = new StringBuilder();
    int i = 0;
    int j = paramCharSequence.length();
    label91: while (i < j) {
      char c = paramCharSequence.charAt(i);
      if (c == ',' || c == ';') {
        i++;
        while (i < j && paramCharSequence.charAt(i) == ' ')
          i++; 
        crunch(stringBuilder1);
        if (stringBuilder2.length() > 0) {
          String str1 = stringBuilder1.toString();
          String str2 = stringBuilder2.toString();
          Rfc822Token rfc822Token = new Rfc822Token(str1, str2, stringBuilder3.toString());
          paramCollection.add(rfc822Token);
        } else if (stringBuilder1.length() > 0) {
          String str = stringBuilder1.toString();
          Rfc822Token rfc822Token = new Rfc822Token(null, str, stringBuilder3.toString());
          paramCollection.add(rfc822Token);
        } 
        stringBuilder1.setLength(0);
        stringBuilder2.setLength(0);
        stringBuilder3.setLength(0);
        continue;
      } 
      if (c == '"') {
        int k = i + 1;
        while (true) {
          i = k;
          if (k < j) {
            c = paramCharSequence.charAt(k);
            if (c == '"') {
              i = k + 1;
              continue label91;
            } 
            if (c == '\\') {
              if (k + 1 < j)
                stringBuilder1.append(paramCharSequence.charAt(k + 1)); 
              k += 2;
              continue;
            } 
            stringBuilder1.append(c);
            k++;
            continue;
          } 
          continue label91;
        } 
      } 
      if (c == '(') {
        byte b = 1;
        i++;
        while (i < j && b) {
          c = paramCharSequence.charAt(i);
          if (c == ')') {
            if (b > 1)
              stringBuilder3.append(c); 
            b--;
            i++;
            continue;
          } 
          if (c == '(') {
            stringBuilder3.append(c);
            b++;
            i++;
            continue;
          } 
          if (c == '\\') {
            if (i + 1 < j)
              stringBuilder3.append(paramCharSequence.charAt(i + 1)); 
            i += 2;
            continue;
          } 
          stringBuilder3.append(c);
          i++;
        } 
        continue;
      } 
      if (c == '<') {
        int k = i + 1;
        while (true) {
          i = k;
          if (k < j) {
            c = paramCharSequence.charAt(k);
            if (c == '>') {
              i = k + 1;
              continue label91;
            } 
            stringBuilder2.append(c);
            k++;
            continue;
          } 
          continue label91;
        } 
      } 
      if (c == ' ') {
        stringBuilder1.append(false);
        i++;
        continue;
      } 
      stringBuilder1.append(c);
      i++;
    } 
    crunch(stringBuilder1);
    if (stringBuilder2.length() > 0) {
      paramCharSequence = stringBuilder1.toString();
      String str = stringBuilder2.toString();
      Rfc822Token rfc822Token = new Rfc822Token((String)paramCharSequence, str, stringBuilder3.toString());
      paramCollection.add(rfc822Token);
    } else if (stringBuilder1.length() > 0) {
      paramCharSequence = stringBuilder1.toString();
      Rfc822Token rfc822Token = new Rfc822Token(null, (String)paramCharSequence, stringBuilder3.toString());
      paramCollection.add(rfc822Token);
    } 
  }
  
  public static Rfc822Token[] tokenize(CharSequence paramCharSequence) {
    ArrayList<Rfc822Token> arrayList = new ArrayList();
    tokenize(paramCharSequence, arrayList);
    return arrayList.<Rfc822Token>toArray(new Rfc822Token[arrayList.size()]);
  }
  
  private static void crunch(StringBuilder paramStringBuilder) {
    byte b = 0;
    int i = paramStringBuilder.length();
    while (b < i) {
      char c = paramStringBuilder.charAt(b);
      if (c == '\000') {
        if (b == 0 || b == i - 1 || 
          paramStringBuilder.charAt(b - 1) == ' ' || 
          paramStringBuilder.charAt(b - 1) == '\000' || 
          paramStringBuilder.charAt(b + 1) == ' ' || 
          paramStringBuilder.charAt(b + 1) == '\000') {
          paramStringBuilder.deleteCharAt(b);
          i--;
          continue;
        } 
        b++;
        continue;
      } 
      b++;
    } 
    for (b = 0; b < i; b++) {
      if (paramStringBuilder.charAt(b) == '\000')
        paramStringBuilder.setCharAt(b, ' '); 
    } 
  }
  
  public int findTokenStart(CharSequence paramCharSequence, int paramInt) {
    int i = 0;
    int j = 0;
    while (j < paramInt) {
      int k = findTokenEnd(paramCharSequence, j);
      j = k;
      if (k < paramInt) {
        k++;
        while (k < paramInt && paramCharSequence.charAt(k) == ' ')
          k++; 
        j = k;
        if (k < paramInt) {
          i = k;
          j = k;
        } 
      } 
    } 
    return i;
  }
  
  public int findTokenEnd(CharSequence paramCharSequence, int paramInt) {
    int i = paramCharSequence.length();
    label61: while (paramInt < i) {
      int j = paramCharSequence.charAt(paramInt);
      if (j == 44 || j == 59)
        return paramInt; 
      if (j == 34) {
        j = paramInt + 1;
        while (true) {
          paramInt = j;
          if (j < i) {
            paramInt = paramCharSequence.charAt(j);
            if (paramInt == 34) {
              paramInt = j + 1;
              continue label61;
            } 
            if (paramInt == 92 && j + 1 < i) {
              j += 2;
              continue;
            } 
            j++;
            continue;
          } 
          continue label61;
        } 
      } 
      if (j == 40) {
        j = 1;
        paramInt++;
        while (paramInt < i && j > 0) {
          char c = paramCharSequence.charAt(paramInt);
          if (c == ')') {
            j--;
            paramInt++;
            continue;
          } 
          if (c == '(') {
            j++;
            paramInt++;
            continue;
          } 
          if (c == '\\' && paramInt + 1 < i) {
            paramInt += 2;
            continue;
          } 
          paramInt++;
        } 
        continue;
      } 
      if (j == 60) {
        j = paramInt + 1;
        while (true) {
          paramInt = j;
          if (j < i) {
            paramInt = paramCharSequence.charAt(j);
            if (paramInt == 62) {
              paramInt = j + 1;
              continue label61;
            } 
            j++;
            continue;
          } 
          continue label61;
        } 
      } 
      paramInt++;
    } 
    return paramInt;
  }
  
  public CharSequence terminateToken(CharSequence paramCharSequence) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramCharSequence);
    stringBuilder.append(", ");
    return stringBuilder.toString();
  }
}
