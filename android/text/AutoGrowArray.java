package android.text;

import com.android.internal.util.ArrayUtils;
import libcore.util.EmptyArray;

public final class AutoGrowArray {
  private static final int MAX_CAPACITY_TO_BE_KEPT = 10000;
  
  private static final int MIN_CAPACITY_INCREMENT = 12;
  
  private static int computeNewCapacity(int paramInt1, int paramInt2) {
    int i;
    if (paramInt1 < 6) {
      i = 12;
    } else {
      i = paramInt1 >> 1;
    } 
    paramInt1 = i + paramInt1;
    if (paramInt1 <= paramInt2)
      paramInt1 = paramInt2; 
    return paramInt1;
  }
  
  public static class ByteArray {
    private int mSize;
    
    private byte[] mValues;
    
    public ByteArray() {
      this(10);
    }
    
    public ByteArray(int param1Int) {
      if (param1Int == 0) {
        this.mValues = EmptyArray.BYTE;
      } else {
        this.mValues = ArrayUtils.newUnpaddedByteArray(param1Int);
      } 
      this.mSize = 0;
    }
    
    public void resize(int param1Int) {
      if (param1Int > this.mValues.length)
        ensureCapacity(param1Int - this.mSize); 
      this.mSize = param1Int;
    }
    
    public void append(byte param1Byte) {
      ensureCapacity(1);
      byte[] arrayOfByte = this.mValues;
      int i = this.mSize;
      this.mSize = i + 1;
      arrayOfByte[i] = param1Byte;
    }
    
    private void ensureCapacity(int param1Int) {
      int i = this.mSize;
      param1Int = i + param1Int;
      if (param1Int >= this.mValues.length) {
        param1Int = AutoGrowArray.computeNewCapacity(i, param1Int);
        byte[] arrayOfByte = ArrayUtils.newUnpaddedByteArray(param1Int);
        System.arraycopy(this.mValues, 0, arrayOfByte, 0, this.mSize);
        this.mValues = arrayOfByte;
      } 
    }
    
    public void clear() {
      this.mSize = 0;
    }
    
    public void clearWithReleasingLargeArray() {
      clear();
      if (this.mValues.length > 10000)
        this.mValues = EmptyArray.BYTE; 
    }
    
    public byte get(int param1Int) {
      return this.mValues[param1Int];
    }
    
    public void set(int param1Int, byte param1Byte) {
      this.mValues[param1Int] = param1Byte;
    }
    
    public int size() {
      return this.mSize;
    }
    
    public byte[] getRawArray() {
      return this.mValues;
    }
  }
  
  public static class IntArray {
    private int mSize;
    
    private int[] mValues;
    
    public IntArray() {
      this(10);
    }
    
    public IntArray(int param1Int) {
      if (param1Int == 0) {
        this.mValues = EmptyArray.INT;
      } else {
        this.mValues = ArrayUtils.newUnpaddedIntArray(param1Int);
      } 
      this.mSize = 0;
    }
    
    public void resize(int param1Int) {
      if (param1Int > this.mValues.length)
        ensureCapacity(param1Int - this.mSize); 
      this.mSize = param1Int;
    }
    
    public void append(int param1Int) {
      ensureCapacity(1);
      int arrayOfInt[] = this.mValues, i = this.mSize;
      this.mSize = i + 1;
      arrayOfInt[i] = param1Int;
    }
    
    private void ensureCapacity(int param1Int) {
      int i = this.mSize;
      param1Int = i + param1Int;
      if (param1Int >= this.mValues.length) {
        param1Int = AutoGrowArray.computeNewCapacity(i, param1Int);
        int[] arrayOfInt = ArrayUtils.newUnpaddedIntArray(param1Int);
        System.arraycopy(this.mValues, 0, arrayOfInt, 0, this.mSize);
        this.mValues = arrayOfInt;
      } 
    }
    
    public void clear() {
      this.mSize = 0;
    }
    
    public void clearWithReleasingLargeArray() {
      clear();
      if (this.mValues.length > 10000)
        this.mValues = EmptyArray.INT; 
    }
    
    public int get(int param1Int) {
      return this.mValues[param1Int];
    }
    
    public void set(int param1Int1, int param1Int2) {
      this.mValues[param1Int1] = param1Int2;
    }
    
    public int size() {
      return this.mSize;
    }
    
    public int[] getRawArray() {
      return this.mValues;
    }
  }
  
  public static class FloatArray {
    private int mSize;
    
    private float[] mValues;
    
    public FloatArray() {
      this(10);
    }
    
    public FloatArray(int param1Int) {
      if (param1Int == 0) {
        this.mValues = EmptyArray.FLOAT;
      } else {
        this.mValues = ArrayUtils.newUnpaddedFloatArray(param1Int);
      } 
      this.mSize = 0;
    }
    
    public void resize(int param1Int) {
      if (param1Int > this.mValues.length)
        ensureCapacity(param1Int - this.mSize); 
      this.mSize = param1Int;
    }
    
    public void append(float param1Float) {
      ensureCapacity(1);
      float[] arrayOfFloat = this.mValues;
      int i = this.mSize;
      this.mSize = i + 1;
      arrayOfFloat[i] = param1Float;
    }
    
    private void ensureCapacity(int param1Int) {
      int i = this.mSize;
      param1Int = i + param1Int;
      if (param1Int >= this.mValues.length) {
        param1Int = AutoGrowArray.computeNewCapacity(i, param1Int);
        float[] arrayOfFloat = ArrayUtils.newUnpaddedFloatArray(param1Int);
        System.arraycopy(this.mValues, 0, arrayOfFloat, 0, this.mSize);
        this.mValues = arrayOfFloat;
      } 
    }
    
    public void clear() {
      this.mSize = 0;
    }
    
    public void clearWithReleasingLargeArray() {
      clear();
      if (this.mValues.length > 10000)
        this.mValues = EmptyArray.FLOAT; 
    }
    
    public float get(int param1Int) {
      return this.mValues[param1Int];
    }
    
    public void set(int param1Int, float param1Float) {
      this.mValues[param1Int] = param1Float;
    }
    
    public int size() {
      return this.mSize;
    }
    
    public float[] getRawArray() {
      return this.mValues;
    }
  }
}
