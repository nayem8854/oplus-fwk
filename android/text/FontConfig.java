package android.text;

import android.graphics.fonts.FontVariationAxis;
import android.net.Uri;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class FontConfig {
  private final Alias[] mAliases;
  
  private final Family[] mFamilies;
  
  public FontConfig(Family[] paramArrayOfFamily, Alias[] paramArrayOfAlias) {
    this.mFamilies = paramArrayOfFamily;
    this.mAliases = paramArrayOfAlias;
  }
  
  public Family[] getFamilies() {
    return this.mFamilies;
  }
  
  public Alias[] getAliases() {
    return this.mAliases;
  }
  
  public static final class Font {
    private final FontVariationAxis[] mAxes;
    
    private final String mFallbackFor;
    
    private final String mFontName;
    
    private final boolean mIsItalic;
    
    private final int mTtcIndex;
    
    private Uri mUri;
    
    private final int mWeight;
    
    public Font(String param1String1, int param1Int1, FontVariationAxis[] param1ArrayOfFontVariationAxis, int param1Int2, boolean param1Boolean, String param1String2) {
      this.mFontName = param1String1;
      this.mTtcIndex = param1Int1;
      this.mAxes = param1ArrayOfFontVariationAxis;
      this.mWeight = param1Int2;
      this.mIsItalic = param1Boolean;
      this.mFallbackFor = param1String2;
    }
    
    public String getFontName() {
      return this.mFontName;
    }
    
    public int getTtcIndex() {
      return this.mTtcIndex;
    }
    
    public FontVariationAxis[] getAxes() {
      return this.mAxes;
    }
    
    public int getWeight() {
      return this.mWeight;
    }
    
    public boolean isItalic() {
      return this.mIsItalic;
    }
    
    public Uri getUri() {
      return this.mUri;
    }
    
    public void setUri(Uri param1Uri) {
      this.mUri = param1Uri;
    }
    
    public String getFallbackFor() {
      return this.mFallbackFor;
    }
  }
  
  public static final class Alias {
    private final String mName;
    
    private final String mToName;
    
    private final int mWeight;
    
    public Alias(String param1String1, String param1String2, int param1Int) {
      this.mName = param1String1;
      this.mToName = param1String2;
      this.mWeight = param1Int;
    }
    
    public String getName() {
      return this.mName;
    }
    
    public String getToName() {
      return this.mToName;
    }
    
    public int getWeight() {
      return this.mWeight;
    }
  }
  
  public static final class Family {
    public static final int VARIANT_COMPACT = 1;
    
    public static final int VARIANT_DEFAULT = 0;
    
    public static final int VARIANT_ELEGANT = 2;
    
    private final FontConfig.Font[] mFonts;
    
    private final String mLanguages;
    
    private final String mName;
    
    private final int mVariant;
    
    public Family(String param1String1, FontConfig.Font[] param1ArrayOfFont, String param1String2, int param1Int) {
      this.mName = param1String1;
      this.mFonts = param1ArrayOfFont;
      this.mLanguages = param1String2;
      this.mVariant = param1Int;
    }
    
    public String getName() {
      return this.mName;
    }
    
    public FontConfig.Font[] getFonts() {
      return this.mFonts;
    }
    
    public String getLanguages() {
      return this.mLanguages;
    }
    
    public int getVariant() {
      return this.mVariant;
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public static @interface Variant {}
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Variant {}
}
