package android.text;

import com.android.internal.util.ArrayUtils;
import com.android.internal.util.GrowingArrayUtils;
import java.lang.reflect.Array;
import libcore.util.EmptyArray;

abstract class SpannableStringInternal {
  private static final int COLUMNS = 3;
  
  SpannableStringInternal(CharSequence paramCharSequence, int paramInt1, int paramInt2, boolean paramBoolean) {
    if (paramInt1 == 0 && paramInt2 == paramCharSequence.length()) {
      this.mText = paramCharSequence.toString();
    } else {
      this.mText = paramCharSequence.toString().substring(paramInt1, paramInt2);
    } 
    this.mSpans = EmptyArray.OBJECT;
    this.mSpanData = EmptyArray.INT;
    if (paramCharSequence instanceof Spanned)
      if (paramCharSequence instanceof SpannableStringInternal) {
        copySpansFromInternal((SpannableStringInternal)paramCharSequence, paramInt1, paramInt2, paramBoolean);
      } else {
        copySpansFromSpanned((Spanned)paramCharSequence, paramInt1, paramInt2, paramBoolean);
      }  
  }
  
  SpannableStringInternal(CharSequence paramCharSequence, int paramInt1, int paramInt2) {
    this(paramCharSequence, paramInt1, paramInt2, false);
  }
  
  private void copySpansFromSpanned(Spanned paramSpanned, int paramInt1, int paramInt2, boolean paramBoolean) {
    Object[] arrayOfObject = paramSpanned.getSpans(paramInt1, paramInt2, Object.class);
    for (byte b = 0; b < arrayOfObject.length; b++) {
      if (!paramBoolean || !(arrayOfObject[b] instanceof NoCopySpan)) {
        int i = paramSpanned.getSpanStart(arrayOfObject[b]);
        int j = paramSpanned.getSpanEnd(arrayOfObject[b]);
        int k = paramSpanned.getSpanFlags(arrayOfObject[b]);
        int m = i;
        if (i < paramInt1)
          m = paramInt1; 
        i = j;
        if (j > paramInt2)
          i = paramInt2; 
        setSpan(arrayOfObject[b], m - paramInt1, i - paramInt1, k, false);
      } 
    } 
  }
  
  private void copySpansFromInternal(SpannableStringInternal paramSpannableStringInternal, int paramInt1, int paramInt2, boolean paramBoolean) {
    Object[] arrayOfObject1;
    int arrayOfInt2[], i = 0;
    int[] arrayOfInt1 = paramSpannableStringInternal.mSpanData;
    Object[] arrayOfObject2 = paramSpannableStringInternal.mSpans;
    int j = paramSpannableStringInternal.mSpanCount;
    int k = 0;
    byte b;
    for (b = 0; b < j; b++) {
      int m = arrayOfInt1[b * 3 + 0];
      int n = arrayOfInt1[b * 3 + 1];
      if (isOutOfCopyRange(paramInt1, paramInt2, m, n))
        continue; 
      if (arrayOfObject2[b] instanceof NoCopySpan) {
        m = 1;
        k = 1;
        if (paramBoolean) {
          k = m;
          continue;
        } 
      } 
      i++;
      continue;
    } 
    if (i == 0)
      return; 
    if (k == 0 && paramInt1 == 0 && paramInt2 == paramSpannableStringInternal.length()) {
      this.mSpans = arrayOfObject2 = ArrayUtils.newUnpaddedObjectArray(paramSpannableStringInternal.mSpans.length);
      this.mSpanData = new int[paramSpannableStringInternal.mSpanData.length];
      this.mSpanCount = paramSpannableStringInternal.mSpanCount;
      arrayOfObject1 = paramSpannableStringInternal.mSpans;
      System.arraycopy(arrayOfObject1, 0, arrayOfObject2, 0, arrayOfObject1.length);
      arrayOfInt2 = paramSpannableStringInternal.mSpanData;
      int[] arrayOfInt = this.mSpanData;
      System.arraycopy(arrayOfInt2, 0, arrayOfInt, 0, arrayOfInt.length);
    } else {
      this.mSpanCount = i;
      Object[] arrayOfObject = ArrayUtils.newUnpaddedObjectArray(i);
      this.mSpanData = new int[arrayOfObject.length * 3];
      for (b = 0, k = 0; b < j; b++, k = m) {
        int m;
        Object object1 = arrayOfObject1[b * 3 + 0];
        Object object2 = arrayOfObject1[b * 3 + 1];
        i = k;
        if (!isOutOfCopyRange(paramInt1, paramInt2, object1, object2))
          if (paramBoolean && arrayOfInt2[b] instanceof NoCopySpan) {
            i = k;
          } else {
            int n;
            Object object = object1;
            if (object1 < paramInt1)
              m = paramInt1; 
            object1 = object2;
            if (object2 > paramInt2)
              n = paramInt2; 
            this.mSpans[k] = arrayOfInt2[b];
            int[] arrayOfInt = this.mSpanData;
            arrayOfInt[k * 3 + 0] = m - paramInt1;
            arrayOfInt[k * 3 + 1] = n - paramInt1;
            arrayOfInt[k * 3 + 2] = arrayOfObject1[b * 3 + 2];
            m = k + 1;
          }  
      } 
    } 
  }
  
  private final boolean isOutOfCopyRange(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (paramInt3 > paramInt2 || paramInt4 < paramInt1)
      return true; 
    if (paramInt3 != paramInt4 && paramInt1 != paramInt2 && (
      paramInt3 == paramInt2 || paramInt4 == paramInt1))
      return true; 
    return false;
  }
  
  public final int length() {
    return this.mText.length();
  }
  
  public final char charAt(int paramInt) {
    return this.mText.charAt(paramInt);
  }
  
  public final String toString() {
    return this.mText;
  }
  
  public final void getChars(int paramInt1, int paramInt2, char[] paramArrayOfchar, int paramInt3) {
    this.mText.getChars(paramInt1, paramInt2, paramArrayOfchar, paramInt3);
  }
  
  void setSpan(Object paramObject, int paramInt1, int paramInt2, int paramInt3) {
    setSpan(paramObject, paramInt1, paramInt2, paramInt3, true);
  }
  
  private boolean isIndexFollowsNextLine(int paramInt) {
    boolean bool;
    if (paramInt != 0 && paramInt != length() && charAt(paramInt - 1) != '\n') {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void setSpan(Object paramObject, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean) {
    checkRange("setSpan", paramInt1, paramInt2);
    if ((paramInt3 & 0x33) == 51) {
      if (isIndexFollowsNextLine(paramInt1)) {
        if (!paramBoolean)
          return; 
        paramObject = new StringBuilder();
        paramObject.append("PARAGRAPH span must start at paragraph boundary (");
        paramObject.append(paramInt1);
        paramObject.append(" follows ");
        paramObject.append(charAt(paramInt1 - 1));
        paramObject.append(")");
        throw new RuntimeException(paramObject.toString());
      } 
      if (isIndexFollowsNextLine(paramInt2)) {
        if (!paramBoolean)
          return; 
        paramObject = new StringBuilder();
        paramObject.append("PARAGRAPH span must end at paragraph boundary (");
        paramObject.append(paramInt2);
        paramObject.append(" follows ");
        paramObject.append(charAt(paramInt2 - 1));
        paramObject.append(")");
        throw new RuntimeException(paramObject.toString());
      } 
    } 
    int i = this.mSpanCount;
    Object[] arrayOfObject1 = this.mSpans;
    int[] arrayOfInt2 = this.mSpanData;
    int j;
    for (j = 0; j < i; j++) {
      if (arrayOfObject1[j] == paramObject) {
        int k = arrayOfInt2[j * 3 + 0];
        i = arrayOfInt2[j * 3 + 1];
        arrayOfInt2[j * 3 + 0] = paramInt1;
        arrayOfInt2[j * 3 + 1] = paramInt2;
        arrayOfInt2[j * 3 + 2] = paramInt3;
        sendSpanChanged(paramObject, k, i, paramInt1, paramInt2);
        return;
      } 
    } 
    j = this.mSpanCount;
    if (j + 1 >= this.mSpans.length) {
      j = GrowingArrayUtils.growSize(j);
      Object[] arrayOfObject = ArrayUtils.newUnpaddedObjectArray(j);
      int[] arrayOfInt = new int[arrayOfObject.length * 3];
      System.arraycopy(this.mSpans, 0, arrayOfObject, 0, this.mSpanCount);
      System.arraycopy(this.mSpanData, 0, arrayOfInt, 0, this.mSpanCount * 3);
      this.mSpans = arrayOfObject;
      this.mSpanData = arrayOfInt;
    } 
    Object[] arrayOfObject2 = this.mSpans;
    j = this.mSpanCount;
    arrayOfObject2[j] = paramObject;
    int[] arrayOfInt1 = this.mSpanData;
    arrayOfInt1[j * 3 + 0] = paramInt1;
    arrayOfInt1[j * 3 + 1] = paramInt2;
    arrayOfInt1[j * 3 + 2] = paramInt3;
    this.mSpanCount = j + 1;
    if (this instanceof Spannable)
      sendSpanAdded(paramObject, paramInt1, paramInt2); 
  }
  
  void removeSpan(Object paramObject) {
    removeSpan(paramObject, 0);
  }
  
  public void removeSpan(Object paramObject, int paramInt) {
    int i = this.mSpanCount;
    Object[] arrayOfObject = this.mSpans;
    int[] arrayOfInt = this.mSpanData;
    for (int j = i - 1; j >= 0; j--) {
      if (arrayOfObject[j] == paramObject) {
        int k = arrayOfInt[j * 3 + 0];
        int m = arrayOfInt[j * 3 + 1];
        i -= j + 1;
        System.arraycopy(arrayOfObject, j + 1, arrayOfObject, j, i);
        System.arraycopy(arrayOfInt, (j + 1) * 3, arrayOfInt, j * 3, i * 3);
        this.mSpanCount--;
        if ((paramInt & 0x200) == 0)
          sendSpanRemoved(paramObject, k, m); 
        return;
      } 
    } 
  }
  
  public int getSpanStart(Object paramObject) {
    int i = this.mSpanCount;
    Object[] arrayOfObject = this.mSpans;
    int[] arrayOfInt = this.mSpanData;
    for (; --i >= 0; i--) {
      if (arrayOfObject[i] == paramObject)
        return arrayOfInt[i * 3 + 0]; 
    } 
    return -1;
  }
  
  public int getSpanEnd(Object paramObject) {
    int i = this.mSpanCount;
    Object[] arrayOfObject = this.mSpans;
    int[] arrayOfInt = this.mSpanData;
    for (; --i >= 0; i--) {
      if (arrayOfObject[i] == paramObject)
        return arrayOfInt[i * 3 + 1]; 
    } 
    return -1;
  }
  
  public int getSpanFlags(Object paramObject) {
    int i = this.mSpanCount;
    Object[] arrayOfObject = this.mSpans;
    int[] arrayOfInt = this.mSpanData;
    for (; --i >= 0; i--) {
      if (arrayOfObject[i] == paramObject)
        return arrayOfInt[i * 3 + 2]; 
    } 
    return 0;
  }
  
  public <T> T[] getSpans(int paramInt1, int paramInt2, Class<T> paramClass) {
    byte b1 = 0;
    int i = this.mSpanCount;
    Object[] arrayOfObject2 = this.mSpans;
    int[] arrayOfInt = this.mSpanData;
    Object[] arrayOfObject3 = null;
    Object object = null;
    for (byte b2 = 0; b2 < i; b2++) {
      int j = arrayOfInt[b2 * 3 + 0];
      int k = arrayOfInt[b2 * 3 + 1];
      if (j > paramInt2)
        continue; 
      if (k < paramInt1)
        continue; 
      if (j != k && paramInt1 != paramInt2) {
        if (j == paramInt2)
          continue; 
        if (k == paramInt1)
          continue; 
      } 
      if (paramClass == null || paramClass == Object.class || paramClass.isInstance(arrayOfObject2[b2]))
        if (!b1) {
          object = arrayOfObject2[b2];
          b1++;
        } else {
          if (b1 == 1) {
            arrayOfObject3 = (Object[])Array.newInstance(paramClass, i - b2 + 1);
            arrayOfObject3[0] = object;
          } 
          j = arrayOfInt[b2 * 3 + 2] & 0xFF0000;
          if (j != 0) {
            for (k = 0; k < b1; k++) {
              int m = getSpanFlags(arrayOfObject3[k]);
              if (j > (m & 0xFF0000))
                break; 
            } 
            System.arraycopy(arrayOfObject3, k, arrayOfObject3, k + 1, b1 - k);
            arrayOfObject3[k] = arrayOfObject2[b2];
            b1++;
          } else {
            arrayOfObject3[b1] = arrayOfObject2[b2];
            b1++;
          } 
        }  
      continue;
    } 
    if (b1 == 0)
      return (T[])ArrayUtils.emptyArray(paramClass); 
    if (b1 == 1) {
      arrayOfObject1 = (Object[])Array.newInstance(paramClass, 1);
      arrayOfObject1[0] = object;
      return (T[])arrayOfObject1;
    } 
    if (b1 == arrayOfObject3.length)
      return (T[])arrayOfObject3; 
    Object[] arrayOfObject1 = (Object[])Array.newInstance((Class<?>)arrayOfObject1, b1);
    System.arraycopy(arrayOfObject3, 0, arrayOfObject1, 0, b1);
    return (T[])arrayOfObject1;
  }
  
  public int nextSpanTransition(int paramInt1, int paramInt2, Class<Object> paramClass) {
    int i = this.mSpanCount;
    Object[] arrayOfObject = this.mSpans;
    int[] arrayOfInt = this.mSpanData;
    Class<Object> clazz = paramClass;
    if (paramClass == null)
      clazz = Object.class; 
    for (byte b = 0; b < i; b++) {
      int j = arrayOfInt[b * 3 + 0];
      int k = arrayOfInt[b * 3 + 1];
      int m = paramInt2;
      if (j > paramInt1) {
        m = paramInt2;
        if (j < paramInt2) {
          m = paramInt2;
          if (clazz.isInstance(arrayOfObject[b]))
            m = j; 
        } 
      } 
      paramInt2 = m;
      if (k > paramInt1) {
        paramInt2 = m;
        if (k < m) {
          paramInt2 = m;
          if (clazz.isInstance(arrayOfObject[b]))
            paramInt2 = k; 
        } 
      } 
    } 
    return paramInt2;
  }
  
  private void sendSpanAdded(Object paramObject, int paramInt1, int paramInt2) {
    SpanWatcher[] arrayOfSpanWatcher = getSpans(paramInt1, paramInt2, SpanWatcher.class);
    int i = arrayOfSpanWatcher.length;
    for (byte b = 0; b < i; b++)
      arrayOfSpanWatcher[b].onSpanAdded((Spannable)this, paramObject, paramInt1, paramInt2); 
  }
  
  private void sendSpanRemoved(Object paramObject, int paramInt1, int paramInt2) {
    SpanWatcher[] arrayOfSpanWatcher = getSpans(paramInt1, paramInt2, SpanWatcher.class);
    int i = arrayOfSpanWatcher.length;
    for (byte b = 0; b < i; b++)
      arrayOfSpanWatcher[b].onSpanRemoved((Spannable)this, paramObject, paramInt1, paramInt2); 
  }
  
  private void sendSpanChanged(Object paramObject, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    SpanWatcher[] arrayOfSpanWatcher = getSpans(Math.min(paramInt1, paramInt3), Math.max(paramInt2, paramInt4), SpanWatcher.class);
    int i = arrayOfSpanWatcher.length;
    for (byte b = 0; b < i; b++)
      arrayOfSpanWatcher[b].onSpanChanged((Spannable)this, paramObject, paramInt1, paramInt2, paramInt3, paramInt4); 
  }
  
  private static String region(int paramInt1, int paramInt2) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("(");
    stringBuilder.append(paramInt1);
    stringBuilder.append(" ... ");
    stringBuilder.append(paramInt2);
    stringBuilder.append(")");
    return stringBuilder.toString();
  }
  
  private void checkRange(String paramString, int paramInt1, int paramInt2) {
    if (paramInt2 >= paramInt1) {
      int i = length();
      if (paramInt1 <= i && paramInt2 <= i) {
        if (paramInt1 >= 0 && paramInt2 >= 0)
          return; 
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append(paramString);
        stringBuilder2.append(" ");
        stringBuilder2.append(region(paramInt1, paramInt2));
        stringBuilder2.append(" starts before 0");
        throw new IndexOutOfBoundsException(stringBuilder2.toString());
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramString);
      stringBuilder1.append(" ");
      stringBuilder1.append(region(paramInt1, paramInt2));
      stringBuilder1.append(" ends beyond length ");
      stringBuilder1.append(i);
      throw new IndexOutOfBoundsException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append(" ");
    stringBuilder.append(region(paramInt1, paramInt2));
    stringBuilder.append(" has end before start");
    throw new IndexOutOfBoundsException(stringBuilder.toString());
  }
  
  public boolean equals(Object paramObject) {
    if (paramObject instanceof Spanned && 
      toString().equals(paramObject.toString())) {
      Spanned spanned = (Spanned)paramObject;
      Object[] arrayOfObject1 = spanned.getSpans(0, spanned.length(), Object.class);
      Object[] arrayOfObject2 = getSpans(0, length(), Object.class);
      if (this.mSpanCount == arrayOfObject1.length) {
        for (byte b = 0; b < this.mSpanCount; b++) {
          Object object = arrayOfObject2[b];
          paramObject = arrayOfObject1[b];
          if (object == this) {
            if (spanned != paramObject || 
              getSpanStart(object) != spanned.getSpanStart(paramObject) || 
              getSpanEnd(object) != spanned.getSpanEnd(paramObject) || 
              getSpanFlags(object) != spanned.getSpanFlags(paramObject))
              return false; 
          } else if (!object.equals(paramObject) || 
            getSpanStart(object) != spanned.getSpanStart(paramObject) || 
            getSpanEnd(object) != spanned.getSpanEnd(paramObject) || 
            getSpanFlags(object) != spanned.getSpanFlags(paramObject)) {
            return false;
          } 
        } 
        return true;
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    int i = toString().hashCode();
    i = i * 31 + this.mSpanCount;
    for (byte b = 0; b < this.mSpanCount; b++) {
      Object object = this.mSpans[b];
      int j = i;
      if (object != this)
        j = i * 31 + object.hashCode(); 
      int k = getSpanStart(object);
      i = getSpanEnd(object);
      i = ((j * 31 + k) * 31 + i) * 31 + getSpanFlags(object);
    } 
    return i;
  }
  
  private void copySpans(Spanned paramSpanned, int paramInt1, int paramInt2) {
    copySpansFromSpanned(paramSpanned, paramInt1, paramInt2, false);
  }
  
  private void copySpans(SpannableStringInternal paramSpannableStringInternal, int paramInt1, int paramInt2) {
    copySpansFromInternal(paramSpannableStringInternal, paramInt1, paramInt2, false);
  }
  
  static final Object[] EMPTY = new Object[0];
  
  private static final int END = 1;
  
  private static final int FLAGS = 2;
  
  private static final int START = 0;
  
  private int mSpanCount;
  
  private int[] mSpanData;
  
  private Object[] mSpans;
  
  private String mText;
}
