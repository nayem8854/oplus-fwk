package android.text;

public interface NoCopySpan {
  class Concrete implements NoCopySpan {}
}
