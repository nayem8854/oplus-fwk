package android.text;

import android.icu.lang.UCharacter;

public class Emoji {
  public static int CANCEL_TAG;
  
  public static int COMBINING_ENCLOSING_KEYCAP = 8419;
  
  public static int VARIATION_SELECTOR_16;
  
  public static int ZERO_WIDTH_JOINER = 8205;
  
  static {
    VARIATION_SELECTOR_16 = 65039;
    CANCEL_TAG = 917631;
  }
  
  public static boolean isRegionalIndicatorSymbol(int paramInt) {
    boolean bool;
    if (127462 <= paramInt && paramInt <= 127487) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isEmojiModifier(int paramInt) {
    return UCharacter.hasBinaryProperty(paramInt, 59);
  }
  
  public static boolean isEmojiModifierBase(int paramInt) {
    if (paramInt == 129309 || paramInt == 129340)
      return true; 
    return UCharacter.hasBinaryProperty(paramInt, 60);
  }
  
  public static boolean isEmoji(int paramInt) {
    return UCharacter.hasBinaryProperty(paramInt, 57);
  }
  
  public static boolean isKeycapBase(int paramInt) {
    return ((48 <= paramInt && paramInt <= 57) || paramInt == 35 || paramInt == 42);
  }
  
  public static boolean isTagSpecChar(int paramInt) {
    boolean bool;
    if (917536 <= paramInt && paramInt <= 917630) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
}
