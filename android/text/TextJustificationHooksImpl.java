package android.text;

import android.content.Context;
import com.oplus.util.OplusContextUtil;

public class TextJustificationHooksImpl implements ITextJustificationHooks {
  public float mTextViewParaSpacing = 0.0F;
  
  public void setTextViewParaSpacing(Object paramObject, float paramFloat, Layout paramLayout) {
    if (paramObject == null || !(paramObject instanceof android.widget.TextView))
      return; 
    paramObject = paramObject;
    paramObject.hashCode();
    float f = this.mTextViewParaSpacing;
    if (paramFloat != f) {
      this.mTextViewParaSpacing = paramFloat;
      if (paramLayout != null) {
        paramObject.nullLayouts();
        paramObject.requestLayout();
        paramObject.invalidate();
      } 
    } 
  }
  
  public float getTextViewParaSpacing(Object paramObject) {
    return this.mTextViewParaSpacing;
  }
  
  public float getTextViewDefaultLineMulti(Object paramObject, float paramFloat1, float paramFloat2) {
    if (paramObject == null || !(paramObject instanceof android.widget.TextView))
      return paramFloat2; 
    paramObject = paramObject;
    paramObject = paramObject.getContext();
    if (!OplusContextUtil.isOplusOSStyle((Context)paramObject))
      return paramFloat2; 
    int i = px2sp((Context)paramObject, paramFloat1);
    paramFloat1 = paramFloat2;
    if (i != 10) {
      if (i != 12) {
        if (i != 14) {
          if (i != 16) {
            if (i != 18) {
              if (i != 20) {
                if (i != 22) {
                  if (i == 24)
                    paramFloat1 = 1.1F; 
                } else {
                  paramFloat1 = 1.1F;
                } 
              } else {
                paramFloat1 = 1.1F;
              } 
            } else {
              paramFloat1 = 1.1F;
            } 
          } else {
            paramFloat1 = 1.1F;
          } 
        } else {
          paramFloat1 = 1.2F;
        } 
      } else {
        paramFloat1 = 1.15F;
      } 
    } else {
      paramFloat1 = 1.3F;
    } 
    return paramFloat1;
  }
  
  private int px2sp(Context paramContext, float paramFloat) {
    float f = (paramContext.getResources().getDisplayMetrics()).scaledDensity;
    return (int)(paramFloat / f + 0.5F);
  }
  
  public float calculateAddedWidth(float paramFloat1, float paramFloat2, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean, char[] paramArrayOfchar, CharSequence paramCharSequence, int paramInt4) {
    paramInt2 = countStretchableHan(0, paramInt3, paramBoolean, paramArrayOfchar, paramCharSequence, paramInt4);
    if (paramInt2 != 0) {
      paramFloat1 = (paramFloat1 - paramFloat2) / (paramInt2 + paramInt1);
      return paramFloat1;
    } 
    return 0.0F;
  }
  
  private int countStretchableHan(int paramInt1, int paramInt2, boolean paramBoolean, char[] paramArrayOfchar, CharSequence paramCharSequence, int paramInt3) {
    int i = 0;
    for (; paramInt1 < paramInt2; paramInt1++, i = j) {
      char c;
      if (paramBoolean) {
        c = paramArrayOfchar[paramInt1];
      } else {
        c = paramCharSequence.charAt(paramInt1 + paramInt3);
      } 
      int j = i;
      if (isStretchableHan(c))
        j = i + 1; 
    } 
    return i;
  }
  
  private boolean isStretchableHan(int paramInt) {
    boolean bool;
    if (paramInt >= 19968 && paramInt <= 40869) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean mLayoutSpecifiedParaSpacing = false;
  
  public float mBuilderParaSpacingAdded = 0.0F;
  
  private static final String TAG = "TextJustificationHooksImpl";
  
  public float getLayoutParaSpacingAdded(StaticLayout paramStaticLayout, Object paramObject, boolean paramBoolean, CharSequence paramCharSequence, int paramInt) {
    paramStaticLayout.hashCode();
    this.mLayoutSpecifiedParaSpacing = false;
    float f1 = 0.0F;
    float f2 = f1;
    if (paramBoolean) {
      f2 = f1;
      if (paramCharSequence.charAt(paramInt - 1) == '\n') {
        f1 = this.mBuilderParaSpacingAdded;
        f2 = f1;
        if (f1 > 0.0F) {
          this.mLayoutSpecifiedParaSpacing = true;
          f2 = f1;
        } 
      } 
    } 
    return f2;
  }
  
  public void setLayoutParaSpacingAdded(Object paramObject, float paramFloat) {
    this.mBuilderParaSpacingAdded = paramFloat;
  }
  
  public boolean lineShouldIncludeFontPad(boolean paramBoolean, StaticLayout paramStaticLayout) {
    return (paramBoolean || this.mLayoutSpecifiedParaSpacing);
  }
  
  public boolean lineNeedMultiply(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, StaticLayout paramStaticLayout) {
    if (paramBoolean1 && (paramBoolean2 || !paramBoolean3) && !this.mLayoutSpecifiedParaSpacing) {
      paramBoolean1 = true;
    } else {
      paramBoolean1 = false;
    } 
    return paramBoolean1;
  }
}
