package android.text;

import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.text.MeasuredText;
import android.text.style.MetricAffectingSpan;
import android.text.style.ReplacementSpan;
import android.util.Pools;
import java.util.Arrays;

public class MeasuredParagraph {
  private static final Pools.SynchronizedPool<MeasuredParagraph> sPool = new Pools.SynchronizedPool<>(1);
  
  private static MeasuredParagraph obtain() {
    MeasuredParagraph measuredParagraph = sPool.acquire();
    if (measuredParagraph == null)
      measuredParagraph = new MeasuredParagraph(); 
    return measuredParagraph;
  }
  
  public void recycle() {
    release();
    sPool.release(this);
  }
  
  private AutoGrowArray.ByteArray mLevels = new AutoGrowArray.ByteArray();
  
  private AutoGrowArray.FloatArray mWidths = new AutoGrowArray.FloatArray();
  
  private AutoGrowArray.IntArray mSpanEndCache = new AutoGrowArray.IntArray(4);
  
  private AutoGrowArray.IntArray mFontMetrics = new AutoGrowArray.IntArray(16);
  
  private TextPaint mCachedPaint = new TextPaint();
  
  private static final char OBJECT_REPLACEMENT_CHARACTER = '￼';
  
  private Paint.FontMetricsInt mCachedFm;
  
  private char[] mCopiedBuffer;
  
  private boolean mLtrWithoutBidi;
  
  private MeasuredText mMeasuredText;
  
  private int mParaDir;
  
  private Spanned mSpanned;
  
  private int mTextLength;
  
  private int mTextStart;
  
  private float mWholeWidth;
  
  public void release() {
    reset();
    this.mLevels.clearWithReleasingLargeArray();
    this.mWidths.clearWithReleasingLargeArray();
    this.mFontMetrics.clearWithReleasingLargeArray();
    this.mSpanEndCache.clearWithReleasingLargeArray();
  }
  
  private void reset() {
    this.mSpanned = null;
    this.mCopiedBuffer = null;
    this.mWholeWidth = 0.0F;
    this.mLevels.clear();
    this.mWidths.clear();
    this.mFontMetrics.clear();
    this.mSpanEndCache.clear();
    this.mMeasuredText = null;
  }
  
  public int getTextLength() {
    return this.mTextLength;
  }
  
  public char[] getChars() {
    return this.mCopiedBuffer;
  }
  
  public int getParagraphDir() {
    return this.mParaDir;
  }
  
  public Layout.Directions getDirections(int paramInt1, int paramInt2) {
    if (this.mLtrWithoutBidi)
      return Layout.DIRS_ALL_LEFT_TO_RIGHT; 
    return AndroidBidi.directions(this.mParaDir, this.mLevels.getRawArray(), paramInt1, this.mCopiedBuffer, paramInt1, paramInt2 - paramInt1);
  }
  
  public float getWholeWidth() {
    return this.mWholeWidth;
  }
  
  public AutoGrowArray.FloatArray getWidths() {
    return this.mWidths;
  }
  
  public AutoGrowArray.IntArray getSpanEndCache() {
    return this.mSpanEndCache;
  }
  
  public AutoGrowArray.IntArray getFontMetrics() {
    return this.mFontMetrics;
  }
  
  public MeasuredText getMeasuredText() {
    return this.mMeasuredText;
  }
  
  public float getWidth(int paramInt1, int paramInt2) {
    float[] arrayOfFloat;
    MeasuredText measuredText = this.mMeasuredText;
    if (measuredText == null) {
      arrayOfFloat = this.mWidths.getRawArray();
      float f = 0.0F;
      for (; paramInt1 < paramInt2; paramInt1++)
        f += arrayOfFloat[paramInt1]; 
      return f;
    } 
    return arrayOfFloat.getWidth(paramInt1, paramInt2);
  }
  
  public void getBounds(int paramInt1, int paramInt2, Rect paramRect) {
    this.mMeasuredText.getBounds(paramInt1, paramInt2, paramRect);
  }
  
  public float getCharWidthAt(int paramInt) {
    return this.mMeasuredText.getCharWidthAt(paramInt);
  }
  
  public static MeasuredParagraph buildForBidi(CharSequence paramCharSequence, int paramInt1, int paramInt2, TextDirectionHeuristic paramTextDirectionHeuristic, MeasuredParagraph paramMeasuredParagraph) {
    if (paramMeasuredParagraph == null)
      paramMeasuredParagraph = obtain(); 
    paramMeasuredParagraph.resetAndAnalyzeBidi(paramCharSequence, paramInt1, paramInt2, paramTextDirectionHeuristic);
    return paramMeasuredParagraph;
  }
  
  public static MeasuredParagraph buildForMeasurement(TextPaint paramTextPaint, CharSequence paramCharSequence, int paramInt1, int paramInt2, TextDirectionHeuristic paramTextDirectionHeuristic, MeasuredParagraph paramMeasuredParagraph) {
    if (paramMeasuredParagraph == null)
      paramMeasuredParagraph = obtain(); 
    paramMeasuredParagraph.resetAndAnalyzeBidi(paramCharSequence, paramInt1, paramInt2, paramTextDirectionHeuristic);
    paramMeasuredParagraph.mWidths.resize(paramMeasuredParagraph.mTextLength);
    if (paramMeasuredParagraph.mTextLength == 0)
      return paramMeasuredParagraph; 
    if (paramMeasuredParagraph.mSpanned == null) {
      paramMeasuredParagraph.applyMetricsAffectingSpan(paramTextPaint, null, paramInt1, paramInt2, null);
    } else {
      for (; paramInt1 < paramInt2; paramInt1 = i) {
        int i = paramMeasuredParagraph.mSpanned.nextSpanTransition(paramInt1, paramInt2, MetricAffectingSpan.class);
        MetricAffectingSpan[] arrayOfMetricAffectingSpan = paramMeasuredParagraph.mSpanned.<MetricAffectingSpan>getSpans(paramInt1, i, MetricAffectingSpan.class);
        arrayOfMetricAffectingSpan = TextUtils.<MetricAffectingSpan>removeEmptySpans(arrayOfMetricAffectingSpan, paramMeasuredParagraph.mSpanned, MetricAffectingSpan.class);
        paramMeasuredParagraph.applyMetricsAffectingSpan(paramTextPaint, arrayOfMetricAffectingSpan, paramInt1, i, null);
      } 
    } 
    return paramMeasuredParagraph;
  }
  
  public static MeasuredParagraph buildForStaticLayout(TextPaint paramTextPaint, CharSequence paramCharSequence, int paramInt1, int paramInt2, TextDirectionHeuristic paramTextDirectionHeuristic, boolean paramBoolean1, boolean paramBoolean2, MeasuredParagraph paramMeasuredParagraph1, MeasuredParagraph paramMeasuredParagraph2) {
    MeasuredText.Builder builder;
    if (paramMeasuredParagraph2 == null)
      paramMeasuredParagraph2 = obtain(); 
    paramMeasuredParagraph2.resetAndAnalyzeBidi(paramCharSequence, paramInt1, paramInt2, paramTextDirectionHeuristic);
    if (paramMeasuredParagraph1 == null) {
      builder = new MeasuredText.Builder(paramMeasuredParagraph2.mCopiedBuffer);
      builder = builder.setComputeHyphenation(paramBoolean1);
      builder = builder.setComputeLayout(paramBoolean2);
    } else {
      builder = new MeasuredText.Builder(paramMeasuredParagraph1.mMeasuredText);
    } 
    if (paramMeasuredParagraph2.mTextLength == 0) {
      paramMeasuredParagraph2.mMeasuredText = builder.build();
    } else {
      if (paramMeasuredParagraph2.mSpanned == null) {
        paramMeasuredParagraph2.applyMetricsAffectingSpan(paramTextPaint, null, paramInt1, paramInt2, builder);
        paramMeasuredParagraph2.mSpanEndCache.append(paramInt2);
      } else {
        for (; paramInt1 < paramInt2; paramInt1 = i) {
          int i = paramMeasuredParagraph2.mSpanned.nextSpanTransition(paramInt1, paramInt2, MetricAffectingSpan.class);
          MetricAffectingSpan[] arrayOfMetricAffectingSpan = paramMeasuredParagraph2.mSpanned.<MetricAffectingSpan>getSpans(paramInt1, i, MetricAffectingSpan.class);
          arrayOfMetricAffectingSpan = TextUtils.<MetricAffectingSpan>removeEmptySpans(arrayOfMetricAffectingSpan, paramMeasuredParagraph2.mSpanned, MetricAffectingSpan.class);
          paramMeasuredParagraph2.applyMetricsAffectingSpan(paramTextPaint, arrayOfMetricAffectingSpan, paramInt1, i, builder);
          paramMeasuredParagraph2.mSpanEndCache.append(i);
        } 
      } 
      paramMeasuredParagraph2.mMeasuredText = builder.build();
    } 
    return paramMeasuredParagraph2;
  }
  
  private void resetAndAnalyzeBidi(CharSequence paramCharSequence, int paramInt1, int paramInt2, TextDirectionHeuristic paramTextDirectionHeuristic) {
    Spanned spanned;
    reset();
    if (paramCharSequence instanceof Spanned) {
      spanned = (Spanned)paramCharSequence;
    } else {
      spanned = null;
    } 
    this.mSpanned = spanned;
    this.mTextStart = paramInt1;
    int i = paramInt2 - paramInt1;
    char[] arrayOfChar = this.mCopiedBuffer;
    if (arrayOfChar == null || arrayOfChar.length != i)
      this.mCopiedBuffer = new char[this.mTextLength]; 
    TextUtils.getChars(paramCharSequence, paramInt1, paramInt2, this.mCopiedBuffer, 0);
    paramCharSequence = this.mSpanned;
    if (paramCharSequence != null) {
      ReplacementSpan[] arrayOfReplacementSpan = paramCharSequence.<ReplacementSpan>getSpans(paramInt1, paramInt2, ReplacementSpan.class);
      for (paramInt2 = 0; paramInt2 < arrayOfReplacementSpan.length; paramInt2++) {
        int j = this.mSpanned.getSpanStart(arrayOfReplacementSpan[paramInt2]) - paramInt1;
        int k = this.mSpanned.getSpanEnd(arrayOfReplacementSpan[paramInt2]) - paramInt1;
        i = j;
        if (j < 0)
          i = 0; 
        j = k;
        if (k > this.mTextLength)
          j = this.mTextLength; 
        Arrays.fill(this.mCopiedBuffer, i, j, '￼');
      } 
    } 
    TextDirectionHeuristic textDirectionHeuristic = TextDirectionHeuristics.LTR;
    paramInt1 = 1;
    if (paramTextDirectionHeuristic == textDirectionHeuristic || paramTextDirectionHeuristic == TextDirectionHeuristics.FIRSTSTRONG_LTR || paramTextDirectionHeuristic == TextDirectionHeuristics.ANYRTL_LTR) {
      char[] arrayOfChar1 = this.mCopiedBuffer;
      paramInt2 = this.mTextLength;
      if (TextUtils.doesNotNeedBidi(arrayOfChar1, 0, paramInt2)) {
        this.mLevels.clear();
        this.mParaDir = 1;
        this.mLtrWithoutBidi = true;
        return;
      } 
    } 
    if (paramTextDirectionHeuristic == TextDirectionHeuristics.LTR) {
      paramInt1 = 1;
    } else if (paramTextDirectionHeuristic == TextDirectionHeuristics.RTL) {
      paramInt1 = -1;
    } else if (paramTextDirectionHeuristic == TextDirectionHeuristics.FIRSTSTRONG_LTR) {
      paramInt1 = 2;
    } else if (paramTextDirectionHeuristic == TextDirectionHeuristics.FIRSTSTRONG_RTL) {
      paramInt1 = -2;
    } else {
      boolean bool = paramTextDirectionHeuristic.isRtl(this.mCopiedBuffer, 0, this.mTextLength);
      if (bool)
        paramInt1 = -1; 
    } 
    this.mLevels.resize(this.mTextLength);
    this.mParaDir = AndroidBidi.bidi(paramInt1, this.mCopiedBuffer, this.mLevels.getRawArray());
    this.mLtrWithoutBidi = false;
  }
  
  private void applyReplacementRun(ReplacementSpan paramReplacementSpan, int paramInt1, int paramInt2, MeasuredText.Builder paramBuilder) {
    TextPaint textPaint = this.mCachedPaint;
    Spanned spanned = this.mSpanned;
    int i = this.mTextStart;
    float f = paramReplacementSpan.getSize(textPaint, spanned, paramInt1 + i, paramInt2 + i, this.mCachedFm);
    if (paramBuilder == null) {
      this.mWidths.set(paramInt1, f);
      if (paramInt2 > paramInt1 + 1)
        Arrays.fill(this.mWidths.getRawArray(), paramInt1 + 1, paramInt2, 0.0F); 
      this.mWholeWidth += f;
    } else {
      paramBuilder.appendReplacementRun(this.mCachedPaint, paramInt2 - paramInt1, f);
    } 
  }
  
  private void applyStyleRun(int paramInt1, int paramInt2, MeasuredText.Builder paramBuilder) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mLtrWithoutBidi : Z
    //   4: ifeq -> 85
    //   7: aload_3
    //   8: ifnonnull -> 69
    //   11: aload_0
    //   12: getfield mWholeWidth : F
    //   15: fstore #4
    //   17: aload_0
    //   18: getfield mCachedPaint : Landroid/text/TextPaint;
    //   21: astore_3
    //   22: aload_0
    //   23: getfield mCopiedBuffer : [C
    //   26: astore #5
    //   28: aload_0
    //   29: getfield mWidths : Landroid/text/AutoGrowArray$FloatArray;
    //   32: astore #6
    //   34: aload #6
    //   36: invokevirtual getRawArray : ()[F
    //   39: astore #6
    //   41: aload_0
    //   42: fload #4
    //   44: aload_3
    //   45: aload #5
    //   47: iload_1
    //   48: iload_2
    //   49: iload_1
    //   50: isub
    //   51: iload_1
    //   52: iload_2
    //   53: iload_1
    //   54: isub
    //   55: iconst_0
    //   56: aload #6
    //   58: iload_1
    //   59: invokevirtual getTextRunAdvances : ([CIIIIZ[FI)F
    //   62: fadd
    //   63: putfield mWholeWidth : F
    //   66: goto -> 235
    //   69: aload_3
    //   70: aload_0
    //   71: getfield mCachedPaint : Landroid/text/TextPaint;
    //   74: iload_2
    //   75: iload_1
    //   76: isub
    //   77: iconst_0
    //   78: invokevirtual appendStyleRun : (Landroid/graphics/Paint;IZ)Landroid/graphics/text/MeasuredText$Builder;
    //   81: pop
    //   82: goto -> 235
    //   85: aload_0
    //   86: getfield mLevels : Landroid/text/AutoGrowArray$ByteArray;
    //   89: iload_1
    //   90: invokevirtual get : (I)B
    //   93: istore #7
    //   95: iload_1
    //   96: istore #8
    //   98: iinc #1, 1
    //   101: iload_1
    //   102: iload_2
    //   103: if_icmpeq -> 127
    //   106: iload #7
    //   108: istore #9
    //   110: iload #8
    //   112: istore #10
    //   114: aload_0
    //   115: getfield mLevels : Landroid/text/AutoGrowArray$ByteArray;
    //   118: iload_1
    //   119: invokevirtual get : (I)B
    //   122: iload #7
    //   124: if_icmpeq -> 249
    //   127: iload #7
    //   129: iconst_1
    //   130: iand
    //   131: ifeq -> 140
    //   134: iconst_1
    //   135: istore #11
    //   137: goto -> 143
    //   140: iconst_0
    //   141: istore #11
    //   143: aload_3
    //   144: ifnonnull -> 215
    //   147: iload_1
    //   148: iload #8
    //   150: isub
    //   151: istore #10
    //   153: aload_0
    //   154: getfield mWholeWidth : F
    //   157: fstore #4
    //   159: aload_0
    //   160: getfield mCachedPaint : Landroid/text/TextPaint;
    //   163: astore #6
    //   165: aload_0
    //   166: getfield mCopiedBuffer : [C
    //   169: astore #5
    //   171: aload_0
    //   172: getfield mWidths : Landroid/text/AutoGrowArray$FloatArray;
    //   175: astore #12
    //   177: aload #12
    //   179: invokevirtual getRawArray : ()[F
    //   182: astore #12
    //   184: aload_0
    //   185: fload #4
    //   187: aload #6
    //   189: aload #5
    //   191: iload #8
    //   193: iload #10
    //   195: iload #8
    //   197: iload #10
    //   199: iload #11
    //   201: aload #12
    //   203: iload #8
    //   205: invokevirtual getTextRunAdvances : ([CIIIIZ[FI)F
    //   208: fadd
    //   209: putfield mWholeWidth : F
    //   212: goto -> 230
    //   215: aload_3
    //   216: aload_0
    //   217: getfield mCachedPaint : Landroid/text/TextPaint;
    //   220: iload_1
    //   221: iload #8
    //   223: isub
    //   224: iload #11
    //   226: invokevirtual appendStyleRun : (Landroid/graphics/Paint;IZ)Landroid/graphics/text/MeasuredText$Builder;
    //   229: pop
    //   230: iload_1
    //   231: iload_2
    //   232: if_icmpne -> 236
    //   235: return
    //   236: iload_1
    //   237: istore #10
    //   239: aload_0
    //   240: getfield mLevels : Landroid/text/AutoGrowArray$ByteArray;
    //   243: iload_1
    //   244: invokevirtual get : (I)B
    //   247: istore #9
    //   249: iinc #1, 1
    //   252: iload #9
    //   254: istore #7
    //   256: iload #10
    //   258: istore #8
    //   260: goto -> 101
    // Line number table:
    //   Java source line number -> byte code offset
    //   #525	-> 0
    //   #527	-> 7
    //   #528	-> 11
    //   #530	-> 34
    //   #528	-> 41
    //   #532	-> 69
    //   #536	-> 85
    //   #539	-> 95
    //   #540	-> 101
    //   #541	-> 127
    //   #542	-> 143
    //   #543	-> 147
    //   #544	-> 153
    //   #546	-> 177
    //   #544	-> 184
    //   #547	-> 212
    //   #548	-> 215
    //   #550	-> 230
    //   #551	-> 235
    //   #558	-> 235
    //   #553	-> 236
    //   #554	-> 239
    //   #539	-> 249
  }
  
  private void applyMetricsAffectingSpan(TextPaint paramTextPaint, MetricAffectingSpan[] paramArrayOfMetricAffectingSpan, int paramInt1, int paramInt2, MeasuredText.Builder paramBuilder) {
    MetricAffectingSpan metricAffectingSpan;
    this.mCachedPaint.set(paramTextPaint);
    paramTextPaint = this.mCachedPaint;
    boolean bool = false;
    paramTextPaint.baselineShift = 0;
    if (paramBuilder != null)
      bool = true; 
    if (bool && this.mCachedFm == null)
      this.mCachedFm = new Paint.FontMetricsInt(); 
    TextPaint textPaint = null;
    paramTextPaint = null;
    if (paramArrayOfMetricAffectingSpan != null) {
      byte b = 0;
      while (true) {
        textPaint = paramTextPaint;
        if (b < paramArrayOfMetricAffectingSpan.length) {
          metricAffectingSpan = paramArrayOfMetricAffectingSpan[b];
          if (metricAffectingSpan instanceof ReplacementSpan) {
            ReplacementSpan replacementSpan = (ReplacementSpan)metricAffectingSpan;
          } else {
            metricAffectingSpan.updateMeasureState(this.mCachedPaint);
          } 
          b++;
          continue;
        } 
        break;
      } 
    } 
    int i = this.mTextStart;
    paramInt1 -= i;
    paramInt2 -= i;
    if (paramBuilder != null)
      this.mCachedPaint.getFontMetricsInt(this.mCachedFm); 
    if (metricAffectingSpan != null) {
      applyReplacementRun((ReplacementSpan)metricAffectingSpan, paramInt1, paramInt2, paramBuilder);
    } else {
      applyStyleRun(paramInt1, paramInt2, paramBuilder);
    } 
    if (bool) {
      if (this.mCachedPaint.baselineShift < 0) {
        Paint.FontMetricsInt fontMetricsInt = this.mCachedFm;
        fontMetricsInt.ascent += this.mCachedPaint.baselineShift;
        fontMetricsInt = this.mCachedFm;
        fontMetricsInt.top += this.mCachedPaint.baselineShift;
      } else {
        Paint.FontMetricsInt fontMetricsInt = this.mCachedFm;
        fontMetricsInt.descent += this.mCachedPaint.baselineShift;
        fontMetricsInt = this.mCachedFm;
        fontMetricsInt.bottom += this.mCachedPaint.baselineShift;
      } 
      this.mFontMetrics.append(this.mCachedFm.top);
      this.mFontMetrics.append(this.mCachedFm.bottom);
      this.mFontMetrics.append(this.mCachedFm.ascent);
      this.mFontMetrics.append(this.mCachedFm.descent);
    } 
  }
  
  int breakText(int paramInt, boolean paramBoolean, float paramFloat) {
    int j;
    float[] arrayOfFloat = this.mWidths.getRawArray();
    if (paramBoolean) {
      byte b = 0;
      while (true) {
        j = b;
        if (b < paramInt) {
          paramFloat -= arrayOfFloat[b];
          if (paramFloat < 0.0F) {
            j = b;
            break;
          } 
          b++;
          continue;
        } 
        break;
      } 
      for (; j > 0 && this.mCopiedBuffer[j - 1] == ' '; j--);
      return j;
    } 
    int i = paramInt - 1;
    while (true) {
      j = i;
      if (i >= 0) {
        paramFloat -= arrayOfFloat[i];
        if (paramFloat < 0.0F) {
          j = i;
          break;
        } 
        i--;
        continue;
      } 
      break;
    } 
    while (j < paramInt - 1 && (this.mCopiedBuffer[j + 1] == ' ' || arrayOfFloat[j + 1] == 0.0F))
      j++; 
    return paramInt - j - 1;
  }
  
  float measure(int paramInt1, int paramInt2) {
    float f = 0.0F;
    float[] arrayOfFloat = this.mWidths.getRawArray();
    for (; paramInt1 < paramInt2; paramInt1++)
      f += arrayOfFloat[paramInt1]; 
    return f;
  }
  
  public int getMemoryUsage() {
    return this.mMeasuredText.getMemoryUsage();
  }
}
