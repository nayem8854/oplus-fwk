package android.text;

@Deprecated
public class AlteredCharSequence implements CharSequence, GetChars {
  private char[] mChars;
  
  private int mEnd;
  
  private CharSequence mSource;
  
  private int mStart;
  
  public static AlteredCharSequence make(CharSequence paramCharSequence, char[] paramArrayOfchar, int paramInt1, int paramInt2) {
    if (paramCharSequence instanceof Spanned)
      return new AlteredSpanned(paramArrayOfchar, paramInt1, paramInt2); 
    return new AlteredCharSequence(paramCharSequence, paramArrayOfchar, paramInt1, paramInt2);
  }
  
  private AlteredCharSequence(CharSequence paramCharSequence, char[] paramArrayOfchar, int paramInt1, int paramInt2) {
    this.mSource = paramCharSequence;
    this.mChars = paramArrayOfchar;
    this.mStart = paramInt1;
    this.mEnd = paramInt2;
  }
  
  void update(char[] paramArrayOfchar, int paramInt1, int paramInt2) {
    this.mChars = paramArrayOfchar;
    this.mStart = paramInt1;
    this.mEnd = paramInt2;
  }
  
  class AlteredSpanned extends AlteredCharSequence implements Spanned {
    private Spanned mSpanned;
    
    private AlteredSpanned(AlteredCharSequence this$0, char[] param1ArrayOfchar, int param1Int1, int param1Int2) {
      super(this$0, param1ArrayOfchar, param1Int1, param1Int2);
      this.mSpanned = (Spanned)this$0;
    }
    
    public <T> T[] getSpans(int param1Int1, int param1Int2, Class<T> param1Class) {
      return this.mSpanned.getSpans(param1Int1, param1Int2, param1Class);
    }
    
    public int getSpanStart(Object param1Object) {
      return this.mSpanned.getSpanStart(param1Object);
    }
    
    public int getSpanEnd(Object param1Object) {
      return this.mSpanned.getSpanEnd(param1Object);
    }
    
    public int getSpanFlags(Object param1Object) {
      return this.mSpanned.getSpanFlags(param1Object);
    }
    
    public int nextSpanTransition(int param1Int1, int param1Int2, Class param1Class) {
      return this.mSpanned.nextSpanTransition(param1Int1, param1Int2, param1Class);
    }
  }
  
  public char charAt(int paramInt) {
    int i = this.mStart;
    if (paramInt >= i && paramInt < this.mEnd)
      return this.mChars[paramInt - i]; 
    return this.mSource.charAt(paramInt);
  }
  
  public int length() {
    return this.mSource.length();
  }
  
  public CharSequence subSequence(int paramInt1, int paramInt2) {
    return make(this.mSource.subSequence(paramInt1, paramInt2), this.mChars, this.mStart - paramInt1, this.mEnd - paramInt1);
  }
  
  public void getChars(int paramInt1, int paramInt2, char[] paramArrayOfchar, int paramInt3) {
    TextUtils.getChars(this.mSource, paramInt1, paramInt2, paramArrayOfchar, paramInt3);
    paramInt1 = Math.max(this.mStart, paramInt1);
    paramInt2 = Math.min(this.mEnd, paramInt2);
    if (paramInt1 > paramInt2)
      System.arraycopy(this.mChars, paramInt1 - this.mStart, paramArrayOfchar, paramInt3, paramInt2 - paramInt1); 
  }
  
  public String toString() {
    int i = length();
    char[] arrayOfChar = new char[i];
    getChars(0, i, arrayOfChar, 0);
    return String.valueOf(arrayOfChar);
  }
}
