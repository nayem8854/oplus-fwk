package android.text;

import android.common.ColorFrameworkFactory;
import android.graphics.Paint;
import android.text.style.LineHeightSpan;
import android.util.Log;
import android.util.Pools;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.GrowingArrayUtils;

public class StaticLayout extends Layout {
  private static final char CHAR_NEW_LINE = '\n';
  
  private static final int COLUMNS_ELLIPSIZE = 7;
  
  private static final int COLUMNS_NORMAL = 5;
  
  private static final int DEFAULT_MAX_LINE_HEIGHT = -1;
  
  private static final int DESCENT = 2;
  
  private static final int DIR = 0;
  
  private static final int DIR_SHIFT = 30;
  
  private static final int ELLIPSIS_COUNT = 6;
  
  private static final int ELLIPSIS_START = 5;
  
  private static final int END_HYPHEN_MASK = 7;
  
  private static final int EXTRA = 3;
  
  private static final double EXTRA_ROUNDING = 0.5D;
  
  private static final int HYPHEN = 4;
  
  private static final int HYPHEN_MASK = 255;
  
  private static final int START = 0;
  
  private static final int START_HYPHEN_BITS_SHIFT = 3;
  
  private static final int START_HYPHEN_MASK = 24;
  
  private static final int START_MASK = 536870911;
  
  private static final int TAB = 0;
  
  private static final float TAB_INCREMENT = 20.0F;
  
  private static final int TAB_MASK = 536870912;
  
  static final String TAG = "StaticLayout";
  
  private static final int TOP = 1;
  
  private int mBottomPadding;
  
  private int mColumns;
  
  private boolean mEllipsized;
  
  private int mEllipsizedWidth;
  
  private int[] mLeftIndents;
  
  private int mLineCount;
  
  private Layout.Directions[] mLineDirections;
  
  private int[] mLines;
  
  private int mMaxLineHeight;
  
  private int mMaximumVisibleLineCount;
  
  private int[] mRightIndents;
  
  ITextJustificationHooks mTextJustificationHooksImpl;
  
  private int mTopPadding;
  
  class Builder {
    public static Builder obtain(CharSequence param1CharSequence, int param1Int1, int param1Int2, TextPaint param1TextPaint, int param1Int3) {
      Builder builder1 = sPool.acquire();
      Builder builder2 = builder1;
      if (builder1 == null)
        builder2 = new Builder(); 
      builder2.mText = param1CharSequence;
      builder2.mStart = param1Int1;
      builder2.mEnd = param1Int2;
      builder2.mPaint = param1TextPaint;
      builder2.mWidth = param1Int3;
      builder2.mAlignment = Layout.Alignment.ALIGN_NORMAL;
      builder2.mTextDir = TextDirectionHeuristics.FIRSTSTRONG_LTR;
      builder2.mSpacingMult = 1.0F;
      builder2.mSpacingAdd = 0.0F;
      builder2.mIncludePad = true;
      builder2.mFallbackLineSpacing = false;
      builder2.mEllipsizedWidth = param1Int3;
      builder2.mEllipsize = null;
      builder2.mMaxLines = Integer.MAX_VALUE;
      builder2.mBreakStrategy = 0;
      builder2.mHyphenationFrequency = 0;
      builder2.mJustificationMode = 0;
      return builder2;
    }
    
    private static void recycle(Builder param1Builder) {
      param1Builder.mPaint = null;
      param1Builder.mText = null;
      param1Builder.mLeftIndents = null;
      param1Builder.mRightIndents = null;
      sPool.release(param1Builder);
    }
    
    void finish() {
      this.mText = null;
      this.mPaint = null;
      this.mLeftIndents = null;
      this.mRightIndents = null;
    }
    
    public Builder setText(CharSequence param1CharSequence) {
      return setText(param1CharSequence, 0, param1CharSequence.length());
    }
    
    public Builder setText(CharSequence param1CharSequence, int param1Int1, int param1Int2) {
      this.mText = param1CharSequence;
      this.mStart = param1Int1;
      this.mEnd = param1Int2;
      return this;
    }
    
    public Builder setPaint(TextPaint param1TextPaint) {
      this.mPaint = param1TextPaint;
      return this;
    }
    
    public Builder setWidth(int param1Int) {
      this.mWidth = param1Int;
      if (this.mEllipsize == null)
        this.mEllipsizedWidth = param1Int; 
      return this;
    }
    
    public Builder setAlignment(Layout.Alignment param1Alignment) {
      this.mAlignment = param1Alignment;
      return this;
    }
    
    public Builder setTextDirection(TextDirectionHeuristic param1TextDirectionHeuristic) {
      this.mTextDir = param1TextDirectionHeuristic;
      return this;
    }
    
    public Builder setLineSpacing(float param1Float1, float param1Float2) {
      this.mSpacingAdd = param1Float1;
      this.mSpacingMult = param1Float2;
      return this;
    }
    
    public Builder setIncludePad(boolean param1Boolean) {
      this.mIncludePad = param1Boolean;
      return this;
    }
    
    public Builder setUseLineSpacingFromFallbacks(boolean param1Boolean) {
      this.mFallbackLineSpacing = param1Boolean;
      return this;
    }
    
    public Builder setEllipsizedWidth(int param1Int) {
      this.mEllipsizedWidth = param1Int;
      return this;
    }
    
    public Builder setEllipsize(TextUtils.TruncateAt param1TruncateAt) {
      this.mEllipsize = param1TruncateAt;
      return this;
    }
    
    public Builder setMaxLines(int param1Int) {
      this.mMaxLines = param1Int;
      return this;
    }
    
    public Builder setBreakStrategy(int param1Int) {
      this.mBreakStrategy = param1Int;
      return this;
    }
    
    public Builder setHyphenationFrequency(int param1Int) {
      this.mHyphenationFrequency = param1Int;
      return this;
    }
    
    public Builder setIndents(int[] param1ArrayOfint1, int[] param1ArrayOfint2) {
      this.mLeftIndents = param1ArrayOfint1;
      this.mRightIndents = param1ArrayOfint2;
      return this;
    }
    
    public Builder setJustificationMode(int param1Int) {
      this.mJustificationMode = param1Int;
      return this;
    }
    
    Builder setAddLastLineLineSpacing(boolean param1Boolean) {
      this.mAddLastLineLineSpacing = param1Boolean;
      return this;
    }
    
    public StaticLayout build() {
      StaticLayout staticLayout = new StaticLayout(this);
      recycle(this);
      return staticLayout;
    }
    
    private final Paint.FontMetricsInt mFontMetricsInt = new Paint.FontMetricsInt();
    
    private static final Pools.SynchronizedPool<Builder> sPool = new Pools.SynchronizedPool<>(3);
    
    public final ITextJustificationHooks mBuilderJustificationHooksImpl = (ITextJustificationHooks)ColorFrameworkFactory.getInstance().getFeature(ITextJustificationHooks.DEFAULT, new Object[0]);
    
    private boolean mAddLastLineLineSpacing;
    
    private Layout.Alignment mAlignment;
    
    private int mBreakStrategy;
    
    private TextUtils.TruncateAt mEllipsize;
    
    private int mEllipsizedWidth;
    
    private int mEnd;
    
    private boolean mFallbackLineSpacing;
    
    private int mHyphenationFrequency;
    
    private boolean mIncludePad;
    
    private int mJustificationMode;
    
    private int[] mLeftIndents;
    
    private int mMaxLines;
    
    private TextPaint mPaint;
    
    private int[] mRightIndents;
    
    private float mSpacingAdd;
    
    private float mSpacingMult;
    
    private int mStart;
    
    private CharSequence mText;
    
    private TextDirectionHeuristic mTextDir;
    
    private int mWidth;
  }
  
  @Deprecated
  public StaticLayout(CharSequence paramCharSequence, TextPaint paramTextPaint, int paramInt, Layout.Alignment paramAlignment, float paramFloat1, float paramFloat2, boolean paramBoolean) {
    this(paramCharSequence, 0, paramCharSequence.length(), paramTextPaint, paramInt, paramAlignment, paramFloat1, paramFloat2, paramBoolean);
  }
  
  @Deprecated
  public StaticLayout(CharSequence paramCharSequence, int paramInt1, int paramInt2, TextPaint paramTextPaint, int paramInt3, Layout.Alignment paramAlignment, float paramFloat1, float paramFloat2, boolean paramBoolean) {
    this(paramCharSequence, paramInt1, paramInt2, paramTextPaint, paramInt3, paramAlignment, paramFloat1, paramFloat2, paramBoolean, (TextUtils.TruncateAt)null, 0);
  }
  
  @Deprecated
  public StaticLayout(CharSequence paramCharSequence, int paramInt1, int paramInt2, TextPaint paramTextPaint, int paramInt3, Layout.Alignment paramAlignment, float paramFloat1, float paramFloat2, boolean paramBoolean, TextUtils.TruncateAt paramTruncateAt, int paramInt4) {
    this(paramCharSequence, paramInt1, paramInt2, paramTextPaint, paramInt3, paramAlignment, TextDirectionHeuristics.FIRSTSTRONG_LTR, paramFloat1, paramFloat2, paramBoolean, paramTruncateAt, paramInt4, 2147483647);
  }
  
  @Deprecated
  public StaticLayout(CharSequence paramCharSequence, int paramInt1, int paramInt2, TextPaint paramTextPaint, int paramInt3, Layout.Alignment paramAlignment, TextDirectionHeuristic paramTextDirectionHeuristic, float paramFloat1, float paramFloat2, boolean paramBoolean, TextUtils.TruncateAt paramTruncateAt, int paramInt4, int paramInt5) {
    super(charSequence, paramTextPaint, paramInt3, paramAlignment, paramTextDirectionHeuristic, paramFloat1, paramFloat2);
    CharSequence charSequence;
    this.mMaxLineHeight = -1;
    this.mMaximumVisibleLineCount = Integer.MAX_VALUE;
    Builder builder = Builder.obtain(paramCharSequence, paramInt1, paramInt2, paramTextPaint, paramInt3);
    builder = builder.setAlignment(paramAlignment);
    builder = builder.setTextDirection(paramTextDirectionHeuristic);
    builder = builder.setLineSpacing(paramFloat2, paramFloat1);
    builder = builder.setIncludePad(paramBoolean);
    builder = builder.setEllipsizedWidth(paramInt4);
    builder = builder.setEllipsize(paramTruncateAt);
    builder = builder.setMaxLines(paramInt5);
    if (paramTruncateAt != null) {
      Layout.Ellipsizer ellipsizer = (Layout.Ellipsizer)getText();
      ellipsizer.mLayout = this;
      ellipsizer.mWidth = paramInt4;
      ellipsizer.mMethod = paramTruncateAt;
      this.mEllipsizedWidth = paramInt4;
      this.mColumns = 7;
    } else {
      this.mColumns = 5;
      this.mEllipsizedWidth = paramInt3;
    } 
    this.mLineDirections = (Layout.Directions[])ArrayUtils.newUnpaddedArray(Layout.Directions.class, 2);
    this.mLines = ArrayUtils.newUnpaddedIntArray(this.mColumns * 2);
    this.mMaximumVisibleLineCount = paramInt5;
    generate(builder, builder.mIncludePad, builder.mIncludePad);
    Builder.recycle(builder);
  }
  
  StaticLayout(CharSequence paramCharSequence) {
    super(paramCharSequence, null, 0, null, 0.0F, 0.0F);
    this.mMaxLineHeight = -1;
    this.mMaximumVisibleLineCount = Integer.MAX_VALUE;
    this.mColumns = 7;
    this.mLineDirections = (Layout.Directions[])ArrayUtils.newUnpaddedArray(Layout.Directions.class, 2);
    this.mLines = ArrayUtils.newUnpaddedIntArray(this.mColumns * 2);
  }
  
  private StaticLayout(Builder paramBuilder) {
    super(charSequence, textPaint, i, alignment, textDirectionHeuristic, f1, f2);
    CharSequence charSequence;
    this.mMaxLineHeight = -1;
    this.mMaximumVisibleLineCount = Integer.MAX_VALUE;
    this.mTextJustificationHooksImpl = paramBuilder.mBuilderJustificationHooksImpl;
    if (paramBuilder.mEllipsize != null) {
      charSequence = getText();
      ((Layout.Ellipsizer)charSequence).mLayout = this;
      ((Layout.Ellipsizer)charSequence).mWidth = paramBuilder.mEllipsizedWidth;
      ((Layout.Ellipsizer)charSequence).mMethod = paramBuilder.mEllipsize;
      this.mEllipsizedWidth = paramBuilder.mEllipsizedWidth;
      this.mColumns = 7;
    } else {
      this.mColumns = 5;
      this.mEllipsizedWidth = paramBuilder.mWidth;
    } 
    this.mLineDirections = (Layout.Directions[])ArrayUtils.newUnpaddedArray(Layout.Directions.class, 2);
    this.mLines = ArrayUtils.newUnpaddedIntArray(this.mColumns * 2);
    this.mMaximumVisibleLineCount = paramBuilder.mMaxLines;
    this.mLeftIndents = paramBuilder.mLeftIndents;
    this.mRightIndents = paramBuilder.mRightIndents;
    setJustificationMode(paramBuilder.mJustificationMode);
    generate(paramBuilder, paramBuilder.mIncludePad, paramBuilder.mIncludePad);
  }
  
  void generate(Builder paramBuilder, boolean paramBoolean1, boolean paramBoolean2) {
    // Byte code:
    //   0: aload_0
    //   1: astore #4
    //   3: aload_1
    //   4: invokestatic access$400 : (Landroid/text/StaticLayout$Builder;)Ljava/lang/CharSequence;
    //   7: astore #5
    //   9: aload_1
    //   10: invokestatic access$1600 : (Landroid/text/StaticLayout$Builder;)I
    //   13: istore #6
    //   15: aload_1
    //   16: invokestatic access$1700 : (Landroid/text/StaticLayout$Builder;)I
    //   19: istore #7
    //   21: aload_1
    //   22: invokestatic access$500 : (Landroid/text/StaticLayout$Builder;)Landroid/text/TextPaint;
    //   25: astore #8
    //   27: aload_1
    //   28: invokestatic access$600 : (Landroid/text/StaticLayout$Builder;)I
    //   31: istore #9
    //   33: aload_1
    //   34: invokestatic access$800 : (Landroid/text/StaticLayout$Builder;)Landroid/text/TextDirectionHeuristic;
    //   37: astore #10
    //   39: aload_1
    //   40: invokestatic access$1800 : (Landroid/text/StaticLayout$Builder;)Z
    //   43: istore #11
    //   45: aload_1
    //   46: invokestatic access$900 : (Landroid/text/StaticLayout$Builder;)F
    //   49: fstore #12
    //   51: aload_1
    //   52: invokestatic access$1000 : (Landroid/text/StaticLayout$Builder;)F
    //   55: fstore #13
    //   57: aload_1
    //   58: invokestatic access$1100 : (Landroid/text/StaticLayout$Builder;)I
    //   61: i2f
    //   62: fstore #14
    //   64: aload_1
    //   65: invokestatic access$300 : (Landroid/text/StaticLayout$Builder;)Landroid/text/TextUtils$TruncateAt;
    //   68: astore #15
    //   70: aload_1
    //   71: invokestatic access$1900 : (Landroid/text/StaticLayout$Builder;)Z
    //   74: istore #16
    //   76: iconst_0
    //   77: istore #17
    //   79: aconst_null
    //   80: astore #18
    //   82: aconst_null
    //   83: astore #19
    //   85: aconst_null
    //   86: astore #20
    //   88: aconst_null
    //   89: astore #21
    //   91: aconst_null
    //   92: astore #22
    //   94: aconst_null
    //   95: astore #23
    //   97: aload #4
    //   99: getfield mTextJustificationHooksImpl : Landroid/text/ITextJustificationHooks;
    //   102: ifnonnull -> 126
    //   105: aload #4
    //   107: invokestatic getInstance : ()Landroid/common/ColorFrameworkFactory;
    //   110: getstatic android/text/ITextJustificationHooks.DEFAULT : Landroid/text/ITextJustificationHooks;
    //   113: iconst_0
    //   114: anewarray java/lang/Object
    //   117: invokevirtual getFeature : (Landroid/common/IOplusCommonFeature;[Ljava/lang/Object;)Landroid/common/IOplusCommonFeature;
    //   120: checkcast android/text/ITextJustificationHooks
    //   123: putfield mTextJustificationHooksImpl : Landroid/text/ITextJustificationHooks;
    //   126: aload #4
    //   128: iconst_0
    //   129: putfield mLineCount : I
    //   132: aload #4
    //   134: iconst_0
    //   135: putfield mEllipsized : Z
    //   138: aload #4
    //   140: getfield mMaximumVisibleLineCount : I
    //   143: iconst_1
    //   144: if_icmpge -> 153
    //   147: iconst_0
    //   148: istore #24
    //   150: goto -> 156
    //   153: iconst_m1
    //   154: istore #24
    //   156: aload #4
    //   158: iload #24
    //   160: putfield mMaxLineHeight : I
    //   163: iconst_0
    //   164: istore #25
    //   166: fload #12
    //   168: fconst_1
    //   169: fcmpl
    //   170: ifne -> 189
    //   173: fload #13
    //   175: fconst_0
    //   176: fcmpl
    //   177: ifeq -> 183
    //   180: goto -> 189
    //   183: iconst_0
    //   184: istore #26
    //   186: goto -> 192
    //   189: iconst_1
    //   190: istore #26
    //   192: aload_1
    //   193: invokestatic access$2000 : (Landroid/text/StaticLayout$Builder;)Landroid/graphics/Paint$FontMetricsInt;
    //   196: astore #27
    //   198: aload #4
    //   200: getfield mLeftIndents : [I
    //   203: ifnonnull -> 223
    //   206: aload #4
    //   208: getfield mRightIndents : [I
    //   211: ifnull -> 217
    //   214: goto -> 223
    //   217: aconst_null
    //   218: astore #28
    //   220: goto -> 348
    //   223: aload #4
    //   225: getfield mLeftIndents : [I
    //   228: astore #28
    //   230: aload #28
    //   232: ifnonnull -> 241
    //   235: iconst_0
    //   236: istore #24
    //   238: goto -> 246
    //   241: aload #28
    //   243: arraylength
    //   244: istore #24
    //   246: aload #4
    //   248: getfield mRightIndents : [I
    //   251: astore #28
    //   253: aload #28
    //   255: ifnonnull -> 264
    //   258: iconst_0
    //   259: istore #29
    //   261: goto -> 269
    //   264: aload #28
    //   266: arraylength
    //   267: istore #29
    //   269: iload #24
    //   271: iload #29
    //   273: invokestatic max : (II)I
    //   276: istore #30
    //   278: iload #30
    //   280: newarray int
    //   282: astore #28
    //   284: iconst_0
    //   285: istore #30
    //   287: iload #30
    //   289: iload #24
    //   291: if_icmpge -> 313
    //   294: aload #28
    //   296: iload #30
    //   298: aload #4
    //   300: getfield mLeftIndents : [I
    //   303: iload #30
    //   305: iaload
    //   306: iastore
    //   307: iinc #30, 1
    //   310: goto -> 287
    //   313: iconst_0
    //   314: istore #30
    //   316: iload #30
    //   318: iload #29
    //   320: if_icmpge -> 348
    //   323: aload #28
    //   325: iload #30
    //   327: aload #28
    //   329: iload #30
    //   331: iaload
    //   332: aload #4
    //   334: getfield mRightIndents : [I
    //   337: iload #30
    //   339: iaload
    //   340: iadd
    //   341: iastore
    //   342: iinc #30, 1
    //   345: goto -> 316
    //   348: new android/graphics/text/LineBreaker$Builder
    //   351: dup
    //   352: invokespecial <init> : ()V
    //   355: astore #31
    //   357: aload #31
    //   359: aload_1
    //   360: invokestatic access$2200 : (Landroid/text/StaticLayout$Builder;)I
    //   363: invokevirtual setBreakStrategy : (I)Landroid/graphics/text/LineBreaker$Builder;
    //   366: astore #31
    //   368: aload #31
    //   370: aload_1
    //   371: invokestatic access$2100 : (Landroid/text/StaticLayout$Builder;)I
    //   374: invokevirtual setHyphenationFrequency : (I)Landroid/graphics/text/LineBreaker$Builder;
    //   377: astore #31
    //   379: aload #31
    //   381: aload_1
    //   382: invokestatic access$1500 : (Landroid/text/StaticLayout$Builder;)I
    //   385: invokevirtual setJustificationMode : (I)Landroid/graphics/text/LineBreaker$Builder;
    //   388: astore #31
    //   390: aload #31
    //   392: aload #28
    //   394: invokevirtual setIndents : ([I)Landroid/graphics/text/LineBreaker$Builder;
    //   397: astore #28
    //   399: aload #28
    //   401: invokevirtual build : ()Landroid/graphics/text/LineBreaker;
    //   404: astore #32
    //   406: new android/graphics/text/LineBreaker$ParagraphConstraints
    //   409: dup
    //   410: invokespecial <init> : ()V
    //   413: astore #33
    //   415: aconst_null
    //   416: astore #28
    //   418: aload #5
    //   420: instanceof android/text/Spanned
    //   423: ifeq -> 436
    //   426: aload #5
    //   428: checkcast android/text/Spanned
    //   431: astore #31
    //   433: goto -> 439
    //   436: aconst_null
    //   437: astore #31
    //   439: aload #5
    //   441: instanceof android/text/PrecomputedText
    //   444: istore #34
    //   446: iload #34
    //   448: ifeq -> 582
    //   451: aload #5
    //   453: checkcast android/text/PrecomputedText
    //   456: astore #35
    //   458: aload_1
    //   459: invokestatic access$2200 : (Landroid/text/StaticLayout$Builder;)I
    //   462: istore #24
    //   464: aload_1
    //   465: invokestatic access$2100 : (Landroid/text/StaticLayout$Builder;)I
    //   468: istore #29
    //   470: aload #35
    //   472: iload #6
    //   474: iload #7
    //   476: aload #10
    //   478: aload #8
    //   480: iload #24
    //   482: iload #29
    //   484: invokevirtual checkResultUsable : (IILandroid/text/TextDirectionHeuristic;Landroid/text/TextPaint;II)I
    //   487: istore #24
    //   489: iload #24
    //   491: iconst_1
    //   492: if_icmpeq -> 514
    //   495: iload #24
    //   497: iconst_2
    //   498: if_icmpeq -> 504
    //   501: goto -> 582
    //   504: aload #35
    //   506: invokevirtual getParagraphInfo : ()[Landroid/text/PrecomputedText$ParagraphInfo;
    //   509: astore #28
    //   511: goto -> 582
    //   514: new android/text/PrecomputedText$Params$Builder
    //   517: dup
    //   518: aload #8
    //   520: invokespecial <init> : (Landroid/text/TextPaint;)V
    //   523: astore #28
    //   525: aload #28
    //   527: aload_1
    //   528: invokestatic access$2200 : (Landroid/text/StaticLayout$Builder;)I
    //   531: invokevirtual setBreakStrategy : (I)Landroid/text/PrecomputedText$Params$Builder;
    //   534: astore #28
    //   536: aload #28
    //   538: aload_1
    //   539: invokestatic access$2100 : (Landroid/text/StaticLayout$Builder;)I
    //   542: invokevirtual setHyphenationFrequency : (I)Landroid/text/PrecomputedText$Params$Builder;
    //   545: astore #28
    //   547: aload #28
    //   549: aload #10
    //   551: invokevirtual setTextDirection : (Landroid/text/TextDirectionHeuristic;)Landroid/text/PrecomputedText$Params$Builder;
    //   554: astore #28
    //   556: aload #28
    //   558: invokevirtual build : ()Landroid/text/PrecomputedText$Params;
    //   561: astore #28
    //   563: aload #35
    //   565: aload #28
    //   567: invokestatic create : (Ljava/lang/CharSequence;Landroid/text/PrecomputedText$Params;)Landroid/text/PrecomputedText;
    //   570: astore #28
    //   572: aload #28
    //   574: invokevirtual getParagraphInfo : ()[Landroid/text/PrecomputedText$ParagraphInfo;
    //   577: astore #28
    //   579: goto -> 582
    //   582: aload #28
    //   584: ifnonnull -> 625
    //   587: new android/text/PrecomputedText$Params
    //   590: dup
    //   591: aload #8
    //   593: aload #10
    //   595: aload_1
    //   596: invokestatic access$2200 : (Landroid/text/StaticLayout$Builder;)I
    //   599: aload_1
    //   600: invokestatic access$2100 : (Landroid/text/StaticLayout$Builder;)I
    //   603: invokespecial <init> : (Landroid/text/TextPaint;Landroid/text/TextDirectionHeuristic;II)V
    //   606: astore #28
    //   608: aload #5
    //   610: aload #28
    //   612: iload #6
    //   614: iload #7
    //   616: iconst_0
    //   617: invokestatic createMeasuredParagraphs : (Ljava/lang/CharSequence;Landroid/text/PrecomputedText$Params;IIZ)[Landroid/text/PrecomputedText$ParagraphInfo;
    //   620: astore #35
    //   622: goto -> 629
    //   625: aload #28
    //   627: astore #35
    //   629: iconst_0
    //   630: istore #36
    //   632: aconst_null
    //   633: astore #28
    //   635: aload #31
    //   637: astore #37
    //   639: aload #5
    //   641: astore #31
    //   643: iload #6
    //   645: istore #24
    //   647: iload #7
    //   649: istore #30
    //   651: iload #36
    //   653: aload #35
    //   655: arraylength
    //   656: if_icmpge -> 2435
    //   659: iload #36
    //   661: ifne -> 671
    //   664: iload #24
    //   666: istore #29
    //   668: goto -> 683
    //   671: aload #35
    //   673: iload #36
    //   675: iconst_1
    //   676: isub
    //   677: aaload
    //   678: getfield paragraphEnd : I
    //   681: istore #29
    //   683: aload #35
    //   685: iload #36
    //   687: aaload
    //   688: getfield paragraphEnd : I
    //   691: istore #38
    //   693: iconst_1
    //   694: istore #39
    //   696: iload #9
    //   698: istore #6
    //   700: iload #9
    //   702: istore #7
    //   704: aload #37
    //   706: ifnull -> 994
    //   709: aload #37
    //   711: iload #29
    //   713: iload #38
    //   715: ldc_w android/text/style/LeadingMarginSpan
    //   718: invokestatic getParagraphSpans : (Landroid/text/Spanned;IILjava/lang/Class;)[Ljava/lang/Object;
    //   721: checkcast [Landroid/text/style/LeadingMarginSpan;
    //   724: astore #5
    //   726: iconst_0
    //   727: istore #40
    //   729: iload #24
    //   731: istore #41
    //   733: iload #39
    //   735: istore #24
    //   737: iload #40
    //   739: aload #5
    //   741: arraylength
    //   742: if_icmpge -> 835
    //   745: aload #5
    //   747: iload #40
    //   749: aaload
    //   750: astore #42
    //   752: iload #6
    //   754: aload #5
    //   756: iload #40
    //   758: aaload
    //   759: iconst_1
    //   760: invokeinterface getLeadingMargin : (Z)I
    //   765: isub
    //   766: istore #6
    //   768: iload #7
    //   770: aload #5
    //   772: iload #40
    //   774: aaload
    //   775: iconst_0
    //   776: invokeinterface getLeadingMargin : (Z)I
    //   781: isub
    //   782: istore #39
    //   784: iload #24
    //   786: istore #7
    //   788: aload #42
    //   790: instanceof android/text/style/LeadingMarginSpan$LeadingMarginSpan2
    //   793: ifeq -> 821
    //   796: aload #42
    //   798: checkcast android/text/style/LeadingMarginSpan$LeadingMarginSpan2
    //   801: astore #42
    //   803: aload #42
    //   805: invokeinterface getLeadingMarginLineCount : ()I
    //   810: istore #7
    //   812: iload #24
    //   814: iload #7
    //   816: invokestatic max : (II)I
    //   819: istore #7
    //   821: iinc #40, 1
    //   824: iload #7
    //   826: istore #24
    //   828: iload #39
    //   830: istore #7
    //   832: goto -> 737
    //   835: iload #41
    //   837: istore #40
    //   839: aload #31
    //   841: astore #42
    //   843: aload #37
    //   845: iload #29
    //   847: iload #38
    //   849: ldc_w android/text/style/LineHeightSpan
    //   852: invokestatic getParagraphSpans : (Landroid/text/Spanned;IILjava/lang/Class;)[Ljava/lang/Object;
    //   855: checkcast [Landroid/text/style/LineHeightSpan;
    //   858: astore #5
    //   860: aload #5
    //   862: arraylength
    //   863: ifne -> 884
    //   866: iload #6
    //   868: istore #41
    //   870: aconst_null
    //   871: astore #5
    //   873: iload #7
    //   875: istore #6
    //   877: iload #40
    //   879: istore #7
    //   881: goto -> 1020
    //   884: aload #28
    //   886: ifnull -> 902
    //   889: aload #28
    //   891: astore #31
    //   893: aload #28
    //   895: arraylength
    //   896: aload #5
    //   898: arraylength
    //   899: if_icmpge -> 910
    //   902: aload #5
    //   904: arraylength
    //   905: invokestatic newUnpaddedIntArray : (I)[I
    //   908: astore #31
    //   910: iconst_0
    //   911: istore #41
    //   913: iload #41
    //   915: aload #5
    //   917: arraylength
    //   918: if_icmpge -> 975
    //   921: aload #37
    //   923: aload #5
    //   925: iload #41
    //   927: aaload
    //   928: invokeinterface getSpanStart : (Ljava/lang/Object;)I
    //   933: istore #39
    //   935: iload #39
    //   937: iload #29
    //   939: if_icmpge -> 962
    //   942: aload #31
    //   944: iload #41
    //   946: aload #4
    //   948: aload #4
    //   950: iload #39
    //   952: invokevirtual getLineForOffset : (I)I
    //   955: invokevirtual getLineTop : (I)I
    //   958: iastore
    //   959: goto -> 969
    //   962: aload #31
    //   964: iload #41
    //   966: iload #25
    //   968: iastore
    //   969: iinc #41, 1
    //   972: goto -> 913
    //   975: aload #31
    //   977: astore #28
    //   979: iload #6
    //   981: istore #41
    //   983: iload #7
    //   985: istore #6
    //   987: iload #40
    //   989: istore #7
    //   991: goto -> 1020
    //   994: iconst_1
    //   995: istore #40
    //   997: iload #6
    //   999: istore #41
    //   1001: iload #7
    //   1003: istore #6
    //   1005: aconst_null
    //   1006: astore #5
    //   1008: aload #31
    //   1010: astore #42
    //   1012: iload #24
    //   1014: istore #7
    //   1016: iload #40
    //   1018: istore #24
    //   1020: aload #10
    //   1022: astore #43
    //   1024: iconst_0
    //   1025: istore #34
    //   1027: aload #37
    //   1029: ifnull -> 1110
    //   1032: aload #37
    //   1034: iload #29
    //   1036: iload #38
    //   1038: ldc_w android/text/style/TabStopSpan
    //   1041: invokestatic getParagraphSpans : (Landroid/text/Spanned;IILjava/lang/Class;)[Ljava/lang/Object;
    //   1044: checkcast [Landroid/text/style/TabStopSpan;
    //   1047: astore #10
    //   1049: aload #10
    //   1051: arraylength
    //   1052: ifle -> 1107
    //   1055: aload #10
    //   1057: arraylength
    //   1058: newarray float
    //   1060: astore #31
    //   1062: iconst_0
    //   1063: istore #40
    //   1065: iload #40
    //   1067: aload #10
    //   1069: arraylength
    //   1070: if_icmpge -> 1095
    //   1073: aload #31
    //   1075: iload #40
    //   1077: aload #10
    //   1079: iload #40
    //   1081: aaload
    //   1082: invokeinterface getTabStop : ()I
    //   1087: i2f
    //   1088: fastore
    //   1089: iinc #40, 1
    //   1092: goto -> 1065
    //   1095: aload #31
    //   1097: iconst_0
    //   1098: aload #31
    //   1100: arraylength
    //   1101: invokestatic sort : ([FII)V
    //   1104: goto -> 1113
    //   1107: goto -> 1110
    //   1110: aconst_null
    //   1111: astore #31
    //   1113: aload #35
    //   1115: iload #36
    //   1117: aaload
    //   1118: getfield measured : Landroid/text/MeasuredParagraph;
    //   1121: astore #44
    //   1123: aload #44
    //   1125: invokevirtual getChars : ()[C
    //   1128: astore #45
    //   1130: aload #44
    //   1132: invokevirtual getSpanEndCache : ()Landroid/text/AutoGrowArray$IntArray;
    //   1135: invokevirtual getRawArray : ()[I
    //   1138: astore #46
    //   1140: aload #44
    //   1142: invokevirtual getFontMetrics : ()Landroid/text/AutoGrowArray$IntArray;
    //   1145: invokevirtual getRawArray : ()[I
    //   1148: astore #47
    //   1150: aload #33
    //   1152: iload #6
    //   1154: i2f
    //   1155: invokevirtual setWidth : (F)V
    //   1158: aload #33
    //   1160: iload #41
    //   1162: i2f
    //   1163: iload #24
    //   1165: invokevirtual setIndent : (FI)V
    //   1168: aload #33
    //   1170: aload #31
    //   1172: ldc 20.0
    //   1174: invokevirtual setTabStops : ([FF)V
    //   1177: aload #44
    //   1179: invokevirtual getMeasuredText : ()Landroid/graphics/text/MeasuredText;
    //   1182: astore #10
    //   1184: iload #36
    //   1186: istore #40
    //   1188: aload #4
    //   1190: getfield mLineCount : I
    //   1193: istore #36
    //   1195: iload #24
    //   1197: istore #48
    //   1199: aload #32
    //   1201: aload #10
    //   1203: aload #33
    //   1205: iload #36
    //   1207: invokevirtual computeLineBreaks : (Landroid/graphics/text/MeasuredText;Landroid/graphics/text/LineBreaker$ParagraphConstraints;I)Landroid/graphics/text/LineBreaker$Result;
    //   1210: astore #49
    //   1212: aload #49
    //   1214: invokevirtual getLineCount : ()I
    //   1217: istore #36
    //   1219: iload #17
    //   1221: iload #36
    //   1223: if_icmpge -> 1269
    //   1226: iload #36
    //   1228: newarray int
    //   1230: astore #18
    //   1232: iload #36
    //   1234: newarray float
    //   1236: astore #19
    //   1238: iload #36
    //   1240: newarray float
    //   1242: astore #20
    //   1244: iload #36
    //   1246: newarray float
    //   1248: astore #21
    //   1250: iload #36
    //   1252: newarray boolean
    //   1254: astore #22
    //   1256: iload #36
    //   1258: newarray int
    //   1260: astore #23
    //   1262: iload #36
    //   1264: istore #17
    //   1266: goto -> 1269
    //   1269: aload #33
    //   1271: astore #50
    //   1273: iconst_0
    //   1274: istore #24
    //   1276: iload #24
    //   1278: iload #36
    //   1280: if_icmpge -> 1371
    //   1283: aload #18
    //   1285: iload #24
    //   1287: aload #49
    //   1289: iload #24
    //   1291: invokevirtual getLineBreakOffset : (I)I
    //   1294: iastore
    //   1295: aload #19
    //   1297: iload #24
    //   1299: aload #49
    //   1301: iload #24
    //   1303: invokevirtual getLineWidth : (I)F
    //   1306: fastore
    //   1307: aload #20
    //   1309: iload #24
    //   1311: aload #49
    //   1313: iload #24
    //   1315: invokevirtual getLineAscent : (I)F
    //   1318: fastore
    //   1319: aload #21
    //   1321: iload #24
    //   1323: aload #49
    //   1325: iload #24
    //   1327: invokevirtual getLineDescent : (I)F
    //   1330: fastore
    //   1331: aload #22
    //   1333: iload #24
    //   1335: aload #49
    //   1337: iload #24
    //   1339: invokevirtual hasLineTab : (I)Z
    //   1342: bastore
    //   1343: aload #23
    //   1345: iload #24
    //   1347: aload #49
    //   1349: iload #24
    //   1351: invokevirtual getStartLineHyphenEdit : (I)I
    //   1354: aload #49
    //   1356: iload #24
    //   1358: invokevirtual getEndLineHyphenEdit : (I)I
    //   1361: invokestatic packHyphenEdit : (II)I
    //   1364: iastore
    //   1365: iinc #24, 1
    //   1368: goto -> 1276
    //   1371: aload #35
    //   1373: astore #33
    //   1375: aload #4
    //   1377: getfield mMaximumVisibleLineCount : I
    //   1380: aload #4
    //   1382: getfield mLineCount : I
    //   1385: isub
    //   1386: istore #39
    //   1388: aload #15
    //   1390: ifnull -> 1424
    //   1393: aload #15
    //   1395: getstatic android/text/TextUtils$TruncateAt.END : Landroid/text/TextUtils$TruncateAt;
    //   1398: if_acmpeq -> 1418
    //   1401: aload #4
    //   1403: getfield mMaximumVisibleLineCount : I
    //   1406: iconst_1
    //   1407: if_icmpne -> 1424
    //   1410: aload #15
    //   1412: getstatic android/text/TextUtils$TruncateAt.MARQUEE : Landroid/text/TextUtils$TruncateAt;
    //   1415: if_acmpeq -> 1424
    //   1418: iconst_1
    //   1419: istore #24
    //   1421: goto -> 1427
    //   1424: iconst_0
    //   1425: istore #24
    //   1427: iload #39
    //   1429: ifle -> 1612
    //   1432: iload #39
    //   1434: iload #36
    //   1436: if_icmpge -> 1612
    //   1439: iload #24
    //   1441: ifeq -> 1612
    //   1444: fconst_0
    //   1445: fstore #51
    //   1447: iconst_0
    //   1448: istore #34
    //   1450: iload #39
    //   1452: iconst_1
    //   1453: isub
    //   1454: istore #52
    //   1456: iload #52
    //   1458: iload #36
    //   1460: if_icmpge -> 1573
    //   1463: iload #52
    //   1465: iload #36
    //   1467: iconst_1
    //   1468: isub
    //   1469: if_icmpne -> 1485
    //   1472: fload #51
    //   1474: aload #19
    //   1476: iload #52
    //   1478: faload
    //   1479: fadd
    //   1480: fstore #51
    //   1482: goto -> 1557
    //   1485: iload #52
    //   1487: ifne -> 1500
    //   1490: iconst_0
    //   1491: istore #24
    //   1493: fload #51
    //   1495: fstore #53
    //   1497: goto -> 1513
    //   1500: aload #18
    //   1502: iload #52
    //   1504: iconst_1
    //   1505: isub
    //   1506: iaload
    //   1507: istore #24
    //   1509: fload #51
    //   1511: fstore #53
    //   1513: aload #31
    //   1515: astore #10
    //   1517: fload #53
    //   1519: fstore #51
    //   1521: aload #10
    //   1523: astore #31
    //   1525: iload #24
    //   1527: aload #18
    //   1529: iload #52
    //   1531: iaload
    //   1532: if_icmpge -> 1557
    //   1535: fload #53
    //   1537: aload #44
    //   1539: iload #24
    //   1541: invokevirtual getCharWidthAt : (I)F
    //   1544: fadd
    //   1545: fstore #53
    //   1547: iinc #24, 1
    //   1550: aload #10
    //   1552: astore #31
    //   1554: goto -> 1513
    //   1557: iload #34
    //   1559: aload #22
    //   1561: iload #52
    //   1563: baload
    //   1564: ior
    //   1565: istore #34
    //   1567: iinc #52, 1
    //   1570: goto -> 1456
    //   1573: aload #18
    //   1575: iload #39
    //   1577: iconst_1
    //   1578: isub
    //   1579: aload #18
    //   1581: iload #36
    //   1583: iconst_1
    //   1584: isub
    //   1585: iaload
    //   1586: iastore
    //   1587: aload #19
    //   1589: iload #39
    //   1591: iconst_1
    //   1592: isub
    //   1593: fload #51
    //   1595: fastore
    //   1596: aload #22
    //   1598: iload #39
    //   1600: iconst_1
    //   1601: isub
    //   1602: iload #34
    //   1604: bastore
    //   1605: iload #39
    //   1607: istore #36
    //   1609: goto -> 1612
    //   1612: aload #49
    //   1614: astore #31
    //   1616: iload #29
    //   1618: istore #54
    //   1620: iconst_0
    //   1621: istore #55
    //   1623: iconst_0
    //   1624: istore #52
    //   1626: iconst_0
    //   1627: istore #56
    //   1629: iconst_0
    //   1630: istore #57
    //   1632: iconst_0
    //   1633: istore #58
    //   1635: iconst_0
    //   1636: istore #59
    //   1638: iconst_0
    //   1639: istore #60
    //   1641: aload #32
    //   1643: astore #49
    //   1645: iload #29
    //   1647: istore #61
    //   1649: aload #27
    //   1651: astore #10
    //   1653: aload #42
    //   1655: astore #35
    //   1657: iload #7
    //   1659: istore #24
    //   1661: aload #31
    //   1663: astore #32
    //   1665: aload #43
    //   1667: astore #27
    //   1669: aload #33
    //   1671: astore #31
    //   1673: iload #25
    //   1675: istore #7
    //   1677: iload #60
    //   1679: istore #25
    //   1681: iload #41
    //   1683: istore #62
    //   1685: aload #44
    //   1687: astore #42
    //   1689: iload #6
    //   1691: istore #60
    //   1693: iload #36
    //   1695: istore #41
    //   1697: iload #39
    //   1699: istore #36
    //   1701: iload #52
    //   1703: istore #6
    //   1705: iload #29
    //   1707: istore #39
    //   1709: iload #38
    //   1711: istore #52
    //   1713: iload #54
    //   1715: istore #29
    //   1717: iload #61
    //   1719: iload #52
    //   1721: if_icmpge -> 2376
    //   1724: aload #46
    //   1726: iload #59
    //   1728: iaload
    //   1729: istore #63
    //   1731: iconst_0
    //   1732: istore #64
    //   1734: aload #47
    //   1736: iload #58
    //   1738: iconst_4
    //   1739: imul
    //   1740: iconst_0
    //   1741: iadd
    //   1742: iaload
    //   1743: istore #54
    //   1745: aload #10
    //   1747: astore #33
    //   1749: aload #33
    //   1751: iload #54
    //   1753: putfield top : I
    //   1756: iconst_1
    //   1757: istore #65
    //   1759: aload #33
    //   1761: aload #47
    //   1763: iload #58
    //   1765: iconst_4
    //   1766: imul
    //   1767: iconst_1
    //   1768: iadd
    //   1769: iaload
    //   1770: putfield bottom : I
    //   1773: iconst_2
    //   1774: istore #66
    //   1776: aload #33
    //   1778: aload #47
    //   1780: iload #58
    //   1782: iconst_4
    //   1783: imul
    //   1784: iconst_2
    //   1785: iadd
    //   1786: iaload
    //   1787: putfield ascent : I
    //   1790: aload #33
    //   1792: aload #47
    //   1794: iload #58
    //   1796: iconst_4
    //   1797: imul
    //   1798: iconst_3
    //   1799: iadd
    //   1800: iaload
    //   1801: putfield descent : I
    //   1804: iload #55
    //   1806: istore #54
    //   1808: aload #33
    //   1810: getfield top : I
    //   1813: iload #55
    //   1815: if_icmpge -> 1825
    //   1818: aload #33
    //   1820: getfield top : I
    //   1823: istore #54
    //   1825: iload #56
    //   1827: istore #55
    //   1829: aload #33
    //   1831: getfield ascent : I
    //   1834: iload #56
    //   1836: if_icmpge -> 1846
    //   1839: aload #33
    //   1841: getfield ascent : I
    //   1844: istore #55
    //   1846: iload #57
    //   1848: istore #56
    //   1850: aload #33
    //   1852: getfield descent : I
    //   1855: iload #57
    //   1857: if_icmple -> 1867
    //   1860: aload #33
    //   1862: getfield descent : I
    //   1865: istore #56
    //   1867: aload #33
    //   1869: getfield bottom : I
    //   1872: iload #6
    //   1874: if_icmple -> 1895
    //   1877: aload #33
    //   1879: getfield bottom : I
    //   1882: istore #57
    //   1884: iload #25
    //   1886: istore #6
    //   1888: iload #57
    //   1890: istore #25
    //   1892: goto -> 1907
    //   1895: iload #25
    //   1897: istore #57
    //   1899: iload #6
    //   1901: istore #25
    //   1903: iload #57
    //   1905: istore #6
    //   1907: iload #6
    //   1909: iload #41
    //   1911: if_icmpge -> 1933
    //   1914: iload #39
    //   1916: aload #18
    //   1918: iload #6
    //   1920: iaload
    //   1921: iadd
    //   1922: iload #61
    //   1924: if_icmpge -> 1933
    //   1927: iinc #6, 1
    //   1930: goto -> 1907
    //   1933: iload #56
    //   1935: istore #67
    //   1937: iload #29
    //   1939: istore #57
    //   1941: iload #25
    //   1943: istore #38
    //   1945: iload #55
    //   1947: istore #29
    //   1949: iload #7
    //   1951: istore #56
    //   1953: iload #54
    //   1955: istore #55
    //   1957: iload #57
    //   1959: istore #54
    //   1961: iload #24
    //   1963: istore #57
    //   1965: aload #31
    //   1967: astore #10
    //   1969: iload #48
    //   1971: istore #7
    //   1973: iload #64
    //   1975: istore #24
    //   1977: iload #65
    //   1979: istore #48
    //   1981: iload #61
    //   1983: istore #64
    //   1985: iload #63
    //   1987: istore #61
    //   1989: iload #30
    //   1991: istore #25
    //   1993: aload #33
    //   1995: astore #31
    //   1997: iload #67
    //   1999: istore #30
    //   2001: iload #6
    //   2003: iload #41
    //   2005: if_icmpge -> 2296
    //   2008: aload #18
    //   2010: iload #6
    //   2012: iaload
    //   2013: iload #39
    //   2015: iadd
    //   2016: iload #61
    //   2018: if_icmpgt -> 2296
    //   2021: iload #39
    //   2023: aload #18
    //   2025: iload #6
    //   2027: iaload
    //   2028: iadd
    //   2029: istore #63
    //   2031: iload #63
    //   2033: iload #25
    //   2035: if_icmpge -> 2045
    //   2038: iload #48
    //   2040: istore #67
    //   2042: goto -> 2049
    //   2045: iload #24
    //   2047: istore #67
    //   2049: iload #11
    //   2051: ifeq -> 2072
    //   2054: iload #29
    //   2056: aload #20
    //   2058: iload #6
    //   2060: faload
    //   2061: invokestatic round : (F)I
    //   2064: invokestatic min : (II)I
    //   2067: istore #29
    //   2069: goto -> 2072
    //   2072: iload #11
    //   2074: ifeq -> 2095
    //   2077: iload #30
    //   2079: aload #21
    //   2081: iload #6
    //   2083: faload
    //   2084: invokestatic round : (F)I
    //   2087: invokestatic max : (II)I
    //   2090: istore #30
    //   2092: goto -> 2095
    //   2095: aload #4
    //   2097: getfield mTextJustificationHooksImpl : Landroid/text/ITextJustificationHooks;
    //   2100: aload_0
    //   2101: aload_1
    //   2102: iload #67
    //   2104: aload #35
    //   2106: iload #63
    //   2108: invokeinterface getLayoutParaSpacingAdded : (Landroid/text/StaticLayout;Ljava/lang/Object;ZLjava/lang/CharSequence;I)F
    //   2113: fstore #51
    //   2115: iload #38
    //   2117: i2f
    //   2118: fload #51
    //   2120: fadd
    //   2121: f2i
    //   2122: istore #38
    //   2124: iload #30
    //   2126: i2f
    //   2127: fload #51
    //   2129: fadd
    //   2130: f2i
    //   2131: istore #30
    //   2133: aload_0
    //   2134: aload #35
    //   2136: iload #54
    //   2138: iload #63
    //   2140: iload #29
    //   2142: iload #30
    //   2144: iload #55
    //   2146: iload #38
    //   2148: iload #56
    //   2150: fload #12
    //   2152: fload #13
    //   2154: aload #5
    //   2156: aload #28
    //   2158: aload #31
    //   2160: aload #22
    //   2162: iload #6
    //   2164: baload
    //   2165: aload #23
    //   2167: iload #6
    //   2169: iaload
    //   2170: iload #26
    //   2172: aload #42
    //   2174: iload #25
    //   2176: iload_2
    //   2177: iload_3
    //   2178: iload #16
    //   2180: aload #45
    //   2182: iload #39
    //   2184: aload #15
    //   2186: fload #14
    //   2188: aload #19
    //   2190: iload #6
    //   2192: faload
    //   2193: aload #8
    //   2195: iload #67
    //   2197: invokespecial out : (Ljava/lang/CharSequence;IIIIIIIFF[Landroid/text/style/LineHeightSpan;[ILandroid/graphics/Paint$FontMetricsInt;ZIZLandroid/text/MeasuredParagraph;IZZZ[CILandroid/text/TextUtils$TruncateAt;FFLandroid/text/TextPaint;Z)I
    //   2200: istore #67
    //   2202: iload #63
    //   2204: iload #61
    //   2206: if_icmpge -> 2240
    //   2209: aload #31
    //   2211: getfield top : I
    //   2214: istore #55
    //   2216: aload #31
    //   2218: getfield bottom : I
    //   2221: istore #56
    //   2223: aload #31
    //   2225: getfield ascent : I
    //   2228: istore #29
    //   2230: aload #31
    //   2232: getfield descent : I
    //   2235: istore #30
    //   2237: goto -> 2256
    //   2240: iload #24
    //   2242: istore #29
    //   2244: iload #24
    //   2246: istore #56
    //   2248: iload #24
    //   2250: istore #55
    //   2252: iload #24
    //   2254: istore #30
    //   2256: iload #63
    //   2258: istore #54
    //   2260: aload #4
    //   2262: getfield mLineCount : I
    //   2265: aload #4
    //   2267: getfield mMaximumVisibleLineCount : I
    //   2270: if_icmplt -> 2282
    //   2273: aload #4
    //   2275: getfield mEllipsized : Z
    //   2278: ifeq -> 2282
    //   2281: return
    //   2282: iinc #6, 1
    //   2285: iload #56
    //   2287: istore #38
    //   2289: iload #67
    //   2291: istore #56
    //   2293: goto -> 2001
    //   2296: iload #57
    //   2298: istore #24
    //   2300: iinc #59, 1
    //   2303: iload #58
    //   2305: iconst_1
    //   2306: iadd
    //   2307: istore #66
    //   2309: iload #54
    //   2311: istore #58
    //   2313: iload #56
    //   2315: istore #48
    //   2317: iload #6
    //   2319: istore #54
    //   2321: iload #30
    //   2323: istore #57
    //   2325: iload #29
    //   2327: istore #56
    //   2329: aload #10
    //   2331: astore #33
    //   2333: iload #25
    //   2335: istore #30
    //   2337: iload #7
    //   2339: istore #64
    //   2341: aload #31
    //   2343: astore #10
    //   2345: iload #58
    //   2347: istore #29
    //   2349: iload #38
    //   2351: istore #6
    //   2353: iload #66
    //   2355: istore #58
    //   2357: iload #54
    //   2359: istore #25
    //   2361: iload #48
    //   2363: istore #7
    //   2365: iload #64
    //   2367: istore #48
    //   2369: aload #33
    //   2371: astore #31
    //   2373: goto -> 1717
    //   2376: iload #52
    //   2378: iload #30
    //   2380: if_icmpne -> 2390
    //   2383: iload #7
    //   2385: istore #29
    //   2387: goto -> 2453
    //   2390: iload #40
    //   2392: iconst_1
    //   2393: iadd
    //   2394: istore #36
    //   2396: aload #10
    //   2398: astore #5
    //   2400: aload #31
    //   2402: astore #10
    //   2404: aload #35
    //   2406: astore #31
    //   2408: aload #10
    //   2410: astore #35
    //   2412: aload #27
    //   2414: astore #10
    //   2416: iload #7
    //   2418: istore #25
    //   2420: aload #50
    //   2422: astore #33
    //   2424: aload #5
    //   2426: astore #27
    //   2428: aload #49
    //   2430: astore #32
    //   2432: goto -> 651
    //   2435: aload #27
    //   2437: astore_1
    //   2438: iload #25
    //   2440: istore #29
    //   2442: aload #31
    //   2444: astore #35
    //   2446: aload #10
    //   2448: astore #27
    //   2450: aload_1
    //   2451: astore #10
    //   2453: iload #30
    //   2455: iload #24
    //   2457: if_icmpeq -> 2482
    //   2460: aload #35
    //   2462: iload #30
    //   2464: iconst_1
    //   2465: isub
    //   2466: invokeinterface charAt : (I)C
    //   2471: bipush #10
    //   2473: if_icmpne -> 2479
    //   2476: goto -> 2482
    //   2479: goto -> 2582
    //   2482: aload #4
    //   2484: getfield mLineCount : I
    //   2487: aload #4
    //   2489: getfield mMaximumVisibleLineCount : I
    //   2492: if_icmpge -> 2582
    //   2495: aload #35
    //   2497: iload #30
    //   2499: iload #30
    //   2501: aload #27
    //   2503: aconst_null
    //   2504: invokestatic buildForBidi : (Ljava/lang/CharSequence;IILandroid/text/TextDirectionHeuristic;Landroid/text/MeasuredParagraph;)Landroid/text/MeasuredParagraph;
    //   2507: astore_1
    //   2508: aload #8
    //   2510: aload #10
    //   2512: invokevirtual getFontMetricsInt : (Landroid/graphics/Paint$FontMetricsInt;)I
    //   2515: pop
    //   2516: aload_0
    //   2517: aload #35
    //   2519: iload #30
    //   2521: iload #30
    //   2523: aload #10
    //   2525: getfield ascent : I
    //   2528: aload #10
    //   2530: getfield descent : I
    //   2533: aload #10
    //   2535: getfield top : I
    //   2538: aload #10
    //   2540: getfield bottom : I
    //   2543: iload #29
    //   2545: fload #12
    //   2547: fload #13
    //   2549: aconst_null
    //   2550: aconst_null
    //   2551: aload #10
    //   2553: iconst_0
    //   2554: iconst_0
    //   2555: iload #26
    //   2557: aload_1
    //   2558: iload #30
    //   2560: iload_2
    //   2561: iload_3
    //   2562: iload #16
    //   2564: aconst_null
    //   2565: iload #24
    //   2567: aload #15
    //   2569: fload #14
    //   2571: fconst_0
    //   2572: aload #8
    //   2574: iconst_0
    //   2575: invokespecial out : (Ljava/lang/CharSequence;IIIIIIIFF[Landroid/text/style/LineHeightSpan;[ILandroid/graphics/Paint$FontMetricsInt;ZIZLandroid/text/MeasuredParagraph;IZZZ[CILandroid/text/TextUtils$TruncateAt;FFLandroid/text/TextPaint;Z)I
    //   2578: pop
    //   2579: goto -> 2582
    //   2582: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #617	-> 0
    //   #618	-> 9
    //   #619	-> 15
    //   #620	-> 21
    //   #621	-> 27
    //   #622	-> 33
    //   #623	-> 39
    //   #624	-> 45
    //   #625	-> 51
    //   #626	-> 57
    //   #627	-> 64
    //   #628	-> 70
    //   #630	-> 76
    //   #631	-> 79
    //   #632	-> 82
    //   #633	-> 85
    //   #634	-> 88
    //   #635	-> 91
    //   #636	-> 94
    //   #639	-> 97
    //   #640	-> 105
    //   #644	-> 126
    //   #645	-> 132
    //   #646	-> 138
    //   #648	-> 163
    //   #649	-> 166
    //   #651	-> 192
    //   #652	-> 198
    //   #655	-> 198
    //   #667	-> 217
    //   #656	-> 223
    //   #657	-> 246
    //   #658	-> 269
    //   #659	-> 278
    //   #660	-> 284
    //   #661	-> 294
    //   #660	-> 307
    //   #663	-> 313
    //   #664	-> 323
    //   #663	-> 342
    //   #666	-> 348
    //   #670	-> 348
    //   #671	-> 357
    //   #672	-> 368
    //   #674	-> 379
    //   #675	-> 390
    //   #676	-> 399
    //   #678	-> 406
    //   #681	-> 415
    //   #682	-> 418
    //   #683	-> 439
    //   #684	-> 451
    //   #685	-> 458
    //   #687	-> 458
    //   #686	-> 470
    //   #688	-> 489
    //   #703	-> 504
    //   #692	-> 514
    //   #694	-> 525
    //   #695	-> 536
    //   #696	-> 547
    //   #697	-> 556
    //   #698	-> 563
    //   #699	-> 572
    //   #700	-> 579
    //   #683	-> 582
    //   #708	-> 582
    //   #709	-> 587
    //   #710	-> 587
    //   #711	-> 608
    //   #708	-> 625
    //   #715	-> 629
    //   #716	-> 659
    //   #717	-> 664
    //   #718	-> 683
    //   #720	-> 693
    //   #721	-> 696
    //   #722	-> 700
    //   #724	-> 704
    //   #725	-> 704
    //   #726	-> 709
    //   #728	-> 726
    //   #729	-> 745
    //   #730	-> 752
    //   #731	-> 768
    //   #735	-> 784
    //   #736	-> 796
    //   #737	-> 803
    //   #738	-> 803
    //   #737	-> 812
    //   #728	-> 821
    //   #742	-> 843
    //   #744	-> 860
    //   #745	-> 866
    //   #747	-> 884
    //   #748	-> 902
    //   #751	-> 910
    //   #752	-> 921
    //   #754	-> 935
    //   #758	-> 942
    //   #762	-> 962
    //   #751	-> 969
    //   #725	-> 994
    //   #768	-> 1020
    //   #769	-> 1027
    //   #770	-> 1032
    //   #772	-> 1049
    //   #773	-> 1055
    //   #774	-> 1062
    //   #775	-> 1073
    //   #774	-> 1089
    //   #777	-> 1095
    //   #778	-> 1104
    //   #772	-> 1107
    //   #769	-> 1110
    //   #782	-> 1110
    //   #783	-> 1123
    //   #784	-> 1130
    //   #785	-> 1140
    //   #787	-> 1150
    //   #788	-> 1158
    //   #789	-> 1168
    //   #791	-> 1177
    //   #792	-> 1177
    //   #791	-> 1195
    //   #793	-> 1212
    //   #794	-> 1219
    //   #795	-> 1226
    //   #796	-> 1226
    //   #797	-> 1232
    //   #798	-> 1238
    //   #799	-> 1244
    //   #800	-> 1250
    //   #801	-> 1256
    //   #794	-> 1269
    //   #804	-> 1269
    //   #805	-> 1283
    //   #806	-> 1295
    //   #807	-> 1307
    //   #808	-> 1319
    //   #809	-> 1331
    //   #810	-> 1343
    //   #811	-> 1343
    //   #804	-> 1365
    //   #814	-> 1375
    //   #815	-> 1388
    //   #819	-> 1427
    //   #822	-> 1444
    //   #823	-> 1447
    //   #824	-> 1450
    //   #825	-> 1463
    //   #826	-> 1472
    //   #828	-> 1485
    //   #829	-> 1535
    //   #828	-> 1547
    //   #832	-> 1557
    //   #824	-> 1567
    //   #835	-> 1573
    //   #836	-> 1587
    //   #837	-> 1596
    //   #839	-> 1605
    //   #819	-> 1612
    //   #844	-> 1612
    //   #846	-> 1620
    //   #847	-> 1632
    //   #848	-> 1635
    //   #849	-> 1638
    //   #850	-> 1641
    //   #852	-> 1724
    //   #855	-> 1731
    //   #856	-> 1756
    //   #857	-> 1773
    //   #858	-> 1790
    //   #859	-> 1804
    //   #861	-> 1804
    //   #862	-> 1818
    //   #864	-> 1825
    //   #865	-> 1839
    //   #867	-> 1846
    //   #868	-> 1860
    //   #870	-> 1867
    //   #871	-> 1877
    //   #870	-> 1895
    //   #875	-> 1907
    //   #876	-> 1927
    //   #875	-> 1933
    //   #879	-> 1933
    //   #880	-> 2021
    //   #882	-> 2031
    //   #884	-> 2049
    //   #885	-> 2054
    //   #886	-> 2072
    //   #892	-> 2072
    //   #894	-> 2077
    //   #895	-> 2095
    //   #898	-> 2095
    //   #899	-> 2115
    //   #900	-> 2124
    //   #903	-> 2133
    //   #911	-> 2202
    //   #913	-> 2209
    //   #914	-> 2216
    //   #915	-> 2223
    //   #916	-> 2230
    //   #918	-> 2240
    //   #921	-> 2256
    //   #922	-> 2260
    //   #924	-> 2260
    //   #925	-> 2281
    //   #927	-> 2282
    //   #879	-> 2296
    //   #850	-> 2296
    //   #930	-> 2376
    //   #931	-> 2383
    //   #715	-> 2390
    //   #935	-> 2453
    //   #937	-> 2495
    //   #938	-> 2495
    //   #939	-> 2508
    //   #940	-> 2516
    //   #935	-> 2582
    //   #951	-> 2582
  }
  
  private int out(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, float paramFloat1, float paramFloat2, LineHeightSpan[] paramArrayOfLineHeightSpan, int[] paramArrayOfint, Paint.FontMetricsInt paramFontMetricsInt, boolean paramBoolean1, int paramInt8, boolean paramBoolean2, MeasuredParagraph paramMeasuredParagraph, int paramInt9, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, char[] paramArrayOfchar, int paramInt10, TextUtils.TruncateAt paramTruncateAt, float paramFloat3, float paramFloat4, TextPaint paramTextPaint, boolean paramBoolean6) {
    int[] arrayOfInt1;
    boolean bool1;
    int i = this.mLineCount;
    int j = this.mColumns, k = i * j;
    int m = 1;
    j = k + j + 1;
    int[] arrayOfInt2 = this.mLines;
    int n = paramMeasuredParagraph.getParagraphDir();
    if (j >= arrayOfInt2.length) {
      arrayOfInt1 = ArrayUtils.newUnpaddedIntArray(GrowingArrayUtils.growSize(j));
      System.arraycopy(arrayOfInt2, 0, arrayOfInt1, 0, arrayOfInt2.length);
      this.mLines = arrayOfInt1;
    } else {
      arrayOfInt1 = arrayOfInt2;
    } 
    if (i >= this.mLineDirections.length) {
      bool1 = GrowingArrayUtils.growSize(i);
      Layout.Directions[] arrayOfDirections2 = (Layout.Directions[])ArrayUtils.newUnpaddedArray(Layout.Directions.class, bool1);
      Layout.Directions[] arrayOfDirections1 = this.mLineDirections;
      System.arraycopy(arrayOfDirections1, 0, arrayOfDirections2, 0, arrayOfDirections1.length);
      this.mLineDirections = arrayOfDirections2;
    } 
    if (paramArrayOfLineHeightSpan != null) {
      paramFontMetricsInt.ascent = paramInt3;
      paramFontMetricsInt.descent = paramInt4;
      paramFontMetricsInt.top = paramInt5;
      paramFontMetricsInt.bottom = paramInt6;
      for (paramInt6 = 0, paramInt3 = i, paramInt4 = m, paramInt5 = j; paramInt6 < paramArrayOfLineHeightSpan.length; paramInt6++) {
        if (paramArrayOfLineHeightSpan[paramInt6] instanceof LineHeightSpan.WithDensity) {
          LineHeightSpan.WithDensity withDensity = (LineHeightSpan.WithDensity)paramArrayOfLineHeightSpan[paramInt6];
          m = paramArrayOfint[paramInt6];
          withDensity.chooseHeight(paramCharSequence, paramInt1, paramInt2, m, paramInt7, paramFontMetricsInt, paramTextPaint);
        } else {
          paramArrayOfLineHeightSpan[paramInt6].chooseHeight(paramCharSequence, paramInt1, paramInt2, paramArrayOfint[paramInt6], paramInt7, paramFontMetricsInt);
        } 
      } 
      paramInt5 = paramInt4;
      i = paramInt3;
      paramInt6 = paramFontMetricsInt.ascent;
      j = paramFontMetricsInt.descent;
      m = paramFontMetricsInt.top;
      paramInt4 = paramFontMetricsInt.bottom;
      paramInt3 = paramInt5;
      paramInt5 = j;
    } else {
      bool1 = true;
      j = paramInt4;
      paramInt4 = paramInt6;
      m = paramInt5;
      paramInt5 = j;
      paramInt6 = paramInt3;
      paramInt3 = bool1;
    } 
    boolean bool2 = false;
    if (i == 0) {
      bool1 = paramInt3;
    } else {
      bool1 = false;
    } 
    if (i + 1 == this.mMaximumVisibleLineCount) {
      j = paramInt3;
    } else {
      j = 0;
    } 
    if (paramTruncateAt != null) {
      boolean bool;
      if (paramBoolean6 && this.mLineCount + paramInt3 == this.mMaximumVisibleLineCount) {
        bool = paramInt3;
      } else {
        bool = false;
      } 
      if ((((this.mMaximumVisibleLineCount != paramInt3 || !paramBoolean6) && (!bool1 || paramBoolean6)) || paramTruncateAt == TextUtils.TruncateAt.MARQUEE) && (bool1 || (j == 0 && paramBoolean6) || paramTruncateAt != TextUtils.TruncateAt.END))
        paramInt3 = 0; 
      if (paramInt3 != 0)
        calculateEllipsis(paramInt1, paramInt2, paramMeasuredParagraph, paramInt10, paramFloat3, paramTruncateAt, i, paramFloat4, paramTextPaint, bool); 
    } 
    if (this.mEllipsized) {
      paramBoolean6 = true;
    } else {
      if (paramInt10 != paramInt9 && paramInt9 > 0 && paramCharSequence.charAt(paramInt9 - 1) == '\n') {
        paramInt3 = 1;
      } else {
        paramInt3 = 0;
      } 
      if (paramInt2 == paramInt9 && paramInt3 == 0) {
        paramBoolean6 = true;
      } else if (paramInt1 == paramInt9 && paramInt3 != 0) {
        paramBoolean6 = true;
      } else {
        paramBoolean6 = false;
      } 
    } 
    paramInt9 = paramInt6;
    if (this.mTextJustificationHooksImpl.lineShouldIncludeFontPad(bool1, this)) {
      if (paramBoolean4)
        this.mTopPadding = m - paramInt6; 
      paramInt9 = paramInt6;
      if (paramBoolean3)
        paramInt9 = m; 
    } 
    paramInt3 = paramInt5;
    if (paramBoolean6) {
      if (paramBoolean4)
        this.mBottomPadding = paramInt4 - paramInt5; 
      paramInt3 = paramInt5;
      if (paramBoolean3)
        paramInt3 = paramInt4; 
    } 
    if (this.mTextJustificationHooksImpl.lineNeedMultiply(paramBoolean2, paramBoolean5, paramBoolean6, this)) {
      double d = ((paramInt3 - paramInt9) * (paramFloat1 - 1.0F) + paramFloat2);
      if (d >= 0.0D) {
        paramInt5 = (int)(0.5D + d);
      } else {
        paramInt5 = -((int)(-d + 0.5D));
      } 
    } else {
      paramInt5 = 0;
    } 
    arrayOfInt1[k + 0] = paramInt1;
    arrayOfInt1[k + 1] = paramInt7;
    arrayOfInt1[k + 2] = paramInt3 + paramInt5;
    arrayOfInt1[k + 3] = paramInt5;
    if (!this.mEllipsized && j != 0) {
      if (!paramBoolean3)
        paramInt4 = paramInt3; 
      this.mMaxLineHeight = paramInt7 + paramInt4 - paramInt9;
    } 
    paramInt4 = paramInt7 + paramInt3 - paramInt9 + paramInt5;
    paramInt3 = this.mColumns;
    arrayOfInt1[k + paramInt3 + 0] = paramInt2;
    arrayOfInt1[k + paramInt3 + 1] = paramInt4;
    paramInt6 = k + 0;
    paramInt5 = arrayOfInt1[paramInt6];
    paramInt3 = bool2;
    if (paramBoolean1)
      paramInt3 = 536870912; 
    arrayOfInt1[paramInt6] = paramInt5 | paramInt3;
    arrayOfInt1[k + 4] = paramInt8;
    paramInt3 = k + 0;
    arrayOfInt1[paramInt3] = arrayOfInt1[paramInt3] | n << 30;
    this.mLineDirections[i] = paramMeasuredParagraph.getDirections(paramInt1 - paramInt10, paramInt2 - paramInt10);
    this.mLineCount++;
    return paramInt4;
  }
  
  private void calculateEllipsis(int paramInt1, int paramInt2, MeasuredParagraph paramMeasuredParagraph, int paramInt3, float paramFloat1, TextUtils.TruncateAt paramTruncateAt, int paramInt4, float paramFloat2, TextPaint paramTextPaint, boolean paramBoolean) {
    float f1 = paramFloat1 - getTotalInsets(paramInt4);
    if (paramFloat2 <= f1 && !paramBoolean) {
      arrayOfInt = this.mLines;
      paramInt1 = this.mColumns;
      arrayOfInt[paramInt1 * paramInt4 + 5] = 0;
      arrayOfInt[paramInt1 * paramInt4 + 6] = 0;
      return;
    } 
    float f2 = paramTextPaint.measureText(TextUtils.getEllipsisString(paramTruncateAt));
    int i = 0;
    boolean bool = false;
    int j = paramInt2 - paramInt1;
    if (paramTruncateAt == TextUtils.TruncateAt.START) {
      if (this.mMaximumVisibleLineCount == 1) {
        paramFloat1 = 0.0F;
        paramInt2 = j;
        while (true) {
          i = paramInt2;
          if (paramInt2 > 0) {
            paramFloat2 = arrayOfInt.getCharWidthAt(paramInt2 - 1 + paramInt1 - paramInt3);
            if (paramFloat2 + paramFloat1 + f2 > f1) {
              while (true) {
                i = paramInt2;
                if (paramInt2 < j) {
                  i = paramInt2;
                  if (arrayOfInt.getCharWidthAt(paramInt2 + paramInt1 - paramInt3) == 0.0F) {
                    paramInt2++;
                    continue;
                  } 
                } 
                break;
              } 
              break;
            } 
            paramFloat1 += paramFloat2;
            paramInt2--;
            continue;
          } 
          break;
        } 
        paramInt1 = 0;
        paramInt2 = i;
      } else {
        paramInt1 = i;
        paramInt2 = bool;
        if (Log.isLoggable("StaticLayout", 5)) {
          Log.w("StaticLayout", "Start Ellipsis only supported with one line");
          paramInt1 = i;
          paramInt2 = bool;
        } 
      } 
    } else if (paramTruncateAt == TextUtils.TruncateAt.END || paramTruncateAt == TextUtils.TruncateAt.MARQUEE || paramTruncateAt == TextUtils.TruncateAt.END_SMALL) {
      paramFloat1 = 0.0F;
      for (paramInt2 = 0; paramInt2 < j; paramInt2++) {
        paramFloat2 = arrayOfInt.getCharWidthAt(paramInt2 + paramInt1 - paramInt3);
        if (paramFloat2 + paramFloat1 + f2 > f1)
          break; 
        paramFloat1 += paramFloat2;
      } 
      paramInt3 = paramInt2;
      i = j - paramInt2;
      paramInt1 = paramInt3;
      paramInt2 = i;
      if (paramBoolean) {
        paramInt1 = paramInt3;
        paramInt2 = i;
        if (i == 0) {
          paramInt1 = paramInt3;
          paramInt2 = i;
          if (j > 0) {
            paramInt1 = j - 1;
            paramInt2 = 1;
          } 
        } 
      } 
    } else if (this.mMaximumVisibleLineCount == 1) {
      paramFloat2 = 0.0F;
      paramFloat1 = 0.0F;
      paramInt2 = j;
      float f = (f1 - f2) / 2.0F;
      while (true) {
        i = paramInt2;
        if (paramInt2 > 0) {
          float f3 = arrayOfInt.getCharWidthAt(paramInt2 - 1 + paramInt1 - paramInt3);
          if (f3 + paramFloat1 > f) {
            while (true) {
              i = paramInt2;
              if (paramInt2 < j) {
                i = paramInt2;
                if (arrayOfInt.getCharWidthAt(paramInt2 + paramInt1 - paramInt3) == 0.0F) {
                  paramInt2++;
                  continue;
                } 
              } 
              break;
            } 
            break;
          } 
          paramFloat1 += f3;
          paramInt2--;
          continue;
        } 
        break;
      } 
      for (paramInt2 = 0; paramInt2 < i; paramInt2++) {
        f = arrayOfInt.getCharWidthAt(paramInt2 + paramInt1 - paramInt3);
        if (f + paramFloat2 > f1 - f2 - paramFloat1)
          break; 
        paramFloat2 += f;
      } 
      paramInt1 = paramInt2;
      paramInt2 = i - paramInt2;
    } else {
      paramInt1 = i;
      paramInt2 = bool;
      if (Log.isLoggable("StaticLayout", 5)) {
        Log.w("StaticLayout", "Middle Ellipsis only supported with one line");
        paramInt1 = i;
        paramInt2 = bool;
      } 
    } 
    this.mEllipsized = true;
    int[] arrayOfInt = this.mLines;
    paramInt3 = this.mColumns;
    arrayOfInt[paramInt3 * paramInt4 + 5] = paramInt1;
    arrayOfInt[paramInt3 * paramInt4 + 6] = paramInt2;
  }
  
  private float getTotalInsets(int paramInt) {
    int i = 0;
    int[] arrayOfInt = this.mLeftIndents;
    if (arrayOfInt != null)
      i = arrayOfInt[Math.min(paramInt, arrayOfInt.length - 1)]; 
    arrayOfInt = this.mRightIndents;
    int j = i;
    if (arrayOfInt != null)
      j = i + arrayOfInt[Math.min(paramInt, arrayOfInt.length - 1)]; 
    return j;
  }
  
  public int getLineForVertical(int paramInt) {
    int i = this.mLineCount;
    int j = -1;
    int[] arrayOfInt = this.mLines;
    while (i - j > 1) {
      int k = i + j >> 1;
      if (arrayOfInt[this.mColumns * k + 1] > paramInt) {
        i = k;
        continue;
      } 
      j = k;
    } 
    if (j < 0)
      return 0; 
    return j;
  }
  
  public int getLineCount() {
    return this.mLineCount;
  }
  
  public int getLineTop(int paramInt) {
    return this.mLines[this.mColumns * paramInt + 1];
  }
  
  public int getLineExtra(int paramInt) {
    return this.mLines[this.mColumns * paramInt + 3];
  }
  
  public int getLineDescent(int paramInt) {
    return this.mLines[this.mColumns * paramInt + 2];
  }
  
  public int getLineStart(int paramInt) {
    return this.mLines[this.mColumns * paramInt + 0] & 0x1FFFFFFF;
  }
  
  public int getParagraphDirection(int paramInt) {
    return this.mLines[this.mColumns * paramInt + 0] >> 30;
  }
  
  public boolean getLineContainsTab(int paramInt) {
    int arrayOfInt[] = this.mLines, i = this.mColumns;
    boolean bool = false;
    if ((arrayOfInt[i * paramInt + 0] & 0x20000000) != 0)
      bool = true; 
    return bool;
  }
  
  public final Layout.Directions getLineDirections(int paramInt) {
    if (paramInt <= getLineCount())
      return this.mLineDirections[paramInt]; 
    throw new ArrayIndexOutOfBoundsException();
  }
  
  public int getTopPadding() {
    return this.mTopPadding;
  }
  
  public int getBottomPadding() {
    return this.mBottomPadding;
  }
  
  static int packHyphenEdit(int paramInt1, int paramInt2) {
    return paramInt1 << 3 | paramInt2;
  }
  
  static int unpackStartHyphenEdit(int paramInt) {
    return (paramInt & 0x18) >> 3;
  }
  
  static int unpackEndHyphenEdit(int paramInt) {
    return paramInt & 0x7;
  }
  
  public int getStartHyphenEdit(int paramInt) {
    return unpackStartHyphenEdit(this.mLines[this.mColumns * paramInt + 4] & 0xFF);
  }
  
  public int getEndHyphenEdit(int paramInt) {
    return unpackEndHyphenEdit(this.mLines[this.mColumns * paramInt + 4] & 0xFF);
  }
  
  public int getIndentAdjust(int paramInt, Layout.Alignment paramAlignment) {
    int[] arrayOfInt;
    if (paramAlignment == Layout.Alignment.ALIGN_LEFT) {
      arrayOfInt = this.mLeftIndents;
      if (arrayOfInt == null)
        return 0; 
      return arrayOfInt[Math.min(paramInt, arrayOfInt.length - 1)];
    } 
    if (arrayOfInt == Layout.Alignment.ALIGN_RIGHT) {
      arrayOfInt = this.mRightIndents;
      if (arrayOfInt == null)
        return 0; 
      return -arrayOfInt[Math.min(paramInt, arrayOfInt.length - 1)];
    } 
    if (arrayOfInt == Layout.Alignment.ALIGN_CENTER) {
      int i = 0;
      arrayOfInt = this.mLeftIndents;
      if (arrayOfInt != null)
        i = arrayOfInt[Math.min(paramInt, arrayOfInt.length - 1)]; 
      int j = 0;
      arrayOfInt = this.mRightIndents;
      if (arrayOfInt != null)
        j = arrayOfInt[Math.min(paramInt, arrayOfInt.length - 1)]; 
      return i - j >> 1;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("unhandled alignment ");
    stringBuilder.append(arrayOfInt);
    throw new AssertionError(stringBuilder.toString());
  }
  
  public int getEllipsisCount(int paramInt) {
    int i = this.mColumns;
    if (i < 7)
      return 0; 
    return this.mLines[i * paramInt + 6];
  }
  
  public int getEllipsisStart(int paramInt) {
    int i = this.mColumns;
    if (i < 7)
      return 0; 
    return this.mLines[i * paramInt + 5];
  }
  
  public int getEllipsizedWidth() {
    return this.mEllipsizedWidth;
  }
  
  public int getHeight(boolean paramBoolean) {
    if (paramBoolean && this.mLineCount > this.mMaximumVisibleLineCount && this.mMaxLineHeight == -1 && Log.isLoggable("StaticLayout", 5)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("maxLineHeight should not be -1.  maxLines:");
      stringBuilder.append(this.mMaximumVisibleLineCount);
      stringBuilder.append(" lineCount:");
      stringBuilder.append(this.mLineCount);
      Log.w("StaticLayout", stringBuilder.toString());
    } 
    if (paramBoolean && this.mLineCount > this.mMaximumVisibleLineCount) {
      int i = this.mMaxLineHeight;
      if (i != -1)
        return i; 
    } 
    return getHeight();
  }
  
  class LineBreaks {
    private static final int INITIAL_SIZE = 16;
    
    public float[] ascents;
    
    public int[] breaks;
    
    public float[] descents;
    
    public int[] flags;
    
    public float[] widths;
    
    LineBreaks() {
      this.breaks = new int[16];
      this.widths = new float[16];
      this.ascents = new float[16];
      this.descents = new float[16];
      this.flags = new int[16];
    }
  }
}
