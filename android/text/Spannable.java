package android.text;

public interface Spannable extends Spanned {
  void removeSpan(Object paramObject);
  
  default void removeSpan(Object paramObject, int paramInt) {
    removeSpan(paramObject);
  }
  
  void setSpan(Object paramObject, int paramInt1, int paramInt2, int paramInt3);
  
  class Factory {
    private static Factory sInstance = new Factory();
    
    public static Factory getInstance() {
      return sInstance;
    }
    
    public Spannable newSpannable(CharSequence param1CharSequence) {
      return new SpannableString(param1CharSequence);
    }
  }
}
