package android.text;

import android.graphics.BaseCanvas;
import android.graphics.Paint;
import android.util.Log;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.GrowingArrayUtils;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.IdentityHashMap;
import libcore.util.EmptyArray;

public class SpannableStringBuilder implements CharSequence, GetChars, Spannable, Editable, Appendable, GraphicsOperations {
  private static final int END_MASK = 15;
  
  private static final int MARK = 1;
  
  public SpannableStringBuilder() {
    this("");
  }
  
  public SpannableStringBuilder(CharSequence paramCharSequence) {
    this(paramCharSequence, 0, paramCharSequence.length());
  }
  
  public SpannableStringBuilder(CharSequence paramCharSequence, int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial <init> : ()V
    //   4: aload_0
    //   5: getstatic android/text/SpannableStringBuilder.NO_FILTERS : [Landroid/text/InputFilter;
    //   8: putfield mFilters : [Landroid/text/InputFilter;
    //   11: iload_3
    //   12: iload_2
    //   13: isub
    //   14: istore #4
    //   16: iload #4
    //   18: iflt -> 292
    //   21: iload #4
    //   23: invokestatic growSize : (I)I
    //   26: invokestatic newUnpaddedCharArray : (I)[C
    //   29: astore #5
    //   31: aload_0
    //   32: aload #5
    //   34: putfield mText : [C
    //   37: aload_0
    //   38: iload #4
    //   40: putfield mGapStart : I
    //   43: aload_0
    //   44: aload #5
    //   46: arraylength
    //   47: iload #4
    //   49: isub
    //   50: putfield mGapLength : I
    //   53: aload_1
    //   54: iload_2
    //   55: iload_3
    //   56: aload #5
    //   58: iconst_0
    //   59: invokestatic getChars : (Ljava/lang/CharSequence;II[CI)V
    //   62: aload_0
    //   63: iconst_0
    //   64: putfield mSpanCount : I
    //   67: aload_0
    //   68: iconst_0
    //   69: putfield mSpanInsertCount : I
    //   72: aload_0
    //   73: getstatic libcore/util/EmptyArray.OBJECT : [Ljava/lang/Object;
    //   76: putfield mSpans : [Ljava/lang/Object;
    //   79: aload_0
    //   80: getstatic libcore/util/EmptyArray.INT : [I
    //   83: putfield mSpanStarts : [I
    //   86: aload_0
    //   87: getstatic libcore/util/EmptyArray.INT : [I
    //   90: putfield mSpanEnds : [I
    //   93: aload_0
    //   94: getstatic libcore/util/EmptyArray.INT : [I
    //   97: putfield mSpanFlags : [I
    //   100: aload_0
    //   101: getstatic libcore/util/EmptyArray.INT : [I
    //   104: putfield mSpanMax : [I
    //   107: aload_0
    //   108: getstatic libcore/util/EmptyArray.INT : [I
    //   111: putfield mSpanOrder : [I
    //   114: aload_1
    //   115: instanceof android/text/Spanned
    //   118: ifeq -> 291
    //   121: aload_1
    //   122: checkcast android/text/Spanned
    //   125: astore #5
    //   127: aload #5
    //   129: iload_2
    //   130: iload_3
    //   131: ldc java/lang/Object
    //   133: invokeinterface getSpans : (IILjava/lang/Class;)[Ljava/lang/Object;
    //   138: astore_1
    //   139: iconst_0
    //   140: istore #6
    //   142: iload #6
    //   144: aload_1
    //   145: arraylength
    //   146: if_icmpge -> 287
    //   149: aload_1
    //   150: iload #6
    //   152: aaload
    //   153: instanceof android/text/NoCopySpan
    //   156: ifeq -> 162
    //   159: goto -> 281
    //   162: aload #5
    //   164: aload_1
    //   165: iload #6
    //   167: aaload
    //   168: invokeinterface getSpanStart : (Ljava/lang/Object;)I
    //   173: iload_2
    //   174: isub
    //   175: istore #7
    //   177: aload #5
    //   179: aload_1
    //   180: iload #6
    //   182: aaload
    //   183: invokeinterface getSpanEnd : (Ljava/lang/Object;)I
    //   188: iload_2
    //   189: isub
    //   190: istore #8
    //   192: aload #5
    //   194: aload_1
    //   195: iload #6
    //   197: aaload
    //   198: invokeinterface getSpanFlags : (Ljava/lang/Object;)I
    //   203: istore #9
    //   205: iload #7
    //   207: istore #4
    //   209: iload #7
    //   211: ifge -> 217
    //   214: iconst_0
    //   215: istore #4
    //   217: iload #4
    //   219: iload_3
    //   220: iload_2
    //   221: isub
    //   222: if_icmple -> 233
    //   225: iload_3
    //   226: iload_2
    //   227: isub
    //   228: istore #7
    //   230: goto -> 237
    //   233: iload #4
    //   235: istore #7
    //   237: iload #8
    //   239: istore #4
    //   241: iload #8
    //   243: ifge -> 249
    //   246: iconst_0
    //   247: istore #4
    //   249: iload #4
    //   251: iload_3
    //   252: iload_2
    //   253: isub
    //   254: if_icmple -> 265
    //   257: iload_3
    //   258: iload_2
    //   259: isub
    //   260: istore #4
    //   262: goto -> 265
    //   265: aload_0
    //   266: iconst_0
    //   267: aload_1
    //   268: iload #6
    //   270: aaload
    //   271: iload #7
    //   273: iload #4
    //   275: iload #9
    //   277: iconst_0
    //   278: invokespecial setSpan : (ZLjava/lang/Object;IIIZ)V
    //   281: iinc #6, 1
    //   284: goto -> 142
    //   287: aload_0
    //   288: invokespecial restoreInvariants : ()V
    //   291: return
    //   292: new java/lang/StringIndexOutOfBoundsException
    //   295: dup
    //   296: invokespecial <init> : ()V
    //   299: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #59	-> 0
    //   #1778	-> 4
    //   #60	-> 11
    //   #62	-> 16
    //   #64	-> 21
    //   #65	-> 37
    //   #66	-> 43
    //   #68	-> 53
    //   #70	-> 62
    //   #71	-> 67
    //   #72	-> 72
    //   #73	-> 79
    //   #74	-> 86
    //   #75	-> 93
    //   #76	-> 100
    //   #77	-> 107
    //   #79	-> 114
    //   #80	-> 121
    //   #81	-> 127
    //   #83	-> 139
    //   #84	-> 149
    //   #85	-> 159
    //   #88	-> 162
    //   #89	-> 177
    //   #90	-> 192
    //   #92	-> 205
    //   #93	-> 214
    //   #94	-> 217
    //   #95	-> 225
    //   #94	-> 233
    //   #97	-> 237
    //   #98	-> 246
    //   #99	-> 249
    //   #100	-> 257
    //   #99	-> 265
    //   #102	-> 265
    //   #83	-> 281
    //   #104	-> 287
    //   #106	-> 291
    //   #62	-> 292
  }
  
  public static SpannableStringBuilder valueOf(CharSequence paramCharSequence) {
    if (paramCharSequence instanceof SpannableStringBuilder)
      return (SpannableStringBuilder)paramCharSequence; 
    return new SpannableStringBuilder(paramCharSequence);
  }
  
  public char charAt(int paramInt) {
    int i = length();
    if (paramInt >= 0) {
      if (paramInt < i) {
        if (paramInt >= this.mGapStart)
          return this.mText[this.mGapLength + paramInt]; 
        return this.mText[paramInt];
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("charAt: ");
      stringBuilder1.append(paramInt);
      stringBuilder1.append(" >= length ");
      stringBuilder1.append(i);
      throw new IndexOutOfBoundsException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("charAt: ");
    stringBuilder.append(paramInt);
    stringBuilder.append(" < 0");
    throw new IndexOutOfBoundsException(stringBuilder.toString());
  }
  
  public int length() {
    return this.mText.length - this.mGapLength;
  }
  
  private void resizeFor(int paramInt) {
    int i = this.mText.length;
    if (paramInt + 1 <= i)
      return; 
    char[] arrayOfChar = ArrayUtils.newUnpaddedCharArray(GrowingArrayUtils.growSize(paramInt));
    System.arraycopy(this.mText, 0, arrayOfChar, 0, this.mGapStart);
    int j = arrayOfChar.length;
    int k = j - i;
    paramInt = i - this.mGapStart + this.mGapLength;
    System.arraycopy(this.mText, i - paramInt, arrayOfChar, j - paramInt, paramInt);
    this.mText = arrayOfChar;
    this.mGapLength = paramInt = this.mGapLength + k;
    if (paramInt < 1)
      (new Exception("mGapLength < 1")).printStackTrace(); 
    if (this.mSpanCount != 0) {
      for (paramInt = 0; paramInt < this.mSpanCount; paramInt++) {
        int[] arrayOfInt = this.mSpanStarts;
        if (arrayOfInt[paramInt] > this.mGapStart)
          arrayOfInt[paramInt] = arrayOfInt[paramInt] + k; 
        arrayOfInt = this.mSpanEnds;
        if (arrayOfInt[paramInt] > this.mGapStart)
          arrayOfInt[paramInt] = arrayOfInt[paramInt] + k; 
      } 
      calcMax(treeRoot());
    } 
  }
  
  private void moveGapTo(int paramInt) {
    // Byte code:
    //   0: iload_1
    //   1: aload_0
    //   2: getfield mGapStart : I
    //   5: if_icmpne -> 9
    //   8: return
    //   9: iload_1
    //   10: aload_0
    //   11: invokevirtual length : ()I
    //   14: if_icmpne -> 22
    //   17: iconst_1
    //   18: istore_2
    //   19: goto -> 24
    //   22: iconst_0
    //   23: istore_2
    //   24: aload_0
    //   25: getfield mGapStart : I
    //   28: istore_3
    //   29: iload_1
    //   30: iload_3
    //   31: if_icmpge -> 67
    //   34: iload_3
    //   35: iload_1
    //   36: isub
    //   37: istore #4
    //   39: aload_0
    //   40: getfield mText : [C
    //   43: astore #5
    //   45: aload #5
    //   47: iload_1
    //   48: aload #5
    //   50: iload_3
    //   51: aload_0
    //   52: getfield mGapLength : I
    //   55: iadd
    //   56: iload #4
    //   58: isub
    //   59: iload #4
    //   61: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   64: goto -> 97
    //   67: iload_1
    //   68: iload_3
    //   69: isub
    //   70: istore #4
    //   72: aload_0
    //   73: getfield mText : [C
    //   76: astore #5
    //   78: aload #5
    //   80: aload_0
    //   81: getfield mGapLength : I
    //   84: iload_1
    //   85: iadd
    //   86: iload #4
    //   88: isub
    //   89: aload #5
    //   91: iload_3
    //   92: iload #4
    //   94: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   97: aload_0
    //   98: getfield mSpanCount : I
    //   101: ifeq -> 350
    //   104: iconst_0
    //   105: istore #6
    //   107: iload #6
    //   109: aload_0
    //   110: getfield mSpanCount : I
    //   113: if_icmpge -> 341
    //   116: aload_0
    //   117: getfield mSpanStarts : [I
    //   120: iload #6
    //   122: iaload
    //   123: istore_3
    //   124: aload_0
    //   125: getfield mSpanEnds : [I
    //   128: iload #6
    //   130: iaload
    //   131: istore #7
    //   133: iload_3
    //   134: istore #4
    //   136: iload_3
    //   137: aload_0
    //   138: getfield mGapStart : I
    //   141: if_icmple -> 152
    //   144: iload_3
    //   145: aload_0
    //   146: getfield mGapLength : I
    //   149: isub
    //   150: istore #4
    //   152: iload #4
    //   154: iload_1
    //   155: if_icmple -> 169
    //   158: iload #4
    //   160: aload_0
    //   161: getfield mGapLength : I
    //   164: iadd
    //   165: istore_3
    //   166: goto -> 223
    //   169: iload #4
    //   171: istore_3
    //   172: iload #4
    //   174: iload_1
    //   175: if_icmpne -> 223
    //   178: aload_0
    //   179: getfield mSpanFlags : [I
    //   182: iload #6
    //   184: iaload
    //   185: sipush #240
    //   188: iand
    //   189: iconst_4
    //   190: ishr
    //   191: istore #8
    //   193: iload #8
    //   195: iconst_2
    //   196: if_icmpeq -> 215
    //   199: iload #4
    //   201: istore_3
    //   202: iload_2
    //   203: ifeq -> 223
    //   206: iload #4
    //   208: istore_3
    //   209: iload #8
    //   211: iconst_3
    //   212: if_icmpne -> 223
    //   215: iload #4
    //   217: aload_0
    //   218: getfield mGapLength : I
    //   221: iadd
    //   222: istore_3
    //   223: iload #7
    //   225: istore #4
    //   227: iload #7
    //   229: aload_0
    //   230: getfield mGapStart : I
    //   233: if_icmple -> 245
    //   236: iload #7
    //   238: aload_0
    //   239: getfield mGapLength : I
    //   242: isub
    //   243: istore #4
    //   245: iload #4
    //   247: iload_1
    //   248: if_icmple -> 263
    //   251: iload #4
    //   253: aload_0
    //   254: getfield mGapLength : I
    //   257: iadd
    //   258: istore #7
    //   260: goto -> 318
    //   263: iload #4
    //   265: istore #7
    //   267: iload #4
    //   269: iload_1
    //   270: if_icmpne -> 318
    //   273: aload_0
    //   274: getfield mSpanFlags : [I
    //   277: iload #6
    //   279: iaload
    //   280: bipush #15
    //   282: iand
    //   283: istore #8
    //   285: iload #8
    //   287: iconst_2
    //   288: if_icmpeq -> 309
    //   291: iload #4
    //   293: istore #7
    //   295: iload_2
    //   296: ifeq -> 318
    //   299: iload #4
    //   301: istore #7
    //   303: iload #8
    //   305: iconst_3
    //   306: if_icmpne -> 318
    //   309: iload #4
    //   311: aload_0
    //   312: getfield mGapLength : I
    //   315: iadd
    //   316: istore #7
    //   318: aload_0
    //   319: getfield mSpanStarts : [I
    //   322: iload #6
    //   324: iload_3
    //   325: iastore
    //   326: aload_0
    //   327: getfield mSpanEnds : [I
    //   330: iload #6
    //   332: iload #7
    //   334: iastore
    //   335: iinc #6, 1
    //   338: goto -> 107
    //   341: aload_0
    //   342: aload_0
    //   343: invokespecial treeRoot : ()I
    //   346: invokespecial calcMax : (I)I
    //   349: pop
    //   350: aload_0
    //   351: iload_1
    //   352: putfield mGapStart : I
    //   355: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #168	-> 0
    //   #169	-> 8
    //   #171	-> 9
    //   #173	-> 24
    //   #174	-> 34
    //   #175	-> 39
    //   #176	-> 64
    //   #177	-> 67
    //   #178	-> 72
    //   #182	-> 97
    //   #183	-> 104
    //   #184	-> 116
    //   #185	-> 124
    //   #187	-> 133
    //   #188	-> 144
    //   #189	-> 152
    //   #190	-> 158
    //   #191	-> 169
    //   #192	-> 178
    //   #194	-> 193
    //   #195	-> 215
    //   #198	-> 223
    //   #199	-> 236
    //   #200	-> 245
    //   #201	-> 251
    //   #202	-> 263
    //   #203	-> 273
    //   #205	-> 285
    //   #206	-> 309
    //   #209	-> 318
    //   #210	-> 326
    //   #183	-> 335
    //   #212	-> 341
    //   #215	-> 350
    //   #216	-> 355
  }
  
  public SpannableStringBuilder insert(int paramInt1, CharSequence paramCharSequence, int paramInt2, int paramInt3) {
    return replace(paramInt1, paramInt1, paramCharSequence, paramInt2, paramInt3);
  }
  
  public SpannableStringBuilder insert(int paramInt, CharSequence paramCharSequence) {
    return replace(paramInt, paramInt, paramCharSequence, 0, paramCharSequence.length());
  }
  
  public SpannableStringBuilder delete(int paramInt1, int paramInt2) {
    SpannableStringBuilder spannableStringBuilder = replace(paramInt1, paramInt2, "", 0, 0);
    if (this.mGapLength > length() * 2)
      resizeFor(length()); 
    return spannableStringBuilder;
  }
  
  public void clear() {
    replace(0, length(), "", 0, 0);
    this.mSpanInsertCount = 0;
  }
  
  public void clearSpans() {
    for (int i = this.mSpanCount - 1; i >= 0; i--) {
      Object object = this.mSpans[i];
      int j = this.mSpanStarts[i];
      int k = this.mSpanEnds[i];
      int m = j;
      if (j > this.mGapStart)
        m = j - this.mGapLength; 
      j = k;
      if (k > this.mGapStart)
        j = k - this.mGapLength; 
      this.mSpanCount = i;
      this.mSpans[i] = null;
      sendSpanRemoved(object, m, j);
    } 
    IdentityHashMap<Object, Integer> identityHashMap = this.mIndexOfSpan;
    if (identityHashMap != null)
      identityHashMap.clear(); 
    this.mSpanInsertCount = 0;
  }
  
  public SpannableStringBuilder append(CharSequence paramCharSequence) {
    int i = length();
    return replace(i, i, paramCharSequence, 0, paramCharSequence.length());
  }
  
  public SpannableStringBuilder append(CharSequence paramCharSequence, Object paramObject, int paramInt) {
    int i = length();
    append(paramCharSequence);
    setSpan(paramObject, i, length(), paramInt);
    return this;
  }
  
  public SpannableStringBuilder append(CharSequence paramCharSequence, int paramInt1, int paramInt2) {
    int i = length();
    return replace(i, i, paramCharSequence, paramInt1, paramInt2);
  }
  
  public SpannableStringBuilder append(char paramChar) {
    return append(String.valueOf(paramChar));
  }
  
  private boolean removeSpansForChange(int paramInt1, int paramInt2, boolean paramBoolean, int paramInt3) {
    boolean bool = true;
    if ((paramInt3 & 0x1) != 0)
      if (resolveGap(this.mSpanMax[paramInt3]) >= paramInt1 && 
        removeSpansForChange(paramInt1, paramInt2, paramBoolean, leftChild(paramInt3)))
        return true;  
    if (paramInt3 < this.mSpanCount) {
      if ((this.mSpanFlags[paramInt3] & 0x21) == 33) {
        int[] arrayOfInt = this.mSpanStarts;
        if (arrayOfInt[paramInt3] >= paramInt1) {
          int i = arrayOfInt[paramInt3], j = this.mGapStart, k = this.mGapLength;
          if (i < j + k) {
            int[] arrayOfInt1 = this.mSpanEnds;
            if (arrayOfInt1[paramInt3] >= paramInt1 && arrayOfInt1[paramInt3] < k + j && (paramBoolean || arrayOfInt[paramInt3] > paramInt1 || arrayOfInt1[paramInt3] < j)) {
              this.mIndexOfSpan.remove(this.mSpans[paramInt3]);
              removeSpan(paramInt3, 0);
              return true;
            } 
          } 
        } 
      } 
      if (resolveGap(this.mSpanStarts[paramInt3]) <= paramInt2 && (paramInt3 & 0x1) != 0 && 
        removeSpansForChange(paramInt1, paramInt2, paramBoolean, rightChild(paramInt3))) {
        paramBoolean = bool;
      } else {
        paramBoolean = false;
      } 
      return paramBoolean;
    } 
    return false;
  }
  
  private void change(int paramInt1, int paramInt2, CharSequence paramCharSequence, int paramInt3, int paramInt4) {
    // Byte code:
    //   0: iload #4
    //   2: istore #6
    //   4: iload #5
    //   6: istore #7
    //   8: iload_2
    //   9: iload_1
    //   10: isub
    //   11: istore #8
    //   13: iload #7
    //   15: iload #6
    //   17: isub
    //   18: istore #9
    //   20: iload #9
    //   22: iload #8
    //   24: isub
    //   25: istore #10
    //   27: aload_0
    //   28: getfield mSpanCount : I
    //   31: istore #11
    //   33: iconst_0
    //   34: istore #12
    //   36: iload #11
    //   38: iconst_1
    //   39: isub
    //   40: istore #13
    //   42: iload #13
    //   44: iflt -> 417
    //   47: aload_0
    //   48: getfield mSpanStarts : [I
    //   51: iload #13
    //   53: iaload
    //   54: istore #14
    //   56: iload #14
    //   58: istore #11
    //   60: iload #14
    //   62: aload_0
    //   63: getfield mGapStart : I
    //   66: if_icmple -> 78
    //   69: iload #14
    //   71: aload_0
    //   72: getfield mGapLength : I
    //   75: isub
    //   76: istore #11
    //   78: aload_0
    //   79: getfield mSpanEnds : [I
    //   82: iload #13
    //   84: iaload
    //   85: istore #15
    //   87: iload #15
    //   89: istore #14
    //   91: iload #15
    //   93: aload_0
    //   94: getfield mGapStart : I
    //   97: if_icmple -> 109
    //   100: iload #15
    //   102: aload_0
    //   103: getfield mGapLength : I
    //   106: isub
    //   107: istore #14
    //   109: iload #11
    //   111: istore #16
    //   113: iload #14
    //   115: istore #15
    //   117: iload #12
    //   119: istore #17
    //   121: aload_0
    //   122: getfield mSpanFlags : [I
    //   125: iload #13
    //   127: iaload
    //   128: bipush #51
    //   130: iand
    //   131: bipush #51
    //   133: if_icmpne -> 316
    //   136: aload_0
    //   137: invokevirtual length : ()I
    //   140: istore #17
    //   142: iload #11
    //   144: istore #16
    //   146: iload #11
    //   148: iload_1
    //   149: if_icmple -> 208
    //   152: iload #11
    //   154: istore #16
    //   156: iload #11
    //   158: iload_2
    //   159: if_icmpgt -> 208
    //   162: iload_2
    //   163: istore #15
    //   165: iload #15
    //   167: istore #16
    //   169: iload #15
    //   171: iload #17
    //   173: if_icmpge -> 208
    //   176: iload #15
    //   178: iload_2
    //   179: if_icmple -> 202
    //   182: aload_0
    //   183: iload #15
    //   185: iconst_1
    //   186: isub
    //   187: invokevirtual charAt : (I)C
    //   190: bipush #10
    //   192: if_icmpne -> 202
    //   195: iload #15
    //   197: istore #16
    //   199: goto -> 208
    //   202: iinc #15, 1
    //   205: goto -> 165
    //   208: iload #14
    //   210: iload_1
    //   211: if_icmple -> 261
    //   214: iload #14
    //   216: iload_2
    //   217: if_icmpgt -> 261
    //   220: iload_2
    //   221: istore #15
    //   223: iload #15
    //   225: iload #17
    //   227: if_icmpge -> 258
    //   230: iload #15
    //   232: iload_2
    //   233: if_icmple -> 252
    //   236: aload_0
    //   237: iload #15
    //   239: iconst_1
    //   240: isub
    //   241: invokevirtual charAt : (I)C
    //   244: bipush #10
    //   246: if_icmpne -> 252
    //   249: goto -> 258
    //   252: iinc #15, 1
    //   255: goto -> 223
    //   258: goto -> 265
    //   261: iload #14
    //   263: istore #15
    //   265: iload #16
    //   267: iload #11
    //   269: if_icmpne -> 289
    //   272: iload #15
    //   274: iload #14
    //   276: if_icmpeq -> 282
    //   279: goto -> 289
    //   282: iload #12
    //   284: istore #17
    //   286: goto -> 316
    //   289: aload_0
    //   290: iconst_0
    //   291: aload_0
    //   292: getfield mSpans : [Ljava/lang/Object;
    //   295: iload #13
    //   297: aaload
    //   298: iload #16
    //   300: iload #15
    //   302: aload_0
    //   303: getfield mSpanFlags : [I
    //   306: iload #13
    //   308: iaload
    //   309: iconst_1
    //   310: invokespecial setSpan : (ZLjava/lang/Object;IIIZ)V
    //   313: iconst_1
    //   314: istore #17
    //   316: iconst_0
    //   317: istore #11
    //   319: iload #16
    //   321: iload_1
    //   322: if_icmpne -> 335
    //   325: iconst_0
    //   326: sipush #4096
    //   329: ior
    //   330: istore #11
    //   332: goto -> 351
    //   335: iload #16
    //   337: iload_2
    //   338: iload #10
    //   340: iadd
    //   341: if_icmpne -> 351
    //   344: iconst_0
    //   345: sipush #8192
    //   348: ior
    //   349: istore #11
    //   351: iload #15
    //   353: iload_1
    //   354: if_icmpne -> 368
    //   357: iload #11
    //   359: sipush #16384
    //   362: ior
    //   363: istore #14
    //   365: goto -> 388
    //   368: iload #11
    //   370: istore #14
    //   372: iload #15
    //   374: iload_2
    //   375: iload #10
    //   377: iadd
    //   378: if_icmpne -> 388
    //   381: iload #11
    //   383: ldc 32768
    //   385: ior
    //   386: istore #14
    //   388: aload_0
    //   389: getfield mSpanFlags : [I
    //   392: astore #18
    //   394: aload #18
    //   396: iload #13
    //   398: aload #18
    //   400: iload #13
    //   402: iaload
    //   403: iload #14
    //   405: ior
    //   406: iastore
    //   407: iinc #13, -1
    //   410: iload #17
    //   412: istore #12
    //   414: goto -> 42
    //   417: iload #12
    //   419: ifeq -> 426
    //   422: aload_0
    //   423: invokespecial restoreInvariants : ()V
    //   426: aload_0
    //   427: iload_2
    //   428: invokespecial moveGapTo : (I)V
    //   431: aload_0
    //   432: getfield mGapLength : I
    //   435: istore #11
    //   437: iload #10
    //   439: iload #11
    //   441: if_icmplt -> 459
    //   444: aload_0
    //   445: aload_0
    //   446: getfield mText : [C
    //   449: arraylength
    //   450: iload #10
    //   452: iadd
    //   453: iload #11
    //   455: isub
    //   456: invokespecial resizeFor : (I)V
    //   459: iload #9
    //   461: ifne -> 470
    //   464: iconst_1
    //   465: istore #19
    //   467: goto -> 473
    //   470: iconst_0
    //   471: istore #19
    //   473: iload #8
    //   475: ifle -> 503
    //   478: aload_0
    //   479: getfield mSpanCount : I
    //   482: ifle -> 503
    //   485: aload_0
    //   486: iload_1
    //   487: iload_2
    //   488: iload #19
    //   490: aload_0
    //   491: invokespecial treeRoot : ()I
    //   494: invokespecial removeSpansForChange : (IIZI)Z
    //   497: ifeq -> 503
    //   500: goto -> 478
    //   503: aload_0
    //   504: aload_0
    //   505: getfield mGapStart : I
    //   508: iload #10
    //   510: iadd
    //   511: putfield mGapStart : I
    //   514: aload_0
    //   515: getfield mGapLength : I
    //   518: iload #10
    //   520: isub
    //   521: istore_2
    //   522: aload_0
    //   523: iload_2
    //   524: putfield mGapLength : I
    //   527: iload_2
    //   528: iconst_1
    //   529: if_icmpge -> 544
    //   532: new java/lang/Exception
    //   535: dup
    //   536: ldc 'mGapLength < 1'
    //   538: invokespecial <init> : (Ljava/lang/String;)V
    //   541: invokevirtual printStackTrace : ()V
    //   544: aload_3
    //   545: iload #6
    //   547: iload #7
    //   549: aload_0
    //   550: getfield mText : [C
    //   553: iload_1
    //   554: invokestatic getChars : (Ljava/lang/CharSequence;II[CI)V
    //   557: iload #8
    //   559: ifle -> 697
    //   562: aload_0
    //   563: getfield mGapStart : I
    //   566: aload_0
    //   567: getfield mGapLength : I
    //   570: iadd
    //   571: aload_0
    //   572: getfield mText : [C
    //   575: arraylength
    //   576: if_icmpne -> 585
    //   579: iconst_1
    //   580: istore #20
    //   582: goto -> 588
    //   585: iconst_0
    //   586: istore #20
    //   588: iconst_0
    //   589: istore_2
    //   590: iload_2
    //   591: aload_0
    //   592: getfield mSpanCount : I
    //   595: if_icmpge -> 683
    //   598: aload_0
    //   599: getfield mSpanFlags : [I
    //   602: iload_2
    //   603: iaload
    //   604: istore #11
    //   606: aload_0
    //   607: getfield mSpanStarts : [I
    //   610: astore #18
    //   612: aload #18
    //   614: iload_2
    //   615: aload_0
    //   616: aload #18
    //   618: iload_2
    //   619: iaload
    //   620: iload_1
    //   621: iload #10
    //   623: iload #11
    //   625: sipush #240
    //   628: iand
    //   629: iconst_4
    //   630: ishr
    //   631: iload #20
    //   633: iload #19
    //   635: invokespecial updatedIntervalBound : (IIIIZZ)I
    //   638: iastore
    //   639: aload_0
    //   640: getfield mSpanFlags : [I
    //   643: iload_2
    //   644: iaload
    //   645: istore #11
    //   647: aload_0
    //   648: getfield mSpanEnds : [I
    //   651: astore #18
    //   653: aload #18
    //   655: iload_2
    //   656: aload_0
    //   657: aload #18
    //   659: iload_2
    //   660: iaload
    //   661: iload_1
    //   662: iload #10
    //   664: iload #11
    //   666: bipush #15
    //   668: iand
    //   669: iload #20
    //   671: iload #19
    //   673: invokespecial updatedIntervalBound : (IIIIZZ)I
    //   676: iastore
    //   677: iinc #2, 1
    //   680: goto -> 590
    //   683: iload #7
    //   685: istore #11
    //   687: iload #6
    //   689: istore_2
    //   690: aload_0
    //   691: invokespecial restoreInvariants : ()V
    //   694: goto -> 704
    //   697: iload #7
    //   699: istore #11
    //   701: iload #6
    //   703: istore_2
    //   704: aload_3
    //   705: instanceof android/text/Spanned
    //   708: ifeq -> 864
    //   711: aload_3
    //   712: checkcast android/text/Spanned
    //   715: astore #18
    //   717: aload #18
    //   719: iload_2
    //   720: iload #11
    //   722: ldc java/lang/Object
    //   724: invokeinterface getSpans : (IILjava/lang/Class;)[Ljava/lang/Object;
    //   729: astore_3
    //   730: iconst_0
    //   731: istore #14
    //   733: iload #14
    //   735: aload_3
    //   736: arraylength
    //   737: if_icmpge -> 860
    //   740: aload #18
    //   742: aload_3
    //   743: iload #14
    //   745: aaload
    //   746: invokeinterface getSpanStart : (Ljava/lang/Object;)I
    //   751: istore #7
    //   753: aload #18
    //   755: aload_3
    //   756: iload #14
    //   758: aaload
    //   759: invokeinterface getSpanEnd : (Ljava/lang/Object;)I
    //   764: istore #6
    //   766: iload #7
    //   768: istore #15
    //   770: iload #7
    //   772: iload_2
    //   773: if_icmpge -> 780
    //   776: iload #4
    //   778: istore #15
    //   780: iload #6
    //   782: istore #7
    //   784: iload #6
    //   786: iload #11
    //   788: if_icmple -> 795
    //   791: iload #5
    //   793: istore #7
    //   795: aload_0
    //   796: aload_3
    //   797: iload #14
    //   799: aaload
    //   800: invokevirtual getSpanStart : (Ljava/lang/Object;)I
    //   803: ifge -> 847
    //   806: aload #18
    //   808: aload_3
    //   809: iload #14
    //   811: aaload
    //   812: invokeinterface getSpanFlags : (Ljava/lang/Object;)I
    //   817: istore #11
    //   819: aload_0
    //   820: iconst_0
    //   821: aload_3
    //   822: iload #14
    //   824: aaload
    //   825: iload #15
    //   827: iload_2
    //   828: isub
    //   829: iload_1
    //   830: iadd
    //   831: iload #7
    //   833: iload_2
    //   834: isub
    //   835: iload_1
    //   836: iadd
    //   837: iload #11
    //   839: sipush #2048
    //   842: ior
    //   843: iconst_0
    //   844: invokespecial setSpan : (ZLjava/lang/Object;IIIZ)V
    //   847: iinc #14, 1
    //   850: iload #4
    //   852: istore_2
    //   853: iload #5
    //   855: istore #11
    //   857: goto -> 733
    //   860: aload_0
    //   861: invokespecial restoreInvariants : ()V
    //   864: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #327	-> 0
    //   #328	-> 13
    //   #329	-> 20
    //   #331	-> 27
    //   #332	-> 27
    //   #333	-> 47
    //   #334	-> 56
    //   #335	-> 69
    //   #337	-> 78
    //   #338	-> 87
    //   #339	-> 100
    //   #341	-> 109
    //   #342	-> 136
    //   #343	-> 136
    //   #344	-> 136
    //   #346	-> 142
    //   #347	-> 162
    //   #348	-> 176
    //   #349	-> 195
    //   #347	-> 202
    //   #352	-> 208
    //   #353	-> 220
    //   #354	-> 230
    //   #355	-> 249
    //   #353	-> 252
    //   #358	-> 258
    //   #359	-> 289
    //   #361	-> 313
    //   #365	-> 316
    //   #366	-> 319
    //   #367	-> 335
    //   #368	-> 351
    //   #369	-> 368
    //   #370	-> 388
    //   #332	-> 407
    //   #372	-> 417
    //   #373	-> 422
    //   #376	-> 426
    //   #378	-> 431
    //   #379	-> 444
    //   #382	-> 459
    //   #385	-> 473
    //   #386	-> 478
    //   #387	-> 485
    //   #393	-> 503
    //   #394	-> 514
    //   #396	-> 527
    //   #397	-> 532
    //   #399	-> 544
    //   #401	-> 557
    //   #403	-> 562
    //   #405	-> 588
    //   #406	-> 598
    //   #407	-> 606
    //   #410	-> 639
    //   #411	-> 647
    //   #405	-> 677
    //   #415	-> 690
    //   #401	-> 697
    //   #418	-> 704
    //   #419	-> 711
    //   #420	-> 717
    //   #422	-> 730
    //   #423	-> 740
    //   #424	-> 753
    //   #426	-> 766
    //   #427	-> 780
    //   #430	-> 795
    //   #431	-> 806
    //   #432	-> 806
    //   #433	-> 806
    //   #435	-> 819
    //   #422	-> 847
    //   #439	-> 860
    //   #441	-> 864
  }
  
  private int updatedIntervalBound(int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean1, boolean paramBoolean2) {
    if (paramInt1 >= paramInt2) {
      int i = this.mGapStart, j = this.mGapLength;
      if (paramInt1 < i + j)
        if (paramInt4 == 2) {
          if (paramBoolean2 || paramInt1 > paramInt2)
            return this.mGapStart + this.mGapLength; 
        } else if (paramInt4 == 3) {
          if (paramBoolean1)
            return i + j; 
        } else {
          if (paramBoolean2 || paramInt1 < i - paramInt3)
            return paramInt2; 
          return i;
        }  
    } 
    return paramInt1;
  }
  
  private void removeSpan(int paramInt1, int paramInt2) {
    Object object = this.mSpans[paramInt1];
    int i = this.mSpanStarts[paramInt1];
    int j = this.mSpanEnds[paramInt1];
    int k = i;
    if (i > this.mGapStart)
      k = i - this.mGapLength; 
    i = j;
    if (j > this.mGapStart)
      i = j - this.mGapLength; 
    j = this.mSpanCount - paramInt1 + 1;
    Object[] arrayOfObject = this.mSpans;
    System.arraycopy(arrayOfObject, paramInt1 + 1, arrayOfObject, paramInt1, j);
    int[] arrayOfInt = this.mSpanStarts;
    System.arraycopy(arrayOfInt, paramInt1 + 1, arrayOfInt, paramInt1, j);
    arrayOfInt = this.mSpanEnds;
    System.arraycopy(arrayOfInt, paramInt1 + 1, arrayOfInt, paramInt1, j);
    arrayOfInt = this.mSpanFlags;
    System.arraycopy(arrayOfInt, paramInt1 + 1, arrayOfInt, paramInt1, j);
    arrayOfInt = this.mSpanOrder;
    System.arraycopy(arrayOfInt, paramInt1 + 1, arrayOfInt, paramInt1, j);
    this.mSpanCount--;
    invalidateIndex(paramInt1);
    this.mSpans[this.mSpanCount] = null;
    restoreInvariants();
    if ((paramInt2 & 0x200) == 0)
      sendSpanRemoved(object, k, i); 
  }
  
  public SpannableStringBuilder replace(int paramInt1, int paramInt2, CharSequence paramCharSequence) {
    return replace(paramInt1, paramInt2, paramCharSequence, 0, paramCharSequence.length());
  }
  
  public SpannableStringBuilder replace(int paramInt1, int paramInt2, CharSequence paramCharSequence, int paramInt3, int paramInt4) {
    boolean bool;
    checkRange("replace", paramInt1, paramInt2);
    int i = this.mFilters.length;
    int j;
    for (j = paramInt3, paramInt3 = 0; paramInt3 < i; paramInt3++) {
      CharSequence charSequence = this.mFilters[paramInt3].filter(paramCharSequence, j, paramInt4, this, paramInt1, paramInt2);
      if (charSequence != null) {
        paramInt4 = charSequence.length();
        paramCharSequence = charSequence;
        j = 0;
      } 
    } 
    int k = paramInt2 - paramInt1;
    int m = paramInt4 - j;
    if (k == 0 && m == 0 && !hasNonExclusiveExclusiveSpanAt(paramCharSequence, j))
      return this; 
    TextWatcher[] arrayOfTextWatcher = getSpans(paramInt1, paramInt1 + k, TextWatcher.class);
    sendBeforeTextChanged(arrayOfTextWatcher, paramInt1, k, m);
    if (k != 0 && m != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    if (bool) {
      paramInt3 = Selection.getSelectionStart(this);
      i = Selection.getSelectionEnd(this);
    } else {
      paramInt3 = 0;
      i = 0;
    } 
    change(paramInt1, paramInt2, paramCharSequence, j, paramInt4);
    if (bool) {
      j = 0;
      if (paramInt3 > paramInt1 && paramInt3 < paramInt2) {
        long l = (paramInt3 - paramInt1);
        paramInt3 = Math.toIntExact(m * l / k);
        paramInt4 = paramInt1 + paramInt3;
        setSpan(false, Selection.SELECTION_START, paramInt4, paramInt4, 34, true);
        paramInt3 = 1;
      } else {
        paramInt4 = paramInt3;
        paramInt3 = j;
      } 
      if (i > paramInt1 && i < paramInt2) {
        long l = (i - paramInt1);
        paramInt3 = Math.toIntExact(m * l / k);
        paramInt3 = paramInt1 + paramInt3;
        setSpan(false, Selection.SELECTION_END, paramInt3, paramInt3, 34, true);
        paramInt3 = 1;
      } 
      if (paramInt3 != 0)
        restoreInvariants(); 
    } 
    sendTextChanged(arrayOfTextWatcher, paramInt1, k, m);
    sendAfterTextChanged(arrayOfTextWatcher);
    sendToSpanWatchers(paramInt1, paramInt2, m - k);
    return this;
  }
  
  private static boolean hasNonExclusiveExclusiveSpanAt(CharSequence paramCharSequence, int paramInt) {
    if (paramCharSequence instanceof Spanned) {
      Spanned spanned = (Spanned)paramCharSequence;
      Object[] arrayOfObject = spanned.getSpans(paramInt, paramInt, Object.class);
      int i = arrayOfObject.length;
      for (paramInt = 0; paramInt < i; paramInt++) {
        Object object = arrayOfObject[paramInt];
        int j = spanned.getSpanFlags(object);
        if (j != 33)
          return true; 
      } 
    } 
    return false;
  }
  
  private void sendToSpanWatchers(int paramInt1, int paramInt2, int paramInt3) {
    // Byte code:
    //   0: iconst_0
    //   1: istore #4
    //   3: iload #4
    //   5: aload_0
    //   6: getfield mSpanCount : I
    //   9: if_icmpge -> 311
    //   12: aload_0
    //   13: getfield mSpanFlags : [I
    //   16: iload #4
    //   18: iaload
    //   19: istore #5
    //   21: iload #5
    //   23: sipush #2048
    //   26: iand
    //   27: ifeq -> 33
    //   30: goto -> 305
    //   33: aload_0
    //   34: getfield mSpanStarts : [I
    //   37: iload #4
    //   39: iaload
    //   40: istore #6
    //   42: aload_0
    //   43: getfield mSpanEnds : [I
    //   46: iload #4
    //   48: iaload
    //   49: istore #7
    //   51: iload #6
    //   53: istore #8
    //   55: iload #6
    //   57: aload_0
    //   58: getfield mGapStart : I
    //   61: if_icmple -> 73
    //   64: iload #6
    //   66: aload_0
    //   67: getfield mGapLength : I
    //   70: isub
    //   71: istore #8
    //   73: iload #7
    //   75: istore #6
    //   77: iload #7
    //   79: aload_0
    //   80: getfield mGapStart : I
    //   83: if_icmple -> 95
    //   86: iload #7
    //   88: aload_0
    //   89: getfield mGapLength : I
    //   92: isub
    //   93: istore #6
    //   95: iload_2
    //   96: iload_3
    //   97: iadd
    //   98: istore #9
    //   100: iconst_0
    //   101: istore #7
    //   103: iload #8
    //   105: iload #9
    //   107: if_icmple -> 126
    //   110: iload_3
    //   111: ifeq -> 179
    //   114: iconst_1
    //   115: istore #7
    //   117: iload #8
    //   119: iload_3
    //   120: isub
    //   121: istore #10
    //   123: goto -> 183
    //   126: iload #8
    //   128: iload_1
    //   129: if_icmplt -> 179
    //   132: iload #8
    //   134: iload_1
    //   135: if_icmpne -> 150
    //   138: iload #5
    //   140: sipush #4096
    //   143: iand
    //   144: sipush #4096
    //   147: if_icmpeq -> 179
    //   150: iload #8
    //   152: iload #9
    //   154: if_icmpne -> 169
    //   157: iload #5
    //   159: sipush #8192
    //   162: iand
    //   163: sipush #8192
    //   166: if_icmpeq -> 179
    //   169: iconst_1
    //   170: istore #7
    //   172: iload #8
    //   174: istore #10
    //   176: goto -> 183
    //   179: iload #8
    //   181: istore #10
    //   183: iload #6
    //   185: iload #9
    //   187: if_icmple -> 206
    //   190: iload_3
    //   191: ifeq -> 257
    //   194: iconst_1
    //   195: istore #7
    //   197: iload #6
    //   199: iload_3
    //   200: isub
    //   201: istore #5
    //   203: goto -> 261
    //   206: iload #6
    //   208: iload_1
    //   209: if_icmplt -> 257
    //   212: iload #6
    //   214: iload_1
    //   215: if_icmpne -> 230
    //   218: iload #5
    //   220: sipush #16384
    //   223: iand
    //   224: sipush #16384
    //   227: if_icmpeq -> 257
    //   230: iload #6
    //   232: iload #9
    //   234: if_icmpne -> 247
    //   237: iload #5
    //   239: ldc 32768
    //   241: iand
    //   242: ldc 32768
    //   244: if_icmpeq -> 257
    //   247: iconst_1
    //   248: istore #7
    //   250: iload #6
    //   252: istore #5
    //   254: goto -> 261
    //   257: iload #6
    //   259: istore #5
    //   261: iload #7
    //   263: ifeq -> 285
    //   266: aload_0
    //   267: aload_0
    //   268: getfield mSpans : [Ljava/lang/Object;
    //   271: iload #4
    //   273: aaload
    //   274: iload #10
    //   276: iload #5
    //   278: iload #8
    //   280: iload #6
    //   282: invokespecial sendSpanChanged : (Ljava/lang/Object;IIII)V
    //   285: aload_0
    //   286: getfield mSpanFlags : [I
    //   289: astore #11
    //   291: aload #11
    //   293: iload #4
    //   295: aload #11
    //   297: iload #4
    //   299: iaload
    //   300: ldc_w -61441
    //   303: iand
    //   304: iastore
    //   305: iinc #4, 1
    //   308: goto -> 3
    //   311: iconst_0
    //   312: istore_1
    //   313: iload_1
    //   314: aload_0
    //   315: getfield mSpanCount : I
    //   318: if_icmpge -> 422
    //   321: aload_0
    //   322: getfield mSpanFlags : [I
    //   325: astore #11
    //   327: aload #11
    //   329: iload_1
    //   330: iaload
    //   331: istore_2
    //   332: iload_2
    //   333: sipush #2048
    //   336: iand
    //   337: ifeq -> 416
    //   340: aload #11
    //   342: iload_1
    //   343: aload #11
    //   345: iload_1
    //   346: iaload
    //   347: sipush #-2049
    //   350: iand
    //   351: iastore
    //   352: aload_0
    //   353: getfield mSpanStarts : [I
    //   356: iload_1
    //   357: iaload
    //   358: istore_3
    //   359: aload_0
    //   360: getfield mSpanEnds : [I
    //   363: iload_1
    //   364: iaload
    //   365: istore #8
    //   367: iload_3
    //   368: istore_2
    //   369: iload_3
    //   370: aload_0
    //   371: getfield mGapStart : I
    //   374: if_icmple -> 384
    //   377: iload_3
    //   378: aload_0
    //   379: getfield mGapLength : I
    //   382: isub
    //   383: istore_2
    //   384: iload #8
    //   386: istore_3
    //   387: iload #8
    //   389: aload_0
    //   390: getfield mGapStart : I
    //   393: if_icmple -> 404
    //   396: iload #8
    //   398: aload_0
    //   399: getfield mGapLength : I
    //   402: isub
    //   403: istore_3
    //   404: aload_0
    //   405: aload_0
    //   406: getfield mSpans : [Ljava/lang/Object;
    //   409: iload_1
    //   410: aaload
    //   411: iload_2
    //   412: iload_3
    //   413: invokespecial sendSpanAdded : (Ljava/lang/Object;II)V
    //   416: iinc #1, 1
    //   419: goto -> 313
    //   422: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #601	-> 0
    //   #602	-> 12
    //   #605	-> 21
    //   #606	-> 33
    //   #607	-> 42
    //   #608	-> 51
    //   #609	-> 73
    //   #611	-> 95
    //   #612	-> 100
    //   #614	-> 103
    //   #615	-> 103
    //   #616	-> 110
    //   #617	-> 114
    //   #618	-> 114
    //   #620	-> 126
    //   #622	-> 132
    //   #629	-> 169
    //   #633	-> 179
    //   #634	-> 183
    //   #635	-> 190
    //   #636	-> 194
    //   #637	-> 194
    //   #639	-> 206
    //   #641	-> 212
    //   #646	-> 247
    //   #650	-> 257
    //   #651	-> 266
    //   #653	-> 285
    //   #601	-> 305
    //   #657	-> 311
    //   #658	-> 321
    //   #659	-> 332
    //   #660	-> 340
    //   #661	-> 352
    //   #662	-> 359
    //   #663	-> 367
    //   #664	-> 384
    //   #665	-> 404
    //   #657	-> 416
    //   #668	-> 422
  }
  
  public void setSpan(Object paramObject, int paramInt1, int paramInt2, int paramInt3) {
    setSpan(true, paramObject, paramInt1, paramInt2, paramInt3, true);
  }
  
  private void setSpan(boolean paramBoolean1, Object paramObject, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean2) {
    checkRange("setSpan", paramInt1, paramInt2);
    int i = (paramInt3 & 0xF0) >> 4;
    if (isInvalidParagraph(paramInt1, i)) {
      if (!paramBoolean2)
        return; 
      paramObject = new StringBuilder();
      paramObject.append("PARAGRAPH span must start at paragraph boundary (");
      paramObject.append(paramInt1);
      paramObject.append(" follows ");
      paramObject.append(charAt(paramInt1 - 1));
      paramObject.append(")");
      throw new RuntimeException(paramObject.toString());
    } 
    int j = paramInt3 & 0xF;
    if (isInvalidParagraph(paramInt2, j)) {
      if (!paramBoolean2)
        return; 
      paramObject = new StringBuilder();
      paramObject.append("PARAGRAPH span must end at paragraph boundary (");
      paramObject.append(paramInt2);
      paramObject.append(" follows ");
      paramObject.append(charAt(paramInt2 - 1));
      paramObject.append(")");
      throw new RuntimeException(paramObject.toString());
    } 
    if (i == 2 && j == 1 && paramInt1 == paramInt2) {
      if (paramBoolean1)
        Log.e("SpannableStringBuilder", "SPAN_EXCLUSIVE_EXCLUSIVE spans cannot have a zero length"); 
      return;
    } 
    int k = this.mGapStart;
    if (paramInt1 > k) {
      k = paramInt1 + this.mGapLength;
    } else if (paramInt1 == k && (
      i == 2 || (i == 3 && paramInt1 == length()))) {
      k = paramInt1 + this.mGapLength;
    } else {
      k = paramInt1;
    } 
    i = this.mGapStart;
    if (paramInt2 > i) {
      j = this.mGapLength + paramInt2;
    } else if (paramInt2 == i && (
      j == 2 || (j == 3 && paramInt2 == length()))) {
      j = this.mGapLength + paramInt2;
    } else {
      j = paramInt2;
    } 
    IdentityHashMap<Object, Integer> identityHashMap = this.mIndexOfSpan;
    if (identityHashMap != null) {
      Integer integer = identityHashMap.get(paramObject);
      if (integer != null) {
        int m = integer.intValue();
        i = this.mSpanStarts[m];
        int n = this.mSpanEnds[m];
        if (i > this.mGapStart)
          i -= this.mGapLength; 
        if (n > this.mGapStart)
          n -= this.mGapLength; 
        this.mSpanStarts[m] = k;
        this.mSpanEnds[m] = j;
        this.mSpanFlags[m] = paramInt3;
        if (paramBoolean1) {
          restoreInvariants();
          sendSpanChanged(paramObject, i, n, paramInt1, paramInt2);
        } 
        return;
      } 
    } 
    this.mSpans = GrowingArrayUtils.append(this.mSpans, this.mSpanCount, paramObject);
    this.mSpanStarts = GrowingArrayUtils.append(this.mSpanStarts, this.mSpanCount, k);
    this.mSpanEnds = GrowingArrayUtils.append(this.mSpanEnds, this.mSpanCount, j);
    this.mSpanFlags = GrowingArrayUtils.append(this.mSpanFlags, this.mSpanCount, paramInt3);
    this.mSpanOrder = GrowingArrayUtils.append(this.mSpanOrder, this.mSpanCount, this.mSpanInsertCount);
    invalidateIndex(this.mSpanCount);
    this.mSpanCount++;
    this.mSpanInsertCount++;
    paramInt3 = treeRoot() * 2 + 1;
    if (this.mSpanMax.length < paramInt3)
      this.mSpanMax = new int[paramInt3]; 
    if (paramBoolean1) {
      restoreInvariants();
      sendSpanAdded(paramObject, paramInt1, paramInt2);
    } 
  }
  
  private boolean isInvalidParagraph(int paramInt1, int paramInt2) {
    boolean bool;
    if (paramInt2 == 3 && paramInt1 != 0 && paramInt1 != length() && charAt(paramInt1 - 1) != '\n') {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void removeSpan(Object paramObject) {
    removeSpan(paramObject, 0);
  }
  
  public void removeSpan(Object paramObject, int paramInt) {
    IdentityHashMap<Object, Integer> identityHashMap = this.mIndexOfSpan;
    if (identityHashMap == null)
      return; 
    paramObject = identityHashMap.remove(paramObject);
    if (paramObject != null)
      removeSpan(paramObject.intValue(), paramInt); 
  }
  
  private int resolveGap(int paramInt) {
    if (paramInt > this.mGapStart)
      paramInt -= this.mGapLength; 
    return paramInt;
  }
  
  public int getSpanStart(Object paramObject) {
    IdentityHashMap<Object, Integer> identityHashMap = this.mIndexOfSpan;
    int i = -1;
    if (identityHashMap == null)
      return -1; 
    paramObject = identityHashMap.get(paramObject);
    if (paramObject != null)
      i = resolveGap(this.mSpanStarts[paramObject.intValue()]); 
    return i;
  }
  
  public int getSpanEnd(Object paramObject) {
    IdentityHashMap<Object, Integer> identityHashMap = this.mIndexOfSpan;
    int i = -1;
    if (identityHashMap == null)
      return -1; 
    paramObject = identityHashMap.get(paramObject);
    if (paramObject != null)
      i = resolveGap(this.mSpanEnds[paramObject.intValue()]); 
    return i;
  }
  
  public int getSpanFlags(Object paramObject) {
    IdentityHashMap<Object, Integer> identityHashMap = this.mIndexOfSpan;
    int i = 0;
    if (identityHashMap == null)
      return 0; 
    paramObject = identityHashMap.get(paramObject);
    if (paramObject != null)
      i = this.mSpanFlags[paramObject.intValue()]; 
    return i;
  }
  
  public <T> T[] getSpans(int paramInt1, int paramInt2, Class<T> paramClass) {
    return getSpans(paramInt1, paramInt2, paramClass, true);
  }
  
  public <T> T[] getSpans(int paramInt1, int paramInt2, Class<T> paramClass, boolean paramBoolean) {
    int[] arrayOfInt1, arrayOfInt2;
    if (paramClass == null)
      return (T[])ArrayUtils.emptyArray(Object.class); 
    if (this.mSpanCount == 0)
      return (T[])ArrayUtils.emptyArray(paramClass); 
    int i = countSpans(paramInt1, paramInt2, paramClass, treeRoot());
    if (i == 0)
      return (T[])ArrayUtils.emptyArray(paramClass); 
    Object[] arrayOfObject = (Object[])Array.newInstance(paramClass, i);
    if (paramBoolean) {
      arrayOfInt1 = obtain(i);
    } else {
      arrayOfInt1 = EmptyArray.INT;
    } 
    if (paramBoolean) {
      arrayOfInt2 = obtain(i);
    } else {
      arrayOfInt2 = EmptyArray.INT;
    } 
    getSpansRec(paramInt1, paramInt2, paramClass, treeRoot(), (T[])arrayOfObject, arrayOfInt1, arrayOfInt2, 0, paramBoolean);
    if (paramBoolean) {
      sort(arrayOfObject, arrayOfInt1, arrayOfInt2);
      recycle(arrayOfInt1);
      recycle(arrayOfInt2);
    } 
    return (T[])arrayOfObject;
  }
  
  private int countSpans(int paramInt1, int paramInt2, Class paramClass, int paramInt3) {
    // Byte code:
    //   0: iconst_0
    //   1: istore #5
    //   3: iload #5
    //   5: istore #6
    //   7: iload #4
    //   9: iconst_1
    //   10: iand
    //   11: ifeq -> 73
    //   14: iload #4
    //   16: invokestatic leftChild : (I)I
    //   19: istore #7
    //   21: aload_0
    //   22: getfield mSpanMax : [I
    //   25: iload #7
    //   27: iaload
    //   28: istore #6
    //   30: iload #6
    //   32: istore #8
    //   34: iload #6
    //   36: aload_0
    //   37: getfield mGapStart : I
    //   40: if_icmple -> 52
    //   43: iload #6
    //   45: aload_0
    //   46: getfield mGapLength : I
    //   49: isub
    //   50: istore #8
    //   52: iload #5
    //   54: istore #6
    //   56: iload #8
    //   58: iload_1
    //   59: if_icmplt -> 73
    //   62: aload_0
    //   63: iload_1
    //   64: iload_2
    //   65: aload_3
    //   66: iload #7
    //   68: invokespecial countSpans : (IILjava/lang/Class;I)I
    //   71: istore #6
    //   73: iload #6
    //   75: istore #5
    //   77: iload #4
    //   79: aload_0
    //   80: getfield mSpanCount : I
    //   83: if_icmpge -> 262
    //   86: aload_0
    //   87: getfield mSpanStarts : [I
    //   90: iload #4
    //   92: iaload
    //   93: istore #8
    //   95: iload #8
    //   97: istore #7
    //   99: iload #8
    //   101: aload_0
    //   102: getfield mGapStart : I
    //   105: if_icmple -> 117
    //   108: iload #8
    //   110: aload_0
    //   111: getfield mGapLength : I
    //   114: isub
    //   115: istore #7
    //   117: iload #6
    //   119: istore #5
    //   121: iload #7
    //   123: iload_2
    //   124: if_icmpgt -> 262
    //   127: aload_0
    //   128: getfield mSpanEnds : [I
    //   131: iload #4
    //   133: iaload
    //   134: istore #8
    //   136: iload #8
    //   138: istore #5
    //   140: iload #8
    //   142: aload_0
    //   143: getfield mGapStart : I
    //   146: if_icmple -> 158
    //   149: iload #8
    //   151: aload_0
    //   152: getfield mGapLength : I
    //   155: isub
    //   156: istore #5
    //   158: iload #6
    //   160: istore #8
    //   162: iload #5
    //   164: iload_1
    //   165: if_icmplt -> 234
    //   168: iload #7
    //   170: iload #5
    //   172: if_icmpeq -> 200
    //   175: iload_1
    //   176: iload_2
    //   177: if_icmpeq -> 200
    //   180: iload #6
    //   182: istore #8
    //   184: iload #7
    //   186: iload_2
    //   187: if_icmpeq -> 234
    //   190: iload #6
    //   192: istore #8
    //   194: iload #5
    //   196: iload_1
    //   197: if_icmpeq -> 234
    //   200: ldc java/lang/Object
    //   202: aload_3
    //   203: if_acmpeq -> 228
    //   206: aload_0
    //   207: getfield mSpans : [Ljava/lang/Object;
    //   210: iload #4
    //   212: aaload
    //   213: astore #9
    //   215: iload #6
    //   217: istore #8
    //   219: aload_3
    //   220: aload #9
    //   222: invokevirtual isInstance : (Ljava/lang/Object;)Z
    //   225: ifeq -> 234
    //   228: iload #6
    //   230: iconst_1
    //   231: iadd
    //   232: istore #8
    //   234: iload #8
    //   236: istore #5
    //   238: iload #4
    //   240: iconst_1
    //   241: iand
    //   242: ifeq -> 262
    //   245: iload #8
    //   247: aload_0
    //   248: iload_1
    //   249: iload_2
    //   250: aload_3
    //   251: iload #4
    //   253: invokestatic rightChild : (I)I
    //   256: invokespecial countSpans : (IILjava/lang/Class;I)I
    //   259: iadd
    //   260: istore #5
    //   262: iload #5
    //   264: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #891	-> 0
    //   #892	-> 3
    //   #894	-> 14
    //   #895	-> 21
    //   #896	-> 30
    //   #897	-> 43
    //   #899	-> 52
    //   #900	-> 62
    //   #903	-> 73
    //   #904	-> 86
    //   #905	-> 95
    //   #906	-> 108
    //   #908	-> 117
    //   #909	-> 127
    //   #910	-> 136
    //   #911	-> 149
    //   #913	-> 158
    //   #916	-> 215
    //   #917	-> 228
    //   #919	-> 234
    //   #920	-> 245
    //   #924	-> 262
  }
  
  private <T> int getSpansRec(int paramInt1, int paramInt2, Class<T> paramClass, int paramInt3, T[] paramArrayOfT, int[] paramArrayOfint1, int[] paramArrayOfint2, int paramInt4, boolean paramBoolean) {
    // Byte code:
    //   0: iload #4
    //   2: iconst_1
    //   3: iand
    //   4: ifeq -> 74
    //   7: iload #4
    //   9: invokestatic leftChild : (I)I
    //   12: istore #10
    //   14: aload_0
    //   15: getfield mSpanMax : [I
    //   18: iload #10
    //   20: iaload
    //   21: istore #11
    //   23: iload #11
    //   25: aload_0
    //   26: getfield mGapStart : I
    //   29: if_icmple -> 44
    //   32: iload #11
    //   34: aload_0
    //   35: getfield mGapLength : I
    //   38: isub
    //   39: istore #11
    //   41: goto -> 44
    //   44: iload #11
    //   46: iload_1
    //   47: if_icmplt -> 74
    //   50: aload_0
    //   51: iload_1
    //   52: iload_2
    //   53: aload_3
    //   54: iload #10
    //   56: aload #5
    //   58: aload #6
    //   60: aload #7
    //   62: iload #8
    //   64: iload #9
    //   66: invokespecial getSpansRec : (IILjava/lang/Class;I[Ljava/lang/Object;[I[IIZ)I
    //   69: istore #8
    //   71: goto -> 74
    //   74: iload #4
    //   76: aload_0
    //   77: getfield mSpanCount : I
    //   80: if_icmplt -> 86
    //   83: iload #8
    //   85: ireturn
    //   86: aload_0
    //   87: getfield mSpanStarts : [I
    //   90: iload #4
    //   92: iaload
    //   93: istore #11
    //   95: iload #11
    //   97: aload_0
    //   98: getfield mGapStart : I
    //   101: if_icmple -> 116
    //   104: iload #11
    //   106: aload_0
    //   107: getfield mGapLength : I
    //   110: isub
    //   111: istore #11
    //   113: goto -> 116
    //   116: iload #11
    //   118: iload_2
    //   119: if_icmpgt -> 377
    //   122: aload_0
    //   123: getfield mSpanEnds : [I
    //   126: iload #4
    //   128: iaload
    //   129: istore #10
    //   131: iload #10
    //   133: aload_0
    //   134: getfield mGapStart : I
    //   137: if_icmple -> 152
    //   140: iload #10
    //   142: aload_0
    //   143: getfield mGapLength : I
    //   146: isub
    //   147: istore #10
    //   149: goto -> 152
    //   152: iload #10
    //   154: iload_1
    //   155: if_icmplt -> 332
    //   158: iload #11
    //   160: iload #10
    //   162: if_icmpeq -> 182
    //   165: iload_1
    //   166: iload_2
    //   167: if_icmpeq -> 182
    //   170: iload #11
    //   172: iload_2
    //   173: if_icmpeq -> 332
    //   176: iload #10
    //   178: iload_1
    //   179: if_icmpeq -> 332
    //   182: ldc java/lang/Object
    //   184: aload_3
    //   185: if_acmpeq -> 206
    //   188: aload_0
    //   189: getfield mSpans : [Ljava/lang/Object;
    //   192: iload #4
    //   194: aaload
    //   195: astore #12
    //   197: aload_3
    //   198: aload #12
    //   200: invokevirtual isInstance : (Ljava/lang/Object;)Z
    //   203: ifeq -> 332
    //   206: aload_0
    //   207: getfield mSpanFlags : [I
    //   210: iload #4
    //   212: iaload
    //   213: ldc_w 16711680
    //   216: iand
    //   217: istore #10
    //   219: iload #8
    //   221: istore #11
    //   223: iload #9
    //   225: ifeq -> 250
    //   228: aload #6
    //   230: iload #11
    //   232: iload #10
    //   234: iastore
    //   235: aload #7
    //   237: iload #11
    //   239: aload_0
    //   240: getfield mSpanOrder : [I
    //   243: iload #4
    //   245: iaload
    //   246: iastore
    //   247: goto -> 314
    //   250: iload #10
    //   252: ifeq -> 314
    //   255: iconst_0
    //   256: istore #11
    //   258: iload #11
    //   260: iload #8
    //   262: if_icmpge -> 296
    //   265: aload_0
    //   266: aload #5
    //   268: iload #11
    //   270: aaload
    //   271: invokevirtual getSpanFlags : (Ljava/lang/Object;)I
    //   274: istore #13
    //   276: iload #10
    //   278: iload #13
    //   280: ldc_w 16711680
    //   283: iand
    //   284: if_icmple -> 290
    //   287: goto -> 296
    //   290: iinc #11, 1
    //   293: goto -> 258
    //   296: aload #5
    //   298: iload #11
    //   300: aload #5
    //   302: iload #11
    //   304: iconst_1
    //   305: iadd
    //   306: iload #8
    //   308: iload #11
    //   310: isub
    //   311: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   314: aload #5
    //   316: iload #11
    //   318: aload_0
    //   319: getfield mSpans : [Ljava/lang/Object;
    //   322: iload #4
    //   324: aaload
    //   325: aastore
    //   326: iinc #8, 1
    //   329: goto -> 332
    //   332: iload #8
    //   334: aload #5
    //   336: arraylength
    //   337: if_icmpge -> 374
    //   340: iload #4
    //   342: iconst_1
    //   343: iand
    //   344: ifeq -> 374
    //   347: aload_0
    //   348: iload_1
    //   349: iload_2
    //   350: aload_3
    //   351: iload #4
    //   353: invokestatic rightChild : (I)I
    //   356: aload #5
    //   358: aload #6
    //   360: aload #7
    //   362: iload #8
    //   364: iload #9
    //   366: invokespecial getSpansRec : (IILjava/lang/Class;I[Ljava/lang/Object;[I[IIZ)I
    //   369: istore #8
    //   371: goto -> 377
    //   374: goto -> 377
    //   377: iload #8
    //   379: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #946	-> 0
    //   #948	-> 7
    //   #949	-> 14
    //   #950	-> 23
    //   #951	-> 32
    //   #950	-> 44
    //   #953	-> 44
    //   #954	-> 50
    //   #953	-> 74
    //   #958	-> 74
    //   #959	-> 86
    //   #960	-> 95
    //   #961	-> 104
    //   #960	-> 116
    //   #963	-> 116
    //   #964	-> 122
    //   #965	-> 131
    //   #966	-> 140
    //   #965	-> 152
    //   #968	-> 152
    //   #971	-> 197
    //   #972	-> 206
    //   #973	-> 219
    //   #974	-> 223
    //   #975	-> 228
    //   #976	-> 235
    //   #977	-> 250
    //   #979	-> 255
    //   #980	-> 258
    //   #981	-> 265
    //   #982	-> 276
    //   #980	-> 290
    //   #984	-> 296
    //   #985	-> 314
    //   #987	-> 314
    //   #988	-> 326
    //   #990	-> 332
    //   #991	-> 347
    //   #990	-> 374
    //   #995	-> 374
    //   #963	-> 377
    //   #995	-> 377
  }
  
  private static int[] obtain(int paramInt) {
    // Byte code:
    //   0: aconst_null
    //   1: astore_1
    //   2: getstatic android/text/SpannableStringBuilder.sCachedIntBuffer : [[I
    //   5: astore_2
    //   6: aload_2
    //   7: monitorenter
    //   8: iconst_m1
    //   9: istore_3
    //   10: getstatic android/text/SpannableStringBuilder.sCachedIntBuffer : [[I
    //   13: arraylength
    //   14: iconst_1
    //   15: isub
    //   16: istore #4
    //   18: iload_3
    //   19: istore #5
    //   21: iload #4
    //   23: iflt -> 77
    //   26: iload_3
    //   27: istore #5
    //   29: getstatic android/text/SpannableStringBuilder.sCachedIntBuffer : [[I
    //   32: iload #4
    //   34: aaload
    //   35: ifnull -> 68
    //   38: getstatic android/text/SpannableStringBuilder.sCachedIntBuffer : [[I
    //   41: iload #4
    //   43: aaload
    //   44: arraylength
    //   45: iload_0
    //   46: if_icmplt -> 56
    //   49: iload #4
    //   51: istore #5
    //   53: goto -> 77
    //   56: iload_3
    //   57: istore #5
    //   59: iload_3
    //   60: iconst_m1
    //   61: if_icmpne -> 68
    //   64: iload #4
    //   66: istore #5
    //   68: iinc #4, -1
    //   71: iload #5
    //   73: istore_3
    //   74: goto -> 18
    //   77: iload #5
    //   79: iconst_m1
    //   80: if_icmpeq -> 97
    //   83: getstatic android/text/SpannableStringBuilder.sCachedIntBuffer : [[I
    //   86: iload #5
    //   88: aaload
    //   89: astore_1
    //   90: getstatic android/text/SpannableStringBuilder.sCachedIntBuffer : [[I
    //   93: iload #5
    //   95: aconst_null
    //   96: aastore
    //   97: aload_2
    //   98: monitorexit
    //   99: aload_1
    //   100: iload_0
    //   101: invokestatic checkSortBuffer : ([II)[I
    //   104: astore_1
    //   105: aload_1
    //   106: areturn
    //   107: astore_1
    //   108: aload_2
    //   109: monitorexit
    //   110: aload_1
    //   111: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1005	-> 0
    //   #1006	-> 2
    //   #1009	-> 8
    //   #1010	-> 10
    //   #1011	-> 26
    //   #1012	-> 38
    //   #1013	-> 49
    //   #1014	-> 49
    //   #1015	-> 56
    //   #1016	-> 64
    //   #1010	-> 68
    //   #1021	-> 77
    //   #1022	-> 83
    //   #1023	-> 90
    //   #1025	-> 97
    //   #1026	-> 99
    //   #1027	-> 105
    //   #1025	-> 107
    // Exception table:
    //   from	to	target	type
    //   10	18	107	finally
    //   29	38	107	finally
    //   38	49	107	finally
    //   83	90	107	finally
    //   90	97	107	finally
    //   97	99	107	finally
    //   108	110	107	finally
  }
  
  private static void recycle(int[] paramArrayOfint) {
    // Byte code:
    //   0: getstatic android/text/SpannableStringBuilder.sCachedIntBuffer : [[I
    //   3: astore_1
    //   4: aload_1
    //   5: monitorenter
    //   6: iconst_0
    //   7: istore_2
    //   8: iload_2
    //   9: getstatic android/text/SpannableStringBuilder.sCachedIntBuffer : [[I
    //   12: arraylength
    //   13: if_icmpge -> 50
    //   16: getstatic android/text/SpannableStringBuilder.sCachedIntBuffer : [[I
    //   19: iload_2
    //   20: aaload
    //   21: ifnull -> 44
    //   24: aload_0
    //   25: arraylength
    //   26: getstatic android/text/SpannableStringBuilder.sCachedIntBuffer : [[I
    //   29: iload_2
    //   30: aaload
    //   31: arraylength
    //   32: if_icmple -> 38
    //   35: goto -> 44
    //   38: iinc #2, 1
    //   41: goto -> 8
    //   44: getstatic android/text/SpannableStringBuilder.sCachedIntBuffer : [[I
    //   47: iload_2
    //   48: aload_0
    //   49: aastore
    //   50: aload_1
    //   51: monitorexit
    //   52: return
    //   53: astore_0
    //   54: aload_1
    //   55: monitorexit
    //   56: aload_0
    //   57: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1036	-> 0
    //   #1037	-> 6
    //   #1038	-> 16
    //   #1037	-> 38
    //   #1039	-> 44
    //   #1043	-> 50
    //   #1044	-> 52
    //   #1043	-> 53
    // Exception table:
    //   from	to	target	type
    //   8	16	53	finally
    //   16	35	53	finally
    //   44	50	53	finally
    //   50	52	53	finally
    //   54	56	53	finally
  }
  
  private static int[] checkSortBuffer(int[] paramArrayOfint, int paramInt) {
    if (paramArrayOfint == null || paramInt > paramArrayOfint.length)
      return ArrayUtils.newUnpaddedIntArray(GrowingArrayUtils.growSize(paramInt)); 
    return paramArrayOfint;
  }
  
  private final <T> void sort(T[] paramArrayOfT, int[] paramArrayOfint1, int[] paramArrayOfint2) {
    int i = paramArrayOfT.length;
    int j;
    for (j = i / 2 - 1; j >= 0; j--)
      siftDown(j, paramArrayOfT, i, paramArrayOfint1, paramArrayOfint2); 
    for (j = i - 1; j > 0; j--) {
      T t = paramArrayOfT[0];
      paramArrayOfT[0] = paramArrayOfT[j];
      paramArrayOfT[j] = t;
      i = paramArrayOfint1[0];
      paramArrayOfint1[0] = paramArrayOfint1[j];
      paramArrayOfint1[j] = i;
      i = paramArrayOfint2[0];
      paramArrayOfint2[0] = paramArrayOfint2[j];
      paramArrayOfint2[j] = i;
      siftDown(0, paramArrayOfT, j, paramArrayOfint1, paramArrayOfint2);
    } 
  }
  
  private final <T> void siftDown(int paramInt1, T[] paramArrayOfT, int paramInt2, int[] paramArrayOfint1, int[] paramArrayOfint2) {
    int i = paramInt1 * 2 + 1, j = paramInt1;
    while (i < paramInt2) {
      paramInt1 = i;
      if (i < paramInt2 - 1) {
        paramInt1 = i;
        if (compareSpans(i, i + 1, paramArrayOfint1, paramArrayOfint2) < 0)
          paramInt1 = i + 1; 
      } 
      if (compareSpans(j, paramInt1, paramArrayOfint1, paramArrayOfint2) >= 0)
        break; 
      T t = paramArrayOfT[j];
      paramArrayOfT[j] = paramArrayOfT[paramInt1];
      paramArrayOfT[paramInt1] = t;
      i = paramArrayOfint1[j];
      paramArrayOfint1[j] = paramArrayOfint1[paramInt1];
      paramArrayOfint1[paramInt1] = i;
      i = paramArrayOfint2[j];
      paramArrayOfint2[j] = paramArrayOfint2[paramInt1];
      paramArrayOfint2[paramInt1] = i;
      i = paramInt1 * 2 + 1;
      j = paramInt1;
    } 
  }
  
  private final int compareSpans(int paramInt1, int paramInt2, int[] paramArrayOfint1, int[] paramArrayOfint2) {
    int i = paramArrayOfint1[paramInt1];
    int j = paramArrayOfint1[paramInt2];
    if (i == j)
      return Integer.compare(paramArrayOfint2[paramInt1], paramArrayOfint2[paramInt2]); 
    return Integer.compare(j, i);
  }
  
  public int nextSpanTransition(int paramInt1, int paramInt2, Class<Object> paramClass) {
    if (this.mSpanCount == 0)
      return paramInt2; 
    Class<Object> clazz = paramClass;
    if (paramClass == null)
      clazz = Object.class; 
    return nextSpanTransitionRec(paramInt1, paramInt2, clazz, treeRoot());
  }
  
  private int nextSpanTransitionRec(int paramInt1, int paramInt2, Class paramClass, int paramInt3) {
    int i = paramInt2;
    if ((paramInt3 & 0x1) != 0) {
      int j = leftChild(paramInt3);
      i = paramInt2;
      if (resolveGap(this.mSpanMax[j]) > paramInt1)
        i = nextSpanTransitionRec(paramInt1, paramInt2, paramClass, j); 
    } 
    paramInt2 = i;
    if (paramInt3 < this.mSpanCount) {
      int j = resolveGap(this.mSpanStarts[paramInt3]);
      int k = resolveGap(this.mSpanEnds[paramInt3]);
      paramInt2 = i;
      if (j > paramInt1) {
        paramInt2 = i;
        if (j < i) {
          paramInt2 = i;
          if (paramClass.isInstance(this.mSpans[paramInt3]))
            paramInt2 = j; 
        } 
      } 
      i = paramInt2;
      if (k > paramInt1) {
        i = paramInt2;
        if (k < paramInt2) {
          i = paramInt2;
          if (paramClass.isInstance(this.mSpans[paramInt3]))
            i = k; 
        } 
      } 
      paramInt2 = i;
      if (j < i) {
        paramInt2 = i;
        if ((paramInt3 & 0x1) != 0)
          paramInt2 = nextSpanTransitionRec(paramInt1, i, paramClass, rightChild(paramInt3)); 
      } 
    } 
    return paramInt2;
  }
  
  public CharSequence subSequence(int paramInt1, int paramInt2) {
    return new SpannableStringBuilder(this, paramInt1, paramInt2);
  }
  
  public void getChars(int paramInt1, int paramInt2, char[] paramArrayOfchar, int paramInt3) {
    checkRange("getChars", paramInt1, paramInt2);
    int i = this.mGapStart;
    if (paramInt2 <= i) {
      System.arraycopy(this.mText, paramInt1, paramArrayOfchar, paramInt3, paramInt2 - paramInt1);
    } else if (paramInt1 >= i) {
      System.arraycopy(this.mText, this.mGapLength + paramInt1, paramArrayOfchar, paramInt3, paramInt2 - paramInt1);
    } else {
      System.arraycopy(this.mText, paramInt1, paramArrayOfchar, paramInt3, i - paramInt1);
      char[] arrayOfChar = this.mText;
      i = this.mGapStart;
      System.arraycopy(arrayOfChar, this.mGapLength + i, paramArrayOfchar, i - paramInt1 + paramInt3, paramInt2 - i);
    } 
  }
  
  public String toString() {
    int i = length();
    char[] arrayOfChar = new char[i];
    getChars(0, i, arrayOfChar, 0);
    return new String(arrayOfChar);
  }
  
  public String substring(int paramInt1, int paramInt2) {
    char[] arrayOfChar = new char[paramInt2 - paramInt1];
    getChars(paramInt1, paramInt2, arrayOfChar, 0);
    return new String(arrayOfChar);
  }
  
  public int getTextWatcherDepth() {
    return this.mTextWatcherDepth;
  }
  
  private void sendBeforeTextChanged(TextWatcher[] paramArrayOfTextWatcher, int paramInt1, int paramInt2, int paramInt3) {
    int i = paramArrayOfTextWatcher.length;
    this.mTextWatcherDepth++;
    for (byte b = 0; b < i; b++)
      paramArrayOfTextWatcher[b].beforeTextChanged(this, paramInt1, paramInt2, paramInt3); 
    this.mTextWatcherDepth--;
  }
  
  private void sendTextChanged(TextWatcher[] paramArrayOfTextWatcher, int paramInt1, int paramInt2, int paramInt3) {
    int i = paramArrayOfTextWatcher.length;
    this.mTextWatcherDepth++;
    for (byte b = 0; b < i; b++)
      paramArrayOfTextWatcher[b].onTextChanged(this, paramInt1, paramInt2, paramInt3); 
    this.mTextWatcherDepth--;
  }
  
  private void sendAfterTextChanged(TextWatcher[] paramArrayOfTextWatcher) {
    int i = paramArrayOfTextWatcher.length;
    this.mTextWatcherDepth++;
    for (byte b = 0; b < i; b++)
      paramArrayOfTextWatcher[b].afterTextChanged(this); 
    this.mTextWatcherDepth--;
  }
  
  private void sendSpanAdded(Object paramObject, int paramInt1, int paramInt2) {
    SpanWatcher[] arrayOfSpanWatcher = getSpans(paramInt1, paramInt2, SpanWatcher.class);
    int i = arrayOfSpanWatcher.length;
    for (byte b = 0; b < i; b++)
      arrayOfSpanWatcher[b].onSpanAdded(this, paramObject, paramInt1, paramInt2); 
  }
  
  private void sendSpanRemoved(Object paramObject, int paramInt1, int paramInt2) {
    SpanWatcher[] arrayOfSpanWatcher = getSpans(paramInt1, paramInt2, SpanWatcher.class);
    int i = arrayOfSpanWatcher.length;
    for (byte b = 0; b < i; b++)
      arrayOfSpanWatcher[b].onSpanRemoved(this, paramObject, paramInt1, paramInt2); 
  }
  
  private void sendSpanChanged(Object paramObject, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    int i = Math.min(paramInt1, paramInt3);
    int j = Math.min(Math.max(paramInt2, paramInt4), length());
    SpanWatcher[] arrayOfSpanWatcher = getSpans(i, j, SpanWatcher.class);
    j = arrayOfSpanWatcher.length;
    for (i = 0; i < j; i++)
      arrayOfSpanWatcher[i].onSpanChanged(this, paramObject, paramInt1, paramInt2, paramInt3, paramInt4); 
  }
  
  private static String region(int paramInt1, int paramInt2) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("(");
    stringBuilder.append(paramInt1);
    stringBuilder.append(" ... ");
    stringBuilder.append(paramInt2);
    stringBuilder.append(")");
    return stringBuilder.toString();
  }
  
  private void checkRange(String paramString, int paramInt1, int paramInt2) {
    if (paramInt2 >= paramInt1) {
      int i = length();
      if (paramInt1 <= i && paramInt2 <= i) {
        if (paramInt1 >= 0 && paramInt2 >= 0)
          return; 
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append(paramString);
        stringBuilder2.append(" ");
        stringBuilder2.append(region(paramInt1, paramInt2));
        stringBuilder2.append(" starts before 0");
        throw new IndexOutOfBoundsException(stringBuilder2.toString());
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramString);
      stringBuilder1.append(" ");
      stringBuilder1.append(region(paramInt1, paramInt2));
      stringBuilder1.append(" ends beyond length ");
      stringBuilder1.append(i);
      throw new IndexOutOfBoundsException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append(" ");
    stringBuilder.append(region(paramInt1, paramInt2));
    stringBuilder.append(" has end before start");
    throw new IndexOutOfBoundsException(stringBuilder.toString());
  }
  
  public void drawText(BaseCanvas paramBaseCanvas, int paramInt1, int paramInt2, float paramFloat1, float paramFloat2, Paint paramPaint) {
    checkRange("drawText", paramInt1, paramInt2);
    int i = this.mGapStart;
    if (paramInt2 <= i) {
      paramBaseCanvas.drawText(this.mText, paramInt1, paramInt2 - paramInt1, paramFloat1, paramFloat2, paramPaint);
    } else if (paramInt1 >= i) {
      paramBaseCanvas.drawText(this.mText, paramInt1 + this.mGapLength, paramInt2 - paramInt1, paramFloat1, paramFloat2, paramPaint);
    } else {
      char[] arrayOfChar = TextUtils.obtain(paramInt2 - paramInt1);
      getChars(paramInt1, paramInt2, arrayOfChar, 0);
      paramBaseCanvas.drawText(arrayOfChar, 0, paramInt2 - paramInt1, paramFloat1, paramFloat2, paramPaint);
      TextUtils.recycle(arrayOfChar);
    } 
  }
  
  public void drawTextRun(BaseCanvas paramBaseCanvas, int paramInt1, int paramInt2, int paramInt3, int paramInt4, float paramFloat1, float paramFloat2, boolean paramBoolean, Paint paramPaint) {
    checkRange("drawTextRun", paramInt1, paramInt2);
    int i = paramInt4 - paramInt3;
    paramInt2 -= paramInt1;
    int j = this.mGapStart;
    if (paramInt4 <= j) {
      paramBaseCanvas.drawTextRun(this.mText, paramInt1, paramInt2, paramInt3, i, paramFloat1, paramFloat2, paramBoolean, paramPaint);
    } else if (paramInt3 >= j) {
      char[] arrayOfChar = this.mText;
      paramInt4 = this.mGapLength;
      paramBaseCanvas.drawTextRun(arrayOfChar, paramInt1 + paramInt4, paramInt2, paramInt3 + paramInt4, i, paramFloat1, paramFloat2, paramBoolean, paramPaint);
    } else {
      char[] arrayOfChar = TextUtils.obtain(i);
      getChars(paramInt3, paramInt4, arrayOfChar, 0);
      paramBaseCanvas.drawTextRun(arrayOfChar, paramInt1 - paramInt3, paramInt2, 0, i, paramFloat1, paramFloat2, paramBoolean, paramPaint);
      TextUtils.recycle(arrayOfChar);
    } 
  }
  
  public float measureText(int paramInt1, int paramInt2, Paint paramPaint) {
    float f;
    checkRange("measureText", paramInt1, paramInt2);
    int i = this.mGapStart;
    if (paramInt2 <= i) {
      f = paramPaint.measureText(this.mText, paramInt1, paramInt2 - paramInt1);
    } else if (paramInt1 >= i) {
      f = paramPaint.measureText(this.mText, this.mGapLength + paramInt1, paramInt2 - paramInt1);
    } else {
      char[] arrayOfChar = TextUtils.obtain(paramInt2 - paramInt1);
      getChars(paramInt1, paramInt2, arrayOfChar, 0);
      f = paramPaint.measureText(arrayOfChar, 0, paramInt2 - paramInt1);
      TextUtils.recycle(arrayOfChar);
    } 
    return f;
  }
  
  public int getTextWidths(int paramInt1, int paramInt2, float[] paramArrayOffloat, Paint paramPaint) {
    checkRange("getTextWidths", paramInt1, paramInt2);
    int i = this.mGapStart;
    if (paramInt2 <= i) {
      paramInt1 = paramPaint.getTextWidths(this.mText, paramInt1, paramInt2 - paramInt1, paramArrayOffloat);
    } else if (paramInt1 >= i) {
      paramInt1 = paramPaint.getTextWidths(this.mText, this.mGapLength + paramInt1, paramInt2 - paramInt1, paramArrayOffloat);
    } else {
      char[] arrayOfChar = TextUtils.obtain(paramInt2 - paramInt1);
      getChars(paramInt1, paramInt2, arrayOfChar, 0);
      paramInt1 = paramPaint.getTextWidths(arrayOfChar, 0, paramInt2 - paramInt1, paramArrayOffloat);
      TextUtils.recycle(arrayOfChar);
    } 
    return paramInt1;
  }
  
  public float getTextRunAdvances(int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean, float[] paramArrayOffloat, int paramInt5, Paint paramPaint) {
    float f;
    int i = paramInt4 - paramInt3;
    int j = paramInt2 - paramInt1;
    int k = this.mGapStart;
    if (paramInt2 <= k) {
      f = paramPaint.getTextRunAdvances(this.mText, paramInt1, j, paramInt3, i, paramBoolean, paramArrayOffloat, paramInt5);
    } else if (paramInt1 >= k) {
      char[] arrayOfChar = this.mText;
      paramInt2 = this.mGapLength;
      f = paramPaint.getTextRunAdvances(arrayOfChar, paramInt1 + paramInt2, j, paramInt3 + paramInt2, i, paramBoolean, paramArrayOffloat, paramInt5);
    } else {
      char[] arrayOfChar = TextUtils.obtain(i);
      getChars(paramInt3, paramInt4, arrayOfChar, 0);
      f = paramPaint.getTextRunAdvances(arrayOfChar, paramInt1 - paramInt3, j, 0, i, paramBoolean, paramArrayOffloat, paramInt5);
      TextUtils.recycle(arrayOfChar);
    } 
    return f;
  }
  
  @Deprecated
  public int getTextRunCursor(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, Paint paramPaint) {
    boolean bool = true;
    if (paramInt3 != 1)
      bool = false; 
    return getTextRunCursor(paramInt1, paramInt2, bool, paramInt4, paramInt5, paramPaint);
  }
  
  public int getTextRunCursor(int paramInt1, int paramInt2, boolean paramBoolean, int paramInt3, int paramInt4, Paint paramPaint) {
    int i = paramInt2 - paramInt1;
    int j = this.mGapStart;
    if (paramInt2 <= j) {
      paramInt1 = paramPaint.getTextRunCursor(this.mText, paramInt1, i, paramBoolean, paramInt3, paramInt4);
    } else if (paramInt1 >= j) {
      char[] arrayOfChar = this.mText;
      paramInt2 = this.mGapLength;
      paramInt1 = paramPaint.getTextRunCursor(arrayOfChar, paramInt1 + paramInt2, i, paramBoolean, paramInt3 + paramInt2, paramInt4) - this.mGapLength;
    } else {
      char[] arrayOfChar = TextUtils.obtain(i);
      getChars(paramInt1, paramInt2, arrayOfChar, 0);
      paramInt1 = paramPaint.getTextRunCursor(arrayOfChar, 0, i, paramBoolean, paramInt3 - paramInt1, paramInt4) + paramInt1;
      TextUtils.recycle(arrayOfChar);
    } 
    return paramInt1;
  }
  
  public void setFilters(InputFilter[] paramArrayOfInputFilter) {
    if (paramArrayOfInputFilter != null) {
      this.mFilters = paramArrayOfInputFilter;
      return;
    } 
    throw new IllegalArgumentException();
  }
  
  public InputFilter[] getFilters() {
    return this.mFilters;
  }
  
  public boolean equals(Object paramObject) {
    if (paramObject instanceof Spanned && 
      toString().equals(paramObject.toString())) {
      paramObject = paramObject;
      Object[] arrayOfObject1 = paramObject.getSpans(0, paramObject.length(), Object.class);
      Object[] arrayOfObject2 = getSpans(0, length(), Object.class);
      if (this.mSpanCount == arrayOfObject1.length) {
        for (byte b = 0; b < this.mSpanCount; b++) {
          Object object1 = arrayOfObject2[b];
          Object object2 = arrayOfObject1[b];
          if (object1 == this) {
            if (paramObject != object2 || 
              getSpanStart(object1) != paramObject.getSpanStart(object2) || 
              getSpanEnd(object1) != paramObject.getSpanEnd(object2) || 
              getSpanFlags(object1) != paramObject.getSpanFlags(object2))
              return false; 
          } else if (!object1.equals(object2) || 
            getSpanStart(object1) != paramObject.getSpanStart(object2) || 
            getSpanEnd(object1) != paramObject.getSpanEnd(object2) || 
            getSpanFlags(object1) != paramObject.getSpanFlags(object2)) {
            return false;
          } 
        } 
        return true;
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    int i = toString().hashCode();
    i = i * 31 + this.mSpanCount;
    for (byte b = 0; b < this.mSpanCount; b++) {
      Object object = this.mSpans[b];
      int j = i;
      if (object != this)
        j = i * 31 + object.hashCode(); 
      i = getSpanStart(object);
      int k = getSpanEnd(object);
      i = ((j * 31 + i) * 31 + k) * 31 + getSpanFlags(object);
    } 
    return i;
  }
  
  private int treeRoot() {
    return Integer.highestOneBit(this.mSpanCount) - 1;
  }
  
  private static int leftChild(int paramInt) {
    return paramInt - ((paramInt + 1 & (paramInt ^ 0xFFFFFFFF)) >> 1);
  }
  
  private static int rightChild(int paramInt) {
    return ((paramInt + 1 & (paramInt ^ 0xFFFFFFFF)) >> 1) + paramInt;
  }
  
  private int calcMax(int paramInt) {
    int i = 0;
    if ((paramInt & 0x1) != 0)
      i = calcMax(leftChild(paramInt)); 
    int j = i;
    if (paramInt < this.mSpanCount) {
      i = Math.max(i, this.mSpanEnds[paramInt]);
      j = i;
      if ((paramInt & 0x1) != 0)
        j = Math.max(i, calcMax(rightChild(paramInt))); 
    } 
    this.mSpanMax[paramInt] = j;
    return j;
  }
  
  private void restoreInvariants() {
    if (this.mSpanCount == 0)
      return; 
    int i;
    for (i = 1; i < this.mSpanCount; i++) {
      int[] arrayOfInt = this.mSpanStarts;
      if (arrayOfInt[i] < arrayOfInt[i - 1]) {
        int i2;
        Object object = this.mSpans[i];
        int j = arrayOfInt[i];
        int k = this.mSpanEnds[i];
        int m = this.mSpanFlags[i];
        int n = this.mSpanOrder[i];
        int i1 = i;
        while (true) {
          Object[] arrayOfObject = this.mSpans;
          arrayOfObject[i1] = arrayOfObject[i1 - 1];
          int[] arrayOfInt1 = this.mSpanStarts;
          arrayOfInt1[i1] = arrayOfInt1[i1 - 1];
          int[] arrayOfInt2 = this.mSpanEnds;
          arrayOfInt2[i1] = arrayOfInt2[i1 - 1];
          arrayOfInt2 = this.mSpanFlags;
          arrayOfInt2[i1] = arrayOfInt2[i1 - 1];
          arrayOfInt2 = this.mSpanOrder;
          arrayOfInt2[i1] = arrayOfInt2[i1 - 1];
          i2 = i1 - 1;
          if (i2 > 0) {
            i1 = i2;
            if (j >= arrayOfInt1[i2 - 1])
              break; 
            continue;
          } 
          break;
        } 
        this.mSpans[i2] = object;
        this.mSpanStarts[i2] = j;
        this.mSpanEnds[i2] = k;
        this.mSpanFlags[i2] = m;
        this.mSpanOrder[i2] = n;
        invalidateIndex(i2);
      } 
    } 
    calcMax(treeRoot());
    if (this.mIndexOfSpan == null)
      this.mIndexOfSpan = new IdentityHashMap<>(); 
    for (i = this.mLowWaterMark; i < this.mSpanCount; i++) {
      Integer integer = this.mIndexOfSpan.get(this.mSpans[i]);
      if (integer == null || integer.intValue() != i)
        this.mIndexOfSpan.put(this.mSpans[i], Integer.valueOf(i)); 
    } 
    this.mLowWaterMark = Integer.MAX_VALUE;
  }
  
  private void invalidateIndex(int paramInt) {
    this.mLowWaterMark = Math.min(paramInt, this.mLowWaterMark);
  }
  
  private static final InputFilter[] NO_FILTERS = new InputFilter[0];
  
  private static final int PARAGRAPH = 3;
  
  private static final int POINT = 2;
  
  private static final int SPAN_ADDED = 2048;
  
  private static final int SPAN_END_AT_END = 32768;
  
  private static final int SPAN_END_AT_START = 16384;
  
  private static final int SPAN_START_AT_END = 8192;
  
  private static final int SPAN_START_AT_START = 4096;
  
  private static final int SPAN_START_END_MASK = 61440;
  
  private static final int START_MASK = 240;
  
  private static final int START_SHIFT = 4;
  
  private static final String TAG = "SpannableStringBuilder";
  
  private static final int[][] sCachedIntBuffer = new int[6][0];
  
  private InputFilter[] mFilters;
  
  private int mGapLength;
  
  private int mGapStart;
  
  private IdentityHashMap<Object, Integer> mIndexOfSpan;
  
  private int mLowWaterMark;
  
  private int mSpanCount;
  
  private int[] mSpanEnds;
  
  private int[] mSpanFlags;
  
  private int mSpanInsertCount;
  
  private int[] mSpanMax;
  
  private int[] mSpanOrder;
  
  private int[] mSpanStarts;
  
  private Object[] mSpans;
  
  private char[] mText;
  
  private int mTextWatcherDepth;
}
