package android.text;

import android.icu.text.Bidi;

public class AndroidBidi {
  public static int bidi(int paramInt, char[] paramArrayOfchar, byte[] paramArrayOfbyte) {
    if (paramArrayOfchar != null && paramArrayOfbyte != null) {
      int i = paramArrayOfchar.length;
      if (paramArrayOfbyte.length >= i) {
        byte b1;
        byte b = -1;
        if (paramInt != -2) {
          if (paramInt != -1) {
            if (paramInt != 1) {
              if (paramInt != 2) {
                b1 = 0;
              } else {
                b1 = 126;
              } 
            } else {
              b1 = 0;
            } 
          } else {
            b1 = 1;
          } 
        } else {
          b1 = 127;
        } 
        Bidi bidi = new Bidi(i, 0);
        bidi.setPara(paramArrayOfchar, b1, null);
        for (paramInt = 0; paramInt < i; paramInt++)
          paramArrayOfbyte[paramInt] = bidi.getLevelAt(paramInt); 
        i = bidi.getParaLevel();
        paramInt = b;
        if ((i & 0x1) == 0)
          paramInt = 1; 
        return paramInt;
      } 
      throw new IndexOutOfBoundsException();
    } 
    throw null;
  }
  
  public static Layout.Directions directions(int paramInt1, byte[] paramArrayOfbyte, int paramInt2, char[] paramArrayOfchar, int paramInt3, int paramInt4) {
    byte b;
    if (paramInt4 == 0)
      return Layout.DIRS_ALL_LEFT_TO_RIGHT; 
    if (paramInt1 == 1) {
      b = 0;
    } else {
      b = 1;
    } 
    byte b1 = paramArrayOfbyte[paramInt2];
    paramInt1 = b1;
    int i = 1;
    int j;
    for (j = paramInt2 + 1; j < paramInt2 + paramInt4; j++, b1 = b3, i = i4) {
      byte b2 = paramArrayOfbyte[j];
      byte b3 = b1;
      int i4 = i;
      if (b2 != b1) {
        b3 = b2;
        i4 = i + 1;
      } 
    } 
    j = paramInt4;
    int k = i, m = j;
    if ((b1 & 0x1) != (b & 0x1)) {
      int i4;
      while (true) {
        i4 = --j;
        if (j >= 0) {
          i4 = paramArrayOfchar[paramInt3 + j];
          if (i4 == 10) {
            i4 = j - 1;
            break;
          } 
          if (i4 != 32 && i4 != 9) {
            i4 = j;
            break;
          } 
          continue;
        } 
        break;
      } 
      paramInt3 = i4 + 1;
      k = i;
      m = paramInt3;
      if (paramInt3 != paramInt4) {
        k = i + 1;
        m = paramInt3;
      } 
    } 
    if (k == 1 && paramInt1 == b) {
      if ((paramInt1 & 0x1) != 0)
        return Layout.DIRS_ALL_RIGHT_TO_LEFT; 
      return Layout.DIRS_ALL_LEFT_TO_RIGHT;
    } 
    int[] arrayOfInt = new int[k * 2];
    paramInt3 = paramInt1;
    int n = paramInt1 << 26;
    int i1 = 1;
    int i2 = paramInt2;
    int i3 = paramInt1;
    for (i = paramInt2; i < paramInt2 + m; i++, i3 = i5, paramInt1 = j, paramInt3 = i4, n = i6, i1 = i7, i2 = i8) {
      byte b2 = paramArrayOfbyte[i];
      int i5 = i3;
      j = paramInt1;
      int i4 = paramInt3, i6 = n, i7 = i1, i8 = i2;
      if (b2 != i3) {
        i3 = b2;
        if (b2 > paramInt3) {
          i4 = b2;
          j = paramInt1;
        } else {
          j = paramInt1;
          i4 = paramInt3;
          if (b2 < paramInt1) {
            j = b2;
            i4 = paramInt3;
          } 
        } 
        paramInt1 = i1 + 1;
        arrayOfInt[i1] = i - i2 | n;
        i7 = paramInt1 + 1;
        arrayOfInt[paramInt1] = i - paramInt2;
        i6 = i3 << 26;
        i8 = i;
        i5 = i3;
      } 
    } 
    arrayOfInt[i1] = paramInt2 + m - i2 | n;
    if (m < paramInt4) {
      paramInt2 = i1 + 1;
      arrayOfInt[paramInt2] = m;
      arrayOfInt[paramInt2 + 1] = paramInt4 - m | b << 26;
    } 
    if ((paramInt1 & 0x1) == b) {
      paramInt4 = paramInt1 + 1;
      if (paramInt3 > paramInt4) {
        paramInt2 = 1;
      } else {
        paramInt2 = 0;
      } 
    } else if (k > 1) {
      paramInt2 = 1;
      paramInt4 = paramInt1;
    } else {
      paramInt2 = 0;
      paramInt4 = paramInt1;
    } 
    if (paramInt2 != 0)
      for (paramInt2 = paramInt3 - 1; paramInt2 >= paramInt4; paramInt2--) {
        for (paramInt1 = 0; paramInt1 < arrayOfInt.length; paramInt1 = paramInt3 + 2) {
          paramInt3 = paramInt1;
          if (paramArrayOfbyte[arrayOfInt[paramInt1]] >= paramInt2) {
            paramInt3 = paramInt1 + 2;
            while (paramInt3 < arrayOfInt.length && paramArrayOfbyte[arrayOfInt[paramInt3]] >= paramInt2)
              paramInt3 += 2; 
            for (i = paramInt1, paramInt1 = paramInt3 - 2; i < paramInt1; i += 2, paramInt1 -= 2) {
              j = arrayOfInt[i];
              arrayOfInt[i] = arrayOfInt[paramInt1];
              arrayOfInt[paramInt1] = j;
              j = arrayOfInt[i + 1];
              arrayOfInt[i + 1] = arrayOfInt[paramInt1 + 1];
              arrayOfInt[paramInt1 + 1] = j;
            } 
            paramInt3 += 2;
          } 
        } 
      }  
    return new Layout.Directions(arrayOfInt);
  }
}
