package android.text;

import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.view.View;
import com.android.internal.util.XmlUtils;
import java.io.IOException;
import java.util.Locale;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class AutoText {
  private static final int DEFAULT = 14337;
  
  private static final int INCREMENT = 1024;
  
  private static final int RIGHT = 9300;
  
  private static final int TRIE_C = 0;
  
  private static final int TRIE_CHILD = 2;
  
  private static final int TRIE_NEXT = 3;
  
  private static final char TRIE_NULL = '￿';
  
  private static final int TRIE_OFF = 1;
  
  private static final int TRIE_ROOT = 0;
  
  private static final int TRIE_SIZEOF = 4;
  
  private static AutoText sInstance = new AutoText(Resources.getSystem());
  
  private static Object sLock = new Object();
  
  private Locale mLocale;
  
  private int mSize;
  
  private String mText;
  
  private char[] mTrie;
  
  private char mTrieUsed;
  
  private AutoText(Resources paramResources) {
    this.mLocale = (paramResources.getConfiguration()).locale;
    init(paramResources);
  }
  
  private static AutoText getInstance(View paramView) {
    Resources resources = paramView.getContext().getResources();
    Locale locale = (resources.getConfiguration()).locale;
    synchronized (sLock) {
      AutoText autoText2 = sInstance;
      AutoText autoText1 = autoText2;
      if (!locale.equals(autoText2.mLocale)) {
        autoText1 = new AutoText();
        this(resources);
        sInstance = autoText1;
      } 
      return autoText1;
    } 
  }
  
  public static String get(CharSequence paramCharSequence, int paramInt1, int paramInt2, View paramView) {
    return getInstance(paramView).lookup(paramCharSequence, paramInt1, paramInt2);
  }
  
  public static int getSize(View paramView) {
    return getInstance(paramView).getSize();
  }
  
  private int getSize() {
    return this.mSize;
  }
  
  private String lookup(CharSequence paramCharSequence, int paramInt1, int paramInt2) {
    char c = this.mTrie[0];
    for (int i = paramInt1; i < paramInt2; i++) {
      char c1 = paramCharSequence.charAt(i);
      int j = paramInt1;
      while (true) {
        paramInt1 = j;
        if (j != 65535) {
          char[] arrayOfChar = this.mTrie;
          if (c1 == arrayOfChar[j + 0]) {
            if (i == paramInt2 - 1 && arrayOfChar[j + 1] != Character.MAX_VALUE) {
              paramInt1 = arrayOfChar[j + 1];
              paramInt2 = this.mText.charAt(paramInt1);
              return this.mText.substring(paramInt1 + 1, paramInt1 + 1 + paramInt2);
            } 
            paramInt1 = this.mTrie[j + 2];
            break;
          } 
          j = arrayOfChar[j + 3];
          continue;
        } 
        break;
      } 
      if (paramInt1 == 65535)
        return null; 
    } 
    return null;
  }
  
  private void init(Resources paramResources) {
    XmlResourceParser xmlResourceParser = paramResources.getXml(18284547);
    StringBuilder stringBuilder = new StringBuilder(9300);
    char[] arrayOfChar = new char[14337];
    arrayOfChar[0] = Character.MAX_VALUE;
    this.mTrieUsed = '\001';
    try {
      XmlUtils.beginDocument((XmlPullParser)xmlResourceParser, "words");
      while (true) {
        XmlUtils.nextElement((XmlPullParser)xmlResourceParser);
        String str = xmlResourceParser.getName();
        if (str == null || !str.equals("word"))
          break; 
        str = xmlResourceParser.getAttributeValue(null, "src");
        if (xmlResourceParser.next() == 4) {
          char c;
          String str1 = xmlResourceParser.getText();
          if (str1.equals("")) {
            c = Character.MIN_VALUE;
          } else {
            c = (char)stringBuilder.length();
            stringBuilder.append((char)str1.length());
            stringBuilder.append(str1);
          } 
          add(str, c);
        } 
      } 
      paramResources.flushLayoutCache();
      xmlResourceParser.close();
      this.mText = stringBuilder.toString();
      return;
    } catch (XmlPullParserException xmlPullParserException) {
      RuntimeException runtimeException = new RuntimeException();
      this((Throwable)xmlPullParserException);
      throw runtimeException;
    } catch (IOException iOException) {
      RuntimeException runtimeException = new RuntimeException();
      this(iOException);
      throw runtimeException;
    } finally {}
    xmlResourceParser.close();
    throw paramResources;
  }
  
  private void add(String paramString, char paramChar) {
    int i = paramString.length();
    int j = 0;
    this.mSize++;
    for (byte b = 0; b < i; b++) {
      int k;
      boolean bool2;
      char c = paramString.charAt(b);
      boolean bool1 = false;
      while (true) {
        char[] arrayOfChar = this.mTrie;
        k = j;
        bool2 = bool1;
        if (arrayOfChar[j] != Character.MAX_VALUE) {
          if (c == arrayOfChar[arrayOfChar[j] + 0]) {
            if (b == i - 1) {
              arrayOfChar[arrayOfChar[j] + 1] = paramChar;
              return;
            } 
            k = arrayOfChar[j] + 2;
            bool2 = true;
            break;
          } 
          j = arrayOfChar[j] + 3;
          continue;
        } 
        break;
      } 
      j = k;
      if (!bool2) {
        char c1 = newTrieNode();
        char[] arrayOfChar = this.mTrie;
        arrayOfChar[k] = c1;
        arrayOfChar[arrayOfChar[k] + 0] = c;
        arrayOfChar[arrayOfChar[k] + 1] = Character.MAX_VALUE;
        arrayOfChar[arrayOfChar[k] + 3] = Character.MAX_VALUE;
        arrayOfChar[arrayOfChar[k] + 2] = Character.MAX_VALUE;
        if (b == i - 1) {
          arrayOfChar[arrayOfChar[k] + 1] = paramChar;
          return;
        } 
        j = arrayOfChar[k] + 2;
      } 
    } 
  }
  
  private char newTrieNode() {
    char c1 = this.mTrieUsed, arrayOfChar[] = this.mTrie;
    if (c1 + 4 > arrayOfChar.length) {
      char[] arrayOfChar1 = new char[arrayOfChar.length + 1024];
      System.arraycopy(arrayOfChar, 0, arrayOfChar1, 0, arrayOfChar.length);
      this.mTrie = arrayOfChar1;
    } 
    char c2 = this.mTrieUsed;
    this.mTrieUsed = (char)(this.mTrieUsed + 4);
    return c2;
  }
}
