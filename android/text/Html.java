package android.text;

import android.app.ActivityThread;
import android.app.Application;
import android.graphics.drawable.Drawable;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.AlignmentSpan;
import android.text.style.BackgroundColorSpan;
import android.text.style.CharacterStyle;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.text.style.ParagraphStyle;
import android.text.style.QuoteSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.text.style.TypefaceSpan;
import android.text.style.URLSpan;
import org.ccil.cowan.tagsoup.HTMLSchema;
import org.ccil.cowan.tagsoup.Parser;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.XMLReader;

public class Html {
  public static final int FROM_HTML_MODE_COMPACT = 63;
  
  public static final int FROM_HTML_MODE_LEGACY = 0;
  
  public static final int FROM_HTML_OPTION_USE_CSS_COLORS = 256;
  
  public static final int FROM_HTML_SEPARATOR_LINE_BREAK_BLOCKQUOTE = 32;
  
  public static final int FROM_HTML_SEPARATOR_LINE_BREAK_DIV = 16;
  
  public static final int FROM_HTML_SEPARATOR_LINE_BREAK_HEADING = 2;
  
  public static final int FROM_HTML_SEPARATOR_LINE_BREAK_LIST = 8;
  
  public static final int FROM_HTML_SEPARATOR_LINE_BREAK_LIST_ITEM = 4;
  
  public static final int FROM_HTML_SEPARATOR_LINE_BREAK_PARAGRAPH = 1;
  
  private static final int TO_HTML_PARAGRAPH_FLAG = 1;
  
  public static final int TO_HTML_PARAGRAPH_LINES_CONSECUTIVE = 0;
  
  public static final int TO_HTML_PARAGRAPH_LINES_INDIVIDUAL = 1;
  
  @Deprecated
  public static Spanned fromHtml(String paramString) {
    return fromHtml(paramString, 0, null, null);
  }
  
  public static Spanned fromHtml(String paramString, int paramInt) {
    return fromHtml(paramString, paramInt, null, null);
  }
  
  private static class HtmlParser {
    private static final HTMLSchema schema = new HTMLSchema();
  }
  
  @Deprecated
  public static Spanned fromHtml(String paramString, ImageGetter paramImageGetter, TagHandler paramTagHandler) {
    return fromHtml(paramString, 0, paramImageGetter, paramTagHandler);
  }
  
  public static Spanned fromHtml(String paramString, int paramInt, ImageGetter paramImageGetter, TagHandler paramTagHandler) {
    Parser parser = new Parser();
    try {
      parser.setProperty("http://www.ccil.org/~cowan/tagsoup/properties/schema", HtmlParser.schema);
      HtmlToSpannedConverter htmlToSpannedConverter = new HtmlToSpannedConverter(paramString, paramImageGetter, paramTagHandler, parser, paramInt);
      return htmlToSpannedConverter.convert();
    } catch (SAXNotRecognizedException sAXNotRecognizedException) {
      throw new RuntimeException(sAXNotRecognizedException);
    } catch (SAXNotSupportedException sAXNotSupportedException) {
      throw new RuntimeException(sAXNotSupportedException);
    } 
  }
  
  @Deprecated
  public static String toHtml(Spanned paramSpanned) {
    return toHtml(paramSpanned, 0);
  }
  
  public static String toHtml(Spanned paramSpanned, int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    withinHtml(stringBuilder, paramSpanned, paramInt);
    return stringBuilder.toString();
  }
  
  public static String escapeHtml(CharSequence paramCharSequence) {
    StringBuilder stringBuilder = new StringBuilder();
    withinStyle(stringBuilder, paramCharSequence, 0, paramCharSequence.length());
    return stringBuilder.toString();
  }
  
  private static void withinHtml(StringBuilder paramStringBuilder, Spanned paramSpanned, int paramInt) {
    if ((paramInt & 0x1) == 0) {
      encodeTextAlignmentByDiv(paramStringBuilder, paramSpanned, paramInt);
      return;
    } 
    withinDiv(paramStringBuilder, paramSpanned, 0, paramSpanned.length(), paramInt);
  }
  
  private static void encodeTextAlignmentByDiv(StringBuilder paramStringBuilder, Spanned paramSpanned, int paramInt) {
    int i = paramSpanned.length();
    int j;
    for (j = 0; j < i; j = k) {
      int k = paramSpanned.nextSpanTransition(j, i, ParagraphStyle.class);
      ParagraphStyle[] arrayOfParagraphStyle = paramSpanned.<ParagraphStyle>getSpans(j, k, ParagraphStyle.class);
      String str = " ";
      boolean bool = false;
      for (byte b = 0; b < arrayOfParagraphStyle.length; b++, str = str1) {
        String str1 = str;
        if (arrayOfParagraphStyle[b] instanceof AlignmentSpan) {
          String str2;
          AlignmentSpan alignmentSpan = (AlignmentSpan)arrayOfParagraphStyle[b];
          Layout.Alignment alignment = alignmentSpan.getAlignment();
          bool = true;
          if (alignment == Layout.Alignment.ALIGN_CENTER) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("align=\"center\" ");
            stringBuilder.append(str);
            str2 = stringBuilder.toString();
          } else if (str2 == Layout.Alignment.ALIGN_OPPOSITE) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("align=\"right\" ");
            stringBuilder.append(str);
            String str3 = stringBuilder.toString();
          } else {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("align=\"left\" ");
            stringBuilder.append(str);
            str1 = stringBuilder.toString();
          } 
        } 
      } 
      if (bool) {
        paramStringBuilder.append("<div ");
        paramStringBuilder.append(str);
        paramStringBuilder.append(">");
      } 
      withinDiv(paramStringBuilder, paramSpanned, j, k, paramInt);
      if (bool)
        paramStringBuilder.append("</div>"); 
    } 
  }
  
  private static void withinDiv(StringBuilder paramStringBuilder, Spanned paramSpanned, int paramInt1, int paramInt2, int paramInt3) {
    for (; paramInt1 < paramInt2; paramInt1 = i) {
      int i = paramSpanned.nextSpanTransition(paramInt1, paramInt2, QuoteSpan.class);
      QuoteSpan[] arrayOfQuoteSpan = paramSpanned.<QuoteSpan>getSpans(paramInt1, i, QuoteSpan.class);
      boolean bool;
      int k;
      for (int j = arrayOfQuoteSpan.length; k < j; ) {
        QuoteSpan quoteSpan = arrayOfQuoteSpan[k];
        paramStringBuilder.append("<blockquote>");
        k++;
      } 
      withinBlockquote(paramStringBuilder, paramSpanned, paramInt1, i, paramInt3);
      for (k = arrayOfQuoteSpan.length, paramInt1 = bool; paramInt1 < k; ) {
        QuoteSpan quoteSpan = arrayOfQuoteSpan[paramInt1];
        paramStringBuilder.append("</blockquote>\n");
        paramInt1++;
      } 
    } 
  }
  
  private static String getTextDirection(Spanned paramSpanned, int paramInt1, int paramInt2) {
    if (TextDirectionHeuristics.FIRSTSTRONG_LTR.isRtl(paramSpanned, paramInt1, paramInt2 - paramInt1))
      return " dir=\"rtl\""; 
    return " dir=\"ltr\"";
  }
  
  private static String getTextStyles(Spanned paramSpanned, int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2) {
    String str2, str1 = null;
    AlignmentSpan alignmentSpan1 = null;
    if (paramBoolean1)
      str1 = "margin-top:0; margin-bottom:0;"; 
    AlignmentSpan alignmentSpan2 = alignmentSpan1;
    if (paramBoolean2) {
      AlignmentSpan[] arrayOfAlignmentSpan = paramSpanned.<AlignmentSpan>getSpans(paramInt1, paramInt2, AlignmentSpan.class);
      paramInt1 = arrayOfAlignmentSpan.length - 1;
      while (true) {
        alignmentSpan2 = alignmentSpan1;
        if (paramInt1 >= 0) {
          alignmentSpan2 = arrayOfAlignmentSpan[paramInt1];
          if ((paramSpanned.getSpanFlags(alignmentSpan2) & 0x33) == 51) {
            Layout.Alignment alignment = alignmentSpan2.getAlignment();
            if (alignment == Layout.Alignment.ALIGN_NORMAL) {
              str2 = "text-align:start;";
              break;
            } 
            if (alignment == Layout.Alignment.ALIGN_CENTER) {
              str2 = "text-align:center;";
              break;
            } 
            alignmentSpan2 = alignmentSpan1;
            if (alignment == Layout.Alignment.ALIGN_OPPOSITE)
              str2 = "text-align:end;"; 
            break;
          } 
          paramInt1--;
          continue;
        } 
        break;
      } 
    } 
    if (str1 == null && str2 == null)
      return ""; 
    StringBuilder stringBuilder = new StringBuilder(" style=\"");
    if (str1 != null && str2 != null) {
      stringBuilder.append(str1);
      stringBuilder.append(" ");
      stringBuilder.append(str2);
    } else if (str1 != null) {
      stringBuilder.append(str1);
    } else if (str2 != null) {
      stringBuilder.append(str2);
    } 
    stringBuilder.append("\"");
    return stringBuilder.toString();
  }
  
  private static void withinBlockquote(StringBuilder paramStringBuilder, Spanned paramSpanned, int paramInt1, int paramInt2, int paramInt3) {
    if ((paramInt3 & 0x1) == 0) {
      withinBlockquoteConsecutive(paramStringBuilder, paramSpanned, paramInt1, paramInt2);
    } else {
      withinBlockquoteIndividual(paramStringBuilder, paramSpanned, paramInt1, paramInt2);
    } 
  }
  
  private static void withinBlockquoteIndividual(StringBuilder paramStringBuilder, Spanned paramSpanned, int paramInt1, int paramInt2) {
    int i = 0;
    int j;
    for (j = paramInt1, paramInt1 = i; j <= paramInt2; j = k + 1) {
      i = TextUtils.indexOf(paramSpanned, '\n', j, paramInt2);
      int k = i;
      if (i < 0)
        k = paramInt2; 
      if (k == j) {
        i = paramInt1;
        if (paramInt1 != 0) {
          i = 0;
          paramStringBuilder.append("</ul>\n");
        } 
        paramStringBuilder.append("<br>\n");
        paramInt1 = i;
      } else {
        int i1;
        String str;
        int m = 0;
        ParagraphStyle[] arrayOfParagraphStyle = paramSpanned.<ParagraphStyle>getSpans(j, k, ParagraphStyle.class);
        int n = arrayOfParagraphStyle.length;
        i = 0;
        while (true) {
          i1 = m;
          if (i < n) {
            ParagraphStyle paragraphStyle = arrayOfParagraphStyle[i];
            i1 = paramSpanned.getSpanFlags(paragraphStyle);
            if ((i1 & 0x33) == 51 && paragraphStyle instanceof android.text.style.BulletSpan) {
              i1 = 1;
              break;
            } 
            i++;
            continue;
          } 
          break;
        } 
        m = paramInt1;
        if (i1 != 0) {
          m = paramInt1;
          if (paramInt1 == 0) {
            m = 1;
            paramStringBuilder.append("<ul");
            paramStringBuilder.append(getTextStyles(paramSpanned, j, k, true, false));
            paramStringBuilder.append(">\n");
          } 
        } 
        i = m;
        if (m != 0) {
          i = m;
          if (i1 == 0) {
            i = 0;
            paramStringBuilder.append("</ul>\n");
          } 
        } 
        if (i1 != 0) {
          str = "li";
        } else {
          str = "p";
        } 
        paramStringBuilder.append("<");
        paramStringBuilder.append(str);
        paramStringBuilder.append(getTextDirection(paramSpanned, j, k));
        paramStringBuilder.append(getTextStyles(paramSpanned, j, k, i1 ^ 0x1, true));
        paramStringBuilder.append(">");
        withinParagraph(paramStringBuilder, paramSpanned, j, k);
        paramStringBuilder.append("</");
        paramStringBuilder.append(str);
        paramStringBuilder.append(">\n");
        paramInt1 = i;
        if (k == paramInt2) {
          paramInt1 = i;
          if (i != 0) {
            paramInt1 = 0;
            paramStringBuilder.append("</ul>\n");
          } 
        } 
      } 
    } 
  }
  
  private static void withinBlockquoteConsecutive(StringBuilder paramStringBuilder, Spanned paramSpanned, int paramInt1, int paramInt2) {
    paramStringBuilder.append("<p");
    paramStringBuilder.append(getTextDirection(paramSpanned, paramInt1, paramInt2));
    paramStringBuilder.append(">");
    int i;
    for (i = paramInt1; i < paramInt2; i = k) {
      int j = TextUtils.indexOf(paramSpanned, '\n', i, paramInt2);
      int k = j;
      if (j < 0)
        k = paramInt2; 
      j = 0;
      while (k < paramInt2 && paramSpanned.charAt(k) == '\n') {
        j++;
        k++;
      } 
      withinParagraph(paramStringBuilder, paramSpanned, i, k - j);
      if (j == 1) {
        paramStringBuilder.append("<br>\n");
      } else {
        for (i = 2; i < j; i++)
          paramStringBuilder.append("<br>"); 
        if (k != paramInt2) {
          paramStringBuilder.append("</p>\n");
          paramStringBuilder.append("<p");
          paramStringBuilder.append(getTextDirection(paramSpanned, paramInt1, paramInt2));
          paramStringBuilder.append(">");
        } 
      } 
    } 
    paramStringBuilder.append("</p>\n");
  }
  
  private static void withinParagraph(StringBuilder paramStringBuilder, Spanned paramSpanned, int paramInt1, int paramInt2) {
    for (; paramInt1 < paramInt2; paramInt1 = i) {
      int i = paramSpanned.nextSpanTransition(paramInt1, paramInt2, CharacterStyle.class);
      CharacterStyle[] arrayOfCharacterStyle = paramSpanned.<CharacterStyle>getSpans(paramInt1, i, CharacterStyle.class);
      int j;
      for (j = 0; j < arrayOfCharacterStyle.length; j++) {
        if (arrayOfCharacterStyle[j] instanceof StyleSpan) {
          int k = ((StyleSpan)arrayOfCharacterStyle[j]).getStyle();
          if ((k & 0x1) != 0)
            paramStringBuilder.append("<b>"); 
          if ((k & 0x2) != 0)
            paramStringBuilder.append("<i>"); 
        } 
        if (arrayOfCharacterStyle[j] instanceof TypefaceSpan) {
          String str = ((TypefaceSpan)arrayOfCharacterStyle[j]).getFamily();
          if ("monospace".equals(str))
            paramStringBuilder.append("<tt>"); 
        } 
        if (arrayOfCharacterStyle[j] instanceof android.text.style.SuperscriptSpan)
          paramStringBuilder.append("<sup>"); 
        if (arrayOfCharacterStyle[j] instanceof android.text.style.SubscriptSpan)
          paramStringBuilder.append("<sub>"); 
        if (arrayOfCharacterStyle[j] instanceof android.text.style.UnderlineSpan)
          paramStringBuilder.append("<u>"); 
        if (arrayOfCharacterStyle[j] instanceof android.text.style.StrikethroughSpan)
          paramStringBuilder.append("<span style=\"text-decoration:line-through;\">"); 
        if (arrayOfCharacterStyle[j] instanceof URLSpan) {
          paramStringBuilder.append("<a href=\"");
          paramStringBuilder.append(((URLSpan)arrayOfCharacterStyle[j]).getURL());
          paramStringBuilder.append("\">");
        } 
        if (arrayOfCharacterStyle[j] instanceof ImageSpan) {
          paramStringBuilder.append("<img src=\"");
          paramStringBuilder.append(((ImageSpan)arrayOfCharacterStyle[j]).getSource());
          paramStringBuilder.append("\">");
          paramInt1 = i;
        } 
        if (arrayOfCharacterStyle[j] instanceof AbsoluteSizeSpan) {
          AbsoluteSizeSpan absoluteSizeSpan = (AbsoluteSizeSpan)arrayOfCharacterStyle[j];
          float f1 = absoluteSizeSpan.getSize();
          float f2 = f1;
          if (!absoluteSizeSpan.getDip()) {
            Application application = ActivityThread.currentApplication();
            f2 = f1 / (application.getResources().getDisplayMetrics()).density;
          } 
          paramStringBuilder.append(String.format("<span style=\"font-size:%.0fpx\";>", new Object[] { Float.valueOf(f2) }));
        } 
        if (arrayOfCharacterStyle[j] instanceof RelativeSizeSpan) {
          float f = ((RelativeSizeSpan)arrayOfCharacterStyle[j]).getSizeChange();
          paramStringBuilder.append(String.format("<span style=\"font-size:%.2fem;\">", new Object[] { Float.valueOf(f) }));
        } 
        if (arrayOfCharacterStyle[j] instanceof ForegroundColorSpan) {
          int k = ((ForegroundColorSpan)arrayOfCharacterStyle[j]).getForegroundColor();
          paramStringBuilder.append(String.format("<span style=\"color:#%06X;\">", new Object[] { Integer.valueOf(k & 0xFFFFFF) }));
        } 
        if (arrayOfCharacterStyle[j] instanceof BackgroundColorSpan) {
          int k = ((BackgroundColorSpan)arrayOfCharacterStyle[j]).getBackgroundColor();
          paramStringBuilder.append(String.format("<span style=\"background-color:#%06X;\">", new Object[] { Integer.valueOf(0xFFFFFF & k) }));
        } 
      } 
      withinStyle(paramStringBuilder, paramSpanned, paramInt1, i);
      for (paramInt1 = arrayOfCharacterStyle.length - 1; paramInt1 >= 0; paramInt1--) {
        if (arrayOfCharacterStyle[paramInt1] instanceof BackgroundColorSpan)
          paramStringBuilder.append("</span>"); 
        if (arrayOfCharacterStyle[paramInt1] instanceof ForegroundColorSpan)
          paramStringBuilder.append("</span>"); 
        if (arrayOfCharacterStyle[paramInt1] instanceof RelativeSizeSpan)
          paramStringBuilder.append("</span>"); 
        if (arrayOfCharacterStyle[paramInt1] instanceof AbsoluteSizeSpan)
          paramStringBuilder.append("</span>"); 
        if (arrayOfCharacterStyle[paramInt1] instanceof URLSpan)
          paramStringBuilder.append("</a>"); 
        if (arrayOfCharacterStyle[paramInt1] instanceof android.text.style.StrikethroughSpan)
          paramStringBuilder.append("</span>"); 
        if (arrayOfCharacterStyle[paramInt1] instanceof android.text.style.UnderlineSpan)
          paramStringBuilder.append("</u>"); 
        if (arrayOfCharacterStyle[paramInt1] instanceof android.text.style.SubscriptSpan)
          paramStringBuilder.append("</sub>"); 
        if (arrayOfCharacterStyle[paramInt1] instanceof android.text.style.SuperscriptSpan)
          paramStringBuilder.append("</sup>"); 
        if (arrayOfCharacterStyle[paramInt1] instanceof TypefaceSpan) {
          String str = ((TypefaceSpan)arrayOfCharacterStyle[paramInt1]).getFamily();
          if (str.equals("monospace"))
            paramStringBuilder.append("</tt>"); 
        } 
        if (arrayOfCharacterStyle[paramInt1] instanceof StyleSpan) {
          j = ((StyleSpan)arrayOfCharacterStyle[paramInt1]).getStyle();
          if ((j & 0x1) != 0)
            paramStringBuilder.append("</b>"); 
          if ((j & 0x2) != 0)
            paramStringBuilder.append("</i>"); 
        } 
      } 
    } 
  }
  
  private static void withinStyle(StringBuilder paramStringBuilder, CharSequence paramCharSequence, int paramInt1, int paramInt2) {
    for (; paramInt1 < paramInt2; paramInt1 = i + 1) {
      int i;
      char c = paramCharSequence.charAt(paramInt1);
      if (c == '<') {
        paramStringBuilder.append("&lt;");
        i = paramInt1;
      } else if (c == '>') {
        paramStringBuilder.append("&gt;");
        i = paramInt1;
      } else if (c == '&') {
        paramStringBuilder.append("&amp;");
        i = paramInt1;
      } else if (c >= '?' && c <= '?') {
        i = paramInt1;
        if (c < '?') {
          i = paramInt1;
          if (paramInt1 + 1 < paramInt2) {
            char c1 = paramCharSequence.charAt(paramInt1 + 1);
            i = paramInt1;
            if (c1 >= '?') {
              i = paramInt1;
              if (c1 <= '?') {
                i = paramInt1 + 1;
                paramStringBuilder.append("&#");
                paramStringBuilder.append(c - 55296 << 10 | 0x10000 | c1 - 56320);
                paramStringBuilder.append(";");
              } 
            } 
          } 
        } 
      } else if (c > '~' || c < ' ') {
        paramStringBuilder.append("&#");
        paramStringBuilder.append(c);
        paramStringBuilder.append(";");
        i = paramInt1;
      } else if (c == ' ') {
        while (paramInt1 + 1 < paramInt2 && paramCharSequence.charAt(paramInt1 + 1) == ' ') {
          paramStringBuilder.append("&nbsp;");
          paramInt1++;
        } 
        paramStringBuilder.append(' ');
        i = paramInt1;
      } else {
        paramStringBuilder.append(c);
        i = paramInt1;
      } 
    } 
  }
  
  public static interface ImageGetter {
    Drawable getDrawable(String param1String);
  }
  
  public static interface TagHandler {
    void handleTag(boolean param1Boolean, String param1String, Editable param1Editable, XMLReader param1XMLReader);
  }
}
