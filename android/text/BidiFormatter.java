package android.text;

import java.util.Locale;

public final class BidiFormatter {
  private static final int DEFAULT_FLAGS = 2;
  
  private static final BidiFormatter DEFAULT_LTR_INSTANCE;
  
  private static final BidiFormatter DEFAULT_RTL_INSTANCE;
  
  private static TextDirectionHeuristic DEFAULT_TEXT_DIRECTION_HEURISTIC = TextDirectionHeuristics.FIRSTSTRONG_LTR;
  
  private static final int DIR_LTR = -1;
  
  private static final int DIR_RTL = 1;
  
  private static final int DIR_UNKNOWN = 0;
  
  private static final String EMPTY_STRING = "";
  
  private static final int FLAG_STEREO_RESET = 2;
  
  private static final char LRE = '‪';
  
  private static final char LRM = '‎';
  
  private static final String LRM_STRING = Character.toString('‎');
  
  private static final char PDF = '‬';
  
  private static final char RLE = '‫';
  
  private static final char RLM = '‏';
  
  private static final String RLM_STRING = Character.toString('‏');
  
  private final TextDirectionHeuristic mDefaultTextDirectionHeuristic;
  
  private final int mFlags;
  
  private final boolean mIsRtlContext;
  
  public static final class Builder {
    private int mFlags;
    
    private boolean mIsRtlContext;
    
    private TextDirectionHeuristic mTextDirectionHeuristic;
    
    public Builder() {
      initialize(BidiFormatter.isRtlLocale(Locale.getDefault()));
    }
    
    public Builder(boolean param1Boolean) {
      initialize(param1Boolean);
    }
    
    public Builder(Locale param1Locale) {
      initialize(BidiFormatter.isRtlLocale(param1Locale));
    }
    
    private void initialize(boolean param1Boolean) {
      this.mIsRtlContext = param1Boolean;
      this.mTextDirectionHeuristic = BidiFormatter.DEFAULT_TEXT_DIRECTION_HEURISTIC;
      this.mFlags = 2;
    }
    
    public Builder stereoReset(boolean param1Boolean) {
      if (param1Boolean) {
        this.mFlags |= 0x2;
      } else {
        this.mFlags &= 0xFFFFFFFD;
      } 
      return this;
    }
    
    public Builder setTextDirectionHeuristic(TextDirectionHeuristic param1TextDirectionHeuristic) {
      this.mTextDirectionHeuristic = param1TextDirectionHeuristic;
      return this;
    }
    
    public BidiFormatter build() {
      if (this.mFlags == 2) {
        TextDirectionHeuristic textDirectionHeuristic = this.mTextDirectionHeuristic;
        if (textDirectionHeuristic == BidiFormatter.DEFAULT_TEXT_DIRECTION_HEURISTIC)
          return BidiFormatter.getDefaultInstanceFromContext(this.mIsRtlContext); 
      } 
      return new BidiFormatter(this.mIsRtlContext, this.mFlags, this.mTextDirectionHeuristic);
    }
  }
  
  static {
    DEFAULT_LTR_INSTANCE = new BidiFormatter(false, 2, DEFAULT_TEXT_DIRECTION_HEURISTIC);
    DEFAULT_RTL_INSTANCE = new BidiFormatter(true, 2, DEFAULT_TEXT_DIRECTION_HEURISTIC);
  }
  
  public static BidiFormatter getInstance() {
    return getDefaultInstanceFromContext(isRtlLocale(Locale.getDefault()));
  }
  
  public static BidiFormatter getInstance(boolean paramBoolean) {
    return getDefaultInstanceFromContext(paramBoolean);
  }
  
  public static BidiFormatter getInstance(Locale paramLocale) {
    return getDefaultInstanceFromContext(isRtlLocale(paramLocale));
  }
  
  private BidiFormatter(boolean paramBoolean, int paramInt, TextDirectionHeuristic paramTextDirectionHeuristic) {
    this.mIsRtlContext = paramBoolean;
    this.mFlags = paramInt;
    this.mDefaultTextDirectionHeuristic = paramTextDirectionHeuristic;
  }
  
  public boolean isRtlContext() {
    return this.mIsRtlContext;
  }
  
  public boolean getStereoReset() {
    boolean bool;
    if ((this.mFlags & 0x2) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public String markAfter(CharSequence paramCharSequence, TextDirectionHeuristic paramTextDirectionHeuristic) {
    boolean bool = paramTextDirectionHeuristic.isRtl(paramCharSequence, 0, paramCharSequence.length());
    if (!this.mIsRtlContext && (bool || getExitDir(paramCharSequence) == 1))
      return LRM_STRING; 
    if (this.mIsRtlContext && (!bool || getExitDir(paramCharSequence) == -1))
      return RLM_STRING; 
    return "";
  }
  
  public String markBefore(CharSequence paramCharSequence, TextDirectionHeuristic paramTextDirectionHeuristic) {
    boolean bool = paramTextDirectionHeuristic.isRtl(paramCharSequence, 0, paramCharSequence.length());
    if (!this.mIsRtlContext && (bool || getEntryDir(paramCharSequence) == 1))
      return LRM_STRING; 
    if (this.mIsRtlContext && (!bool || getEntryDir(paramCharSequence) == -1))
      return RLM_STRING; 
    return "";
  }
  
  public boolean isRtl(String paramString) {
    return isRtl(paramString);
  }
  
  public boolean isRtl(CharSequence paramCharSequence) {
    return this.mDefaultTextDirectionHeuristic.isRtl(paramCharSequence, 0, paramCharSequence.length());
  }
  
  public String unicodeWrap(String paramString, TextDirectionHeuristic paramTextDirectionHeuristic, boolean paramBoolean) {
    if (paramString == null)
      return null; 
    return unicodeWrap(paramString, paramTextDirectionHeuristic, paramBoolean).toString();
  }
  
  public CharSequence unicodeWrap(CharSequence paramCharSequence, TextDirectionHeuristic paramTextDirectionHeuristic, boolean paramBoolean) {
    if (paramCharSequence == null)
      return null; 
    boolean bool = paramTextDirectionHeuristic.isRtl(paramCharSequence, 0, paramCharSequence.length());
    SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
    if (getStereoReset() && paramBoolean) {
      if (bool) {
        paramTextDirectionHeuristic = TextDirectionHeuristics.RTL;
      } else {
        paramTextDirectionHeuristic = TextDirectionHeuristics.LTR;
      } 
      spannableStringBuilder.append(markBefore(paramCharSequence, paramTextDirectionHeuristic));
    } 
    if (bool != this.mIsRtlContext) {
      char c;
      if (bool) {
        c = '‫';
      } else {
        c = '‪';
      } 
      spannableStringBuilder.append(c);
      spannableStringBuilder.append(paramCharSequence);
      spannableStringBuilder.append('‬');
    } else {
      spannableStringBuilder.append(paramCharSequence);
    } 
    if (paramBoolean) {
      if (bool) {
        paramTextDirectionHeuristic = TextDirectionHeuristics.RTL;
      } else {
        paramTextDirectionHeuristic = TextDirectionHeuristics.LTR;
      } 
      spannableStringBuilder.append(markAfter(paramCharSequence, paramTextDirectionHeuristic));
    } 
    return spannableStringBuilder;
  }
  
  public String unicodeWrap(String paramString, TextDirectionHeuristic paramTextDirectionHeuristic) {
    return unicodeWrap(paramString, paramTextDirectionHeuristic, true);
  }
  
  public CharSequence unicodeWrap(CharSequence paramCharSequence, TextDirectionHeuristic paramTextDirectionHeuristic) {
    return unicodeWrap(paramCharSequence, paramTextDirectionHeuristic, true);
  }
  
  public String unicodeWrap(String paramString, boolean paramBoolean) {
    return unicodeWrap(paramString, this.mDefaultTextDirectionHeuristic, paramBoolean);
  }
  
  public CharSequence unicodeWrap(CharSequence paramCharSequence, boolean paramBoolean) {
    return unicodeWrap(paramCharSequence, this.mDefaultTextDirectionHeuristic, paramBoolean);
  }
  
  public String unicodeWrap(String paramString) {
    return unicodeWrap(paramString, this.mDefaultTextDirectionHeuristic, true);
  }
  
  public CharSequence unicodeWrap(CharSequence paramCharSequence) {
    return unicodeWrap(paramCharSequence, this.mDefaultTextDirectionHeuristic, true);
  }
  
  private static BidiFormatter getDefaultInstanceFromContext(boolean paramBoolean) {
    BidiFormatter bidiFormatter;
    if (paramBoolean) {
      bidiFormatter = DEFAULT_RTL_INSTANCE;
    } else {
      bidiFormatter = DEFAULT_LTR_INSTANCE;
    } 
    return bidiFormatter;
  }
  
  private static boolean isRtlLocale(Locale paramLocale) {
    int i = TextUtils.getLayoutDirectionFromLocale(paramLocale);
    boolean bool = true;
    if (i != 1)
      bool = false; 
    return bool;
  }
  
  private static int getExitDir(CharSequence paramCharSequence) {
    return (new DirectionalityEstimator(paramCharSequence, false)).getExitDir();
  }
  
  private static int getEntryDir(CharSequence paramCharSequence) {
    return (new DirectionalityEstimator(paramCharSequence, false)).getEntryDir();
  }
  
  public static class DirectionalityEstimator {
    private static final byte[] DIR_TYPE_CACHE = new byte[1792];
    
    private static final int DIR_TYPE_CACHE_SIZE = 1792;
    
    private int charIndex;
    
    private final boolean isHtml;
    
    private char lastChar;
    
    private final int length;
    
    private final CharSequence text;
    
    static {
      for (byte b = 0; b < '܀'; b++)
        DIR_TYPE_CACHE[b] = Character.getDirectionality(b); 
    }
    
    public static byte getDirectionality(int param1Int) {
      return Character.getDirectionality(param1Int);
    }
    
    DirectionalityEstimator(CharSequence param1CharSequence, boolean param1Boolean) {
      this.text = param1CharSequence;
      this.isHtml = param1Boolean;
      this.length = param1CharSequence.length();
    }
    
    int getEntryDir() {
      this.charIndex = 0;
      byte b1 = 0;
      byte b = 0;
      byte b2 = 0;
      while (this.charIndex < this.length && !b2) {
        byte b3 = dirTypeForward();
        if (b3 != 0) {
          if (b3 != 1 && b3 != 2) {
            if (b3 != 9) {
              switch (b3) {
                default:
                  b2 = b1;
                  continue;
                case 18:
                  b1--;
                  b = 0;
                  continue;
                case 16:
                case 17:
                  b1++;
                  b = 1;
                  continue;
                case 14:
                case 15:
                  break;
              } 
              b1++;
              b = -1;
            } 
            continue;
          } 
          if (b1 == 0)
            return 1; 
          b2 = b1;
          continue;
        } 
        if (b1 == 0)
          return -1; 
        b2 = b1;
      } 
      if (b2 == 0)
        return 0; 
      if (b != 0)
        return b; 
      while (this.charIndex > 0) {
        switch (dirTypeBackward()) {
          default:
            continue;
          case 18:
            b1++;
            continue;
          case 16:
          case 17:
            if (b2 == b1)
              return 1; 
            b1--;
            continue;
          case 14:
          case 15:
            break;
        } 
        if (b2 == b1)
          return -1; 
        b1--;
      } 
      return 0;
    }
    
    int getExitDir() {
      this.charIndex = this.length;
      byte b1 = 0;
      byte b2 = 0;
      while (this.charIndex > 0) {
        byte b = dirTypeBackward();
        if (b != 0) {
          if (b != 1 && b != 2) {
            if (b != 9) {
              switch (b) {
                default:
                  if (!b2)
                    b2 = b1; 
                  continue;
                case 18:
                  b1++;
                  continue;
                case 16:
                case 17:
                  if (b2 == b1)
                    return 1; 
                  b1--;
                  continue;
                case 14:
                case 15:
                  break;
              } 
              if (b2 == b1)
                return -1; 
              b1--;
            } 
            continue;
          } 
          if (b1 == 0)
            return 1; 
          if (b2 == 0)
            b2 = b1; 
          continue;
        } 
        if (b1 == 0)
          return -1; 
        if (b2 == 0)
          b2 = b1; 
      } 
      return 0;
    }
    
    private static byte getCachedDirectionality(char param1Char) {
      byte b;
      if (param1Char < '܀') {
        b = DIR_TYPE_CACHE[param1Char];
      } else {
        b = getDirectionality(param1Char);
      } 
      return b;
    }
    
    byte dirTypeForward() {
      char c = this.text.charAt(this.charIndex);
      if (Character.isHighSurrogate(c)) {
        int i = Character.codePointAt(this.text, this.charIndex);
        this.charIndex += Character.charCount(i);
        return getDirectionality(i);
      } 
      this.charIndex++;
      byte b = getCachedDirectionality(this.lastChar);
      byte b1 = b;
      if (this.isHtml) {
        char c1 = this.lastChar;
        if (c1 == '<') {
          b1 = skipTagForward();
        } else {
          b1 = b;
          if (c1 == '&')
            b1 = skipEntityForward(); 
        } 
      } 
      return b1;
    }
    
    byte dirTypeBackward() {
      char c = this.text.charAt(this.charIndex - 1);
      if (Character.isLowSurrogate(c)) {
        int i = Character.codePointBefore(this.text, this.charIndex);
        this.charIndex -= Character.charCount(i);
        return getDirectionality(i);
      } 
      this.charIndex--;
      byte b = getCachedDirectionality(this.lastChar);
      byte b1 = b;
      if (this.isHtml) {
        char c1 = this.lastChar;
        if (c1 == '>') {
          b1 = skipTagBackward();
        } else {
          b1 = b;
          if (c1 == ';')
            b1 = skipEntityBackward(); 
        } 
      } 
      return b1;
    }
    
    private byte skipTagForward() {
      int i = this.charIndex;
      label20: while (true) {
        int j = this.charIndex;
        if (j < this.length) {
          CharSequence charSequence = this.text;
          this.charIndex = j + 1;
          char c = charSequence.charAt(j);
          if (c == '>')
            return 12; 
          if (c == '"' || c == '\'') {
            j = this.lastChar;
            while (true) {
              int k = this.charIndex;
              if (k < this.length) {
                charSequence = this.text;
                this.charIndex = k + 1;
                this.lastChar = c = charSequence.charAt(k);
                if (c != j)
                  continue; 
                continue label20;
              } 
              continue label20;
            } 
          } 
          continue;
        } 
        break;
      } 
      this.charIndex = i;
      this.lastChar = '<';
      return 13;
    }
    
    private byte skipTagBackward() {
      int i = this.charIndex;
      label22: while (true) {
        int j = this.charIndex;
        if (j > 0) {
          CharSequence charSequence = this.text;
          this.charIndex = --j;
          char c = charSequence.charAt(j);
          if (c == '<')
            return 12; 
          if (c == '>')
            break; 
          if (c == '"' || c == '\'') {
            j = this.lastChar;
            while (true) {
              int k = this.charIndex;
              if (k > 0) {
                charSequence = this.text;
                this.charIndex = --k;
                this.lastChar = c = charSequence.charAt(k);
                if (c != j)
                  continue; 
                continue label22;
              } 
              continue label22;
            } 
          } 
          continue;
        } 
        break;
      } 
      this.charIndex = i;
      this.lastChar = '>';
      return 13;
    }
    
    private byte skipEntityForward() {
      while (true) {
        int i = this.charIndex;
        if (i < this.length) {
          CharSequence charSequence = this.text;
          this.charIndex = i + 1;
          char c = charSequence.charAt(i);
          if (c != ';')
            continue; 
        } 
        break;
      } 
      return 12;
    }
    
    private byte skipEntityBackward() {
      int i = this.charIndex;
      while (true) {
        int j = this.charIndex;
        if (j > 0) {
          CharSequence charSequence = this.text;
          this.charIndex = --j;
          char c = charSequence.charAt(j);
          if (c == '&')
            return 12; 
          if (c == ';')
            break; 
          continue;
        } 
        break;
      } 
      this.charIndex = i;
      this.lastChar = ';';
      return 13;
    }
  }
}
