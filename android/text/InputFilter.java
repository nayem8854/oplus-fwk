package android.text;

import com.android.internal.util.Preconditions;
import java.util.Locale;

public interface InputFilter {
  CharSequence filter(CharSequence paramCharSequence, int paramInt1, int paramInt2, Spanned paramSpanned, int paramInt3, int paramInt4);
  
  class AllCaps implements InputFilter {
    private final Locale mLocale;
    
    public AllCaps() {
      this.mLocale = null;
    }
    
    public AllCaps(InputFilter this$0) {
      Preconditions.checkNotNull(this$0);
      this.mLocale = (Locale)this$0;
    }
    
    public CharSequence filter(CharSequence param1CharSequence, int param1Int1, int param1Int2, Spanned param1Spanned, int param1Int3, int param1Int4) {
      param1Spanned = new CharSequenceWrapper(param1CharSequence, param1Int1, param1Int2);
      boolean bool = false;
      param1Int3 = 0;
      while (true) {
        param1Int4 = bool;
        if (param1Int3 < param1Int2 - param1Int1) {
          param1Int4 = Character.codePointAt(param1Spanned, param1Int3);
          if (Character.isLowerCase(param1Int4) || Character.isTitleCase(param1Int4)) {
            param1Int4 = 1;
            break;
          } 
          param1Int3 += Character.charCount(param1Int4);
          continue;
        } 
        break;
      } 
      if (param1Int4 == 0)
        return null; 
      boolean bool1 = param1CharSequence instanceof Spanned;
      param1CharSequence = TextUtils.toUpperCase(this.mLocale, param1Spanned, bool1);
      if (param1CharSequence == param1Spanned)
        return null; 
      if (bool1) {
        param1CharSequence = new SpannableString(param1CharSequence);
      } else {
        param1CharSequence = param1CharSequence.toString();
      } 
      return param1CharSequence;
    }
    
    private static class CharSequenceWrapper implements CharSequence, Spanned {
      private final int mEnd;
      
      private final int mLength;
      
      private final CharSequence mSource;
      
      private final int mStart;
      
      CharSequenceWrapper(CharSequence param2CharSequence, int param2Int1, int param2Int2) {
        this.mSource = param2CharSequence;
        this.mStart = param2Int1;
        this.mEnd = param2Int2;
        this.mLength = param2Int2 - param2Int1;
      }
      
      public int length() {
        return this.mLength;
      }
      
      public char charAt(int param2Int) {
        if (param2Int >= 0 && param2Int < this.mLength)
          return this.mSource.charAt(this.mStart + param2Int); 
        throw new IndexOutOfBoundsException();
      }
      
      public CharSequence subSequence(int param2Int1, int param2Int2) {
        if (param2Int1 >= 0 && param2Int2 >= 0 && param2Int2 <= this.mLength && param2Int1 <= param2Int2) {
          CharSequence charSequence = this.mSource;
          int i = this.mStart;
          return new CharSequenceWrapper(charSequence, i + param2Int1, i + param2Int2);
        } 
        throw new IndexOutOfBoundsException();
      }
      
      public String toString() {
        return this.mSource.subSequence(this.mStart, this.mEnd).toString();
      }
      
      public <T> T[] getSpans(int param2Int1, int param2Int2, Class<T> param2Class) {
        Spanned spanned = (Spanned)this.mSource;
        int i = this.mStart;
        return spanned.getSpans(i + param2Int1, i + param2Int2, param2Class);
      }
      
      public int getSpanStart(Object param2Object) {
        return ((Spanned)this.mSource).getSpanStart(param2Object) - this.mStart;
      }
      
      public int getSpanEnd(Object param2Object) {
        return ((Spanned)this.mSource).getSpanEnd(param2Object) - this.mStart;
      }
      
      public int getSpanFlags(Object param2Object) {
        return ((Spanned)this.mSource).getSpanFlags(param2Object);
      }
      
      public int nextSpanTransition(int param2Int1, int param2Int2, Class param2Class) {
        Spanned spanned = (Spanned)this.mSource;
        int i = this.mStart;
        return spanned.nextSpanTransition(i + param2Int1, i + param2Int2, param2Class) - this.mStart;
      }
    }
  }
  
  class LengthFilter implements InputFilter {
    private final int mMax;
    
    public LengthFilter(InputFilter this$0) {
      this.mMax = this$0;
    }
    
    public CharSequence filter(CharSequence param1CharSequence, int param1Int1, int param1Int2, Spanned param1Spanned, int param1Int3, int param1Int4) {
      param1Int3 = this.mMax - param1Spanned.length() - param1Int4 - param1Int3;
      if (param1Int3 <= 0)
        return ""; 
      if (param1Int3 >= param1Int2 - param1Int1)
        return null; 
      param1Int3 += param1Int1;
      param1Int2 = param1Int3;
      if (Character.isHighSurrogate(param1CharSequence.charAt(param1Int3 - 1))) {
        param1Int2 = --param1Int3;
        if (param1Int3 == param1Int1)
          return ""; 
      } 
      return param1CharSequence.subSequence(param1Int1, param1Int2);
    }
    
    public int getMax() {
      return this.mMax;
    }
  }
}
