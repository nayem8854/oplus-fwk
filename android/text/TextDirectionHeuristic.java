package android.text;

public interface TextDirectionHeuristic {
  boolean isRtl(CharSequence paramCharSequence, int paramInt1, int paramInt2);
  
  boolean isRtl(char[] paramArrayOfchar, int paramInt1, int paramInt2);
}
