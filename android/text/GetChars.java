package android.text;

public interface GetChars extends CharSequence {
  void getChars(int paramInt1, int paramInt2, char[] paramArrayOfchar, int paramInt3);
}
