package android.text;

public interface SpanWatcher extends NoCopySpan {
  void onSpanAdded(Spannable paramSpannable, Object paramObject, int paramInt1, int paramInt2);
  
  void onSpanChanged(Spannable paramSpannable, Object paramObject, int paramInt1, int paramInt2, int paramInt3, int paramInt4);
  
  void onSpanRemoved(Spannable paramSpannable, Object paramObject, int paramInt1, int paramInt2);
}
