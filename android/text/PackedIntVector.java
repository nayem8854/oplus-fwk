package android.text;

import com.android.internal.util.ArrayUtils;
import com.android.internal.util.GrowingArrayUtils;

public class PackedIntVector {
  private final int mColumns;
  
  private int mRowGapLength;
  
  private int mRowGapStart;
  
  private int mRows;
  
  private int[] mValueGap;
  
  private int[] mValues;
  
  public PackedIntVector(int paramInt) {
    this.mColumns = paramInt;
    this.mRows = 0;
    this.mRowGapStart = 0;
    this.mRowGapLength = 0;
    this.mValues = null;
    this.mValueGap = new int[paramInt * 2];
  }
  
  public int getValue(int paramInt1, int paramInt2) {
    int i = this.mColumns;
    if ((paramInt1 | paramInt2) >= 0 && paramInt1 < size() && paramInt2 < i) {
      int j = paramInt1;
      if (paramInt1 >= this.mRowGapStart)
        j = paramInt1 + this.mRowGapLength; 
      int k = this.mValues[j * i + paramInt2];
      int[] arrayOfInt = this.mValueGap;
      paramInt1 = k;
      if (j >= arrayOfInt[paramInt2])
        paramInt1 = k + arrayOfInt[paramInt2 + i]; 
      return paramInt1;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramInt1);
    stringBuilder.append(", ");
    stringBuilder.append(paramInt2);
    throw new IndexOutOfBoundsException(stringBuilder.toString());
  }
  
  public void setValue(int paramInt1, int paramInt2, int paramInt3) {
    if ((paramInt1 | paramInt2) >= 0 && paramInt1 < size() && paramInt2 < this.mColumns) {
      int i = paramInt1;
      if (paramInt1 >= this.mRowGapStart)
        i = paramInt1 + this.mRowGapLength; 
      int[] arrayOfInt = this.mValueGap;
      paramInt1 = paramInt3;
      if (i >= arrayOfInt[paramInt2])
        paramInt1 = paramInt3 - arrayOfInt[this.mColumns + paramInt2]; 
      this.mValues[this.mColumns * i + paramInt2] = paramInt1;
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramInt1);
    stringBuilder.append(", ");
    stringBuilder.append(paramInt2);
    throw new IndexOutOfBoundsException(stringBuilder.toString());
  }
  
  private void setValueInternal(int paramInt1, int paramInt2, int paramInt3) {
    int i = paramInt1;
    if (paramInt1 >= this.mRowGapStart)
      i = paramInt1 + this.mRowGapLength; 
    int[] arrayOfInt = this.mValueGap;
    paramInt1 = paramInt3;
    if (i >= arrayOfInt[paramInt2])
      paramInt1 = paramInt3 - arrayOfInt[this.mColumns + paramInt2]; 
    this.mValues[this.mColumns * i + paramInt2] = paramInt1;
  }
  
  public void adjustValuesBelow(int paramInt1, int paramInt2, int paramInt3) {
    if ((paramInt1 | paramInt2) >= 0 && paramInt1 <= size() && 
      paramInt2 < width()) {
      int i = paramInt1;
      if (paramInt1 >= this.mRowGapStart)
        i = paramInt1 + this.mRowGapLength; 
      moveValueGapTo(paramInt2, i);
      int[] arrayOfInt = this.mValueGap;
      paramInt1 = this.mColumns + paramInt2;
      arrayOfInt[paramInt1] = arrayOfInt[paramInt1] + paramInt3;
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramInt1);
    stringBuilder.append(", ");
    stringBuilder.append(paramInt2);
    throw new IndexOutOfBoundsException(stringBuilder.toString());
  }
  
  public void insertAt(int paramInt, int[] paramArrayOfint) {
    if (paramInt >= 0 && paramInt <= size()) {
      if (paramArrayOfint == null || paramArrayOfint.length >= width()) {
        moveRowGapTo(paramInt);
        if (this.mRowGapLength == 0)
          growBuffer(); 
        this.mRowGapStart++;
        this.mRowGapLength--;
        if (paramArrayOfint == null) {
          for (int i = this.mColumns - 1; i >= 0; i--)
            setValueInternal(paramInt, i, 0); 
        } else {
          for (int i = this.mColumns - 1; i >= 0; i--)
            setValueInternal(paramInt, i, paramArrayOfint[i]); 
        } 
        return;
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("value count ");
      stringBuilder1.append(paramArrayOfint.length);
      throw new IndexOutOfBoundsException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("row ");
    stringBuilder.append(paramInt);
    throw new IndexOutOfBoundsException(stringBuilder.toString());
  }
  
  public void deleteAt(int paramInt1, int paramInt2) {
    if ((paramInt1 | paramInt2) >= 0 && paramInt1 + paramInt2 <= size()) {
      moveRowGapTo(paramInt1 + paramInt2);
      this.mRowGapStart -= paramInt2;
      this.mRowGapLength += paramInt2;
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramInt1);
    stringBuilder.append(", ");
    stringBuilder.append(paramInt2);
    throw new IndexOutOfBoundsException(stringBuilder.toString());
  }
  
  public int size() {
    return this.mRows - this.mRowGapLength;
  }
  
  public int width() {
    return this.mColumns;
  }
  
  private final void growBuffer() {
    int i = this.mColumns;
    int j = GrowingArrayUtils.growSize(size());
    int[] arrayOfInt1 = ArrayUtils.newUnpaddedIntArray(j * i);
    int k = arrayOfInt1.length / i;
    int[] arrayOfInt2 = this.mValueGap;
    int m = this.mRowGapStart;
    j = this.mRows - this.mRowGapLength + m;
    int[] arrayOfInt3 = this.mValues;
    if (arrayOfInt3 != null) {
      System.arraycopy(arrayOfInt3, 0, arrayOfInt1, 0, i * m);
      System.arraycopy(this.mValues, (this.mRows - j) * i, arrayOfInt1, (k - j) * i, j * i);
    } 
    for (j = 0; j < i; j++) {
      if (arrayOfInt2[j] >= m) {
        arrayOfInt2[j] = arrayOfInt2[j] + k - this.mRows;
        if (arrayOfInt2[j] < m)
          arrayOfInt2[j] = m; 
      } 
    } 
    this.mRowGapLength += k - this.mRows;
    this.mRows = k;
    this.mValues = arrayOfInt1;
  }
  
  private final void moveValueGapTo(int paramInt1, int paramInt2) {
    int[] arrayOfInt1 = this.mValueGap;
    int[] arrayOfInt2 = this.mValues;
    int i = this.mColumns;
    if (paramInt2 == arrayOfInt1[paramInt1])
      return; 
    if (paramInt2 > arrayOfInt1[paramInt1]) {
      for (int j = arrayOfInt1[paramInt1]; j < paramInt2; j++) {
        int k = j * i + paramInt1;
        arrayOfInt2[k] = arrayOfInt2[k] + arrayOfInt1[paramInt1 + i];
      } 
    } else {
      for (int j = paramInt2; j < arrayOfInt1[paramInt1]; j++) {
        int k = j * i + paramInt1;
        arrayOfInt2[k] = arrayOfInt2[k] - arrayOfInt1[paramInt1 + i];
      } 
    } 
    arrayOfInt1[paramInt1] = paramInt2;
  }
  
  private final void moveRowGapTo(int paramInt) {
    int i = this.mRowGapStart;
    if (paramInt == i)
      return; 
    if (paramInt > i) {
      int j = this.mRowGapLength;
      int k = this.mColumns;
      int[] arrayOfInt1 = this.mValueGap;
      int[] arrayOfInt2 = this.mValues;
      int m = i + j;
      for (int n = m; n < m + paramInt + j - i + j; n++) {
        int i1 = n - m + this.mRowGapStart;
        for (byte b = 0; b < k; b++) {
          int i2 = arrayOfInt2[n * k + b];
          int i3 = i2;
          if (n >= arrayOfInt1[b])
            i3 = i2 + arrayOfInt1[b + k]; 
          i2 = i3;
          if (i1 >= arrayOfInt1[b])
            i2 = i3 - arrayOfInt1[b + k]; 
          arrayOfInt2[i1 * k + b] = i2;
        } 
      } 
    } else {
      int j = i - paramInt;
      int k = this.mColumns;
      int[] arrayOfInt1 = this.mValueGap;
      int[] arrayOfInt2 = this.mValues;
      int m = this.mRowGapLength;
      for (int n = paramInt + j - 1; n >= paramInt; n--) {
        int i1 = n - paramInt + i + m - j;
        for (byte b = 0; b < k; b++) {
          int i2 = arrayOfInt2[n * k + b];
          int i3 = i2;
          if (n >= arrayOfInt1[b])
            i3 = i2 + arrayOfInt1[b + k]; 
          i2 = i3;
          if (i1 >= arrayOfInt1[b])
            i2 = i3 - arrayOfInt1[b + k]; 
          arrayOfInt2[i1 * k + b] = i2;
        } 
      } 
    } 
    this.mRowGapStart = paramInt;
  }
}
