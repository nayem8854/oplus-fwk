package android.text;

@Deprecated
public abstract class LoginFilter implements InputFilter {
  private boolean mAppendInvalid;
  
  LoginFilter(boolean paramBoolean) {
    this.mAppendInvalid = paramBoolean;
  }
  
  LoginFilter() {
    this.mAppendInvalid = false;
  }
  
  public CharSequence filter(CharSequence paramCharSequence, int paramInt1, int paramInt2, Spanned paramSpanned, int paramInt3, int paramInt4) {
    onStart();
    int i;
    for (i = 0; i < paramInt3; i++) {
      char c = paramSpanned.charAt(i);
      if (!isAllowed(c))
        onInvalidCharacter(c); 
    } 
    SpannableStringBuilder spannableStringBuilder = null;
    paramInt3 = 0;
    for (i = paramInt1; i < paramInt2; i++) {
      char c = paramCharSequence.charAt(i);
      if (isAllowed(c)) {
        paramInt3++;
      } else {
        if (this.mAppendInvalid) {
          paramInt3++;
        } else {
          SpannableStringBuilder spannableStringBuilder1 = spannableStringBuilder;
          if (spannableStringBuilder == null) {
            spannableStringBuilder1 = new SpannableStringBuilder(paramCharSequence, paramInt1, paramInt2);
            paramInt3 = i - paramInt1;
          } 
          spannableStringBuilder1.delete(paramInt3, paramInt3 + 1);
          spannableStringBuilder = spannableStringBuilder1;
        } 
        onInvalidCharacter(c);
      } 
    } 
    for (; paramInt4 < paramSpanned.length(); paramInt4++) {
      char c = paramSpanned.charAt(paramInt4);
      if (!isAllowed(c))
        onInvalidCharacter(c); 
    } 
    onStop();
    return spannableStringBuilder;
  }
  
  public void onStart() {}
  
  public void onInvalidCharacter(char paramChar) {}
  
  public void onStop() {}
  
  public abstract boolean isAllowed(char paramChar);
  
  @Deprecated
  class UsernameFilterGMail extends LoginFilter {
    public UsernameFilterGMail() {
      super(false);
    }
    
    public UsernameFilterGMail(LoginFilter this$0) {
      super(this$0);
    }
    
    public boolean isAllowed(char param1Char) {
      if ('0' <= param1Char && param1Char <= '9')
        return true; 
      if ('a' <= param1Char && param1Char <= 'z')
        return true; 
      if ('A' <= param1Char && param1Char <= 'Z')
        return true; 
      if ('.' == param1Char)
        return true; 
      return false;
    }
  }
  
  class UsernameFilterGeneric extends LoginFilter {
    private static final String mAllowed = "@_-+.";
    
    public UsernameFilterGeneric() {
      super(false);
    }
    
    public UsernameFilterGeneric(LoginFilter this$0) {
      super(this$0);
    }
    
    public boolean isAllowed(char param1Char) {
      if ('0' <= param1Char && param1Char <= '9')
        return true; 
      if ('a' <= param1Char && param1Char <= 'z')
        return true; 
      if ('A' <= param1Char && param1Char <= 'Z')
        return true; 
      if ("@_-+.".indexOf(param1Char) != -1)
        return true; 
      return false;
    }
  }
  
  @Deprecated
  class PasswordFilterGMail extends LoginFilter {
    public PasswordFilterGMail() {
      super(false);
    }
    
    public PasswordFilterGMail(LoginFilter this$0) {
      super(this$0);
    }
    
    public boolean isAllowed(char param1Char) {
      if (' ' <= param1Char && param1Char <= '')
        return true; 
      if (' ' <= param1Char && param1Char <= 'ÿ')
        return true; 
      return false;
    }
  }
}
