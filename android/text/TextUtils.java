package android.text;

import android.content.Context;
import android.content.res.Resources;
import android.icu.lang.UCharacter;
import android.icu.text.CaseMap;
import android.icu.text.Edits;
import android.icu.util.ULocale;
import android.os.Parcel;
import android.os.Parcelable;
import android.sysprop.DisplayProperties;
import android.text.style.CharacterStyle;
import android.text.style.ParagraphStyle;
import android.text.style.ReplacementSpan;
import android.text.style.UpdateAppearance;
import android.util.Log;
import android.util.Printer;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Array;
import java.util.BitSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

public class TextUtils {
  public static final int ABSOLUTE_SIZE_SPAN = 16;
  
  public static final int ACCESSIBILITY_CLICKABLE_SPAN = 25;
  
  public static final int ACCESSIBILITY_REPLACEMENT_SPAN = 29;
  
  public static final int ACCESSIBILITY_URL_SPAN = 26;
  
  public static final int ALIGNMENT_SPAN = 1;
  
  public static final int ANNOTATION = 18;
  
  public static final int BACKGROUND_COLOR_SPAN = 12;
  
  public static final int BULLET_SPAN = 8;
  
  public static final int CAP_MODE_CHARACTERS = 4096;
  
  public static final int CAP_MODE_SENTENCES = 16384;
  
  public static final int CAP_MODE_WORDS = 8192;
  
  public static String getEllipsisString(TruncateAt paramTruncateAt) {
    String str;
    if (paramTruncateAt == TruncateAt.END_SMALL) {
      str = "‥";
    } else {
      str = "…";
    } 
    return str;
  }
  
  public static void getChars(CharSequence paramCharSequence, int paramInt1, int paramInt2, char[] paramArrayOfchar, int paramInt3) {
    Class<?> clazz = paramCharSequence.getClass();
    if (clazz == String.class) {
      ((String)paramCharSequence).getChars(paramInt1, paramInt2, paramArrayOfchar, paramInt3);
    } else if (clazz == StringBuffer.class) {
      ((StringBuffer)paramCharSequence).getChars(paramInt1, paramInt2, paramArrayOfchar, paramInt3);
    } else if (clazz == StringBuilder.class) {
      ((StringBuilder)paramCharSequence).getChars(paramInt1, paramInt2, paramArrayOfchar, paramInt3);
    } else if (paramCharSequence instanceof GetChars) {
      ((GetChars)paramCharSequence).getChars(paramInt1, paramInt2, paramArrayOfchar, paramInt3);
    } else {
      for (; paramInt1 < paramInt2; paramInt1++, paramInt3++)
        paramArrayOfchar[paramInt3] = paramCharSequence.charAt(paramInt1); 
    } 
  }
  
  public static int indexOf(CharSequence paramCharSequence, char paramChar) {
    return indexOf(paramCharSequence, paramChar, 0);
  }
  
  public static int indexOf(CharSequence paramCharSequence, char paramChar, int paramInt) {
    Class<?> clazz = paramCharSequence.getClass();
    if (clazz == String.class)
      return ((String)paramCharSequence).indexOf(paramChar, paramInt); 
    return indexOf(paramCharSequence, paramChar, paramInt, paramCharSequence.length());
  }
  
  public static int indexOf(CharSequence paramCharSequence, char paramChar, int paramInt1, int paramInt2) {
    Class<?> clazz = paramCharSequence.getClass();
    if (paramCharSequence instanceof GetChars || clazz == StringBuffer.class || clazz == StringBuilder.class || clazz == String.class) {
      char[] arrayOfChar = obtain(500);
      int i = paramInt1;
      while (i < paramInt2) {
        int j = i + 500;
        paramInt1 = j;
        if (j > paramInt2)
          paramInt1 = paramInt2; 
        getChars(paramCharSequence, i, paramInt1, arrayOfChar, 0);
        for (j = 0; j < paramInt1 - i; j++) {
          if (arrayOfChar[j] == paramChar) {
            recycle(arrayOfChar);
            return j + i;
          } 
        } 
        i = paramInt1;
      } 
      recycle(arrayOfChar);
      return -1;
    } 
    for (; paramInt1 < paramInt2; paramInt1++) {
      if (paramCharSequence.charAt(paramInt1) == paramChar)
        return paramInt1; 
    } 
    return -1;
  }
  
  public static int lastIndexOf(CharSequence paramCharSequence, char paramChar) {
    return lastIndexOf(paramCharSequence, paramChar, paramCharSequence.length() - 1);
  }
  
  public static int lastIndexOf(CharSequence paramCharSequence, char paramChar, int paramInt) {
    Class<?> clazz = paramCharSequence.getClass();
    if (clazz == String.class)
      return ((String)paramCharSequence).lastIndexOf(paramChar, paramInt); 
    return lastIndexOf(paramCharSequence, paramChar, 0, paramInt);
  }
  
  public static int lastIndexOf(CharSequence paramCharSequence, char paramChar, int paramInt1, int paramInt2) {
    if (paramInt2 < 0)
      return -1; 
    int i = paramInt2;
    if (paramInt2 >= paramCharSequence.length())
      i = paramCharSequence.length() - 1; 
    i++;
    Class<?> clazz = paramCharSequence.getClass();
    if (paramCharSequence instanceof GetChars || clazz == StringBuffer.class || clazz == StringBuilder.class || clazz == String.class) {
      char[] arrayOfChar = obtain(500);
      while (paramInt1 < i) {
        int j = i - 500;
        paramInt2 = j;
        if (j < paramInt1)
          paramInt2 = paramInt1; 
        getChars(paramCharSequence, paramInt2, i, arrayOfChar, 0);
        for (i = i - paramInt2 - 1; i >= 0; i--) {
          if (arrayOfChar[i] == paramChar) {
            recycle(arrayOfChar);
            return i + paramInt2;
          } 
        } 
        i = paramInt2;
      } 
      recycle(arrayOfChar);
      return -1;
    } 
    for (paramInt2 = i - 1; paramInt2 >= paramInt1; paramInt2--) {
      if (paramCharSequence.charAt(paramInt2) == paramChar)
        return paramInt2; 
    } 
    return -1;
  }
  
  public static int indexOf(CharSequence paramCharSequence1, CharSequence paramCharSequence2) {
    return indexOf(paramCharSequence1, paramCharSequence2, 0, paramCharSequence1.length());
  }
  
  public static int indexOf(CharSequence paramCharSequence1, CharSequence paramCharSequence2, int paramInt) {
    return indexOf(paramCharSequence1, paramCharSequence2, paramInt, paramCharSequence1.length());
  }
  
  public static int indexOf(CharSequence paramCharSequence1, CharSequence paramCharSequence2, int paramInt1, int paramInt2) {
    int i = paramCharSequence2.length();
    if (i == 0)
      return paramInt1; 
    char c = paramCharSequence2.charAt(0);
    while (true) {
      paramInt1 = indexOf(paramCharSequence1, c, paramInt1);
      if (paramInt1 > paramInt2 - i)
        return -1; 
      if (paramInt1 < 0)
        return -1; 
      if (regionMatches(paramCharSequence1, paramInt1, paramCharSequence2, 0, i))
        return paramInt1; 
      paramInt1++;
    } 
  }
  
  public static boolean regionMatches(CharSequence paramCharSequence1, int paramInt1, CharSequence paramCharSequence2, int paramInt2, int paramInt3) {
    int i = paramInt3 * 2;
    if (i >= paramInt3) {
      boolean bool2;
      char[] arrayOfChar = obtain(i);
      getChars(paramCharSequence1, paramInt1, paramInt1 + paramInt3, arrayOfChar, 0);
      getChars(paramCharSequence2, paramInt2, paramInt2 + paramInt3, arrayOfChar, paramInt3);
      boolean bool1 = true;
      paramInt1 = 0;
      while (true) {
        bool2 = bool1;
        if (paramInt1 < paramInt3) {
          if (arrayOfChar[paramInt1] != arrayOfChar[paramInt1 + paramInt3]) {
            bool2 = false;
            break;
          } 
          paramInt1++;
          continue;
        } 
        break;
      } 
      recycle(arrayOfChar);
      return bool2;
    } 
    throw new IndexOutOfBoundsException();
  }
  
  public static String substring(CharSequence paramCharSequence, int paramInt1, int paramInt2) {
    if (paramCharSequence instanceof String)
      return ((String)paramCharSequence).substring(paramInt1, paramInt2); 
    if (paramCharSequence instanceof StringBuilder)
      return ((StringBuilder)paramCharSequence).substring(paramInt1, paramInt2); 
    if (paramCharSequence instanceof StringBuffer)
      return ((StringBuffer)paramCharSequence).substring(paramInt1, paramInt2); 
    char[] arrayOfChar = obtain(paramInt2 - paramInt1);
    getChars(paramCharSequence, paramInt1, paramInt2, arrayOfChar, 0);
    paramCharSequence = new String(arrayOfChar, 0, paramInt2 - paramInt1);
    recycle(arrayOfChar);
    return (String)paramCharSequence;
  }
  
  public static String join(CharSequence paramCharSequence, Object[] paramArrayOfObject) {
    int i = paramArrayOfObject.length;
    if (i == 0)
      return ""; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramArrayOfObject[0]);
    for (byte b = 1; b < i; b++) {
      stringBuilder.append(paramCharSequence);
      stringBuilder.append(paramArrayOfObject[b]);
    } 
    return stringBuilder.toString();
  }
  
  public static String join(CharSequence paramCharSequence, Iterable paramIterable) {
    Iterator iterator = paramIterable.iterator();
    if (!iterator.hasNext())
      return ""; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(iterator.next());
    while (iterator.hasNext()) {
      stringBuilder.append(paramCharSequence);
      stringBuilder.append(iterator.next());
    } 
    return stringBuilder.toString();
  }
  
  public static String[] split(String paramString1, String paramString2) {
    if (paramString1.length() == 0)
      return EMPTY_STRING_ARRAY; 
    return paramString1.split(paramString2, -1);
  }
  
  public static String[] split(String paramString, Pattern paramPattern) {
    if (paramString.length() == 0)
      return EMPTY_STRING_ARRAY; 
    return paramPattern.split(paramString, -1);
  }
  
  class SimpleStringSplitter implements StringSplitter, Iterator<String> {
    private char mDelimiter;
    
    private int mLength;
    
    private int mPosition;
    
    private String mString;
    
    public SimpleStringSplitter(TextUtils this$0) {
      this.mDelimiter = this$0;
    }
    
    public void setString(String param1String) {
      this.mString = param1String;
      this.mPosition = 0;
      this.mLength = param1String.length();
    }
    
    public Iterator<String> iterator() {
      return this;
    }
    
    public boolean hasNext() {
      boolean bool;
      if (this.mPosition < this.mLength) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public String next() {
      int i = this.mString.indexOf(this.mDelimiter, this.mPosition);
      int j = i;
      if (i == -1)
        j = this.mLength; 
      String str = this.mString.substring(this.mPosition, j);
      this.mPosition = j + 1;
      return str;
    }
    
    public void remove() {
      throw new UnsupportedOperationException();
    }
  }
  
  public static CharSequence stringOrSpannedString(CharSequence paramCharSequence) {
    if (paramCharSequence == null)
      return null; 
    if (paramCharSequence instanceof SpannedString)
      return paramCharSequence; 
    if (paramCharSequence instanceof Spanned)
      return new SpannedString(paramCharSequence); 
    return paramCharSequence.toString();
  }
  
  public static boolean isEmpty(CharSequence paramCharSequence) {
    return (paramCharSequence == null || paramCharSequence.length() == 0);
  }
  
  public static String nullIfEmpty(String paramString) {
    if (isEmpty(paramString))
      paramString = null; 
    return paramString;
  }
  
  public static String emptyIfNull(String paramString) {
    if (paramString == null)
      paramString = ""; 
    return paramString;
  }
  
  public static String firstNotEmpty(String paramString1, String paramString2) {
    if (isEmpty(paramString1))
      paramString1 = (String)Preconditions.checkStringNotEmpty(paramString2); 
    return paramString1;
  }
  
  public static int length(String paramString) {
    boolean bool;
    if (paramString != null) {
      bool = paramString.length();
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static String safeIntern(String paramString) {
    if (paramString != null) {
      paramString = paramString.intern();
    } else {
      paramString = null;
    } 
    return paramString;
  }
  
  public static int getTrimmedLength(CharSequence paramCharSequence) {
    int i = paramCharSequence.length();
    byte b = 0;
    while (b < i && paramCharSequence.charAt(b) <= ' ')
      b++; 
    while (i > b && paramCharSequence.charAt(i - 1) <= ' ')
      i--; 
    return i - b;
  }
  
  public static boolean equals(CharSequence paramCharSequence1, CharSequence paramCharSequence2) {
    if (paramCharSequence1 == paramCharSequence2)
      return true; 
    if (paramCharSequence1 != null && paramCharSequence2 != null) {
      int i = paramCharSequence1.length();
      if (i == paramCharSequence2.length()) {
        if (paramCharSequence1 instanceof String && paramCharSequence2 instanceof String)
          return paramCharSequence1.equals(paramCharSequence2); 
        for (byte b = 0; b < i; b++) {
          if (paramCharSequence1.charAt(b) != paramCharSequence2.charAt(b))
            return false; 
        } 
        return true;
      } 
    } 
    return false;
  }
  
  @Deprecated
  public static CharSequence getReverse(CharSequence paramCharSequence, int paramInt1, int paramInt2) {
    return new Reverser(paramCharSequence, paramInt1, paramInt2);
  }
  
  class Reverser implements CharSequence, GetChars {
    private int mEnd;
    
    private CharSequence mSource;
    
    private int mStart;
    
    public Reverser(TextUtils this$0, int param1Int1, int param1Int2) {
      this.mSource = (CharSequence)this$0;
      this.mStart = param1Int1;
      this.mEnd = param1Int2;
    }
    
    public int length() {
      return this.mEnd - this.mStart;
    }
    
    public CharSequence subSequence(int param1Int1, int param1Int2) {
      char[] arrayOfChar = new char[param1Int2 - param1Int1];
      getChars(param1Int1, param1Int2, arrayOfChar, 0);
      return new String(arrayOfChar);
    }
    
    public String toString() {
      return subSequence(0, length()).toString();
    }
    
    public char charAt(int param1Int) {
      return (char)UCharacter.getMirror(this.mSource.charAt(this.mEnd - 1 - param1Int));
    }
    
    public void getChars(int param1Int1, int param1Int2, char[] param1ArrayOfchar, int param1Int3) {
      CharSequence charSequence = this.mSource;
      int i = this.mStart;
      TextUtils.getChars(charSequence, param1Int1 + i, i + param1Int2, param1ArrayOfchar, param1Int3);
      AndroidCharacter.mirror(param1ArrayOfchar, 0, param1Int2 - param1Int1);
      i = param1Int2 - param1Int1;
      param1Int2 = (param1Int2 - param1Int1) / 2;
      for (param1Int1 = 0; param1Int1 < param1Int2; param1Int1++) {
        char c = param1ArrayOfchar[param1Int3 + param1Int1];
        param1ArrayOfchar[param1Int3 + param1Int1] = param1ArrayOfchar[param1Int3 + i - param1Int1 - 1];
        param1ArrayOfchar[param1Int3 + i - param1Int1 - 1] = c;
      } 
    }
  }
  
  public static void writeToParcel(CharSequence paramCharSequence, Parcel paramParcel, int paramInt) {
    Object object;
    if (paramCharSequence instanceof Spanned) {
      paramParcel.writeInt(0);
      paramParcel.writeString8(paramCharSequence.toString());
      Spanned spanned = (Spanned)paramCharSequence;
      Object[] arrayOfObject = spanned.getSpans(0, paramCharSequence.length(), Object.class);
      for (byte b = 0; b < arrayOfObject.length; b++) {
        Object object1 = arrayOfObject[b];
        Object object2 = arrayOfObject[b];
        object = object2;
        if (object2 instanceof CharacterStyle)
          object = ((CharacterStyle)object2).getUnderlying(); 
        if (object instanceof ParcelableSpan) {
          object = object;
          int i = object.getSpanTypeIdInternal();
          if (i < 1 || i > 29) {
            object2 = new StringBuilder();
            object2.append("External class \"");
            object2.append(object.getClass().getSimpleName());
            object2.append("\" is attempting to use the frameworks-only ParcelableSpan interface");
            Log.e("TextUtils", object2.toString());
          } else {
            paramParcel.writeInt(i);
            object.writeToParcelInternal(paramParcel, paramInt);
            writeWhere(paramParcel, spanned, object1);
          } 
        } 
      } 
      paramParcel.writeInt(0);
    } else {
      paramParcel.writeInt(1);
      if (object != null) {
        paramParcel.writeString8(object.toString());
      } else {
        paramParcel.writeString8(null);
      } 
    } 
  }
  
  private static void writeWhere(Parcel paramParcel, Spanned paramSpanned, Object paramObject) {
    paramParcel.writeInt(paramSpanned.getSpanStart(paramObject));
    paramParcel.writeInt(paramSpanned.getSpanEnd(paramObject));
    paramParcel.writeInt(paramSpanned.getSpanFlags(paramObject));
  }
  
  public static final Parcelable.Creator<CharSequence> CHAR_SEQUENCE_CREATOR = (Parcelable.Creator<CharSequence>)new Object();
  
  public static final int EASY_EDIT_SPAN = 22;
  
  static final char ELLIPSIS_FILLER = '﻿';
  
  private static final String ELLIPSIS_NORMAL = "…";
  
  private static final String ELLIPSIS_TWO_DOTS = "‥";
  
  private static String[] EMPTY_STRING_ARRAY;
  
  public static final int FIRST_SPAN = 1;
  
  public static final int FOREGROUND_COLOR_SPAN = 2;
  
  public static final int LAST_SPAN = 29;
  
  public static final int LEADING_MARGIN_SPAN = 10;
  
  public static final int LINE_BACKGROUND_SPAN = 27;
  
  private static final int LINE_FEED_CODE_POINT = 10;
  
  public static final int LINE_HEIGHT_SPAN = 28;
  
  public static final int LOCALE_SPAN = 23;
  
  private static final int NBSP_CODE_POINT = 160;
  
  private static final int PARCEL_SAFE_TEXT_LENGTH = 100000;
  
  public static final int QUOTE_SPAN = 9;
  
  public static final int RELATIVE_SIZE_SPAN = 3;
  
  public static final int SAFE_STRING_FLAG_FIRST_LINE = 4;
  
  public static final int SAFE_STRING_FLAG_SINGLE_LINE = 2;
  
  public static final int SAFE_STRING_FLAG_TRIM = 1;
  
  public static final int SCALE_X_SPAN = 4;
  
  public static final int SPELL_CHECK_SPAN = 20;
  
  public static final int STRIKETHROUGH_SPAN = 5;
  
  public static final int STYLE_SPAN = 7;
  
  public static final int SUBSCRIPT_SPAN = 15;
  
  public static final int SUGGESTION_RANGE_SPAN = 21;
  
  public static final int SUGGESTION_SPAN = 19;
  
  public static final int SUPERSCRIPT_SPAN = 14;
  
  private static final String TAG = "TextUtils";
  
  public static final int TEXT_APPEARANCE_SPAN = 17;
  
  public static final int TTS_SPAN = 24;
  
  public static final int TYPEFACE_SPAN = 13;
  
  public static final int UNDERLINE_SPAN = 6;
  
  public static final int URL_SPAN = 11;
  
  public static void dumpSpans(CharSequence paramCharSequence, Printer paramPrinter, String paramString) {
    if (paramCharSequence instanceof Spanned) {
      Spanned spanned = (Spanned)paramCharSequence;
      Object[] arrayOfObject = spanned.getSpans(0, paramCharSequence.length(), Object.class);
      for (byte b = 0; b < arrayOfObject.length; b++) {
        Object object = arrayOfObject[b];
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(paramString);
        int i = spanned.getSpanStart(object);
        int j = spanned.getSpanEnd(object);
        stringBuilder.append(paramCharSequence.subSequence(i, j));
        stringBuilder.append(": ");
        stringBuilder.append(Integer.toHexString(System.identityHashCode(object)));
        stringBuilder.append(" ");
        stringBuilder.append(object.getClass().getCanonicalName());
        stringBuilder.append(" (");
        stringBuilder.append(spanned.getSpanStart(object));
        stringBuilder.append("-");
        stringBuilder.append(spanned.getSpanEnd(object));
        stringBuilder.append(") fl=#");
        stringBuilder.append(spanned.getSpanFlags(object));
        object = stringBuilder.toString();
        paramPrinter.println((String)object);
      } 
    } else {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString);
      stringBuilder.append(paramCharSequence);
      stringBuilder.append(": (no spans)");
      paramPrinter.println(stringBuilder.toString());
    } 
  }
  
  public static CharSequence replace(CharSequence paramCharSequence, String[] paramArrayOfString, CharSequence[] paramArrayOfCharSequence) {
    paramCharSequence = new SpannableStringBuilder(paramCharSequence);
    byte b;
    for (b = 0; b < paramArrayOfString.length; b++) {
      int i = indexOf(paramCharSequence, paramArrayOfString[b]);
      if (i >= 0)
        paramCharSequence.setSpan(paramArrayOfString[b], i, paramArrayOfString[b].length() + i, 33); 
    } 
    for (b = 0; b < paramArrayOfString.length; b++) {
      int i = paramCharSequence.getSpanStart(paramArrayOfString[b]);
      int j = paramCharSequence.getSpanEnd(paramArrayOfString[b]);
      if (i >= 0)
        paramCharSequence.replace(i, j, paramArrayOfCharSequence[b]); 
    } 
    return paramCharSequence;
  }
  
  public static CharSequence expandTemplate(CharSequence paramCharSequence, CharSequence... paramVarArgs) {
    if (paramVarArgs.length <= 9) {
      paramCharSequence = new SpannableStringBuilder(paramCharSequence);
      int i = 0;
      try {
        while (i < paramCharSequence.length()) {
          if (paramCharSequence.charAt(i) == '^') {
            char c = paramCharSequence.charAt(i + 1);
            if (c == '^') {
              paramCharSequence.delete(i + 1, i + 2);
              i++;
              continue;
            } 
            if (Character.isDigit(c)) {
              int j = Character.getNumericValue(c);
              j--;
              if (j >= 0) {
                if (j < paramVarArgs.length) {
                  paramCharSequence.replace(i, i + 2, paramVarArgs[j]);
                  i += paramVarArgs[j].length();
                  continue;
                } 
                IllegalArgumentException illegalArgumentException1 = new IllegalArgumentException();
                StringBuilder stringBuilder1 = new StringBuilder();
                this();
                stringBuilder1.append("template requests value ^");
                stringBuilder1.append(j + 1);
                stringBuilder1.append("; only ");
                stringBuilder1.append(paramVarArgs.length);
                stringBuilder1.append(" provided");
                this(stringBuilder1.toString());
                throw illegalArgumentException1;
              } 
              IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("template requests value ^");
              stringBuilder.append(j + 1);
              this(stringBuilder.toString());
              throw illegalArgumentException;
            } 
          } 
          i++;
        } 
      } catch (IndexOutOfBoundsException indexOutOfBoundsException) {}
      return paramCharSequence;
    } 
    throw new IllegalArgumentException("max of 9 values are supported");
  }
  
  public static int getOffsetBefore(CharSequence paramCharSequence, int paramInt) {
    if (paramInt == 0)
      return 0; 
    if (paramInt == 1)
      return 0; 
    char c = paramCharSequence.charAt(paramInt - 1);
    if (c >= '?' && c <= '?') {
      c = paramCharSequence.charAt(paramInt - 2);
      if (c >= '?' && c <= '?') {
        paramInt -= 2;
      } else {
        paramInt--;
      } 
    } else {
      paramInt--;
    } 
    int i = paramInt;
    if (paramCharSequence instanceof Spanned) {
      ReplacementSpan[] arrayOfReplacementSpan = ((Spanned)paramCharSequence).<ReplacementSpan>getSpans(paramInt, paramInt, ReplacementSpan.class);
      c = Character.MIN_VALUE;
      while (true) {
        i = paramInt;
        if (c < arrayOfReplacementSpan.length) {
          int j = ((Spanned)paramCharSequence).getSpanStart(arrayOfReplacementSpan[c]);
          int k = ((Spanned)paramCharSequence).getSpanEnd(arrayOfReplacementSpan[c]);
          i = paramInt;
          if (j < paramInt) {
            i = paramInt;
            if (k > paramInt)
              i = j; 
          } 
          c++;
          paramInt = i;
          continue;
        } 
        break;
      } 
    } 
    return i;
  }
  
  public static int getOffsetAfter(CharSequence paramCharSequence, int paramInt) {
    int i = paramCharSequence.length();
    if (paramInt == i)
      return i; 
    if (paramInt == i - 1)
      return i; 
    i = paramCharSequence.charAt(paramInt);
    if (i >= 55296 && i <= 56319) {
      i = paramCharSequence.charAt(paramInt + 1);
      if (i >= 56320 && i <= 57343) {
        paramInt += 2;
      } else {
        paramInt++;
      } 
    } else {
      paramInt++;
    } 
    int j = paramInt;
    if (paramCharSequence instanceof Spanned) {
      ReplacementSpan[] arrayOfReplacementSpan = ((Spanned)paramCharSequence).<ReplacementSpan>getSpans(paramInt, paramInt, ReplacementSpan.class);
      i = 0;
      while (true) {
        j = paramInt;
        if (i < arrayOfReplacementSpan.length) {
          int k = ((Spanned)paramCharSequence).getSpanStart(arrayOfReplacementSpan[i]);
          int m = ((Spanned)paramCharSequence).getSpanEnd(arrayOfReplacementSpan[i]);
          j = paramInt;
          if (k < paramInt) {
            j = paramInt;
            if (m > paramInt)
              j = m; 
          } 
          i++;
          paramInt = j;
          continue;
        } 
        break;
      } 
    } 
    return j;
  }
  
  private static void readSpan(Parcel paramParcel, Spannable paramSpannable, Object paramObject) {
    paramSpannable.setSpan(paramObject, paramParcel.readInt(), paramParcel.readInt(), paramParcel.readInt());
  }
  
  public static void copySpansFrom(Spanned paramSpanned, int paramInt1, int paramInt2, Class<Object> paramClass, Spannable paramSpannable, int paramInt3) {
    // Byte code:
    //   0: aload_3
    //   1: astore #6
    //   3: aload_3
    //   4: ifnonnull -> 11
    //   7: ldc java/lang/Object
    //   9: astore #6
    //   11: aload_0
    //   12: iload_1
    //   13: iload_2
    //   14: aload #6
    //   16: invokeinterface getSpans : (IILjava/lang/Class;)[Ljava/lang/Object;
    //   21: astore_3
    //   22: iconst_0
    //   23: istore #7
    //   25: iload #7
    //   27: aload_3
    //   28: arraylength
    //   29: if_icmpge -> 127
    //   32: aload_0
    //   33: aload_3
    //   34: iload #7
    //   36: aaload
    //   37: invokeinterface getSpanStart : (Ljava/lang/Object;)I
    //   42: istore #8
    //   44: aload_0
    //   45: aload_3
    //   46: iload #7
    //   48: aaload
    //   49: invokeinterface getSpanEnd : (Ljava/lang/Object;)I
    //   54: istore #9
    //   56: aload_0
    //   57: aload_3
    //   58: iload #7
    //   60: aaload
    //   61: invokeinterface getSpanFlags : (Ljava/lang/Object;)I
    //   66: istore #10
    //   68: iload #8
    //   70: istore #11
    //   72: iload #8
    //   74: iload_1
    //   75: if_icmpge -> 81
    //   78: iload_1
    //   79: istore #11
    //   81: iload #9
    //   83: istore #8
    //   85: iload #9
    //   87: iload_2
    //   88: if_icmple -> 94
    //   91: iload_2
    //   92: istore #8
    //   94: aload #4
    //   96: aload_3
    //   97: iload #7
    //   99: aaload
    //   100: iload #11
    //   102: iload_1
    //   103: isub
    //   104: iload #5
    //   106: iadd
    //   107: iload #8
    //   109: iload_1
    //   110: isub
    //   111: iload #5
    //   113: iadd
    //   114: iload #10
    //   116: invokeinterface setSpan : (Ljava/lang/Object;III)V
    //   121: iinc #7, 1
    //   124: goto -> 25
    //   127: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1163	-> 0
    //   #1164	-> 7
    //   #1167	-> 11
    //   #1169	-> 22
    //   #1170	-> 32
    //   #1171	-> 44
    //   #1172	-> 56
    //   #1174	-> 68
    //   #1175	-> 78
    //   #1176	-> 81
    //   #1177	-> 91
    //   #1179	-> 94
    //   #1169	-> 121
    //   #1182	-> 127
  }
  
  public static CharSequence toUpperCase(Locale paramLocale, CharSequence paramCharSequence, boolean paramBoolean) {
    Edits edits = new Edits();
    if (!paramBoolean) {
      charSequence = (StringBuilder)CaseMap.toUpper().apply(paramLocale, paramCharSequence, new StringBuilder(), edits);
      if (!edits.hasChanges())
        charSequence = paramCharSequence; 
      return charSequence;
    } 
    CharSequence charSequence = (SpannableStringBuilder)CaseMap.toUpper().apply((Locale)charSequence, paramCharSequence, new SpannableStringBuilder(), edits);
    if (!edits.hasChanges())
      return paramCharSequence; 
    Edits.Iterator iterator = edits.getFineIterator();
    int i = paramCharSequence.length();
    Spanned spanned = (Spanned)paramCharSequence;
    byte b = 0;
    Object[] arrayOfObject = spanned.getSpans(0, i, Object.class);
    for (int j = arrayOfObject.length; b < j; ) {
      Object object = arrayOfObject[b];
      int k = spanned.getSpanStart(object);
      int m = spanned.getSpanEnd(object);
      int n = spanned.getSpanFlags(object);
      if (k == i) {
        k = charSequence.length();
      } else {
        k = toUpperMapToDest(iterator, k);
      } 
      if (m == i) {
        m = charSequence.length();
      } else {
        m = toUpperMapToDest(iterator, m);
      } 
      charSequence.setSpan(object, k, m, n);
      b++;
    } 
    return charSequence;
  }
  
  private static int toUpperMapToDest(Edits.Iterator paramIterator, int paramInt) {
    paramIterator.findSourceIndex(paramInt);
    if (paramInt == paramIterator.sourceIndex())
      return paramIterator.destinationIndex(); 
    if (paramIterator.hasChange())
      return paramIterator.destinationIndex() + paramIterator.newLength(); 
    return paramIterator.destinationIndex() + paramInt - paramIterator.sourceIndex();
  }
  
  public enum TruncateAt {
    END, END_SMALL, MARQUEE, MIDDLE, START;
    
    private static final TruncateAt[] $VALUES;
    
    static {
      END = new TruncateAt("END", 2);
      MARQUEE = new TruncateAt("MARQUEE", 3);
      TruncateAt truncateAt = new TruncateAt("END_SMALL", 4);
      $VALUES = new TruncateAt[] { START, MIDDLE, END, MARQUEE, truncateAt };
    }
  }
  
  public static CharSequence ellipsize(CharSequence paramCharSequence, TextPaint paramTextPaint, float paramFloat, TruncateAt paramTruncateAt) {
    return ellipsize(paramCharSequence, paramTextPaint, paramFloat, paramTruncateAt, false, null);
  }
  
  public static CharSequence ellipsize(CharSequence paramCharSequence, TextPaint paramTextPaint, float paramFloat, TruncateAt paramTruncateAt, boolean paramBoolean, EllipsizeCallback paramEllipsizeCallback) {
    TextDirectionHeuristic textDirectionHeuristic = TextDirectionHeuristics.FIRSTSTRONG_LTR;
    String str = getEllipsisString(paramTruncateAt);
    return ellipsize(paramCharSequence, paramTextPaint, paramFloat, paramTruncateAt, paramBoolean, paramEllipsizeCallback, textDirectionHeuristic, str);
  }
  
  public static CharSequence ellipsize(CharSequence paramCharSequence, TextPaint paramTextPaint, float paramFloat, TruncateAt paramTruncateAt, boolean paramBoolean, EllipsizeCallback paramEllipsizeCallback, TextDirectionHeuristic paramTextDirectionHeuristic, String paramString) {
    // Byte code:
    //   0: aload_0
    //   1: invokeinterface length : ()I
    //   6: istore #8
    //   8: aconst_null
    //   9: astore #9
    //   11: aload_1
    //   12: aload_0
    //   13: iconst_0
    //   14: aload_0
    //   15: invokeinterface length : ()I
    //   20: aload #6
    //   22: aconst_null
    //   23: invokestatic buildForMeasurement : (Landroid/text/TextPaint;Ljava/lang/CharSequence;IILandroid/text/TextDirectionHeuristic;Landroid/text/MeasuredParagraph;)Landroid/text/MeasuredParagraph;
    //   26: astore #6
    //   28: aload #6
    //   30: astore #9
    //   32: aload #6
    //   34: invokevirtual getWholeWidth : ()F
    //   37: fstore #10
    //   39: fload #10
    //   41: fload_2
    //   42: fcmpg
    //   43: ifgt -> 76
    //   46: aload #5
    //   48: ifnull -> 64
    //   51: aload #6
    //   53: astore #9
    //   55: aload #5
    //   57: iconst_0
    //   58: iconst_0
    //   59: invokeinterface ellipsized : (II)V
    //   64: aload #6
    //   66: ifnull -> 74
    //   69: aload #6
    //   71: invokevirtual recycle : ()V
    //   74: aload_0
    //   75: areturn
    //   76: aload_1
    //   77: aload #7
    //   79: invokevirtual measureText : (Ljava/lang/String;)F
    //   82: fstore #10
    //   84: fload_2
    //   85: fload #10
    //   87: fsub
    //   88: fstore #10
    //   90: iconst_0
    //   91: istore #11
    //   93: fload #10
    //   95: fconst_0
    //   96: fcmpg
    //   97: ifge -> 107
    //   100: iload #8
    //   102: istore #12
    //   104: goto -> 235
    //   107: fload #10
    //   109: fstore_2
    //   110: aload_3
    //   111: getstatic android/text/TextUtils$TruncateAt.START : Landroid/text/TextUtils$TruncateAt;
    //   114: if_acmpne -> 138
    //   117: fload #10
    //   119: fstore_2
    //   120: iload #8
    //   122: aload #6
    //   124: iload #8
    //   126: iconst_0
    //   127: fload #10
    //   129: invokevirtual breakText : (IZF)I
    //   132: isub
    //   133: istore #12
    //   135: goto -> 235
    //   138: fload #10
    //   140: fstore_2
    //   141: aload_3
    //   142: getstatic android/text/TextUtils$TruncateAt.END : Landroid/text/TextUtils$TruncateAt;
    //   145: if_acmpeq -> 216
    //   148: fload #10
    //   150: fstore_2
    //   151: aload_3
    //   152: getstatic android/text/TextUtils$TruncateAt.END_SMALL : Landroid/text/TextUtils$TruncateAt;
    //   155: if_acmpne -> 161
    //   158: goto -> 216
    //   161: fload #10
    //   163: fstore_2
    //   164: iload #8
    //   166: aload #6
    //   168: iload #8
    //   170: iconst_0
    //   171: fload #10
    //   173: fconst_2
    //   174: fdiv
    //   175: invokevirtual breakText : (IZF)I
    //   178: isub
    //   179: istore #12
    //   181: fload #10
    //   183: fstore_2
    //   184: fload #10
    //   186: aload #6
    //   188: iload #12
    //   190: iload #8
    //   192: invokevirtual measure : (II)F
    //   195: fsub
    //   196: fstore #10
    //   198: fload #10
    //   200: fstore_2
    //   201: aload #6
    //   203: iload #12
    //   205: iconst_1
    //   206: fload #10
    //   208: invokevirtual breakText : (IZF)I
    //   211: istore #11
    //   213: goto -> 235
    //   216: fload #10
    //   218: fstore_2
    //   219: aload #6
    //   221: iload #8
    //   223: iconst_1
    //   224: fload #10
    //   226: invokevirtual breakText : (IZF)I
    //   229: istore #11
    //   231: iload #8
    //   233: istore #12
    //   235: aload #5
    //   237: ifnull -> 251
    //   240: aload #5
    //   242: iload #11
    //   244: iload #12
    //   246: invokeinterface ellipsized : (II)V
    //   251: aload #6
    //   253: invokevirtual getChars : ()[C
    //   256: astore_3
    //   257: aload_0
    //   258: instanceof android/text/Spanned
    //   261: ifeq -> 272
    //   264: aload_0
    //   265: checkcast android/text/Spanned
    //   268: astore_1
    //   269: goto -> 274
    //   272: aconst_null
    //   273: astore_1
    //   274: iload #12
    //   276: iload #11
    //   278: isub
    //   279: istore #13
    //   281: iload #8
    //   283: iload #13
    //   285: isub
    //   286: istore #14
    //   288: iload #4
    //   290: ifeq -> 414
    //   293: iload #14
    //   295: ifle -> 335
    //   298: iload #13
    //   300: aload #7
    //   302: invokevirtual length : ()I
    //   305: if_icmplt -> 335
    //   308: aload #7
    //   310: iconst_0
    //   311: aload #7
    //   313: invokevirtual length : ()I
    //   316: aload_3
    //   317: iload #11
    //   319: invokevirtual getChars : (II[CI)V
    //   322: iload #11
    //   324: aload #7
    //   326: invokevirtual length : ()I
    //   329: iadd
    //   330: istore #11
    //   332: goto -> 335
    //   335: iload #11
    //   337: iload #12
    //   339: if_icmpge -> 354
    //   342: aload_3
    //   343: iload #11
    //   345: ldc 65279
    //   347: castore
    //   348: iinc #11, 1
    //   351: goto -> 335
    //   354: new java/lang/String
    //   357: astore_0
    //   358: aload_0
    //   359: aload_3
    //   360: iconst_0
    //   361: iload #8
    //   363: invokespecial <init> : ([CII)V
    //   366: aload_1
    //   367: ifnonnull -> 382
    //   370: aload #6
    //   372: ifnull -> 380
    //   375: aload #6
    //   377: invokevirtual recycle : ()V
    //   380: aload_0
    //   381: areturn
    //   382: new android/text/SpannableString
    //   385: astore_3
    //   386: aload_3
    //   387: aload_0
    //   388: invokespecial <init> : (Ljava/lang/CharSequence;)V
    //   391: aload_1
    //   392: iconst_0
    //   393: iload #8
    //   395: ldc java/lang/Object
    //   397: aload_3
    //   398: iconst_0
    //   399: invokestatic copySpansFrom : (Landroid/text/Spanned;IILjava/lang/Class;Landroid/text/Spannable;I)V
    //   402: aload #6
    //   404: ifnull -> 412
    //   407: aload #6
    //   409: invokevirtual recycle : ()V
    //   412: aload_3
    //   413: areturn
    //   414: iload #14
    //   416: ifne -> 432
    //   419: aload #6
    //   421: ifnull -> 429
    //   424: aload #6
    //   426: invokevirtual recycle : ()V
    //   429: ldc ''
    //   431: areturn
    //   432: aload_1
    //   433: ifnonnull -> 498
    //   436: new java/lang/StringBuilder
    //   439: astore_0
    //   440: aload_0
    //   441: iload #14
    //   443: aload #7
    //   445: invokevirtual length : ()I
    //   448: iadd
    //   449: invokespecial <init> : (I)V
    //   452: aload_0
    //   453: aload_3
    //   454: iconst_0
    //   455: iload #11
    //   457: invokevirtual append : ([CII)Ljava/lang/StringBuilder;
    //   460: pop
    //   461: aload_0
    //   462: aload #7
    //   464: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   467: pop
    //   468: aload_0
    //   469: aload_3
    //   470: iload #12
    //   472: iload #8
    //   474: iload #12
    //   476: isub
    //   477: invokevirtual append : ([CII)Ljava/lang/StringBuilder;
    //   480: pop
    //   481: aload_0
    //   482: invokevirtual toString : ()Ljava/lang/String;
    //   485: astore_0
    //   486: aload #6
    //   488: ifnull -> 496
    //   491: aload #6
    //   493: invokevirtual recycle : ()V
    //   496: aload_0
    //   497: areturn
    //   498: new android/text/SpannableStringBuilder
    //   501: astore_1
    //   502: aload_1
    //   503: invokespecial <init> : ()V
    //   506: aload_1
    //   507: aload_0
    //   508: iconst_0
    //   509: iload #11
    //   511: invokevirtual append : (Ljava/lang/CharSequence;II)Landroid/text/SpannableStringBuilder;
    //   514: pop
    //   515: aload_1
    //   516: aload #7
    //   518: invokevirtual append : (Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
    //   521: pop
    //   522: aload_1
    //   523: aload_0
    //   524: iload #12
    //   526: iload #8
    //   528: invokevirtual append : (Ljava/lang/CharSequence;II)Landroid/text/SpannableStringBuilder;
    //   531: pop
    //   532: aload #6
    //   534: ifnull -> 542
    //   537: aload #6
    //   539: invokevirtual recycle : ()V
    //   542: aload_1
    //   543: areturn
    //   544: astore_0
    //   545: goto -> 561
    //   548: astore_0
    //   549: goto -> 561
    //   552: astore_0
    //   553: goto -> 561
    //   556: astore_0
    //   557: aload #9
    //   559: astore #6
    //   561: aload #6
    //   563: ifnull -> 571
    //   566: aload #6
    //   568: invokevirtual recycle : ()V
    //   571: aload_0
    //   572: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1327	-> 0
    //   #1329	-> 8
    //   #1331	-> 11
    //   #1332	-> 28
    //   #1334	-> 39
    //   #1335	-> 46
    //   #1336	-> 51
    //   #1339	-> 64
    //   #1405	-> 64
    //   #1406	-> 69
    //   #1339	-> 74
    //   #1344	-> 76
    //   #1345	-> 84
    //   #1347	-> 90
    //   #1348	-> 93
    //   #1349	-> 93
    //   #1351	-> 107
    //   #1352	-> 117
    //   #1353	-> 138
    //   #1356	-> 161
    //   #1357	-> 181
    //   #1358	-> 198
    //   #1354	-> 216
    //   #1361	-> 235
    //   #1362	-> 240
    //   #1365	-> 251
    //   #1366	-> 257
    //   #1368	-> 274
    //   #1369	-> 281
    //   #1370	-> 288
    //   #1371	-> 293
    //   #1372	-> 308
    //   #1373	-> 322
    //   #1375	-> 335
    //   #1376	-> 342
    //   #1375	-> 348
    //   #1378	-> 354
    //   #1379	-> 366
    //   #1380	-> 370
    //   #1405	-> 370
    //   #1406	-> 375
    //   #1380	-> 380
    //   #1382	-> 382
    //   #1383	-> 391
    //   #1384	-> 402
    //   #1405	-> 402
    //   #1406	-> 407
    //   #1384	-> 412
    //   #1387	-> 414
    //   #1388	-> 419
    //   #1405	-> 419
    //   #1406	-> 424
    //   #1388	-> 429
    //   #1391	-> 432
    //   #1392	-> 436
    //   #1393	-> 452
    //   #1394	-> 461
    //   #1395	-> 468
    //   #1396	-> 481
    //   #1405	-> 486
    //   #1406	-> 491
    //   #1396	-> 496
    //   #1399	-> 498
    //   #1400	-> 506
    //   #1401	-> 515
    //   #1402	-> 522
    //   #1403	-> 532
    //   #1405	-> 532
    //   #1406	-> 537
    //   #1403	-> 542
    //   #1405	-> 544
    //   #1406	-> 566
    //   #1408	-> 571
    // Exception table:
    //   from	to	target	type
    //   11	28	556	finally
    //   32	39	556	finally
    //   55	64	556	finally
    //   76	84	552	finally
    //   110	117	548	finally
    //   120	135	548	finally
    //   141	148	548	finally
    //   151	158	548	finally
    //   164	181	548	finally
    //   184	198	548	finally
    //   201	213	548	finally
    //   219	231	548	finally
    //   240	251	544	finally
    //   251	257	544	finally
    //   257	269	544	finally
    //   298	308	544	finally
    //   308	322	544	finally
    //   322	332	544	finally
    //   354	366	544	finally
    //   382	391	544	finally
    //   391	402	544	finally
    //   436	452	544	finally
    //   452	461	544	finally
    //   461	468	544	finally
    //   468	481	544	finally
    //   481	486	544	finally
    //   498	506	544	finally
    //   506	515	544	finally
    //   515	522	544	finally
    //   522	532	544	finally
  }
  
  public static CharSequence listEllipsize(Context paramContext, List<CharSequence> paramList, String paramString, TextPaint paramTextPaint, float paramFloat, int paramInt) {
    Resources resources;
    BidiFormatter bidiFormatter;
    if (paramList == null)
      return ""; 
    int i = paramList.size();
    if (i == 0)
      return ""; 
    if (paramContext == null) {
      paramContext = null;
      bidiFormatter = BidiFormatter.getInstance();
    } else {
      resources = paramContext.getResources();
      bidiFormatter = BidiFormatter.getInstance(resources.getConfiguration().getLocales().get(0));
    } 
    SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
    int[] arrayOfInt = new int[i];
    int j;
    for (j = 0; j < i; j++) {
      spannableStringBuilder.append(bidiFormatter.unicodeWrap(paramList.get(j)));
      if (j != i - 1)
        spannableStringBuilder.append(paramString); 
      arrayOfInt[j] = spannableStringBuilder.length();
    } 
    for (j = i - 1; j >= 0; j--) {
      spannableStringBuilder.delete(arrayOfInt[j], spannableStringBuilder.length());
      int k = i - j - 1;
      if (k > 0) {
        String str;
        if (resources == null) {
          str = "…";
        } else {
          str = resources.getQuantityString(paramInt, k, new Object[] { Integer.valueOf(k) });
        } 
        CharSequence charSequence = bidiFormatter.unicodeWrap(str);
        spannableStringBuilder.append(charSequence);
      } 
      float f = paramTextPaint.measureText(spannableStringBuilder, 0, spannableStringBuilder.length());
      if (f <= paramFloat)
        return spannableStringBuilder; 
    } 
    return "";
  }
  
  @Deprecated
  public static CharSequence commaEllipsize(CharSequence paramCharSequence, TextPaint paramTextPaint, float paramFloat, String paramString1, String paramString2) {
    return commaEllipsize(paramCharSequence, paramTextPaint, paramFloat, paramString1, paramString2, TextDirectionHeuristics.FIRSTSTRONG_LTR);
  }
  
  @Deprecated
  public static CharSequence commaEllipsize(CharSequence paramCharSequence, TextPaint paramTextPaint, float paramFloat, String paramString1, String paramString2, TextDirectionHeuristic paramTextDirectionHeuristic) {
    // Byte code:
    //   0: aconst_null
    //   1: astore #6
    //   3: aconst_null
    //   4: astore #7
    //   6: aconst_null
    //   7: astore #8
    //   9: aload #6
    //   11: astore #9
    //   13: aload #7
    //   15: astore #10
    //   17: aload_0
    //   18: invokeinterface length : ()I
    //   23: istore #11
    //   25: aload #6
    //   27: astore #9
    //   29: aload #7
    //   31: astore #10
    //   33: aload_1
    //   34: aload_0
    //   35: iconst_0
    //   36: iload #11
    //   38: aload #5
    //   40: aconst_null
    //   41: invokestatic buildForMeasurement : (Landroid/text/TextPaint;Ljava/lang/CharSequence;IILandroid/text/TextDirectionHeuristic;Landroid/text/MeasuredParagraph;)Landroid/text/MeasuredParagraph;
    //   44: astore #6
    //   46: aload #6
    //   48: astore #9
    //   50: aload #7
    //   52: astore #10
    //   54: aload #6
    //   56: invokevirtual getWholeWidth : ()F
    //   59: fstore #12
    //   61: fload #12
    //   63: fload_2
    //   64: fcmpg
    //   65: ifgt -> 92
    //   68: aload #6
    //   70: ifnull -> 78
    //   73: aload #6
    //   75: invokevirtual recycle : ()V
    //   78: iconst_0
    //   79: ifeq -> 90
    //   82: new java/lang/NullPointerException
    //   85: dup
    //   86: invokespecial <init> : ()V
    //   89: athrow
    //   90: aload_0
    //   91: areturn
    //   92: aload #6
    //   94: astore #9
    //   96: aload #7
    //   98: astore #10
    //   100: aload #6
    //   102: invokevirtual getChars : ()[C
    //   105: astore #13
    //   107: iconst_0
    //   108: istore #14
    //   110: iconst_0
    //   111: istore #15
    //   113: iload #15
    //   115: iload #11
    //   117: if_icmpge -> 150
    //   120: iload #14
    //   122: istore #16
    //   124: aload #13
    //   126: iload #15
    //   128: caload
    //   129: bipush #44
    //   131: if_icmpne -> 140
    //   134: iload #14
    //   136: iconst_1
    //   137: iadd
    //   138: istore #16
    //   140: iinc #15, 1
    //   143: iload #16
    //   145: istore #14
    //   147: goto -> 113
    //   150: iload #14
    //   152: iconst_1
    //   153: iadd
    //   154: istore #17
    //   156: iconst_0
    //   157: istore #16
    //   159: ldc ''
    //   161: astore #18
    //   163: iconst_0
    //   164: istore #19
    //   166: iconst_0
    //   167: istore #14
    //   169: aload #6
    //   171: astore #9
    //   173: aload #7
    //   175: astore #10
    //   177: aload #6
    //   179: invokevirtual getWidths : ()Landroid/text/AutoGrowArray$FloatArray;
    //   182: invokevirtual getRawArray : ()[F
    //   185: astore #20
    //   187: iconst_0
    //   188: istore #15
    //   190: aload #18
    //   192: astore #7
    //   194: iload #15
    //   196: iload #11
    //   198: if_icmpge -> 487
    //   201: iload #19
    //   203: i2f
    //   204: aload #20
    //   206: iload #15
    //   208: faload
    //   209: fadd
    //   210: f2i
    //   211: istore #19
    //   213: aload #13
    //   215: iload #15
    //   217: caload
    //   218: istore #21
    //   220: iload #21
    //   222: bipush #44
    //   224: if_icmpne -> 473
    //   227: iload #14
    //   229: iconst_1
    //   230: iadd
    //   231: istore #21
    //   233: iload #17
    //   235: iconst_1
    //   236: isub
    //   237: istore #22
    //   239: iload #22
    //   241: iconst_1
    //   242: if_icmpne -> 304
    //   245: new java/lang/StringBuilder
    //   248: astore #18
    //   250: aload #18
    //   252: invokespecial <init> : ()V
    //   255: aload #18
    //   257: ldc ' '
    //   259: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   262: pop
    //   263: aload #6
    //   265: astore #9
    //   267: aload #8
    //   269: astore #10
    //   271: aload #18
    //   273: aload_3
    //   274: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   277: pop
    //   278: aload #6
    //   280: astore #9
    //   282: aload #8
    //   284: astore #10
    //   286: aload #18
    //   288: invokevirtual toString : ()Ljava/lang/String;
    //   291: astore #18
    //   293: aload #18
    //   295: astore #10
    //   297: goto -> 384
    //   300: astore_0
    //   301: goto -> 558
    //   304: aload #6
    //   306: astore #9
    //   308: aload #8
    //   310: astore #10
    //   312: new java/lang/StringBuilder
    //   315: astore #18
    //   317: aload #6
    //   319: astore #9
    //   321: aload #8
    //   323: astore #10
    //   325: aload #18
    //   327: invokespecial <init> : ()V
    //   330: aload #6
    //   332: astore #9
    //   334: aload #8
    //   336: astore #10
    //   338: aload #18
    //   340: ldc ' '
    //   342: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   345: pop
    //   346: aload #8
    //   348: astore #9
    //   350: aload #18
    //   352: aload #4
    //   354: iconst_1
    //   355: anewarray java/lang/Object
    //   358: dup
    //   359: iconst_0
    //   360: iload #22
    //   362: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   365: aastore
    //   366: invokestatic format : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   369: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   372: pop
    //   373: aload #8
    //   375: astore #9
    //   377: aload #18
    //   379: invokevirtual toString : ()Ljava/lang/String;
    //   382: astore #10
    //   384: aload #8
    //   386: astore #9
    //   388: aload #10
    //   390: invokevirtual length : ()I
    //   393: istore #14
    //   395: aload #8
    //   397: astore #9
    //   399: aload_1
    //   400: aload #10
    //   402: iconst_0
    //   403: iload #14
    //   405: aload #5
    //   407: aload #8
    //   409: invokestatic buildForMeasurement : (Landroid/text/TextPaint;Ljava/lang/CharSequence;IILandroid/text/TextDirectionHeuristic;Landroid/text/MeasuredParagraph;)Landroid/text/MeasuredParagraph;
    //   412: astore #8
    //   414: aload #8
    //   416: astore #9
    //   418: aload #8
    //   420: invokevirtual getWholeWidth : ()F
    //   423: fstore #23
    //   425: iload #22
    //   427: istore #17
    //   429: aload #8
    //   431: astore #9
    //   433: iload #21
    //   435: istore #14
    //   437: iload #19
    //   439: i2f
    //   440: fload #23
    //   442: fadd
    //   443: fload_2
    //   444: fcmpg
    //   445: ifgt -> 477
    //   448: iload #15
    //   450: iconst_1
    //   451: iadd
    //   452: istore #16
    //   454: iload #22
    //   456: istore #17
    //   458: aload #8
    //   460: astore #9
    //   462: aload #10
    //   464: astore #7
    //   466: iload #21
    //   468: istore #14
    //   470: goto -> 477
    //   473: aload #8
    //   475: astore #9
    //   477: iinc #15, 1
    //   480: aload #9
    //   482: astore #8
    //   484: goto -> 194
    //   487: aload #8
    //   489: astore #9
    //   491: new android/text/SpannableStringBuilder
    //   494: astore_1
    //   495: aload #8
    //   497: astore #9
    //   499: aload_1
    //   500: aload #7
    //   502: invokespecial <init> : (Ljava/lang/CharSequence;)V
    //   505: aload #8
    //   507: astore #9
    //   509: aload_1
    //   510: iconst_0
    //   511: aload_0
    //   512: iconst_0
    //   513: iload #16
    //   515: invokevirtual insert : (ILjava/lang/CharSequence;II)Landroid/text/SpannableStringBuilder;
    //   518: pop
    //   519: aload #6
    //   521: ifnull -> 529
    //   524: aload #6
    //   526: invokevirtual recycle : ()V
    //   529: aload #8
    //   531: ifnull -> 539
    //   534: aload #8
    //   536: invokevirtual recycle : ()V
    //   539: aload_1
    //   540: areturn
    //   541: astore_0
    //   542: aload #9
    //   544: astore #8
    //   546: goto -> 558
    //   549: astore_0
    //   550: aload #10
    //   552: astore #8
    //   554: aload #9
    //   556: astore #6
    //   558: aload #6
    //   560: ifnull -> 568
    //   563: aload #6
    //   565: invokevirtual recycle : ()V
    //   568: aload #8
    //   570: ifnull -> 578
    //   573: aload #8
    //   575: invokevirtual recycle : ()V
    //   578: aload_0
    //   579: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1524	-> 0
    //   #1525	-> 3
    //   #1527	-> 9
    //   #1528	-> 25
    //   #1529	-> 46
    //   #1530	-> 61
    //   #1531	-> 68
    //   #1583	-> 68
    //   #1584	-> 73
    //   #1586	-> 78
    //   #1587	-> 82
    //   #1531	-> 90
    //   #1534	-> 92
    //   #1536	-> 107
    //   #1537	-> 110
    //   #1538	-> 120
    //   #1539	-> 134
    //   #1537	-> 140
    //   #1543	-> 150
    //   #1545	-> 156
    //   #1546	-> 159
    //   #1548	-> 163
    //   #1549	-> 166
    //   #1550	-> 169
    //   #1552	-> 187
    //   #1553	-> 201
    //   #1555	-> 213
    //   #1556	-> 227
    //   #1561	-> 233
    //   #1562	-> 245
    //   #1583	-> 300
    //   #1564	-> 304
    //   #1568	-> 384
    //   #1569	-> 384
    //   #1568	-> 395
    //   #1570	-> 414
    //   #1572	-> 425
    //   #1573	-> 448
    //   #1574	-> 448
    //   #1555	-> 473
    //   #1552	-> 477
    //   #1579	-> 487
    //   #1580	-> 505
    //   #1581	-> 519
    //   #1583	-> 519
    //   #1584	-> 524
    //   #1586	-> 529
    //   #1587	-> 534
    //   #1581	-> 539
    //   #1583	-> 541
    //   #1584	-> 563
    //   #1586	-> 568
    //   #1587	-> 573
    //   #1589	-> 578
    // Exception table:
    //   from	to	target	type
    //   17	25	549	finally
    //   33	46	549	finally
    //   54	61	549	finally
    //   100	107	549	finally
    //   177	187	549	finally
    //   245	263	300	finally
    //   271	278	549	finally
    //   286	293	549	finally
    //   312	317	549	finally
    //   325	330	549	finally
    //   338	346	549	finally
    //   350	373	541	finally
    //   377	384	541	finally
    //   388	395	541	finally
    //   399	414	541	finally
    //   418	425	541	finally
    //   491	495	541	finally
    //   499	505	541	finally
    //   509	519	541	finally
  }
  
  static boolean couldAffectRtl(char paramChar) {
    return (('֐' <= paramChar && paramChar <= 'ࣿ') || paramChar == '‎' || paramChar == '‏' || ('‪' <= paramChar && paramChar <= '‮') || ('⁦' <= paramChar && paramChar <= '⁩') || ('?' <= paramChar && paramChar <= '?') || ('יִ' <= paramChar && paramChar <= '﷿') || ('ﹰ' <= paramChar && paramChar <= '﻾'));
  }
  
  static boolean doesNotNeedBidi(char[] paramArrayOfchar, int paramInt1, int paramInt2) {
    for (int i = paramInt1; i < paramInt1 + paramInt2; i++) {
      if (couldAffectRtl(paramArrayOfchar[i]))
        return false; 
    } 
    return true;
  }
  
  static char[] obtain(int paramInt) {
    synchronized (sLock) {
      char[] arrayOfChar = sTemp;
      sTemp = null;
      if (arrayOfChar != null) {
        null = arrayOfChar;
        if (arrayOfChar.length < paramInt) {
          null = ArrayUtils.newUnpaddedCharArray(paramInt);
          return (char[])null;
        } 
        return (char[])null;
      } 
      null = ArrayUtils.newUnpaddedCharArray(paramInt);
      return (char[])null;
    } 
  }
  
  static void recycle(char[] paramArrayOfchar) {
    if (paramArrayOfchar.length > 1000)
      return; 
    synchronized (sLock) {
      sTemp = paramArrayOfchar;
      return;
    } 
  }
  
  public static String htmlEncode(String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    for (byte b = 0; b < paramString.length(); b++) {
      char c = paramString.charAt(b);
      if (c != '"') {
        if (c != '<') {
          if (c != '>') {
            if (c != '&') {
              if (c != '\'') {
                stringBuilder.append(c);
              } else {
                stringBuilder.append("&#39;");
              } 
            } else {
              stringBuilder.append("&amp;");
            } 
          } else {
            stringBuilder.append("&gt;");
          } 
        } else {
          stringBuilder.append("&lt;");
        } 
      } else {
        stringBuilder.append("&quot;");
      } 
    } 
    return stringBuilder.toString();
  }
  
  public static CharSequence concat(CharSequence... paramVarArgs) {
    if (paramVarArgs.length == 0)
      return ""; 
    int i = paramVarArgs.length;
    boolean bool1 = false, bool2 = false;
    if (i == 1)
      return paramVarArgs[0]; 
    byte b = 0;
    int j = paramVarArgs.length;
    i = 0;
    while (true) {
      k = b;
      if (i < j) {
        CharSequence charSequence = paramVarArgs[i];
        if (charSequence instanceof Spanned) {
          k = 1;
          break;
        } 
        i++;
        continue;
      } 
      break;
    } 
    if (k) {
      SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
      for (k = paramVarArgs.length, i = bool2; i < k; ) {
        CharSequence charSequence = paramVarArgs[i];
        if (charSequence == null)
          charSequence = "null"; 
        spannableStringBuilder.append(charSequence);
        i++;
      } 
      return new SpannedString(spannableStringBuilder);
    } 
    StringBuilder stringBuilder = new StringBuilder();
    for (int k = paramVarArgs.length; i < k; ) {
      CharSequence charSequence = paramVarArgs[i];
      stringBuilder.append(charSequence);
      i++;
    } 
    return stringBuilder.toString();
  }
  
  public static boolean isGraphic(CharSequence paramCharSequence) {
    int i = paramCharSequence.length();
    for (int j = 0; j < i; j += Character.charCount(k)) {
      int k = Character.codePointAt(paramCharSequence, j);
      int m = Character.getType(k);
      if (m != 15 && m != 16 && m != 19 && m != 0 && m != 13 && m != 14 && m != 12)
        return true; 
    } 
    return false;
  }
  
  @Deprecated
  public static boolean isGraphic(char paramChar) {
    boolean bool;
    int i = Character.getType(paramChar);
    if (i != 15 && i != 16 && i != 19 && i != 0 && i != 13 && i != 14 && i != 12) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isDigitsOnly(CharSequence paramCharSequence) {
    int i = paramCharSequence.length();
    for (int j = 0; j < i; j += Character.charCount(k)) {
      int k = Character.codePointAt(paramCharSequence, j);
      if (!Character.isDigit(k))
        return false; 
    } 
    return true;
  }
  
  public static boolean isPrintableAscii(char paramChar) {
    return ((' ' <= paramChar && paramChar <= '~') || paramChar == '\r' || paramChar == '\n');
  }
  
  public static boolean isPrintableAsciiOnly(CharSequence paramCharSequence) {
    int i = paramCharSequence.length();
    for (byte b = 0; b < i; b++) {
      if (!isPrintableAscii(paramCharSequence.charAt(b)))
        return false; 
    } 
    return true;
  }
  
  public static int getCapsMode(CharSequence paramCharSequence, int paramInt1, int paramInt2) {
    if (paramInt1 < 0)
      return 0; 
    int i = 0;
    if ((paramInt2 & 0x1000) != 0)
      i = Character.MIN_VALUE | 0x1000; 
    if ((paramInt2 & 0x6000) == 0)
      return i; 
    for (; paramInt1 > 0; paramInt1--) {
      char c = paramCharSequence.charAt(paramInt1 - 1);
      if (c != '"' && c != '\'' && 
        Character.getType(c) != 21)
        break; 
    } 
    int j = paramInt1;
    while (j > 0) {
      char c = paramCharSequence.charAt(j - 1);
      if (c == ' ' || c == '\t')
        j--; 
    } 
    if (j == 0 || paramCharSequence.charAt(j - 1) == '\n')
      return i | 0x2000; 
    if ((paramInt2 & 0x4000) == 0) {
      paramInt2 = i;
      if (paramInt1 != j)
        paramInt2 = i | 0x2000; 
      return paramInt2;
    } 
    paramInt2 = j;
    if (paramInt1 == j)
      return i; 
    for (; paramInt2 > 0; paramInt2--) {
      char c = paramCharSequence.charAt(paramInt2 - 1);
      if (c != '"' && c != '\'' && 
        Character.getType(c) != 22)
        break; 
    } 
    if (paramInt2 > 0) {
      paramInt1 = paramCharSequence.charAt(paramInt2 - 1);
      if (paramInt1 == 46 || paramInt1 == 63 || paramInt1 == 33) {
        if (paramInt1 == 46)
          for (paramInt1 = paramInt2 - 2; paramInt1 >= 0; paramInt1--) {
            char c = paramCharSequence.charAt(paramInt1);
            if (c == '.')
              return i; 
            if (!Character.isLetter(c))
              break; 
          }  
        return i | 0x4000;
      } 
    } 
    return i;
  }
  
  public static boolean delimitedStringContains(String paramString1, char paramChar, String paramString2) {
    if (isEmpty(paramString1) || isEmpty(paramString2))
      return false; 
    int i = -1;
    int j = paramString1.length();
    while (true) {
      int k = paramString1.indexOf(paramString2, i + 1);
      if (k != -1) {
        if (i > 0 && paramString1.charAt(i - 1) != paramChar)
          continue; 
        k = paramString2.length() + i;
        if (k == j)
          return true; 
        if (paramString1.charAt(k) == paramChar)
          return true; 
        continue;
      } 
      break;
    } 
    return false;
  }
  
  public static <T> T[] removeEmptySpans(T[] paramArrayOfT, Spanned paramSpanned, Class<T> paramClass) {
    Object[] arrayOfObject = null;
    int i = 0;
    for (byte b = 0; b < paramArrayOfT.length; b++, arrayOfObject = arrayOfObject1, i = j) {
      Object[] arrayOfObject1;
      T t = paramArrayOfT[b];
      int j = paramSpanned.getSpanStart(t);
      int k = paramSpanned.getSpanEnd(t);
      if (j == k) {
        arrayOfObject1 = arrayOfObject;
        j = i;
        if (arrayOfObject == null) {
          arrayOfObject1 = (Object[])Array.newInstance(paramClass, paramArrayOfT.length - 1);
          System.arraycopy(paramArrayOfT, 0, arrayOfObject1, 0, b);
          j = b;
        } 
      } else {
        arrayOfObject1 = arrayOfObject;
        j = i;
        if (arrayOfObject != null) {
          arrayOfObject[i] = t;
          j = i + 1;
          arrayOfObject1 = arrayOfObject;
        } 
      } 
    } 
    if (arrayOfObject != null) {
      paramArrayOfT = (T[])Array.newInstance(paramClass, i);
      System.arraycopy(arrayOfObject, 0, paramArrayOfT, 0, i);
      return paramArrayOfT;
    } 
    return paramArrayOfT;
  }
  
  public static long packRangeInLong(int paramInt1, int paramInt2) {
    return paramInt1 << 32L | paramInt2;
  }
  
  public static int unpackRangeStartFromLong(long paramLong) {
    return (int)(paramLong >>> 32L);
  }
  
  public static int unpackRangeEndFromLong(long paramLong) {
    return (int)(0xFFFFFFFFL & paramLong);
  }
  
  public static int getLayoutDirectionFromLocale(Locale paramLocale) {
    null = false;
    return (paramLocale == null || paramLocale.equals(Locale.ROOT) || 
      !ULocale.forLocale(paramLocale).isRightToLeft()) ? (
      
      ((Boolean)DisplayProperties.debug_force_rtl().orElse(Boolean.valueOf(false))).booleanValue() ? 
      1 : null) : 1;
  }
  
  public static CharSequence formatSelectedCount(int paramInt) {
    return Resources.getSystem().getQuantityString(18153496, paramInt, new Object[] { Integer.valueOf(paramInt) });
  }
  
  public static boolean hasStyleSpan(Spanned paramSpanned) {
    boolean bool;
    if (paramSpanned != null) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool);
    Class[] arrayOfClass = new Class[3];
    arrayOfClass[0] = CharacterStyle.class;
    arrayOfClass[1] = ParagraphStyle.class;
    arrayOfClass[2] = UpdateAppearance.class;
    int i;
    byte b;
    for (i = arrayOfClass.length, b = 0; b < i; ) {
      Class clazz = arrayOfClass[b];
      if (paramSpanned.nextSpanTransition(-1, paramSpanned.length(), clazz) < paramSpanned.length())
        return true; 
      b++;
    } 
    return false;
  }
  
  public static CharSequence trimNoCopySpans(CharSequence paramCharSequence) {
    if (paramCharSequence != null && paramCharSequence instanceof Spanned)
      return new SpannableStringBuilder(paramCharSequence); 
    return paramCharSequence;
  }
  
  public static void wrap(StringBuilder paramStringBuilder, String paramString1, String paramString2) {
    paramStringBuilder.insert(0, paramString1);
    paramStringBuilder.append(paramString2);
  }
  
  public static <T extends CharSequence> T trimToParcelableSize(T paramT) {
    return trimToSize(paramT, 100000);
  }
  
  public static <T extends CharSequence> T trimToSize(T paramT, int paramInt) {
    boolean bool;
    if (paramInt > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool);
    if (isEmpty((CharSequence)paramT) || paramT.length() <= paramInt)
      return paramT; 
    int i = paramInt;
    if (Character.isHighSurrogate(paramT.charAt(paramInt - 1))) {
      i = paramInt;
      if (Character.isLowSurrogate(paramT.charAt(paramInt)))
        i = paramInt - 1; 
    } 
    return (T)paramT.subSequence(0, i);
  }
  
  public static <T extends CharSequence> T trimToLengthWithEllipsis(T paramT, int paramInt) {
    CharSequence charSequence1 = (CharSequence)trimToSize((Object)paramT, paramInt);
    CharSequence charSequence2 = charSequence1;
    if (charSequence1.length() < paramT.length()) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(charSequence1.toString());
      stringBuilder.append("...");
      charSequence2 = stringBuilder.toString();
    } 
    return (T)charSequence2;
  }
  
  private static boolean isNewline(int paramInt) {
    int i = Character.getType(paramInt);
    return (i == 14 || i == 13 || paramInt == 10);
  }
  
  private static boolean isWhiteSpace(int paramInt) {
    return (Character.isWhitespace(paramInt) || paramInt == 160);
  }
  
  public static String withoutPrefix(String paramString1, String paramString2) {
    if (paramString1 == null || paramString2 == null)
      return paramString2; 
    if (paramString2.startsWith(paramString1)) {
      paramString1 = paramString2.substring(paramString1.length());
    } else {
      paramString1 = paramString2;
    } 
    return paramString1;
  }
  
  public static CharSequence makeSafeForPresentation(String paramString, int paramInt1, float paramFloat, int paramInt2) {
    boolean bool2, bool3, bool4;
    boolean bool1 = true;
    if ((paramInt2 & 0x4) != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    if ((paramInt2 & 0x2) != 0) {
      bool3 = true;
    } else {
      bool3 = false;
    } 
    if ((paramInt2 & 0x1) != 0) {
      bool4 = true;
    } else {
      bool4 = false;
    } 
    Preconditions.checkNotNull(paramString);
    Preconditions.checkArgumentNonnegative(paramInt1);
    Preconditions.checkArgumentNonNegative(paramFloat, "ellipsizeDip");
    Preconditions.checkFlagsArgument(paramInt2, 7);
    boolean bool5 = bool1;
    if (bool2)
      if (!bool3) {
        bool5 = bool1;
      } else {
        bool5 = false;
      }  
    Preconditions.checkArgument(bool5, "Cannot set SAFE_STRING_FLAG_SINGLE_LINE and SAFE_STRING_FLAG_FIRST_LINE at thesame time");
    if (paramInt1 > 0)
      paramString = paramString.substring(0, Math.min(paramString.length(), paramInt1)); 
    StringWithRemovedChars stringWithRemovedChars = new StringWithRemovedChars(Html.fromHtml(paramString).toString());
    paramInt2 = -1;
    int i = -1;
    int j = stringWithRemovedChars.length();
    for (paramInt1 = 0; paramInt1 < j; ) {
      int i1, k = stringWithRemovedChars.codePointAt(paramInt1);
      int m = Character.getType(k);
      int n = Character.charCount(k);
      bool5 = isNewline(k);
      if (bool2 && bool5) {
        stringWithRemovedChars.removeAllCharAfter(paramInt1);
        break;
      } 
      if (bool3 && bool5) {
        stringWithRemovedChars.removeRange(paramInt1, paramInt1 + n);
        i1 = paramInt2;
        m = i;
      } else if (m == 15 && !bool5) {
        stringWithRemovedChars.removeRange(paramInt1, paramInt1 + n);
        i1 = paramInt2;
        m = i;
      } else {
        i1 = paramInt2;
        m = i;
        if (bool4) {
          i1 = paramInt2;
          m = i;
          if (!isWhiteSpace(k)) {
            i = paramInt2;
            if (paramInt2 == -1)
              i = paramInt1; 
            m = paramInt1 + n;
            i1 = i;
          } 
        } 
      } 
      paramInt1 += n;
      paramInt2 = i1;
      i = m;
    } 
    if (bool4)
      if (paramInt2 == -1) {
        stringWithRemovedChars.removeAllCharAfter(0);
      } else {
        if (paramInt2 > 0)
          stringWithRemovedChars.removeAllCharBefore(paramInt2); 
        if (i < j)
          stringWithRemovedChars.removeAllCharAfter(i); 
      }  
    if (paramFloat == 0.0F)
      return stringWithRemovedChars.toString(); 
    TextPaint textPaint = new TextPaint();
    textPaint.setTextSize(42.0F);
    return ellipsize(stringWithRemovedChars.toString(), textPaint, paramFloat, TruncateAt.END);
  }
  
  private static class StringWithRemovedChars {
    private final String mOriginal;
    
    private BitSet mRemovedChars;
    
    StringWithRemovedChars(String param1String) {
      this.mOriginal = param1String;
    }
    
    void removeRange(int param1Int1, int param1Int2) {
      if (this.mRemovedChars == null)
        this.mRemovedChars = new BitSet(this.mOriginal.length()); 
      this.mRemovedChars.set(param1Int1, param1Int2);
    }
    
    void removeAllCharBefore(int param1Int) {
      if (this.mRemovedChars == null)
        this.mRemovedChars = new BitSet(this.mOriginal.length()); 
      this.mRemovedChars.set(0, param1Int);
    }
    
    void removeAllCharAfter(int param1Int) {
      if (this.mRemovedChars == null)
        this.mRemovedChars = new BitSet(this.mOriginal.length()); 
      this.mRemovedChars.set(param1Int, this.mOriginal.length());
    }
    
    public String toString() {
      if (this.mRemovedChars == null)
        return this.mOriginal; 
      StringBuilder stringBuilder = new StringBuilder(this.mOriginal.length());
      for (byte b = 0; b < this.mOriginal.length(); b++) {
        if (!this.mRemovedChars.get(b))
          stringBuilder.append(this.mOriginal.charAt(b)); 
      } 
      return stringBuilder.toString();
    }
    
    int length() {
      return this.mOriginal.length();
    }
    
    int codePointAt(int param1Int) {
      return this.mOriginal.codePointAt(param1Int);
    }
  }
  
  private static Object sLock = new Object();
  
  private static char[] sTemp = null;
  
  static {
    EMPTY_STRING_ARRAY = new String[0];
  }
  
  public static interface EllipsizeCallback {
    void ellipsized(int param1Int1, int param1Int2);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface SafeStringFlags {}
  
  public static interface StringSplitter extends Iterable<String> {
    void setString(String param1String);
  }
}
