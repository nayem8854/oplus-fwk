package android.text;

import android.os.Parcel;

public class Annotation implements ParcelableSpan {
  private final String mKey;
  
  private final String mValue;
  
  public Annotation(String paramString1, String paramString2) {
    this.mKey = paramString1;
    this.mValue = paramString2;
  }
  
  public Annotation(Parcel paramParcel) {
    this.mKey = paramParcel.readString();
    this.mValue = paramParcel.readString();
  }
  
  public int getSpanTypeId() {
    return getSpanTypeIdInternal();
  }
  
  public int getSpanTypeIdInternal() {
    return 18;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    writeToParcelInternal(paramParcel, paramInt);
  }
  
  public void writeToParcelInternal(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mKey);
    paramParcel.writeString(this.mValue);
  }
  
  public String getKey() {
    return this.mKey;
  }
  
  public String getValue() {
    return this.mValue;
  }
}
