package android.text;

import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.style.AlignmentSpan;
import android.text.style.BackgroundColorSpan;
import android.text.style.BulletSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.text.style.QuoteSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.text.style.SubscriptSpan;
import android.text.style.SuperscriptSpan;
import android.text.style.TypefaceSpan;
import android.text.style.URLSpan;
import android.text.style.UnderlineSpan;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.ccil.cowan.tagsoup.Parser;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

class HtmlToSpannedConverter implements ContentHandler {
  private static final float[] HEADING_SIZES = new float[] { 1.5F, 1.4F, 1.3F, 1.2F, 1.1F, 1.0F };
  
  private static Pattern sBackgroundColorPattern;
  
  private static final Map<String, Integer> sColorMap;
  
  private static Pattern sForegroundColorPattern;
  
  private static Pattern sTextAlignPattern;
  
  private static Pattern sTextDecorationPattern;
  
  private int mFlags;
  
  private Html.ImageGetter mImageGetter;
  
  private XMLReader mReader;
  
  private String mSource;
  
  private SpannableStringBuilder mSpannableStringBuilder;
  
  private Html.TagHandler mTagHandler;
  
  static {
    HashMap<Object, Object> hashMap = new HashMap<>();
    Integer integer2 = Integer.valueOf(-5658199);
    hashMap.put("darkgray", integer2);
    Map<String, Integer> map = sColorMap;
    Integer integer1 = Integer.valueOf(-8355712);
    map.put("gray", integer1);
    map = sColorMap;
    Integer integer3 = Integer.valueOf(-2894893);
    map.put("lightgray", integer3);
    sColorMap.put("darkgrey", integer2);
    sColorMap.put("grey", integer1);
    sColorMap.put("lightgrey", integer3);
    sColorMap.put("green", Integer.valueOf(-16744448));
  }
  
  private static Pattern getTextAlignPattern() {
    if (sTextAlignPattern == null)
      sTextAlignPattern = Pattern.compile("(?:\\s+|\\A)text-align\\s*:\\s*(\\S*)\\b"); 
    return sTextAlignPattern;
  }
  
  private static Pattern getForegroundColorPattern() {
    if (sForegroundColorPattern == null)
      sForegroundColorPattern = Pattern.compile("(?:\\s+|\\A)color\\s*:\\s*(\\S*)\\b"); 
    return sForegroundColorPattern;
  }
  
  private static Pattern getBackgroundColorPattern() {
    if (sBackgroundColorPattern == null)
      sBackgroundColorPattern = Pattern.compile("(?:\\s+|\\A)background(?:-color)?\\s*:\\s*(\\S*)\\b"); 
    return sBackgroundColorPattern;
  }
  
  private static Pattern getTextDecorationPattern() {
    if (sTextDecorationPattern == null)
      sTextDecorationPattern = Pattern.compile("(?:\\s+|\\A)text-decoration\\s*:\\s*(\\S*)\\b"); 
    return sTextDecorationPattern;
  }
  
  public HtmlToSpannedConverter(String paramString, Html.ImageGetter paramImageGetter, Html.TagHandler paramTagHandler, Parser paramParser, int paramInt) {
    this.mSource = paramString;
    this.mSpannableStringBuilder = new SpannableStringBuilder();
    this.mImageGetter = paramImageGetter;
    this.mTagHandler = paramTagHandler;
    this.mReader = (XMLReader)paramParser;
    this.mFlags = paramInt;
  }
  
  public Spanned convert() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mReader : Lorg/xml/sax/XMLReader;
    //   4: aload_0
    //   5: invokeinterface setContentHandler : (Lorg/xml/sax/ContentHandler;)V
    //   10: aload_0
    //   11: getfield mReader : Lorg/xml/sax/XMLReader;
    //   14: astore_1
    //   15: new org/xml/sax/InputSource
    //   18: astore_2
    //   19: new java/io/StringReader
    //   22: astore_3
    //   23: aload_3
    //   24: aload_0
    //   25: getfield mSource : Ljava/lang/String;
    //   28: invokespecial <init> : (Ljava/lang/String;)V
    //   31: aload_2
    //   32: aload_3
    //   33: invokespecial <init> : (Ljava/io/Reader;)V
    //   36: aload_1
    //   37: aload_2
    //   38: invokeinterface parse : (Lorg/xml/sax/InputSource;)V
    //   43: aload_0
    //   44: getfield mSpannableStringBuilder : Landroid/text/SpannableStringBuilder;
    //   47: astore_1
    //   48: aload_1
    //   49: iconst_0
    //   50: aload_1
    //   51: invokevirtual length : ()I
    //   54: ldc_w android/text/style/ParagraphStyle
    //   57: invokevirtual getSpans : (IILjava/lang/Class;)[Ljava/lang/Object;
    //   60: astore_1
    //   61: iconst_0
    //   62: istore #4
    //   64: iload #4
    //   66: aload_1
    //   67: arraylength
    //   68: if_icmpge -> 200
    //   71: aload_0
    //   72: getfield mSpannableStringBuilder : Landroid/text/SpannableStringBuilder;
    //   75: aload_1
    //   76: iload #4
    //   78: aaload
    //   79: invokevirtual getSpanStart : (Ljava/lang/Object;)I
    //   82: istore #5
    //   84: aload_0
    //   85: getfield mSpannableStringBuilder : Landroid/text/SpannableStringBuilder;
    //   88: aload_1
    //   89: iload #4
    //   91: aaload
    //   92: invokevirtual getSpanEnd : (Ljava/lang/Object;)I
    //   95: istore #6
    //   97: iload #6
    //   99: istore #7
    //   101: iload #6
    //   103: iconst_2
    //   104: isub
    //   105: iflt -> 156
    //   108: iload #6
    //   110: istore #7
    //   112: aload_0
    //   113: getfield mSpannableStringBuilder : Landroid/text/SpannableStringBuilder;
    //   116: iload #6
    //   118: iconst_1
    //   119: isub
    //   120: invokevirtual charAt : (I)C
    //   123: bipush #10
    //   125: if_icmpne -> 156
    //   128: aload_0
    //   129: getfield mSpannableStringBuilder : Landroid/text/SpannableStringBuilder;
    //   132: astore_3
    //   133: iload #6
    //   135: istore #7
    //   137: aload_3
    //   138: iload #6
    //   140: iconst_2
    //   141: isub
    //   142: invokevirtual charAt : (I)C
    //   145: bipush #10
    //   147: if_icmpne -> 156
    //   150: iload #6
    //   152: iconst_1
    //   153: isub
    //   154: istore #7
    //   156: iload #7
    //   158: iload #5
    //   160: if_icmpne -> 177
    //   163: aload_0
    //   164: getfield mSpannableStringBuilder : Landroid/text/SpannableStringBuilder;
    //   167: aload_1
    //   168: iload #4
    //   170: aaload
    //   171: invokevirtual removeSpan : (Ljava/lang/Object;)V
    //   174: goto -> 194
    //   177: aload_0
    //   178: getfield mSpannableStringBuilder : Landroid/text/SpannableStringBuilder;
    //   181: aload_1
    //   182: iload #4
    //   184: aaload
    //   185: iload #5
    //   187: iload #7
    //   189: bipush #51
    //   191: invokevirtual setSpan : (Ljava/lang/Object;III)V
    //   194: iinc #4, 1
    //   197: goto -> 64
    //   200: aload_0
    //   201: getfield mSpannableStringBuilder : Landroid/text/SpannableStringBuilder;
    //   204: areturn
    //   205: astore_1
    //   206: new java/lang/RuntimeException
    //   209: dup
    //   210: aload_1
    //   211: invokespecial <init> : (Ljava/lang/Throwable;)V
    //   214: athrow
    //   215: astore_1
    //   216: new java/lang/RuntimeException
    //   219: dup
    //   220: aload_1
    //   221: invokespecial <init> : (Ljava/lang/Throwable;)V
    //   224: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #746	-> 0
    //   #748	-> 10
    //   #755	-> 43
    //   #758	-> 43
    //   #759	-> 61
    //   #760	-> 71
    //   #761	-> 84
    //   #764	-> 97
    //   #765	-> 108
    //   #766	-> 133
    //   #767	-> 150
    //   #771	-> 156
    //   #772	-> 163
    //   #774	-> 177
    //   #759	-> 194
    //   #778	-> 200
    //   #752	-> 205
    //   #754	-> 206
    //   #749	-> 215
    //   #751	-> 216
    // Exception table:
    //   from	to	target	type
    //   10	43	215	java/io/IOException
    //   10	43	205	org/xml/sax/SAXException
  }
  
  private void handleStartTag(String paramString, Attributes paramAttributes) {
    if (!paramString.equalsIgnoreCase("br"))
      if (paramString.equalsIgnoreCase("p")) {
        startBlockElement(this.mSpannableStringBuilder, paramAttributes, getMarginParagraph());
        startCssStyle(this.mSpannableStringBuilder, paramAttributes);
      } else if (paramString.equalsIgnoreCase("ul")) {
        startBlockElement(this.mSpannableStringBuilder, paramAttributes, getMarginList());
      } else if (paramString.equalsIgnoreCase("li")) {
        startLi(this.mSpannableStringBuilder, paramAttributes);
      } else if (paramString.equalsIgnoreCase("div")) {
        startBlockElement(this.mSpannableStringBuilder, paramAttributes, getMarginDiv());
      } else if (paramString.equalsIgnoreCase("span")) {
        startCssStyle(this.mSpannableStringBuilder, paramAttributes);
      } else if (paramString.equalsIgnoreCase("strong")) {
        start(this.mSpannableStringBuilder, new Bold());
      } else if (paramString.equalsIgnoreCase("b")) {
        start(this.mSpannableStringBuilder, new Bold());
      } else if (paramString.equalsIgnoreCase("em")) {
        start(this.mSpannableStringBuilder, new Italic());
      } else if (paramString.equalsIgnoreCase("cite")) {
        start(this.mSpannableStringBuilder, new Italic());
      } else if (paramString.equalsIgnoreCase("dfn")) {
        start(this.mSpannableStringBuilder, new Italic());
      } else if (paramString.equalsIgnoreCase("i")) {
        start(this.mSpannableStringBuilder, new Italic());
      } else if (paramString.equalsIgnoreCase("big")) {
        start(this.mSpannableStringBuilder, new Big());
      } else if (paramString.equalsIgnoreCase("small")) {
        start(this.mSpannableStringBuilder, new Small());
      } else if (paramString.equalsIgnoreCase("font")) {
        startFont(this.mSpannableStringBuilder, paramAttributes);
      } else if (paramString.equalsIgnoreCase("blockquote")) {
        startBlockquote(this.mSpannableStringBuilder, paramAttributes);
      } else if (paramString.equalsIgnoreCase("tt")) {
        start(this.mSpannableStringBuilder, new Monospace());
      } else if (paramString.equalsIgnoreCase("a")) {
        startA(this.mSpannableStringBuilder, paramAttributes);
      } else if (paramString.equalsIgnoreCase("u")) {
        start(this.mSpannableStringBuilder, new Underline());
      } else if (paramString.equalsIgnoreCase("del")) {
        start(this.mSpannableStringBuilder, new Strikethrough());
      } else if (paramString.equalsIgnoreCase("s")) {
        start(this.mSpannableStringBuilder, new Strikethrough());
      } else if (paramString.equalsIgnoreCase("strike")) {
        start(this.mSpannableStringBuilder, new Strikethrough());
      } else if (paramString.equalsIgnoreCase("sup")) {
        start(this.mSpannableStringBuilder, new Super());
      } else if (paramString.equalsIgnoreCase("sub")) {
        start(this.mSpannableStringBuilder, new Sub());
      } else if (paramString.length() == 2 && 
        Character.toLowerCase(paramString.charAt(0)) == 'h' && 
        paramString.charAt(1) >= '1' && paramString.charAt(1) <= '6') {
        startHeading(this.mSpannableStringBuilder, paramAttributes, paramString.charAt(1) - 49);
      } else if (paramString.equalsIgnoreCase("img")) {
        startImg(this.mSpannableStringBuilder, paramAttributes, this.mImageGetter);
      } else {
        Html.TagHandler tagHandler = this.mTagHandler;
        if (tagHandler != null)
          tagHandler.handleTag(true, paramString, this.mSpannableStringBuilder, this.mReader); 
      }  
  }
  
  private void handleEndTag(String paramString) {
    if (paramString.equalsIgnoreCase("br")) {
      handleBr(this.mSpannableStringBuilder);
    } else if (paramString.equalsIgnoreCase("p")) {
      endCssStyle(this.mSpannableStringBuilder);
      endBlockElement(this.mSpannableStringBuilder);
    } else if (paramString.equalsIgnoreCase("ul")) {
      endBlockElement(this.mSpannableStringBuilder);
    } else if (paramString.equalsIgnoreCase("li")) {
      endLi(this.mSpannableStringBuilder);
    } else if (paramString.equalsIgnoreCase("div")) {
      endBlockElement(this.mSpannableStringBuilder);
    } else if (paramString.equalsIgnoreCase("span")) {
      endCssStyle(this.mSpannableStringBuilder);
    } else if (paramString.equalsIgnoreCase("strong")) {
      end(this.mSpannableStringBuilder, Bold.class, new StyleSpan(1));
    } else if (paramString.equalsIgnoreCase("b")) {
      end(this.mSpannableStringBuilder, Bold.class, new StyleSpan(1));
    } else if (paramString.equalsIgnoreCase("em")) {
      end(this.mSpannableStringBuilder, Italic.class, new StyleSpan(2));
    } else if (paramString.equalsIgnoreCase("cite")) {
      end(this.mSpannableStringBuilder, Italic.class, new StyleSpan(2));
    } else if (paramString.equalsIgnoreCase("dfn")) {
      end(this.mSpannableStringBuilder, Italic.class, new StyleSpan(2));
    } else if (paramString.equalsIgnoreCase("i")) {
      end(this.mSpannableStringBuilder, Italic.class, new StyleSpan(2));
    } else if (paramString.equalsIgnoreCase("big")) {
      end(this.mSpannableStringBuilder, Big.class, new RelativeSizeSpan(1.25F));
    } else if (paramString.equalsIgnoreCase("small")) {
      end(this.mSpannableStringBuilder, Small.class, new RelativeSizeSpan(0.8F));
    } else if (paramString.equalsIgnoreCase("font")) {
      endFont(this.mSpannableStringBuilder);
    } else if (paramString.equalsIgnoreCase("blockquote")) {
      endBlockquote(this.mSpannableStringBuilder);
    } else if (paramString.equalsIgnoreCase("tt")) {
      end(this.mSpannableStringBuilder, Monospace.class, new TypefaceSpan("monospace"));
    } else if (paramString.equalsIgnoreCase("a")) {
      endA(this.mSpannableStringBuilder);
    } else if (paramString.equalsIgnoreCase("u")) {
      end(this.mSpannableStringBuilder, Underline.class, new UnderlineSpan());
    } else if (paramString.equalsIgnoreCase("del")) {
      end(this.mSpannableStringBuilder, Strikethrough.class, new StrikethroughSpan());
    } else if (paramString.equalsIgnoreCase("s")) {
      end(this.mSpannableStringBuilder, Strikethrough.class, new StrikethroughSpan());
    } else if (paramString.equalsIgnoreCase("strike")) {
      end(this.mSpannableStringBuilder, Strikethrough.class, new StrikethroughSpan());
    } else if (paramString.equalsIgnoreCase("sup")) {
      end(this.mSpannableStringBuilder, Super.class, new SuperscriptSpan());
    } else if (paramString.equalsIgnoreCase("sub")) {
      end(this.mSpannableStringBuilder, Sub.class, new SubscriptSpan());
    } else if (paramString.length() == 2 && 
      Character.toLowerCase(paramString.charAt(0)) == 'h' && 
      paramString.charAt(1) >= '1' && paramString.charAt(1) <= '6') {
      endHeading(this.mSpannableStringBuilder);
    } else {
      Html.TagHandler tagHandler = this.mTagHandler;
      if (tagHandler != null)
        tagHandler.handleTag(false, paramString, this.mSpannableStringBuilder, this.mReader); 
    } 
  }
  
  private int getMarginParagraph() {
    return getMargin(1);
  }
  
  private int getMarginHeading() {
    return getMargin(2);
  }
  
  private int getMarginListItem() {
    return getMargin(4);
  }
  
  private int getMarginList() {
    return getMargin(8);
  }
  
  private int getMarginDiv() {
    return getMargin(16);
  }
  
  private int getMarginBlockquote() {
    return getMargin(32);
  }
  
  private int getMargin(int paramInt) {
    if ((this.mFlags & paramInt) != 0)
      return 1; 
    return 2;
  }
  
  private static void appendNewlines(Editable paramEditable, int paramInt) {
    int i = paramEditable.length();
    if (i == 0)
      return; 
    byte b = 0;
    for (; --i >= 0 && paramEditable.charAt(i) == '\n'; i--)
      b++; 
    for (; b < paramInt; b++)
      paramEditable.append("\n"); 
  }
  
  private static void startBlockElement(Editable paramEditable, Attributes paramAttributes, int paramInt) {
    paramEditable.length();
    if (paramInt > 0) {
      appendNewlines(paramEditable, paramInt);
      start(paramEditable, new Newline(paramInt));
    } 
    String str = paramAttributes.getValue("", "style");
    if (str != null) {
      Matcher matcher = getTextAlignPattern().matcher(str);
      if (matcher.find()) {
        String str1 = matcher.group(1);
        if (str1.equalsIgnoreCase("start")) {
          start(paramEditable, new Alignment(Layout.Alignment.ALIGN_NORMAL));
        } else if (str1.equalsIgnoreCase("center")) {
          start(paramEditable, new Alignment(Layout.Alignment.ALIGN_CENTER));
        } else if (str1.equalsIgnoreCase("end")) {
          start(paramEditable, new Alignment(Layout.Alignment.ALIGN_OPPOSITE));
        } 
      } 
    } 
  }
  
  private static void endBlockElement(Editable paramEditable) {
    Newline newline = getLast(paramEditable, Newline.class);
    if (newline != null) {
      appendNewlines(paramEditable, newline.mNumNewlines);
      paramEditable.removeSpan(newline);
    } 
    Alignment alignment = getLast(paramEditable, Alignment.class);
    if (alignment != null)
      setSpanFromMark(paramEditable, alignment, new Object[] { new AlignmentSpan.Standard(Alignment.access$1000(alignment)) }); 
  }
  
  private static void handleBr(Editable paramEditable) {
    paramEditable.append('\n');
  }
  
  private void startLi(Editable paramEditable, Attributes paramAttributes) {
    startBlockElement(paramEditable, paramAttributes, getMarginListItem());
    start(paramEditable, new Bullet());
    startCssStyle(paramEditable, paramAttributes);
  }
  
  private static void endLi(Editable paramEditable) {
    endCssStyle(paramEditable);
    endBlockElement(paramEditable);
    end(paramEditable, Bullet.class, new BulletSpan());
  }
  
  private void startBlockquote(Editable paramEditable, Attributes paramAttributes) {
    startBlockElement(paramEditable, paramAttributes, getMarginBlockquote());
    start(paramEditable, new Blockquote());
  }
  
  private static void endBlockquote(Editable paramEditable) {
    endBlockElement(paramEditable);
    end(paramEditable, Blockquote.class, new QuoteSpan());
  }
  
  private void startHeading(Editable paramEditable, Attributes paramAttributes, int paramInt) {
    startBlockElement(paramEditable, paramAttributes, getMarginHeading());
    start(paramEditable, new Heading(paramInt));
  }
  
  private static void endHeading(Editable paramEditable) {
    Heading heading = getLast(paramEditable, Heading.class);
    if (heading != null)
      setSpanFromMark(paramEditable, heading, new Object[] { new RelativeSizeSpan(HEADING_SIZES[Heading.access$1300(heading)]), new StyleSpan(1) }); 
    endBlockElement(paramEditable);
  }
  
  private static <T> T getLast(Spanned paramSpanned, Class<T> paramClass) {
    // Byte code:
    //   0: aload_0
    //   1: iconst_0
    //   2: aload_0
    //   3: invokeinterface length : ()I
    //   8: aload_1
    //   9: invokeinterface getSpans : (IILjava/lang/Class;)[Ljava/lang/Object;
    //   14: astore_0
    //   15: aload_0
    //   16: arraylength
    //   17: ifne -> 22
    //   20: aconst_null
    //   21: areturn
    //   22: aload_0
    //   23: aload_0
    //   24: arraylength
    //   25: iconst_1
    //   26: isub
    //   27: aaload
    //   28: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1040	-> 0
    //   #1042	-> 15
    //   #1043	-> 20
    //   #1045	-> 22
  }
  
  private static void setSpanFromMark(Spannable paramSpannable, Object paramObject, Object... paramVarArgs) {
    int i = paramSpannable.getSpanStart(paramObject);
    paramSpannable.removeSpan(paramObject);
    int j = paramSpannable.length();
    if (i != j) {
      int k;
      byte b;
      for (k = paramVarArgs.length, b = 0; b < k; ) {
        paramObject = paramVarArgs[b];
        paramSpannable.setSpan(paramObject, i, j, 33);
        b++;
      } 
    } 
  }
  
  private static void start(Editable paramEditable, Object paramObject) {
    int i = paramEditable.length();
    paramEditable.setSpan(paramObject, i, i, 17);
  }
  
  private static void end(Editable paramEditable, Class<Class<?>> paramClass, Object paramObject) {
    paramEditable.length();
    paramClass = getLast(paramEditable, paramClass);
    if (paramClass != null)
      setSpanFromMark(paramEditable, paramClass, new Object[] { paramObject }); 
  }
  
  private void startCssStyle(Editable paramEditable, Attributes paramAttributes) {
    String str = paramAttributes.getValue("", "style");
    if (str != null) {
      Matcher matcher2 = getForegroundColorPattern().matcher(str);
      if (matcher2.find()) {
        int i = getHtmlColor(matcher2.group(1));
        if (i != -1)
          start(paramEditable, new Foreground(i | 0xFF000000)); 
      } 
      matcher2 = getBackgroundColorPattern().matcher(str);
      if (matcher2.find()) {
        int i = getHtmlColor(matcher2.group(1));
        if (i != -1)
          start(paramEditable, new Background(0xFF000000 | i)); 
      } 
      Matcher matcher1 = getTextDecorationPattern().matcher(str);
      if (matcher1.find()) {
        String str1 = matcher1.group(1);
        if (str1.equalsIgnoreCase("line-through"))
          start(paramEditable, new Strikethrough()); 
      } 
    } 
  }
  
  private static void endCssStyle(Editable paramEditable) {
    Strikethrough strikethrough = getLast(paramEditable, Strikethrough.class);
    if (strikethrough != null)
      setSpanFromMark(paramEditable, strikethrough, new Object[] { new StrikethroughSpan() }); 
    Background background = getLast(paramEditable, Background.class);
    if (background != null)
      setSpanFromMark(paramEditable, background, new Object[] { new BackgroundColorSpan(Background.access$1400(background)) }); 
    Foreground foreground = getLast(paramEditable, Foreground.class);
    if (foreground != null)
      setSpanFromMark(paramEditable, foreground, new Object[] { new ForegroundColorSpan(Foreground.access$1500(foreground)) }); 
  }
  
  private static void startImg(Editable paramEditable, Attributes paramAttributes, Html.ImageGetter paramImageGetter) {
    Drawable drawable1;
    String str = paramAttributes.getValue("", "src");
    paramAttributes = null;
    if (paramImageGetter != null)
      drawable1 = paramImageGetter.getDrawable(str); 
    Drawable drawable2 = drawable1;
    if (drawable1 == null) {
      Resources resources = Resources.getSystem();
      drawable2 = resources.getDrawable(17303805);
      drawable2.setBounds(0, 0, drawable2.getIntrinsicWidth(), drawable2.getIntrinsicHeight());
    } 
    int i = paramEditable.length();
    paramEditable.append("￼");
    paramEditable.setSpan(new ImageSpan(drawable2, str), i, paramEditable.length(), 33);
  }
  
  private void startFont(Editable paramEditable, Attributes paramAttributes) {
    String str2 = paramAttributes.getValue("", "color");
    String str1 = paramAttributes.getValue("", "face");
    if (!TextUtils.isEmpty(str2)) {
      int i = getHtmlColor(str2);
      if (i != -1)
        start(paramEditable, new Foreground(0xFF000000 | i)); 
    } 
    if (!TextUtils.isEmpty(str1))
      start(paramEditable, new Font(str1)); 
  }
  
  private static void endFont(Editable paramEditable) {
    Font font = getLast(paramEditable, Font.class);
    if (font != null)
      setSpanFromMark(paramEditable, font, new Object[] { new TypefaceSpan(font.mFace) }); 
    Foreground foreground = getLast(paramEditable, Foreground.class);
    if (foreground != null) {
      ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(foreground.mForegroundColor);
      setSpanFromMark(paramEditable, foreground, new Object[] { foregroundColorSpan });
    } 
  }
  
  private static void startA(Editable paramEditable, Attributes paramAttributes) {
    String str = paramAttributes.getValue("", "href");
    start(paramEditable, new Href(str));
  }
  
  private static void endA(Editable paramEditable) {
    Href href = getLast(paramEditable, Href.class);
    if (href != null && 
      href.mHref != null)
      setSpanFromMark(paramEditable, href, new Object[] { new URLSpan(href.mHref) }); 
  }
  
  private int getHtmlColor(String paramString) {
    if ((this.mFlags & 0x100) == 256) {
      Integer integer = sColorMap.get(paramString.toLowerCase(Locale.US));
      if (integer != null)
        return integer.intValue(); 
    } 
    return Color.getHtmlColor(paramString);
  }
  
  public void setDocumentLocator(Locator paramLocator) {}
  
  public void startDocument() throws SAXException {}
  
  public void endDocument() throws SAXException {}
  
  public void startPrefixMapping(String paramString1, String paramString2) throws SAXException {}
  
  public void endPrefixMapping(String paramString) throws SAXException {}
  
  public void startElement(String paramString1, String paramString2, String paramString3, Attributes paramAttributes) throws SAXException {
    handleStartTag(paramString2, paramAttributes);
  }
  
  public void endElement(String paramString1, String paramString2, String paramString3) throws SAXException {
    handleEndTag(paramString2);
  }
  
  public void characters(char[] paramArrayOfchar, int paramInt1, int paramInt2) throws SAXException {
    StringBuilder stringBuilder = new StringBuilder();
    for (byte b = 0; b < paramInt2; b++) {
      char c = paramArrayOfchar[b + paramInt1];
      if (c == ' ' || c == '\n') {
        int i = stringBuilder.length();
        if (i == 0) {
          i = this.mSpannableStringBuilder.length();
          if (i == 0) {
            i = 10;
          } else {
            i = this.mSpannableStringBuilder.charAt(i - 1);
          } 
        } else {
          i = stringBuilder.charAt(i - 1);
        } 
        if (i != 32 && i != 10)
          stringBuilder.append(' '); 
      } else {
        stringBuilder.append(c);
      } 
    } 
    this.mSpannableStringBuilder.append(stringBuilder);
  }
  
  public void ignorableWhitespace(char[] paramArrayOfchar, int paramInt1, int paramInt2) throws SAXException {}
  
  public void processingInstruction(String paramString1, String paramString2) throws SAXException {}
  
  public void skippedEntity(String paramString) throws SAXException {}
  
  private static class Bold {
    private Bold() {}
  }
  
  private static class Italic {
    private Italic() {}
  }
  
  private static class Underline {
    private Underline() {}
  }
  
  private static class Strikethrough {
    private Strikethrough() {}
  }
  
  class Big {
    private Big() {}
  }
  
  private static class Small {
    private Small() {}
  }
  
  private static class Monospace {
    private Monospace() {}
  }
  
  private static class Blockquote {
    private Blockquote() {}
  }
  
  private static class Super {
    private Super() {}
  }
  
  private static class Sub {
    private Sub() {}
  }
  
  private static class Bullet {
    private Bullet() {}
  }
  
  private static class Font {
    public String mFace;
    
    public Font(String param1String) {
      this.mFace = param1String;
    }
  }
  
  private static class Href {
    public String mHref;
    
    public Href(String param1String) {
      this.mHref = param1String;
    }
  }
  
  private static class Foreground {
    private int mForegroundColor;
    
    public Foreground(int param1Int) {
      this.mForegroundColor = param1Int;
    }
  }
  
  class Background {
    private int mBackgroundColor;
    
    public Background(HtmlToSpannedConverter this$0) {
      this.mBackgroundColor = this$0;
    }
  }
  
  private static class Heading {
    private int mLevel;
    
    public Heading(int param1Int) {
      this.mLevel = param1Int;
    }
  }
  
  private static class Newline {
    private int mNumNewlines;
    
    public Newline(int param1Int) {
      this.mNumNewlines = param1Int;
    }
  }
  
  class Alignment {
    private Layout.Alignment mAlignment;
    
    public Alignment(HtmlToSpannedConverter this$0) {
      this.mAlignment = (Layout.Alignment)this$0;
    }
  }
}
