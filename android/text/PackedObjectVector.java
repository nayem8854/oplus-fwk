package android.text;

import com.android.internal.util.ArrayUtils;
import com.android.internal.util.GrowingArrayUtils;
import java.io.PrintStream;
import libcore.util.EmptyArray;

class PackedObjectVector<E> {
  private int mColumns;
  
  private int mRowGapLength;
  
  private int mRowGapStart;
  
  private int mRows;
  
  private Object[] mValues;
  
  public PackedObjectVector(int paramInt) {
    this.mColumns = paramInt;
    this.mValues = EmptyArray.OBJECT;
    this.mRows = 0;
    this.mRowGapStart = 0;
    this.mRowGapLength = 0;
  }
  
  public E getValue(int paramInt1, int paramInt2) {
    int i = paramInt1;
    if (paramInt1 >= this.mRowGapStart)
      i = paramInt1 + this.mRowGapLength; 
    return (E)this.mValues[this.mColumns * i + paramInt2];
  }
  
  public void setValue(int paramInt1, int paramInt2, E paramE) {
    int i = paramInt1;
    if (paramInt1 >= this.mRowGapStart)
      i = paramInt1 + this.mRowGapLength; 
    this.mValues[this.mColumns * i + paramInt2] = paramE;
  }
  
  public void insertAt(int paramInt, E[] paramArrayOfE) {
    moveRowGapTo(paramInt);
    if (this.mRowGapLength == 0)
      growBuffer(); 
    this.mRowGapStart++;
    this.mRowGapLength--;
    if (paramArrayOfE == null) {
      for (byte b = 0; b < this.mColumns; b++)
        setValue(paramInt, b, null); 
    } else {
      for (byte b = 0; b < this.mColumns; b++)
        setValue(paramInt, b, paramArrayOfE[b]); 
    } 
  }
  
  public void deleteAt(int paramInt1, int paramInt2) {
    moveRowGapTo(paramInt1 + paramInt2);
    this.mRowGapStart -= paramInt2;
    this.mRowGapLength += paramInt2;
    size();
  }
  
  public int size() {
    return this.mRows - this.mRowGapLength;
  }
  
  public int width() {
    return this.mColumns;
  }
  
  private void growBuffer() {
    int i = GrowingArrayUtils.growSize(size()), j = this.mColumns;
    Object[] arrayOfObject1 = ArrayUtils.newUnpaddedObjectArray(i * j);
    j = arrayOfObject1.length;
    i = this.mColumns;
    j /= i;
    int k = this.mRows, m = this.mRowGapStart;
    k -= this.mRowGapLength + m;
    System.arraycopy(this.mValues, 0, arrayOfObject1, 0, i * m);
    Object[] arrayOfObject2 = this.mValues;
    m = this.mRows;
    i = this.mColumns;
    System.arraycopy(arrayOfObject2, (m - k) * i, arrayOfObject1, (j - k) * i, i * k);
    this.mRowGapLength += j - this.mRows;
    this.mRows = j;
    this.mValues = arrayOfObject1;
  }
  
  private void moveRowGapTo(int paramInt) {
    int i = this.mRowGapStart;
    if (paramInt == i)
      return; 
    if (paramInt > i) {
      int j = this.mRowGapLength;
      int k = i + j;
      while (true) {
        int m = this.mRowGapStart, n = this.mRowGapLength;
        if (k < m + n + paramInt + j - i + j) {
          byte b = 0;
          for (;; k++) {
            int i1 = this.mColumns;
            if (b < i1) {
              Object arrayOfObject[] = this.mValues, object = arrayOfObject[k * i1 + b];
              arrayOfObject[i1 * (k - n + m + m) + b] = object;
              b++;
              continue;
            } 
          } 
        } 
        break;
      } 
    } else {
      int j = i - paramInt;
      for (int k = paramInt + j - 1; k >= paramInt; ) {
        int m = this.mRowGapStart;
        i = this.mRowGapLength;
        byte b = 0;
        for (;; k--) {
          int n = this.mColumns;
          if (b < n) {
            Object arrayOfObject[] = this.mValues, object = arrayOfObject[k * n + b];
            arrayOfObject[n * (k - paramInt + m + i - j) + b] = object;
            b++;
            continue;
          } 
        } 
      } 
    } 
    this.mRowGapStart = paramInt;
  }
  
  public void dump() {
    for (byte b = 0; b < this.mRows; b++) {
      byte b1 = 0;
      while (true) {
        int i = this.mColumns;
        if (b1 < i) {
          Object object = this.mValues[i * b + b1];
          i = this.mRowGapStart;
          if (b < i || b >= i + this.mRowGapLength) {
            PrintStream printStream = System.out;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(object);
            stringBuilder.append(" ");
            printStream.print(stringBuilder.toString());
          } else {
            PrintStream printStream = System.out;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("(");
            stringBuilder.append(object);
            stringBuilder.append(") ");
            printStream.print(stringBuilder.toString());
          } 
          b1++;
          continue;
        } 
        break;
      } 
      System.out.print(" << \n");
    } 
    System.out.print("-----\n\n");
  }
}
