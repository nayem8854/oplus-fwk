package android.text;

import java.text.CharacterIterator;

public class CharSequenceCharacterIterator implements CharacterIterator {
  private final int mBeginIndex;
  
  private final CharSequence mCharSeq;
  
  private final int mEndIndex;
  
  private int mIndex;
  
  public CharSequenceCharacterIterator(CharSequence paramCharSequence, int paramInt1, int paramInt2) {
    this.mCharSeq = paramCharSequence;
    this.mIndex = paramInt1;
    this.mBeginIndex = paramInt1;
    this.mEndIndex = paramInt2;
  }
  
  public char first() {
    this.mIndex = this.mBeginIndex;
    return current();
  }
  
  public char last() {
    int i = this.mBeginIndex, j = this.mEndIndex;
    if (i == j) {
      this.mIndex = j;
      return Character.MAX_VALUE;
    } 
    this.mIndex = --j;
    return this.mCharSeq.charAt(j);
  }
  
  public char current() {
    char c;
    int i = this.mIndex;
    if (i == this.mEndIndex) {
      c = Character.MAX_VALUE;
    } else {
      c = this.mCharSeq.charAt(i);
    } 
    return c;
  }
  
  public char next() {
    int i = this.mIndex + 1;
    int j = this.mEndIndex;
    if (i >= j) {
      this.mIndex = j;
      return Character.MAX_VALUE;
    } 
    return this.mCharSeq.charAt(i);
  }
  
  public char previous() {
    int i = this.mIndex;
    if (i <= this.mBeginIndex)
      return Character.MAX_VALUE; 
    this.mIndex = --i;
    return this.mCharSeq.charAt(i);
  }
  
  public char setIndex(int paramInt) {
    if (this.mBeginIndex <= paramInt && paramInt <= this.mEndIndex) {
      this.mIndex = paramInt;
      return current();
    } 
    throw new IllegalArgumentException("invalid position");
  }
  
  public int getBeginIndex() {
    return this.mBeginIndex;
  }
  
  public int getEndIndex() {
    return this.mEndIndex;
  }
  
  public int getIndex() {
    return this.mIndex;
  }
  
  public Object clone() {
    try {
      return super.clone();
    } catch (CloneNotSupportedException cloneNotSupportedException) {
      throw new InternalError();
    } 
  }
}
