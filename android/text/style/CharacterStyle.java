package android.text.style;

import android.text.TextPaint;

public abstract class CharacterStyle {
  public static CharacterStyle wrap(CharacterStyle paramCharacterStyle) {
    if (paramCharacterStyle instanceof MetricAffectingSpan)
      return new MetricAffectingSpan.Passthrough((MetricAffectingSpan)paramCharacterStyle); 
    return new Passthrough(paramCharacterStyle);
  }
  
  public CharacterStyle getUnderlying() {
    return this;
  }
  
  public abstract void updateDrawState(TextPaint paramTextPaint);
  
  class Passthrough extends CharacterStyle {
    private CharacterStyle mStyle;
    
    public Passthrough(CharacterStyle this$0) {
      this.mStyle = this$0;
    }
    
    public void updateDrawState(TextPaint param1TextPaint) {
      this.mStyle.updateDrawState(param1TextPaint);
    }
    
    public CharacterStyle getUnderlying() {
      return this.mStyle.getUnderlying();
    }
  }
}
