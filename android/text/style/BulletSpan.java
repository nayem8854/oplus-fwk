package android.text.style;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Parcel;
import android.text.Layout;
import android.text.ParcelableSpan;
import android.text.Spanned;

public class BulletSpan implements LeadingMarginSpan, ParcelableSpan {
  private static final int STANDARD_BULLET_RADIUS = 4;
  
  private static final int STANDARD_COLOR = 0;
  
  public static final int STANDARD_GAP_WIDTH = 2;
  
  private final int mBulletRadius;
  
  private final int mColor;
  
  private final int mGapWidth;
  
  private final boolean mWantColor;
  
  public BulletSpan() {
    this(2, 0, false, 4);
  }
  
  public BulletSpan(int paramInt) {
    this(paramInt, 0, false, 4);
  }
  
  public BulletSpan(int paramInt1, int paramInt2) {
    this(paramInt1, paramInt2, true, 4);
  }
  
  public BulletSpan(int paramInt1, int paramInt2, int paramInt3) {
    this(paramInt1, paramInt2, true, paramInt3);
  }
  
  private BulletSpan(int paramInt1, int paramInt2, boolean paramBoolean, int paramInt3) {
    this.mGapWidth = paramInt1;
    this.mBulletRadius = paramInt3;
    this.mColor = paramInt2;
    this.mWantColor = paramBoolean;
  }
  
  public BulletSpan(Parcel paramParcel) {
    boolean bool;
    this.mGapWidth = paramParcel.readInt();
    if (paramParcel.readInt() != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mWantColor = bool;
    this.mColor = paramParcel.readInt();
    this.mBulletRadius = paramParcel.readInt();
  }
  
  public int getSpanTypeId() {
    return getSpanTypeIdInternal();
  }
  
  public int getSpanTypeIdInternal() {
    return 8;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    writeToParcelInternal(paramParcel, paramInt);
  }
  
  public void writeToParcelInternal(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mGapWidth);
    paramParcel.writeInt(this.mWantColor);
    paramParcel.writeInt(this.mColor);
    paramParcel.writeInt(this.mBulletRadius);
  }
  
  public int getLeadingMargin(boolean paramBoolean) {
    return this.mBulletRadius * 2 + this.mGapWidth;
  }
  
  public int getGapWidth() {
    return this.mGapWidth;
  }
  
  public int getBulletRadius() {
    return this.mBulletRadius;
  }
  
  public int getColor() {
    return this.mColor;
  }
  
  public void drawLeadingMargin(Canvas paramCanvas, Paint paramPaint, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, CharSequence paramCharSequence, int paramInt6, int paramInt7, boolean paramBoolean, Layout paramLayout) {
    if (((Spanned)paramCharSequence).getSpanStart(this) == paramInt6) {
      Paint.Style style = paramPaint.getStyle();
      paramInt4 = 0;
      if (this.mWantColor) {
        paramInt4 = paramPaint.getColor();
        paramPaint.setColor(this.mColor);
      } 
      paramPaint.setStyle(Paint.Style.FILL);
      if (paramLayout != null) {
        paramInt6 = paramLayout.getLineForOffset(paramInt6);
        paramInt5 -= paramLayout.getLineExtra(paramInt6);
      } 
      float f1 = (paramInt3 + paramInt5) / 2.0F;
      paramInt3 = this.mBulletRadius;
      float f2 = (paramInt2 * paramInt3 + paramInt1);
      paramCanvas.drawCircle(f2, f1, paramInt3, paramPaint);
      if (this.mWantColor)
        paramPaint.setColor(paramInt4); 
      paramPaint.setStyle(style);
    } 
  }
}
