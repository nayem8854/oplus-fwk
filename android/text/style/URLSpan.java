package android.text.style;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcel;
import android.text.ParcelableSpan;
import android.util.Log;
import android.view.View;

public class URLSpan extends ClickableSpan implements ParcelableSpan {
  private final String mURL;
  
  public URLSpan(String paramString) {
    this.mURL = paramString;
  }
  
  public URLSpan(Parcel paramParcel) {
    this.mURL = paramParcel.readString();
  }
  
  public int getSpanTypeId() {
    return getSpanTypeIdInternal();
  }
  
  public int getSpanTypeIdInternal() {
    return 11;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    writeToParcelInternal(paramParcel, paramInt);
  }
  
  public void writeToParcelInternal(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mURL);
  }
  
  public String getURL() {
    return this.mURL;
  }
  
  public void onClick(View paramView) {
    Uri uri = Uri.parse(getURL());
    Context context = paramView.getContext();
    Intent intent = new Intent("android.intent.action.VIEW", uri);
    intent.putExtra("com.android.browser.application_id", context.getPackageName());
    try {
      context.startActivity(intent);
    } catch (ActivityNotFoundException activityNotFoundException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Actvity was not found for intent, ");
      stringBuilder.append(intent.toString());
      Log.w("URLSpan", stringBuilder.toString());
    } 
  }
}
