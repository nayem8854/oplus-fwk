package android.text.style;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.text.TextPaint;

public abstract class ReplacementSpan extends MetricAffectingSpan {
  private CharSequence mContentDescription = null;
  
  public CharSequence getContentDescription() {
    return this.mContentDescription;
  }
  
  public void setContentDescription(CharSequence paramCharSequence) {
    this.mContentDescription = paramCharSequence;
  }
  
  public void updateMeasureState(TextPaint paramTextPaint) {}
  
  public void updateDrawState(TextPaint paramTextPaint) {}
  
  public abstract void draw(Canvas paramCanvas, CharSequence paramCharSequence, int paramInt1, int paramInt2, float paramFloat, int paramInt3, int paramInt4, int paramInt5, Paint paramPaint);
  
  public abstract int getSize(Paint paramPaint, CharSequence paramCharSequence, int paramInt1, int paramInt2, Paint.FontMetricsInt paramFontMetricsInt);
}
