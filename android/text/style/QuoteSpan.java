package android.text.style;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Parcel;
import android.text.Layout;
import android.text.ParcelableSpan;

public class QuoteSpan implements LeadingMarginSpan, ParcelableSpan {
  public static final int STANDARD_COLOR = -16776961;
  
  public static final int STANDARD_GAP_WIDTH_PX = 2;
  
  public static final int STANDARD_STRIPE_WIDTH_PX = 2;
  
  private final int mColor;
  
  private final int mGapWidth;
  
  private final int mStripeWidth;
  
  public QuoteSpan() {
    this(-16776961, 2, 2);
  }
  
  public QuoteSpan(int paramInt) {
    this(paramInt, 2, 2);
  }
  
  public QuoteSpan(int paramInt1, int paramInt2, int paramInt3) {
    this.mColor = paramInt1;
    this.mStripeWidth = paramInt2;
    this.mGapWidth = paramInt3;
  }
  
  public QuoteSpan(Parcel paramParcel) {
    this.mColor = paramParcel.readInt();
    this.mStripeWidth = paramParcel.readInt();
    this.mGapWidth = paramParcel.readInt();
  }
  
  public int getSpanTypeId() {
    return getSpanTypeIdInternal();
  }
  
  public int getSpanTypeIdInternal() {
    return 9;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    writeToParcelInternal(paramParcel, paramInt);
  }
  
  public void writeToParcelInternal(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mColor);
    paramParcel.writeInt(this.mStripeWidth);
    paramParcel.writeInt(this.mGapWidth);
  }
  
  public int getColor() {
    return this.mColor;
  }
  
  public int getStripeWidth() {
    return this.mStripeWidth;
  }
  
  public int getGapWidth() {
    return this.mGapWidth;
  }
  
  public int getLeadingMargin(boolean paramBoolean) {
    return this.mStripeWidth + this.mGapWidth;
  }
  
  public void drawLeadingMargin(Canvas paramCanvas, Paint paramPaint, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, CharSequence paramCharSequence, int paramInt6, int paramInt7, boolean paramBoolean, Layout paramLayout) {
    Paint.Style style = paramPaint.getStyle();
    paramInt4 = paramPaint.getColor();
    paramPaint.setStyle(Paint.Style.FILL);
    paramPaint.setColor(this.mColor);
    paramCanvas.drawRect(paramInt1, paramInt3, (this.mStripeWidth * paramInt2 + paramInt1), paramInt5, paramPaint);
    paramPaint.setStyle(style);
    paramPaint.setColor(paramInt4);
  }
}
