package android.text.style;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import android.view.accessibility.AccessibilityNodeInfo;

public class AccessibilityURLSpan extends URLSpan implements Parcelable {
  final AccessibilityClickableSpan mAccessibilityClickableSpan;
  
  public AccessibilityURLSpan(URLSpan paramURLSpan) {
    super(paramURLSpan.getURL());
    this.mAccessibilityClickableSpan = new AccessibilityClickableSpan(paramURLSpan.getId());
  }
  
  public AccessibilityURLSpan(Parcel paramParcel) {
    super(paramParcel);
    this.mAccessibilityClickableSpan = new AccessibilityClickableSpan(paramParcel);
  }
  
  public int getSpanTypeId() {
    return getSpanTypeIdInternal();
  }
  
  public int getSpanTypeIdInternal() {
    return 26;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    writeToParcelInternal(paramParcel, paramInt);
  }
  
  public void writeToParcelInternal(Parcel paramParcel, int paramInt) {
    super.writeToParcelInternal(paramParcel, paramInt);
    this.mAccessibilityClickableSpan.writeToParcel(paramParcel, paramInt);
  }
  
  public void onClick(View paramView) {
    this.mAccessibilityClickableSpan.onClick(paramView);
  }
  
  public void copyConnectionDataFrom(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    this.mAccessibilityClickableSpan.copyConnectionDataFrom(paramAccessibilityNodeInfo);
  }
}
