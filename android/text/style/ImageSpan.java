package android.text.style;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import java.io.InputStream;

public class ImageSpan extends DynamicDrawableSpan {
  private Uri mContentUri;
  
  private Context mContext;
  
  private Drawable mDrawable;
  
  private int mResourceId;
  
  private String mSource;
  
  @Deprecated
  public ImageSpan(Bitmap paramBitmap) {
    this((Context)null, paramBitmap, 0);
  }
  
  @Deprecated
  public ImageSpan(Bitmap paramBitmap, int paramInt) {
    this((Context)null, paramBitmap, paramInt);
  }
  
  public ImageSpan(Context paramContext, Bitmap paramBitmap) {
    this(paramContext, paramBitmap, 0);
  }
  
  public ImageSpan(Context paramContext, Bitmap paramBitmap, int paramInt) {
    super(paramInt);
    BitmapDrawable bitmapDrawable;
    this.mContext = paramContext;
    if (paramContext != null) {
      bitmapDrawable = new BitmapDrawable(paramContext.getResources(), paramBitmap);
    } else {
      bitmapDrawable = new BitmapDrawable(paramBitmap);
    } 
    this.mDrawable = (Drawable)bitmapDrawable;
    paramInt = bitmapDrawable.getIntrinsicWidth();
    int i = this.mDrawable.getIntrinsicHeight();
    Drawable drawable = this.mDrawable;
    if (paramInt <= 0)
      paramInt = 0; 
    if (i <= 0)
      i = 0; 
    drawable.setBounds(0, 0, paramInt, i);
  }
  
  public ImageSpan(Drawable paramDrawable) {
    this(paramDrawable, 0);
  }
  
  public ImageSpan(Drawable paramDrawable, int paramInt) {
    super(paramInt);
    this.mDrawable = paramDrawable;
  }
  
  public ImageSpan(Drawable paramDrawable, String paramString) {
    this(paramDrawable, paramString, 0);
  }
  
  public ImageSpan(Drawable paramDrawable, String paramString, int paramInt) {
    super(paramInt);
    this.mDrawable = paramDrawable;
    this.mSource = paramString;
  }
  
  public ImageSpan(Context paramContext, Uri paramUri) {
    this(paramContext, paramUri, 0);
  }
  
  public ImageSpan(Context paramContext, Uri paramUri, int paramInt) {
    super(paramInt);
    this.mContext = paramContext;
    this.mContentUri = paramUri;
    this.mSource = paramUri.toString();
  }
  
  public ImageSpan(Context paramContext, int paramInt) {
    this(paramContext, paramInt, 0);
  }
  
  public ImageSpan(Context paramContext, int paramInt1, int paramInt2) {
    super(paramInt2);
    this.mContext = paramContext;
    this.mResourceId = paramInt1;
  }
  
  public Drawable getDrawable() {
    Drawable drawable1 = null, drawable2 = null;
    if (this.mDrawable != null) {
      drawable1 = this.mDrawable;
    } else if (this.mContentUri != null) {
      drawable1 = drawable2;
      try {
        InputStream inputStream = this.mContext.getContentResolver().openInputStream(this.mContentUri);
        drawable1 = drawable2;
        Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
        drawable1 = drawable2;
        BitmapDrawable bitmapDrawable3 = new BitmapDrawable();
        drawable1 = drawable2;
        this(this.mContext.getResources(), bitmap);
        BitmapDrawable bitmapDrawable2 = bitmapDrawable3;
        BitmapDrawable bitmapDrawable1 = bitmapDrawable2;
        int i = bitmapDrawable2.getIntrinsicWidth();
        bitmapDrawable1 = bitmapDrawable2;
        int j = bitmapDrawable2.getIntrinsicHeight();
        bitmapDrawable1 = bitmapDrawable2;
        bitmapDrawable2.setBounds(0, 0, i, j);
        bitmapDrawable1 = bitmapDrawable2;
        inputStream.close();
        bitmapDrawable1 = bitmapDrawable2;
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to loaded content ");
        stringBuilder.append(this.mContentUri);
        Log.e("ImageSpan", stringBuilder.toString(), exception);
      } 
    } else {
      try {
        drawable2 = this.mContext.getDrawable(this.mResourceId);
        drawable1 = drawable2;
        int i = drawable2.getIntrinsicWidth();
        drawable1 = drawable2;
        int j = drawable2.getIntrinsicHeight();
        drawable1 = drawable2;
        drawable2.setBounds(0, 0, i, j);
        drawable1 = drawable2;
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unable to find resource: ");
        stringBuilder.append(this.mResourceId);
        Log.e("ImageSpan", stringBuilder.toString());
      } 
    } 
    return drawable1;
  }
  
  public String getSource() {
    return this.mSource;
  }
}
