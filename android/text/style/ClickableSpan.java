package android.text.style;

import android.text.TextPaint;
import android.view.View;

public abstract class ClickableSpan extends CharacterStyle implements UpdateAppearance {
  public ClickableSpan() {
    int i = sIdCounter;
    sIdCounter = i + 1;
    this.mId = i;
  }
  
  private static int sIdCounter = 0;
  
  private int mId;
  
  public void updateDrawState(TextPaint paramTextPaint) {
    paramTextPaint.setColor(paramTextPaint.linkColor);
    paramTextPaint.setUnderlineText(true);
  }
  
  public int getId() {
    return this.mId;
  }
  
  public abstract void onClick(View paramView);
}
