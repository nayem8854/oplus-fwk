package android.text.style;

import android.os.Parcel;
import android.text.ParcelableSpan;
import android.text.TextPaint;

public class ScaleXSpan extends MetricAffectingSpan implements ParcelableSpan {
  private final float mProportion;
  
  public ScaleXSpan(float paramFloat) {
    this.mProportion = paramFloat;
  }
  
  public ScaleXSpan(Parcel paramParcel) {
    this.mProportion = paramParcel.readFloat();
  }
  
  public int getSpanTypeId() {
    return getSpanTypeIdInternal();
  }
  
  public int getSpanTypeIdInternal() {
    return 4;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    writeToParcelInternal(paramParcel, paramInt);
  }
  
  public void writeToParcelInternal(Parcel paramParcel, int paramInt) {
    paramParcel.writeFloat(this.mProportion);
  }
  
  public float getScaleX() {
    return this.mProportion;
  }
  
  public void updateDrawState(TextPaint paramTextPaint) {
    paramTextPaint.setTextScaleX(paramTextPaint.getTextScaleX() * this.mProportion);
  }
  
  public void updateMeasureState(TextPaint paramTextPaint) {
    paramTextPaint.setTextScaleX(paramTextPaint.getTextScaleX() * this.mProportion);
  }
}
