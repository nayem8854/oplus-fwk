package android.text.style;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.ParcelableSpan;

public class EasyEditSpan implements ParcelableSpan {
  public static final String EXTRA_TEXT_CHANGED_TYPE = "android.text.style.EXTRA_TEXT_CHANGED_TYPE";
  
  public static final int TEXT_DELETED = 1;
  
  public static final int TEXT_MODIFIED = 2;
  
  private boolean mDeleteEnabled;
  
  private final PendingIntent mPendingIntent;
  
  public EasyEditSpan() {
    this.mPendingIntent = null;
    this.mDeleteEnabled = true;
  }
  
  public EasyEditSpan(PendingIntent paramPendingIntent) {
    this.mPendingIntent = paramPendingIntent;
    this.mDeleteEnabled = true;
  }
  
  public EasyEditSpan(Parcel paramParcel) {
    this.mPendingIntent = (PendingIntent)paramParcel.readParcelable(null);
    byte b = paramParcel.readByte();
    boolean bool = true;
    if (b != 1)
      bool = false; 
    this.mDeleteEnabled = bool;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    writeToParcelInternal(paramParcel, paramInt);
  }
  
  public void writeToParcelInternal(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable((Parcelable)this.mPendingIntent, 0);
    paramParcel.writeByte((byte)this.mDeleteEnabled);
  }
  
  public int getSpanTypeId() {
    return getSpanTypeIdInternal();
  }
  
  public int getSpanTypeIdInternal() {
    return 22;
  }
  
  public boolean isDeleteEnabled() {
    return this.mDeleteEnabled;
  }
  
  public void setDeleteEnabled(boolean paramBoolean) {
    this.mDeleteEnabled = paramBoolean;
  }
  
  public PendingIntent getPendingIntent() {
    return this.mPendingIntent;
  }
}
