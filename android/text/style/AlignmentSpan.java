package android.text.style;

import android.os.Parcel;
import android.text.Layout;
import android.text.ParcelableSpan;

public interface AlignmentSpan extends ParagraphStyle {
  Layout.Alignment getAlignment();
  
  class Standard implements AlignmentSpan, ParcelableSpan {
    private final Layout.Alignment mAlignment;
    
    public Standard(AlignmentSpan this$0) {
      this.mAlignment = (Layout.Alignment)this$0;
    }
    
    public Standard(AlignmentSpan this$0) {
      this.mAlignment = Layout.Alignment.valueOf(this$0.readString());
    }
    
    public int getSpanTypeId() {
      return getSpanTypeIdInternal();
    }
    
    public int getSpanTypeIdInternal() {
      return 1;
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      writeToParcelInternal(param1Parcel, param1Int);
    }
    
    public void writeToParcelInternal(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeString(this.mAlignment.name());
    }
    
    public Layout.Alignment getAlignment() {
      return this.mAlignment;
    }
  }
}
