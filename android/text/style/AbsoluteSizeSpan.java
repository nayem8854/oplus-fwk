package android.text.style;

import android.os.Parcel;
import android.text.ParcelableSpan;
import android.text.TextPaint;

public class AbsoluteSizeSpan extends MetricAffectingSpan implements ParcelableSpan {
  private final boolean mDip;
  
  private final int mSize;
  
  public AbsoluteSizeSpan(int paramInt) {
    this(paramInt, false);
  }
  
  public AbsoluteSizeSpan(int paramInt, boolean paramBoolean) {
    this.mSize = paramInt;
    this.mDip = paramBoolean;
  }
  
  public AbsoluteSizeSpan(Parcel paramParcel) {
    boolean bool;
    this.mSize = paramParcel.readInt();
    if (paramParcel.readInt() != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mDip = bool;
  }
  
  public int getSpanTypeId() {
    return getSpanTypeIdInternal();
  }
  
  public int getSpanTypeIdInternal() {
    return 16;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    writeToParcelInternal(paramParcel, paramInt);
  }
  
  public void writeToParcelInternal(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mSize);
    paramParcel.writeInt(this.mDip);
  }
  
  public int getSize() {
    return this.mSize;
  }
  
  public boolean getDip() {
    return this.mDip;
  }
  
  public void updateDrawState(TextPaint paramTextPaint) {
    if (this.mDip) {
      paramTextPaint.setTextSize(this.mSize * paramTextPaint.density);
    } else {
      paramTextPaint.setTextSize(this.mSize);
    } 
  }
  
  public void updateMeasureState(TextPaint paramTextPaint) {
    if (this.mDip) {
      paramTextPaint.setTextSize(this.mSize * paramTextPaint.density);
    } else {
      paramTextPaint.setTextSize(this.mSize);
    } 
  }
}
