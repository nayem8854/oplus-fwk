package android.text.style;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.ParcelableSpan;
import android.text.Spanned;
import android.view.View;
import android.view.accessibility.AccessibilityInteractionClient;
import android.view.accessibility.AccessibilityNodeInfo;

public class AccessibilityClickableSpan extends ClickableSpan implements ParcelableSpan {
  private int mWindowId = -1;
  
  private long mSourceNodeId = AccessibilityNodeInfo.UNDEFINED_NODE_ID;
  
  private final int mOriginalClickableSpanId;
  
  private int mConnectionId = -1;
  
  public AccessibilityClickableSpan(int paramInt) {
    this.mOriginalClickableSpanId = paramInt;
  }
  
  public AccessibilityClickableSpan(Parcel paramParcel) {
    this.mOriginalClickableSpanId = paramParcel.readInt();
  }
  
  public int getSpanTypeId() {
    return getSpanTypeIdInternal();
  }
  
  public int getSpanTypeIdInternal() {
    return 25;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    writeToParcelInternal(paramParcel, paramInt);
  }
  
  public void writeToParcelInternal(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mOriginalClickableSpanId);
  }
  
  public ClickableSpan findClickableSpan(CharSequence paramCharSequence) {
    if (!(paramCharSequence instanceof Spanned))
      return null; 
    Spanned spanned = (Spanned)paramCharSequence;
    ClickableSpan[] arrayOfClickableSpan = spanned.<ClickableSpan>getSpans(0, paramCharSequence.length(), ClickableSpan.class);
    for (byte b = 0; b < arrayOfClickableSpan.length; b++) {
      if (arrayOfClickableSpan[b].getId() == this.mOriginalClickableSpanId)
        return arrayOfClickableSpan[b]; 
    } 
    return null;
  }
  
  public void copyConnectionDataFrom(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    this.mConnectionId = paramAccessibilityNodeInfo.getConnectionId();
    this.mWindowId = paramAccessibilityNodeInfo.getWindowId();
    this.mSourceNodeId = paramAccessibilityNodeInfo.getSourceNodeId();
  }
  
  public void onClick(View paramView) {
    Bundle bundle = new Bundle();
    bundle.putParcelable("android.view.accessibility.action.ACTION_ARGUMENT_ACCESSIBLE_CLICKABLE_SPAN", this);
    if (this.mWindowId != -1 && this.mSourceNodeId != AccessibilityNodeInfo.UNDEFINED_NODE_ID && this.mConnectionId != -1) {
      AccessibilityInteractionClient accessibilityInteractionClient = AccessibilityInteractionClient.getInstance();
      accessibilityInteractionClient.performAccessibilityAction(this.mConnectionId, this.mWindowId, this.mSourceNodeId, 16908669, bundle);
      return;
    } 
    throw new RuntimeException("ClickableSpan for accessibility service not properly initialized");
  }
  
  public static final Parcelable.Creator<AccessibilityClickableSpan> CREATOR = (Parcelable.Creator<AccessibilityClickableSpan>)new Object();
}
