package android.text.style;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.LeakyTypefaceStorage;
import android.graphics.Typeface;
import android.os.LocaleList;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.ParcelableSpan;
import android.text.TextPaint;
import com.android.internal.R;

public class TextAppearanceSpan extends MetricAffectingSpan implements ParcelableSpan {
  private final boolean mElegantTextHeight;
  
  private final String mFamilyName;
  
  private final String mFontFeatureSettings;
  
  private final String mFontVariationSettings;
  
  private final boolean mHasElegantTextHeight;
  
  private final boolean mHasLetterSpacing;
  
  private final float mLetterSpacing;
  
  private final int mShadowColor;
  
  private final float mShadowDx;
  
  private final float mShadowDy;
  
  private final float mShadowRadius;
  
  private final int mStyle;
  
  private final ColorStateList mTextColor;
  
  private final ColorStateList mTextColorLink;
  
  private final int mTextFontWeight;
  
  private final LocaleList mTextLocales;
  
  private final int mTextSize;
  
  private final Typeface mTypeface;
  
  public TextAppearanceSpan(Context paramContext, int paramInt) {
    this(paramContext, paramInt, -1);
  }
  
  public TextAppearanceSpan(Context paramContext, int paramInt1, int paramInt2) {
    int[] arrayOfInt = R.styleable.TextAppearance;
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramInt1, arrayOfInt);
    ColorStateList colorStateList = typedArray.getColorStateList(3);
    this.mTextColorLink = typedArray.getColorStateList(6);
    this.mTextSize = typedArray.getDimensionPixelSize(0, -1);
    this.mStyle = typedArray.getInt(2, 0);
    if (!paramContext.isRestricted() && paramContext.canLoadUnsafeResources()) {
      this.mTypeface = typedArray.getFont(12);
    } else {
      this.mTypeface = null;
    } 
    if (this.mTypeface != null) {
      this.mFamilyName = null;
    } else {
      String str1 = typedArray.getString(12);
      if (str1 != null) {
        this.mFamilyName = str1;
      } else {
        paramInt1 = typedArray.getInt(1, 0);
        if (paramInt1 != 1) {
          if (paramInt1 != 2) {
            if (paramInt1 != 3) {
              this.mFamilyName = null;
            } else {
              this.mFamilyName = "monospace";
            } 
          } else {
            this.mFamilyName = "serif";
          } 
        } else {
          this.mFamilyName = "sans";
        } 
      } 
    } 
    this.mTextFontWeight = typedArray.getInt(18, -1);
    String str = typedArray.getString(19);
    if (str != null) {
      LocaleList localeList = LocaleList.forLanguageTags(str);
      if (!localeList.isEmpty()) {
        this.mTextLocales = localeList;
      } else {
        this.mTextLocales = null;
      } 
    } else {
      this.mTextLocales = null;
    } 
    this.mShadowRadius = typedArray.getFloat(10, 0.0F);
    this.mShadowDx = typedArray.getFloat(8, 0.0F);
    this.mShadowDy = typedArray.getFloat(9, 0.0F);
    this.mShadowColor = typedArray.getInt(7, 0);
    this.mHasElegantTextHeight = typedArray.hasValue(13);
    this.mElegantTextHeight = typedArray.getBoolean(13, false);
    this.mHasLetterSpacing = typedArray.hasValue(14);
    this.mLetterSpacing = typedArray.getFloat(14, 0.0F);
    this.mFontFeatureSettings = typedArray.getString(15);
    this.mFontVariationSettings = typedArray.getString(16);
    typedArray.recycle();
    if (paramInt2 >= 0) {
      TypedArray typedArray1 = paramContext.obtainStyledAttributes(16973829, R.styleable.Theme);
      colorStateList = typedArray1.getColorStateList(paramInt2);
      typedArray1.recycle();
    } 
    this.mTextColor = colorStateList;
  }
  
  public TextAppearanceSpan(String paramString, int paramInt1, int paramInt2, ColorStateList paramColorStateList1, ColorStateList paramColorStateList2) {
    this.mFamilyName = paramString;
    this.mStyle = paramInt1;
    this.mTextSize = paramInt2;
    this.mTextColor = paramColorStateList1;
    this.mTextColorLink = paramColorStateList2;
    this.mTypeface = null;
    this.mTextFontWeight = -1;
    this.mTextLocales = null;
    this.mShadowRadius = 0.0F;
    this.mShadowDx = 0.0F;
    this.mShadowDy = 0.0F;
    this.mShadowColor = 0;
    this.mHasElegantTextHeight = false;
    this.mElegantTextHeight = false;
    this.mHasLetterSpacing = false;
    this.mLetterSpacing = 0.0F;
    this.mFontFeatureSettings = null;
    this.mFontVariationSettings = null;
  }
  
  public TextAppearanceSpan(Parcel paramParcel) {
    this.mFamilyName = paramParcel.readString();
    this.mStyle = paramParcel.readInt();
    this.mTextSize = paramParcel.readInt();
    if (paramParcel.readInt() != 0) {
      this.mTextColor = (ColorStateList)ColorStateList.CREATOR.createFromParcel(paramParcel);
    } else {
      this.mTextColor = null;
    } 
    if (paramParcel.readInt() != 0) {
      this.mTextColorLink = (ColorStateList)ColorStateList.CREATOR.createFromParcel(paramParcel);
    } else {
      this.mTextColorLink = null;
    } 
    this.mTypeface = LeakyTypefaceStorage.readTypefaceFromParcel(paramParcel);
    this.mTextFontWeight = paramParcel.readInt();
    this.mTextLocales = (LocaleList)paramParcel.readParcelable(LocaleList.class.getClassLoader());
    this.mShadowRadius = paramParcel.readFloat();
    this.mShadowDx = paramParcel.readFloat();
    this.mShadowDy = paramParcel.readFloat();
    this.mShadowColor = paramParcel.readInt();
    this.mHasElegantTextHeight = paramParcel.readBoolean();
    this.mElegantTextHeight = paramParcel.readBoolean();
    this.mHasLetterSpacing = paramParcel.readBoolean();
    this.mLetterSpacing = paramParcel.readFloat();
    this.mFontFeatureSettings = paramParcel.readString();
    this.mFontVariationSettings = paramParcel.readString();
  }
  
  public int getSpanTypeId() {
    return getSpanTypeIdInternal();
  }
  
  public int getSpanTypeIdInternal() {
    return 17;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    writeToParcelInternal(paramParcel, paramInt);
  }
  
  public void writeToParcelInternal(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mFamilyName);
    paramParcel.writeInt(this.mStyle);
    paramParcel.writeInt(this.mTextSize);
    if (this.mTextColor != null) {
      paramParcel.writeInt(1);
      this.mTextColor.writeToParcel(paramParcel, paramInt);
    } else {
      paramParcel.writeInt(0);
    } 
    if (this.mTextColorLink != null) {
      paramParcel.writeInt(1);
      this.mTextColorLink.writeToParcel(paramParcel, paramInt);
    } else {
      paramParcel.writeInt(0);
    } 
    LeakyTypefaceStorage.writeTypefaceToParcel(this.mTypeface, paramParcel);
    paramParcel.writeInt(this.mTextFontWeight);
    paramParcel.writeParcelable((Parcelable)this.mTextLocales, paramInt);
    paramParcel.writeFloat(this.mShadowRadius);
    paramParcel.writeFloat(this.mShadowDx);
    paramParcel.writeFloat(this.mShadowDy);
    paramParcel.writeInt(this.mShadowColor);
    paramParcel.writeBoolean(this.mHasElegantTextHeight);
    paramParcel.writeBoolean(this.mElegantTextHeight);
    paramParcel.writeBoolean(this.mHasLetterSpacing);
    paramParcel.writeFloat(this.mLetterSpacing);
    paramParcel.writeString(this.mFontFeatureSettings);
    paramParcel.writeString(this.mFontVariationSettings);
  }
  
  public String getFamily() {
    return this.mFamilyName;
  }
  
  public ColorStateList getTextColor() {
    return this.mTextColor;
  }
  
  public ColorStateList getLinkTextColor() {
    return this.mTextColorLink;
  }
  
  public int getTextSize() {
    return this.mTextSize;
  }
  
  public int getTextStyle() {
    return this.mStyle;
  }
  
  public int getTextFontWeight() {
    return this.mTextFontWeight;
  }
  
  public LocaleList getTextLocales() {
    return this.mTextLocales;
  }
  
  public Typeface getTypeface() {
    return this.mTypeface;
  }
  
  public int getShadowColor() {
    return this.mShadowColor;
  }
  
  public float getShadowDx() {
    return this.mShadowDx;
  }
  
  public float getShadowDy() {
    return this.mShadowDy;
  }
  
  public float getShadowRadius() {
    return this.mShadowRadius;
  }
  
  public String getFontFeatureSettings() {
    return this.mFontFeatureSettings;
  }
  
  public String getFontVariationSettings() {
    return this.mFontVariationSettings;
  }
  
  public boolean isElegantTextHeight() {
    return this.mElegantTextHeight;
  }
  
  public void updateDrawState(TextPaint paramTextPaint) {
    updateMeasureState(paramTextPaint);
    ColorStateList colorStateList = this.mTextColor;
    if (colorStateList != null)
      paramTextPaint.setColor(colorStateList.getColorForState(paramTextPaint.drawableState, 0)); 
    colorStateList = this.mTextColorLink;
    if (colorStateList != null)
      paramTextPaint.linkColor = colorStateList.getColorForState(paramTextPaint.drawableState, 0); 
    int i = this.mShadowColor;
    if (i != 0)
      paramTextPaint.setShadowLayer(this.mShadowRadius, this.mShadowDx, this.mShadowDy, i); 
  }
  
  public void updateMeasureState(TextPaint paramTextPaint) {
    int i = 0, j = 0;
    Typeface typeface = this.mTypeface;
    if (typeface != null) {
      i = this.mStyle;
      typeface = Typeface.create(typeface, i);
    } else if (this.mFamilyName != null || this.mStyle != 0) {
      typeface = paramTextPaint.getTypeface();
      i = j;
      if (typeface != null)
        i = typeface.getStyle(); 
      i |= this.mStyle;
      String str1 = this.mFamilyName;
      if (str1 != null) {
        typeface = Typeface.create(str1, i);
      } else if (typeface == null) {
        typeface = Typeface.defaultFromStyle(i);
      } else {
        typeface = Typeface.create(typeface, i);
      } 
    } else {
      typeface = null;
    } 
    if (typeface != null) {
      j = this.mTextFontWeight;
      if (j >= 0) {
        boolean bool;
        j = Math.min(1000, j);
        if ((i & 0x2) != 0) {
          bool = true;
        } else {
          bool = false;
        } 
        typeface = paramTextPaint.setTypeface(Typeface.create(typeface, j, bool));
      } 
      i = (typeface.getStyle() ^ 0xFFFFFFFF) & i;
      if ((i & 0x1) != 0)
        paramTextPaint.setFakeBoldText(true); 
      if ((i & 0x2) != 0)
        paramTextPaint.setTextSkewX(-0.25F); 
      paramTextPaint.setTypeface(typeface);
    } 
    i = this.mTextSize;
    if (i > 0)
      paramTextPaint.setTextSize(i); 
    LocaleList localeList = this.mTextLocales;
    if (localeList != null)
      paramTextPaint.setTextLocales(localeList); 
    if (this.mHasElegantTextHeight)
      paramTextPaint.setElegantTextHeight(this.mElegantTextHeight); 
    if (this.mHasLetterSpacing)
      paramTextPaint.setLetterSpacing(this.mLetterSpacing); 
    String str = this.mFontFeatureSettings;
    if (str != null)
      paramTextPaint.setFontFeatureSettings(str); 
    str = this.mFontVariationSettings;
    if (str != null)
      paramTextPaint.setFontVariationSettings(str); 
  }
}
