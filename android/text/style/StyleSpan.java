package android.text.style;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Parcel;
import android.text.ParcelableSpan;
import android.text.TextPaint;

public class StyleSpan extends MetricAffectingSpan implements ParcelableSpan {
  private final int mStyle;
  
  public StyleSpan(int paramInt) {
    this.mStyle = paramInt;
  }
  
  public StyleSpan(Parcel paramParcel) {
    this.mStyle = paramParcel.readInt();
  }
  
  public int getSpanTypeId() {
    return getSpanTypeIdInternal();
  }
  
  public int getSpanTypeIdInternal() {
    return 7;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    writeToParcelInternal(paramParcel, paramInt);
  }
  
  public void writeToParcelInternal(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mStyle);
  }
  
  public int getStyle() {
    return this.mStyle;
  }
  
  public void updateDrawState(TextPaint paramTextPaint) {
    apply(paramTextPaint, this.mStyle);
  }
  
  public void updateMeasureState(TextPaint paramTextPaint) {
    apply(paramTextPaint, this.mStyle);
  }
  
  private static void apply(Paint paramPaint, int paramInt) {
    int i;
    Typeface typeface = paramPaint.getTypeface();
    if (typeface == null) {
      i = 0;
    } else {
      i = typeface.getStyle();
    } 
    paramInt = i | paramInt;
    if (typeface == null) {
      typeface = Typeface.defaultFromStyle(paramInt);
    } else {
      typeface = Typeface.create(typeface, paramInt);
    } 
    paramInt = (typeface.getStyle() ^ 0xFFFFFFFF) & paramInt;
    if ((paramInt & 0x1) != 0)
      paramPaint.setFakeBoldText(true); 
    if ((paramInt & 0x2) != 0)
      paramPaint.setTextSkewX(-0.25F); 
    paramPaint.setTypeface(typeface);
  }
}
