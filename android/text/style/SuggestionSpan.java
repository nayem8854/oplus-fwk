package android.text.style;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.text.ParcelableSpan;
import android.text.TextPaint;
import android.util.Log;
import com.android.internal.R;
import java.util.Arrays;
import java.util.Locale;

public class SuggestionSpan extends CharacterStyle implements ParcelableSpan {
  @Deprecated
  public static final String ACTION_SUGGESTION_PICKED = "android.text.style.SUGGESTION_PICKED";
  
  public SuggestionSpan(Context paramContext, String[] paramArrayOfString, int paramInt) {
    this(paramContext, null, paramArrayOfString, paramInt, null);
  }
  
  public SuggestionSpan(Locale paramLocale, String[] paramArrayOfString, int paramInt) {
    this(null, paramLocale, paramArrayOfString, paramInt, null);
  }
  
  public SuggestionSpan(Context paramContext, Locale paramLocale, String[] paramArrayOfString, int paramInt, Class<?> paramClass) {
    String str1, str2;
    int i = Math.min(5, paramArrayOfString.length);
    this.mSuggestions = Arrays.<String>copyOf(paramArrayOfString, i);
    this.mFlags = paramInt;
    if (paramLocale == null)
      if (paramContext != null) {
        paramLocale = (paramContext.getResources().getConfiguration()).locale;
      } else {
        Log.e("SuggestionSpan", "No locale or context specified in SuggestionSpan constructor");
        paramLocale = null;
      }  
    String str3 = "";
    if (paramLocale == null) {
      str2 = "";
    } else {
      str2 = paramLocale.toString();
    } 
    this.mLocaleStringForCompatibility = str2;
    if (paramLocale == null) {
      str1 = str3;
    } else {
      str1 = str1.toLanguageTag();
    } 
    this.mLanguageTag = str1;
    this.mHashCode = hashCodeInternal(this.mSuggestions, str1, this.mLocaleStringForCompatibility);
    initStyle(paramContext);
  }
  
  private void initStyle(Context paramContext) {
    if (paramContext == null) {
      this.mMisspelledUnderlineThickness = 0.0F;
      this.mEasyCorrectUnderlineThickness = 0.0F;
      this.mAutoCorrectionUnderlineThickness = 0.0F;
      this.mMisspelledUnderlineColor = -16777216;
      this.mEasyCorrectUnderlineColor = -16777216;
      this.mAutoCorrectionUnderlineColor = -16777216;
      return;
    } 
    TypedArray typedArray2 = paramContext.obtainStyledAttributes(null, R.styleable.SuggestionSpan, 17957090, 0);
    this.mMisspelledUnderlineThickness = typedArray2.getDimension(1, 0.0F);
    this.mMisspelledUnderlineColor = typedArray2.getColor(0, -16777216);
    typedArray2 = paramContext.obtainStyledAttributes(null, R.styleable.SuggestionSpan, 17957089, 0);
    this.mEasyCorrectUnderlineThickness = typedArray2.getDimension(1, 0.0F);
    this.mEasyCorrectUnderlineColor = typedArray2.getColor(0, -16777216);
    TypedArray typedArray1 = paramContext.obtainStyledAttributes(null, R.styleable.SuggestionSpan, 17957088, 0);
    this.mAutoCorrectionUnderlineThickness = typedArray1.getDimension(1, 0.0F);
    this.mAutoCorrectionUnderlineColor = typedArray1.getColor(0, -16777216);
  }
  
  public SuggestionSpan(Parcel paramParcel) {
    this.mSuggestions = paramParcel.readStringArray();
    this.mFlags = paramParcel.readInt();
    this.mLocaleStringForCompatibility = paramParcel.readString();
    this.mLanguageTag = paramParcel.readString();
    this.mHashCode = paramParcel.readInt();
    this.mEasyCorrectUnderlineColor = paramParcel.readInt();
    this.mEasyCorrectUnderlineThickness = paramParcel.readFloat();
    this.mMisspelledUnderlineColor = paramParcel.readInt();
    this.mMisspelledUnderlineThickness = paramParcel.readFloat();
    this.mAutoCorrectionUnderlineColor = paramParcel.readInt();
    this.mAutoCorrectionUnderlineThickness = paramParcel.readFloat();
  }
  
  public String[] getSuggestions() {
    return this.mSuggestions;
  }
  
  @Deprecated
  public String getLocale() {
    return this.mLocaleStringForCompatibility;
  }
  
  public Locale getLocaleObject() {
    Locale locale;
    if (this.mLanguageTag.isEmpty()) {
      locale = null;
    } else {
      locale = Locale.forLanguageTag(this.mLanguageTag);
    } 
    return locale;
  }
  
  @Deprecated
  public String getNotificationTargetClassName() {
    return null;
  }
  
  public int getFlags() {
    return this.mFlags;
  }
  
  public void setFlags(int paramInt) {
    this.mFlags = paramInt;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    writeToParcelInternal(paramParcel, paramInt);
  }
  
  public void writeToParcelInternal(Parcel paramParcel, int paramInt) {
    paramParcel.writeStringArray(this.mSuggestions);
    paramParcel.writeInt(this.mFlags);
    paramParcel.writeString(this.mLocaleStringForCompatibility);
    paramParcel.writeString(this.mLanguageTag);
    paramParcel.writeInt(this.mHashCode);
    paramParcel.writeInt(this.mEasyCorrectUnderlineColor);
    paramParcel.writeFloat(this.mEasyCorrectUnderlineThickness);
    paramParcel.writeInt(this.mMisspelledUnderlineColor);
    paramParcel.writeFloat(this.mMisspelledUnderlineThickness);
    paramParcel.writeInt(this.mAutoCorrectionUnderlineColor);
    paramParcel.writeFloat(this.mAutoCorrectionUnderlineThickness);
  }
  
  public int getSpanTypeId() {
    return getSpanTypeIdInternal();
  }
  
  public int getSpanTypeIdInternal() {
    return 19;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof SuggestionSpan;
    boolean bool1 = false;
    if (bool) {
      if (((SuggestionSpan)paramObject).hashCode() == this.mHashCode)
        bool1 = true; 
      return bool1;
    } 
    return false;
  }
  
  public int hashCode() {
    return this.mHashCode;
  }
  
  private static int hashCodeInternal(String[] paramArrayOfString, String paramString1, String paramString2) {
    return Arrays.hashCode(new Object[] { Long.valueOf(SystemClock.uptimeMillis()), paramArrayOfString, paramString1, paramString2 });
  }
  
  public static final Parcelable.Creator<SuggestionSpan> CREATOR = (Parcelable.Creator<SuggestionSpan>)new Object();
  
  public static final int FLAG_AUTO_CORRECTION = 4;
  
  public static final int FLAG_EASY_CORRECT = 1;
  
  public static final int FLAG_MISSPELLED = 2;
  
  public static final int SUGGESTIONS_MAX_SIZE = 5;
  
  @Deprecated
  public static final String SUGGESTION_SPAN_PICKED_AFTER = "after";
  
  @Deprecated
  public static final String SUGGESTION_SPAN_PICKED_BEFORE = "before";
  
  @Deprecated
  public static final String SUGGESTION_SPAN_PICKED_HASHCODE = "hashcode";
  
  private static final String TAG = "SuggestionSpan";
  
  private int mAutoCorrectionUnderlineColor;
  
  private float mAutoCorrectionUnderlineThickness;
  
  private int mEasyCorrectUnderlineColor;
  
  private float mEasyCorrectUnderlineThickness;
  
  private int mFlags;
  
  private final int mHashCode;
  
  private final String mLanguageTag;
  
  private final String mLocaleStringForCompatibility;
  
  private int mMisspelledUnderlineColor;
  
  private float mMisspelledUnderlineThickness;
  
  private final String[] mSuggestions;
  
  public void updateDrawState(TextPaint paramTextPaint) {
    boolean bool2;
    int i = this.mFlags;
    boolean bool1 = false;
    if ((i & 0x2) != 0) {
      i = 1;
    } else {
      i = 0;
    } 
    if ((this.mFlags & 0x1) != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    if ((this.mFlags & 0x4) != 0)
      bool1 = true; 
    if (bool2) {
      if (i == 0) {
        paramTextPaint.setUnderlineText(this.mEasyCorrectUnderlineColor, this.mEasyCorrectUnderlineThickness);
      } else if (paramTextPaint.underlineColor == 0) {
        paramTextPaint.setUnderlineText(this.mMisspelledUnderlineColor, this.mMisspelledUnderlineThickness);
      } 
    } else if (bool1) {
      paramTextPaint.setUnderlineText(this.mAutoCorrectionUnderlineColor, this.mAutoCorrectionUnderlineThickness);
    } 
  }
  
  public int getUnderlineColor() {
    boolean bool2;
    int i = this.mFlags;
    boolean bool1 = true;
    if ((i & 0x2) != 0) {
      i = 1;
    } else {
      i = 0;
    } 
    if ((this.mFlags & 0x1) != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    if ((this.mFlags & 0x4) == 0)
      bool1 = false; 
    if (bool2) {
      if (i == 0)
        return this.mEasyCorrectUnderlineColor; 
      return this.mMisspelledUnderlineColor;
    } 
    if (bool1)
      return this.mAutoCorrectionUnderlineColor; 
    return 0;
  }
  
  @Deprecated
  public void notifySelection(Context paramContext, String paramString, int paramInt) {
    Log.w("SuggestionSpan", "notifySelection() is deprecated.  Does nothing.");
  }
}
