package android.text.style;

public interface TabStopSpan extends ParagraphStyle {
  int getTabStop();
  
  class Standard implements TabStopSpan {
    private int mTabOffset;
    
    public Standard(TabStopSpan this$0) {
      this.mTabOffset = this$0;
    }
    
    public int getTabStop() {
      return this.mTabOffset;
    }
  }
}
