package android.text.style;

import android.os.Parcel;
import android.text.ParcelableSpan;

public class SpellCheckSpan implements ParcelableSpan {
  private boolean mSpellCheckInProgress;
  
  public SpellCheckSpan() {
    this.mSpellCheckInProgress = false;
  }
  
  public SpellCheckSpan(Parcel paramParcel) {
    boolean bool;
    if (paramParcel.readInt() != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mSpellCheckInProgress = bool;
  }
  
  public void setSpellCheckInProgress(boolean paramBoolean) {
    this.mSpellCheckInProgress = paramBoolean;
  }
  
  public boolean isSpellCheckInProgress() {
    return this.mSpellCheckInProgress;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    writeToParcelInternal(paramParcel, paramInt);
  }
  
  public void writeToParcelInternal(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mSpellCheckInProgress);
  }
  
  public int getSpanTypeId() {
    return getSpanTypeIdInternal();
  }
  
  public int getSpanTypeIdInternal() {
    return 20;
  }
}
