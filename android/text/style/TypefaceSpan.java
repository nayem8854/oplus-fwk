package android.text.style;

import android.graphics.LeakyTypefaceStorage;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Parcel;
import android.text.ParcelableSpan;
import android.text.TextPaint;

public class TypefaceSpan extends MetricAffectingSpan implements ParcelableSpan {
  private final String mFamily;
  
  private final Typeface mTypeface;
  
  public TypefaceSpan(String paramString) {
    this(paramString, null);
  }
  
  public TypefaceSpan(Typeface paramTypeface) {
    this(null, paramTypeface);
  }
  
  public TypefaceSpan(Parcel paramParcel) {
    this.mFamily = paramParcel.readString();
    this.mTypeface = LeakyTypefaceStorage.readTypefaceFromParcel(paramParcel);
  }
  
  private TypefaceSpan(String paramString, Typeface paramTypeface) {
    this.mFamily = paramString;
    this.mTypeface = paramTypeface;
  }
  
  public int getSpanTypeId() {
    return getSpanTypeIdInternal();
  }
  
  public int getSpanTypeIdInternal() {
    return 13;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    writeToParcelInternal(paramParcel, paramInt);
  }
  
  public void writeToParcelInternal(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mFamily);
    LeakyTypefaceStorage.writeTypefaceToParcel(this.mTypeface, paramParcel);
  }
  
  public String getFamily() {
    return this.mFamily;
  }
  
  public Typeface getTypeface() {
    return this.mTypeface;
  }
  
  public void updateDrawState(TextPaint paramTextPaint) {
    updateTypeface(paramTextPaint);
  }
  
  public void updateMeasureState(TextPaint paramTextPaint) {
    updateTypeface(paramTextPaint);
  }
  
  private void updateTypeface(Paint paramPaint) {
    Typeface typeface = this.mTypeface;
    if (typeface != null) {
      paramPaint.setTypeface(typeface);
    } else {
      String str = this.mFamily;
      if (str != null)
        applyFontFamily(paramPaint, str); 
    } 
  }
  
  private void applyFontFamily(Paint paramPaint, String paramString) {
    Typeface typeface2 = paramPaint.getTypeface();
    if (typeface2 == null) {
      i = 0;
    } else {
      i = typeface2.getStyle();
    } 
    Typeface typeface1 = Typeface.create(paramString, i);
    int i = (typeface1.getStyle() ^ 0xFFFFFFFF) & i;
    if ((i & 0x1) != 0)
      paramPaint.setFakeBoldText(true); 
    if ((i & 0x2) != 0)
      paramPaint.setTextSkewX(-0.25F); 
    paramPaint.setTypeface(typeface1);
  }
}
