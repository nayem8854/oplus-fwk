package android.text.style;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.text.Layout;
import android.text.Spanned;

public class DrawableMarginSpan implements LeadingMarginSpan, LineHeightSpan {
  private static final int STANDARD_PAD_WIDTH = 0;
  
  private final Drawable mDrawable;
  
  private final int mPad;
  
  public DrawableMarginSpan(Drawable paramDrawable) {
    this(paramDrawable, 0);
  }
  
  public DrawableMarginSpan(Drawable paramDrawable, int paramInt) {
    this.mDrawable = paramDrawable;
    this.mPad = paramInt;
  }
  
  public int getLeadingMargin(boolean paramBoolean) {
    return this.mDrawable.getIntrinsicWidth() + this.mPad;
  }
  
  public void drawLeadingMargin(Canvas paramCanvas, Paint paramPaint, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, CharSequence paramCharSequence, int paramInt6, int paramInt7, boolean paramBoolean, Layout paramLayout) {
    paramInt2 = ((Spanned)paramCharSequence).getSpanStart(this);
    paramInt2 = paramLayout.getLineTop(paramLayout.getLineForOffset(paramInt2));
    paramInt3 = this.mDrawable.getIntrinsicWidth();
    paramInt4 = this.mDrawable.getIntrinsicHeight();
    this.mDrawable.setBounds(paramInt1, paramInt2, paramInt1 + paramInt3, paramInt2 + paramInt4);
    this.mDrawable.draw(paramCanvas);
  }
  
  public void chooseHeight(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3, int paramInt4, Paint.FontMetricsInt paramFontMetricsInt) {
    if (paramInt2 == ((Spanned)paramCharSequence).getSpanEnd(this)) {
      paramInt2 = this.mDrawable.getIntrinsicHeight();
      paramInt1 = paramInt2 - paramFontMetricsInt.descent + paramInt4 - paramFontMetricsInt.ascent - paramInt3;
      if (paramInt1 > 0)
        paramFontMetricsInt.descent += paramInt1; 
      paramInt1 = paramInt2 - paramFontMetricsInt.bottom + paramInt4 - paramFontMetricsInt.top - paramInt3;
      if (paramInt1 > 0)
        paramFontMetricsInt.bottom += paramInt1; 
    } 
  }
}
