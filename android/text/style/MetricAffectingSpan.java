package android.text.style;

import android.text.TextPaint;

public abstract class MetricAffectingSpan extends CharacterStyle implements UpdateLayout {
  public MetricAffectingSpan getUnderlying() {
    return this;
  }
  
  public abstract void updateMeasureState(TextPaint paramTextPaint);
  
  class Passthrough extends MetricAffectingSpan {
    private MetricAffectingSpan mStyle;
    
    Passthrough(MetricAffectingSpan this$0) {
      this.mStyle = this$0;
    }
    
    public void updateDrawState(TextPaint param1TextPaint) {
      this.mStyle.updateDrawState(param1TextPaint);
    }
    
    public void updateMeasureState(TextPaint param1TextPaint) {
      this.mStyle.updateMeasureState(param1TextPaint);
    }
    
    public MetricAffectingSpan getUnderlying() {
      return this.mStyle.getUnderlying();
    }
  }
}
