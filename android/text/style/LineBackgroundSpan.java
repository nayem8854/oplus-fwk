package android.text.style;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Parcel;
import android.text.ParcelableSpan;

public interface LineBackgroundSpan extends ParagraphStyle {
  void drawBackground(Canvas paramCanvas, Paint paramPaint, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, CharSequence paramCharSequence, int paramInt6, int paramInt7, int paramInt8);
  
  class Standard implements LineBackgroundSpan, ParcelableSpan {
    private final int mColor;
    
    public Standard(LineBackgroundSpan this$0) {
      this.mColor = this$0;
    }
    
    public Standard(LineBackgroundSpan this$0) {
      this.mColor = this$0.readInt();
    }
    
    public int getSpanTypeId() {
      return getSpanTypeIdInternal();
    }
    
    public int getSpanTypeIdInternal() {
      return 27;
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      writeToParcelInternal(param1Parcel, param1Int);
    }
    
    public void writeToParcelInternal(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.mColor);
    }
    
    public final int getColor() {
      return this.mColor;
    }
    
    public void drawBackground(Canvas param1Canvas, Paint param1Paint, int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5, CharSequence param1CharSequence, int param1Int6, int param1Int7, int param1Int8) {
      param1Int4 = param1Paint.getColor();
      param1Paint.setColor(this.mColor);
      param1Canvas.drawRect(param1Int1, param1Int3, param1Int2, param1Int5, param1Paint);
      param1Paint.setColor(param1Int4);
    }
  }
}
