package android.text.style;

import android.os.Parcel;
import android.os.PersistableBundle;
import android.text.ParcelableSpan;
import java.text.NumberFormat;
import java.util.Locale;

public class TtsSpan implements ParcelableSpan {
  public static final String ANIMACY_ANIMATE = "android.animate";
  
  public static final String ANIMACY_INANIMATE = "android.inanimate";
  
  public static final String ARG_ANIMACY = "android.arg.animacy";
  
  public static final String ARG_CASE = "android.arg.case";
  
  public static final String ARG_COUNTRY_CODE = "android.arg.country_code";
  
  public static final String ARG_CURRENCY = "android.arg.money";
  
  public static final String ARG_DAY = "android.arg.day";
  
  public static final String ARG_DENOMINATOR = "android.arg.denominator";
  
  public static final String ARG_DIGITS = "android.arg.digits";
  
  public static final String ARG_DOMAIN = "android.arg.domain";
  
  public static final String ARG_EXTENSION = "android.arg.extension";
  
  public static final String ARG_FRACTIONAL_PART = "android.arg.fractional_part";
  
  public static final String ARG_FRAGMENT_ID = "android.arg.fragment_id";
  
  public static final String ARG_GENDER = "android.arg.gender";
  
  public static final String ARG_HOURS = "android.arg.hours";
  
  public static final String ARG_INTEGER_PART = "android.arg.integer_part";
  
  public static final String ARG_MINUTES = "android.arg.minutes";
  
  public static final String ARG_MONTH = "android.arg.month";
  
  public static final String ARG_MULTIPLICITY = "android.arg.multiplicity";
  
  public static final String ARG_NUMBER = "android.arg.number";
  
  public static final String ARG_NUMBER_PARTS = "android.arg.number_parts";
  
  public static final String ARG_NUMERATOR = "android.arg.numerator";
  
  public static final String ARG_PASSWORD = "android.arg.password";
  
  public static final String ARG_PATH = "android.arg.path";
  
  public static final String ARG_PORT = "android.arg.port";
  
  public static final String ARG_PROTOCOL = "android.arg.protocol";
  
  public static final String ARG_QUANTITY = "android.arg.quantity";
  
  public static final String ARG_QUERY_STRING = "android.arg.query_string";
  
  public static final String ARG_TEXT = "android.arg.text";
  
  public static final String ARG_UNIT = "android.arg.unit";
  
  public static final String ARG_USERNAME = "android.arg.username";
  
  public static final String ARG_VERBATIM = "android.arg.verbatim";
  
  public static final String ARG_WEEKDAY = "android.arg.weekday";
  
  public static final String ARG_YEAR = "android.arg.year";
  
  public static final String CASE_ABLATIVE = "android.ablative";
  
  public static final String CASE_ACCUSATIVE = "android.accusative";
  
  public static final String CASE_DATIVE = "android.dative";
  
  public static final String CASE_GENITIVE = "android.genitive";
  
  public static final String CASE_INSTRUMENTAL = "android.instrumental";
  
  public static final String CASE_LOCATIVE = "android.locative";
  
  public static final String CASE_NOMINATIVE = "android.nominative";
  
  public static final String CASE_VOCATIVE = "android.vocative";
  
  public static final String GENDER_FEMALE = "android.female";
  
  public static final String GENDER_MALE = "android.male";
  
  public static final String GENDER_NEUTRAL = "android.neutral";
  
  public static final int MONTH_APRIL = 3;
  
  public static final int MONTH_AUGUST = 7;
  
  public static final int MONTH_DECEMBER = 11;
  
  public static final int MONTH_FEBRUARY = 1;
  
  public static final int MONTH_JANUARY = 0;
  
  public static final int MONTH_JULY = 6;
  
  public static final int MONTH_JUNE = 5;
  
  public static final int MONTH_MARCH = 2;
  
  public static final int MONTH_MAY = 4;
  
  public static final int MONTH_NOVEMBER = 10;
  
  public static final int MONTH_OCTOBER = 9;
  
  public static final int MONTH_SEPTEMBER = 8;
  
  public static final String MULTIPLICITY_DUAL = "android.dual";
  
  public static final String MULTIPLICITY_PLURAL = "android.plural";
  
  public static final String MULTIPLICITY_SINGLE = "android.single";
  
  public static final String TYPE_CARDINAL = "android.type.cardinal";
  
  public static final String TYPE_DATE = "android.type.date";
  
  public static final String TYPE_DECIMAL = "android.type.decimal";
  
  public static final String TYPE_DIGITS = "android.type.digits";
  
  public static final String TYPE_ELECTRONIC = "android.type.electronic";
  
  public static final String TYPE_FRACTION = "android.type.fraction";
  
  public static final String TYPE_MEASURE = "android.type.measure";
  
  public static final String TYPE_MONEY = "android.type.money";
  
  public static final String TYPE_ORDINAL = "android.type.ordinal";
  
  public static final String TYPE_TELEPHONE = "android.type.telephone";
  
  public static final String TYPE_TEXT = "android.type.text";
  
  public static final String TYPE_TIME = "android.type.time";
  
  public static final String TYPE_VERBATIM = "android.type.verbatim";
  
  public static final int WEEKDAY_FRIDAY = 6;
  
  public static final int WEEKDAY_MONDAY = 2;
  
  public static final int WEEKDAY_SATURDAY = 7;
  
  public static final int WEEKDAY_SUNDAY = 1;
  
  public static final int WEEKDAY_THURSDAY = 5;
  
  public static final int WEEKDAY_TUESDAY = 3;
  
  public static final int WEEKDAY_WEDNESDAY = 4;
  
  private final PersistableBundle mArgs;
  
  private final String mType;
  
  public TtsSpan(String paramString, PersistableBundle paramPersistableBundle) {
    this.mType = paramString;
    this.mArgs = paramPersistableBundle;
  }
  
  public TtsSpan(Parcel paramParcel) {
    this.mType = paramParcel.readString();
    this.mArgs = paramParcel.readPersistableBundle();
  }
  
  public String getType() {
    return this.mType;
  }
  
  public PersistableBundle getArgs() {
    return this.mArgs;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    writeToParcelInternal(paramParcel, paramInt);
  }
  
  public void writeToParcelInternal(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mType);
    paramParcel.writePersistableBundle(this.mArgs);
  }
  
  public int getSpanTypeId() {
    return getSpanTypeIdInternal();
  }
  
  public int getSpanTypeIdInternal() {
    return 24;
  }
  
  class Builder<C extends Builder<?>> {
    private PersistableBundle mArgs = new PersistableBundle();
    
    private final String mType;
    
    public Builder(TtsSpan this$0) {
      this.mType = (String)this$0;
    }
    
    public TtsSpan build() {
      return new TtsSpan(this.mType, this.mArgs);
    }
    
    public C setStringArgument(String param1String1, String param1String2) {
      this.mArgs.putString(param1String1, param1String2);
      return (C)this;
    }
    
    public C setIntArgument(String param1String, int param1Int) {
      this.mArgs.putInt(param1String, param1Int);
      return (C)this;
    }
    
    public C setLongArgument(String param1String, long param1Long) {
      this.mArgs.putLong(param1String, param1Long);
      return (C)this;
    }
  }
  
  class SemioticClassBuilder<C extends SemioticClassBuilder<?>> extends Builder<C> {
    public SemioticClassBuilder(TtsSpan this$0) {
      super((String)this$0);
    }
    
    public C setGender(String param1String) {
      return setStringArgument("android.arg.gender", param1String);
    }
    
    public C setAnimacy(String param1String) {
      return setStringArgument("android.arg.animacy", param1String);
    }
    
    public C setMultiplicity(String param1String) {
      return setStringArgument("android.arg.multiplicity", param1String);
    }
    
    public C setCase(String param1String) {
      return setStringArgument("android.arg.case", param1String);
    }
  }
  
  public static class TextBuilder extends SemioticClassBuilder<TextBuilder> {
    public TextBuilder() {
      super("android.type.text");
    }
    
    public TextBuilder(String param1String) {
      this();
      setText(param1String);
    }
    
    public TextBuilder setText(String param1String) {
      return setStringArgument("android.arg.text", param1String);
    }
  }
  
  public static class CardinalBuilder extends SemioticClassBuilder<CardinalBuilder> {
    public CardinalBuilder() {
      super("android.type.cardinal");
    }
    
    public CardinalBuilder(long param1Long) {
      this();
      setNumber(param1Long);
    }
    
    public CardinalBuilder(String param1String) {
      this();
      setNumber(param1String);
    }
    
    public CardinalBuilder setNumber(long param1Long) {
      return setNumber(String.valueOf(param1Long));
    }
    
    public CardinalBuilder setNumber(String param1String) {
      return setStringArgument("android.arg.number", param1String);
    }
  }
  
  public static class OrdinalBuilder extends SemioticClassBuilder<OrdinalBuilder> {
    public OrdinalBuilder() {
      super("android.type.ordinal");
    }
    
    public OrdinalBuilder(long param1Long) {
      this();
      setNumber(param1Long);
    }
    
    public OrdinalBuilder(String param1String) {
      this();
      setNumber(param1String);
    }
    
    public OrdinalBuilder setNumber(long param1Long) {
      return setNumber(String.valueOf(param1Long));
    }
    
    public OrdinalBuilder setNumber(String param1String) {
      return setStringArgument("android.arg.number", param1String);
    }
  }
  
  public static class DecimalBuilder extends SemioticClassBuilder<DecimalBuilder> {
    public DecimalBuilder() {
      super("android.type.decimal");
    }
    
    public DecimalBuilder(double param1Double, int param1Int1, int param1Int2) {
      this();
      setArgumentsFromDouble(param1Double, param1Int1, param1Int2);
    }
    
    public DecimalBuilder(String param1String1, String param1String2) {
      this();
      setIntegerPart(param1String1);
      setFractionalPart(param1String2);
    }
    
    public DecimalBuilder setArgumentsFromDouble(double param1Double, int param1Int1, int param1Int2) {
      NumberFormat numberFormat = NumberFormat.getInstance(Locale.US);
      numberFormat.setMinimumFractionDigits(param1Int2);
      numberFormat.setMaximumFractionDigits(param1Int2);
      numberFormat.setGroupingUsed(false);
      String str = numberFormat.format(param1Double);
      param1Int1 = str.indexOf('.');
      if (param1Int1 >= 0) {
        setIntegerPart(str.substring(0, param1Int1));
        setFractionalPart(str.substring(param1Int1 + 1));
      } else {
        setIntegerPart(str);
      } 
      return this;
    }
    
    public DecimalBuilder setIntegerPart(long param1Long) {
      return setIntegerPart(String.valueOf(param1Long));
    }
    
    public DecimalBuilder setIntegerPart(String param1String) {
      return setStringArgument("android.arg.integer_part", param1String);
    }
    
    public DecimalBuilder setFractionalPart(String param1String) {
      return setStringArgument("android.arg.fractional_part", param1String);
    }
  }
  
  public static class FractionBuilder extends SemioticClassBuilder<FractionBuilder> {
    public FractionBuilder() {
      super("android.type.fraction");
    }
    
    public FractionBuilder(long param1Long1, long param1Long2, long param1Long3) {
      this();
      setIntegerPart(param1Long1);
      setNumerator(param1Long2);
      setDenominator(param1Long3);
    }
    
    public FractionBuilder setIntegerPart(long param1Long) {
      return setIntegerPart(String.valueOf(param1Long));
    }
    
    public FractionBuilder setIntegerPart(String param1String) {
      return setStringArgument("android.arg.integer_part", param1String);
    }
    
    public FractionBuilder setNumerator(long param1Long) {
      return setNumerator(String.valueOf(param1Long));
    }
    
    public FractionBuilder setNumerator(String param1String) {
      return setStringArgument("android.arg.numerator", param1String);
    }
    
    public FractionBuilder setDenominator(long param1Long) {
      return setDenominator(String.valueOf(param1Long));
    }
    
    public FractionBuilder setDenominator(String param1String) {
      return setStringArgument("android.arg.denominator", param1String);
    }
  }
  
  public static class MeasureBuilder extends SemioticClassBuilder<MeasureBuilder> {
    public MeasureBuilder() {
      super("android.type.measure");
    }
    
    public MeasureBuilder setNumber(long param1Long) {
      return setNumber(String.valueOf(param1Long));
    }
    
    public MeasureBuilder setNumber(String param1String) {
      return setStringArgument("android.arg.number", param1String);
    }
    
    public MeasureBuilder setIntegerPart(long param1Long) {
      return setIntegerPart(String.valueOf(param1Long));
    }
    
    public MeasureBuilder setIntegerPart(String param1String) {
      return setStringArgument("android.arg.integer_part", param1String);
    }
    
    public MeasureBuilder setFractionalPart(String param1String) {
      return setStringArgument("android.arg.fractional_part", param1String);
    }
    
    public MeasureBuilder setNumerator(long param1Long) {
      return setNumerator(String.valueOf(param1Long));
    }
    
    public MeasureBuilder setNumerator(String param1String) {
      return setStringArgument("android.arg.numerator", param1String);
    }
    
    public MeasureBuilder setDenominator(long param1Long) {
      return setDenominator(String.valueOf(param1Long));
    }
    
    public MeasureBuilder setDenominator(String param1String) {
      return setStringArgument("android.arg.denominator", param1String);
    }
    
    public MeasureBuilder setUnit(String param1String) {
      return setStringArgument("android.arg.unit", param1String);
    }
  }
  
  public static class TimeBuilder extends SemioticClassBuilder<TimeBuilder> {
    public TimeBuilder() {
      super("android.type.time");
    }
    
    public TimeBuilder(int param1Int1, int param1Int2) {
      this();
      setHours(param1Int1);
      setMinutes(param1Int2);
    }
    
    public TimeBuilder setHours(int param1Int) {
      return setIntArgument("android.arg.hours", param1Int);
    }
    
    public TimeBuilder setMinutes(int param1Int) {
      return setIntArgument("android.arg.minutes", param1Int);
    }
  }
  
  public static class DateBuilder extends SemioticClassBuilder<DateBuilder> {
    public DateBuilder() {
      super("android.type.date");
    }
    
    public DateBuilder(Integer param1Integer1, Integer param1Integer2, Integer param1Integer3, Integer param1Integer4) {
      this();
      if (param1Integer1 != null)
        setWeekday(param1Integer1.intValue()); 
      if (param1Integer2 != null)
        setDay(param1Integer2.intValue()); 
      if (param1Integer3 != null)
        setMonth(param1Integer3.intValue()); 
      if (param1Integer4 != null)
        setYear(param1Integer4.intValue()); 
    }
    
    public DateBuilder setWeekday(int param1Int) {
      return setIntArgument("android.arg.weekday", param1Int);
    }
    
    public DateBuilder setDay(int param1Int) {
      return setIntArgument("android.arg.day", param1Int);
    }
    
    public DateBuilder setMonth(int param1Int) {
      return setIntArgument("android.arg.month", param1Int);
    }
    
    public DateBuilder setYear(int param1Int) {
      return setIntArgument("android.arg.year", param1Int);
    }
  }
  
  public static class MoneyBuilder extends SemioticClassBuilder<MoneyBuilder> {
    public MoneyBuilder() {
      super("android.type.money");
    }
    
    public MoneyBuilder setIntegerPart(long param1Long) {
      return setIntegerPart(String.valueOf(param1Long));
    }
    
    public MoneyBuilder setIntegerPart(String param1String) {
      return setStringArgument("android.arg.integer_part", param1String);
    }
    
    public MoneyBuilder setFractionalPart(String param1String) {
      return setStringArgument("android.arg.fractional_part", param1String);
    }
    
    public MoneyBuilder setCurrency(String param1String) {
      return setStringArgument("android.arg.money", param1String);
    }
    
    public MoneyBuilder setQuantity(String param1String) {
      return setStringArgument("android.arg.quantity", param1String);
    }
  }
  
  public static class TelephoneBuilder extends SemioticClassBuilder<TelephoneBuilder> {
    public TelephoneBuilder() {
      super("android.type.telephone");
    }
    
    public TelephoneBuilder(String param1String) {
      this();
      setNumberParts(param1String);
    }
    
    public TelephoneBuilder setCountryCode(String param1String) {
      return setStringArgument("android.arg.country_code", param1String);
    }
    
    public TelephoneBuilder setNumberParts(String param1String) {
      return setStringArgument("android.arg.number_parts", param1String);
    }
    
    public TelephoneBuilder setExtension(String param1String) {
      return setStringArgument("android.arg.extension", param1String);
    }
  }
  
  public static class ElectronicBuilder extends SemioticClassBuilder<ElectronicBuilder> {
    public ElectronicBuilder() {
      super("android.type.electronic");
    }
    
    public ElectronicBuilder setEmailArguments(String param1String1, String param1String2) {
      return setDomain(param1String2).setUsername(param1String1);
    }
    
    public ElectronicBuilder setProtocol(String param1String) {
      return setStringArgument("android.arg.protocol", param1String);
    }
    
    public ElectronicBuilder setUsername(String param1String) {
      return setStringArgument("android.arg.username", param1String);
    }
    
    public ElectronicBuilder setPassword(String param1String) {
      return setStringArgument("android.arg.password", param1String);
    }
    
    public ElectronicBuilder setDomain(String param1String) {
      return setStringArgument("android.arg.domain", param1String);
    }
    
    public ElectronicBuilder setPort(int param1Int) {
      return setIntArgument("android.arg.port", param1Int);
    }
    
    public ElectronicBuilder setPath(String param1String) {
      return setStringArgument("android.arg.path", param1String);
    }
    
    public ElectronicBuilder setQueryString(String param1String) {
      return setStringArgument("android.arg.query_string", param1String);
    }
    
    public ElectronicBuilder setFragmentId(String param1String) {
      return setStringArgument("android.arg.fragment_id", param1String);
    }
  }
  
  public static class DigitsBuilder extends SemioticClassBuilder<DigitsBuilder> {
    public DigitsBuilder() {
      super("android.type.digits");
    }
    
    public DigitsBuilder(String param1String) {
      this();
      setDigits(param1String);
    }
    
    public DigitsBuilder setDigits(String param1String) {
      return setStringArgument("android.arg.digits", param1String);
    }
  }
  
  public static class VerbatimBuilder extends SemioticClassBuilder<VerbatimBuilder> {
    public VerbatimBuilder() {
      super("android.type.verbatim");
    }
    
    public VerbatimBuilder(String param1String) {
      this();
      setVerbatim(param1String);
    }
    
    public VerbatimBuilder setVerbatim(String param1String) {
      return setStringArgument("android.arg.verbatim", param1String);
    }
  }
}
