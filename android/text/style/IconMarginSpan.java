package android.text.style;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.text.Layout;
import android.text.Spanned;

public class IconMarginSpan implements LeadingMarginSpan, LineHeightSpan {
  private final Bitmap mBitmap;
  
  private final int mPad;
  
  public IconMarginSpan(Bitmap paramBitmap) {
    this(paramBitmap, 0);
  }
  
  public IconMarginSpan(Bitmap paramBitmap, int paramInt) {
    this.mBitmap = paramBitmap;
    this.mPad = paramInt;
  }
  
  public int getLeadingMargin(boolean paramBoolean) {
    return this.mBitmap.getWidth() + this.mPad;
  }
  
  public void drawLeadingMargin(Canvas paramCanvas, Paint paramPaint, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, CharSequence paramCharSequence, int paramInt6, int paramInt7, boolean paramBoolean, Layout paramLayout) {
    paramInt3 = ((Spanned)paramCharSequence).getSpanStart(this);
    paramInt3 = paramLayout.getLineTop(paramLayout.getLineForOffset(paramInt3));
    if (paramInt2 < 0)
      paramInt1 -= this.mBitmap.getWidth(); 
    paramCanvas.drawBitmap(this.mBitmap, paramInt1, paramInt3, paramPaint);
  }
  
  public void chooseHeight(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3, int paramInt4, Paint.FontMetricsInt paramFontMetricsInt) {
    if (paramInt2 == ((Spanned)paramCharSequence).getSpanEnd(this)) {
      paramInt2 = this.mBitmap.getHeight();
      paramInt1 = paramInt2 - paramFontMetricsInt.descent + paramInt4 - paramFontMetricsInt.ascent - paramInt3;
      if (paramInt1 > 0)
        paramFontMetricsInt.descent += paramInt1; 
      paramInt1 = paramInt2 - paramFontMetricsInt.bottom + paramInt4 - paramFontMetricsInt.top - paramInt3;
      if (paramInt1 > 0)
        paramFontMetricsInt.bottom += paramInt1; 
    } 
  }
}
