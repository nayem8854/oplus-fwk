package android.text.style;

import android.graphics.Paint;
import android.os.LocaleList;
import android.os.Parcel;
import android.text.ParcelableSpan;
import android.text.TextPaint;
import com.android.internal.util.Preconditions;
import java.util.Locale;

public class LocaleSpan extends MetricAffectingSpan implements ParcelableSpan {
  private final LocaleList mLocales;
  
  public LocaleSpan(Locale paramLocale) {
    LocaleList localeList;
    if (paramLocale == null) {
      localeList = LocaleList.getEmptyLocaleList();
    } else {
      localeList = new LocaleList(new Locale[] { (Locale)localeList });
    } 
    this.mLocales = localeList;
  }
  
  public LocaleSpan(LocaleList paramLocaleList) {
    Preconditions.checkNotNull(paramLocaleList, "locales cannot be null");
    this.mLocales = paramLocaleList;
  }
  
  public LocaleSpan(Parcel paramParcel) {
    this.mLocales = (LocaleList)LocaleList.CREATOR.createFromParcel(paramParcel);
  }
  
  public int getSpanTypeId() {
    return getSpanTypeIdInternal();
  }
  
  public int getSpanTypeIdInternal() {
    return 23;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    writeToParcelInternal(paramParcel, paramInt);
  }
  
  public void writeToParcelInternal(Parcel paramParcel, int paramInt) {
    this.mLocales.writeToParcel(paramParcel, paramInt);
  }
  
  public Locale getLocale() {
    return this.mLocales.get(0);
  }
  
  public LocaleList getLocales() {
    return this.mLocales;
  }
  
  public void updateDrawState(TextPaint paramTextPaint) {
    apply(paramTextPaint, this.mLocales);
  }
  
  public void updateMeasureState(TextPaint paramTextPaint) {
    apply(paramTextPaint, this.mLocales);
  }
  
  private static void apply(Paint paramPaint, LocaleList paramLocaleList) {
    paramPaint.setTextLocales(paramLocaleList);
  }
}
