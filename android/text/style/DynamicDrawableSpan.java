package android.text.style;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.ref.WeakReference;

public abstract class DynamicDrawableSpan extends ReplacementSpan {
  public static final int ALIGN_BASELINE = 1;
  
  public static final int ALIGN_BOTTOM = 0;
  
  public static final int ALIGN_CENTER = 2;
  
  private WeakReference<Drawable> mDrawableRef;
  
  protected final int mVerticalAlignment;
  
  public DynamicDrawableSpan() {
    this.mVerticalAlignment = 0;
  }
  
  protected DynamicDrawableSpan(int paramInt) {
    this.mVerticalAlignment = paramInt;
  }
  
  public int getVerticalAlignment() {
    return this.mVerticalAlignment;
  }
  
  public int getSize(Paint paramPaint, CharSequence paramCharSequence, int paramInt1, int paramInt2, Paint.FontMetricsInt paramFontMetricsInt) {
    Drawable drawable = getCachedDrawable();
    Rect rect = drawable.getBounds();
    if (paramFontMetricsInt != null) {
      paramFontMetricsInt.ascent = -rect.bottom;
      paramFontMetricsInt.descent = 0;
      paramFontMetricsInt.top = paramFontMetricsInt.ascent;
      paramFontMetricsInt.bottom = 0;
    } 
    return rect.right;
  }
  
  public void draw(Canvas paramCanvas, CharSequence paramCharSequence, int paramInt1, int paramInt2, float paramFloat, int paramInt3, int paramInt4, int paramInt5, Paint paramPaint) {
    Drawable drawable = getCachedDrawable();
    paramCanvas.save();
    paramInt1 = paramInt5 - (drawable.getBounds()).bottom;
    paramInt2 = this.mVerticalAlignment;
    if (paramInt2 == 1) {
      paramInt1 -= (paramPaint.getFontMetricsInt()).descent;
    } else if (paramInt2 == 2) {
      paramInt1 = (paramInt5 - paramInt3) / 2 + paramInt3 - drawable.getBounds().height() / 2;
    } 
    paramCanvas.translate(paramFloat, paramInt1);
    drawable.draw(paramCanvas);
    paramCanvas.restore();
  }
  
  private Drawable getCachedDrawable() {
    WeakReference<Drawable> weakReference = this.mDrawableRef;
    Drawable drawable2 = null;
    if (weakReference != null)
      drawable2 = weakReference.get(); 
    Drawable drawable1 = drawable2;
    if (drawable2 == null) {
      drawable1 = getDrawable();
      this.mDrawableRef = new WeakReference<>(drawable1);
    } 
    return drawable1;
  }
  
  public abstract Drawable getDrawable();
  
  @Retention(RetentionPolicy.SOURCE)
  class AlignmentType implements Annotation {}
}
