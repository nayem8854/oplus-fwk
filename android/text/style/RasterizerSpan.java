package android.text.style;

import android.graphics.Rasterizer;
import android.text.TextPaint;

public class RasterizerSpan extends CharacterStyle implements UpdateAppearance {
  private Rasterizer mRasterizer;
  
  public RasterizerSpan(Rasterizer paramRasterizer) {
    this.mRasterizer = paramRasterizer;
  }
  
  public Rasterizer getRasterizer() {
    return this.mRasterizer;
  }
  
  public void updateDrawState(TextPaint paramTextPaint) {
    paramTextPaint.setRasterizer(this.mRasterizer);
  }
}
