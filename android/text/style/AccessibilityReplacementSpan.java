package android.text.style;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.ParcelableSpan;

public class AccessibilityReplacementSpan extends ReplacementSpan implements ParcelableSpan {
  public AccessibilityReplacementSpan(CharSequence paramCharSequence) {
    setContentDescription(paramCharSequence);
  }
  
  public AccessibilityReplacementSpan(Parcel paramParcel) {
    CharSequence charSequence = paramParcel.readCharSequence();
    setContentDescription(charSequence);
  }
  
  public int getSpanTypeId() {
    return getSpanTypeIdInternal();
  }
  
  public int getSpanTypeIdInternal() {
    return 29;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    writeToParcelInternal(paramParcel, paramInt);
  }
  
  public void writeToParcelInternal(Parcel paramParcel, int paramInt) {
    paramParcel.writeCharSequence(getContentDescription());
  }
  
  public int getSize(Paint paramPaint, CharSequence paramCharSequence, int paramInt1, int paramInt2, Paint.FontMetricsInt paramFontMetricsInt) {
    return 0;
  }
  
  public void draw(Canvas paramCanvas, CharSequence paramCharSequence, int paramInt1, int paramInt2, float paramFloat, int paramInt3, int paramInt4, int paramInt5, Paint paramPaint) {}
  
  public static final Parcelable.Creator<AccessibilityReplacementSpan> CREATOR = (Parcelable.Creator<AccessibilityReplacementSpan>)new Object();
}
