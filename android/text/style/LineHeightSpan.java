package android.text.style;

import android.graphics.Paint;
import android.os.Parcel;
import android.text.ParcelableSpan;
import android.text.TextPaint;
import com.android.internal.util.Preconditions;

public interface LineHeightSpan extends ParagraphStyle, WrapTogetherSpan {
  void chooseHeight(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3, int paramInt4, Paint.FontMetricsInt paramFontMetricsInt);
  
  class Standard implements LineHeightSpan, ParcelableSpan {
    private final int mHeight;
    
    public Standard(LineHeightSpan this$0) {
      boolean bool;
      if (this$0 > null) {
        bool = true;
      } else {
        bool = false;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Height:");
      stringBuilder.append(this$0);
      stringBuilder.append("must be positive");
      Preconditions.checkArgument(bool, stringBuilder.toString());
      this.mHeight = this$0;
    }
    
    public Standard(LineHeightSpan this$0) {
      this.mHeight = this$0.readInt();
    }
    
    public int getHeight() {
      return this.mHeight;
    }
    
    public int getSpanTypeId() {
      return getSpanTypeIdInternal();
    }
    
    public int getSpanTypeIdInternal() {
      return 28;
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      writeToParcelInternal(param1Parcel, param1Int);
    }
    
    public void writeToParcelInternal(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.mHeight);
    }
    
    public void chooseHeight(CharSequence param1CharSequence, int param1Int1, int param1Int2, int param1Int3, int param1Int4, Paint.FontMetricsInt param1FontMetricsInt) {
      param1Int1 = param1FontMetricsInt.descent - param1FontMetricsInt.ascent;
      if (param1Int1 <= 0)
        return; 
      float f = this.mHeight * 1.0F / param1Int1;
      param1FontMetricsInt.descent = Math.round(param1FontMetricsInt.descent * f);
      param1FontMetricsInt.ascent = param1FontMetricsInt.descent - this.mHeight;
    }
  }
  
  class WithDensity implements LineHeightSpan {
    public abstract void chooseHeight(CharSequence param1CharSequence, int param1Int1, int param1Int2, int param1Int3, int param1Int4, Paint.FontMetricsInt param1FontMetricsInt, TextPaint param1TextPaint);
  }
}
