package android.text;

import android.graphics.BaseCanvas;
import android.graphics.Paint;

public interface GraphicsOperations extends CharSequence {
  void drawText(BaseCanvas paramBaseCanvas, int paramInt1, int paramInt2, float paramFloat1, float paramFloat2, Paint paramPaint);
  
  void drawTextRun(BaseCanvas paramBaseCanvas, int paramInt1, int paramInt2, int paramInt3, int paramInt4, float paramFloat1, float paramFloat2, boolean paramBoolean, Paint paramPaint);
  
  float getTextRunAdvances(int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean, float[] paramArrayOffloat, int paramInt5, Paint paramPaint);
  
  int getTextRunCursor(int paramInt1, int paramInt2, boolean paramBoolean, int paramInt3, int paramInt4, Paint paramPaint);
  
  int getTextWidths(int paramInt1, int paramInt2, float[] paramArrayOffloat, Paint paramPaint);
  
  float measureText(int paramInt1, int paramInt2, Paint paramPaint);
}
