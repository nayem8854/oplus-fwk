package android.text;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.text.style.AlignmentSpan;
import android.text.style.LeadingMarginSpan;
import android.text.style.LineBackgroundSpan;
import android.text.style.ParagraphStyle;
import android.text.style.ReplacementSpan;
import android.text.style.TabStopSpan;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.GrowingArrayUtils;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Arrays;

public abstract class Layout {
  private static final ParagraphStyle[] NO_PARA_SPANS = (ParagraphStyle[])ArrayUtils.emptyArray(ParagraphStyle.class);
  
  public static float getDesiredWidth(CharSequence paramCharSequence, TextPaint paramTextPaint) {
    return getDesiredWidth(paramCharSequence, 0, paramCharSequence.length(), paramTextPaint);
  }
  
  public static float getDesiredWidth(CharSequence paramCharSequence, int paramInt1, int paramInt2, TextPaint paramTextPaint) {
    return getDesiredWidth(paramCharSequence, paramInt1, paramInt2, paramTextPaint, TextDirectionHeuristics.FIRSTSTRONG_LTR);
  }
  
  public static float getDesiredWidth(CharSequence paramCharSequence, int paramInt1, int paramInt2, TextPaint paramTextPaint, TextDirectionHeuristic paramTextDirectionHeuristic) {
    return getDesiredWidthWithLimit(paramCharSequence, paramInt1, paramInt2, paramTextPaint, paramTextDirectionHeuristic, Float.MAX_VALUE);
  }
  
  public static float getDesiredWidthWithLimit(CharSequence paramCharSequence, int paramInt1, int paramInt2, TextPaint paramTextPaint, TextDirectionHeuristic paramTextDirectionHeuristic, float paramFloat) {
    float f = 0.0F;
    for (; paramInt1 <= paramInt2; paramInt1 = j + 1, f = f2) {
      int i = TextUtils.indexOf(paramCharSequence, '\n', paramInt1, paramInt2);
      int j = i;
      if (i < 0)
        j = paramInt2; 
      float f1 = measurePara(paramTextPaint, paramCharSequence, paramInt1, j, paramTextDirectionHeuristic);
      if (f1 > paramFloat)
        return paramFloat; 
      float f2 = f;
      if (f1 > f)
        f2 = f1; 
    } 
    return f;
  }
  
  protected Layout(CharSequence paramCharSequence, TextPaint paramTextPaint, int paramInt, Alignment paramAlignment, float paramFloat1, float paramFloat2) {
    this(paramCharSequence, paramTextPaint, paramInt, paramAlignment, TextDirectionHeuristics.FIRSTSTRONG_LTR, paramFloat1, paramFloat2);
  }
  
  protected Layout(CharSequence paramCharSequence, TextPaint paramTextPaint, int paramInt, Alignment paramAlignment, TextDirectionHeuristic paramTextDirectionHeuristic, float paramFloat1, float paramFloat2) {
    if (paramInt >= 0) {
      if (paramTextPaint != null) {
        paramTextPaint.bgColor = 0;
        paramTextPaint.baselineShift = 0;
      } 
      this.mText = paramCharSequence;
      this.mPaint = paramTextPaint;
      this.mWidth = paramInt;
      this.mAlignment = paramAlignment;
      this.mSpacingMult = paramFloat1;
      this.mSpacingAdd = paramFloat2;
      this.mSpannedText = paramCharSequence instanceof Spanned;
      this.mTextDir = paramTextDirectionHeuristic;
      return;
    } 
    paramCharSequence = new StringBuilder();
    paramCharSequence.append("Layout: ");
    paramCharSequence.append(paramInt);
    paramCharSequence.append(" < 0");
    throw new IllegalArgumentException(paramCharSequence.toString());
  }
  
  protected void setJustificationMode(int paramInt) {
    this.mJustificationMode = paramInt;
  }
  
  void replaceWith(CharSequence paramCharSequence, TextPaint paramTextPaint, int paramInt, Alignment paramAlignment, float paramFloat1, float paramFloat2) {
    if (paramInt >= 0) {
      this.mText = paramCharSequence;
      this.mPaint = paramTextPaint;
      this.mWidth = paramInt;
      this.mAlignment = paramAlignment;
      this.mSpacingMult = paramFloat1;
      this.mSpacingAdd = paramFloat2;
      this.mSpannedText = paramCharSequence instanceof Spanned;
      return;
    } 
    paramCharSequence = new StringBuilder();
    paramCharSequence.append("Layout: ");
    paramCharSequence.append(paramInt);
    paramCharSequence.append(" < 0");
    throw new IllegalArgumentException(paramCharSequence.toString());
  }
  
  public void draw(Canvas paramCanvas) {
    draw(paramCanvas, null, null, 0);
  }
  
  public void draw(Canvas paramCanvas, Path paramPath, Paint paramPaint, int paramInt) {
    long l = getLineRangeForDraw(paramCanvas);
    int i = TextUtils.unpackRangeStartFromLong(l);
    int j = TextUtils.unpackRangeEndFromLong(l);
    if (j < 0)
      return; 
    drawBackground(paramCanvas, paramPath, paramPaint, paramInt, i, j);
    drawText(paramCanvas, i, j);
  }
  
  private boolean isJustificationRequired(int paramInt) {
    int i = this.mJustificationMode;
    boolean bool1 = false;
    if (i == 0)
      return false; 
    paramInt = getLineEnd(paramInt);
    boolean bool2 = bool1;
    if (paramInt < this.mText.length()) {
      bool2 = bool1;
      if (this.mText.charAt(paramInt - 1) != '\n')
        bool2 = true; 
    } 
    return bool2;
  }
  
  private float getJustifyWidth(int paramInt) {
    LeadingMarginSpan leadingMarginSpan1, leadingMarginSpan2;
    Alignment alignment1 = this.mAlignment;
    int i = 0;
    boolean bool = false;
    int j = this.mWidth;
    int k = getParagraphDirection(paramInt);
    ParagraphStyle[] arrayOfParagraphStyle = NO_PARA_SPANS;
    Alignment alignment2 = alignment1;
    int m = j;
    if (this.mSpannedText) {
      boolean bool1;
      Spanned spanned = (Spanned)this.mText;
      int n = getLineStart(paramInt);
      if (n == 0 || this.mText.charAt(n - 1) == '\n') {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      Alignment alignment = alignment1;
      if (bool1) {
        i = spanned.nextSpanTransition(n, this.mText.length(), ParagraphStyle.class);
        ParagraphStyle[] arrayOfParagraphStyle1 = getParagraphSpans(spanned, n, i, ParagraphStyle.class);
        n = arrayOfParagraphStyle1.length - 1;
        while (true) {
          alignment = alignment1;
          arrayOfParagraphStyle = arrayOfParagraphStyle1;
          if (n >= 0) {
            if (arrayOfParagraphStyle1[n] instanceof AlignmentSpan) {
              alignment = ((AlignmentSpan)arrayOfParagraphStyle1[n]).getAlignment();
              arrayOfParagraphStyle = arrayOfParagraphStyle1;
              break;
            } 
            n--;
            continue;
          } 
          break;
        } 
      } 
      int i1 = arrayOfParagraphStyle.length;
      boolean bool2 = bool1;
      n = 0;
      while (true) {
        bool1 = bool2;
        if (n < i1) {
          if (arrayOfParagraphStyle[n] instanceof LeadingMarginSpan.LeadingMarginSpan2) {
            i = ((LeadingMarginSpan.LeadingMarginSpan2)arrayOfParagraphStyle[n]).getLeadingMarginLineCount();
            m = getLineForOffset(spanned.getSpanStart(arrayOfParagraphStyle[n]));
            if (paramInt < m + i) {
              bool1 = true;
              break;
            } 
          } 
          n++;
          continue;
        } 
        break;
      } 
      byte b = 0;
      n = bool;
      while (true) {
        alignment2 = alignment;
        i = n;
        m = j;
        if (b < i1) {
          m = n;
          i = j;
          if (arrayOfParagraphStyle[b] instanceof LeadingMarginSpan) {
            leadingMarginSpan1 = (LeadingMarginSpan)arrayOfParagraphStyle[b];
            if (k == -1) {
              i = j - leadingMarginSpan1.getLeadingMargin(bool1);
              m = n;
            } else {
              m = n + leadingMarginSpan1.getLeadingMargin(bool1);
              i = j;
            } 
          } 
          b++;
          n = m;
          j = i;
          continue;
        } 
        break;
      } 
    } 
    if (leadingMarginSpan1 == Alignment.ALIGN_LEFT) {
      if (k == 1) {
        Alignment alignment = Alignment.ALIGN_NORMAL;
      } else {
        Alignment alignment = Alignment.ALIGN_OPPOSITE;
      } 
    } else if (leadingMarginSpan1 == Alignment.ALIGN_RIGHT) {
      if (k == 1) {
        Alignment alignment = Alignment.ALIGN_OPPOSITE;
      } else {
        Alignment alignment = Alignment.ALIGN_NORMAL;
      } 
    } else {
      leadingMarginSpan2 = leadingMarginSpan1;
    } 
    if (leadingMarginSpan2 == Alignment.ALIGN_NORMAL) {
      if (k == 1) {
        paramInt = getIndentAdjust(paramInt, Alignment.ALIGN_LEFT);
      } else {
        paramInt = -getIndentAdjust(paramInt, Alignment.ALIGN_RIGHT);
      } 
    } else if (leadingMarginSpan2 == Alignment.ALIGN_OPPOSITE) {
      if (k == 1) {
        paramInt = -getIndentAdjust(paramInt, Alignment.ALIGN_RIGHT);
      } else {
        paramInt = getIndentAdjust(paramInt, Alignment.ALIGN_LEFT);
      } 
    } else {
      paramInt = getIndentAdjust(paramInt, Alignment.ALIGN_CENTER);
    } 
    return (m - i - paramInt);
  }
  
  public void drawText(Canvas paramCanvas, int paramInt1, int paramInt2) {
    Layout layout = this;
    int i = layout.getLineTop(paramInt1);
    int j = layout.getLineStart(paramInt1);
    ParagraphStyle[] arrayOfParagraphStyle = NO_PARA_SPANS;
    int k = 0;
    TextPaint textPaint = layout.mWorkPaint;
    textPaint.set(layout.mPaint);
    CharSequence charSequence1 = layout.mText;
    Alignment alignment = layout.mAlignment;
    int m = 0;
    TextLine textLine = TextLine.obtain();
    LeadingMarginSpan leadingMarginSpan;
    CharSequence charSequence2;
    int n;
    for (charSequence2 = null, n = paramInt1; n <= paramInt2; n = i7 + 1, alignment7 = alignment5, alignment8 = alignment6, leadingMarginSpan = leadingMarginSpan1, i = i3, charSequence4 = charSequence2, j = i1, m = k, textLine1 = textLine3, k = i9, textPaint2 = textPaint1, tabStops2 = tabStops1, charSequence3 = charSequence4, alignment3 = alignment7, alignment1 = alignment8) {
      Alignment alignment2;
      TextPaint textPaint1;
      Alignment alignment1;
      TextLine textLine2;
      LeadingMarginSpan leadingMarginSpan1;
      TextLine textLine1;
      Alignment alignment4;
      TextLine textLine3;
      Alignment alignment3;
      TabStops tabStops1;
      CharSequence charSequence3;
      TextPaint textPaint3;
      Alignment alignment5;
      TextPaint textPaint2;
      LeadingMarginSpan leadingMarginSpan2;
      Alignment alignment6;
      TabStops tabStops2;
      TextPaint textPaint4;
      Alignment alignment7;
      int i7;
      Alignment alignment8;
      int i8, i9;
      CharSequence charSequence4;
      int i1 = layout.getLineStart(n + 1);
      boolean bool1 = layout.isJustificationRequired(n);
      int i2 = layout.getLineVisibleEnd(n, j, i1);
      textPaint.setStartHyphenEdit(layout.getStartHyphenEdit(n));
      textPaint.setEndHyphenEdit(layout.getEndHyphenEdit(n));
      int i3 = layout.getLineTop(n + 1);
      int i4 = layout.getLineDescent(n);
      int i5 = layout.getParagraphDirection(n);
      int i6 = i3 - i4;
      i4 = layout.mWidth;
      boolean bool2 = layout.mSpannedText;
      if (bool2) {
        Alignment alignment9, alignment10;
        Spanned spanned2 = (Spanned)charSequence1;
        i7 = charSequence1.length();
        if (j == 0 || charSequence1.charAt(j - 1) == '\n') {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        if (j >= k && (n == paramInt1 || bool2)) {
          k = spanned2.nextSpanTransition(j, i7, ParagraphStyle.class);
          ParagraphStyle[] arrayOfParagraphStyle1 = getParagraphSpans(spanned2, j, k, ParagraphStyle.class);
          alignment9 = layout.mAlignment;
          for (m = arrayOfParagraphStyle1.length - 1; m >= 0; m--) {
            if (arrayOfParagraphStyle1[m] instanceof AlignmentSpan) {
              alignment9 = ((AlignmentSpan)arrayOfParagraphStyle1[m]).getAlignment();
              break;
            } 
          } 
          m = 0;
        } else {
          alignment10 = alignment9;
          alignment9 = alignment;
        } 
        int i10 = alignment10.length;
        boolean bool = bool2;
        Spanned spanned1;
        for (i8 = 0, spanned1 = spanned2; i8 < i10; i8++) {
          if (alignment10[i8] instanceof LeadingMarginSpan.LeadingMarginSpan2) {
            int i11 = ((LeadingMarginSpan.LeadingMarginSpan2)alignment10[i8]).getLeadingMarginLineCount();
            int i12 = layout.getLineForOffset(spanned1.getSpanStart(alignment10[i8]));
            if (n < i12 + i11) {
              bool = true;
              break;
            } 
          } 
        } 
        LeadingMarginSpan leadingMarginSpan3;
        CharSequence charSequence;
        for (boolean bool3 = false; i10 < i6; i10++, leadingMarginSpan3 = leadingMarginSpan4) {
          LeadingMarginSpan leadingMarginSpan4;
          if (alignment4[i10] instanceof LeadingMarginSpan) {
            leadingMarginSpan3 = (LeadingMarginSpan)alignment4[i10];
            if (i5 == -1) {
              leadingMarginSpan3.drawLeadingMargin(paramCanvas, textPaint3, i8, i5, i9, i, i3, charSequence, j, i2, bool2, this);
              i8 -= leadingMarginSpan3.getLeadingMargin(bool);
              Layout layout1 = this;
            } else {
              Layout layout1 = this;
              leadingMarginSpan3.drawLeadingMargin(paramCanvas, textPaint3, i4, i5, i9, i, i3, charSequence, j, i2, bool2, this);
              i4 += leadingMarginSpan3.getLeadingMargin(bool);
            } 
          } else {
            leadingMarginSpan4 = leadingMarginSpan3;
          } 
        } 
        charSequence1 = charSequence2;
        charSequence2 = charSequence;
        i7 = i;
        TextLine textLine4 = textLine;
        i6 = i9;
        leadingMarginSpan2 = leadingMarginSpan3;
        i9 = k;
        alignment2 = alignment9;
        i = i5;
        i5 = i7;
        i7 = n;
        textLine2 = textLine4;
      } else {
        i9 = i5;
        CharSequence charSequence = charSequence1;
        Alignment alignment9 = alignment4;
        alignment4 = alignment2;
        i5 = i6;
        i6 = i;
        i = 0;
        i8 = i4;
        TextLine textLine4 = textLine2;
        leadingMarginSpan1 = leadingMarginSpan2;
        charSequence1 = charSequence2;
        i7 = n;
        i4 = i;
        i = i9;
        charSequence2 = charSequence;
        textPaint1 = textPaint3;
        i9 = k;
        alignment6 = alignment4;
        textLine3 = textLine4;
        alignment5 = alignment9;
      } 
      bool2 = alignment6.getLineContainsTab(i7);
      if (bool2 && m == 0) {
        if (charSequence1 == null) {
          tabStops1 = new TabStops(20.0F, (Object[])textLine3);
        } else {
          tabStops1.reset(20.0F, (Object[])textLine3);
        } 
        k = 1;
      } else {
        k = m;
      } 
      if (textPaint1 == Alignment.ALIGN_LEFT) {
        if (i == 1) {
          Alignment alignment9 = Alignment.ALIGN_NORMAL;
        } else {
          Alignment alignment9 = Alignment.ALIGN_OPPOSITE;
        } 
      } else if (textPaint1 == Alignment.ALIGN_RIGHT) {
        if (i == 1) {
          Alignment alignment9 = Alignment.ALIGN_OPPOSITE;
        } else {
          Alignment alignment9 = Alignment.ALIGN_NORMAL;
        } 
      } else {
        textPaint4 = textPaint1;
      } 
      if (textPaint4 == Alignment.ALIGN_NORMAL) {
        if (i == 1) {
          n = alignment6.getIndentAdjust(i7, Alignment.ALIGN_LEFT);
          m = n;
          n = i4 + n;
        } else {
          n = -alignment6.getIndentAdjust(i7, Alignment.ALIGN_RIGHT);
          m = n;
          n = i8 - n;
        } 
      } else {
        int i10 = (int)alignment6.getLineExtent(i7, tabStops1, false);
        if (textPaint4 == Alignment.ALIGN_OPPOSITE) {
          if (i == 1) {
            n = -alignment6.getIndentAdjust(i7, Alignment.ALIGN_RIGHT);
            m = n;
            n = i8 - i10 - n;
          } else {
            n = alignment6.getIndentAdjust(i7, Alignment.ALIGN_LEFT);
            m = n;
            n = i4 - i10 + n;
          } 
        } else {
          m = alignment6.getIndentAdjust(i7, Alignment.ALIGN_CENTER);
          n = (i8 + i4 - (i10 & 0xFFFFFFFE) >> 1) + m;
        } 
      } 
      Directions directions = alignment6.getLineDirections(i7);
      if (directions == DIRS_ALL_LEFT_TO_RIGHT && !((Layout)alignment6).mSpannedText && !bool2 && !bool1) {
        paramCanvas.drawText(charSequence2, j, i2, n, i5, (Paint)alignment5);
      } else {
        int i11 = alignment6.getEllipsisStart(i7);
        int i10 = alignment6.getEllipsisStart(i7), i12 = alignment6.getEllipsisCount(i7);
        leadingMarginSpan1.set((TextPaint)alignment5, charSequence2, j, i2, i, directions, bool2, tabStops1, i11, i10 + i12);
        if (bool1)
          leadingMarginSpan1.justify((i8 - i4 - m)); 
        leadingMarginSpan1.draw(paramCanvas, n, i6, i5, i3);
      } 
    } 
    TextLine.recycle((TextLine)leadingMarginSpan);
  }
  
  public void drawBackground(Canvas paramCanvas, Path paramPath, Paint paramPaint, int paramInt1, int paramInt2, int paramInt3) {
    if (this.mSpannedText) {
      if (this.mLineBackgroundSpans == null)
        this.mLineBackgroundSpans = new SpanSet<>(LineBackgroundSpan.class); 
      Spanned spanned = (Spanned)this.mText;
      int i = spanned.length();
      this.mLineBackgroundSpans.init(spanned, 0, i);
      if (this.mLineBackgroundSpans.numberOfSpans > 0) {
        int j = getLineTop(paramInt2);
        int k = getLineStart(paramInt2);
        ParagraphStyle[] arrayOfParagraphStyle = NO_PARA_SPANS;
        int m = 0;
        TextPaint textPaint = this.mPaint;
        int n = 0;
        int i1 = this.mWidth;
        int i2;
        for (i2 = paramInt2, paramInt2 = m; i2 <= paramInt3; i5++, k = m, j = i3, i2 = paramInt2, paramInt2 = n, n = i2, i2 = i5) {
          m = getLineStart(i2 + 1);
          int i3 = getLineTop(i2 + 1);
          int i4 = getLineDescent(i2);
          if (m >= n) {
            paramInt2 = this.mLineBackgroundSpans.getNextTransition(k, i);
            n = 0;
            if (k != m || k == 0) {
              int i6;
              int i7;
              for (i6 = paramInt2, i7 = 0, paramInt2 = n, n = i7; n < this.mLineBackgroundSpans.numberOfSpans; n++, arrayOfParagraphStyle = arrayOfParagraphStyle1, paramInt2 = i7) {
                ParagraphStyle[] arrayOfParagraphStyle1 = arrayOfParagraphStyle;
                i7 = paramInt2;
                if (this.mLineBackgroundSpans.spanStarts[n] < m)
                  if (this.mLineBackgroundSpans.spanEnds[n] <= k) {
                    arrayOfParagraphStyle1 = arrayOfParagraphStyle;
                    i7 = paramInt2;
                  } else {
                    arrayOfParagraphStyle1 = (ParagraphStyle[])GrowingArrayUtils.append((Object[])arrayOfParagraphStyle, paramInt2, ((LineBackgroundSpan[])this.mLineBackgroundSpans.spans)[n]);
                    i7 = paramInt2 + 1;
                  }  
              } 
              n = paramInt2;
              paramInt2 = i6;
            } else {
              n = 0;
            } 
          } else {
            int i6 = n;
            n = paramInt2;
            paramInt2 = i6;
          } 
          int i5;
          for (byte b = 0; b < n; b++) {
            LineBackgroundSpan lineBackgroundSpan = (LineBackgroundSpan)arrayOfParagraphStyle[b];
            lineBackgroundSpan.drawBackground(paramCanvas, textPaint, 0, i1, j, i3 - i4, i3, spanned, k, i2, i5);
          } 
        } 
      } 
      this.mLineBackgroundSpans.recycle();
    } 
    if (paramPath != null) {
      if (paramInt1 != 0)
        paramCanvas.translate(0.0F, paramInt1); 
      paramCanvas.drawPath(paramPath, paramPaint);
      if (paramInt1 != 0)
        paramCanvas.translate(0.0F, -paramInt1); 
    } 
  }
  
  public long getLineRangeForDraw(Canvas paramCanvas) {
    synchronized (sTempRect) {
      if (!paramCanvas.getClipBounds(sTempRect))
        return TextUtils.packRangeInLong(0, -1); 
      int i = sTempRect.top;
      int j = sTempRect.bottom;
      i = Math.max(i, 0);
      j = Math.min(getLineTop(getLineCount()), j);
      if (i >= j)
        return TextUtils.packRangeInLong(0, -1); 
      return TextUtils.packRangeInLong(getLineForVertical(i), getLineForVertical(j));
    } 
  }
  
  private int getLineStartPos(int paramInt1, int paramInt2, int paramInt3) {
    Alignment alignment2, alignment1 = getParagraphAlignment(paramInt1);
    int i = getParagraphDirection(paramInt1);
    if (alignment1 == Alignment.ALIGN_LEFT) {
      if (i == 1) {
        alignment2 = Alignment.ALIGN_NORMAL;
      } else {
        alignment2 = Alignment.ALIGN_OPPOSITE;
      } 
    } else {
      alignment2 = alignment1;
      if (alignment1 == Alignment.ALIGN_RIGHT)
        if (i == 1) {
          alignment2 = Alignment.ALIGN_OPPOSITE;
        } else {
          alignment2 = Alignment.ALIGN_NORMAL;
        }  
    } 
    if (alignment2 == Alignment.ALIGN_NORMAL) {
      if (i == 1) {
        paramInt1 = getIndentAdjust(paramInt1, Alignment.ALIGN_LEFT) + paramInt2;
      } else {
        paramInt1 = getIndentAdjust(paramInt1, Alignment.ALIGN_RIGHT) + paramInt3;
      } 
    } else {
      TabStops tabStops;
      Alignment alignment = null;
      alignment1 = alignment;
      if (this.mSpannedText) {
        alignment1 = alignment;
        if (getLineContainsTab(paramInt1)) {
          Spanned spanned = (Spanned)this.mText;
          int k = getLineStart(paramInt1);
          int m = spanned.nextSpanTransition(k, spanned.length(), TabStopSpan.class);
          TabStopSpan[] arrayOfTabStopSpan = getParagraphSpans(spanned, k, m, TabStopSpan.class);
          Alignment alignment3 = alignment;
          if (arrayOfTabStopSpan.length > 0)
            tabStops = new TabStops(20.0F, (Object[])arrayOfTabStopSpan); 
        } 
      } 
      int j = (int)getLineExtent(paramInt1, tabStops, false);
      if (alignment2 == Alignment.ALIGN_OPPOSITE) {
        if (i == 1) {
          paramInt1 = paramInt3 - j + getIndentAdjust(paramInt1, Alignment.ALIGN_RIGHT);
        } else {
          paramInt1 = paramInt2 - j + getIndentAdjust(paramInt1, Alignment.ALIGN_LEFT);
        } 
      } else {
        paramInt1 = paramInt2 + paramInt3 - (j & 0xFFFFFFFE) >> getIndentAdjust(paramInt1, Alignment.ALIGN_CENTER) + 1;
      } 
    } 
    return paramInt1;
  }
  
  public final CharSequence getText() {
    return this.mText;
  }
  
  public final TextPaint getPaint() {
    return this.mPaint;
  }
  
  public final int getWidth() {
    return this.mWidth;
  }
  
  public int getEllipsizedWidth() {
    return this.mWidth;
  }
  
  public final void increaseWidthTo(int paramInt) {
    if (paramInt >= this.mWidth) {
      this.mWidth = paramInt;
      return;
    } 
    throw new RuntimeException("attempted to reduce Layout width");
  }
  
  public int getHeight() {
    return getLineTop(getLineCount());
  }
  
  public int getHeight(boolean paramBoolean) {
    return getHeight();
  }
  
  public final Alignment getAlignment() {
    return this.mAlignment;
  }
  
  public final float getSpacingMultiplier() {
    return this.mSpacingMult;
  }
  
  public final float getSpacingAdd() {
    return this.mSpacingAdd;
  }
  
  public final TextDirectionHeuristic getTextDirectionHeuristic() {
    return this.mTextDir;
  }
  
  public int getLineBounds(int paramInt, Rect paramRect) {
    if (paramRect != null) {
      paramRect.left = 0;
      paramRect.top = getLineTop(paramInt);
      paramRect.right = this.mWidth;
      paramRect.bottom = getLineTop(paramInt + 1);
    } 
    return getLineBaseline(paramInt);
  }
  
  public int getStartHyphenEdit(int paramInt) {
    return 0;
  }
  
  public int getEndHyphenEdit(int paramInt) {
    return 0;
  }
  
  public int getIndentAdjust(int paramInt, Alignment paramAlignment) {
    return 0;
  }
  
  public boolean isLevelBoundary(int paramInt) {
    int i = getLineForOffset(paramInt);
    Directions directions1 = getLineDirections(i);
    Directions directions2 = DIRS_ALL_LEFT_TO_RIGHT;
    boolean bool = false;
    if (directions1 == directions2 || directions1 == DIRS_ALL_RIGHT_TO_LEFT)
      return false; 
    int[] arrayOfInt = directions1.mDirections;
    int j = getLineStart(i);
    int k = getLineEnd(i);
    if (paramInt == j || paramInt == k) {
      if (getParagraphDirection(i) == 1) {
        i = 0;
      } else {
        i = 1;
      } 
      if (paramInt == j) {
        paramInt = 0;
      } else {
        paramInt = arrayOfInt.length - 2;
      } 
      if ((arrayOfInt[paramInt + 1] >>> 26 & 0x3F) != i)
        bool = true; 
      return bool;
    } 
    for (i = 0; i < arrayOfInt.length; i += 2) {
      if (paramInt - j == arrayOfInt[i])
        return true; 
    } 
    return false;
  }
  
  public boolean isRtlCharAt(int paramInt) {
    int i = getLineForOffset(paramInt);
    Directions directions1 = getLineDirections(i);
    Directions directions2 = DIRS_ALL_LEFT_TO_RIGHT;
    boolean bool = false;
    if (directions1 == directions2)
      return false; 
    if (directions1 == DIRS_ALL_RIGHT_TO_LEFT)
      return true; 
    int[] arrayOfInt = directions1.mDirections;
    int j = getLineStart(i);
    for (i = 0; i < arrayOfInt.length; i += 2) {
      int k = arrayOfInt[i] + j;
      int m = arrayOfInt[i + 1];
      if (paramInt >= k && paramInt < (m & 0x3FFFFFF) + k) {
        paramInt = arrayOfInt[i + 1];
        if ((paramInt >>> 26 & 0x3F & 0x1) != 0)
          bool = true; 
        return bool;
      } 
    } 
    return false;
  }
  
  public long getRunRange(int paramInt) {
    int i = getLineForOffset(paramInt);
    Directions directions = getLineDirections(i);
    if (directions == DIRS_ALL_LEFT_TO_RIGHT || directions == DIRS_ALL_RIGHT_TO_LEFT)
      return TextUtils.packRangeInLong(0, getLineEnd(i)); 
    int[] arrayOfInt = directions.mDirections;
    int j = getLineStart(i);
    for (byte b = 0; b < arrayOfInt.length; b += 2) {
      int k = arrayOfInt[b] + j;
      int m = (arrayOfInt[b + 1] & 0x3FFFFFF) + k;
      if (paramInt >= k && paramInt < m)
        return TextUtils.packRangeInLong(k, m); 
    } 
    return TextUtils.packRangeInLong(0, getLineEnd(i));
  }
  
  public boolean primaryIsTrailingPrevious(int paramInt) {
    boolean bool;
    int n, i = getLineForOffset(paramInt);
    int j = getLineStart(i);
    int k = getLineEnd(i);
    int[] arrayOfInt = (getLineDirections(i)).mDirections;
    byte b = -1;
    int m = 0;
    while (true) {
      int i1 = arrayOfInt.length;
      bool = false;
      n = b;
      if (m < i1) {
        int i2 = arrayOfInt[m] + j;
        i1 = (arrayOfInt[m + 1] & 0x3FFFFFF) + i2;
        n = i1;
        if (i1 > k)
          n = k; 
        if (paramInt >= i2 && paramInt < n) {
          if (paramInt > i2)
            return false; 
          n = arrayOfInt[m + 1] >>> 26 & 0x3F;
          break;
        } 
        m += 2;
        continue;
      } 
      break;
    } 
    m = n;
    if (n == -1) {
      if (getParagraphDirection(i) == 1) {
        n = 0;
      } else {
        n = 1;
      } 
      m = n;
    } 
    b = -1;
    if (paramInt == j) {
      if (getParagraphDirection(i) == 1) {
        paramInt = 0;
      } else {
        paramInt = 1;
      } 
    } else {
      i = paramInt - 1;
      n = 0;
      while (true) {
        paramInt = b;
        if (n < arrayOfInt.length) {
          int i2 = arrayOfInt[n] + j;
          int i1 = (arrayOfInt[n + 1] & 0x3FFFFFF) + i2;
          paramInt = i1;
          if (i1 > k)
            paramInt = k; 
          if (i >= i2 && i < paramInt) {
            paramInt = arrayOfInt[n + 1] >>> 26 & 0x3F;
            break;
          } 
          n += 2;
          continue;
        } 
        break;
      } 
    } 
    if (paramInt < m)
      bool = true; 
    return bool;
  }
  
  public boolean[] primaryIsTrailingPreviousAllLineOffsets(int paramInt) {
    int i = getLineStart(paramInt);
    int j = getLineEnd(paramInt);
    int[] arrayOfInt = (getLineDirections(paramInt)).mDirections;
    boolean[] arrayOfBoolean = new boolean[j - i + 1];
    byte[] arrayOfByte = new byte[j - i + 1];
    byte b;
    for (b = 0; b < arrayOfInt.length; b += 2) {
      int k = arrayOfInt[b] + i;
      int m = (arrayOfInt[b + 1] & 0x3FFFFFF) + k;
      int n = m;
      if (m > j)
        n = j; 
      if (n != k)
        arrayOfByte[n - i - 1] = (byte)(arrayOfInt[b + 1] >>> 26 & 0x3F); 
    } 
    for (byte b1 = 0; b1 < arrayOfInt.length; b1 += 2) {
      j = arrayOfInt[b1] + i;
      byte b2 = (byte)(arrayOfInt[b1 + 1] >>> 26 & 0x3F);
      boolean bool = false;
      if (j == i) {
        if (getParagraphDirection(paramInt) == 1) {
          b = 0;
        } else {
          b = 1;
        } 
      } else {
        b = arrayOfByte[j - i - 1];
      } 
      if (b2 > b)
        bool = true; 
      arrayOfBoolean[j - i] = bool;
    } 
    return arrayOfBoolean;
  }
  
  public float getPrimaryHorizontal(int paramInt) {
    return getPrimaryHorizontal(paramInt, false);
  }
  
  public float getPrimaryHorizontal(int paramInt, boolean paramBoolean) {
    boolean bool = primaryIsTrailingPrevious(paramInt);
    return getHorizontal(paramInt, bool, paramBoolean);
  }
  
  public float getSecondaryHorizontal(int paramInt) {
    return getSecondaryHorizontal(paramInt, false);
  }
  
  public float getSecondaryHorizontal(int paramInt, boolean paramBoolean) {
    boolean bool = primaryIsTrailingPrevious(paramInt);
    return getHorizontal(paramInt, bool ^ true, paramBoolean);
  }
  
  private float getHorizontal(int paramInt, boolean paramBoolean) {
    float f;
    if (paramBoolean) {
      f = getPrimaryHorizontal(paramInt);
    } else {
      f = getSecondaryHorizontal(paramInt);
    } 
    return f;
  }
  
  private float getHorizontal(int paramInt, boolean paramBoolean1, boolean paramBoolean2) {
    int i = getLineForOffset(paramInt);
    return getHorizontal(paramInt, paramBoolean1, i, paramBoolean2);
  }
  
  private float getHorizontal(int paramInt1, boolean paramBoolean1, int paramInt2, boolean paramBoolean2) {
    // Byte code:
    //   0: aload_0
    //   1: iload_3
    //   2: invokevirtual getLineStart : (I)I
    //   5: istore #5
    //   7: aload_0
    //   8: iload_3
    //   9: invokevirtual getLineEnd : (I)I
    //   12: istore #6
    //   14: aload_0
    //   15: iload_3
    //   16: invokevirtual getParagraphDirection : (I)I
    //   19: istore #7
    //   21: aload_0
    //   22: iload_3
    //   23: invokevirtual getLineContainsTab : (I)Z
    //   26: istore #8
    //   28: aload_0
    //   29: iload_3
    //   30: invokevirtual getLineDirections : (I)Landroid/text/Layout$Directions;
    //   33: astore #9
    //   35: iload #8
    //   37: ifeq -> 96
    //   40: aload_0
    //   41: getfield mText : Ljava/lang/CharSequence;
    //   44: astore #10
    //   46: aload #10
    //   48: instanceof android/text/Spanned
    //   51: ifeq -> 96
    //   54: aload #10
    //   56: checkcast android/text/Spanned
    //   59: iload #5
    //   61: iload #6
    //   63: ldc_w android/text/style/TabStopSpan
    //   66: invokestatic getParagraphSpans : (Landroid/text/Spanned;IILjava/lang/Class;)[Ljava/lang/Object;
    //   69: checkcast [Landroid/text/style/TabStopSpan;
    //   72: astore #10
    //   74: aload #10
    //   76: arraylength
    //   77: ifle -> 96
    //   80: new android/text/Layout$TabStops
    //   83: dup
    //   84: ldc 20.0
    //   86: aload #10
    //   88: invokespecial <init> : (F[Ljava/lang/Object;)V
    //   91: astore #10
    //   93: goto -> 99
    //   96: aconst_null
    //   97: astore #10
    //   99: invokestatic obtain : ()Landroid/text/TextLine;
    //   102: astore #11
    //   104: aload_0
    //   105: getfield mPaint : Landroid/text/TextPaint;
    //   108: astore #12
    //   110: aload_0
    //   111: getfield mText : Ljava/lang/CharSequence;
    //   114: astore #13
    //   116: aload_0
    //   117: iload_3
    //   118: invokevirtual getEllipsisStart : (I)I
    //   121: istore #14
    //   123: aload_0
    //   124: iload_3
    //   125: invokevirtual getEllipsisStart : (I)I
    //   128: istore #15
    //   130: aload_0
    //   131: iload_3
    //   132: invokevirtual getEllipsisCount : (I)I
    //   135: istore #16
    //   137: aload #11
    //   139: aload #12
    //   141: aload #13
    //   143: iload #5
    //   145: iload #6
    //   147: iload #7
    //   149: aload #9
    //   151: iload #8
    //   153: aload #10
    //   155: iload #14
    //   157: iload #15
    //   159: iload #16
    //   161: iadd
    //   162: invokevirtual set : (Landroid/text/TextPaint;Ljava/lang/CharSequence;IIILandroid/text/Layout$Directions;ZLandroid/text/Layout$TabStops;II)V
    //   165: aload #11
    //   167: iload_1
    //   168: iload #5
    //   170: isub
    //   171: iload_2
    //   172: aconst_null
    //   173: invokevirtual measure : (IZLandroid/graphics/Paint$FontMetricsInt;)F
    //   176: fstore #17
    //   178: aload #11
    //   180: invokestatic recycle : (Landroid/text/TextLine;)Landroid/text/TextLine;
    //   183: pop
    //   184: fload #17
    //   186: fstore #18
    //   188: iload #4
    //   190: ifeq -> 214
    //   193: aload_0
    //   194: getfield mWidth : I
    //   197: istore_1
    //   198: fload #17
    //   200: fstore #18
    //   202: fload #17
    //   204: iload_1
    //   205: i2f
    //   206: fcmpl
    //   207: ifle -> 214
    //   210: iload_1
    //   211: i2f
    //   212: fstore #18
    //   214: aload_0
    //   215: iload_3
    //   216: invokevirtual getParagraphLeft : (I)I
    //   219: istore #6
    //   221: aload_0
    //   222: iload_3
    //   223: invokevirtual getParagraphRight : (I)I
    //   226: istore_1
    //   227: aload_0
    //   228: iload_3
    //   229: iload #6
    //   231: iload_1
    //   232: invokespecial getLineStartPos : (III)I
    //   235: i2f
    //   236: fload #18
    //   238: fadd
    //   239: freturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1194	-> 0
    //   #1195	-> 7
    //   #1196	-> 14
    //   #1197	-> 21
    //   #1198	-> 28
    //   #1200	-> 35
    //   #1201	-> 35
    //   #1204	-> 54
    //   #1205	-> 74
    //   #1206	-> 80
    //   #1210	-> 96
    //   #1211	-> 104
    //   #1212	-> 116
    //   #1211	-> 137
    //   #1213	-> 165
    //   #1214	-> 178
    //   #1216	-> 184
    //   #1217	-> 210
    //   #1219	-> 214
    //   #1220	-> 221
    //   #1222	-> 227
  }
  
  private float[] getLineHorizontals(int paramInt, boolean paramBoolean1, boolean paramBoolean2) {
    // Byte code:
    //   0: aload_0
    //   1: iload_1
    //   2: invokevirtual getLineStart : (I)I
    //   5: istore #4
    //   7: aload_0
    //   8: iload_1
    //   9: invokevirtual getLineEnd : (I)I
    //   12: istore #5
    //   14: aload_0
    //   15: iload_1
    //   16: invokevirtual getParagraphDirection : (I)I
    //   19: istore #6
    //   21: aload_0
    //   22: iload_1
    //   23: invokevirtual getLineContainsTab : (I)Z
    //   26: istore #7
    //   28: aload_0
    //   29: iload_1
    //   30: invokevirtual getLineDirections : (I)Landroid/text/Layout$Directions;
    //   33: astore #8
    //   35: iload #7
    //   37: ifeq -> 96
    //   40: aload_0
    //   41: getfield mText : Ljava/lang/CharSequence;
    //   44: astore #9
    //   46: aload #9
    //   48: instanceof android/text/Spanned
    //   51: ifeq -> 96
    //   54: aload #9
    //   56: checkcast android/text/Spanned
    //   59: iload #4
    //   61: iload #5
    //   63: ldc_w android/text/style/TabStopSpan
    //   66: invokestatic getParagraphSpans : (Landroid/text/Spanned;IILjava/lang/Class;)[Ljava/lang/Object;
    //   69: checkcast [Landroid/text/style/TabStopSpan;
    //   72: astore #9
    //   74: aload #9
    //   76: arraylength
    //   77: ifle -> 96
    //   80: new android/text/Layout$TabStops
    //   83: dup
    //   84: ldc 20.0
    //   86: aload #9
    //   88: invokespecial <init> : (F[Ljava/lang/Object;)V
    //   91: astore #9
    //   93: goto -> 99
    //   96: aconst_null
    //   97: astore #9
    //   99: invokestatic obtain : ()Landroid/text/TextLine;
    //   102: astore #10
    //   104: aload_0
    //   105: getfield mPaint : Landroid/text/TextPaint;
    //   108: astore #11
    //   110: aload_0
    //   111: getfield mText : Ljava/lang/CharSequence;
    //   114: astore #12
    //   116: aload_0
    //   117: iload_1
    //   118: invokevirtual getEllipsisStart : (I)I
    //   121: istore #13
    //   123: aload_0
    //   124: iload_1
    //   125: invokevirtual getEllipsisStart : (I)I
    //   128: istore #14
    //   130: aload_0
    //   131: iload_1
    //   132: invokevirtual getEllipsisCount : (I)I
    //   135: istore #15
    //   137: aload #10
    //   139: aload #11
    //   141: aload #12
    //   143: iload #4
    //   145: iload #5
    //   147: iload #6
    //   149: aload #8
    //   151: iload #7
    //   153: aload #9
    //   155: iload #13
    //   157: iload #14
    //   159: iload #15
    //   161: iadd
    //   162: invokevirtual set : (Landroid/text/TextPaint;Ljava/lang/CharSequence;IIILandroid/text/Layout$Directions;ZLandroid/text/Layout$TabStops;II)V
    //   165: aload_0
    //   166: iload_1
    //   167: invokevirtual primaryIsTrailingPreviousAllLineOffsets : (I)[Z
    //   170: astore #9
    //   172: iload_3
    //   173: ifne -> 205
    //   176: iconst_0
    //   177: istore #15
    //   179: iload #15
    //   181: aload #9
    //   183: arraylength
    //   184: if_icmpge -> 205
    //   187: aload #9
    //   189: iload #15
    //   191: aload #9
    //   193: iload #15
    //   195: baload
    //   196: iconst_1
    //   197: ixor
    //   198: bastore
    //   199: iinc #15, 1
    //   202: goto -> 179
    //   205: aload #10
    //   207: aload #9
    //   209: aconst_null
    //   210: invokevirtual measureAllOffsets : ([ZLandroid/graphics/Paint$FontMetricsInt;)[F
    //   213: astore #9
    //   215: aload #10
    //   217: invokestatic recycle : (Landroid/text/TextLine;)Landroid/text/TextLine;
    //   220: pop
    //   221: iload_2
    //   222: ifeq -> 272
    //   225: iconst_0
    //   226: istore #15
    //   228: iload #15
    //   230: aload #9
    //   232: arraylength
    //   233: if_icmpge -> 272
    //   236: aload #9
    //   238: iload #15
    //   240: faload
    //   241: fstore #16
    //   243: aload_0
    //   244: getfield mWidth : I
    //   247: istore #14
    //   249: fload #16
    //   251: iload #14
    //   253: i2f
    //   254: fcmpl
    //   255: ifle -> 266
    //   258: aload #9
    //   260: iload #15
    //   262: iload #14
    //   264: i2f
    //   265: fastore
    //   266: iinc #15, 1
    //   269: goto -> 228
    //   272: aload_0
    //   273: iload_1
    //   274: invokevirtual getParagraphLeft : (I)I
    //   277: istore #15
    //   279: aload_0
    //   280: iload_1
    //   281: invokevirtual getParagraphRight : (I)I
    //   284: istore #14
    //   286: aload_0
    //   287: iload_1
    //   288: iload #15
    //   290: iload #14
    //   292: invokespecial getLineStartPos : (III)I
    //   295: istore #15
    //   297: iload #5
    //   299: iload #4
    //   301: isub
    //   302: iconst_1
    //   303: iadd
    //   304: newarray float
    //   306: astore #8
    //   308: iconst_0
    //   309: istore_1
    //   310: iload_1
    //   311: aload #8
    //   313: arraylength
    //   314: if_icmpge -> 335
    //   317: aload #8
    //   319: iload_1
    //   320: iload #15
    //   322: i2f
    //   323: aload #9
    //   325: iload_1
    //   326: faload
    //   327: fadd
    //   328: fastore
    //   329: iinc #1, 1
    //   332: goto -> 310
    //   335: aload #8
    //   337: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1234	-> 0
    //   #1235	-> 7
    //   #1236	-> 14
    //   #1237	-> 21
    //   #1238	-> 28
    //   #1240	-> 35
    //   #1241	-> 35
    //   #1244	-> 54
    //   #1245	-> 74
    //   #1246	-> 80
    //   #1250	-> 96
    //   #1251	-> 104
    //   #1252	-> 116
    //   #1251	-> 137
    //   #1253	-> 165
    //   #1254	-> 172
    //   #1255	-> 176
    //   #1256	-> 187
    //   #1255	-> 199
    //   #1259	-> 205
    //   #1260	-> 215
    //   #1262	-> 221
    //   #1263	-> 225
    //   #1264	-> 236
    //   #1265	-> 258
    //   #1263	-> 266
    //   #1269	-> 272
    //   #1270	-> 279
    //   #1272	-> 286
    //   #1273	-> 297
    //   #1274	-> 308
    //   #1275	-> 317
    //   #1274	-> 329
    //   #1277	-> 335
  }
  
  public float getLineLeft(int paramInt) {
    int i = getParagraphDirection(paramInt);
    Alignment alignment1 = getParagraphAlignment(paramInt);
    Alignment alignment2 = alignment1;
    if (alignment1 == null)
      alignment2 = Alignment.ALIGN_CENTER; 
    int j = null.$SwitchMap$android$text$Layout$Alignment[alignment2.ordinal()];
    if (j != 1) {
      if (j != 2) {
        if (j != 3) {
          if (j != 4) {
            alignment2 = Alignment.ALIGN_LEFT;
          } else {
            alignment2 = Alignment.ALIGN_RIGHT;
          } 
        } else {
          alignment2 = Alignment.ALIGN_CENTER;
        } 
      } else if (i == -1) {
        alignment2 = Alignment.ALIGN_LEFT;
      } else {
        alignment2 = Alignment.ALIGN_RIGHT;
      } 
    } else if (i == -1) {
      alignment2 = Alignment.ALIGN_RIGHT;
    } else {
      alignment2 = Alignment.ALIGN_LEFT;
    } 
    i = null.$SwitchMap$android$text$Layout$Alignment[alignment2.ordinal()];
    if (i != 3) {
      if (i != 4)
        return 0.0F; 
      return this.mWidth - getLineMax(paramInt);
    } 
    i = getParagraphLeft(paramInt);
    float f = getLineMax(paramInt);
    return (float)Math.floor((i + (this.mWidth - f) / 2.0F));
  }
  
  public float getLineRight(int paramInt) {
    int i = getParagraphDirection(paramInt);
    Alignment alignment1 = getParagraphAlignment(paramInt);
    Alignment alignment2 = alignment1;
    if (alignment1 == null)
      alignment2 = Alignment.ALIGN_CENTER; 
    int j = null.$SwitchMap$android$text$Layout$Alignment[alignment2.ordinal()];
    if (j != 1) {
      if (j != 2) {
        if (j != 3) {
          if (j != 4) {
            alignment2 = Alignment.ALIGN_LEFT;
          } else {
            alignment2 = Alignment.ALIGN_RIGHT;
          } 
        } else {
          alignment2 = Alignment.ALIGN_CENTER;
        } 
      } else if (i == -1) {
        alignment2 = Alignment.ALIGN_LEFT;
      } else {
        alignment2 = Alignment.ALIGN_RIGHT;
      } 
    } else if (i == -1) {
      alignment2 = Alignment.ALIGN_RIGHT;
    } else {
      alignment2 = Alignment.ALIGN_LEFT;
    } 
    j = null.$SwitchMap$android$text$Layout$Alignment[alignment2.ordinal()];
    if (j != 3) {
      if (j != 4)
        return getLineMax(paramInt); 
      return this.mWidth;
    } 
    j = getParagraphRight(paramInt);
    float f = getLineMax(paramInt);
    return (float)Math.ceil((j - (this.mWidth - f) / 2.0F));
  }
  
  public float getLineMax(int paramInt) {
    float f1 = getParagraphLeadingMargin(paramInt);
    float f2 = getLineExtent(paramInt, false);
    if (f2 < 0.0F)
      f2 = -f2; 
    return f2 + f1;
  }
  
  public float getLineWidth(int paramInt) {
    float f1 = getParagraphLeadingMargin(paramInt);
    float f2 = getLineExtent(paramInt, true);
    if (f2 < 0.0F)
      f2 = -f2; 
    return f2 + f1;
  }
  
  private float getLineExtent(int paramInt, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: iload_1
    //   2: invokevirtual getLineStart : (I)I
    //   5: istore_3
    //   6: iload_2
    //   7: ifeq -> 20
    //   10: aload_0
    //   11: iload_1
    //   12: invokevirtual getLineEnd : (I)I
    //   15: istore #4
    //   17: goto -> 27
    //   20: aload_0
    //   21: iload_1
    //   22: invokevirtual getLineVisibleEnd : (I)I
    //   25: istore #4
    //   27: aload_0
    //   28: iload_1
    //   29: invokevirtual getLineContainsTab : (I)Z
    //   32: istore_2
    //   33: iload_2
    //   34: ifeq -> 92
    //   37: aload_0
    //   38: getfield mText : Ljava/lang/CharSequence;
    //   41: astore #5
    //   43: aload #5
    //   45: instanceof android/text/Spanned
    //   48: ifeq -> 92
    //   51: aload #5
    //   53: checkcast android/text/Spanned
    //   56: iload_3
    //   57: iload #4
    //   59: ldc_w android/text/style/TabStopSpan
    //   62: invokestatic getParagraphSpans : (Landroid/text/Spanned;IILjava/lang/Class;)[Ljava/lang/Object;
    //   65: checkcast [Landroid/text/style/TabStopSpan;
    //   68: astore #5
    //   70: aload #5
    //   72: arraylength
    //   73: ifle -> 92
    //   76: new android/text/Layout$TabStops
    //   79: dup
    //   80: ldc 20.0
    //   82: aload #5
    //   84: invokespecial <init> : (F[Ljava/lang/Object;)V
    //   87: astore #5
    //   89: goto -> 95
    //   92: aconst_null
    //   93: astore #5
    //   95: aload_0
    //   96: iload_1
    //   97: invokevirtual getLineDirections : (I)Landroid/text/Layout$Directions;
    //   100: astore #6
    //   102: aload #6
    //   104: ifnonnull -> 109
    //   107: fconst_0
    //   108: freturn
    //   109: aload_0
    //   110: iload_1
    //   111: invokevirtual getParagraphDirection : (I)I
    //   114: istore #7
    //   116: invokestatic obtain : ()Landroid/text/TextLine;
    //   119: astore #8
    //   121: aload_0
    //   122: getfield mWorkPaint : Landroid/text/TextPaint;
    //   125: astore #9
    //   127: aload #9
    //   129: aload_0
    //   130: getfield mPaint : Landroid/text/TextPaint;
    //   133: invokevirtual set : (Landroid/text/TextPaint;)V
    //   136: aload #9
    //   138: aload_0
    //   139: iload_1
    //   140: invokevirtual getStartHyphenEdit : (I)I
    //   143: invokevirtual setStartHyphenEdit : (I)V
    //   146: aload #9
    //   148: aload_0
    //   149: iload_1
    //   150: invokevirtual getEndHyphenEdit : (I)I
    //   153: invokevirtual setEndHyphenEdit : (I)V
    //   156: aload_0
    //   157: getfield mText : Ljava/lang/CharSequence;
    //   160: astore #10
    //   162: aload_0
    //   163: iload_1
    //   164: invokevirtual getEllipsisStart : (I)I
    //   167: istore #11
    //   169: aload_0
    //   170: iload_1
    //   171: invokevirtual getEllipsisStart : (I)I
    //   174: istore #12
    //   176: aload_0
    //   177: iload_1
    //   178: invokevirtual getEllipsisCount : (I)I
    //   181: istore #13
    //   183: aload #8
    //   185: aload #9
    //   187: aload #10
    //   189: iload_3
    //   190: iload #4
    //   192: iload #7
    //   194: aload #6
    //   196: iload_2
    //   197: aload #5
    //   199: iload #11
    //   201: iload #12
    //   203: iload #13
    //   205: iadd
    //   206: invokevirtual set : (Landroid/text/TextPaint;Ljava/lang/CharSequence;IIILandroid/text/Layout$Directions;ZLandroid/text/Layout$TabStops;II)V
    //   209: aload_0
    //   210: iload_1
    //   211: invokespecial isJustificationRequired : (I)Z
    //   214: ifeq -> 227
    //   217: aload #8
    //   219: aload_0
    //   220: iload_1
    //   221: invokespecial getJustifyWidth : (I)F
    //   224: invokevirtual justify : (F)V
    //   227: aload #8
    //   229: aconst_null
    //   230: invokevirtual metrics : (Landroid/graphics/Paint$FontMetricsInt;)F
    //   233: fstore #14
    //   235: aload #8
    //   237: invokestatic recycle : (Landroid/text/TextLine;)Landroid/text/TextLine;
    //   240: pop
    //   241: fload #14
    //   243: freturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1411	-> 0
    //   #1412	-> 6
    //   #1414	-> 27
    //   #1415	-> 33
    //   #1416	-> 33
    //   #1419	-> 51
    //   #1420	-> 70
    //   #1421	-> 76
    //   #1424	-> 92
    //   #1426	-> 102
    //   #1427	-> 107
    //   #1429	-> 109
    //   #1431	-> 116
    //   #1432	-> 121
    //   #1433	-> 127
    //   #1434	-> 136
    //   #1435	-> 146
    //   #1436	-> 156
    //   #1437	-> 162
    //   #1436	-> 183
    //   #1438	-> 209
    //   #1439	-> 217
    //   #1441	-> 227
    //   #1442	-> 235
    //   #1443	-> 241
  }
  
  private float getLineExtent(int paramInt, TabStops paramTabStops, boolean paramBoolean) {
    int j, i = getLineStart(paramInt);
    if (paramBoolean) {
      j = getLineEnd(paramInt);
    } else {
      j = getLineVisibleEnd(paramInt);
    } 
    paramBoolean = getLineContainsTab(paramInt);
    Directions directions = getLineDirections(paramInt);
    int k = getParagraphDirection(paramInt);
    TextLine textLine = TextLine.obtain();
    TextPaint textPaint = this.mWorkPaint;
    textPaint.set(this.mPaint);
    textPaint.setStartHyphenEdit(getStartHyphenEdit(paramInt));
    textPaint.setEndHyphenEdit(getEndHyphenEdit(paramInt));
    CharSequence charSequence = this.mText;
    int m = getEllipsisStart(paramInt), n = getEllipsisStart(paramInt), i1 = getEllipsisCount(paramInt);
    textLine.set(textPaint, charSequence, i, j, k, directions, paramBoolean, paramTabStops, m, n + i1);
    if (isJustificationRequired(paramInt))
      textLine.justify(getJustifyWidth(paramInt)); 
    float f = textLine.metrics(null);
    TextLine.recycle(textLine);
    return f;
  }
  
  public int getLineForVertical(int paramInt) {
    int i = getLineCount(), j = -1;
    while (i - j > 1) {
      int k = (i + j) / 2;
      if (getLineTop(k) > paramInt) {
        i = k;
        continue;
      } 
      j = k;
    } 
    if (j < 0)
      return 0; 
    return j;
  }
  
  public int getLineForOffset(int paramInt) {
    int i = getLineCount(), j = -1;
    while (i - j > 1) {
      int k = (i + j) / 2;
      if (getLineStart(k) > paramInt) {
        i = k;
        continue;
      } 
      j = k;
    } 
    if (j < 0)
      return 0; 
    return j;
  }
  
  public int getOffsetForHorizontal(int paramInt, float paramFloat) {
    return getOffsetForHorizontal(paramInt, paramFloat, true);
  }
  
  public int getOffsetForHorizontal(int paramInt, float paramFloat, boolean paramBoolean) {
    int i = getLineEnd(paramInt);
    int j = getLineStart(paramInt);
    Directions directions1 = getLineDirections(paramInt);
    TextLine textLine = TextLine.obtain();
    TextPaint textPaint = this.mPaint;
    CharSequence charSequence = this.mText;
    int k = getParagraphDirection(paramInt);
    int m = getEllipsisStart(paramInt), n = getEllipsisStart(paramInt), i1 = getEllipsisCount(paramInt);
    Directions directions2 = directions1;
    textLine.set(textPaint, charSequence, j, i, k, directions1, false, null, m, n + i1);
    HorizontalMeasurementProvider horizontalMeasurementProvider = new HorizontalMeasurementProvider(paramInt, paramBoolean);
    if (paramInt == getLineCount() - 1) {
      paramInt = i;
    } else {
      paramBoolean = isRtlCharAt(i - 1);
      paramInt = textLine.getOffsetToLeftRightOf(i - j, paramBoolean ^ true) + j;
    } 
    i = j;
    float f = Math.abs(horizontalMeasurementProvider.get(j) - paramFloat);
    for (k = 0; k < directions2.mDirections.length; k += 2) {
      float f1;
      int i2 = directions2.mDirections[k] + j;
      i1 = (directions2.mDirections[k + 1] & 0x3FFFFFF) + i2;
      if ((directions2.mDirections[k + 1] & 0x4000000) != 0) {
        paramBoolean = true;
      } else {
        paramBoolean = false;
      } 
      if (paramBoolean) {
        m = -1;
      } else {
        m = 1;
      } 
      n = i1;
      if (i1 > paramInt)
        n = paramInt; 
      int i3 = n - 1 + 1, i4 = i2 + 1 - 1;
      i1 = m;
      m = i4;
      while (i3 - m > 1) {
        i4 = (i3 + m) / 2;
        int i5 = getOffsetAtStartOf(i4);
        if (horizontalMeasurementProvider.get(i5) * i1 >= i1 * paramFloat) {
          i3 = i4;
          continue;
        } 
        m = i4;
      } 
      i1 = m;
      if (m < i2 + 1)
        i1 = i2 + 1; 
      if (i1 < n) {
        i4 = textLine.getOffsetToLeftRightOf(i1 - j, paramBoolean) + j;
        if (!paramBoolean) {
          paramBoolean = true;
        } else {
          paramBoolean = false;
        } 
        i3 = textLine.getOffsetToLeftRightOf(i4 - j, paramBoolean) + j;
        i1 = i;
        f1 = f;
        if (i3 >= i2) {
          i1 = i;
          f1 = f;
          if (i3 < n) {
            f1 = Math.abs(horizontalMeasurementProvider.get(i3) - paramFloat);
            float f3 = f1;
            m = i3;
            if (i4 < n) {
              float f4 = Math.abs(horizontalMeasurementProvider.get(i4) - paramFloat);
              f3 = f1;
              m = i3;
              if (f4 < f1) {
                f3 = f4;
                m = i4;
              } 
            } 
            i1 = i;
            f1 = f;
            if (f3 < f) {
              i1 = m;
              f1 = f3;
            } 
          } 
        } 
      } else {
        f1 = f;
        i1 = i;
      } 
      float f2 = Math.abs(horizontalMeasurementProvider.get(i2) - paramFloat);
      i = i1;
      f = f1;
      if (f2 < f1) {
        f = f2;
        i = i2;
      } 
    } 
    paramFloat = Math.abs(horizontalMeasurementProvider.get(paramInt) - paramFloat);
    if (paramFloat <= f)
      i = paramInt; 
    TextLine.recycle(textLine);
    return i;
  }
  
  private class HorizontalMeasurementProvider {
    private float[] mHorizontals;
    
    private final int mLine;
    
    private int mLineStartOffset;
    
    private final boolean mPrimary;
    
    final Layout this$0;
    
    HorizontalMeasurementProvider(int param1Int, boolean param1Boolean) {
      this.mLine = param1Int;
      this.mPrimary = param1Boolean;
      init();
    }
    
    private void init() {
      Layout.Directions directions = Layout.this.getLineDirections(this.mLine);
      if (directions == Layout.DIRS_ALL_LEFT_TO_RIGHT)
        return; 
      this.mHorizontals = Layout.this.getLineHorizontals(this.mLine, false, this.mPrimary);
      this.mLineStartOffset = Layout.this.getLineStart(this.mLine);
    }
    
    float get(int param1Int) {
      int i = param1Int - this.mLineStartOffset;
      float[] arrayOfFloat = this.mHorizontals;
      if (arrayOfFloat == null || i < 0 || i >= arrayOfFloat.length)
        return Layout.this.getHorizontal(param1Int, this.mPrimary); 
      return arrayOfFloat[i];
    }
  }
  
  public final int getLineEnd(int paramInt) {
    return getLineStart(paramInt + 1);
  }
  
  public int getLineVisibleEnd(int paramInt) {
    return getLineVisibleEnd(paramInt, getLineStart(paramInt), getLineStart(paramInt + 1));
  }
  
  private int getLineVisibleEnd(int paramInt1, int paramInt2, int paramInt3) {
    CharSequence charSequence = this.mText;
    int i = paramInt3;
    if (paramInt1 == getLineCount() - 1)
      return paramInt3; 
    for (; i > paramInt2; i--) {
      char c = charSequence.charAt(i - 1);
      if (c == '\n')
        return i - 1; 
      if (!TextLine.isLineEndSpace(c))
        break; 
    } 
    return i;
  }
  
  public final int getLineBottom(int paramInt) {
    return getLineTop(paramInt + 1);
  }
  
  public final int getLineBottomWithoutSpacing(int paramInt) {
    return getLineTop(paramInt + 1) - getLineExtra(paramInt);
  }
  
  public final int getLineBaseline(int paramInt) {
    return getLineTop(paramInt + 1) - getLineDescent(paramInt);
  }
  
  public final int getLineAscent(int paramInt) {
    return getLineTop(paramInt) - getLineTop(paramInt + 1) - getLineDescent(paramInt);
  }
  
  public int getLineExtra(int paramInt) {
    return 0;
  }
  
  public int getOffsetToLeftOf(int paramInt) {
    return getOffsetToLeftRightOf(paramInt, true);
  }
  
  public int getOffsetToRightOf(int paramInt) {
    return getOffsetToLeftRightOf(paramInt, false);
  }
  
  private int getOffsetToLeftRightOf(int paramInt, boolean paramBoolean) {
    int i2, i = getLineForOffset(paramInt);
    int j = getLineStart(i);
    int k = getLineEnd(i);
    int m = getParagraphDirection(i);
    int n = 0;
    int i1 = 0;
    if (m == -1) {
      bool = true;
    } else {
      bool = false;
    } 
    if (paramBoolean == bool)
      i1 = 1; 
    if (i1) {
      i1 = i;
      if (paramInt == k)
        if (i < getLineCount() - 1) {
          n = 1;
          i1 = i + 1;
        } else {
          return paramInt;
        }  
    } else {
      i1 = i;
      if (paramInt == j)
        if (i > 0) {
          n = 1;
          i1 = i - 1;
        } else {
          return paramInt;
        }  
    } 
    boolean bool = paramBoolean;
    i = m;
    if (n) {
      int i3 = getLineStart(i1);
      n = getLineEnd(i1);
      int i4 = getParagraphDirection(i1);
      bool = paramBoolean;
      j = i3;
      k = n;
      i = m;
      if (i4 != m) {
        i2 = paramBoolean ^ true;
        i = i4;
        k = n;
        j = i3;
      } 
    } 
    Directions directions = getLineDirections(i1);
    TextLine textLine = TextLine.obtain();
    TextPaint textPaint = this.mPaint;
    CharSequence charSequence = this.mText;
    m = getEllipsisStart(i1);
    n = getEllipsisStart(i1);
    i1 = getEllipsisCount(i1);
    textLine.set(textPaint, charSequence, j, k, i, directions, false, null, m, n + i1);
    paramInt = textLine.getOffsetToLeftRightOf(paramInt - j, i2);
    TextLine.recycle(textLine);
    return paramInt + j;
  }
  
  private int getOffsetAtStartOf(int paramInt) {
    if (paramInt == 0)
      return 0; 
    CharSequence charSequence = this.mText;
    char c = charSequence.charAt(paramInt);
    int j = paramInt;
    if (c >= '?') {
      j = paramInt;
      if (c <= '?') {
        c = charSequence.charAt(paramInt - 1);
        j = paramInt;
        if (c >= '?') {
          j = paramInt;
          if (c <= '?')
            j = paramInt - 1; 
        } 
      } 
    } 
    int i = j;
    if (this.mSpannedText) {
      ReplacementSpan[] arrayOfReplacementSpan = ((Spanned)charSequence).<ReplacementSpan>getSpans(j, j, ReplacementSpan.class);
      paramInt = 0;
      while (true) {
        i = j;
        if (paramInt < arrayOfReplacementSpan.length) {
          int k = ((Spanned)charSequence).getSpanStart(arrayOfReplacementSpan[paramInt]);
          int m = ((Spanned)charSequence).getSpanEnd(arrayOfReplacementSpan[paramInt]);
          i = j;
          if (k < j) {
            i = j;
            if (m > j)
              i = k; 
          } 
          paramInt++;
          j = i;
          continue;
        } 
        break;
      } 
    } 
    return i;
  }
  
  public boolean shouldClampCursor(int paramInt) {
    int i = null.$SwitchMap$android$text$Layout$Alignment[getParagraphAlignment(paramInt).ordinal()];
    boolean bool = false;
    if (i != 1) {
      if (i != 5)
        return false; 
      return true;
    } 
    if (getParagraphDirection(paramInt) > 0)
      bool = true; 
    return bool;
  }
  
  public void getCursorPath(int paramInt, Path paramPath, CharSequence paramCharSequence) {
    // Byte code:
    //   0: aload_2
    //   1: invokevirtual reset : ()V
    //   4: aload_0
    //   5: iload_1
    //   6: invokevirtual getLineForOffset : (I)I
    //   9: istore #4
    //   11: aload_0
    //   12: iload #4
    //   14: invokevirtual getLineTop : (I)I
    //   17: istore #5
    //   19: aload_0
    //   20: iload #4
    //   22: invokevirtual getLineBottomWithoutSpacing : (I)I
    //   25: istore #6
    //   27: aload_0
    //   28: iload #4
    //   30: invokevirtual shouldClampCursor : (I)Z
    //   33: istore #7
    //   35: aload_0
    //   36: iload_1
    //   37: iload #7
    //   39: invokevirtual getPrimaryHorizontal : (IZ)F
    //   42: ldc_w 0.5
    //   45: fsub
    //   46: fstore #8
    //   48: aload_3
    //   49: iconst_1
    //   50: invokestatic getMetaState : (Ljava/lang/CharSequence;I)I
    //   53: istore_1
    //   54: iload_1
    //   55: aload_3
    //   56: sipush #2048
    //   59: invokestatic getMetaState : (Ljava/lang/CharSequence;I)I
    //   62: ior
    //   63: istore #9
    //   65: aload_3
    //   66: iconst_2
    //   67: invokestatic getMetaState : (Ljava/lang/CharSequence;I)I
    //   70: istore #10
    //   72: iconst_0
    //   73: istore #4
    //   75: iload #9
    //   77: ifne -> 93
    //   80: iload #5
    //   82: istore #11
    //   84: iload #6
    //   86: istore #12
    //   88: iload #10
    //   90: ifeq -> 146
    //   93: iload #6
    //   95: iload #5
    //   97: isub
    //   98: iconst_2
    //   99: ishr
    //   100: istore #13
    //   102: iload #5
    //   104: istore_1
    //   105: iload #10
    //   107: ifeq -> 116
    //   110: iload #5
    //   112: iload #13
    //   114: iadd
    //   115: istore_1
    //   116: iload_1
    //   117: istore #11
    //   119: iload #6
    //   121: istore #12
    //   123: iload #13
    //   125: istore #4
    //   127: iload #9
    //   129: ifeq -> 146
    //   132: iload #6
    //   134: iload #13
    //   136: isub
    //   137: istore #12
    //   139: iload #13
    //   141: istore #4
    //   143: iload_1
    //   144: istore #11
    //   146: fload #8
    //   148: fstore #14
    //   150: fload #8
    //   152: ldc_w 0.5
    //   155: fcmpg
    //   156: ifge -> 164
    //   159: ldc_w 0.5
    //   162: fstore #14
    //   164: aload_2
    //   165: fload #14
    //   167: iload #11
    //   169: i2f
    //   170: invokevirtual moveTo : (FF)V
    //   173: aload_2
    //   174: fload #14
    //   176: iload #12
    //   178: i2f
    //   179: invokevirtual lineTo : (FF)V
    //   182: iload #9
    //   184: iconst_2
    //   185: if_icmpne -> 241
    //   188: aload_2
    //   189: fload #14
    //   191: iload #12
    //   193: i2f
    //   194: invokevirtual moveTo : (FF)V
    //   197: aload_2
    //   198: fload #14
    //   200: iload #4
    //   202: i2f
    //   203: fsub
    //   204: iload #12
    //   206: iload #4
    //   208: iadd
    //   209: i2f
    //   210: invokevirtual lineTo : (FF)V
    //   213: aload_2
    //   214: fload #14
    //   216: iload #12
    //   218: i2f
    //   219: invokevirtual lineTo : (FF)V
    //   222: aload_2
    //   223: iload #4
    //   225: i2f
    //   226: fload #14
    //   228: fadd
    //   229: iload #12
    //   231: iload #4
    //   233: iadd
    //   234: i2f
    //   235: invokevirtual lineTo : (FF)V
    //   238: goto -> 337
    //   241: iload #9
    //   243: iconst_1
    //   244: if_icmpne -> 337
    //   247: aload_2
    //   248: fload #14
    //   250: iload #12
    //   252: i2f
    //   253: invokevirtual moveTo : (FF)V
    //   256: aload_2
    //   257: fload #14
    //   259: iload #4
    //   261: i2f
    //   262: fsub
    //   263: iload #12
    //   265: iload #4
    //   267: iadd
    //   268: i2f
    //   269: invokevirtual lineTo : (FF)V
    //   272: aload_2
    //   273: fload #14
    //   275: iload #4
    //   277: i2f
    //   278: fsub
    //   279: iload #12
    //   281: iload #4
    //   283: iadd
    //   284: i2f
    //   285: ldc_w 0.5
    //   288: fsub
    //   289: invokevirtual moveTo : (FF)V
    //   292: aload_2
    //   293: iload #4
    //   295: i2f
    //   296: fload #14
    //   298: fadd
    //   299: iload #12
    //   301: iload #4
    //   303: iadd
    //   304: i2f
    //   305: ldc_w 0.5
    //   308: fsub
    //   309: invokevirtual lineTo : (FF)V
    //   312: aload_2
    //   313: iload #4
    //   315: i2f
    //   316: fload #14
    //   318: fadd
    //   319: iload #12
    //   321: iload #4
    //   323: iadd
    //   324: i2f
    //   325: invokevirtual moveTo : (FF)V
    //   328: aload_2
    //   329: fload #14
    //   331: iload #12
    //   333: i2f
    //   334: invokevirtual lineTo : (FF)V
    //   337: iload #10
    //   339: iconst_2
    //   340: if_icmpne -> 396
    //   343: aload_2
    //   344: fload #14
    //   346: iload #11
    //   348: i2f
    //   349: invokevirtual moveTo : (FF)V
    //   352: aload_2
    //   353: fload #14
    //   355: iload #4
    //   357: i2f
    //   358: fsub
    //   359: iload #11
    //   361: iload #4
    //   363: isub
    //   364: i2f
    //   365: invokevirtual lineTo : (FF)V
    //   368: aload_2
    //   369: fload #14
    //   371: iload #11
    //   373: i2f
    //   374: invokevirtual lineTo : (FF)V
    //   377: aload_2
    //   378: iload #4
    //   380: i2f
    //   381: fload #14
    //   383: fadd
    //   384: iload #11
    //   386: iload #4
    //   388: isub
    //   389: i2f
    //   390: invokevirtual lineTo : (FF)V
    //   393: goto -> 492
    //   396: iload #10
    //   398: iconst_1
    //   399: if_icmpne -> 492
    //   402: aload_2
    //   403: fload #14
    //   405: iload #11
    //   407: i2f
    //   408: invokevirtual moveTo : (FF)V
    //   411: aload_2
    //   412: fload #14
    //   414: iload #4
    //   416: i2f
    //   417: fsub
    //   418: iload #11
    //   420: iload #4
    //   422: isub
    //   423: i2f
    //   424: invokevirtual lineTo : (FF)V
    //   427: aload_2
    //   428: fload #14
    //   430: iload #4
    //   432: i2f
    //   433: fsub
    //   434: iload #11
    //   436: iload #4
    //   438: isub
    //   439: i2f
    //   440: ldc_w 0.5
    //   443: fadd
    //   444: invokevirtual moveTo : (FF)V
    //   447: aload_2
    //   448: iload #4
    //   450: i2f
    //   451: fload #14
    //   453: fadd
    //   454: iload #11
    //   456: iload #4
    //   458: isub
    //   459: i2f
    //   460: ldc_w 0.5
    //   463: fadd
    //   464: invokevirtual lineTo : (FF)V
    //   467: aload_2
    //   468: iload #4
    //   470: i2f
    //   471: fload #14
    //   473: fadd
    //   474: iload #11
    //   476: iload #4
    //   478: isub
    //   479: i2f
    //   480: invokevirtual moveTo : (FF)V
    //   483: aload_2
    //   484: fload #14
    //   486: iload #11
    //   488: i2f
    //   489: invokevirtual lineTo : (FF)V
    //   492: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1873	-> 0
    //   #1875	-> 4
    //   #1876	-> 11
    //   #1877	-> 19
    //   #1879	-> 27
    //   #1880	-> 35
    //   #1882	-> 48
    //   #1883	-> 54
    //   #1884	-> 65
    //   #1885	-> 72
    //   #1887	-> 75
    //   #1888	-> 93
    //   #1890	-> 102
    //   #1891	-> 110
    //   #1892	-> 116
    //   #1893	-> 132
    //   #1896	-> 146
    //   #1897	-> 159
    //   #1899	-> 164
    //   #1900	-> 173
    //   #1902	-> 182
    //   #1903	-> 188
    //   #1904	-> 197
    //   #1905	-> 213
    //   #1906	-> 222
    //   #1907	-> 241
    //   #1908	-> 247
    //   #1909	-> 256
    //   #1911	-> 272
    //   #1912	-> 292
    //   #1914	-> 312
    //   #1915	-> 328
    //   #1918	-> 337
    //   #1919	-> 343
    //   #1920	-> 352
    //   #1921	-> 368
    //   #1922	-> 377
    //   #1923	-> 396
    //   #1924	-> 402
    //   #1925	-> 411
    //   #1927	-> 427
    //   #1928	-> 447
    //   #1930	-> 467
    //   #1931	-> 483
    //   #1933	-> 492
  }
  
  private void addSelection(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, SelectionRectangleConsumer paramSelectionRectangleConsumer) {
    int i = getLineStart(paramInt1);
    int j = getLineEnd(paramInt1);
    Directions directions = getLineDirections(paramInt1);
    int k = j;
    if (j > i) {
      k = j;
      if (this.mText.charAt(j - 1) == '\n')
        k = j - 1; 
    } 
    for (j = 0; j < directions.mDirections.length; j += 2) {
      int m = directions.mDirections[j] + i;
      int n = (directions.mDirections[j + 1] & 0x3FFFFFF) + m;
      int i1 = n;
      if (n > k)
        i1 = k; 
      if (paramInt2 <= i1 && paramInt3 >= m) {
        n = Math.max(paramInt2, m);
        i1 = Math.min(paramInt3, i1);
        if (n != i1) {
          float f1 = getHorizontal(n, false, paramInt1, false);
          float f2 = getHorizontal(i1, true, paramInt1, false);
          float f3 = Math.min(f1, f2);
          f2 = Math.max(f1, f2);
          if ((directions.mDirections[j + 1] & 0x4000000) != 0) {
            i1 = 0;
          } else {
            i1 = 1;
          } 
          paramSelectionRectangleConsumer.accept(f3, paramInt4, f2, paramInt5, i1);
        } 
      } 
    } 
  }
  
  public void getSelectionPath(int paramInt1, int paramInt2, Path paramPath) {
    paramPath.reset();
    getSelection(paramInt1, paramInt2, new _$$Lambda$Layout$MzjK2UE2G8VG0asK8_KWY3gHAmY(paramPath));
  }
  
  public final void getSelection(int paramInt1, int paramInt2, SelectionRectangleConsumer paramSelectionRectangleConsumer) {
    if (paramInt1 == paramInt2)
      return; 
    if (paramInt2 >= paramInt1) {
      int n = paramInt1;
      paramInt1 = paramInt2;
      paramInt2 = n;
    } 
    int j = getLineForOffset(paramInt2);
    int i = getLineForOffset(paramInt1);
    int k = getLineTop(j);
    int m = getLineBottomWithoutSpacing(i);
    if (j == i) {
      addSelection(j, paramInt2, paramInt1, k, m, paramSelectionRectangleConsumer);
    } else {
      float f = this.mWidth;
      int n = getLineEnd(j);
      m = getLineBottom(j);
      addSelection(j, paramInt2, n, k, m, paramSelectionRectangleConsumer);
      if (getParagraphDirection(j) == -1) {
        paramSelectionRectangleConsumer.accept(getLineLeft(j), k, 0.0F, getLineBottom(j), 0);
      } else {
        paramSelectionRectangleConsumer.accept(getLineRight(j), k, f, getLineBottom(j), 1);
      } 
      for (paramInt2 = j + 1; paramInt2 < i; paramInt2++) {
        k = getLineTop(paramInt2);
        j = getLineBottom(paramInt2);
        if (getParagraphDirection(paramInt2) == -1) {
          paramSelectionRectangleConsumer.accept(0.0F, k, f, j, 0);
        } else {
          paramSelectionRectangleConsumer.accept(0.0F, k, f, j, 1);
        } 
      } 
      paramInt2 = getLineTop(i);
      j = getLineBottomWithoutSpacing(i);
      addSelection(i, getLineStart(i), paramInt1, paramInt2, j, paramSelectionRectangleConsumer);
      if (getParagraphDirection(i) == -1) {
        paramSelectionRectangleConsumer.accept(f, paramInt2, getLineRight(i), j, 0);
      } else {
        paramSelectionRectangleConsumer.accept(0.0F, paramInt2, getLineLeft(i), j, 1);
      } 
    } 
  }
  
  public final Alignment getParagraphAlignment(int paramInt) {
    Alignment alignment1 = this.mAlignment;
    Alignment alignment2 = alignment1;
    if (this.mSpannedText) {
      Spanned spanned = (Spanned)this.mText;
      int i = getLineStart(paramInt);
      paramInt = getLineEnd(paramInt);
      AlignmentSpan[] arrayOfAlignmentSpan = getParagraphSpans(spanned, i, paramInt, AlignmentSpan.class);
      paramInt = arrayOfAlignmentSpan.length;
      alignment2 = alignment1;
      if (paramInt > 0)
        alignment2 = arrayOfAlignmentSpan[paramInt - 1].getAlignment(); 
    } 
    return alignment2;
  }
  
  public final int getParagraphLeft(int paramInt) {
    int i = getParagraphDirection(paramInt);
    if (i == -1 || !this.mSpannedText)
      return 0; 
    return getParagraphLeadingMargin(paramInt);
  }
  
  public final int getParagraphRight(int paramInt) {
    int i = this.mWidth;
    int j = getParagraphDirection(paramInt);
    if (j == 1 || !this.mSpannedText)
      return i; 
    return i - getParagraphLeadingMargin(paramInt);
  }
  
  private int getParagraphLeadingMargin(int paramInt) {
    if (!this.mSpannedText)
      return 0; 
    Spanned spanned = (Spanned)this.mText;
    int i = getLineStart(paramInt);
    int j = getLineEnd(paramInt);
    j = spanned.nextSpanTransition(i, j, LeadingMarginSpan.class);
    LeadingMarginSpan[] arrayOfLeadingMarginSpan = getParagraphSpans(spanned, i, j, LeadingMarginSpan.class);
    if (arrayOfLeadingMarginSpan.length == 0)
      return 0; 
    boolean bool = false;
    if (i == 0 || spanned.charAt(i - 1) == '\n') {
      k = 1;
    } else {
      k = 0;
    } 
    int k;
    for (i = 0; i < arrayOfLeadingMarginSpan.length; i++, k = m) {
      int m = k;
      if (arrayOfLeadingMarginSpan[i] instanceof LeadingMarginSpan.LeadingMarginSpan2) {
        j = spanned.getSpanStart(arrayOfLeadingMarginSpan[i]);
        j = getLineForOffset(j);
        int n = ((LeadingMarginSpan.LeadingMarginSpan2)arrayOfLeadingMarginSpan[i]).getLeadingMarginLineCount();
        if (paramInt < j + n) {
          j = 1;
        } else {
          j = 0;
        } 
        m = k | j;
      } 
    } 
    for (i = 0, paramInt = bool; i < arrayOfLeadingMarginSpan.length; i++) {
      LeadingMarginSpan leadingMarginSpan = arrayOfLeadingMarginSpan[i];
      paramInt += leadingMarginSpan.getLeadingMargin(k);
    } 
    return paramInt;
  }
  
  private static float measurePara(TextPaint paramTextPaint, CharSequence paramCharSequence, int paramInt1, int paramInt2, TextDirectionHeuristic paramTextDirectionHeuristic) {
    // Byte code:
    //   0: aconst_null
    //   1: astore #5
    //   3: invokestatic obtain : ()Landroid/text/TextLine;
    //   6: astore #6
    //   8: aload_1
    //   9: iload_2
    //   10: iload_3
    //   11: aload #4
    //   13: aconst_null
    //   14: invokestatic buildForBidi : (Ljava/lang/CharSequence;IILandroid/text/TextDirectionHeuristic;Landroid/text/MeasuredParagraph;)Landroid/text/MeasuredParagraph;
    //   17: astore #4
    //   19: aload #4
    //   21: invokevirtual getChars : ()[C
    //   24: astore #7
    //   26: aload #7
    //   28: arraylength
    //   29: istore #8
    //   31: aload #4
    //   33: iconst_0
    //   34: iload #8
    //   36: invokevirtual getDirections : (II)Landroid/text/Layout$Directions;
    //   39: astore #9
    //   41: aload #4
    //   43: invokevirtual getParagraphDir : ()I
    //   46: istore #10
    //   48: iconst_0
    //   49: istore #11
    //   51: aconst_null
    //   52: astore #5
    //   54: aload_1
    //   55: instanceof android/text/Spanned
    //   58: istore #12
    //   60: iload #12
    //   62: ifeq -> 140
    //   65: aload_1
    //   66: checkcast android/text/Spanned
    //   69: astore #13
    //   71: aload #13
    //   73: iload_2
    //   74: iload_3
    //   75: ldc_w android/text/style/LeadingMarginSpan
    //   78: invokestatic getParagraphSpans : (Landroid/text/Spanned;IILjava/lang/Class;)[Ljava/lang/Object;
    //   81: checkcast [Landroid/text/style/LeadingMarginSpan;
    //   84: astore #14
    //   86: aload #14
    //   88: arraylength
    //   89: istore #15
    //   91: iconst_0
    //   92: istore #16
    //   94: iconst_0
    //   95: istore #17
    //   97: iload #17
    //   99: iload #15
    //   101: if_icmpge -> 130
    //   104: aload #14
    //   106: iload #17
    //   108: aaload
    //   109: astore #13
    //   111: iload #16
    //   113: aload #13
    //   115: iconst_1
    //   116: invokeinterface getLeadingMargin : (Z)I
    //   121: iadd
    //   122: istore #16
    //   124: iinc #17, 1
    //   127: goto -> 97
    //   130: goto -> 146
    //   133: astore_0
    //   134: aload #4
    //   136: astore_1
    //   137: goto -> 330
    //   140: iconst_0
    //   141: istore #11
    //   143: iconst_0
    //   144: istore #16
    //   146: iconst_0
    //   147: istore #15
    //   149: iload #15
    //   151: iload #8
    //   153: if_icmpge -> 252
    //   156: aload #7
    //   158: iload #15
    //   160: caload
    //   161: bipush #9
    //   163: if_icmpne -> 246
    //   166: aload_1
    //   167: instanceof android/text/Spanned
    //   170: ifeq -> 237
    //   173: aload_1
    //   174: checkcast android/text/Spanned
    //   177: astore #7
    //   179: aload #7
    //   181: iload_2
    //   182: iload_3
    //   183: ldc_w android/text/style/TabStopSpan
    //   186: invokeinterface nextSpanTransition : (IILjava/lang/Class;)I
    //   191: istore #15
    //   193: aload #7
    //   195: iload_2
    //   196: iload #15
    //   198: ldc_w android/text/style/TabStopSpan
    //   201: invokestatic getParagraphSpans : (Landroid/text/Spanned;IILjava/lang/Class;)[Ljava/lang/Object;
    //   204: checkcast [Landroid/text/style/TabStopSpan;
    //   207: astore #7
    //   209: iconst_1
    //   210: istore #11
    //   212: aload #7
    //   214: arraylength
    //   215: ifle -> 234
    //   218: new android/text/Layout$TabStops
    //   221: dup
    //   222: ldc 20.0
    //   224: aload #7
    //   226: invokespecial <init> : (F[Ljava/lang/Object;)V
    //   229: astore #5
    //   231: goto -> 234
    //   234: goto -> 255
    //   237: iconst_1
    //   238: istore #11
    //   240: aconst_null
    //   241: astore #5
    //   243: goto -> 255
    //   246: iinc #15, 1
    //   249: goto -> 149
    //   252: aconst_null
    //   253: astore #5
    //   255: aload #6
    //   257: aload_0
    //   258: aload_1
    //   259: iload_2
    //   260: iload_3
    //   261: iload #10
    //   263: aload #9
    //   265: iload #11
    //   267: aload #5
    //   269: iconst_0
    //   270: iconst_0
    //   271: invokevirtual set : (Landroid/text/TextPaint;Ljava/lang/CharSequence;IIILandroid/text/Layout$Directions;ZLandroid/text/Layout$TabStops;II)V
    //   274: iload #16
    //   276: i2f
    //   277: fstore #18
    //   279: aload #6
    //   281: aconst_null
    //   282: invokevirtual metrics : (Landroid/graphics/Paint$FontMetricsInt;)F
    //   285: invokestatic abs : (F)F
    //   288: fstore #19
    //   290: aload #6
    //   292: invokestatic recycle : (Landroid/text/TextLine;)Landroid/text/TextLine;
    //   295: pop
    //   296: aload #4
    //   298: ifnull -> 306
    //   301: aload #4
    //   303: invokevirtual recycle : ()V
    //   306: fload #18
    //   308: fload #19
    //   310: fadd
    //   311: freturn
    //   312: astore_0
    //   313: aload #4
    //   315: astore_1
    //   316: goto -> 330
    //   319: astore_0
    //   320: aload #4
    //   322: astore_1
    //   323: goto -> 330
    //   326: astore_0
    //   327: aload #5
    //   329: astore_1
    //   330: aload #6
    //   332: invokestatic recycle : (Landroid/text/TextLine;)Landroid/text/TextLine;
    //   335: pop
    //   336: aload_1
    //   337: ifnull -> 344
    //   340: aload_1
    //   341: invokevirtual recycle : ()V
    //   344: aload_0
    //   345: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2146	-> 0
    //   #2147	-> 3
    //   #2149	-> 8
    //   #2150	-> 19
    //   #2151	-> 26
    //   #2152	-> 31
    //   #2153	-> 41
    //   #2154	-> 48
    //   #2155	-> 51
    //   #2157	-> 54
    //   #2158	-> 54
    //   #2159	-> 65
    //   #2160	-> 71
    //   #2162	-> 86
    //   #2163	-> 111
    //   #2162	-> 124
    //   #2186	-> 133
    //   #2158	-> 140
    //   #2166	-> 146
    //   #2167	-> 156
    //   #2168	-> 166
    //   #2169	-> 166
    //   #2170	-> 173
    //   #2171	-> 179
    //   #2173	-> 193
    //   #2175	-> 209
    //   #2176	-> 218
    //   #2175	-> 234
    //   #2178	-> 234
    //   #2169	-> 237
    //   #2166	-> 246
    //   #2182	-> 255
    //   #2184	-> 274
    //   #2186	-> 290
    //   #2187	-> 296
    //   #2188	-> 301
    //   #2184	-> 306
    //   #2186	-> 312
    //   #2187	-> 336
    //   #2188	-> 340
    //   #2190	-> 344
    // Exception table:
    //   from	to	target	type
    //   8	19	326	finally
    //   19	26	319	finally
    //   26	31	319	finally
    //   31	41	319	finally
    //   41	48	319	finally
    //   54	60	319	finally
    //   65	71	133	finally
    //   71	86	133	finally
    //   86	91	133	finally
    //   111	124	133	finally
    //   166	173	133	finally
    //   173	179	133	finally
    //   179	193	133	finally
    //   193	209	133	finally
    //   212	218	133	finally
    //   218	231	133	finally
    //   255	274	312	finally
    //   279	290	312	finally
  }
  
  public static class TabStops {
    private float mIncrement;
    
    private int mNumStops;
    
    private float[] mStops;
    
    public TabStops(float param1Float, Object[] param1ArrayOfObject) {
      reset(param1Float, param1ArrayOfObject);
    }
    
    void reset(float param1Float, Object[] param1ArrayOfObject) {
      this.mIncrement = param1Float;
      int i = 0, j = 0;
      if (param1ArrayOfObject != null) {
        float[] arrayOfFloat = this.mStops;
        for (int k = param1ArrayOfObject.length; i < k; ) {
          Object object = param1ArrayOfObject[i];
          int m = j;
          float[] arrayOfFloat1 = arrayOfFloat;
          if (object instanceof TabStopSpan) {
            if (arrayOfFloat == null) {
              arrayOfFloat1 = new float[10];
            } else {
              arrayOfFloat1 = arrayOfFloat;
              if (j == arrayOfFloat.length) {
                arrayOfFloat1 = new float[j * 2];
                for (m = 0; m < j; m++)
                  arrayOfFloat1[m] = arrayOfFloat[m]; 
              } 
            } 
            arrayOfFloat1[j] = ((TabStopSpan)object).getTabStop();
            m = j + 1;
          } 
          i++;
          j = m;
          arrayOfFloat = arrayOfFloat1;
        } 
        if (j > 1)
          Arrays.sort(arrayOfFloat, 0, j); 
        i = j;
        if (arrayOfFloat != this.mStops) {
          this.mStops = arrayOfFloat;
          i = j;
        } 
      } 
      this.mNumStops = i;
    }
    
    float nextTab(float param1Float) {
      int i = this.mNumStops;
      if (i > 0) {
        float[] arrayOfFloat = this.mStops;
        for (byte b = 0; b < i; b++) {
          float f = arrayOfFloat[b];
          if (f > param1Float)
            return f; 
        } 
      } 
      return nextDefaultStop(param1Float, this.mIncrement);
    }
    
    public static float nextDefaultStop(float param1Float1, float param1Float2) {
      return (int)((param1Float1 + param1Float2) / param1Float2) * param1Float2;
    }
  }
  
  static float nextTab(CharSequence paramCharSequence, int paramInt1, int paramInt2, float paramFloat, Object[] paramArrayOfObject) {
    float f = Float.MAX_VALUE;
    boolean bool = false;
    if (paramCharSequence instanceof Spanned) {
      Object[] arrayOfObject = paramArrayOfObject;
      if (paramArrayOfObject == null) {
        arrayOfObject = getParagraphSpans((Spanned)paramCharSequence, paramInt1, paramInt2, TabStopSpan.class);
        bool = true;
      } 
      for (paramInt1 = 0; paramInt1 < arrayOfObject.length; paramInt1++, f = f1) {
        float f1;
        if (!bool && 
          !(arrayOfObject[paramInt1] instanceof TabStopSpan)) {
          f1 = f;
        } else {
          paramInt2 = ((TabStopSpan)arrayOfObject[paramInt1]).getTabStop();
          f1 = f;
          if (paramInt2 < f) {
            f1 = f;
            if (paramInt2 > paramFloat)
              f1 = paramInt2; 
          } 
        } 
      } 
      if (f != Float.MAX_VALUE)
        return f; 
    } 
    return (int)((paramFloat + 20.0F) / 20.0F) * 20.0F;
  }
  
  protected final boolean isSpanned() {
    return this.mSpannedText;
  }
  
  static <T> T[] getParagraphSpans(Spanned paramSpanned, int paramInt1, int paramInt2, Class<T> paramClass) {
    if (paramInt1 == paramInt2 && paramInt1 > 0)
      return (T[])ArrayUtils.emptyArray(paramClass); 
    if (paramSpanned instanceof SpannableStringBuilder)
      return ((SpannableStringBuilder)paramSpanned).getSpans(paramInt1, paramInt2, paramClass, false); 
    return paramSpanned.getSpans(paramInt1, paramInt2, paramClass);
  }
  
  private void ellipsize(int paramInt1, int paramInt2, int paramInt3, char[] paramArrayOfchar, int paramInt4, TextUtils.TruncateAt paramTruncateAt) {
    int i = getEllipsisCount(paramInt3);
    if (i == 0)
      return; 
    int j = getEllipsisStart(paramInt3);
    int k = getLineStart(paramInt3);
    String str = TextUtils.getEllipsisString(paramTruncateAt);
    int m = str.length();
    if (i >= m) {
      paramInt3 = 1;
    } else {
      paramInt3 = 0;
    } 
    int n = Math.max(0, paramInt1 - j - k);
    i = Math.min(i, paramInt2 - j - k);
    for (paramInt2 = n; paramInt2 < i; paramInt2++) {
      char c;
      if (paramInt3 != 0 && paramInt2 < m) {
        c = str.charAt(paramInt2);
      } else {
        c = '﻿';
      } 
      paramArrayOfchar[paramInt4 + paramInt2 + j + k - paramInt1] = c;
    } 
  }
  
  public static class Directions {
    public int[] mDirections;
    
    public Directions(int[] param1ArrayOfint) {
      this.mDirections = param1ArrayOfint;
    }
    
    public int getRunCount() {
      return this.mDirections.length / 2;
    }
    
    public int getRunStart(int param1Int) {
      return this.mDirections[param1Int * 2];
    }
    
    public int getRunLength(int param1Int) {
      return this.mDirections[param1Int * 2 + 1] & 0x3FFFFFF;
    }
    
    public boolean isRunRtl(int param1Int) {
      int[] arrayOfInt = this.mDirections;
      boolean bool = true;
      if ((arrayOfInt[param1Int * 2 + 1] & 0x4000000) == 0)
        bool = false; 
      return bool;
    }
  }
  
  class Ellipsizer implements CharSequence, GetChars {
    Layout mLayout;
    
    TextUtils.TruncateAt mMethod;
    
    CharSequence mText;
    
    int mWidth;
    
    public Ellipsizer(Layout this$0) {
      this.mText = (CharSequence)this$0;
    }
    
    public char charAt(int param1Int) {
      char[] arrayOfChar = TextUtils.obtain(1);
      getChars(param1Int, param1Int + 1, arrayOfChar, 0);
      char c = arrayOfChar[0];
      TextUtils.recycle(arrayOfChar);
      return c;
    }
    
    public void getChars(int param1Int1, int param1Int2, char[] param1ArrayOfchar, int param1Int3) {
      int i = this.mLayout.getLineForOffset(param1Int1);
      int j = this.mLayout.getLineForOffset(param1Int2);
      TextUtils.getChars(this.mText, param1Int1, param1Int2, param1ArrayOfchar, param1Int3);
      for (; i <= j; i++)
        this.mLayout.ellipsize(param1Int1, param1Int2, i, param1ArrayOfchar, param1Int3, this.mMethod); 
    }
    
    public int length() {
      return this.mText.length();
    }
    
    public CharSequence subSequence(int param1Int1, int param1Int2) {
      char[] arrayOfChar = new char[param1Int2 - param1Int1];
      getChars(param1Int1, param1Int2, arrayOfChar, 0);
      return new String(arrayOfChar);
    }
    
    public String toString() {
      char[] arrayOfChar = new char[length()];
      getChars(0, length(), arrayOfChar, 0);
      return new String(arrayOfChar);
    }
  }
  
  class SpannedEllipsizer extends Ellipsizer implements Spanned {
    private Spanned mSpanned;
    
    public SpannedEllipsizer(Layout this$0) {
      super((CharSequence)this$0);
      this.mSpanned = (Spanned)this$0;
    }
    
    public <T> T[] getSpans(int param1Int1, int param1Int2, Class<T> param1Class) {
      return this.mSpanned.getSpans(param1Int1, param1Int2, param1Class);
    }
    
    public int getSpanStart(Object param1Object) {
      return this.mSpanned.getSpanStart(param1Object);
    }
    
    public int getSpanEnd(Object param1Object) {
      return this.mSpanned.getSpanEnd(param1Object);
    }
    
    public int getSpanFlags(Object param1Object) {
      return this.mSpanned.getSpanFlags(param1Object);
    }
    
    public int nextSpanTransition(int param1Int1, int param1Int2, Class param1Class) {
      return this.mSpanned.nextSpanTransition(param1Int1, param1Int2, param1Class);
    }
    
    public CharSequence subSequence(int param1Int1, int param1Int2) {
      char[] arrayOfChar = new char[param1Int2 - param1Int1];
      getChars(param1Int1, param1Int2, arrayOfChar, 0);
      SpannableString spannableString = new SpannableString(new String(arrayOfChar));
      TextUtils.copySpansFrom(this.mSpanned, param1Int1, param1Int2, Object.class, spannableString, 0);
      return spannableString;
    }
  }
  
  private TextPaint mWorkPaint = new TextPaint();
  
  private Alignment mAlignment = Alignment.ALIGN_NORMAL;
  
  private static final Rect sTempRect = new Rect();
  
  public static final int BREAK_STRATEGY_BALANCED = 2;
  
  public static final int BREAK_STRATEGY_HIGH_QUALITY = 1;
  
  public static final int BREAK_STRATEGY_SIMPLE = 0;
  
  public static final float DEFAULT_LINESPACING_ADDITION = 0.0F;
  
  public static final float DEFAULT_LINESPACING_MULTIPLIER = 1.0F;
  
  public static final Directions DIRS_ALL_LEFT_TO_RIGHT;
  
  public static final Directions DIRS_ALL_RIGHT_TO_LEFT;
  
  public static final int DIR_LEFT_TO_RIGHT = 1;
  
  static final int DIR_REQUEST_DEFAULT_LTR = 2;
  
  static final int DIR_REQUEST_DEFAULT_RTL = -2;
  
  static final int DIR_REQUEST_LTR = 1;
  
  static final int DIR_REQUEST_RTL = -1;
  
  public static final int DIR_RIGHT_TO_LEFT = -1;
  
  public static final int HYPHENATION_FREQUENCY_FULL = 2;
  
  public static final int HYPHENATION_FREQUENCY_NONE = 0;
  
  public static final int HYPHENATION_FREQUENCY_NORMAL = 1;
  
  public static final int JUSTIFICATION_MODE_INTER_WORD = 1;
  
  public static final int JUSTIFICATION_MODE_NONE = 0;
  
  static final int RUN_LENGTH_MASK = 67108863;
  
  static final int RUN_LEVEL_MASK = 63;
  
  static final int RUN_LEVEL_SHIFT = 26;
  
  static final int RUN_RTL_FLAG = 67108864;
  
  private static final float TAB_INCREMENT = 20.0F;
  
  public static final int TEXT_SELECTION_LAYOUT_LEFT_TO_RIGHT = 1;
  
  public static final int TEXT_SELECTION_LAYOUT_RIGHT_TO_LEFT = 0;
  
  private int mJustificationMode;
  
  private SpanSet<LineBackgroundSpan> mLineBackgroundSpans;
  
  private TextPaint mPaint;
  
  private float mSpacingAdd;
  
  private float mSpacingMult;
  
  private boolean mSpannedText;
  
  private CharSequence mText;
  
  private TextDirectionHeuristic mTextDir;
  
  private int mWidth;
  
  public enum Alignment {
    ALIGN_CENTER, ALIGN_LEFT, ALIGN_NORMAL, ALIGN_OPPOSITE, ALIGN_RIGHT;
    
    private static final Alignment[] $VALUES;
    
    static {
      Alignment alignment = new Alignment("ALIGN_RIGHT", 4);
      $VALUES = new Alignment[] { ALIGN_NORMAL, ALIGN_OPPOSITE, ALIGN_CENTER, ALIGN_LEFT, alignment };
    }
  }
  
  static {
    DIRS_ALL_LEFT_TO_RIGHT = new Directions(new int[] { 0, 67108863 });
    DIRS_ALL_RIGHT_TO_LEFT = new Directions(new int[] { 0, 134217727 });
  }
  
  public abstract int getBottomPadding();
  
  public abstract int getEllipsisCount(int paramInt);
  
  public abstract int getEllipsisStart(int paramInt);
  
  public abstract boolean getLineContainsTab(int paramInt);
  
  public abstract int getLineCount();
  
  public abstract int getLineDescent(int paramInt);
  
  public abstract Directions getLineDirections(int paramInt);
  
  public abstract int getLineStart(int paramInt);
  
  public abstract int getLineTop(int paramInt);
  
  public abstract int getParagraphDirection(int paramInt);
  
  public abstract int getTopPadding();
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface BreakStrategy {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Direction {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface HyphenationFrequency {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface JustificationMode {}
  
  @FunctionalInterface
  public static interface SelectionRectangleConsumer {
    void accept(float param1Float1, float param1Float2, float param1Float3, float param1Float4, int param1Int);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface TextSelectionLayout {}
}
