package android.text;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;

public interface ITextJustificationHooks extends IOplusCommonFeature {
  public static final ITextJustificationHooks DEFAULT = (ITextJustificationHooks)new Object();
  
  default ITextJustificationHooks getDefault() {
    return DEFAULT;
  }
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.ITextJustificationHooks;
  }
  
  default void setTextViewParaSpacing(Object paramObject, float paramFloat, Layout paramLayout) {}
  
  default float getTextViewParaSpacing(Object paramObject) {
    return 0.0F;
  }
  
  default float getTextViewDefaultLineMulti(Object paramObject, float paramFloat1, float paramFloat2) {
    return paramFloat2;
  }
  
  default float calculateAddedWidth(float paramFloat1, float paramFloat2, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean, char[] paramArrayOfchar, CharSequence paramCharSequence, int paramInt4) {
    return 0.0F;
  }
  
  default float getLayoutParaSpacingAdded(StaticLayout paramStaticLayout, Object paramObject, boolean paramBoolean, CharSequence paramCharSequence, int paramInt) {
    return 0.0F;
  }
  
  default void setLayoutParaSpacingAdded(Object paramObject, float paramFloat) {}
  
  default boolean lineShouldIncludeFontPad(boolean paramBoolean, StaticLayout paramStaticLayout) {
    return paramBoolean;
  }
  
  default boolean lineNeedMultiply(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, StaticLayout paramStaticLayout) {
    if (paramBoolean1 && (paramBoolean2 || !paramBoolean3)) {
      paramBoolean1 = true;
    } else {
      paramBoolean1 = false;
    } 
    return paramBoolean1;
  }
}
