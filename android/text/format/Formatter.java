package android.text.format;

import android.content.Context;
import android.content.res.Resources;
import android.icu.text.MeasureFormat;
import android.icu.util.Measure;
import android.icu.util.MeasureUnit;
import android.net.NetworkUtils;
import android.text.BidiFormatter;
import android.text.TextUtils;
import java.util.Locale;

public final class Formatter {
  public static final int FLAG_CALCULATE_ROUNDED = 2;
  
  public static final int FLAG_IEC_UNITS = 8;
  
  public static final int FLAG_SHORTER = 1;
  
  public static final int FLAG_SI_UNITS = 4;
  
  private static final int MILLIS_PER_MINUTE = 60000;
  
  private static final int SECONDS_PER_DAY = 86400;
  
  private static final int SECONDS_PER_HOUR = 3600;
  
  private static final int SECONDS_PER_MINUTE = 60;
  
  public static class BytesResult {
    public final long roundedBytes;
    
    public final String units;
    
    public final String value;
    
    public BytesResult(String param1String1, String param1String2, long param1Long) {
      this.value = param1String1;
      this.units = param1String2;
      this.roundedBytes = param1Long;
    }
  }
  
  private static Locale localeFromContext(Context paramContext) {
    return paramContext.getResources().getConfiguration().getLocales().get(0);
  }
  
  private static String bidiWrap(Context paramContext, String paramString) {
    Locale locale = localeFromContext(paramContext);
    if (TextUtils.getLayoutDirectionFromLocale(locale) == 1)
      return BidiFormatter.getInstance(true).unicodeWrap(paramString); 
    return paramString;
  }
  
  public static String formatFileSize(Context paramContext, long paramLong) {
    return formatFileSize(paramContext, paramLong, 4);
  }
  
  public static String formatFileSize(Context paramContext, long paramLong, int paramInt) {
    if (paramContext == null)
      return ""; 
    BytesResult bytesResult = formatBytes(paramContext.getResources(), paramLong, paramInt);
    return bidiWrap(paramContext, paramContext.getString(17040218, new Object[] { bytesResult.value, bytesResult.units }));
  }
  
  public static String formatShortFileSize(Context paramContext, long paramLong) {
    if (paramContext == null)
      return ""; 
    BytesResult bytesResult = formatBytes(paramContext.getResources(), paramLong, 5);
    return bidiWrap(paramContext, paramContext.getString(17040218, new Object[] { bytesResult.value, bytesResult.units }));
  }
  
  public static BytesResult formatBytes(Resources paramResources, long paramLong, int paramInt) {
    char c;
    boolean bool;
    if ((paramInt & 0x8) != 0) {
      c = 'Ѐ';
    } else {
      c = 'Ϩ';
    } 
    long l1 = 0L;
    if (paramLong < 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    if (bool) {
      f1 = (float)-paramLong;
    } else {
      f1 = (float)paramLong;
    } 
    int i = 17039786;
    long l2 = 1L;
    float f2 = f1;
    if (f1 > 900.0F) {
      i = 17040443;
      l2 = c;
      f2 = f1 / c;
    } 
    float f1 = f2;
    paramLong = l2;
    if (f2 > 900.0F) {
      i = 17040624;
      paramLong = l2 * c;
      f1 = f2 / c;
    } 
    float f3 = f1;
    l2 = paramLong;
    if (f1 > 900.0F) {
      i = 17040264;
      l2 = paramLong * c;
      f3 = f1 / c;
    } 
    f2 = f3;
    paramLong = l2;
    if (f3 > 900.0F) {
      i = 17041389;
      paramLong = l2 * c;
      f2 = f3 / c;
    } 
    f1 = f2;
    int j = i;
    l2 = paramLong;
    if (f2 > 900.0F) {
      j = 17041071;
      l2 = paramLong * c;
      f1 = f2 / c;
    } 
    if (l2 == 1L || f1 >= 100.0F) {
      i = 1;
      str2 = "%.0f";
    } else if (f1 < 1.0F) {
      i = 100;
      str2 = "%.2f";
    } else if (f1 < 10.0F) {
      if ((paramInt & 0x1) != 0) {
        i = 10;
        str2 = "%.1f";
      } else {
        i = 100;
        str2 = "%.2f";
      } 
    } else if ((paramInt & 0x1) != 0) {
      i = 1;
      str2 = "%.0f";
    } else {
      i = 100;
      str2 = "%.2f";
    } 
    f2 = f1;
    if (bool)
      f2 = -f1; 
    String str2 = String.format(str2, new Object[] { Float.valueOf(f2) });
    if ((paramInt & 0x2) == 0) {
      paramLong = l1;
    } else {
      paramLong = Math.round(i * f2) * l2 / i;
    } 
    String str1 = paramResources.getString(j);
    return new BytesResult(str2, str1, paramLong);
  }
  
  @Deprecated
  public static String formatIpAddress(int paramInt) {
    return NetworkUtils.intToInetAddress(paramInt).getHostAddress();
  }
  
  public static String formatShortElapsedTime(Context paramContext, long paramLong) {
    paramLong /= 1000L;
    int i = 0, j = 0, k = 0;
    long l = paramLong;
    if (paramLong >= 86400L) {
      i = (int)(paramLong / 86400L);
      l = paramLong - (86400 * i);
    } 
    paramLong = l;
    if (l >= 3600L) {
      j = (int)(l / 3600L);
      paramLong = l - (j * 3600);
    } 
    l = paramLong;
    if (paramLong >= 60L) {
      k = (int)(paramLong / 60L);
      l = paramLong - (k * 60);
    } 
    int m = (int)l;
    Locale locale = localeFromContext(paramContext);
    MeasureFormat measureFormat = MeasureFormat.getInstance(locale, MeasureFormat.FormatWidth.SHORT);
    if (i >= 2 || (i > 0 && j == 0)) {
      j = (j + 12) / 24;
      return measureFormat.format(new Measure(Integer.valueOf(i + j), (MeasureUnit)MeasureUnit.DAY));
    } 
    if (i > 0) {
      Measure measure1 = new Measure(Integer.valueOf(i), (MeasureUnit)MeasureUnit.DAY);
      Measure measure2 = new Measure(Integer.valueOf(j), (MeasureUnit)MeasureUnit.HOUR);
      return measureFormat.formatMeasures(new Measure[] { measure1, measure2 });
    } 
    if (j >= 2 || (j > 0 && k == 0)) {
      i = (k + 30) / 60;
      return measureFormat.format(new Measure(Integer.valueOf(j + i), (MeasureUnit)MeasureUnit.HOUR));
    } 
    if (j > 0) {
      Measure measure2 = new Measure(Integer.valueOf(j), (MeasureUnit)MeasureUnit.HOUR);
      Measure measure1 = new Measure(Integer.valueOf(k), (MeasureUnit)MeasureUnit.MINUTE);
      return measureFormat.formatMeasures(new Measure[] { measure2, measure1 });
    } 
    if (k >= 2 || (k > 0 && m == 0)) {
      i = (m + 30) / 60;
      return measureFormat.format(new Measure(Integer.valueOf(k + i), (MeasureUnit)MeasureUnit.MINUTE));
    } 
    if (k > 0) {
      Measure measure1 = new Measure(Integer.valueOf(k), (MeasureUnit)MeasureUnit.MINUTE);
      Measure measure2 = new Measure(Integer.valueOf(m), (MeasureUnit)MeasureUnit.SECOND);
      return measureFormat.formatMeasures(new Measure[] { measure1, measure2 });
    } 
    return measureFormat.format(new Measure(Integer.valueOf(m), (MeasureUnit)MeasureUnit.SECOND));
  }
  
  public static String formatShortElapsedTimeRoundingUpToMinutes(Context paramContext, long paramLong) {
    MeasureFormat measureFormat;
    paramLong = (paramLong + 60000L - 1L) / 60000L;
    if (paramLong == 0L || paramLong == 1L) {
      Locale locale = localeFromContext(paramContext);
      measureFormat = MeasureFormat.getInstance(locale, MeasureFormat.FormatWidth.SHORT);
      return measureFormat.format(new Measure(Long.valueOf(paramLong), (MeasureUnit)MeasureUnit.MINUTE));
    } 
    return formatShortElapsedTime((Context)measureFormat, 60000L * paramLong);
  }
}
