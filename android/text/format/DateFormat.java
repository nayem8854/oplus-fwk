package android.text.format;

import android.content.Context;
import android.provider.Settings;
import android.text.SpannableStringBuilder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;
import libcore.icu.ICU;
import libcore.icu.LocaleData;

public class DateFormat {
  @Deprecated
  public static final char AM_PM = 'a';
  
  @Deprecated
  public static final char CAPITAL_AM_PM = 'A';
  
  @Deprecated
  public static final char DATE = 'd';
  
  @Deprecated
  public static final char DAY = 'E';
  
  @Deprecated
  public static final char HOUR = 'h';
  
  @Deprecated
  public static final char HOUR_OF_DAY = 'k';
  
  @Deprecated
  public static final char MINUTE = 'm';
  
  @Deprecated
  public static final char MONTH = 'M';
  
  @Deprecated
  public static final char QUOTE = '\'';
  
  @Deprecated
  public static final char SECONDS = 's';
  
  @Deprecated
  public static final char STANDALONE_MONTH = 'L';
  
  @Deprecated
  public static final char TIME_ZONE = 'z';
  
  @Deprecated
  public static final char YEAR = 'y';
  
  private static boolean sIs24Hour;
  
  private static Locale sIs24HourLocale;
  
  private static final Object sLocaleLock = new Object();
  
  public static boolean is24HourFormat(Context paramContext) {
    return is24HourFormat(paramContext, paramContext.getUserId());
  }
  
  public static boolean is24HourFormat(Context paramContext, int paramInt) {
    String str = Settings.System.getStringForUser(paramContext.getContentResolver(), "time_12_24", paramInt);
    if (str != null)
      return str.equals("24"); 
    return is24HourLocale((paramContext.getResources().getConfiguration()).locale);
  }
  
  public static boolean is24HourLocale(Locale paramLocale) {
    synchronized (sLocaleLock) {
      boolean bool;
      if (sIs24HourLocale != null && sIs24HourLocale.equals(paramLocale)) {
        bool = sIs24Hour;
        return bool;
      } 
      null = java.text.DateFormat.getTimeInstance(1, paramLocale);
      if (null instanceof SimpleDateFormat) {
        null = null;
        null = null.toPattern();
        bool = hasDesignator((CharSequence)null, 'H');
      } else {
        bool = false;
      } 
      synchronized (sLocaleLock) {
        sIs24HourLocale = paramLocale;
        sIs24Hour = bool;
        return bool;
      } 
    } 
  }
  
  public static String getBestDateTimePattern(Locale paramLocale, String paramString) {
    return ICU.getBestDateTimePattern(paramString, paramLocale);
  }
  
  public static java.text.DateFormat getTimeFormat(Context paramContext) {
    Locale locale = (paramContext.getResources().getConfiguration()).locale;
    return new SimpleDateFormat(getTimeFormatString(paramContext), locale);
  }
  
  public static String getTimeFormatString(Context paramContext) {
    return getTimeFormatString(paramContext, paramContext.getUserId());
  }
  
  public static String getTimeFormatString(Context paramContext, int paramInt) {
    String str;
    LocaleData localeData = LocaleData.get((paramContext.getResources().getConfiguration()).locale);
    if (is24HourFormat(paramContext, paramInt)) {
      str = localeData.timeFormat_Hm;
    } else {
      str = localeData.timeFormat_hm;
    } 
    return str;
  }
  
  public static java.text.DateFormat getDateFormat(Context paramContext) {
    Locale locale = (paramContext.getResources().getConfiguration()).locale;
    return java.text.DateFormat.getDateInstance(3, locale);
  }
  
  public static java.text.DateFormat getLongDateFormat(Context paramContext) {
    Locale locale = (paramContext.getResources().getConfiguration()).locale;
    return java.text.DateFormat.getDateInstance(1, locale);
  }
  
  public static java.text.DateFormat getMediumDateFormat(Context paramContext) {
    Locale locale = (paramContext.getResources().getConfiguration()).locale;
    return java.text.DateFormat.getDateInstance(2, locale);
  }
  
  public static char[] getDateFormatOrder(Context paramContext) {
    return ICU.getDateFormatOrder(getDateFormatString(paramContext));
  }
  
  private static String getDateFormatString(Context paramContext) {
    Locale locale = (paramContext.getResources().getConfiguration()).locale;
    java.text.DateFormat dateFormat = java.text.DateFormat.getDateInstance(3, locale);
    if (dateFormat instanceof SimpleDateFormat)
      return ((SimpleDateFormat)dateFormat).toPattern(); 
    throw new AssertionError("!(df instanceof SimpleDateFormat)");
  }
  
  public static CharSequence format(CharSequence paramCharSequence, long paramLong) {
    return format(paramCharSequence, new Date(paramLong));
  }
  
  public static CharSequence format(CharSequence paramCharSequence, Date paramDate) {
    GregorianCalendar gregorianCalendar = new GregorianCalendar();
    gregorianCalendar.setTime(paramDate);
    return format(paramCharSequence, gregorianCalendar);
  }
  
  public static boolean hasSeconds(CharSequence paramCharSequence) {
    return hasDesignator(paramCharSequence, 's');
  }
  
  public static boolean hasDesignator(CharSequence paramCharSequence, char paramChar) {
    if (paramCharSequence == null)
      return false; 
    int i = paramCharSequence.length();
    boolean bool = false;
    for (byte b = 0; b < i; b++, bool = bool1) {
      char c = paramCharSequence.charAt(b);
      boolean bool1 = true;
      if (c == '\'') {
        if (!bool) {
          bool = bool1;
        } else {
          bool = false;
        } 
        bool1 = bool;
      } else {
        bool1 = bool;
        if (!bool) {
          bool1 = bool;
          if (c == paramChar)
            return true; 
        } 
      } 
    } 
    return false;
  }
  
  public static CharSequence format(CharSequence paramCharSequence, Calendar paramCalendar) {
    // Byte code:
    //   0: new android/text/SpannableStringBuilder
    //   3: dup
    //   4: aload_0
    //   5: invokespecial <init> : (Ljava/lang/CharSequence;)V
    //   8: astore_2
    //   9: invokestatic getDefault : ()Ljava/util/Locale;
    //   12: invokestatic get : (Ljava/util/Locale;)Llibcore/icu/LocaleData;
    //   15: astore_3
    //   16: aload_0
    //   17: invokeinterface length : ()I
    //   22: istore #4
    //   24: iconst_0
    //   25: istore #5
    //   27: iload #5
    //   29: iload #4
    //   31: if_icmpge -> 461
    //   34: iconst_1
    //   35: istore #6
    //   37: aload_2
    //   38: iload #5
    //   40: invokevirtual charAt : (I)C
    //   43: istore #7
    //   45: iload #7
    //   47: bipush #39
    //   49: if_icmpne -> 69
    //   52: aload_2
    //   53: iload #5
    //   55: invokestatic appendQuotedText : (Landroid/text/SpannableStringBuilder;I)I
    //   58: istore #8
    //   60: aload_2
    //   61: invokevirtual length : ()I
    //   64: istore #4
    //   66: goto -> 451
    //   69: iload #5
    //   71: iload #6
    //   73: iadd
    //   74: iload #4
    //   76: if_icmpge -> 99
    //   79: aload_2
    //   80: iload #5
    //   82: iload #6
    //   84: iadd
    //   85: invokevirtual charAt : (I)C
    //   88: iload #7
    //   90: if_icmpne -> 99
    //   93: iinc #6, 1
    //   96: goto -> 69
    //   99: iload #7
    //   101: bipush #65
    //   103: if_icmpeq -> 400
    //   106: iload #7
    //   108: bipush #69
    //   110: if_icmpeq -> 377
    //   113: iload #7
    //   115: bipush #72
    //   117: if_icmpeq -> 357
    //   120: iload #7
    //   122: bipush #97
    //   124: if_icmpeq -> 400
    //   127: iload #7
    //   129: bipush #104
    //   131: if_icmpeq -> 313
    //   134: iload #7
    //   136: bipush #107
    //   138: if_icmpeq -> 357
    //   141: iload #7
    //   143: bipush #109
    //   145: if_icmpeq -> 297
    //   148: iload #7
    //   150: bipush #115
    //   152: if_icmpeq -> 281
    //   155: iload #7
    //   157: bipush #99
    //   159: if_icmpeq -> 377
    //   162: iload #7
    //   164: bipush #100
    //   166: if_icmpeq -> 266
    //   169: iload #7
    //   171: bipush #121
    //   173: if_icmpeq -> 251
    //   176: iload #7
    //   178: bipush #122
    //   180: if_icmpeq -> 240
    //   183: iload #7
    //   185: tableswitch default -> 212, 75 -> 313, 76 -> 218, 77 -> 218
    //   212: aconst_null
    //   213: astore #9
    //   215: goto -> 415
    //   218: aload_1
    //   219: iconst_2
    //   220: invokevirtual get : (I)I
    //   223: istore #8
    //   225: aload_3
    //   226: iload #8
    //   228: iload #6
    //   230: iload #7
    //   232: invokestatic getMonthString : (Llibcore/icu/LocaleData;III)Ljava/lang/String;
    //   235: astore #9
    //   237: goto -> 415
    //   240: aload_1
    //   241: iload #6
    //   243: invokestatic getTimeZoneString : (Ljava/util/Calendar;I)Ljava/lang/String;
    //   246: astore #9
    //   248: goto -> 415
    //   251: aload_1
    //   252: iconst_1
    //   253: invokevirtual get : (I)I
    //   256: iload #6
    //   258: invokestatic getYearString : (II)Ljava/lang/String;
    //   261: astore #9
    //   263: goto -> 415
    //   266: aload_1
    //   267: iconst_5
    //   268: invokevirtual get : (I)I
    //   271: iload #6
    //   273: invokestatic zeroPad : (II)Ljava/lang/String;
    //   276: astore #9
    //   278: goto -> 415
    //   281: aload_1
    //   282: bipush #13
    //   284: invokevirtual get : (I)I
    //   287: iload #6
    //   289: invokestatic zeroPad : (II)Ljava/lang/String;
    //   292: astore #9
    //   294: goto -> 415
    //   297: aload_1
    //   298: bipush #12
    //   300: invokevirtual get : (I)I
    //   303: iload #6
    //   305: invokestatic zeroPad : (II)Ljava/lang/String;
    //   308: astore #9
    //   310: goto -> 415
    //   313: aload_1
    //   314: bipush #10
    //   316: invokevirtual get : (I)I
    //   319: istore #10
    //   321: iload #10
    //   323: istore #8
    //   325: iload #7
    //   327: bipush #104
    //   329: if_icmpne -> 345
    //   332: iload #10
    //   334: istore #8
    //   336: iload #10
    //   338: ifne -> 345
    //   341: bipush #12
    //   343: istore #8
    //   345: iload #8
    //   347: iload #6
    //   349: invokestatic zeroPad : (II)Ljava/lang/String;
    //   352: astore #9
    //   354: goto -> 415
    //   357: aload_1
    //   358: bipush #11
    //   360: invokevirtual get : (I)I
    //   363: istore #8
    //   365: iload #8
    //   367: iload #6
    //   369: invokestatic zeroPad : (II)Ljava/lang/String;
    //   372: astore #9
    //   374: goto -> 415
    //   377: aload_1
    //   378: bipush #7
    //   380: invokevirtual get : (I)I
    //   383: istore #8
    //   385: aload_3
    //   386: iload #8
    //   388: iload #6
    //   390: iload #7
    //   392: invokestatic getDayOfWeekString : (Llibcore/icu/LocaleData;III)Ljava/lang/String;
    //   395: astore #9
    //   397: goto -> 415
    //   400: aload_3
    //   401: getfield amPm : [Ljava/lang/String;
    //   404: aload_1
    //   405: bipush #9
    //   407: invokevirtual get : (I)I
    //   410: iconst_0
    //   411: iadd
    //   412: aaload
    //   413: astore #9
    //   415: iload #6
    //   417: istore #8
    //   419: aload #9
    //   421: ifnull -> 451
    //   424: aload_2
    //   425: iload #5
    //   427: iload #5
    //   429: iload #6
    //   431: iadd
    //   432: aload #9
    //   434: invokevirtual replace : (IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
    //   437: pop
    //   438: aload #9
    //   440: invokevirtual length : ()I
    //   443: istore #8
    //   445: aload_2
    //   446: invokevirtual length : ()I
    //   449: istore #4
    //   451: iload #5
    //   453: iload #8
    //   455: iadd
    //   456: istore #5
    //   458: goto -> 27
    //   461: aload_0
    //   462: instanceof android/text/Spanned
    //   465: ifeq -> 477
    //   468: new android/text/SpannedString
    //   471: dup
    //   472: aload_2
    //   473: invokespecial <init> : (Ljava/lang/CharSequence;)V
    //   476: areturn
    //   477: aload_2
    //   478: invokevirtual toString : ()Ljava/lang/String;
    //   481: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #428	-> 0
    //   #431	-> 9
    //   #433	-> 16
    //   #435	-> 24
    //   #436	-> 34
    //   #437	-> 37
    //   #439	-> 45
    //   #440	-> 52
    //   #441	-> 60
    //   #442	-> 66
    //   #445	-> 69
    //   #446	-> 93
    //   #450	-> 99
    //   #504	-> 212
    //   #488	-> 218
    //   #489	-> 218
    //   #488	-> 225
    //   #490	-> 237
    //   #501	-> 240
    //   #502	-> 248
    //   #498	-> 251
    //   #499	-> 263
    //   #456	-> 266
    //   #457	-> 278
    //   #495	-> 281
    //   #496	-> 294
    //   #492	-> 297
    //   #493	-> 310
    //   #466	-> 313
    //   #467	-> 321
    //   #468	-> 341
    //   #470	-> 345
    //   #472	-> 354
    //   #476	-> 357
    //   #483	-> 365
    //   #485	-> 374
    //   #460	-> 377
    //   #461	-> 377
    //   #460	-> 385
    //   #462	-> 397
    //   #453	-> 400
    //   #454	-> 415
    //   #508	-> 415
    //   #509	-> 424
    //   #510	-> 438
    //   #511	-> 445
    //   #435	-> 451
    //   #515	-> 461
    //   #516	-> 468
    //   #518	-> 477
  }
  
  private static String getDayOfWeekString(LocaleData paramLocaleData, int paramInt1, int paramInt2, int paramInt3) {
    String str;
    if (paramInt3 == 99) {
      paramInt3 = 1;
    } else {
      paramInt3 = 0;
    } 
    if (paramInt2 == 5) {
      if (paramInt3 != 0) {
        str = paramLocaleData.tinyStandAloneWeekdayNames[paramInt1];
      } else {
        str = ((LocaleData)str).tinyWeekdayNames[paramInt1];
      } 
      return str;
    } 
    if (paramInt2 == 4) {
      if (paramInt3 != 0) {
        str = ((LocaleData)str).longStandAloneWeekdayNames[paramInt1];
      } else {
        str = ((LocaleData)str).longWeekdayNames[paramInt1];
      } 
      return str;
    } 
    if (paramInt3 != 0) {
      str = ((LocaleData)str).shortStandAloneWeekdayNames[paramInt1];
    } else {
      str = ((LocaleData)str).shortWeekdayNames[paramInt1];
    } 
    return str;
  }
  
  private static String getMonthString(LocaleData paramLocaleData, int paramInt1, int paramInt2, int paramInt3) {
    String str;
    if (paramInt3 == 76) {
      paramInt3 = 1;
    } else {
      paramInt3 = 0;
    } 
    if (paramInt2 == 5) {
      if (paramInt3 != 0) {
        str = paramLocaleData.tinyStandAloneMonthNames[paramInt1];
      } else {
        str = ((LocaleData)str).tinyMonthNames[paramInt1];
      } 
      return str;
    } 
    if (paramInt2 == 4) {
      if (paramInt3 != 0) {
        str = ((LocaleData)str).longStandAloneMonthNames[paramInt1];
      } else {
        str = ((LocaleData)str).longMonthNames[paramInt1];
      } 
      return str;
    } 
    if (paramInt2 == 3) {
      if (paramInt3 != 0) {
        str = ((LocaleData)str).shortStandAloneMonthNames[paramInt1];
      } else {
        str = ((LocaleData)str).shortMonthNames[paramInt1];
      } 
      return str;
    } 
    return zeroPad(paramInt1 + 1, paramInt2);
  }
  
  private static String getTimeZoneString(Calendar paramCalendar, int paramInt) {
    boolean bool;
    TimeZone timeZone = paramCalendar.getTimeZone();
    if (paramInt < 2) {
      int i = paramCalendar.get(16);
      int j = paramCalendar.get(15);
      return formatZoneOffset(i + j, paramInt);
    } 
    if (paramCalendar.get(16) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return timeZone.getDisplayName(bool, 0);
  }
  
  private static String formatZoneOffset(int paramInt1, int paramInt2) {
    paramInt1 /= 1000;
    StringBuilder stringBuilder = new StringBuilder();
    if (paramInt1 < 0) {
      stringBuilder.insert(0, "-");
      paramInt1 = -paramInt1;
    } else {
      stringBuilder.insert(0, "+");
    } 
    paramInt2 = paramInt1 / 3600;
    paramInt1 = paramInt1 % 3600 / 60;
    stringBuilder.append(zeroPad(paramInt2, 2));
    stringBuilder.append(zeroPad(paramInt1, 2));
    return stringBuilder.toString();
  }
  
  private static String getYearString(int paramInt1, int paramInt2) {
    String str;
    if (paramInt2 <= 2) {
      str = zeroPad(paramInt1 % 100, 2);
    } else {
      str = String.format(Locale.getDefault(), "%d", new Object[] { Integer.valueOf(paramInt1) });
    } 
    return str;
  }
  
  public static int appendQuotedText(SpannableStringBuilder paramSpannableStringBuilder, int paramInt) {
    int i = paramSpannableStringBuilder.length();
    if (paramInt + 1 < i && paramSpannableStringBuilder.charAt(paramInt + 1) == '\'') {
      paramSpannableStringBuilder.delete(paramInt, paramInt + 1);
      return 1;
    } 
    byte b = 0;
    paramSpannableStringBuilder.delete(paramInt, paramInt + 1);
    i--;
    while (paramInt < i) {
      char c = paramSpannableStringBuilder.charAt(paramInt);
      if (c == '\'') {
        if (paramInt + 1 < i && paramSpannableStringBuilder.charAt(paramInt + 1) == '\'') {
          paramSpannableStringBuilder.delete(paramInt, paramInt + 1);
          i--;
          b++;
          paramInt++;
          continue;
        } 
        paramSpannableStringBuilder.delete(paramInt, paramInt + 1);
        break;
      } 
      paramInt++;
      b++;
    } 
    return b;
  }
  
  private static String zeroPad(int paramInt1, int paramInt2) {
    Locale locale = Locale.getDefault();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("%0");
    stringBuilder.append(paramInt2);
    stringBuilder.append("d");
    return String.format(locale, stringBuilder.toString(), new Object[] { Integer.valueOf(paramInt1) });
  }
}
