package android.text.format;

import android.util.TimeFormatException;
import java.io.IOException;
import java.util.Locale;
import java.util.TimeZone;
import libcore.timezone.ZoneInfoDb;
import libcore.util.ZoneInfo;

@Deprecated
public class Time {
  public Time(String paramString) {
    if (paramString != null) {
      initialize(paramString);
      return;
    } 
    throw new NullPointerException("timezoneId is null!");
  }
  
  public Time() {
    initialize(TimeZone.getDefault().getID());
  }
  
  public Time(Time paramTime) {
    initialize(paramTime.timezone);
    set(paramTime);
  }
  
  private void initialize(String paramString) {
    this.timezone = paramString;
    this.year = 1970;
    this.monthDay = 1;
    this.isDst = -1;
    this.calculator = new TimeCalculator(paramString);
  }
  
  public long normalize(boolean paramBoolean) {
    this.calculator.copyFieldsFromTime(this);
    long l = this.calculator.toMillis(paramBoolean);
    this.calculator.copyFieldsToTime(this);
    return l;
  }
  
  public void switchTimezone(String paramString) {
    this.calculator.copyFieldsFromTime(this);
    this.calculator.switchTimeZone(paramString);
    this.calculator.copyFieldsToTime(this);
    this.timezone = paramString;
  }
  
  private static final int[] DAYS_PER_MONTH = new int[] { 
      31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 
      30, 31 };
  
  public static final int EPOCH_JULIAN_DAY = 2440588;
  
  public static final int FRIDAY = 5;
  
  public static final int HOUR = 3;
  
  public static final int MINUTE = 2;
  
  public static final int MONDAY = 1;
  
  public static final int MONDAY_BEFORE_JULIAN_EPOCH = 2440585;
  
  public static final int MONTH = 5;
  
  public static final int MONTH_DAY = 4;
  
  public static final int SATURDAY = 6;
  
  public static final int SECOND = 1;
  
  public static final int SUNDAY = 0;
  
  public static final int THURSDAY = 4;
  
  public static final String TIMEZONE_UTC = "UTC";
  
  public static final int TUESDAY = 2;
  
  public static final int WEDNESDAY = 3;
  
  public static final int WEEK_DAY = 7;
  
  public static final int WEEK_NUM = 9;
  
  public static final int YEAR = 6;
  
  public static final int YEAR_DAY = 8;
  
  private static final String Y_M_D = "%Y-%m-%d";
  
  private static final String Y_M_D_T_H_M_S_000 = "%Y-%m-%dT%H:%M:%S.000";
  
  private static final String Y_M_D_T_H_M_S_000_Z = "%Y-%m-%dT%H:%M:%S.000Z";
  
  public int getActualMaximum(int paramInt) {
    StringBuilder stringBuilder;
    byte b;
    int i;
    switch (paramInt) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("bad field=");
        stringBuilder.append(paramInt);
        throw new RuntimeException(stringBuilder.toString());
      case 9:
        throw new RuntimeException("WEEK_NUM not implemented");
      case 8:
        paramInt = this.year;
        if (paramInt % 4 == 0 && (paramInt % 100 != 0 || paramInt % 400 == 0)) {
          paramInt = 365;
        } else {
          paramInt = 364;
        } 
        return paramInt;
      case 7:
        return 6;
      case 6:
        return 2037;
      case 5:
        return 11;
      case 4:
        paramInt = DAYS_PER_MONTH[this.month];
        b = 28;
        if (paramInt != 28)
          return paramInt; 
        i = this.year;
        paramInt = b;
        if (i % 4 == 0) {
          if (i % 100 == 0) {
            paramInt = b;
            return (i % 400 == 0) ? 29 : paramInt;
          } 
        } else {
          return paramInt;
        } 
        return 29;
      case 3:
        return 23;
      case 2:
        return 59;
      case 1:
        break;
    } 
    return 59;
  }
  
  public void clear(String paramString) {
    if (paramString != null) {
      this.timezone = paramString;
      this.allDay = false;
      this.second = 0;
      this.minute = 0;
      this.hour = 0;
      this.monthDay = 0;
      this.month = 0;
      this.year = 0;
      this.weekDay = 0;
      this.yearDay = 0;
      this.gmtoff = 0L;
      this.isDst = -1;
      return;
    } 
    throw new NullPointerException("timezone is null!");
  }
  
  public static int compare(Time paramTime1, Time paramTime2) {
    if (paramTime1 != null) {
      if (paramTime2 != null) {
        paramTime1.calculator.copyFieldsFromTime(paramTime1);
        paramTime2.calculator.copyFieldsFromTime(paramTime2);
        return TimeCalculator.compare(paramTime1.calculator, paramTime2.calculator);
      } 
      throw new NullPointerException("b == null");
    } 
    throw new NullPointerException("a == null");
  }
  
  public String format(String paramString) {
    this.calculator.copyFieldsFromTime(this);
    return this.calculator.format(paramString);
  }
  
  public String toString() {
    TimeCalculator timeCalculator = new TimeCalculator(this.timezone);
    timeCalculator.copyFieldsFromTime(this);
    return timeCalculator.toStringInternal();
  }
  
  public boolean parse(String paramString) {
    if (paramString != null) {
      if (parseInternal(paramString)) {
        this.timezone = "UTC";
        return true;
      } 
      return false;
    } 
    throw new NullPointerException("time string is null");
  }
  
  private boolean parseInternal(String paramString) {
    int i = paramString.length();
    if (i >= 8) {
      boolean bool = false;
      int j = getChar(paramString, 0, 1000);
      int k = getChar(paramString, 1, 100);
      int m = getChar(paramString, 2, 10);
      int n = getChar(paramString, 3, 1);
      this.year = j + k + m + n;
      k = getChar(paramString, 4, 10);
      m = getChar(paramString, 5, 1);
      this.month = k + m - 1;
      m = getChar(paramString, 6, 10);
      k = getChar(paramString, 7, 1);
      this.monthDay = m + k;
      if (i > 8) {
        if (i >= 15) {
          checkChar(paramString, 8, 'T');
          this.allDay = false;
          m = getChar(paramString, 9, 10);
          k = getChar(paramString, 10, 1);
          this.hour = m + k;
          k = getChar(paramString, 11, 10);
          m = getChar(paramString, 12, 1);
          this.minute = k + m;
          k = getChar(paramString, 13, 10);
          m = getChar(paramString, 14, 1);
          this.second = k + m;
          if (i > 15) {
            checkChar(paramString, 15, 'Z');
            bool = true;
          } 
        } else {
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("String is too short: \"");
          stringBuilder1.append(paramString);
          stringBuilder1.append("\" If there are more than 8 characters there must be at least 15.");
          throw new TimeFormatException(stringBuilder1.toString());
        } 
      } else {
        this.allDay = true;
        this.hour = 0;
        this.minute = 0;
        this.second = 0;
      } 
      this.weekDay = 0;
      this.yearDay = 0;
      this.isDst = -1;
      this.gmtoff = 0L;
      return bool;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("String is too short: \"");
    stringBuilder.append(paramString);
    stringBuilder.append("\" Expected at least 8 characters.");
    throw new TimeFormatException(stringBuilder.toString());
  }
  
  private void checkChar(String paramString, int paramInt, char paramChar) {
    char c = paramString.charAt(paramInt);
    if (c == paramChar)
      return; 
    throw new TimeFormatException(String.format("Unexpected character 0x%02d at pos=%d.  Expected 0x%02d ('%c').", new Object[] { Integer.valueOf(c), Integer.valueOf(paramInt), Integer.valueOf(paramChar), Character.valueOf(paramChar) }));
  }
  
  private static int getChar(String paramString, int paramInt1, int paramInt2) {
    char c = paramString.charAt(paramInt1);
    if (Character.isDigit(c))
      return Character.getNumericValue(c) * paramInt2; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Parse error at pos=");
    stringBuilder.append(paramInt1);
    throw new TimeFormatException(stringBuilder.toString());
  }
  
  public boolean parse3339(String paramString) {
    if (paramString != null) {
      if (parse3339Internal(paramString)) {
        this.timezone = "UTC";
        return true;
      } 
      return false;
    } 
    throw new NullPointerException("time string is null");
  }
  
  private boolean parse3339Internal(String paramString) {
    int i = paramString.length();
    if (i >= 10) {
      boolean bool1 = false, bool2 = false;
      int j = getChar(paramString, 0, 1000);
      int k = getChar(paramString, 1, 100);
      int m = getChar(paramString, 2, 10);
      int n = getChar(paramString, 3, 1);
      this.year = j + k + m + n;
      checkChar(paramString, 4, '-');
      m = getChar(paramString, 5, 10);
      n = getChar(paramString, 6, 1);
      this.month = m + n - 1;
      checkChar(paramString, 7, '-');
      m = getChar(paramString, 8, 10);
      n = getChar(paramString, 9, 1);
      this.monthDay = m + n;
      if (i >= 19) {
        checkChar(paramString, 10, 'T');
        this.allDay = false;
        m = getChar(paramString, 11, 10);
        n = getChar(paramString, 12, 1);
        int i1 = m + n;
        checkChar(paramString, 13, ':');
        m = getChar(paramString, 14, 10);
        n = getChar(paramString, 15, 1);
        int i2 = m + n;
        checkChar(paramString, 16, ':');
        m = getChar(paramString, 17, 10);
        n = getChar(paramString, 18, 1);
        this.second = m + n;
        n = 19;
        m = n;
        if (19 < i) {
          m = n;
          if (paramString.charAt(19) == '.') {
            m = n;
            while (true) {
              n = m + 1;
              m = n;
              if (n < i) {
                m = n;
                if (!Character.isDigit(paramString.charAt(n))) {
                  m = n;
                  break;
                } 
                continue;
              } 
              break;
            } 
          } 
        } 
        int i3 = 0;
        j = i1;
        k = i2;
        if (i > m) {
          n = paramString.charAt(m);
          if (n != 43) {
            if (n != 45) {
              if (n == 90) {
                n = 0;
              } else {
                throw new TimeFormatException(String.format("Unexpected character 0x%02d at position %d.  Expected + or -", new Object[] { Integer.valueOf(n), Integer.valueOf(m) }));
              } 
            } else {
              n = 1;
            } 
          } else {
            n = -1;
          } 
          bool1 = true;
          bool2 = bool1;
          j = i1;
          k = i2;
          i3 = n;
          if (n != 0)
            if (i >= m + 6) {
              k = getChar(paramString, m + 1, 10);
              j = getChar(paramString, m + 2, 1);
              j = i1 + (k + j) * n;
              k = getChar(paramString, m + 4, 10);
              m = getChar(paramString, m + 5, 1);
              k = i2 + (k + m) * n;
              bool2 = bool1;
              i3 = n;
            } else {
              throw new TimeFormatException(String.format("Unexpected length; should be %d characters", new Object[] { Integer.valueOf(m + 6) }));
            }  
        } 
        this.hour = j;
        this.minute = k;
        if (i3 != 0)
          normalize(false); 
      } else {
        this.allDay = true;
        this.hour = 0;
        this.minute = 0;
        this.second = 0;
        bool2 = bool1;
      } 
      this.weekDay = 0;
      this.yearDay = 0;
      this.isDst = -1;
      this.gmtoff = 0L;
      return bool2;
    } 
    throw new TimeFormatException("String too short --- expected at least 10 characters.");
  }
  
  public static String getCurrentTimezone() {
    return TimeZone.getDefault().getID();
  }
  
  public void setToNow() {
    set(System.currentTimeMillis());
  }
  
  public long toMillis(boolean paramBoolean) {
    this.calculator.copyFieldsFromTime(this);
    return this.calculator.toMillis(paramBoolean);
  }
  
  public void set(long paramLong) {
    this.allDay = false;
    this.calculator.timezone = this.timezone;
    this.calculator.setTimeInMillis(paramLong);
    this.calculator.copyFieldsToTime(this);
  }
  
  public String format2445() {
    this.calculator.copyFieldsFromTime(this);
    return this.calculator.format2445(this.allDay ^ true);
  }
  
  public void set(Time paramTime) {
    this.timezone = paramTime.timezone;
    this.allDay = paramTime.allDay;
    this.second = paramTime.second;
    this.minute = paramTime.minute;
    this.hour = paramTime.hour;
    this.monthDay = paramTime.monthDay;
    this.month = paramTime.month;
    this.year = paramTime.year;
    this.weekDay = paramTime.weekDay;
    this.yearDay = paramTime.yearDay;
    this.isDst = paramTime.isDst;
    this.gmtoff = paramTime.gmtoff;
  }
  
  public void set(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6) {
    this.allDay = false;
    this.second = paramInt1;
    this.minute = paramInt2;
    this.hour = paramInt3;
    this.monthDay = paramInt4;
    this.month = paramInt5;
    this.year = paramInt6;
    this.weekDay = 0;
    this.yearDay = 0;
    this.isDst = -1;
    this.gmtoff = 0L;
  }
  
  public void set(int paramInt1, int paramInt2, int paramInt3) {
    this.allDay = true;
    this.second = 0;
    this.minute = 0;
    this.hour = 0;
    this.monthDay = paramInt1;
    this.month = paramInt2;
    this.year = paramInt3;
    this.weekDay = 0;
    this.yearDay = 0;
    this.isDst = -1;
    this.gmtoff = 0L;
  }
  
  public boolean before(Time paramTime) {
    boolean bool;
    if (compare(this, paramTime) < 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean after(Time paramTime) {
    boolean bool;
    if (compare(this, paramTime) > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private static final int[] sThursdayOffset = new int[] { -3, 3, 2, 1, 0, -1, -2 };
  
  public boolean allDay;
  
  private TimeCalculator calculator;
  
  public long gmtoff;
  
  public int hour;
  
  public int isDst;
  
  public int minute;
  
  public int month;
  
  public int monthDay;
  
  public int second;
  
  public String timezone;
  
  public int weekDay;
  
  public int year;
  
  public int yearDay;
  
  public int getWeekNumber() {
    int i = this.yearDay + sThursdayOffset[this.weekDay];
    if (i >= 0 && i <= 364)
      return i / 7 + 1; 
    Time time = new Time(this);
    time.monthDay += sThursdayOffset[this.weekDay];
    time.normalize(true);
    return time.yearDay / 7 + 1;
  }
  
  public String format3339(boolean paramBoolean) {
    String str2;
    if (paramBoolean)
      return format("%Y-%m-%d"); 
    if ("UTC".equals(this.timezone))
      return format("%Y-%m-%dT%H:%M:%S.000Z"); 
    String str1 = format("%Y-%m-%dT%H:%M:%S.000");
    if (this.gmtoff < 0L) {
      str2 = "-";
    } else {
      str2 = "+";
    } 
    int i = (int)Math.abs(this.gmtoff);
    int j = i % 3600 / 60;
    i /= 3600;
    return String.format(Locale.US, "%s%s%02d:%02d", new Object[] { str1, str2, Integer.valueOf(i), Integer.valueOf(j) });
  }
  
  public static boolean isEpoch(Time paramTime) {
    boolean bool = true;
    long l = paramTime.toMillis(true);
    if (getJulianDay(l, 0L) != 2440588)
      bool = false; 
    return bool;
  }
  
  @Deprecated
  public static int getJulianDay(long paramLong1, long paramLong2) {
    long l = paramLong1 + 1000L * paramLong2;
    paramLong2 = l / 86400000L;
    paramLong1 = paramLong2;
    if (l < 0L) {
      paramLong1 = paramLong2;
      if (l % 86400000L != 0L)
        paramLong1 = paramLong2 - 1L; 
    } 
    return (int)(2440588L + paramLong1);
  }
  
  public long setJulianDay(int paramInt) {
    long l = (paramInt - 2440588) * 86400000L;
    set(l);
    int i = getJulianDay(l, this.gmtoff);
    this.monthDay += paramInt - i;
    this.hour = 0;
    this.minute = 0;
    this.second = 0;
    l = normalize(true);
    return l;
  }
  
  public static int getWeeksSinceEpochFromJulianDay(int paramInt1, int paramInt2) {
    int i = 4 - paramInt2;
    paramInt2 = i;
    if (i < 0)
      paramInt2 = i + 7; 
    return (paramInt1 - 2440588 - paramInt2) / 7;
  }
  
  public static int getJulianMondayFromWeeksSinceEpoch(int paramInt) {
    return paramInt * 7 + 2440585;
  }
  
  private static class TimeCalculator {
    public String timezone;
    
    public final ZoneInfo.WallTime wallTime;
    
    private ZoneInfo zoneInfo;
    
    public TimeCalculator(String param1String) {
      this.zoneInfo = lookupZoneInfo(param1String);
      this.wallTime = new ZoneInfo.WallTime();
    }
    
    public long toMillis(boolean param1Boolean) {
      if (param1Boolean)
        this.wallTime.setIsDst(-1); 
      int i = this.wallTime.mktime(this.zoneInfo);
      if (i == -1)
        return -1L; 
      return i * 1000L;
    }
    
    public void setTimeInMillis(long param1Long) {
      int i = (int)(param1Long / 1000L);
      updateZoneInfoFromTimeZone();
      this.wallTime.localtime(i, this.zoneInfo);
    }
    
    public String format(String param1String) {
      String str = param1String;
      if (param1String == null)
        str = "%c"; 
      TimeFormatter timeFormatter = new TimeFormatter();
      return timeFormatter.format(str, this.wallTime, this.zoneInfo);
    }
    
    private void updateZoneInfoFromTimeZone() {
      if (!this.zoneInfo.getID().equals(this.timezone))
        this.zoneInfo = lookupZoneInfo(this.timezone); 
    }
    
    private static ZoneInfo lookupZoneInfo(String param1String) {
      try {
        ZoneInfo zoneInfo1 = ZoneInfoDb.getInstance().makeTimeZone(param1String);
        ZoneInfo zoneInfo2 = zoneInfo1;
        if (zoneInfo1 == null)
          zoneInfo2 = ZoneInfoDb.getInstance().makeTimeZone("GMT"); 
        if (zoneInfo2 != null)
          return zoneInfo2; 
        AssertionError assertionError = new AssertionError();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("GMT not found: \"");
        stringBuilder.append(param1String);
        stringBuilder.append("\"");
        this(stringBuilder.toString());
        throw assertionError;
      } catch (IOException iOException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Error loading timezone: \"");
        stringBuilder.append(param1String);
        stringBuilder.append("\"");
        throw new AssertionError(stringBuilder.toString(), iOException);
      } 
    }
    
    public void switchTimeZone(String param1String) {
      int i = this.wallTime.mktime(this.zoneInfo);
      this.timezone = param1String;
      updateZoneInfoFromTimeZone();
      this.wallTime.localtime(i, this.zoneInfo);
    }
    
    public String format2445(boolean param1Boolean) {
      if (param1Boolean) {
        i = 16;
      } else {
        i = 8;
      } 
      char[] arrayOfChar = new char[i];
      int i = this.wallTime.getYear();
      arrayOfChar[0] = toChar(i / 1000);
      i %= 1000;
      arrayOfChar[1] = toChar(i / 100);
      i %= 100;
      arrayOfChar[2] = toChar(i / 10);
      arrayOfChar[3] = toChar(i % 10);
      i = this.wallTime.getMonth() + 1;
      arrayOfChar[4] = toChar(i / 10);
      arrayOfChar[5] = toChar(i % 10);
      i = this.wallTime.getMonthDay();
      arrayOfChar[6] = toChar(i / 10);
      arrayOfChar[7] = toChar(i % 10);
      if (!param1Boolean)
        return new String(arrayOfChar, 0, 8); 
      arrayOfChar[8] = 'T';
      i = this.wallTime.getHour();
      arrayOfChar[9] = toChar(i / 10);
      arrayOfChar[10] = toChar(i % 10);
      i = this.wallTime.getMinute();
      arrayOfChar[11] = toChar(i / 10);
      arrayOfChar[12] = toChar(i % 10);
      i = this.wallTime.getSecond();
      arrayOfChar[13] = toChar(i / 10);
      arrayOfChar[14] = toChar(i % 10);
      if ("UTC".equals(this.timezone)) {
        arrayOfChar[15] = 'Z';
        return new String(arrayOfChar, 0, 16);
      } 
      return new String(arrayOfChar, 0, 15);
    }
    
    private char toChar(int param1Int) {
      byte b;
      if (param1Int >= 0 && param1Int <= 9) {
        b = (char)(param1Int + 48);
      } else {
        b = 32;
      } 
      return b;
    }
    
    public String toStringInternal() {
      ZoneInfo.WallTime wallTime1 = this.wallTime;
      int i = wallTime1.getYear();
      wallTime1 = this.wallTime;
      int j = wallTime1.getMonth();
      wallTime1 = this.wallTime;
      int k = wallTime1.getMonthDay();
      wallTime1 = this.wallTime;
      int m = wallTime1.getHour();
      wallTime1 = this.wallTime;
      int n = wallTime1.getMinute();
      wallTime1 = this.wallTime;
      int i1 = wallTime1.getSecond();
      String str = this.timezone;
      ZoneInfo.WallTime wallTime2 = this.wallTime;
      int i2 = wallTime2.getWeekDay();
      wallTime2 = this.wallTime;
      int i3 = wallTime2.getYearDay();
      wallTime2 = this.wallTime;
      int i4 = wallTime2.getGmtOffset();
      wallTime2 = this.wallTime;
      int i5 = wallTime2.getIsDst();
      long l = toMillis(false) / 1000L;
      return String.format("%04d%02d%02dT%02d%02d%02d%s(%d,%d,%d,%d,%d)", new Object[] { 
            Integer.valueOf(i), Integer.valueOf(j + 1), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n), Integer.valueOf(i1), str, Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4), 
            Integer.valueOf(i5), Long.valueOf(l) });
    }
    
    public static int compare(TimeCalculator param1TimeCalculator1, TimeCalculator param1TimeCalculator2) {
      boolean bool = param1TimeCalculator1.timezone.equals(param1TimeCalculator2.timezone);
      int i = 0;
      if (bool) {
        i = param1TimeCalculator1.wallTime.getYear() - param1TimeCalculator2.wallTime.getYear();
        if (i != 0)
          return i; 
        i = param1TimeCalculator1.wallTime.getMonth() - param1TimeCalculator2.wallTime.getMonth();
        if (i != 0)
          return i; 
        i = param1TimeCalculator1.wallTime.getMonthDay() - param1TimeCalculator2.wallTime.getMonthDay();
        if (i != 0)
          return i; 
        i = param1TimeCalculator1.wallTime.getHour() - param1TimeCalculator2.wallTime.getHour();
        if (i != 0)
          return i; 
        i = param1TimeCalculator1.wallTime.getMinute() - param1TimeCalculator2.wallTime.getMinute();
        if (i != 0)
          return i; 
        i = param1TimeCalculator1.wallTime.getSecond() - param1TimeCalculator2.wallTime.getSecond();
        if (i != 0)
          return i; 
        return 0;
      } 
      long l1 = param1TimeCalculator1.toMillis(false);
      long l2 = param1TimeCalculator2.toMillis(false);
      l2 = l1 - l2;
      if (l2 < 0L) {
        i = -1;
      } else if (l2 > 0L) {
        i = 1;
      } 
      return i;
    }
    
    public void copyFieldsToTime(Time param1Time) {
      param1Time.second = this.wallTime.getSecond();
      param1Time.minute = this.wallTime.getMinute();
      param1Time.hour = this.wallTime.getHour();
      param1Time.monthDay = this.wallTime.getMonthDay();
      param1Time.month = this.wallTime.getMonth();
      param1Time.year = this.wallTime.getYear();
      param1Time.weekDay = this.wallTime.getWeekDay();
      param1Time.yearDay = this.wallTime.getYearDay();
      param1Time.isDst = this.wallTime.getIsDst();
      param1Time.gmtoff = this.wallTime.getGmtOffset();
    }
    
    public void copyFieldsFromTime(Time param1Time) {
      this.wallTime.setSecond(param1Time.second);
      this.wallTime.setMinute(param1Time.minute);
      this.wallTime.setHour(param1Time.hour);
      this.wallTime.setMonthDay(param1Time.monthDay);
      this.wallTime.setMonth(param1Time.month);
      this.wallTime.setYear(param1Time.year);
      this.wallTime.setWeekDay(param1Time.weekDay);
      this.wallTime.setYearDay(param1Time.yearDay);
      this.wallTime.setIsDst(param1Time.isDst);
      this.wallTime.setGmtOffset((int)param1Time.gmtoff);
      if (!param1Time.allDay || (param1Time.second == 0 && param1Time.minute == 0 && param1Time.hour == 0)) {
        this.timezone = param1Time.timezone;
        updateZoneInfoFromTimeZone();
        return;
      } 
      throw new IllegalArgumentException("allDay is true but sec, min, hour are not 0.");
    }
  }
}
