package android.text.format;

import java.nio.CharBuffer;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Formatter;
import java.util.Locale;
import libcore.icu.LocaleData;
import libcore.util.ZoneInfo;

class TimeFormatter {
  private static final int DAYSPERLYEAR = 366;
  
  private static final int DAYSPERNYEAR = 365;
  
  private static final int DAYSPERWEEK = 7;
  
  private static final int FORCE_LOWER_CASE = -1;
  
  private static final int HOURSPERDAY = 24;
  
  private static final int MINSPERHOUR = 60;
  
  private static final int MONSPERYEAR = 12;
  
  private static final int SECSPERMIN = 60;
  
  private static String sDateOnlyFormat;
  
  private static String sDateTimeFormat;
  
  private static Locale sLocale;
  
  private static LocaleData sLocaleData;
  
  private static String sTimeOnlyFormat;
  
  private final String dateOnlyFormat;
  
  private final String dateTimeFormat;
  
  private final LocaleData localeData;
  
  private Formatter numberFormatter;
  
  private StringBuilder outputBuilder;
  
  private final String timeOnlyFormat;
  
  public TimeFormatter() {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial <init> : ()V
    //   4: ldc android/text/format/TimeFormatter
    //   6: monitorenter
    //   7: invokestatic getDefault : ()Ljava/util/Locale;
    //   10: astore_1
    //   11: getstatic android/text/format/TimeFormatter.sLocale : Ljava/util/Locale;
    //   14: ifnull -> 27
    //   17: aload_1
    //   18: getstatic android/text/format/TimeFormatter.sLocale : Ljava/util/Locale;
    //   21: invokevirtual equals : (Ljava/lang/Object;)Z
    //   24: ifne -> 69
    //   27: aload_1
    //   28: putstatic android/text/format/TimeFormatter.sLocale : Ljava/util/Locale;
    //   31: aload_1
    //   32: invokestatic get : (Ljava/util/Locale;)Llibcore/icu/LocaleData;
    //   35: putstatic android/text/format/TimeFormatter.sLocaleData : Llibcore/icu/LocaleData;
    //   38: invokestatic getSystem : ()Landroid/content/res/Resources;
    //   41: astore_1
    //   42: aload_1
    //   43: ldc 17041396
    //   45: invokevirtual getString : (I)Ljava/lang/String;
    //   48: putstatic android/text/format/TimeFormatter.sTimeOnlyFormat : Ljava/lang/String;
    //   51: aload_1
    //   52: ldc 17040673
    //   54: invokevirtual getString : (I)Ljava/lang/String;
    //   57: putstatic android/text/format/TimeFormatter.sDateOnlyFormat : Ljava/lang/String;
    //   60: aload_1
    //   61: ldc 17040025
    //   63: invokevirtual getString : (I)Ljava/lang/String;
    //   66: putstatic android/text/format/TimeFormatter.sDateTimeFormat : Ljava/lang/String;
    //   69: aload_0
    //   70: getstatic android/text/format/TimeFormatter.sDateTimeFormat : Ljava/lang/String;
    //   73: putfield dateTimeFormat : Ljava/lang/String;
    //   76: aload_0
    //   77: getstatic android/text/format/TimeFormatter.sTimeOnlyFormat : Ljava/lang/String;
    //   80: putfield timeOnlyFormat : Ljava/lang/String;
    //   83: aload_0
    //   84: getstatic android/text/format/TimeFormatter.sDateOnlyFormat : Ljava/lang/String;
    //   87: putfield dateOnlyFormat : Ljava/lang/String;
    //   90: aload_0
    //   91: getstatic android/text/format/TimeFormatter.sLocaleData : Llibcore/icu/LocaleData;
    //   94: putfield localeData : Llibcore/icu/LocaleData;
    //   97: ldc android/text/format/TimeFormatter
    //   99: monitorexit
    //   100: return
    //   101: astore_1
    //   102: ldc android/text/format/TimeFormatter
    //   104: monitorexit
    //   105: aload_1
    //   106: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #70	-> 0
    //   #71	-> 4
    //   #72	-> 7
    //   #74	-> 11
    //   #75	-> 27
    //   #76	-> 31
    //   #78	-> 38
    //   #79	-> 42
    //   #80	-> 51
    //   #81	-> 60
    //   #84	-> 69
    //   #85	-> 76
    //   #86	-> 83
    //   #87	-> 90
    //   #88	-> 97
    //   #89	-> 100
    //   #88	-> 101
    // Exception table:
    //   from	to	target	type
    //   7	11	101	finally
    //   11	27	101	finally
    //   27	31	101	finally
    //   31	38	101	finally
    //   38	42	101	finally
    //   42	51	101	finally
    //   51	60	101	finally
    //   60	69	101	finally
    //   69	76	101	finally
    //   76	83	101	finally
    //   83	90	101	finally
    //   90	97	101	finally
    //   97	100	101	finally
    //   102	105	101	finally
  }
  
  String formatMillisWithFixedFormat(long paramLong) {
    Instant instant = Instant.ofEpochMilli(paramLong);
    LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
    StringBuilder stringBuilder = new StringBuilder(19);
    stringBuilder.append(localDateTime.getYear());
    stringBuilder.append('-');
    append2DigitNumber(stringBuilder, localDateTime.getMonthValue());
    stringBuilder.append('-');
    append2DigitNumber(stringBuilder, localDateTime.getDayOfMonth());
    stringBuilder.append(' ');
    append2DigitNumber(stringBuilder, localDateTime.getHour());
    stringBuilder.append(':');
    append2DigitNumber(stringBuilder, localDateTime.getMinute());
    stringBuilder.append(':');
    append2DigitNumber(stringBuilder, localDateTime.getSecond());
    String str = stringBuilder.toString();
    return localizeDigits(str);
  }
  
  private static void append2DigitNumber(StringBuilder paramStringBuilder, int paramInt) {
    if (paramInt < 10)
      paramStringBuilder.append('0'); 
    paramStringBuilder.append(paramInt);
  }
  
  public String format(String paramString, ZoneInfo.WallTime paramWallTime, ZoneInfo paramZoneInfo) {
    try {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      this.outputBuilder = stringBuilder;
      Formatter formatter = new Formatter();
      this(stringBuilder, Locale.US);
      this.numberFormatter = formatter;
      formatInternal(paramString, paramWallTime, paramZoneInfo);
      paramString = stringBuilder.toString();
      paramString = localizeDigits(paramString);
      return paramString;
    } finally {
      this.outputBuilder = null;
      this.numberFormatter = null;
    } 
  }
  
  private String localizeDigits(String paramString) {
    if (this.localeData.zeroDigit == '0')
      return paramString; 
    int i = paramString.length();
    char c = this.localeData.zeroDigit;
    StringBuilder stringBuilder = new StringBuilder(i);
    for (byte b = 0; b < i; b++) {
      char c1 = paramString.charAt(b);
      char c2 = c1;
      if (c1 >= '0') {
        c2 = c1;
        if (c1 <= '9')
          c2 = (char)(c1 + c - 48); 
      } 
      stringBuilder.append(c2);
    } 
    return stringBuilder.toString();
  }
  
  private void formatInternal(String paramString, ZoneInfo.WallTime paramWallTime, ZoneInfo paramZoneInfo) {
    CharBuffer charBuffer = CharBuffer.wrap(paramString);
    while (charBuffer.remaining() > 0) {
      boolean bool = true;
      char c = charBuffer.get(charBuffer.position());
      if (c == '%')
        bool = handleToken(charBuffer, paramWallTime, paramZoneInfo); 
      if (bool)
        this.outputBuilder.append(charBuffer.get(charBuffer.position())); 
      charBuffer.position(charBuffer.position() + 1);
    } 
  }
  
  private boolean handleToken(CharBuffer paramCharBuffer, ZoneInfo.WallTime paramWallTime, ZoneInfo paramZoneInfo) {
    // Byte code:
    //   0: iconst_0
    //   1: istore #4
    //   3: aload_1
    //   4: invokevirtual remaining : ()I
    //   7: istore #5
    //   9: iconst_1
    //   10: istore #6
    //   12: iload #5
    //   14: iconst_1
    //   15: if_icmple -> 1954
    //   18: aload_1
    //   19: aload_1
    //   20: invokevirtual position : ()I
    //   23: iconst_1
    //   24: iadd
    //   25: invokevirtual position : (I)Ljava/nio/Buffer;
    //   28: pop
    //   29: aload_1
    //   30: aload_1
    //   31: invokevirtual position : ()I
    //   34: invokevirtual get : (I)C
    //   37: istore #7
    //   39: iload #7
    //   41: bipush #35
    //   43: if_icmpeq -> 1947
    //   46: iload #7
    //   48: bipush #43
    //   50: if_icmpeq -> 1936
    //   53: iload #7
    //   55: bipush #45
    //   57: if_icmpeq -> 1947
    //   60: iload #7
    //   62: bipush #48
    //   64: if_icmpeq -> 1947
    //   67: iload #7
    //   69: bipush #77
    //   71: if_icmpeq -> 1891
    //   74: bipush #12
    //   76: istore #5
    //   78: iload #7
    //   80: bipush #112
    //   82: if_icmpeq -> 1850
    //   85: iload #7
    //   87: bipush #79
    //   89: if_icmpeq -> 1847
    //   92: iload #7
    //   94: bipush #80
    //   96: if_icmpeq -> 1807
    //   99: iload #7
    //   101: bipush #94
    //   103: if_icmpeq -> 1947
    //   106: iload #7
    //   108: bipush #95
    //   110: if_icmpeq -> 1947
    //   113: bipush #7
    //   115: istore #8
    //   117: iload #7
    //   119: bipush #103
    //   121: if_icmpeq -> 1563
    //   124: ldc '?'
    //   126: astore #9
    //   128: iload #7
    //   130: bipush #104
    //   132: if_icmpeq -> 1517
    //   135: iload #7
    //   137: tableswitch default -> 188, 65 -> 1469, 66 -> 1368, 67 -> 1354, 68 -> 1344, 69 -> 1847, 70 -> 1334, 71 -> 1563, 72 -> 1289, 73 -> 1235
    //   188: iload #7
    //   190: tableswitch default -> 240, 82 -> 1225, 83 -> 1180, 84 -> 1170, 85 -> 1114, 86 -> 1563, 87 -> 1038, 88 -> 1026, 89 -> 1012, 90 -> 975
    //   240: iload #7
    //   242: tableswitch default -> 276, 97 -> 927, 98 -> 1517, 99 -> 915, 100 -> 870, 101 -> 825
    //   276: iload #7
    //   278: tableswitch default -> 312, 106 -> 778, 107 -> 733, 108 -> 679, 109 -> 632, 110 -> 620
    //   312: iload #7
    //   314: tableswitch default -> 364, 114 -> 610, 115 -> 588, 116 -> 576, 117 -> 532, 118 -> 522, 119 -> 496, 120 -> 484, 121 -> 470, 122 -> 366
    //   364: iconst_1
    //   365: ireturn
    //   366: aload_2
    //   367: invokevirtual getIsDst : ()I
    //   370: ifge -> 375
    //   373: iconst_0
    //   374: ireturn
    //   375: aload_2
    //   376: invokevirtual getGmtOffset : ()I
    //   379: istore #5
    //   381: iload #5
    //   383: ifge -> 398
    //   386: bipush #45
    //   388: istore #10
    //   390: iload #5
    //   392: ineg
    //   393: istore #5
    //   395: goto -> 402
    //   398: bipush #43
    //   400: istore #10
    //   402: aload_0
    //   403: getfield outputBuilder : Ljava/lang/StringBuilder;
    //   406: iload #10
    //   408: invokevirtual append : (C)Ljava/lang/StringBuilder;
    //   411: pop
    //   412: iload #5
    //   414: bipush #60
    //   416: idiv
    //   417: istore #8
    //   419: iload #8
    //   421: bipush #60
    //   423: idiv
    //   424: istore #5
    //   426: aload_0
    //   427: getfield numberFormatter : Ljava/util/Formatter;
    //   430: iload #4
    //   432: ldc '%04d'
    //   434: ldc '%4d'
    //   436: ldc '%d'
    //   438: ldc '%04d'
    //   440: invokestatic getFormat : (ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   443: iconst_1
    //   444: anewarray java/lang/Object
    //   447: dup
    //   448: iconst_0
    //   449: iload #5
    //   451: bipush #100
    //   453: imul
    //   454: iload #8
    //   456: bipush #60
    //   458: irem
    //   459: iadd
    //   460: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   463: aastore
    //   464: invokevirtual format : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;
    //   467: pop
    //   468: iconst_0
    //   469: ireturn
    //   470: aload_0
    //   471: aload_2
    //   472: invokevirtual getYear : ()I
    //   475: iconst_0
    //   476: iconst_1
    //   477: iload #4
    //   479: invokespecial outputYear : (IZZI)V
    //   482: iconst_0
    //   483: ireturn
    //   484: aload_0
    //   485: aload_0
    //   486: getfield dateOnlyFormat : Ljava/lang/String;
    //   489: aload_2
    //   490: aload_3
    //   491: invokespecial formatInternal : (Ljava/lang/String;Llibcore/util/ZoneInfo$WallTime;Llibcore/util/ZoneInfo;)V
    //   494: iconst_0
    //   495: ireturn
    //   496: aload_0
    //   497: getfield numberFormatter : Ljava/util/Formatter;
    //   500: ldc '%d'
    //   502: iconst_1
    //   503: anewarray java/lang/Object
    //   506: dup
    //   507: iconst_0
    //   508: aload_2
    //   509: invokevirtual getWeekDay : ()I
    //   512: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   515: aastore
    //   516: invokevirtual format : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;
    //   519: pop
    //   520: iconst_0
    //   521: ireturn
    //   522: aload_0
    //   523: ldc '%e-%b-%Y'
    //   525: aload_2
    //   526: aload_3
    //   527: invokespecial formatInternal : (Ljava/lang/String;Llibcore/util/ZoneInfo$WallTime;Llibcore/util/ZoneInfo;)V
    //   530: iconst_0
    //   531: ireturn
    //   532: aload_2
    //   533: invokevirtual getWeekDay : ()I
    //   536: ifne -> 546
    //   539: iload #8
    //   541: istore #5
    //   543: goto -> 552
    //   546: aload_2
    //   547: invokevirtual getWeekDay : ()I
    //   550: istore #5
    //   552: aload_0
    //   553: getfield numberFormatter : Ljava/util/Formatter;
    //   556: ldc '%d'
    //   558: iconst_1
    //   559: anewarray java/lang/Object
    //   562: dup
    //   563: iconst_0
    //   564: iload #5
    //   566: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   569: aastore
    //   570: invokevirtual format : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;
    //   573: pop
    //   574: iconst_0
    //   575: ireturn
    //   576: aload_0
    //   577: getfield outputBuilder : Ljava/lang/StringBuilder;
    //   580: bipush #9
    //   582: invokevirtual append : (C)Ljava/lang/StringBuilder;
    //   585: pop
    //   586: iconst_0
    //   587: ireturn
    //   588: aload_2
    //   589: aload_3
    //   590: invokevirtual mktime : (Llibcore/util/ZoneInfo;)I
    //   593: istore #5
    //   595: aload_0
    //   596: getfield outputBuilder : Ljava/lang/StringBuilder;
    //   599: iload #5
    //   601: invokestatic toString : (I)Ljava/lang/String;
    //   604: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   607: pop
    //   608: iconst_0
    //   609: ireturn
    //   610: aload_0
    //   611: ldc '%I:%M:%S %p'
    //   613: aload_2
    //   614: aload_3
    //   615: invokespecial formatInternal : (Ljava/lang/String;Llibcore/util/ZoneInfo$WallTime;Llibcore/util/ZoneInfo;)V
    //   618: iconst_0
    //   619: ireturn
    //   620: aload_0
    //   621: getfield outputBuilder : Ljava/lang/StringBuilder;
    //   624: bipush #10
    //   626: invokevirtual append : (C)Ljava/lang/StringBuilder;
    //   629: pop
    //   630: iconst_0
    //   631: ireturn
    //   632: aload_0
    //   633: getfield numberFormatter : Ljava/util/Formatter;
    //   636: astore_1
    //   637: iload #4
    //   639: ldc '%02d'
    //   641: ldc '%2d'
    //   643: ldc '%d'
    //   645: ldc '%02d'
    //   647: invokestatic getFormat : (ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   650: astore_3
    //   651: aload_2
    //   652: invokevirtual getMonth : ()I
    //   655: istore #5
    //   657: aload_1
    //   658: aload_3
    //   659: iconst_1
    //   660: anewarray java/lang/Object
    //   663: dup
    //   664: iconst_0
    //   665: iload #5
    //   667: iconst_1
    //   668: iadd
    //   669: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   672: aastore
    //   673: invokevirtual format : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;
    //   676: pop
    //   677: iconst_0
    //   678: ireturn
    //   679: aload_2
    //   680: invokevirtual getHour : ()I
    //   683: bipush #12
    //   685: irem
    //   686: ifeq -> 698
    //   689: aload_2
    //   690: invokevirtual getHour : ()I
    //   693: bipush #12
    //   695: irem
    //   696: istore #5
    //   698: aload_0
    //   699: getfield numberFormatter : Ljava/util/Formatter;
    //   702: iload #4
    //   704: ldc '%2d'
    //   706: ldc '%2d'
    //   708: ldc '%d'
    //   710: ldc '%02d'
    //   712: invokestatic getFormat : (ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   715: iconst_1
    //   716: anewarray java/lang/Object
    //   719: dup
    //   720: iconst_0
    //   721: iload #5
    //   723: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   726: aastore
    //   727: invokevirtual format : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;
    //   730: pop
    //   731: iconst_0
    //   732: ireturn
    //   733: aload_0
    //   734: getfield numberFormatter : Ljava/util/Formatter;
    //   737: astore_3
    //   738: iload #4
    //   740: ldc '%2d'
    //   742: ldc '%2d'
    //   744: ldc '%d'
    //   746: ldc '%02d'
    //   748: invokestatic getFormat : (ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   751: astore_1
    //   752: aload_2
    //   753: invokevirtual getHour : ()I
    //   756: istore #5
    //   758: aload_3
    //   759: aload_1
    //   760: iconst_1
    //   761: anewarray java/lang/Object
    //   764: dup
    //   765: iconst_0
    //   766: iload #5
    //   768: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   771: aastore
    //   772: invokevirtual format : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;
    //   775: pop
    //   776: iconst_0
    //   777: ireturn
    //   778: aload_2
    //   779: invokevirtual getYearDay : ()I
    //   782: istore #5
    //   784: aload_0
    //   785: getfield numberFormatter : Ljava/util/Formatter;
    //   788: astore_2
    //   789: iload #4
    //   791: ldc '%03d'
    //   793: ldc '%3d'
    //   795: ldc '%d'
    //   797: ldc '%03d'
    //   799: invokestatic getFormat : (ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   802: astore_1
    //   803: aload_2
    //   804: aload_1
    //   805: iconst_1
    //   806: anewarray java/lang/Object
    //   809: dup
    //   810: iconst_0
    //   811: iload #5
    //   813: iconst_1
    //   814: iadd
    //   815: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   818: aastore
    //   819: invokevirtual format : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;
    //   822: pop
    //   823: iconst_0
    //   824: ireturn
    //   825: aload_0
    //   826: getfield numberFormatter : Ljava/util/Formatter;
    //   829: astore_3
    //   830: iload #4
    //   832: ldc '%2d'
    //   834: ldc '%2d'
    //   836: ldc '%d'
    //   838: ldc '%02d'
    //   840: invokestatic getFormat : (ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   843: astore_1
    //   844: aload_2
    //   845: invokevirtual getMonthDay : ()I
    //   848: istore #5
    //   850: aload_3
    //   851: aload_1
    //   852: iconst_1
    //   853: anewarray java/lang/Object
    //   856: dup
    //   857: iconst_0
    //   858: iload #5
    //   860: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   863: aastore
    //   864: invokevirtual format : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;
    //   867: pop
    //   868: iconst_0
    //   869: ireturn
    //   870: aload_0
    //   871: getfield numberFormatter : Ljava/util/Formatter;
    //   874: astore_1
    //   875: iload #4
    //   877: ldc '%02d'
    //   879: ldc '%2d'
    //   881: ldc '%d'
    //   883: ldc '%02d'
    //   885: invokestatic getFormat : (ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   888: astore_3
    //   889: aload_2
    //   890: invokevirtual getMonthDay : ()I
    //   893: istore #5
    //   895: aload_1
    //   896: aload_3
    //   897: iconst_1
    //   898: anewarray java/lang/Object
    //   901: dup
    //   902: iconst_0
    //   903: iload #5
    //   905: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   908: aastore
    //   909: invokevirtual format : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;
    //   912: pop
    //   913: iconst_0
    //   914: ireturn
    //   915: aload_0
    //   916: aload_0
    //   917: getfield dateTimeFormat : Ljava/lang/String;
    //   920: aload_2
    //   921: aload_3
    //   922: invokespecial formatInternal : (Ljava/lang/String;Llibcore/util/ZoneInfo$WallTime;Llibcore/util/ZoneInfo;)V
    //   925: iconst_0
    //   926: ireturn
    //   927: aload_2
    //   928: invokevirtual getWeekDay : ()I
    //   931: iflt -> 965
    //   934: aload_2
    //   935: invokevirtual getWeekDay : ()I
    //   938: bipush #7
    //   940: if_icmplt -> 946
    //   943: goto -> 965
    //   946: aload_0
    //   947: getfield localeData : Llibcore/icu/LocaleData;
    //   950: getfield shortWeekdayNames : [Ljava/lang/String;
    //   953: aload_2
    //   954: invokevirtual getWeekDay : ()I
    //   957: iconst_1
    //   958: iadd
    //   959: aaload
    //   960: astore #9
    //   962: goto -> 965
    //   965: aload_0
    //   966: aload #9
    //   968: iload #4
    //   970: invokespecial modifyAndAppend : (Ljava/lang/CharSequence;I)V
    //   973: iconst_0
    //   974: ireturn
    //   975: aload_2
    //   976: invokevirtual getIsDst : ()I
    //   979: ifge -> 984
    //   982: iconst_0
    //   983: ireturn
    //   984: aload_2
    //   985: invokevirtual getIsDst : ()I
    //   988: ifeq -> 994
    //   991: goto -> 997
    //   994: iconst_0
    //   995: istore #6
    //   997: aload_0
    //   998: aload_3
    //   999: iload #6
    //   1001: iconst_0
    //   1002: invokevirtual getDisplayName : (ZI)Ljava/lang/String;
    //   1005: iload #4
    //   1007: invokespecial modifyAndAppend : (Ljava/lang/CharSequence;I)V
    //   1010: iconst_0
    //   1011: ireturn
    //   1012: aload_0
    //   1013: aload_2
    //   1014: invokevirtual getYear : ()I
    //   1017: iconst_1
    //   1018: iconst_1
    //   1019: iload #4
    //   1021: invokespecial outputYear : (IZZI)V
    //   1024: iconst_0
    //   1025: ireturn
    //   1026: aload_0
    //   1027: aload_0
    //   1028: getfield timeOnlyFormat : Ljava/lang/String;
    //   1031: aload_2
    //   1032: aload_3
    //   1033: invokespecial formatInternal : (Ljava/lang/String;Llibcore/util/ZoneInfo$WallTime;Llibcore/util/ZoneInfo;)V
    //   1036: iconst_0
    //   1037: ireturn
    //   1038: aload_2
    //   1039: invokevirtual getYearDay : ()I
    //   1042: istore #8
    //   1044: aload_2
    //   1045: invokevirtual getWeekDay : ()I
    //   1048: ifeq -> 1062
    //   1051: aload_2
    //   1052: invokevirtual getWeekDay : ()I
    //   1055: iconst_1
    //   1056: isub
    //   1057: istore #5
    //   1059: goto -> 1066
    //   1062: bipush #6
    //   1064: istore #5
    //   1066: iload #8
    //   1068: bipush #7
    //   1070: iadd
    //   1071: iload #5
    //   1073: isub
    //   1074: bipush #7
    //   1076: idiv
    //   1077: istore #5
    //   1079: aload_0
    //   1080: getfield numberFormatter : Ljava/util/Formatter;
    //   1083: iload #4
    //   1085: ldc '%02d'
    //   1087: ldc '%2d'
    //   1089: ldc '%d'
    //   1091: ldc '%02d'
    //   1093: invokestatic getFormat : (ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   1096: iconst_1
    //   1097: anewarray java/lang/Object
    //   1100: dup
    //   1101: iconst_0
    //   1102: iload #5
    //   1104: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1107: aastore
    //   1108: invokevirtual format : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;
    //   1111: pop
    //   1112: iconst_0
    //   1113: ireturn
    //   1114: aload_0
    //   1115: getfield numberFormatter : Ljava/util/Formatter;
    //   1118: astore_3
    //   1119: iload #4
    //   1121: ldc '%02d'
    //   1123: ldc '%2d'
    //   1125: ldc '%d'
    //   1127: ldc '%02d'
    //   1129: invokestatic getFormat : (ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   1132: astore_1
    //   1133: aload_2
    //   1134: invokevirtual getYearDay : ()I
    //   1137: bipush #7
    //   1139: iadd
    //   1140: aload_2
    //   1141: invokevirtual getWeekDay : ()I
    //   1144: isub
    //   1145: bipush #7
    //   1147: idiv
    //   1148: istore #5
    //   1150: aload_3
    //   1151: aload_1
    //   1152: iconst_1
    //   1153: anewarray java/lang/Object
    //   1156: dup
    //   1157: iconst_0
    //   1158: iload #5
    //   1160: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1163: aastore
    //   1164: invokevirtual format : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;
    //   1167: pop
    //   1168: iconst_0
    //   1169: ireturn
    //   1170: aload_0
    //   1171: ldc '%H:%M:%S'
    //   1173: aload_2
    //   1174: aload_3
    //   1175: invokespecial formatInternal : (Ljava/lang/String;Llibcore/util/ZoneInfo$WallTime;Llibcore/util/ZoneInfo;)V
    //   1178: iconst_0
    //   1179: ireturn
    //   1180: aload_0
    //   1181: getfield numberFormatter : Ljava/util/Formatter;
    //   1184: astore_3
    //   1185: iload #4
    //   1187: ldc '%02d'
    //   1189: ldc '%2d'
    //   1191: ldc '%d'
    //   1193: ldc '%02d'
    //   1195: invokestatic getFormat : (ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   1198: astore_1
    //   1199: aload_2
    //   1200: invokevirtual getSecond : ()I
    //   1203: istore #5
    //   1205: aload_3
    //   1206: aload_1
    //   1207: iconst_1
    //   1208: anewarray java/lang/Object
    //   1211: dup
    //   1212: iconst_0
    //   1213: iload #5
    //   1215: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1218: aastore
    //   1219: invokevirtual format : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;
    //   1222: pop
    //   1223: iconst_0
    //   1224: ireturn
    //   1225: aload_0
    //   1226: ldc '%H:%M'
    //   1228: aload_2
    //   1229: aload_3
    //   1230: invokespecial formatInternal : (Ljava/lang/String;Llibcore/util/ZoneInfo$WallTime;Llibcore/util/ZoneInfo;)V
    //   1233: iconst_0
    //   1234: ireturn
    //   1235: aload_2
    //   1236: invokevirtual getHour : ()I
    //   1239: bipush #12
    //   1241: irem
    //   1242: ifeq -> 1254
    //   1245: aload_2
    //   1246: invokevirtual getHour : ()I
    //   1249: bipush #12
    //   1251: irem
    //   1252: istore #5
    //   1254: aload_0
    //   1255: getfield numberFormatter : Ljava/util/Formatter;
    //   1258: iload #4
    //   1260: ldc '%02d'
    //   1262: ldc '%2d'
    //   1264: ldc '%d'
    //   1266: ldc '%02d'
    //   1268: invokestatic getFormat : (ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   1271: iconst_1
    //   1272: anewarray java/lang/Object
    //   1275: dup
    //   1276: iconst_0
    //   1277: iload #5
    //   1279: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1282: aastore
    //   1283: invokevirtual format : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;
    //   1286: pop
    //   1287: iconst_0
    //   1288: ireturn
    //   1289: aload_0
    //   1290: getfield numberFormatter : Ljava/util/Formatter;
    //   1293: astore_1
    //   1294: iload #4
    //   1296: ldc '%02d'
    //   1298: ldc '%2d'
    //   1300: ldc '%d'
    //   1302: ldc '%02d'
    //   1304: invokestatic getFormat : (ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   1307: astore_3
    //   1308: aload_2
    //   1309: invokevirtual getHour : ()I
    //   1312: istore #5
    //   1314: aload_1
    //   1315: aload_3
    //   1316: iconst_1
    //   1317: anewarray java/lang/Object
    //   1320: dup
    //   1321: iconst_0
    //   1322: iload #5
    //   1324: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1327: aastore
    //   1328: invokevirtual format : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;
    //   1331: pop
    //   1332: iconst_0
    //   1333: ireturn
    //   1334: aload_0
    //   1335: ldc '%Y-%m-%d'
    //   1337: aload_2
    //   1338: aload_3
    //   1339: invokespecial formatInternal : (Ljava/lang/String;Llibcore/util/ZoneInfo$WallTime;Llibcore/util/ZoneInfo;)V
    //   1342: iconst_0
    //   1343: ireturn
    //   1344: aload_0
    //   1345: ldc '%m/%d/%y'
    //   1347: aload_2
    //   1348: aload_3
    //   1349: invokespecial formatInternal : (Ljava/lang/String;Llibcore/util/ZoneInfo$WallTime;Llibcore/util/ZoneInfo;)V
    //   1352: iconst_0
    //   1353: ireturn
    //   1354: aload_0
    //   1355: aload_2
    //   1356: invokevirtual getYear : ()I
    //   1359: iconst_1
    //   1360: iconst_0
    //   1361: iload #4
    //   1363: invokespecial outputYear : (IZZI)V
    //   1366: iconst_0
    //   1367: ireturn
    //   1368: iload #4
    //   1370: bipush #45
    //   1372: if_icmpne -> 1423
    //   1375: aload_2
    //   1376: invokevirtual getMonth : ()I
    //   1379: iflt -> 1410
    //   1382: aload_2
    //   1383: invokevirtual getMonth : ()I
    //   1386: bipush #12
    //   1388: if_icmplt -> 1394
    //   1391: goto -> 1410
    //   1394: aload_0
    //   1395: getfield localeData : Llibcore/icu/LocaleData;
    //   1398: getfield longStandAloneMonthNames : [Ljava/lang/String;
    //   1401: aload_2
    //   1402: invokevirtual getMonth : ()I
    //   1405: aaload
    //   1406: astore_1
    //   1407: goto -> 1413
    //   1410: ldc '?'
    //   1412: astore_1
    //   1413: aload_0
    //   1414: aload_1
    //   1415: iload #4
    //   1417: invokespecial modifyAndAppend : (Ljava/lang/CharSequence;I)V
    //   1420: goto -> 1467
    //   1423: aload_2
    //   1424: invokevirtual getMonth : ()I
    //   1427: iflt -> 1459
    //   1430: aload_2
    //   1431: invokevirtual getMonth : ()I
    //   1434: bipush #12
    //   1436: if_icmplt -> 1442
    //   1439: goto -> 1459
    //   1442: aload_0
    //   1443: getfield localeData : Llibcore/icu/LocaleData;
    //   1446: getfield longMonthNames : [Ljava/lang/String;
    //   1449: aload_2
    //   1450: invokevirtual getMonth : ()I
    //   1453: aaload
    //   1454: astore #9
    //   1456: goto -> 1459
    //   1459: aload_0
    //   1460: aload #9
    //   1462: iload #4
    //   1464: invokespecial modifyAndAppend : (Ljava/lang/CharSequence;I)V
    //   1467: iconst_0
    //   1468: ireturn
    //   1469: aload_2
    //   1470: invokevirtual getWeekDay : ()I
    //   1473: iflt -> 1507
    //   1476: aload_2
    //   1477: invokevirtual getWeekDay : ()I
    //   1480: bipush #7
    //   1482: if_icmplt -> 1488
    //   1485: goto -> 1507
    //   1488: aload_0
    //   1489: getfield localeData : Llibcore/icu/LocaleData;
    //   1492: getfield longWeekdayNames : [Ljava/lang/String;
    //   1495: aload_2
    //   1496: invokevirtual getWeekDay : ()I
    //   1499: iconst_1
    //   1500: iadd
    //   1501: aaload
    //   1502: astore #9
    //   1504: goto -> 1507
    //   1507: aload_0
    //   1508: aload #9
    //   1510: iload #4
    //   1512: invokespecial modifyAndAppend : (Ljava/lang/CharSequence;I)V
    //   1515: iconst_0
    //   1516: ireturn
    //   1517: aload_2
    //   1518: invokevirtual getMonth : ()I
    //   1521: iflt -> 1553
    //   1524: aload_2
    //   1525: invokevirtual getMonth : ()I
    //   1528: bipush #12
    //   1530: if_icmplt -> 1536
    //   1533: goto -> 1553
    //   1536: aload_0
    //   1537: getfield localeData : Llibcore/icu/LocaleData;
    //   1540: getfield shortMonthNames : [Ljava/lang/String;
    //   1543: aload_2
    //   1544: invokevirtual getMonth : ()I
    //   1547: aaload
    //   1548: astore #9
    //   1550: goto -> 1553
    //   1553: aload_0
    //   1554: aload #9
    //   1556: iload #4
    //   1558: invokespecial modifyAndAppend : (Ljava/lang/CharSequence;I)V
    //   1561: iconst_0
    //   1562: ireturn
    //   1563: aload_2
    //   1564: invokevirtual getYear : ()I
    //   1567: istore #5
    //   1569: aload_2
    //   1570: invokevirtual getYearDay : ()I
    //   1573: istore #8
    //   1575: aload_2
    //   1576: invokevirtual getWeekDay : ()I
    //   1579: istore #11
    //   1581: iload #5
    //   1583: invokestatic isLeap : (I)Z
    //   1586: istore #6
    //   1588: sipush #366
    //   1591: istore #12
    //   1593: iload #6
    //   1595: ifeq -> 1606
    //   1598: sipush #366
    //   1601: istore #13
    //   1603: goto -> 1611
    //   1606: sipush #365
    //   1609: istore #13
    //   1611: iload #8
    //   1613: bipush #11
    //   1615: iadd
    //   1616: iload #11
    //   1618: isub
    //   1619: bipush #7
    //   1621: irem
    //   1622: iconst_3
    //   1623: isub
    //   1624: istore #14
    //   1626: iload #14
    //   1628: iload #13
    //   1630: bipush #7
    //   1632: irem
    //   1633: isub
    //   1634: istore #15
    //   1636: iload #15
    //   1638: istore #16
    //   1640: iload #15
    //   1642: bipush #-3
    //   1644: if_icmpge -> 1654
    //   1647: iload #15
    //   1649: bipush #7
    //   1651: iadd
    //   1652: istore #16
    //   1654: iload #8
    //   1656: iload #16
    //   1658: iload #13
    //   1660: iadd
    //   1661: if_icmplt -> 1673
    //   1664: iinc #5, 1
    //   1667: iconst_1
    //   1668: istore #8
    //   1670: goto -> 1692
    //   1673: iload #8
    //   1675: iload #14
    //   1677: if_icmplt -> 1767
    //   1680: iload #8
    //   1682: iload #14
    //   1684: isub
    //   1685: bipush #7
    //   1687: idiv
    //   1688: iconst_1
    //   1689: iadd
    //   1690: istore #8
    //   1692: iload #7
    //   1694: bipush #86
    //   1696: if_icmpne -> 1735
    //   1699: aload_0
    //   1700: getfield numberFormatter : Ljava/util/Formatter;
    //   1703: iload #4
    //   1705: ldc '%02d'
    //   1707: ldc '%2d'
    //   1709: ldc '%d'
    //   1711: ldc '%02d'
    //   1713: invokestatic getFormat : (ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   1716: iconst_1
    //   1717: anewarray java/lang/Object
    //   1720: dup
    //   1721: iconst_0
    //   1722: iload #8
    //   1724: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1727: aastore
    //   1728: invokevirtual format : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;
    //   1731: pop
    //   1732: goto -> 1765
    //   1735: iload #7
    //   1737: bipush #103
    //   1739: if_icmpne -> 1755
    //   1742: aload_0
    //   1743: iload #5
    //   1745: iconst_0
    //   1746: iconst_1
    //   1747: iload #4
    //   1749: invokespecial outputYear : (IZZI)V
    //   1752: goto -> 1765
    //   1755: aload_0
    //   1756: iload #5
    //   1758: iconst_1
    //   1759: iconst_1
    //   1760: iload #4
    //   1762: invokespecial outputYear : (IZZI)V
    //   1765: iconst_0
    //   1766: ireturn
    //   1767: iload #5
    //   1769: iconst_1
    //   1770: isub
    //   1771: istore #13
    //   1773: iload #13
    //   1775: invokestatic isLeap : (I)Z
    //   1778: ifeq -> 1788
    //   1781: iload #12
    //   1783: istore #5
    //   1785: goto -> 1793
    //   1788: sipush #365
    //   1791: istore #5
    //   1793: iload #8
    //   1795: iload #5
    //   1797: iadd
    //   1798: istore #8
    //   1800: iload #13
    //   1802: istore #5
    //   1804: goto -> 1581
    //   1807: aload_2
    //   1808: invokevirtual getHour : ()I
    //   1811: bipush #12
    //   1813: if_icmplt -> 1829
    //   1816: aload_0
    //   1817: getfield localeData : Llibcore/icu/LocaleData;
    //   1820: getfield amPm : [Ljava/lang/String;
    //   1823: iconst_1
    //   1824: aaload
    //   1825: astore_1
    //   1826: goto -> 1839
    //   1829: aload_0
    //   1830: getfield localeData : Llibcore/icu/LocaleData;
    //   1833: getfield amPm : [Ljava/lang/String;
    //   1836: iconst_0
    //   1837: aaload
    //   1838: astore_1
    //   1839: aload_0
    //   1840: aload_1
    //   1841: iconst_m1
    //   1842: invokespecial modifyAndAppend : (Ljava/lang/CharSequence;I)V
    //   1845: iconst_0
    //   1846: ireturn
    //   1847: goto -> 3
    //   1850: aload_2
    //   1851: invokevirtual getHour : ()I
    //   1854: bipush #12
    //   1856: if_icmplt -> 1872
    //   1859: aload_0
    //   1860: getfield localeData : Llibcore/icu/LocaleData;
    //   1863: getfield amPm : [Ljava/lang/String;
    //   1866: iconst_1
    //   1867: aaload
    //   1868: astore_1
    //   1869: goto -> 1882
    //   1872: aload_0
    //   1873: getfield localeData : Llibcore/icu/LocaleData;
    //   1876: getfield amPm : [Ljava/lang/String;
    //   1879: iconst_0
    //   1880: aaload
    //   1881: astore_1
    //   1882: aload_0
    //   1883: aload_1
    //   1884: iload #4
    //   1886: invokespecial modifyAndAppend : (Ljava/lang/CharSequence;I)V
    //   1889: iconst_0
    //   1890: ireturn
    //   1891: aload_0
    //   1892: getfield numberFormatter : Ljava/util/Formatter;
    //   1895: astore_3
    //   1896: iload #4
    //   1898: ldc '%02d'
    //   1900: ldc '%2d'
    //   1902: ldc '%d'
    //   1904: ldc '%02d'
    //   1906: invokestatic getFormat : (ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   1909: astore_1
    //   1910: aload_2
    //   1911: invokevirtual getMinute : ()I
    //   1914: istore #5
    //   1916: aload_3
    //   1917: aload_1
    //   1918: iconst_1
    //   1919: anewarray java/lang/Object
    //   1922: dup
    //   1923: iconst_0
    //   1924: iload #5
    //   1926: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1929: aastore
    //   1930: invokevirtual format : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;
    //   1933: pop
    //   1934: iconst_0
    //   1935: ireturn
    //   1936: aload_0
    //   1937: ldc_w '%a %b %e %H:%M:%S %Z %Y'
    //   1940: aload_2
    //   1941: aload_3
    //   1942: invokespecial formatInternal : (Ljava/lang/String;Llibcore/util/ZoneInfo$WallTime;Llibcore/util/ZoneInfo;)V
    //   1945: iconst_0
    //   1946: ireturn
    //   1947: iload #7
    //   1949: istore #4
    //   1951: goto -> 3
    //   1954: iconst_1
    //   1955: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #208	-> 0
    //   #209	-> 3
    //   #211	-> 18
    //   #212	-> 29
    //   #213	-> 39
    //   #441	-> 364
    //   #417	-> 366
    //   #418	-> 373
    //   #420	-> 375
    //   #422	-> 381
    //   #423	-> 386
    //   #424	-> 390
    //   #426	-> 398
    //   #428	-> 402
    //   #429	-> 412
    //   #430	-> 419
    //   #431	-> 426
    //   #432	-> 468
    //   #404	-> 470
    //   #405	-> 482
    //   #401	-> 484
    //   #402	-> 494
    //   #395	-> 496
    //   #396	-> 520
    //   #386	-> 522
    //   #387	-> 530
    //   #343	-> 532
    //   #344	-> 552
    //   #345	-> 574
    //   #335	-> 576
    //   #336	-> 586
    //   #328	-> 588
    //   #329	-> 595
    //   #330	-> 608
    //   #321	-> 610
    //   #322	-> 618
    //   #307	-> 620
    //   #308	-> 630
    //   #303	-> 632
    //   #304	-> 651
    //   #303	-> 657
    //   #305	-> 677
    //   #295	-> 679
    //   #296	-> 698
    //   #297	-> 731
    //   #291	-> 733
    //   #292	-> 752
    //   #291	-> 758
    //   #293	-> 776
    //   #286	-> 778
    //   #287	-> 784
    //   #288	-> 803
    //   #287	-> 803
    //   #289	-> 823
    //   #271	-> 825
    //   #272	-> 844
    //   #271	-> 850
    //   #273	-> 868
    //   #256	-> 870
    //   #257	-> 889
    //   #256	-> 895
    //   #258	-> 913
    //   #250	-> 915
    //   #251	-> 925
    //   #221	-> 927
    //   #222	-> 934
    //   #223	-> 946
    //   #221	-> 965
    //   #225	-> 973
    //   #410	-> 975
    //   #411	-> 982
    //   #413	-> 984
    //   #414	-> 997
    //   #415	-> 1010
    //   #407	-> 1012
    //   #408	-> 1024
    //   #398	-> 1026
    //   #399	-> 1036
    //   #389	-> 1038
    //   #390	-> 1044
    //   #391	-> 1062
    //   #392	-> 1079
    //   #393	-> 1112
    //   #338	-> 1114
    //   #339	-> 1133
    //   #338	-> 1150
    //   #341	-> 1168
    //   #332	-> 1170
    //   #333	-> 1178
    //   #324	-> 1180
    //   #325	-> 1199
    //   #324	-> 1205
    //   #326	-> 1223
    //   #318	-> 1225
    //   #319	-> 1233
    //   #282	-> 1235
    //   #283	-> 1254
    //   #284	-> 1287
    //   #278	-> 1289
    //   #279	-> 1308
    //   #278	-> 1314
    //   #280	-> 1332
    //   #275	-> 1334
    //   #276	-> 1342
    //   #253	-> 1344
    //   #254	-> 1352
    //   #247	-> 1354
    //   #248	-> 1366
    //   #227	-> 1368
    //   #228	-> 1375
    //   #229	-> 1382
    //   #231	-> 1394
    //   #230	-> 1410
    //   #228	-> 1413
    //   #234	-> 1423
    //   #235	-> 1430
    //   #236	-> 1442
    //   #234	-> 1459
    //   #239	-> 1467
    //   #215	-> 1469
    //   #216	-> 1476
    //   #217	-> 1488
    //   #215	-> 1507
    //   #219	-> 1515
    //   #242	-> 1517
    //   #243	-> 1536
    //   #242	-> 1553
    //   #245	-> 1561
    //   #350	-> 1563
    //   #351	-> 1569
    //   #352	-> 1575
    //   #355	-> 1581
    //   #357	-> 1611
    //   #359	-> 1626
    //   #360	-> 1636
    //   #361	-> 1647
    //   #363	-> 1654
    //   #364	-> 1654
    //   #365	-> 1664
    //   #366	-> 1667
    //   #367	-> 1670
    //   #369	-> 1673
    //   #370	-> 1680
    //   #371	-> 1692
    //   #376	-> 1692
    //   #377	-> 1699
    //   #378	-> 1735
    //   #379	-> 1742
    //   #381	-> 1755
    //   #383	-> 1765
    //   #373	-> 1767
    //   #374	-> 1773
    //   #375	-> 1800
    //   #314	-> 1807
    //   #315	-> 1829
    //   #314	-> 1839
    //   #316	-> 1845
    //   #262	-> 1847
    //   #310	-> 1850
    //   #311	-> 1872
    //   #310	-> 1882
    //   #312	-> 1889
    //   #299	-> 1891
    //   #300	-> 1910
    //   #299	-> 1916
    //   #301	-> 1934
    //   #435	-> 1936
    //   #436	-> 1945
    //   #268	-> 1947
    //   #269	-> 1951
    //   #444	-> 1954
  }
  
  private void modifyAndAppend(CharSequence paramCharSequence, int paramInt) {
    if (paramInt != -1) {
      if (paramInt != 35) {
        if (paramInt != 94) {
          this.outputBuilder.append(paramCharSequence);
        } else {
          for (paramInt = 0; paramInt < paramCharSequence.length(); paramInt++)
            this.outputBuilder.append(brokenToUpper(paramCharSequence.charAt(paramInt))); 
        } 
      } else {
        for (paramInt = 0; paramInt < paramCharSequence.length(); paramInt++) {
          char c1;
          char c = paramCharSequence.charAt(paramInt);
          if (brokenIsUpper(c)) {
            c1 = brokenToLower(c);
          } else {
            c1 = c;
            if (brokenIsLower(c))
              c1 = brokenToUpper(c); 
          } 
          this.outputBuilder.append(c1);
        } 
      } 
    } else {
      for (paramInt = 0; paramInt < paramCharSequence.length(); paramInt++)
        this.outputBuilder.append(brokenToLower(paramCharSequence.charAt(paramInt))); 
    } 
  }
  
  private void outputYear(int paramInt1, boolean paramBoolean1, boolean paramBoolean2, int paramInt2) {
    int i = paramInt1 % 100;
    int j = paramInt1 / 100 + i / 100;
    int k = i % 100;
    if (k < 0 && j > 0) {
      paramInt1 = k + 100;
      i = j - 1;
    } else {
      paramInt1 = k;
      i = j;
      if (j < 0) {
        paramInt1 = k;
        i = j;
        if (k > 0) {
          paramInt1 = k - 100;
          i = j + 1;
        } 
      } 
    } 
    if (paramBoolean1)
      if (i == 0 && paramInt1 < 0) {
        this.outputBuilder.append("-0");
      } else {
        this.numberFormatter.format(getFormat(paramInt2, "%02d", "%2d", "%d", "%02d"), new Object[] { Integer.valueOf(i) });
      }  
    if (paramBoolean2) {
      if (paramInt1 < 0)
        paramInt1 = -paramInt1; 
      this.numberFormatter.format(getFormat(paramInt2, "%02d", "%2d", "%d", "%02d"), new Object[] { Integer.valueOf(paramInt1) });
    } 
  }
  
  private static String getFormat(int paramInt, String paramString1, String paramString2, String paramString3, String paramString4) {
    if (paramInt != 45) {
      if (paramInt != 48) {
        if (paramInt != 95)
          return paramString1; 
        return paramString2;
      } 
      return paramString4;
    } 
    return paramString3;
  }
  
  private static boolean isLeap(int paramInt) {
    boolean bool;
    if (paramInt % 4 == 0 && (paramInt % 100 != 0 || paramInt % 400 == 0)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private static boolean brokenIsUpper(char paramChar) {
    boolean bool;
    if (paramChar >= 'A' && paramChar <= 'Z') {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private static boolean brokenIsLower(char paramChar) {
    boolean bool;
    if (paramChar >= 'a' && paramChar <= 'z') {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private static char brokenToLower(char paramChar) {
    if (paramChar >= 'A' && paramChar <= 'Z')
      return (char)(paramChar - 65 + 97); 
    return paramChar;
  }
  
  private static char brokenToUpper(char paramChar) {
    if (paramChar >= 'a' && paramChar <= 'z')
      return (char)(paramChar - 97 + 65); 
    return paramChar;
  }
}
