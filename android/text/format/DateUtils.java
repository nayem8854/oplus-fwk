package android.text.format;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.icu.text.MeasureFormat;
import android.icu.util.Measure;
import android.icu.util.MeasureUnit;
import java.io.IOException;
import java.text.DateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Formatter;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;
import libcore.icu.DateIntervalFormat;
import libcore.icu.LocaleData;
import libcore.icu.RelativeDateTimeFormatter;

public class DateUtils {
  @Deprecated
  public static final String ABBREV_MONTH_FORMAT = "%b";
  
  public static final String ABBREV_WEEKDAY_FORMAT = "%a";
  
  public static final long DAY_IN_MILLIS = 86400000L;
  
  @Deprecated
  public static final int FORMAT_12HOUR = 64;
  
  @Deprecated
  public static final int FORMAT_24HOUR = 128;
  
  public static final int FORMAT_ABBREV_ALL = 524288;
  
  public static final int FORMAT_ABBREV_MONTH = 65536;
  
  public static final int FORMAT_ABBREV_RELATIVE = 262144;
  
  public static final int FORMAT_ABBREV_TIME = 16384;
  
  public static final int FORMAT_ABBREV_WEEKDAY = 32768;
  
  @Deprecated
  public static final int FORMAT_CAP_AMPM = 256;
  
  @Deprecated
  public static final int FORMAT_CAP_MIDNIGHT = 4096;
  
  @Deprecated
  public static final int FORMAT_CAP_NOON = 1024;
  
  @Deprecated
  public static final int FORMAT_CAP_NOON_MIDNIGHT = 5120;
  
  public static final int FORMAT_NO_MIDNIGHT = 2048;
  
  public static final int FORMAT_NO_MONTH_DAY = 32;
  
  public static final int FORMAT_NO_NOON = 512;
  
  @Deprecated
  public static final int FORMAT_NO_NOON_MIDNIGHT = 2560;
  
  public static final int FORMAT_NO_YEAR = 8;
  
  public static final int FORMAT_NUMERIC_DATE = 131072;
  
  public static final int FORMAT_SHOW_DATE = 16;
  
  public static final int FORMAT_SHOW_TIME = 1;
  
  public static final int FORMAT_SHOW_WEEKDAY = 2;
  
  public static final int FORMAT_SHOW_YEAR = 4;
  
  @Deprecated
  public static final int FORMAT_UTC = 8192;
  
  public static final long HOUR_IN_MILLIS = 3600000L;
  
  @Deprecated
  public static final String HOUR_MINUTE_24 = "%H:%M";
  
  @Deprecated
  public static final int LENGTH_LONG = 10;
  
  @Deprecated
  public static final int LENGTH_MEDIUM = 20;
  
  @Deprecated
  public static final int LENGTH_SHORT = 30;
  
  @Deprecated
  public static final int LENGTH_SHORTER = 40;
  
  @Deprecated
  public static final int LENGTH_SHORTEST = 50;
  
  public static final long MINUTE_IN_MILLIS = 60000L;
  
  public static final String MONTH_DAY_FORMAT = "%-d";
  
  public static final String MONTH_FORMAT = "%B";
  
  public static final String NUMERIC_MONTH_FORMAT = "%m";
  
  public static final long SECOND_IN_MILLIS = 1000L;
  
  public static final String WEEKDAY_FORMAT = "%A";
  
  public static final long WEEK_IN_MILLIS = 604800000L;
  
  public static final String YEAR_FORMAT = "%Y";
  
  public static final String YEAR_FORMAT_TWO_DIGITS = "%g";
  
  @Deprecated
  public static final long YEAR_IN_MILLIS = 31449600000L;
  
  private static String sElapsedFormatHMMSS;
  
  private static String sElapsedFormatMMSS;
  
  private static Configuration sLastConfig;
  
  private static final Object sLock = new Object();
  
  private static Time sNowTime;
  
  private static Time sThenTime;
  
  @Deprecated
  public static final int[] sameMonthTable;
  
  @Deprecated
  public static final int[] sameYearTable = null;
  
  static {
    sameMonthTable = null;
  }
  
  @Deprecated
  public static String getDayOfWeekString(int paramInt1, int paramInt2) {
    String[] arrayOfString;
    LocaleData localeData = LocaleData.get(Locale.getDefault());
    if (paramInt2 != 10) {
      if (paramInt2 != 20) {
        if (paramInt2 != 30) {
          if (paramInt2 != 40) {
            if (paramInt2 != 50) {
              arrayOfString = localeData.shortWeekdayNames;
            } else {
              arrayOfString = ((LocaleData)arrayOfString).tinyWeekdayNames;
            } 
          } else {
            arrayOfString = ((LocaleData)arrayOfString).shortWeekdayNames;
          } 
        } else {
          arrayOfString = ((LocaleData)arrayOfString).shortWeekdayNames;
        } 
      } else {
        arrayOfString = ((LocaleData)arrayOfString).shortWeekdayNames;
      } 
    } else {
      arrayOfString = ((LocaleData)arrayOfString).longWeekdayNames;
    } 
    return arrayOfString[paramInt1];
  }
  
  @Deprecated
  public static String getAMPMString(int paramInt) {
    return (LocaleData.get(Locale.getDefault())).amPm[paramInt + 0];
  }
  
  @Deprecated
  public static String getMonthString(int paramInt1, int paramInt2) {
    String[] arrayOfString;
    LocaleData localeData = LocaleData.get(Locale.getDefault());
    if (paramInt2 != 10) {
      if (paramInt2 != 20) {
        if (paramInt2 != 30) {
          if (paramInt2 != 40) {
            if (paramInt2 != 50) {
              arrayOfString = localeData.shortMonthNames;
            } else {
              arrayOfString = ((LocaleData)arrayOfString).tinyMonthNames;
            } 
          } else {
            arrayOfString = ((LocaleData)arrayOfString).shortMonthNames;
          } 
        } else {
          arrayOfString = ((LocaleData)arrayOfString).shortMonthNames;
        } 
      } else {
        arrayOfString = ((LocaleData)arrayOfString).shortMonthNames;
      } 
    } else {
      arrayOfString = ((LocaleData)arrayOfString).longMonthNames;
    } 
    return arrayOfString[paramInt1];
  }
  
  public static CharSequence getRelativeTimeSpanString(long paramLong) {
    return getRelativeTimeSpanString(paramLong, System.currentTimeMillis(), 60000L);
  }
  
  public static CharSequence getRelativeTimeSpanString(long paramLong1, long paramLong2, long paramLong3) {
    return getRelativeTimeSpanString(paramLong1, paramLong2, paramLong3, 65556);
  }
  
  public static CharSequence getRelativeTimeSpanString(long paramLong1, long paramLong2, long paramLong3, int paramInt) {
    Locale locale = Locale.getDefault();
    TimeZone timeZone = TimeZone.getDefault();
    return RelativeDateTimeFormatter.getRelativeTimeSpanString(locale, timeZone, paramLong1, paramLong2, paramLong3, paramInt);
  }
  
  public static CharSequence getRelativeDateTimeString(Context paramContext, long paramLong1, long paramLong2, long paramLong3, int paramInt) {
    int i = paramInt;
    if ((paramInt & 0xC1) == 1) {
      if (DateFormat.is24HourFormat(paramContext)) {
        i = 128;
      } else {
        i = 64;
      } 
      i = paramInt | i;
    } 
    Locale locale = Locale.getDefault();
    TimeZone timeZone = TimeZone.getDefault();
    long l = System.currentTimeMillis();
    return RelativeDateTimeFormatter.getRelativeDateTimeString(locale, timeZone, paramLong1, l, paramLong2, paramLong3, i);
  }
  
  private static void initFormatStrings() {
    synchronized (sLock) {
      initFormatStringsLocked();
      return;
    } 
  }
  
  private static void initFormatStringsLocked() {
    Resources resources = Resources.getSystem();
    Configuration configuration1 = resources.getConfiguration();
    Configuration configuration2 = sLastConfig;
    if (configuration2 == null || !configuration2.equals(configuration1)) {
      sLastConfig = configuration1;
      sElapsedFormatMMSS = resources.getString(17040100);
      sElapsedFormatHMMSS = resources.getString(17040099);
    } 
  }
  
  public static CharSequence formatDuration(long paramLong) {
    return formatDuration(paramLong, 10);
  }
  
  public static CharSequence formatDuration(long paramLong, int paramInt) {
    MeasureFormat.FormatWidth formatWidth;
    if (paramInt != 10) {
      if (paramInt != 20 && paramInt != 30 && paramInt != 40) {
        if (paramInt != 50) {
          formatWidth = MeasureFormat.FormatWidth.WIDE;
        } else {
          formatWidth = MeasureFormat.FormatWidth.NARROW;
        } 
      } else {
        formatWidth = MeasureFormat.FormatWidth.SHORT;
      } 
    } else {
      formatWidth = MeasureFormat.FormatWidth.WIDE;
    } 
    MeasureFormat measureFormat = MeasureFormat.getInstance(Locale.getDefault(), formatWidth);
    if (paramLong >= 3600000L) {
      paramInt = (int)((1800000L + paramLong) / 3600000L);
      return measureFormat.format(new Measure(Integer.valueOf(paramInt), (MeasureUnit)MeasureUnit.HOUR));
    } 
    if (paramLong >= 60000L) {
      paramInt = (int)((30000L + paramLong) / 60000L);
      return measureFormat.format(new Measure(Integer.valueOf(paramInt), (MeasureUnit)MeasureUnit.MINUTE));
    } 
    paramInt = (int)((500L + paramLong) / 1000L);
    return measureFormat.format(new Measure(Integer.valueOf(paramInt), (MeasureUnit)MeasureUnit.SECOND));
  }
  
  public static String formatElapsedTime(long paramLong) {
    return formatElapsedTime(null, paramLong);
  }
  
  public static String formatElapsedTime(StringBuilder paramStringBuilder, long paramLong) {
    long l1 = 0L;
    long l2 = 0L;
    if (paramLong >= 3600L) {
      l1 = paramLong / 3600L;
      paramLong -= 3600L * l1;
    } 
    long l3 = paramLong;
    if (paramLong >= 60L) {
      l2 = paramLong / 60L;
      l3 = paramLong - 60L * l2;
    } 
    if (paramStringBuilder == null) {
      paramStringBuilder = new StringBuilder(8);
    } else {
      paramStringBuilder.setLength(0);
    } 
    Formatter formatter = new Formatter(paramStringBuilder, Locale.getDefault());
    initFormatStrings();
    if (l1 > 0L)
      return formatter.format(sElapsedFormatHMMSS, new Object[] { Long.valueOf(l1), Long.valueOf(l2), Long.valueOf(l3) }).toString(); 
    return formatter.format(sElapsedFormatMMSS, new Object[] { Long.valueOf(l2), Long.valueOf(l3) }).toString();
  }
  
  public static final CharSequence formatSameDayTime(long paramLong1, long paramLong2, int paramInt1, int paramInt2) {
    DateFormat dateFormat;
    GregorianCalendar gregorianCalendar1 = new GregorianCalendar();
    gregorianCalendar1.setTimeInMillis(paramLong1);
    Date date = gregorianCalendar1.getTime();
    GregorianCalendar gregorianCalendar2 = new GregorianCalendar();
    gregorianCalendar2.setTimeInMillis(paramLong2);
    if (gregorianCalendar1.get(1) == gregorianCalendar2.get(1) && 
      gregorianCalendar1.get(2) == gregorianCalendar2.get(2) && 
      gregorianCalendar1.get(5) == gregorianCalendar2.get(5)) {
      dateFormat = DateFormat.getTimeInstance(paramInt2);
    } else {
      dateFormat = DateFormat.getDateInstance(paramInt1);
    } 
    return dateFormat.format(date);
  }
  
  public static boolean isToday(long paramLong) {
    return isSameDate(paramLong, System.currentTimeMillis());
  }
  
  private static boolean isSameDate(long paramLong1, long paramLong2) {
    boolean bool;
    ZoneId zoneId = ZoneId.systemDefault();
    Instant instant1 = Instant.ofEpochMilli(paramLong1);
    LocalDateTime localDateTime2 = LocalDateTime.ofInstant(instant1, zoneId);
    Instant instant2 = Instant.ofEpochMilli(paramLong2);
    LocalDateTime localDateTime1 = LocalDateTime.ofInstant(instant2, zoneId);
    if (localDateTime2.getYear() == localDateTime1.getYear() && 
      localDateTime2.getMonthValue() == localDateTime1.getMonthValue() && 
      localDateTime2.getDayOfMonth() == localDateTime1.getDayOfMonth()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static String formatDateRange(Context paramContext, long paramLong1, long paramLong2, int paramInt) {
    Formatter formatter = new Formatter(new StringBuilder(50), Locale.getDefault());
    return formatDateRange(paramContext, formatter, paramLong1, paramLong2, paramInt).toString();
  }
  
  public static Formatter formatDateRange(Context paramContext, Formatter paramFormatter, long paramLong1, long paramLong2, int paramInt) {
    return formatDateRange(paramContext, paramFormatter, paramLong1, paramLong2, paramInt, null);
  }
  
  public static Formatter formatDateRange(Context paramContext, Formatter paramFormatter, long paramLong1, long paramLong2, int paramInt, String paramString) {
    int i = paramInt;
    if ((paramInt & 0xC1) == 1) {
      if (DateFormat.is24HourFormat(paramContext)) {
        i = 128;
      } else {
        i = 64;
      } 
      i = paramInt | i;
    } 
    String str = DateIntervalFormat.formatDateRange(paramLong1, paramLong2, i, paramString);
    try {
      paramFormatter.out().append(str);
      return paramFormatter;
    } catch (IOException iOException) {
      throw new AssertionError(iOException);
    } 
  }
  
  public static String formatDateTime(Context paramContext, long paramLong, int paramInt) {
    return formatDateRange(paramContext, paramLong, paramLong, paramInt);
  }
  
  public static CharSequence getRelativeTimeSpanString(Context paramContext, long paramLong, boolean paramBoolean) {
    // Byte code:
    //   0: invokestatic currentTimeMillis : ()J
    //   3: lstore #4
    //   5: lload #4
    //   7: lload_1
    //   8: lsub
    //   9: invokestatic abs : (J)J
    //   12: lstore #6
    //   14: ldc android/text/format/DateUtils
    //   16: monitorenter
    //   17: getstatic android/text/format/DateUtils.sNowTime : Landroid/text/format/Time;
    //   20: ifnonnull -> 38
    //   23: new android/text/format/Time
    //   26: astore #8
    //   28: aload #8
    //   30: invokespecial <init> : ()V
    //   33: aload #8
    //   35: putstatic android/text/format/DateUtils.sNowTime : Landroid/text/format/Time;
    //   38: getstatic android/text/format/DateUtils.sThenTime : Landroid/text/format/Time;
    //   41: ifnonnull -> 59
    //   44: new android/text/format/Time
    //   47: astore #8
    //   49: aload #8
    //   51: invokespecial <init> : ()V
    //   54: aload #8
    //   56: putstatic android/text/format/DateUtils.sThenTime : Landroid/text/format/Time;
    //   59: getstatic android/text/format/DateUtils.sNowTime : Landroid/text/format/Time;
    //   62: lload #4
    //   64: invokevirtual set : (J)V
    //   67: getstatic android/text/format/DateUtils.sThenTime : Landroid/text/format/Time;
    //   70: lload_1
    //   71: invokevirtual set : (J)V
    //   74: lload #6
    //   76: ldc2_w 86400000
    //   79: lcmp
    //   80: ifge -> 115
    //   83: getstatic android/text/format/DateUtils.sNowTime : Landroid/text/format/Time;
    //   86: getfield weekDay : I
    //   89: getstatic android/text/format/DateUtils.sThenTime : Landroid/text/format/Time;
    //   92: getfield weekDay : I
    //   95: if_icmpne -> 115
    //   98: aload_0
    //   99: lload_1
    //   100: lload_1
    //   101: iconst_1
    //   102: invokestatic formatDateRange : (Landroid/content/Context;JJI)Ljava/lang/String;
    //   105: astore #8
    //   107: ldc_w 17041127
    //   110: istore #9
    //   112: goto -> 165
    //   115: getstatic android/text/format/DateUtils.sNowTime : Landroid/text/format/Time;
    //   118: getfield year : I
    //   121: getstatic android/text/format/DateUtils.sThenTime : Landroid/text/format/Time;
    //   124: getfield year : I
    //   127: if_icmpeq -> 149
    //   130: aload_0
    //   131: lload_1
    //   132: lload_1
    //   133: ldc_w 131092
    //   136: invokestatic formatDateRange : (Landroid/content/Context;JJI)Ljava/lang/String;
    //   139: astore #8
    //   141: ldc_w 17041126
    //   144: istore #9
    //   146: goto -> 165
    //   149: aload_0
    //   150: lload_1
    //   151: lload_1
    //   152: ldc_w 65552
    //   155: invokestatic formatDateRange : (Landroid/content/Context;JJI)Ljava/lang/String;
    //   158: astore #8
    //   160: ldc_w 17041126
    //   163: istore #9
    //   165: aload #8
    //   167: astore #10
    //   169: iload_3
    //   170: ifeq -> 195
    //   173: aload_0
    //   174: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   177: astore_0
    //   178: aload_0
    //   179: iload #9
    //   181: iconst_1
    //   182: anewarray java/lang/Object
    //   185: dup
    //   186: iconst_0
    //   187: aload #8
    //   189: aastore
    //   190: invokevirtual getString : (I[Ljava/lang/Object;)Ljava/lang/String;
    //   193: astore #10
    //   195: ldc android/text/format/DateUtils
    //   197: monitorexit
    //   198: aload #10
    //   200: areturn
    //   201: astore_0
    //   202: ldc android/text/format/DateUtils
    //   204: monitorexit
    //   205: aload_0
    //   206: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #822	-> 0
    //   #823	-> 5
    //   #825	-> 14
    //   #826	-> 17
    //   #827	-> 23
    //   #830	-> 38
    //   #831	-> 44
    //   #834	-> 59
    //   #835	-> 67
    //   #838	-> 74
    //   #840	-> 98
    //   #841	-> 98
    //   #842	-> 107
    //   #843	-> 112
    //   #845	-> 130
    //   #846	-> 130
    //   #849	-> 141
    //   #850	-> 146
    //   #852	-> 149
    //   #853	-> 149
    //   #854	-> 160
    //   #856	-> 165
    //   #857	-> 173
    //   #858	-> 178
    //   #860	-> 195
    //   #861	-> 198
    //   #860	-> 201
    // Exception table:
    //   from	to	target	type
    //   17	23	201	finally
    //   23	38	201	finally
    //   38	44	201	finally
    //   44	59	201	finally
    //   59	67	201	finally
    //   67	74	201	finally
    //   83	98	201	finally
    //   98	107	201	finally
    //   115	130	201	finally
    //   130	141	201	finally
    //   149	160	201	finally
    //   173	178	201	finally
    //   178	195	201	finally
    //   195	198	201	finally
    //   202	205	201	finally
  }
  
  public static CharSequence getRelativeTimeSpanString(Context paramContext, long paramLong) {
    return getRelativeTimeSpanString(paramContext, paramLong, false);
  }
}
