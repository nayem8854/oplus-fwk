package android.text.format;

public class TimeMigrationUtils {
  public static String formatMillisWithFixedFormat(long paramLong) {
    return (new TimeFormatter()).formatMillisWithFixedFormat(paramLong);
  }
}
