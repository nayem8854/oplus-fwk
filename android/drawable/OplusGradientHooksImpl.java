package android.drawable;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import com.oplus.util.OplusRoundRectUtil;

public class OplusGradientHooksImpl implements IOplusGradientHooks {
  public void drawRoundRect(boolean paramBoolean1, Canvas paramCanvas, RectF paramRectF, float paramFloat, boolean paramBoolean2, Paint paramPaint1, Paint paramPaint2) {
    if (!paramBoolean1) {
      paramCanvas.drawRoundRect(paramRectF, paramFloat, paramFloat, paramPaint1);
    } else {
      Path path = OplusRoundRectUtil.getInstance().getPath(paramRectF, paramFloat);
      paramCanvas.drawPath(path, paramPaint1);
    } 
    if (paramBoolean2)
      if (!paramBoolean1) {
        paramCanvas.drawRoundRect(paramRectF, paramFloat, paramFloat, paramPaint2);
      } else {
        Path path = OplusRoundRectUtil.getInstance().getPath(paramRectF, paramFloat);
        paramCanvas.drawPath(path, paramPaint2);
      }  
  }
}
