package android.drawable;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

public interface IOplusGradientHooks extends IOplusCommonFeature {
  public static final IOplusGradientHooks DEFAULT = (IOplusGradientHooks)new Object();
  
  default IOplusGradientHooks getDefault() {
    return DEFAULT;
  }
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusGradientHooks;
  }
  
  default void drawRoundRect(boolean paramBoolean1, Canvas paramCanvas, RectF paramRectF, float paramFloat, boolean paramBoolean2, Paint paramPaint1, Paint paramPaint2) {
    paramCanvas.drawRoundRect(paramRectF, paramFloat, paramFloat, paramPaint1);
    if (paramBoolean2)
      paramCanvas.drawRoundRect(paramRectF, paramFloat, paramFloat, paramPaint2); 
  }
}
