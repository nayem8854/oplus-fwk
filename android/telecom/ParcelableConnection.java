package android.telecom;

import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.telecom.IVideoProvider;
import java.util.ArrayList;
import java.util.List;

public final class ParcelableConnection implements Parcelable {
  public ParcelableConnection(PhoneAccountHandle paramPhoneAccountHandle, int paramInt1, int paramInt2, int paramInt3, int paramInt4, Uri paramUri, int paramInt5, String paramString1, int paramInt6, IVideoProvider paramIVideoProvider, int paramInt7, boolean paramBoolean1, boolean paramBoolean2, long paramLong1, long paramLong2, StatusHints paramStatusHints, DisconnectCause paramDisconnectCause, List<String> paramList, Bundle paramBundle, String paramString2, int paramInt8, int paramInt9) {
    this(paramPhoneAccountHandle, paramInt1, paramInt2, paramInt3, paramInt4, paramUri, paramInt5, paramString1, paramInt6, paramIVideoProvider, paramInt7, paramBoolean1, paramBoolean2, paramLong1, paramLong2, paramStatusHints, paramDisconnectCause, paramList, paramBundle, paramInt9);
    this.mParentCallId = paramString2;
    this.mCallDirection = paramInt8;
  }
  
  public ParcelableConnection(PhoneAccountHandle paramPhoneAccountHandle, int paramInt1, int paramInt2, int paramInt3, int paramInt4, Uri paramUri, int paramInt5, String paramString, int paramInt6, IVideoProvider paramIVideoProvider, int paramInt7, boolean paramBoolean1, boolean paramBoolean2, long paramLong1, long paramLong2, StatusHints paramStatusHints, DisconnectCause paramDisconnectCause, List<String> paramList, Bundle paramBundle, int paramInt8) {
    this.mPhoneAccount = paramPhoneAccountHandle;
    this.mState = paramInt1;
    this.mConnectionCapabilities = paramInt2;
    this.mConnectionProperties = paramInt3;
    this.mSupportedAudioRoutes = paramInt4;
    this.mAddress = paramUri;
    this.mAddressPresentation = paramInt5;
    this.mCallerDisplayName = paramString;
    this.mCallerDisplayNamePresentation = paramInt6;
    this.mVideoProvider = paramIVideoProvider;
    this.mVideoState = paramInt7;
    this.mRingbackRequested = paramBoolean1;
    this.mIsVoipAudioMode = paramBoolean2;
    this.mConnectTimeMillis = paramLong1;
    this.mConnectElapsedTimeMillis = paramLong2;
    this.mStatusHints = paramStatusHints;
    this.mDisconnectCause = paramDisconnectCause;
    this.mConferenceableConnectionIds = paramList;
    this.mExtras = paramBundle;
    this.mParentCallId = null;
    this.mCallDirection = -1;
    this.mCallerNumberVerificationStatus = paramInt8;
  }
  
  public PhoneAccountHandle getPhoneAccount() {
    return this.mPhoneAccount;
  }
  
  public int getState() {
    return this.mState;
  }
  
  public int getConnectionCapabilities() {
    return this.mConnectionCapabilities;
  }
  
  public int getConnectionProperties() {
    return this.mConnectionProperties;
  }
  
  public int getSupportedAudioRoutes() {
    return this.mSupportedAudioRoutes;
  }
  
  public Uri getHandle() {
    return this.mAddress;
  }
  
  public int getHandlePresentation() {
    return this.mAddressPresentation;
  }
  
  public String getCallerDisplayName() {
    return this.mCallerDisplayName;
  }
  
  public int getCallerDisplayNamePresentation() {
    return this.mCallerDisplayNamePresentation;
  }
  
  public IVideoProvider getVideoProvider() {
    return this.mVideoProvider;
  }
  
  public int getVideoState() {
    return this.mVideoState;
  }
  
  public boolean isRingbackRequested() {
    return this.mRingbackRequested;
  }
  
  public boolean getIsVoipAudioMode() {
    return this.mIsVoipAudioMode;
  }
  
  public long getConnectTimeMillis() {
    return this.mConnectTimeMillis;
  }
  
  public long getConnectElapsedTimeMillis() {
    return this.mConnectElapsedTimeMillis;
  }
  
  public final StatusHints getStatusHints() {
    return this.mStatusHints;
  }
  
  public final DisconnectCause getDisconnectCause() {
    return this.mDisconnectCause;
  }
  
  public final List<String> getConferenceableConnectionIds() {
    return this.mConferenceableConnectionIds;
  }
  
  public final Bundle getExtras() {
    return this.mExtras;
  }
  
  public final String getParentCallId() {
    return this.mParentCallId;
  }
  
  public int getCallDirection() {
    return this.mCallDirection;
  }
  
  public int getCallerNumberVerificationStatus() {
    return this.mCallerNumberVerificationStatus;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ParcelableConnection [act:");
    PhoneAccountHandle phoneAccountHandle = this.mPhoneAccount;
    stringBuilder.append(phoneAccountHandle);
    stringBuilder.append("], state:");
    int i = this.mState;
    stringBuilder.append(i);
    stringBuilder.append(", capabilities:");
    i = this.mConnectionCapabilities;
    stringBuilder.append(Connection.capabilitiesToString(i));
    stringBuilder.append(", properties:");
    i = this.mConnectionProperties;
    stringBuilder.append(Connection.propertiesToString(i));
    stringBuilder.append(", extras:");
    Bundle bundle = this.mExtras;
    stringBuilder.append(bundle);
    stringBuilder.append(", parent:");
    String str = this.mParentCallId;
    stringBuilder.append(str);
    stringBuilder.append(", callDirection:");
    i = this.mCallDirection;
    stringBuilder.append(i);
    return stringBuilder.toString();
  }
  
  public static final Parcelable.Creator<ParcelableConnection> CREATOR = new Parcelable.Creator<ParcelableConnection>() {
      public ParcelableConnection createFromParcel(Parcel param1Parcel) {
        boolean bool1, bool2;
        ClassLoader classLoader = ParcelableConnection.class.getClassLoader();
        PhoneAccountHandle phoneAccountHandle = (PhoneAccountHandle)param1Parcel.readParcelable(classLoader);
        int i = param1Parcel.readInt();
        int j = param1Parcel.readInt();
        Uri uri = (Uri)param1Parcel.readParcelable(classLoader);
        int k = param1Parcel.readInt();
        String str2 = param1Parcel.readString();
        int m = param1Parcel.readInt();
        IVideoProvider iVideoProvider = IVideoProvider.Stub.asInterface(param1Parcel.readStrongBinder());
        int n = param1Parcel.readInt();
        if (param1Parcel.readByte() == 1) {
          bool1 = true;
        } else {
          bool1 = false;
        } 
        if (param1Parcel.readByte() == 1) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        long l1 = param1Parcel.readLong();
        StatusHints statusHints = (StatusHints)param1Parcel.readParcelable(classLoader);
        DisconnectCause disconnectCause = (DisconnectCause)param1Parcel.readParcelable(classLoader);
        ArrayList<String> arrayList = new ArrayList();
        param1Parcel.readStringList(arrayList);
        Bundle bundle = Bundle.setDefusable(param1Parcel.readBundle(classLoader), true);
        int i1 = param1Parcel.readInt();
        int i2 = param1Parcel.readInt();
        String str1 = param1Parcel.readString();
        long l2 = param1Parcel.readLong();
        int i3 = param1Parcel.readInt();
        int i4 = param1Parcel.readInt();
        return new ParcelableConnection(phoneAccountHandle, i, j, i1, i2, uri, k, str2, m, iVideoProvider, n, bool1, bool2, l1, l2, statusHints, disconnectCause, arrayList, bundle, str1, i3, i4);
      }
      
      public ParcelableConnection[] newArray(int param1Int) {
        return new ParcelableConnection[param1Int];
      }
    };
  
  private final Uri mAddress;
  
  private final int mAddressPresentation;
  
  private int mCallDirection;
  
  private final String mCallerDisplayName;
  
  private final int mCallerDisplayNamePresentation;
  
  private int mCallerNumberVerificationStatus;
  
  private final List<String> mConferenceableConnectionIds;
  
  private final long mConnectElapsedTimeMillis;
  
  private final long mConnectTimeMillis;
  
  private final int mConnectionCapabilities;
  
  private final int mConnectionProperties;
  
  private final DisconnectCause mDisconnectCause;
  
  private final Bundle mExtras;
  
  private final boolean mIsVoipAudioMode;
  
  private String mParentCallId;
  
  private final PhoneAccountHandle mPhoneAccount;
  
  private final boolean mRingbackRequested;
  
  private final int mState;
  
  private final StatusHints mStatusHints;
  
  private final int mSupportedAudioRoutes;
  
  private final IVideoProvider mVideoProvider;
  
  private final int mVideoState;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable(this.mPhoneAccount, 0);
    paramParcel.writeInt(this.mState);
    paramParcel.writeInt(this.mConnectionCapabilities);
    paramParcel.writeParcelable((Parcelable)this.mAddress, 0);
    paramParcel.writeInt(this.mAddressPresentation);
    paramParcel.writeString(this.mCallerDisplayName);
    paramParcel.writeInt(this.mCallerDisplayNamePresentation);
    IVideoProvider iVideoProvider = this.mVideoProvider;
    if (iVideoProvider != null) {
      IBinder iBinder = iVideoProvider.asBinder();
    } else {
      iVideoProvider = null;
    } 
    paramParcel.writeStrongBinder((IBinder)iVideoProvider);
    paramParcel.writeInt(this.mVideoState);
    paramParcel.writeByte((byte)this.mRingbackRequested);
    paramParcel.writeByte((byte)this.mIsVoipAudioMode);
    paramParcel.writeLong(this.mConnectTimeMillis);
    paramParcel.writeParcelable(this.mStatusHints, 0);
    paramParcel.writeParcelable(this.mDisconnectCause, 0);
    paramParcel.writeStringList(this.mConferenceableConnectionIds);
    paramParcel.writeBundle(this.mExtras);
    paramParcel.writeInt(this.mConnectionProperties);
    paramParcel.writeInt(this.mSupportedAudioRoutes);
    paramParcel.writeString(this.mParentCallId);
    paramParcel.writeLong(this.mConnectElapsedTimeMillis);
    paramParcel.writeInt(this.mCallDirection);
    paramParcel.writeInt(this.mCallerNumberVerificationStatus);
  }
}
