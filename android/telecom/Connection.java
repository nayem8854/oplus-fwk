package android.telecom;

import android.annotation.SystemApi;
import android.bluetooth.BluetoothDevice;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.util.ArraySet;
import android.view.Surface;
import com.android.internal.os.SomeArgs;
import com.android.internal.telecom.IVideoCallback;
import com.android.internal.telecom.IVideoProvider;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.nio.channels.Channels;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public abstract class Connection extends Conferenceable {
  private static final boolean PII_DEBUG = Log.isLoggable(3);
  
  public static String capabilitiesToString(int paramInt) {
    return capabilitiesToStringInternal(paramInt, true);
  }
  
  public static String capabilitiesToStringShort(int paramInt) {
    return capabilitiesToStringInternal(paramInt, false);
  }
  
  private static String capabilitiesToStringInternal(int paramInt, boolean paramBoolean) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("[");
    if (paramBoolean)
      stringBuilder.append("Capabilities:"); 
    if ((paramInt & 0x1) == 1) {
      String str;
      if (paramBoolean) {
        str = " CAPABILITY_HOLD";
      } else {
        str = " hld";
      } 
      stringBuilder.append(str);
    } 
    if ((paramInt & 0x2) == 2) {
      String str;
      if (paramBoolean) {
        str = " CAPABILITY_SUPPORT_HOLD";
      } else {
        str = " sup_hld";
      } 
      stringBuilder.append(str);
    } 
    if ((paramInt & 0x4) == 4) {
      String str;
      if (paramBoolean) {
        str = " CAPABILITY_MERGE_CONFERENCE";
      } else {
        str = " mrg_cnf";
      } 
      stringBuilder.append(str);
    } 
    if ((paramInt & 0x8) == 8) {
      String str;
      if (paramBoolean) {
        str = " CAPABILITY_SWAP_CONFERENCE";
      } else {
        str = " swp_cnf";
      } 
      stringBuilder.append(str);
    } 
    if ((paramInt & 0x20) == 32) {
      String str;
      if (paramBoolean) {
        str = " CAPABILITY_RESPOND_VIA_TEXT";
      } else {
        str = " txt";
      } 
      stringBuilder.append(str);
    } 
    if ((paramInt & 0x40) == 64) {
      String str;
      if (paramBoolean) {
        str = " CAPABILITY_MUTE";
      } else {
        str = " mut";
      } 
      stringBuilder.append(str);
    } 
    if ((paramInt & 0x80) == 128) {
      String str;
      if (paramBoolean) {
        str = " CAPABILITY_MANAGE_CONFERENCE";
      } else {
        str = " mng_cnf";
      } 
      stringBuilder.append(str);
    } 
    if ((paramInt & 0x100) == 256) {
      String str;
      if (paramBoolean) {
        str = " CAPABILITY_SUPPORTS_VT_LOCAL_RX";
      } else {
        str = " VTlrx";
      } 
      stringBuilder.append(str);
    } 
    if ((paramInt & 0x200) == 512) {
      String str;
      if (paramBoolean) {
        str = " CAPABILITY_SUPPORTS_VT_LOCAL_TX";
      } else {
        str = " VTltx";
      } 
      stringBuilder.append(str);
    } 
    if ((paramInt & 0x300) == 768) {
      String str;
      if (paramBoolean) {
        str = " CAPABILITY_SUPPORTS_VT_LOCAL_BIDIRECTIONAL";
      } else {
        str = " VTlbi";
      } 
      stringBuilder.append(str);
    } 
    if ((paramInt & 0x400) == 1024) {
      String str;
      if (paramBoolean) {
        str = " CAPABILITY_SUPPORTS_VT_REMOTE_RX";
      } else {
        str = " VTrrx";
      } 
      stringBuilder.append(str);
    } 
    if ((paramInt & 0x800) == 2048) {
      String str;
      if (paramBoolean) {
        str = " CAPABILITY_SUPPORTS_VT_REMOTE_TX";
      } else {
        str = " VTrtx";
      } 
      stringBuilder.append(str);
    } 
    if ((paramInt & 0xC00) == 3072) {
      String str;
      if (paramBoolean) {
        str = " CAPABILITY_SUPPORTS_VT_REMOTE_BIDIRECTIONAL";
      } else {
        str = " VTrbi";
      } 
      stringBuilder.append(str);
    } 
    if ((paramInt & 0x800000) == 8388608) {
      String str;
      if (paramBoolean) {
        str = " CAPABILITY_CANNOT_DOWNGRADE_VIDEO_TO_AUDIO";
      } else {
        str = " !v2a";
      } 
      stringBuilder.append(str);
    } 
    if ((paramInt & 0x40000) == 262144) {
      String str;
      if (paramBoolean) {
        str = " CAPABILITY_SPEED_UP_MT_AUDIO";
      } else {
        str = " spd_aud";
      } 
      stringBuilder.append(str);
    } 
    if ((paramInt & 0x80000) == 524288) {
      String str;
      if (paramBoolean) {
        str = " CAPABILITY_CAN_UPGRADE_TO_VIDEO";
      } else {
        str = " a2v";
      } 
      stringBuilder.append(str);
    } 
    if ((paramInt & 0x100000) == 1048576) {
      String str;
      if (paramBoolean) {
        str = " CAPABILITY_CAN_PAUSE_VIDEO";
      } else {
        str = " paus_VT";
      } 
      stringBuilder.append(str);
    } 
    if ((paramInt & 0x200000) == 2097152) {
      String str;
      if (paramBoolean) {
        str = " CAPABILITY_SINGLE_PARTY_CONFERENCE";
      } else {
        str = " 1p_cnf";
      } 
      stringBuilder.append(str);
    } 
    if ((paramInt & 0x400000) == 4194304) {
      String str;
      if (paramBoolean) {
        str = " CAPABILITY_CAN_SEND_RESPONSE_VIA_CONNECTION";
      } else {
        str = " rsp_by_con";
      } 
      stringBuilder.append(str);
    } 
    if ((paramInt & 0x1000000) == 16777216) {
      String str;
      if (paramBoolean) {
        str = " CAPABILITY_CAN_PULL_CALL";
      } else {
        str = " pull";
      } 
      stringBuilder.append(str);
    } 
    if ((paramInt & 0x2000000) == 33554432) {
      String str;
      if (paramBoolean) {
        str = " CAPABILITY_SUPPORT_DEFLECT";
      } else {
        str = " sup_def";
      } 
      stringBuilder.append(str);
    } 
    if ((paramInt & 0x4000000) == 67108864) {
      String str;
      if (paramBoolean) {
        str = " CAPABILITY_ADD_PARTICIPANT";
      } else {
        str = " add_participant";
      } 
      stringBuilder.append(str);
    } 
    if ((0x8000000 & paramInt) == 134217728) {
      String str;
      if (paramBoolean) {
        str = " CAPABILITY_TRANSFER";
      } else {
        str = " sup_trans";
      } 
      stringBuilder.append(str);
    } 
    if ((0x10000000 & paramInt) == 268435456) {
      String str;
      if (paramBoolean) {
        str = " CAPABILITY_TRANSFER_CONSULTATIVE";
      } else {
        str = " sup_cTrans";
      } 
      stringBuilder.append(str);
    } 
    if ((0x20000000 & paramInt) == 536870912) {
      String str;
      if (paramBoolean) {
        str = " CAPABILITY_SUPPORTS_RTT_REMOTE";
      } else {
        str = " sup_rtt";
      } 
      stringBuilder.append(str);
    } 
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public static String propertiesToString(int paramInt) {
    return propertiesToStringInternal(paramInt, true);
  }
  
  public static String propertiesToStringShort(int paramInt) {
    return propertiesToStringInternal(paramInt, false);
  }
  
  private static String propertiesToStringInternal(int paramInt, boolean paramBoolean) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("[");
    if (paramBoolean)
      stringBuilder.append("Properties:"); 
    if ((paramInt & 0x80) == 128) {
      String str;
      if (paramBoolean) {
        str = " PROPERTY_SELF_MANAGED";
      } else {
        str = " self_mng";
      } 
      stringBuilder.append(str);
    } 
    if ((paramInt & 0x1) == 1) {
      String str;
      if (paramBoolean) {
        str = " PROPERTY_EMERGENCY_CALLBACK_MODE";
      } else {
        str = " ecbm";
      } 
      stringBuilder.append(str);
    } 
    if ((paramInt & 0x4) == 4) {
      String str;
      if (paramBoolean) {
        str = " PROPERTY_HIGH_DEF_AUDIO";
      } else {
        str = " HD";
      } 
      stringBuilder.append(str);
    } 
    if ((paramInt & 0x8) == 8) {
      String str;
      if (paramBoolean) {
        str = " PROPERTY_WIFI";
      } else {
        str = " wifi";
      } 
      stringBuilder.append(str);
    } 
    if ((paramInt & 0x2) == 2) {
      String str;
      if (paramBoolean) {
        str = " PROPERTY_GENERIC_CONFERENCE";
      } else {
        str = " gen_conf";
      } 
      stringBuilder.append(str);
    } 
    if ((paramInt & 0x10) == 16) {
      String str;
      if (paramBoolean) {
        str = " PROPERTY_IS_EXTERNAL_CALL";
      } else {
        str = " xtrnl";
      } 
      stringBuilder.append(str);
    } 
    if ((paramInt & 0x20) == 32) {
      String str;
      if (paramBoolean) {
        str = " PROPERTY_HAS_CDMA_VOICE_PRIVACY";
      } else {
        str = " priv";
      } 
      stringBuilder.append(str);
    } 
    if ((paramInt & 0x100) == 256) {
      String str;
      if (paramBoolean) {
        str = " PROPERTY_IS_RTT";
      } else {
        str = " rtt";
      } 
      stringBuilder.append(str);
    } 
    if ((paramInt & 0x400) == 1024) {
      String str;
      if (paramBoolean) {
        str = " PROPERTY_NETWORK_IDENTIFIED_EMERGENCY_CALL";
      } else {
        str = " ecall";
      } 
      stringBuilder.append(str);
    } 
    if ((paramInt & 0x800) == 2048) {
      String str;
      if (paramBoolean) {
        str = " PROPERTY_REMOTELY_HOSTED";
      } else {
        str = " remote_hst";
      } 
      stringBuilder.append(str);
    } 
    if ((paramInt & 0x1000) == 4096) {
      String str;
      if (paramBoolean) {
        str = " PROPERTY_IS_ADHOC_CONFERENCE";
      } else {
        str = " adhoc_conf";
      } 
      stringBuilder.append(str);
    } 
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  class Listener {
    public void onStateChanged(Connection param1Connection, int param1Int) {}
    
    public void onAddressChanged(Connection param1Connection, Uri param1Uri, int param1Int) {}
    
    public void onCallerDisplayNameChanged(Connection param1Connection, String param1String, int param1Int) {}
    
    public void onVideoStateChanged(Connection param1Connection, int param1Int) {}
    
    public void onDisconnected(Connection param1Connection, DisconnectCause param1DisconnectCause) {}
    
    public void onPostDialWait(Connection param1Connection, String param1String) {}
    
    public void onPostDialChar(Connection param1Connection, char param1Char) {}
    
    public void onRingbackRequested(Connection param1Connection, boolean param1Boolean) {}
    
    public void onDestroyed(Connection param1Connection) {}
    
    public void onConnectionCapabilitiesChanged(Connection param1Connection, int param1Int) {}
    
    public void onConnectionPropertiesChanged(Connection param1Connection, int param1Int) {}
    
    public void onSupportedAudioRoutesChanged(Connection param1Connection, int param1Int) {}
    
    public void onVideoProviderChanged(Connection param1Connection, Connection.VideoProvider param1VideoProvider) {}
    
    public void onAudioModeIsVoipChanged(Connection param1Connection, boolean param1Boolean) {}
    
    public void onStatusHintsChanged(Connection param1Connection, StatusHints param1StatusHints) {}
    
    public void onConferenceablesChanged(Connection param1Connection, List<Conferenceable> param1List) {}
    
    public void onConferenceChanged(Connection param1Connection, Conference param1Conference) {}
    
    public void onConferenceMergeFailed(Connection param1Connection) {}
    
    public void onExtrasChanged(Connection param1Connection, Bundle param1Bundle) {}
    
    public void onExtrasRemoved(Connection param1Connection, List<String> param1List) {}
    
    public void onConnectionEvent(Connection param1Connection, String param1String, Bundle param1Bundle) {}
    
    public void onAudioRouteChanged(Connection param1Connection, int param1Int, String param1String) {}
    
    public void onRttInitiationSuccess(Connection param1Connection) {}
    
    public void onRttInitiationFailure(Connection param1Connection, int param1Int) {}
    
    public void onRttSessionRemotelyTerminated(Connection param1Connection) {}
    
    public void onRemoteRttRequest(Connection param1Connection) {}
    
    public void onPhoneAccountChanged(Connection param1Connection, PhoneAccountHandle param1PhoneAccountHandle) {}
    
    public void onConnectionTimeReset(Connection param1Connection) {}
  }
  
  class RttTextStream {
    private static final int READ_BUFFER_SIZE = 1000;
    
    private final ParcelFileDescriptor mFdFromInCall;
    
    private final ParcelFileDescriptor mFdToInCall;
    
    private final FileInputStream mFromInCallFileInputStream;
    
    private final InputStreamReader mPipeFromInCall;
    
    private final OutputStreamWriter mPipeToInCall;
    
    private char[] mReadBuffer = new char[1000];
    
    public RttTextStream(Connection this$0, ParcelFileDescriptor param1ParcelFileDescriptor1) {
      this.mFdFromInCall = param1ParcelFileDescriptor1;
      this.mFdToInCall = (ParcelFileDescriptor)this$0;
      this.mFromInCallFileInputStream = new FileInputStream(param1ParcelFileDescriptor1.getFileDescriptor());
      FileInputStream fileInputStream = this.mFromInCallFileInputStream;
      this.mPipeFromInCall = new InputStreamReader(Channels.newInputStream(Channels.newChannel(fileInputStream)));
      this.mPipeToInCall = new OutputStreamWriter(new FileOutputStream(this$0.getFileDescriptor()));
    }
    
    public void write(String param1String) throws IOException {
      this.mPipeToInCall.write(param1String);
      this.mPipeToInCall.flush();
    }
    
    public String read() throws IOException {
      int i = this.mPipeFromInCall.read(this.mReadBuffer, 0, 1000);
      if (i < 0)
        return null; 
      return new String(this.mReadBuffer, 0, i);
    }
    
    public String readImmediately() throws IOException {
      if (this.mFromInCallFileInputStream.available() > 0)
        return read(); 
      return null;
    }
    
    public ParcelFileDescriptor getFdFromInCall() {
      return this.mFdFromInCall;
    }
    
    public ParcelFileDescriptor getFdToInCall() {
      return this.mFdToInCall;
    }
  }
  
  class RttModifyStatus {
    public static final int SESSION_MODIFY_REQUEST_FAIL = 2;
    
    public static final int SESSION_MODIFY_REQUEST_INVALID = 3;
    
    public static final int SESSION_MODIFY_REQUEST_REJECTED_BY_REMOTE = 5;
    
    public static final int SESSION_MODIFY_REQUEST_SUCCESS = 1;
    
    public static final int SESSION_MODIFY_REQUEST_TIMED_OUT = 4;
  }
  
  private final class VideoProviderHandler extends Handler {
    final Connection.VideoProvider this$0;
    
    public VideoProviderHandler() {}
    
    public VideoProviderHandler(Looper param1Looper) {
      super(param1Looper);
    }
    
    public void handleMessage(Message param1Message) {
      SomeArgs someArgs;
      switch (param1Message.what) {
        default:
          return;
        case 12:
          iBinder2 = (IBinder)param1Message.obj;
          null = (IBinder)param1Message.obj;
          IVideoCallback.Stub.asInterface(null);
          if (!this.this$0.mVideoCallbacks.containsKey(iBinder2)) {
            Log.i(this, "removeVideoProvider - skipped; not present.", new Object[0]);
          } else {
            this.this$0.mVideoCallbacks.remove(iBinder2);
          } 
        case 11:
          this.this$0.onSetPauseImage((Uri)((Message)null).obj);
        case 10:
          this.this$0.onRequestConnectionDataUsage();
        case 9:
          this.this$0.onRequestCameraCapabilities();
        case 8:
          this.this$0.onSendSessionModifyResponse((VideoProfile)((Message)null).obj);
        case 7:
          null = (SomeArgs)((Message)null).obj;
          try {
            this.this$0.onSendSessionModifyRequest((VideoProfile)null.arg1, (VideoProfile)null.arg2);
          } finally {
            null.recycle();
          } 
        case 6:
          this.this$0.onSetZoom(((Float)param1Message.obj).floatValue());
        case 5:
          this.this$0.onSetDeviceOrientation(param1Message.arg1);
        case 4:
          this.this$0.onSetDisplaySurface((Surface)param1Message.obj);
        case 3:
          this.this$0.onSetPreviewSurface((Surface)param1Message.obj);
        case 2:
          someArgs = (SomeArgs)param1Message.obj;
          try {
            this.this$0.onSetCamera((String)someArgs.arg1);
            this.this$0.onSetCamera((String)someArgs.arg1, (String)someArgs.arg2, someArgs.argi1, someArgs.argi2, someArgs.argi3);
          } finally {
            someArgs.recycle();
          } 
        case 1:
          break;
      } 
      IBinder iBinder2 = (IBinder)((Message)someArgs).obj;
      IBinder iBinder1 = (IBinder)((Message)someArgs).obj;
      IVideoCallback iVideoCallback = IVideoCallback.Stub.asInterface(iBinder1);
      if (iVideoCallback == null)
        Log.w(this, "addVideoProvider - skipped; callback is null.", new Object[0]); 
      if (this.this$0.mVideoCallbacks.containsKey(iBinder2))
        Log.i(this, "addVideoProvider - skipped; already present.", new Object[0]); 
      this.this$0.mVideoCallbacks.put(iBinder2, iVideoCallback);
    }
  }
  
  class VideoProvider {
    private static final int MSG_ADD_VIDEO_CALLBACK = 1;
    
    private static final int MSG_REMOVE_VIDEO_CALLBACK = 12;
    
    private static final int MSG_REQUEST_CAMERA_CAPABILITIES = 9;
    
    private static final int MSG_REQUEST_CONNECTION_DATA_USAGE = 10;
    
    private static final int MSG_SEND_SESSION_MODIFY_REQUEST = 7;
    
    private static final int MSG_SEND_SESSION_MODIFY_RESPONSE = 8;
    
    private static final int MSG_SET_CAMERA = 2;
    
    private static final int MSG_SET_DEVICE_ORIENTATION = 5;
    
    private static final int MSG_SET_DISPLAY_SURFACE = 4;
    
    private static final int MSG_SET_PAUSE_IMAGE = 11;
    
    private static final int MSG_SET_PREVIEW_SURFACE = 3;
    
    private static final int MSG_SET_ZOOM = 6;
    
    public static final int SESSION_EVENT_CAMERA_FAILURE = 5;
    
    private static final String SESSION_EVENT_CAMERA_FAILURE_STR = "CAMERA_FAIL";
    
    public static final int SESSION_EVENT_CAMERA_PERMISSION_ERROR = 7;
    
    private static final String SESSION_EVENT_CAMERA_PERMISSION_ERROR_STR = "CAMERA_PERMISSION_ERROR";
    
    public static final int SESSION_EVENT_CAMERA_READY = 6;
    
    private static final String SESSION_EVENT_CAMERA_READY_STR = "CAMERA_READY";
    
    public static final int SESSION_EVENT_RX_PAUSE = 1;
    
    private static final String SESSION_EVENT_RX_PAUSE_STR = "RX_PAUSE";
    
    public static final int SESSION_EVENT_RX_RESUME = 2;
    
    private static final String SESSION_EVENT_RX_RESUME_STR = "RX_RESUME";
    
    public static final int SESSION_EVENT_TX_START = 3;
    
    private static final String SESSION_EVENT_TX_START_STR = "TX_START";
    
    public static final int SESSION_EVENT_TX_STOP = 4;
    
    private static final String SESSION_EVENT_TX_STOP_STR = "TX_STOP";
    
    private static final String SESSION_EVENT_UNKNOWN_STR = "UNKNOWN";
    
    public static final int SESSION_MODIFY_REQUEST_FAIL = 2;
    
    public static final int SESSION_MODIFY_REQUEST_INVALID = 3;
    
    public static final int SESSION_MODIFY_REQUEST_REJECTED_BY_REMOTE = 5;
    
    public static final int SESSION_MODIFY_REQUEST_SUCCESS = 1;
    
    public static final int SESSION_MODIFY_REQUEST_TIMED_OUT = 4;
    
    private final VideoProviderBinder mBinder;
    
    private VideoProviderHandler mMessageHandler;
    
    private ConcurrentHashMap<IBinder, IVideoCallback> mVideoCallbacks = new ConcurrentHashMap<>(8, 0.9F, 1);
    
    class VideoProviderHandler extends Handler {
      final Connection.VideoProvider this$0;
      
      public VideoProviderHandler() {}
      
      public VideoProviderHandler(Looper param2Looper) {
        super(param2Looper);
      }
      
      public void handleMessage(Message param2Message) {
        SomeArgs someArgs;
        switch (param2Message.what) {
          default:
            return;
          case 12:
            iBinder2 = (IBinder)param2Message.obj;
            null = (IBinder)param2Message.obj;
            IVideoCallback.Stub.asInterface(null);
            if (!Connection.VideoProvider.this.mVideoCallbacks.containsKey(iBinder2)) {
              Log.i(this, "removeVideoProvider - skipped; not present.", new Object[0]);
            } else {
              Connection.VideoProvider.this.mVideoCallbacks.remove(iBinder2);
            } 
          case 11:
            Connection.VideoProvider.this.onSetPauseImage((Uri)((Message)null).obj);
          case 10:
            Connection.VideoProvider.this.onRequestConnectionDataUsage();
          case 9:
            Connection.VideoProvider.this.onRequestCameraCapabilities();
          case 8:
            Connection.VideoProvider.this.onSendSessionModifyResponse((VideoProfile)((Message)null).obj);
          case 7:
            null = (SomeArgs)((Message)null).obj;
            try {
              Connection.VideoProvider.this.onSendSessionModifyRequest((VideoProfile)null.arg1, (VideoProfile)null.arg2);
            } finally {
              null.recycle();
            } 
          case 6:
            Connection.VideoProvider.this.onSetZoom(((Float)param2Message.obj).floatValue());
          case 5:
            Connection.VideoProvider.this.onSetDeviceOrientation(param2Message.arg1);
          case 4:
            Connection.VideoProvider.this.onSetDisplaySurface((Surface)param2Message.obj);
          case 3:
            Connection.VideoProvider.this.onSetPreviewSurface((Surface)param2Message.obj);
          case 2:
            someArgs = (SomeArgs)param2Message.obj;
            try {
              Connection.VideoProvider.this.onSetCamera((String)someArgs.arg1);
              Connection.VideoProvider.this.onSetCamera((String)someArgs.arg1, (String)someArgs.arg2, someArgs.argi1, someArgs.argi2, someArgs.argi3);
            } finally {
              someArgs.recycle();
            } 
          case 1:
            break;
        } 
        IBinder iBinder2 = (IBinder)((Message)someArgs).obj;
        IBinder iBinder1 = (IBinder)((Message)someArgs).obj;
        IVideoCallback iVideoCallback = IVideoCallback.Stub.asInterface(iBinder1);
        if (iVideoCallback == null)
          Log.w(this, "addVideoProvider - skipped; callback is null.", new Object[0]); 
        if (Connection.VideoProvider.this.mVideoCallbacks.containsKey(iBinder2))
          Log.i(this, "addVideoProvider - skipped; already present.", new Object[0]); 
        Connection.VideoProvider.this.mVideoCallbacks.put(iBinder2, iVideoCallback);
      }
    }
    
    class VideoProviderBinder extends IVideoProvider.Stub {
      final Connection.VideoProvider this$0;
      
      private VideoProviderBinder() {}
      
      public void addVideoCallback(IBinder param2IBinder) {
        Message message = Connection.VideoProvider.this.mMessageHandler.obtainMessage(1, param2IBinder);
        message.sendToTarget();
      }
      
      public void removeVideoCallback(IBinder param2IBinder) {
        Message message = Connection.VideoProvider.this.mMessageHandler.obtainMessage(12, param2IBinder);
        message.sendToTarget();
      }
      
      public void setCamera(String param2String1, String param2String2, int param2Int) {
        SomeArgs someArgs = SomeArgs.obtain();
        someArgs.arg1 = param2String1;
        someArgs.arg2 = param2String2;
        someArgs.argi1 = Binder.getCallingUid();
        someArgs.argi2 = Binder.getCallingPid();
        someArgs.argi3 = param2Int;
        Connection.VideoProvider.this.mMessageHandler.obtainMessage(2, someArgs).sendToTarget();
      }
      
      public void setPreviewSurface(Surface param2Surface) {
        Connection.VideoProvider.this.mMessageHandler.obtainMessage(3, param2Surface).sendToTarget();
      }
      
      public void setDisplaySurface(Surface param2Surface) {
        Connection.VideoProvider.this.mMessageHandler.obtainMessage(4, param2Surface).sendToTarget();
      }
      
      public void setDeviceOrientation(int param2Int) {
        Message message = Connection.VideoProvider.this.mMessageHandler.obtainMessage(5, param2Int, 0);
        message.sendToTarget();
      }
      
      public void setZoom(float param2Float) {
        Connection.VideoProvider.this.mMessageHandler.obtainMessage(6, Float.valueOf(param2Float)).sendToTarget();
      }
      
      public void sendSessionModifyRequest(VideoProfile param2VideoProfile1, VideoProfile param2VideoProfile2) {
        SomeArgs someArgs = SomeArgs.obtain();
        someArgs.arg1 = param2VideoProfile1;
        someArgs.arg2 = param2VideoProfile2;
        Connection.VideoProvider.this.mMessageHandler.obtainMessage(7, someArgs).sendToTarget();
      }
      
      public void sendSessionModifyResponse(VideoProfile param2VideoProfile) {
        Message message = Connection.VideoProvider.this.mMessageHandler.obtainMessage(8, param2VideoProfile);
        message.sendToTarget();
      }
      
      public void requestCameraCapabilities() {
        Connection.VideoProvider.this.mMessageHandler.obtainMessage(9).sendToTarget();
      }
      
      public void requestCallDataUsage() {
        Connection.VideoProvider.this.mMessageHandler.obtainMessage(10).sendToTarget();
      }
      
      public void setPauseImage(Uri param2Uri) {
        Connection.VideoProvider.this.mMessageHandler.obtainMessage(11, param2Uri).sendToTarget();
      }
    }
    
    public VideoProvider() {
      this.mBinder = new VideoProviderBinder();
      this.mMessageHandler = new VideoProviderHandler(Looper.getMainLooper());
    }
    
    public VideoProvider(Connection this$0) {
      this.mBinder = new VideoProviderBinder();
      this.mMessageHandler = new VideoProviderHandler((Looper)this$0);
    }
    
    public final IVideoProvider getInterface() {
      return (IVideoProvider)this.mBinder;
    }
    
    public void onSetCamera(String param1String1, String param1String2, int param1Int1, int param1Int2, int param1Int3) {}
    
    public void receiveSessionModifyRequest(VideoProfile param1VideoProfile) {
      ConcurrentHashMap<IBinder, IVideoCallback> concurrentHashMap = this.mVideoCallbacks;
      if (concurrentHashMap != null)
        for (IVideoCallback iVideoCallback : concurrentHashMap.values()) {
          try {
            iVideoCallback.receiveSessionModifyRequest(param1VideoProfile);
          } catch (RemoteException remoteException) {
            Log.w(this, "receiveSessionModifyRequest callback failed", new Object[] { remoteException });
          } 
        }  
    }
    
    public void receiveSessionModifyResponse(int param1Int, VideoProfile param1VideoProfile1, VideoProfile param1VideoProfile2) {
      ConcurrentHashMap<IBinder, IVideoCallback> concurrentHashMap = this.mVideoCallbacks;
      if (concurrentHashMap != null)
        for (IVideoCallback iVideoCallback : concurrentHashMap.values()) {
          try {
            iVideoCallback.receiveSessionModifyResponse(param1Int, param1VideoProfile1, param1VideoProfile2);
          } catch (RemoteException remoteException) {
            Log.w(this, "receiveSessionModifyResponse callback failed", new Object[] { remoteException });
          } 
        }  
    }
    
    public void handleCallSessionEvent(int param1Int) {
      ConcurrentHashMap<IBinder, IVideoCallback> concurrentHashMap = this.mVideoCallbacks;
      if (concurrentHashMap != null)
        for (IVideoCallback iVideoCallback : concurrentHashMap.values()) {
          try {
            iVideoCallback.handleCallSessionEvent(param1Int);
          } catch (RemoteException remoteException) {
            Log.w(this, "handleCallSessionEvent callback failed", new Object[] { remoteException });
          } 
        }  
    }
    
    public void changePeerDimensions(int param1Int1, int param1Int2) {
      ConcurrentHashMap<IBinder, IVideoCallback> concurrentHashMap = this.mVideoCallbacks;
      if (concurrentHashMap != null)
        for (IVideoCallback iVideoCallback : concurrentHashMap.values()) {
          try {
            iVideoCallback.changePeerDimensions(param1Int1, param1Int2);
          } catch (RemoteException remoteException) {
            Log.w(this, "changePeerDimensions callback failed", new Object[] { remoteException });
          } 
        }  
    }
    
    public void setCallDataUsage(long param1Long) {
      ConcurrentHashMap<IBinder, IVideoCallback> concurrentHashMap = this.mVideoCallbacks;
      if (concurrentHashMap != null)
        for (IVideoCallback iVideoCallback : concurrentHashMap.values()) {
          try {
            iVideoCallback.changeCallDataUsage(param1Long);
          } catch (RemoteException remoteException) {
            Log.w(this, "setCallDataUsage callback failed", new Object[] { remoteException });
          } 
        }  
    }
    
    public void changeCallDataUsage(long param1Long) {
      setCallDataUsage(param1Long);
    }
    
    public void changeCameraCapabilities(VideoProfile.CameraCapabilities param1CameraCapabilities) {
      ConcurrentHashMap<IBinder, IVideoCallback> concurrentHashMap = this.mVideoCallbacks;
      if (concurrentHashMap != null)
        for (IVideoCallback iVideoCallback : concurrentHashMap.values()) {
          try {
            iVideoCallback.changeCameraCapabilities(param1CameraCapabilities);
          } catch (RemoteException remoteException) {
            Log.w(this, "changeCameraCapabilities callback failed", new Object[] { remoteException });
          } 
        }  
    }
    
    public void changeVideoQuality(int param1Int) {
      ConcurrentHashMap<IBinder, IVideoCallback> concurrentHashMap = this.mVideoCallbacks;
      if (concurrentHashMap != null)
        for (IVideoCallback iVideoCallback : concurrentHashMap.values()) {
          try {
            iVideoCallback.changeVideoQuality(param1Int);
          } catch (RemoteException remoteException) {
            Log.w(this, "changeVideoQuality callback failed", new Object[] { remoteException });
          } 
        }  
    }
    
    public static String sessionEventToString(int param1Int) {
      StringBuilder stringBuilder;
      switch (param1Int) {
        default:
          stringBuilder = new StringBuilder();
          stringBuilder.append("UNKNOWN ");
          stringBuilder.append(param1Int);
          return stringBuilder.toString();
        case 7:
          return "CAMERA_PERMISSION_ERROR";
        case 6:
          return "CAMERA_READY";
        case 5:
          return "CAMERA_FAIL";
        case 4:
          return "TX_STOP";
        case 3:
          return "TX_START";
        case 2:
          return "RX_RESUME";
        case 1:
          break;
      } 
      return "RX_PAUSE";
    }
    
    public abstract void onRequestCameraCapabilities();
    
    public abstract void onRequestConnectionDataUsage();
    
    public abstract void onSendSessionModifyRequest(VideoProfile param1VideoProfile1, VideoProfile param1VideoProfile2);
    
    public abstract void onSendSessionModifyResponse(VideoProfile param1VideoProfile);
    
    public abstract void onSetCamera(String param1String);
    
    public abstract void onSetDeviceOrientation(int param1Int);
    
    public abstract void onSetDisplaySurface(Surface param1Surface);
    
    public abstract void onSetPauseImage(Uri param1Uri);
    
    public abstract void onSetPreviewSurface(Surface param1Surface);
    
    public abstract void onSetZoom(float param1Float);
  }
  
  private final Listener mConnectionDeathListener = new Listener() {
      final Connection this$0;
      
      public void onDestroyed(Connection param1Connection) {
        if (Connection.this.mConferenceables.remove(param1Connection))
          Connection.this.fireOnConferenceableConnectionsChanged(); 
      }
    };
  
  private final Conference.Listener mConferenceDeathListener = new Conference.Listener() {
      final Connection this$0;
      
      public void onDestroyed(Conference param1Conference) {
        if (Connection.this.mConferenceables.remove(param1Conference))
          Connection.this.fireOnConferenceableConnectionsChanged(); 
      }
    };
  
  private final Set<Listener> mListeners = Collections.newSetFromMap(new ConcurrentHashMap<>(8, 0.9F, 1));
  
  public static final int AUDIO_CODEC_AMR = 1;
  
  public static final int AUDIO_CODEC_AMR_WB = 2;
  
  public static final int AUDIO_CODEC_EVRC = 4;
  
  public static final int AUDIO_CODEC_EVRC_B = 5;
  
  public static final int AUDIO_CODEC_EVRC_NW = 7;
  
  public static final int AUDIO_CODEC_EVRC_WB = 6;
  
  public static final int AUDIO_CODEC_EVS_FB = 20;
  
  public static final int AUDIO_CODEC_EVS_NB = 17;
  
  public static final int AUDIO_CODEC_EVS_SWB = 19;
  
  public static final int AUDIO_CODEC_EVS_WB = 18;
  
  public static final int AUDIO_CODEC_G711A = 13;
  
  public static final int AUDIO_CODEC_G711AB = 15;
  
  public static final int AUDIO_CODEC_G711U = 11;
  
  public static final int AUDIO_CODEC_G722 = 14;
  
  public static final int AUDIO_CODEC_G723 = 12;
  
  public static final int AUDIO_CODEC_G729 = 16;
  
  public static final int AUDIO_CODEC_GSM_EFR = 8;
  
  public static final int AUDIO_CODEC_GSM_FR = 9;
  
  public static final int AUDIO_CODEC_GSM_HR = 10;
  
  public static final int AUDIO_CODEC_NONE = 0;
  
  public static final int AUDIO_CODEC_QCELP13K = 3;
  
  public static final int CAPABILITY_ADD_PARTICIPANT = 67108864;
  
  public static final int CAPABILITY_CANNOT_DOWNGRADE_VIDEO_TO_AUDIO = 8388608;
  
  public static final int CAPABILITY_CAN_PAUSE_VIDEO = 1048576;
  
  public static final int CAPABILITY_CAN_PULL_CALL = 16777216;
  
  public static final int CAPABILITY_CAN_SEND_RESPONSE_VIA_CONNECTION = 4194304;
  
  public static final int CAPABILITY_CAN_UPGRADE_TO_VIDEO = 524288;
  
  @SystemApi
  public static final int CAPABILITY_CONFERENCE_HAS_NO_CHILDREN = 2097152;
  
  public static final int CAPABILITY_DISCONNECT_FROM_CONFERENCE = 8192;
  
  public static final int CAPABILITY_HOLD = 1;
  
  public static final int CAPABILITY_MANAGE_CONFERENCE = 128;
  
  public static final int CAPABILITY_MERGE_CONFERENCE = 4;
  
  public static final int CAPABILITY_MUTE = 64;
  
  public static final int CAPABILITY_RESPOND_VIA_TEXT = 32;
  
  public static final int CAPABILITY_SEPARATE_FROM_CONFERENCE = 4096;
  
  @SystemApi
  public static final int CAPABILITY_SPEED_UP_MT_AUDIO = 262144;
  
  public static final int CAPABILITY_SUPPORTS_RTT_REMOTE = 536870912;
  
  public static final int CAPABILITY_SUPPORTS_VT_LOCAL_BIDIRECTIONAL = 768;
  
  public static final int CAPABILITY_SUPPORTS_VT_LOCAL_RX = 256;
  
  public static final int CAPABILITY_SUPPORTS_VT_LOCAL_TX = 512;
  
  public static final int CAPABILITY_SUPPORTS_VT_REMOTE_BIDIRECTIONAL = 3072;
  
  public static final int CAPABILITY_SUPPORTS_VT_REMOTE_RX = 1024;
  
  public static final int CAPABILITY_SUPPORTS_VT_REMOTE_TX = 2048;
  
  public static final int CAPABILITY_SUPPORT_DEFLECT = 33554432;
  
  public static final int CAPABILITY_SUPPORT_HOLD = 2;
  
  public static final int CAPABILITY_SWAP_CONFERENCE = 8;
  
  public static final int CAPABILITY_TRANSFER = 134217728;
  
  public static final int CAPABILITY_TRANSFER_CONSULTATIVE = 268435456;
  
  public static final int CAPABILITY_UNUSED = 16;
  
  public static final int CAPABILITY_UNUSED_2 = 16384;
  
  public static final int CAPABILITY_UNUSED_3 = 32768;
  
  public static final int CAPABILITY_UNUSED_4 = 65536;
  
  public static final int CAPABILITY_UNUSED_5 = 131072;
  
  private static final boolean DEBUG = false;
  
  public static final String EVENT_CALL_HOLD_FAILED = "android.telecom.event.CALL_HOLD_FAILED";
  
  public static final String EVENT_CALL_MERGE_FAILED = "android.telecom.event.CALL_MERGE_FAILED";
  
  public static final String EVENT_CALL_PULL_FAILED = "android.telecom.event.CALL_PULL_FAILED";
  
  public static final String EVENT_CALL_REMOTELY_HELD = "android.telecom.event.CALL_REMOTELY_HELD";
  
  public static final String EVENT_CALL_REMOTELY_UNHELD = "android.telecom.event.CALL_REMOTELY_UNHELD";
  
  public static final String EVENT_CALL_SWITCH_FAILED = "android.telecom.event.CALL_SWITCH_FAILED";
  
  public static final String EVENT_HANDOVER_COMPLETE = "android.telecom.event.HANDOVER_COMPLETE";
  
  public static final String EVENT_HANDOVER_FAILED = "android.telecom.event.HANDOVER_FAILED";
  
  public static final String EVENT_MERGE_COMPLETE = "android.telecom.event.MERGE_COMPLETE";
  
  public static final String EVENT_MERGE_START = "android.telecom.event.MERGE_START";
  
  public static final String EVENT_ON_HOLD_TONE_END = "android.telecom.event.ON_HOLD_TONE_END";
  
  public static final String EVENT_ON_HOLD_TONE_START = "android.telecom.event.ON_HOLD_TONE_START";
  
  public static final String EVENT_RTT_AUDIO_INDICATION_CHANGED = "android.telecom.event.RTT_AUDIO_INDICATION_CHANGED";
  
  public static final String EXTRA_ANSWERING_DROPS_FG_CALL = "android.telecom.extra.ANSWERING_DROPS_FG_CALL";
  
  public static final String EXTRA_ANSWERING_DROPS_FG_CALL_APP_NAME = "android.telecom.extra.ANSWERING_DROPS_FG_CALL_APP_NAME";
  
  public static final String EXTRA_AUDIO_CODEC = "android.telecom.extra.AUDIO_CODEC";
  
  public static final String EXTRA_CALL_SUBJECT = "android.telecom.extra.CALL_SUBJECT";
  
  public static final String EXTRA_CHILD_ADDRESS = "android.telecom.extra.CHILD_ADDRESS";
  
  @SystemApi
  public static final String EXTRA_DISABLE_ADD_CALL = "android.telecom.extra.DISABLE_ADD_CALL";
  
  public static final String EXTRA_IS_RTT_AUDIO_PRESENT = "android.telecom.extra.IS_RTT_AUDIO_PRESENT";
  
  public static final String EXTRA_LAST_FORWARDED_NUMBER = "android.telecom.extra.LAST_FORWARDED_NUMBER";
  
  public static final String EXTRA_ORIGINAL_CONNECTION_ID = "android.telecom.extra.ORIGINAL_CONNECTION_ID";
  
  public static final String EXTRA_REMOTE_CONNECTION_ORIGINATING_PACKAGE_NAME = "android.telecom.extra.REMOTE_CONNECTION_ORIGINATING_PACKAGE_NAME";
  
  public static final String EXTRA_REMOTE_PHONE_ACCOUNT_HANDLE = "android.telecom.extra.REMOTE_PHONE_ACCOUNT_HANDLE";
  
  public static final String EXTRA_SIP_INVITE = "android.telecom.extra.SIP_INVITE";
  
  public static final int PROPERTY_ASSISTED_DIALING = 512;
  
  @SystemApi
  public static final int PROPERTY_EMERGENCY_CALLBACK_MODE = 1;
  
  @SystemApi
  public static final int PROPERTY_GENERIC_CONFERENCE = 2;
  
  public static final int PROPERTY_HAS_CDMA_VOICE_PRIVACY = 32;
  
  public static final int PROPERTY_HIGH_DEF_AUDIO = 4;
  
  public static final int PROPERTY_IS_ADHOC_CONFERENCE = 4096;
  
  @SystemApi
  public static final int PROPERTY_IS_DOWNGRADED_CONFERENCE = 64;
  
  public static final int PROPERTY_IS_EXTERNAL_CALL = 16;
  
  public static final int PROPERTY_IS_RTT = 256;
  
  public static final int PROPERTY_NETWORK_IDENTIFIED_EMERGENCY_CALL = 1024;
  
  @SystemApi
  public static final int PROPERTY_REMOTELY_HOSTED = 2048;
  
  public static final int PROPERTY_SELF_MANAGED = 128;
  
  public static final int PROPERTY_WIFI = 8;
  
  public static final int STATE_ACTIVE = 4;
  
  public static final int STATE_DIALING = 3;
  
  public static final int STATE_DISCONNECTED = 6;
  
  public static final int STATE_HOLDING = 5;
  
  public static final int STATE_INITIALIZING = 0;
  
  public static final int STATE_NEW = 1;
  
  public static final int STATE_PULLING_CALL = 7;
  
  public static final int STATE_RINGING = 2;
  
  public static final int VERIFICATION_STATUS_FAILED = 2;
  
  public static final int VERIFICATION_STATUS_NOT_VERIFIED = 0;
  
  public static final int VERIFICATION_STATUS_PASSED = 1;
  
  private Uri mAddress;
  
  private int mAddressPresentation;
  
  private boolean mAudioModeIsVoip;
  
  private CallAudioState mCallAudioState;
  
  private int mCallDirection;
  
  private String mCallerDisplayName;
  
  private int mCallerDisplayNamePresentation;
  
  private int mCallerNumberVerificationStatus;
  
  private Conference mConference;
  
  private final List<Conferenceable> mConferenceables;
  
  private long mConnectElapsedTimeMillis;
  
  private long mConnectTimeMillis;
  
  private int mConnectionCapabilities;
  
  private int mConnectionProperties;
  
  private ConnectionService mConnectionService;
  
  private DisconnectCause mDisconnectCause;
  
  private Bundle mExtras;
  
  private final Object mExtrasLock;
  
  private PhoneAccountHandle mPhoneAccountHandle;
  
  private Set<String> mPreviousExtraKeys;
  
  private boolean mRingbackRequested;
  
  private int mState;
  
  private StatusHints mStatusHints;
  
  private int mSupportedAudioRoutes;
  
  private String mTelecomCallId;
  
  private final List<Conferenceable> mUnmodifiableConferenceables;
  
  private VideoProvider mVideoProvider;
  
  private int mVideoState;
  
  public Connection() {
    ArrayList<Conferenceable> arrayList = new ArrayList();
    this.mUnmodifiableConferenceables = Collections.unmodifiableList(arrayList);
    this.mState = 1;
    this.mRingbackRequested = false;
    this.mSupportedAudioRoutes = 15;
    this.mConnectTimeMillis = 0L;
    this.mConnectElapsedTimeMillis = 0L;
    this.mExtrasLock = new Object();
    this.mCallDirection = -1;
  }
  
  @SystemApi
  public final String getTelecomCallId() {
    return this.mTelecomCallId;
  }
  
  public final Uri getAddress() {
    return this.mAddress;
  }
  
  public final int getAddressPresentation() {
    return this.mAddressPresentation;
  }
  
  public final String getCallerDisplayName() {
    return this.mCallerDisplayName;
  }
  
  public final int getCallerDisplayNamePresentation() {
    return this.mCallerDisplayNamePresentation;
  }
  
  public final int getState() {
    return this.mState;
  }
  
  public final int getVideoState() {
    return this.mVideoState;
  }
  
  @SystemApi
  @Deprecated
  public final AudioState getAudioState() {
    if (this.mCallAudioState == null)
      return null; 
    return new AudioState(this.mCallAudioState);
  }
  
  public final CallAudioState getCallAudioState() {
    return this.mCallAudioState;
  }
  
  public final Conference getConference() {
    return this.mConference;
  }
  
  public final boolean isRingbackRequested() {
    return this.mRingbackRequested;
  }
  
  public final boolean getAudioModeIsVoip() {
    return this.mAudioModeIsVoip;
  }
  
  @SystemApi
  public final long getConnectTimeMillis() {
    return this.mConnectTimeMillis;
  }
  
  @SystemApi
  public final long getConnectionStartElapsedRealtimeMillis() {
    return this.mConnectElapsedTimeMillis;
  }
  
  public final StatusHints getStatusHints() {
    return this.mStatusHints;
  }
  
  public final Bundle getExtras() {
    null = null;
    synchronized (this.mExtrasLock) {
      if (this.mExtras != null) {
        null = new Bundle();
        this(this.mExtras);
      } 
      return null;
    } 
  }
  
  final Connection addConnectionListener(Listener paramListener) {
    this.mListeners.add(paramListener);
    return this;
  }
  
  final Connection removeConnectionListener(Listener paramListener) {
    if (paramListener != null)
      this.mListeners.remove(paramListener); 
    return this;
  }
  
  public final DisconnectCause getDisconnectCause() {
    return this.mDisconnectCause;
  }
  
  @SystemApi
  public void setTelecomCallId(String paramString) {
    this.mTelecomCallId = paramString;
  }
  
  final void setCallAudioState(CallAudioState paramCallAudioState) {
    checkImmutable();
    Log.d(this, "setAudioState %s", new Object[] { paramCallAudioState });
    this.mCallAudioState = paramCallAudioState;
    onAudioStateChanged(getAudioState());
    onCallAudioStateChanged(paramCallAudioState);
  }
  
  public static String stateToString(int paramInt) {
    switch (paramInt) {
      default:
        Log.wtf(Connection.class, "Unknown state %d", new Object[] { Integer.valueOf(paramInt) });
        return "UNKNOWN";
      case 7:
        return "PULLING_CALL";
      case 6:
        return "DISCONNECTED";
      case 5:
        return "HOLDING";
      case 4:
        return "ACTIVE";
      case 3:
        return "DIALING";
      case 2:
        return "RINGING";
      case 1:
        return "NEW";
      case 0:
        break;
    } 
    return "INITIALIZING";
  }
  
  public final int getConnectionCapabilities() {
    return this.mConnectionCapabilities;
  }
  
  public final int getConnectionProperties() {
    return this.mConnectionProperties;
  }
  
  public final int getSupportedAudioRoutes() {
    return this.mSupportedAudioRoutes;
  }
  
  public final void setAddress(Uri paramUri, int paramInt) {
    this.mAddress = paramUri;
    this.mAddressPresentation = paramInt;
    for (Listener listener : this.mListeners)
      listener.onAddressChanged(this, paramUri, paramInt); 
  }
  
  public final void setCallerDisplayName(String paramString, int paramInt) {
    checkImmutable();
    this.mCallerDisplayName = paramString;
    this.mCallerDisplayNamePresentation = paramInt;
    for (Listener listener : this.mListeners)
      listener.onCallerDisplayNameChanged(this, paramString, paramInt); 
  }
  
  public final void setVideoState(int paramInt) {
    checkImmutable();
    Log.d(this, "setVideoState %d", new Object[] { Integer.valueOf(paramInt) });
    this.mVideoState = paramInt;
    for (Listener listener : this.mListeners)
      listener.onVideoStateChanged(this, this.mVideoState); 
  }
  
  public final void setActive() {
    checkImmutable();
    setRingbackRequested(false);
    setState(4);
  }
  
  public final void setRinging() {
    checkImmutable();
    setState(2);
  }
  
  public final void setInitializing() {
    checkImmutable();
    setState(0);
  }
  
  public final void setInitialized() {
    checkImmutable();
    setState(1);
  }
  
  public final void setDialing() {
    checkImmutable();
    setState(3);
  }
  
  public final void setPulling() {
    checkImmutable();
    setState(7);
  }
  
  public final void setOnHold() {
    checkImmutable();
    setState(5);
  }
  
  public final void setVideoProvider(VideoProvider paramVideoProvider) {
    checkImmutable();
    this.mVideoProvider = paramVideoProvider;
    for (Listener listener : this.mListeners)
      listener.onVideoProviderChanged(this, paramVideoProvider); 
  }
  
  public final VideoProvider getVideoProvider() {
    return this.mVideoProvider;
  }
  
  public final void setDisconnected(DisconnectCause paramDisconnectCause) {
    checkImmutable();
    this.mDisconnectCause = paramDisconnectCause;
    setState(6);
    Log.d(this, "Disconnected with cause %s", new Object[] { paramDisconnectCause });
    for (Listener listener : this.mListeners)
      listener.onDisconnected(this, paramDisconnectCause); 
  }
  
  public final void setPostDialWait(String paramString) {
    checkImmutable();
    for (Listener listener : this.mListeners)
      listener.onPostDialWait(this, paramString); 
  }
  
  public final void setNextPostDialChar(char paramChar) {
    checkImmutable();
    for (Listener listener : this.mListeners)
      listener.onPostDialChar(this, paramChar); 
  }
  
  public final void setRingbackRequested(boolean paramBoolean) {
    checkImmutable();
    if (this.mRingbackRequested != paramBoolean) {
      this.mRingbackRequested = paramBoolean;
      for (Listener listener : this.mListeners)
        listener.onRingbackRequested(this, paramBoolean); 
    } 
  }
  
  public final void setConnectionCapabilities(int paramInt) {
    checkImmutable();
    if (this.mConnectionCapabilities != paramInt) {
      this.mConnectionCapabilities = paramInt;
      for (Listener listener : this.mListeners)
        listener.onConnectionCapabilitiesChanged(this, this.mConnectionCapabilities); 
    } 
  }
  
  public final void setConnectionProperties(int paramInt) {
    checkImmutable();
    if (this.mConnectionProperties != paramInt) {
      this.mConnectionProperties = paramInt;
      for (Listener listener : this.mListeners)
        listener.onConnectionPropertiesChanged(this, this.mConnectionProperties); 
    } 
  }
  
  public final void setSupportedAudioRoutes(int paramInt) {
    if ((paramInt & 0x9) != 0) {
      if (this.mSupportedAudioRoutes != paramInt) {
        this.mSupportedAudioRoutes = paramInt;
        for (Listener listener : this.mListeners)
          listener.onSupportedAudioRoutesChanged(this, this.mSupportedAudioRoutes); 
      } 
      return;
    } 
    throw new IllegalArgumentException("supported audio routes must include either speaker or earpiece");
  }
  
  public final void destroy() {
    for (Listener listener : this.mListeners)
      listener.onDestroyed(this); 
  }
  
  public final void setAudioModeIsVoip(boolean paramBoolean) {
    checkImmutable();
    this.mAudioModeIsVoip = paramBoolean;
    for (Listener listener : this.mListeners)
      listener.onAudioModeIsVoipChanged(this, paramBoolean); 
  }
  
  @SystemApi
  public final void setConnectTimeMillis(long paramLong) {
    this.mConnectTimeMillis = paramLong;
  }
  
  @SystemApi
  public final void setConnectionStartElapsedRealtimeMillis(long paramLong) {
    this.mConnectElapsedTimeMillis = paramLong;
  }
  
  public final void setStatusHints(StatusHints paramStatusHints) {
    checkImmutable();
    this.mStatusHints = paramStatusHints;
    for (Listener listener : this.mListeners)
      listener.onStatusHintsChanged(this, paramStatusHints); 
  }
  
  public final void setConferenceableConnections(List<Connection> paramList) {
    checkImmutable();
    clearConferenceableList();
    for (Connection connection : paramList) {
      if (!this.mConferenceables.contains(connection)) {
        connection.addConnectionListener(this.mConnectionDeathListener);
        this.mConferenceables.add(connection);
      } 
    } 
    fireOnConferenceableConnectionsChanged();
  }
  
  public final void setConferenceables(List<Conferenceable> paramList) {
    clearConferenceableList();
    for (Conferenceable conferenceable : paramList) {
      if (!this.mConferenceables.contains(conferenceable)) {
        if (conferenceable instanceof Connection) {
          Connection connection = (Connection)conferenceable;
          connection.addConnectionListener(this.mConnectionDeathListener);
        } else if (conferenceable instanceof Conference) {
          Conference conference = (Conference)conferenceable;
          conference.addListener(this.mConferenceDeathListener);
        } 
        this.mConferenceables.add(conferenceable);
      } 
    } 
    fireOnConferenceableConnectionsChanged();
  }
  
  @SystemApi
  public final void resetConnectionTime() {
    for (Listener listener : this.mListeners)
      listener.onConnectionTimeReset(this); 
  }
  
  public final List<Conferenceable> getConferenceables() {
    return this.mUnmodifiableConferenceables;
  }
  
  public final void setConnectionService(ConnectionService paramConnectionService) {
    checkImmutable();
    if (this.mConnectionService != null) {
      Log.e(this, new Exception(), "Trying to set ConnectionService on a connection which is already associated with another ConnectionService.", new Object[0]);
    } else {
      this.mConnectionService = paramConnectionService;
    } 
  }
  
  public final void unsetConnectionService(ConnectionService paramConnectionService) {
    if (this.mConnectionService != paramConnectionService) {
      Log.e(this, new Exception(), "Trying to remove ConnectionService from a Connection that does not belong to the ConnectionService.", new Object[0]);
    } else {
      this.mConnectionService = null;
    } 
  }
  
  public final boolean setConference(Conference paramConference) {
    checkImmutable();
    if (this.mConference == null) {
      this.mConference = paramConference;
      ConnectionService connectionService = this.mConnectionService;
      if (connectionService != null && connectionService.containsConference(paramConference))
        fireConferenceChanged(); 
      return true;
    } 
    return false;
  }
  
  public final void resetConference() {
    if (this.mConference != null) {
      Log.d(this, "Conference reset", new Object[0]);
      this.mConference = null;
      fireConferenceChanged();
    } 
  }
  
  public final void setExtras(Bundle paramBundle) {
    checkImmutable();
    putExtras(paramBundle);
    if (this.mPreviousExtraKeys != null) {
      ArrayList<String> arrayList = new ArrayList();
      for (String str : this.mPreviousExtraKeys) {
        if (paramBundle == null || !paramBundle.containsKey(str))
          arrayList.add(str); 
      } 
      if (!arrayList.isEmpty())
        removeExtras(arrayList); 
    } 
    if (this.mPreviousExtraKeys == null)
      this.mPreviousExtraKeys = new ArraySet<>(); 
    this.mPreviousExtraKeys.clear();
    if (paramBundle != null)
      this.mPreviousExtraKeys.addAll(paramBundle.keySet()); 
  }
  
  public final void putExtras(Bundle paramBundle) {
    checkImmutable();
    if (paramBundle == null)
      return; 
    synchronized (this.mExtrasLock) {
      if (this.mExtras == null) {
        Bundle bundle = new Bundle();
        this();
        this.mExtras = bundle;
      } 
      this.mExtras.putAll(paramBundle);
      paramBundle = new Bundle();
      this(this.mExtras);
      for (Listener null : this.mListeners)
        null.onExtrasChanged(this, new Bundle(paramBundle)); 
      return;
    } 
  }
  
  public final void removeExtras(List<String> paramList) {
    synchronized (this.mExtrasLock) {
      if (this.mExtras != null)
        for (String str : paramList)
          this.mExtras.remove(str);  
      paramList = Collections.unmodifiableList(paramList);
      for (Listener null : this.mListeners)
        null.onExtrasRemoved(this, paramList); 
      return;
    } 
  }
  
  public final void removeExtras(String... paramVarArgs) {
    removeExtras(Arrays.asList(paramVarArgs));
  }
  
  public final void setAudioRoute(int paramInt) {
    for (Listener listener : this.mListeners)
      listener.onAudioRouteChanged(this, paramInt, null); 
  }
  
  public void requestBluetoothAudio(BluetoothDevice paramBluetoothDevice) {
    for (Listener listener : this.mListeners) {
      String str = paramBluetoothDevice.getAddress();
      listener.onAudioRouteChanged(this, 2, str);
    } 
  }
  
  public final void sendRttInitiationSuccess() {
    this.mListeners.forEach(new _$$Lambda$Connection$8xeoCKtoHEwnDqv6gbuSfOMODH0(this));
  }
  
  public final void sendRttInitiationFailure(int paramInt) {
    this.mListeners.forEach(new _$$Lambda$Connection$noXZvls4rxmO_SOjgkFMZLLrfSg(this, paramInt));
  }
  
  public final void sendRttSessionRemotelyTerminated() {
    this.mListeners.forEach(new _$$Lambda$Connection$SYsjtKchY2AYvOeGveCrqxSfKTU(this));
  }
  
  public final void sendRemoteRttRequest() {
    this.mListeners.forEach(new _$$Lambda$Connection$lnfFNF0t9fPLEf01JE291g4chSk(this));
  }
  
  @SystemApi
  @Deprecated
  public void onAudioStateChanged(AudioState paramAudioState) {}
  
  public void onCallAudioStateChanged(CallAudioState paramCallAudioState) {}
  
  public void onStateChanged(int paramInt) {}
  
  public void onPlayDtmfTone(char paramChar) {}
  
  public void onStopDtmfTone() {}
  
  public void onDisconnect() {}
  
  public void onDisconnectConferenceParticipant(Uri paramUri) {}
  
  public void onSeparate() {}
  
  public void onAddConferenceParticipants(List<Uri> paramList) {}
  
  public void onAbort() {}
  
  public void onHold() {}
  
  public void onUnhold() {}
  
  public void onAnswer(int paramInt) {}
  
  public void onAnswer() {
    onAnswer(0);
  }
  
  public void onDeflect(Uri paramUri) {}
  
  public void onReject() {}
  
  public void onReject(int paramInt) {}
  
  public void onReject(String paramString) {}
  
  public void onTransfer(Uri paramUri, boolean paramBoolean) {}
  
  public void onTransfer(Connection paramConnection) {}
  
  public void onSilence() {}
  
  public void onPostDialContinue(boolean paramBoolean) {}
  
  public void onPullExternalCall() {}
  
  public void onCallEvent(String paramString, Bundle paramBundle) {}
  
  public void onHandoverComplete() {}
  
  public void onExtrasChanged(Bundle paramBundle) {}
  
  public void onShowIncomingCallUi() {}
  
  public void onStartRtt(RttTextStream paramRttTextStream) {}
  
  public void onStopRtt() {}
  
  public void handleRttUpgradeResponse(RttTextStream paramRttTextStream) {}
  
  static String toLogSafePhoneNumber(String paramString) {
    if (paramString == null)
      return ""; 
    if (PII_DEBUG)
      return paramString; 
    StringBuilder stringBuilder = new StringBuilder();
    for (byte b = 0; b < paramString.length(); b++) {
      char c = paramString.charAt(b);
      if (c == '-' || c == '@' || c == '.') {
        stringBuilder.append(c);
      } else {
        stringBuilder.append('x');
      } 
    } 
    return stringBuilder.toString();
  }
  
  private void setState(int paramInt) {
    checkImmutable();
    int i = this.mState;
    if (i == 6 && i != paramInt) {
      Log.d(this, "Connection already DISCONNECTED; cannot transition out of this state.", new Object[0]);
      return;
    } 
    if (this.mState != paramInt) {
      Log.d(this, "setState: %s", new Object[] { stateToString(paramInt) });
      this.mState = paramInt;
      onStateChanged(paramInt);
      for (Listener listener : this.mListeners)
        listener.onStateChanged(this, paramInt); 
    } 
  }
  
  class FailureSignalingConnection extends Connection {
    private boolean mImmutable = false;
    
    public FailureSignalingConnection(Connection this$0) {
      setDisconnected((DisconnectCause)this$0);
      this.mImmutable = true;
    }
    
    public void checkImmutable() {
      if (!this.mImmutable)
        return; 
      throw new UnsupportedOperationException("Connection is immutable");
    }
  }
  
  public static Connection createFailedConnection(DisconnectCause paramDisconnectCause) {
    return new FailureSignalingConnection(paramDisconnectCause);
  }
  
  public void checkImmutable() {}
  
  public static Connection createCanceledConnection() {
    return new FailureSignalingConnection(new DisconnectCause(4));
  }
  
  private final void fireOnConferenceableConnectionsChanged() {
    for (Listener listener : this.mListeners)
      listener.onConferenceablesChanged(this, getConferenceables()); 
  }
  
  private final void fireConferenceChanged() {
    for (Listener listener : this.mListeners)
      listener.onConferenceChanged(this, this.mConference); 
  }
  
  private final void clearConferenceableList() {
    for (Conferenceable conferenceable : this.mConferenceables) {
      if (conferenceable instanceof Connection) {
        conferenceable = conferenceable;
        conferenceable.removeConnectionListener(this.mConnectionDeathListener);
        continue;
      } 
      if (conferenceable instanceof Conference) {
        conferenceable = conferenceable;
        conferenceable.removeListener(this.mConferenceDeathListener);
      } 
    } 
    this.mConferenceables.clear();
  }
  
  final void handleExtrasChanged(Bundle paramBundle) {
    Bundle bundle = null;
    synchronized (this.mExtrasLock) {
      this.mExtras = paramBundle;
      if (paramBundle != null) {
        bundle = new Bundle();
        this(this.mExtras);
      } 
      onExtrasChanged(bundle);
      return;
    } 
  }
  
  public final void notifyConferenceMergeFailed() {
    for (Listener listener : this.mListeners)
      listener.onConferenceMergeFailed(this); 
  }
  
  public void notifyPhoneAccountChanged(PhoneAccountHandle paramPhoneAccountHandle) {
    for (Listener listener : this.mListeners)
      listener.onPhoneAccountChanged(this, paramPhoneAccountHandle); 
  }
  
  @SystemApi
  public void setPhoneAccountHandle(PhoneAccountHandle paramPhoneAccountHandle) {
    if (this.mPhoneAccountHandle != paramPhoneAccountHandle) {
      this.mPhoneAccountHandle = paramPhoneAccountHandle;
      notifyPhoneAccountChanged(paramPhoneAccountHandle);
    } 
  }
  
  @SystemApi
  public PhoneAccountHandle getPhoneAccountHandle() {
    return this.mPhoneAccountHandle;
  }
  
  public void sendConnectionEvent(String paramString, Bundle paramBundle) {
    for (Listener listener : this.mListeners)
      listener.onConnectionEvent(this, paramString, paramBundle); 
  }
  
  public final int getCallDirection() {
    return this.mCallDirection;
  }
  
  @SystemApi
  public void setCallDirection(int paramInt) {
    this.mCallDirection = paramInt;
  }
  
  public final int getCallerNumberVerificationStatus() {
    return this.mCallerNumberVerificationStatus;
  }
  
  public final void setCallerNumberVerificationStatus(int paramInt) {
    this.mCallerNumberVerificationStatus = paramInt;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class AudioCodec implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class VerificationStatus implements Annotation {}
}
