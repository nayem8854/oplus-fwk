package android.telecom;

import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.telecom.IVideoProvider;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class ParcelableConference implements Parcelable {
  class Builder {
    private List<String> mConnectionIds = Collections.emptyList();
    
    private long mConnectTimeMillis = 0L;
    
    private int mVideoState = 0;
    
    private long mConnectElapsedTimeMillis = 0L;
    
    private int mAddressPresentation = 3;
    
    private int mCallerDisplayNamePresentation = 3;
    
    private int mCallDirection = -1;
    
    private Uri mAddress;
    
    private String mCallerDisplayName;
    
    private int mConnectionCapabilities;
    
    private int mConnectionProperties;
    
    private DisconnectCause mDisconnectCause;
    
    private Bundle mExtras;
    
    private final PhoneAccountHandle mPhoneAccount;
    
    private boolean mRingbackRequested;
    
    private final int mState;
    
    private StatusHints mStatusHints;
    
    private IVideoProvider mVideoProvider;
    
    public Builder(ParcelableConference this$0, int param1Int) {
      this.mPhoneAccount = (PhoneAccountHandle)this$0;
      this.mState = param1Int;
    }
    
    public Builder setDisconnectCause(DisconnectCause param1DisconnectCause) {
      this.mDisconnectCause = param1DisconnectCause;
      return this;
    }
    
    public Builder setRingbackRequested(boolean param1Boolean) {
      this.mRingbackRequested = param1Boolean;
      return this;
    }
    
    public Builder setCallerDisplayName(String param1String, int param1Int) {
      this.mCallerDisplayName = param1String;
      this.mCallerDisplayNamePresentation = param1Int;
      return this;
    }
    
    public Builder setAddress(Uri param1Uri, int param1Int) {
      this.mAddress = param1Uri;
      this.mAddressPresentation = param1Int;
      return this;
    }
    
    public Builder setExtras(Bundle param1Bundle) {
      this.mExtras = param1Bundle;
      return this;
    }
    
    public Builder setStatusHints(StatusHints param1StatusHints) {
      this.mStatusHints = param1StatusHints;
      return this;
    }
    
    public Builder setConnectTimeMillis(long param1Long1, long param1Long2) {
      this.mConnectTimeMillis = param1Long1;
      this.mConnectElapsedTimeMillis = param1Long2;
      return this;
    }
    
    public Builder setVideoAttributes(IVideoProvider param1IVideoProvider, int param1Int) {
      this.mVideoProvider = param1IVideoProvider;
      this.mVideoState = param1Int;
      return this;
    }
    
    public Builder setConnectionIds(List<String> param1List) {
      this.mConnectionIds = param1List;
      return this;
    }
    
    public Builder setConnectionProperties(int param1Int) {
      this.mConnectionProperties = param1Int;
      return this;
    }
    
    public Builder setConnectionCapabilities(int param1Int) {
      this.mConnectionCapabilities = param1Int;
      return this;
    }
    
    public Builder setCallDirection(int param1Int) {
      this.mCallDirection = param1Int;
      return this;
    }
    
    public ParcelableConference build() {
      return new ParcelableConference(this.mPhoneAccount, this.mState, this.mConnectionCapabilities, this.mConnectionProperties, this.mConnectionIds, this.mVideoProvider, this.mVideoState, this.mConnectTimeMillis, this.mConnectElapsedTimeMillis, this.mStatusHints, this.mExtras, this.mAddress, this.mAddressPresentation, this.mCallerDisplayName, this.mCallerDisplayNamePresentation, this.mDisconnectCause, this.mRingbackRequested, this.mCallDirection);
    }
  }
  
  private ParcelableConference(PhoneAccountHandle paramPhoneAccountHandle, int paramInt1, int paramInt2, int paramInt3, List<String> paramList, IVideoProvider paramIVideoProvider, int paramInt4, long paramLong1, long paramLong2, StatusHints paramStatusHints, Bundle paramBundle, Uri paramUri, int paramInt5, String paramString, int paramInt6, DisconnectCause paramDisconnectCause, boolean paramBoolean, int paramInt7) {
    this.mPhoneAccount = paramPhoneAccountHandle;
    this.mState = paramInt1;
    this.mConnectionCapabilities = paramInt2;
    this.mConnectionProperties = paramInt3;
    this.mConnectionIds = paramList;
    this.mVideoProvider = paramIVideoProvider;
    this.mVideoState = paramInt4;
    this.mConnectTimeMillis = paramLong1;
    this.mStatusHints = paramStatusHints;
    this.mExtras = paramBundle;
    this.mConnectElapsedTimeMillis = paramLong2;
    this.mAddress = paramUri;
    this.mAddressPresentation = paramInt5;
    this.mCallerDisplayName = paramString;
    this.mCallerDisplayNamePresentation = paramInt6;
    this.mDisconnectCause = paramDisconnectCause;
    this.mRingbackRequested = paramBoolean;
    this.mCallDirection = paramInt7;
  }
  
  public String toString() {
    StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append("account: ");
    PhoneAccountHandle phoneAccountHandle = this.mPhoneAccount;
    stringBuffer.append(phoneAccountHandle);
    stringBuffer.append(", state: ");
    int i = this.mState;
    stringBuffer.append(Connection.stateToString(i));
    stringBuffer.append(", capabilities: ");
    i = this.mConnectionCapabilities;
    stringBuffer.append(Connection.capabilitiesToString(i));
    stringBuffer.append(", properties: ");
    i = this.mConnectionProperties;
    stringBuffer.append(Connection.propertiesToString(i));
    stringBuffer.append(", connectTime: ");
    long l = this.mConnectTimeMillis;
    stringBuffer.append(l);
    stringBuffer.append(", children: ");
    List<String> list = this.mConnectionIds;
    stringBuffer.append(list);
    stringBuffer.append(", VideoState: ");
    i = this.mVideoState;
    stringBuffer.append(i);
    stringBuffer.append(", VideoProvider: ");
    IVideoProvider iVideoProvider = this.mVideoProvider;
    stringBuffer.append(iVideoProvider);
    stringBuffer.append(", isRingbackRequested: ");
    boolean bool = this.mRingbackRequested;
    stringBuffer.append(bool);
    stringBuffer.append(", disconnectCause: ");
    DisconnectCause disconnectCause = this.mDisconnectCause;
    stringBuffer.append(disconnectCause);
    stringBuffer.append(", callDirection: ");
    i = this.mCallDirection;
    stringBuffer.append(i);
    return stringBuffer.toString();
  }
  
  public PhoneAccountHandle getPhoneAccount() {
    return this.mPhoneAccount;
  }
  
  public int getState() {
    return this.mState;
  }
  
  public int getConnectionCapabilities() {
    return this.mConnectionCapabilities;
  }
  
  public int getConnectionProperties() {
    return this.mConnectionProperties;
  }
  
  public List<String> getConnectionIds() {
    return this.mConnectionIds;
  }
  
  public long getConnectTimeMillis() {
    return this.mConnectTimeMillis;
  }
  
  public long getConnectElapsedTimeMillis() {
    return this.mConnectElapsedTimeMillis;
  }
  
  public IVideoProvider getVideoProvider() {
    return this.mVideoProvider;
  }
  
  public int getVideoState() {
    return this.mVideoState;
  }
  
  public StatusHints getStatusHints() {
    return this.mStatusHints;
  }
  
  public Bundle getExtras() {
    return this.mExtras;
  }
  
  public Uri getHandle() {
    return this.mAddress;
  }
  
  public final DisconnectCause getDisconnectCause() {
    return this.mDisconnectCause;
  }
  
  public boolean isRingbackRequested() {
    return this.mRingbackRequested;
  }
  
  public int getHandlePresentation() {
    return this.mAddressPresentation;
  }
  
  public int getCallDirection() {
    return this.mCallDirection;
  }
  
  public static final Parcelable.Creator<ParcelableConference> CREATOR = new Parcelable.Creator<ParcelableConference>() {
      public ParcelableConference createFromParcel(Parcel param1Parcel) {
        boolean bool;
        ClassLoader classLoader = ParcelableConference.class.getClassLoader();
        PhoneAccountHandle phoneAccountHandle = (PhoneAccountHandle)param1Parcel.readParcelable(classLoader);
        int i = param1Parcel.readInt();
        int j = param1Parcel.readInt();
        ArrayList arrayList = new ArrayList(2);
        param1Parcel.readList(arrayList, classLoader);
        long l1 = param1Parcel.readLong();
        IVideoProvider iVideoProvider = IVideoProvider.Stub.asInterface(param1Parcel.readStrongBinder());
        int k = param1Parcel.readInt();
        StatusHints statusHints = (StatusHints)param1Parcel.readParcelable(classLoader);
        Bundle bundle = param1Parcel.readBundle(classLoader);
        int m = param1Parcel.readInt();
        long l2 = param1Parcel.readLong();
        Uri uri = (Uri)param1Parcel.readParcelable(classLoader);
        int n = param1Parcel.readInt();
        String str = param1Parcel.readString();
        int i1 = param1Parcel.readInt();
        DisconnectCause disconnectCause = (DisconnectCause)param1Parcel.readParcelable(classLoader);
        if (param1Parcel.readInt() == 1) {
          bool = true;
        } else {
          bool = false;
        } 
        int i2 = param1Parcel.readInt();
        return new ParcelableConference(phoneAccountHandle, i, j, m, arrayList, iVideoProvider, k, l1, l2, statusHints, bundle, uri, n, str, i1, disconnectCause, bool, i2);
      }
      
      public ParcelableConference[] newArray(int param1Int) {
        return new ParcelableConference[param1Int];
      }
    };
  
  private final Uri mAddress;
  
  private final int mAddressPresentation;
  
  private final int mCallDirection;
  
  private final String mCallerDisplayName;
  
  private final int mCallerDisplayNamePresentation;
  
  private final long mConnectElapsedTimeMillis;
  
  private final long mConnectTimeMillis;
  
  private final int mConnectionCapabilities;
  
  private final List<String> mConnectionIds;
  
  private final int mConnectionProperties;
  
  private final DisconnectCause mDisconnectCause;
  
  private final Bundle mExtras;
  
  private final PhoneAccountHandle mPhoneAccount;
  
  private final boolean mRingbackRequested;
  
  private final int mState;
  
  private final StatusHints mStatusHints;
  
  private final IVideoProvider mVideoProvider;
  
  private final int mVideoState;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable(this.mPhoneAccount, 0);
    paramParcel.writeInt(this.mState);
    paramParcel.writeInt(this.mConnectionCapabilities);
    paramParcel.writeList(this.mConnectionIds);
    paramParcel.writeLong(this.mConnectTimeMillis);
    IVideoProvider iVideoProvider = this.mVideoProvider;
    if (iVideoProvider != null) {
      IBinder iBinder = iVideoProvider.asBinder();
    } else {
      iVideoProvider = null;
    } 
    paramParcel.writeStrongBinder((IBinder)iVideoProvider);
    paramParcel.writeInt(this.mVideoState);
    paramParcel.writeParcelable(this.mStatusHints, 0);
    paramParcel.writeBundle(this.mExtras);
    paramParcel.writeInt(this.mConnectionProperties);
    paramParcel.writeLong(this.mConnectElapsedTimeMillis);
    paramParcel.writeParcelable((Parcelable)this.mAddress, 0);
    paramParcel.writeInt(this.mAddressPresentation);
    paramParcel.writeString(this.mCallerDisplayName);
    paramParcel.writeInt(this.mCallerDisplayNamePresentation);
    paramParcel.writeParcelable(this.mDisconnectCause, 0);
    paramParcel.writeInt(this.mRingbackRequested);
    paramParcel.writeInt(this.mCallDirection);
  }
}
