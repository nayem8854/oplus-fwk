package android.telecom;

import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.view.Surface;
import com.android.internal.os.SomeArgs;
import com.android.internal.telecom.IVideoCallback;
import com.android.internal.telecom.IVideoProvider;
import java.util.NoSuchElementException;

public class VideoCallImpl extends InCallService.VideoCall {
  private int mVideoQuality = 0;
  
  private int mVideoState = 0;
  
  private IBinder.DeathRecipient mDeathRecipient = new IBinder.DeathRecipient() {
      final VideoCallImpl this$0;
      
      public void binderDied() {
        try {
          VideoCallImpl.this.mVideoProvider.asBinder().unlinkToDeath(this, 0);
        } catch (NoSuchElementException noSuchElementException) {}
      }
    };
  
  private final VideoCallListenerBinder mBinder;
  
  private InCallService.VideoCall.Callback mCallback;
  
  private final String mCallingPackageName;
  
  private Handler mHandler;
  
  private int mTargetSdkVersion;
  
  private final IVideoProvider mVideoProvider;
  
  class VideoCallListenerBinder extends IVideoCallback.Stub {
    final VideoCallImpl this$0;
    
    private VideoCallListenerBinder() {}
    
    public void receiveSessionModifyRequest(VideoProfile param1VideoProfile) {
      if (VideoCallImpl.this.mHandler == null)
        return; 
      Message message = VideoCallImpl.this.mHandler.obtainMessage(1, param1VideoProfile);
      message.sendToTarget();
    }
    
    public void receiveSessionModifyResponse(int param1Int, VideoProfile param1VideoProfile1, VideoProfile param1VideoProfile2) {
      if (VideoCallImpl.this.mHandler == null)
        return; 
      SomeArgs someArgs = SomeArgs.obtain();
      someArgs.arg1 = Integer.valueOf(param1Int);
      someArgs.arg2 = param1VideoProfile1;
      someArgs.arg3 = param1VideoProfile2;
      Message message = VideoCallImpl.this.mHandler.obtainMessage(2, someArgs);
      message.sendToTarget();
    }
    
    public void handleCallSessionEvent(int param1Int) {
      if (VideoCallImpl.this.mHandler == null)
        return; 
      Message message = VideoCallImpl.this.mHandler.obtainMessage(3, Integer.valueOf(param1Int));
      message.sendToTarget();
    }
    
    public void changePeerDimensions(int param1Int1, int param1Int2) {
      if (VideoCallImpl.this.mHandler == null)
        return; 
      SomeArgs someArgs = SomeArgs.obtain();
      someArgs.arg1 = Integer.valueOf(param1Int1);
      someArgs.arg2 = Integer.valueOf(param1Int2);
      VideoCallImpl.this.mHandler.obtainMessage(4, someArgs).sendToTarget();
    }
    
    public void changeVideoQuality(int param1Int) {
      if (VideoCallImpl.this.mHandler == null)
        return; 
      Message message = VideoCallImpl.this.mHandler.obtainMessage(7, param1Int, 0);
      message.sendToTarget();
    }
    
    public void changeCallDataUsage(long param1Long) {
      if (VideoCallImpl.this.mHandler == null)
        return; 
      Message message = VideoCallImpl.this.mHandler.obtainMessage(5, Long.valueOf(param1Long));
      message.sendToTarget();
    }
    
    public void changeCameraCapabilities(VideoProfile.CameraCapabilities param1CameraCapabilities) {
      if (VideoCallImpl.this.mHandler == null)
        return; 
      Message message = VideoCallImpl.this.mHandler.obtainMessage(6, param1CameraCapabilities);
      message.sendToTarget();
    }
  }
  
  private final class MessageHandler extends Handler {
    private static final int MSG_CHANGE_CALL_DATA_USAGE = 5;
    
    private static final int MSG_CHANGE_CAMERA_CAPABILITIES = 6;
    
    private static final int MSG_CHANGE_PEER_DIMENSIONS = 4;
    
    private static final int MSG_CHANGE_VIDEO_QUALITY = 7;
    
    private static final int MSG_HANDLE_CALL_SESSION_EVENT = 3;
    
    private static final int MSG_RECEIVE_SESSION_MODIFY_REQUEST = 1;
    
    private static final int MSG_RECEIVE_SESSION_MODIFY_RESPONSE = 2;
    
    final VideoCallImpl this$0;
    
    public MessageHandler(Looper param1Looper) {
      super(param1Looper);
    }
    
    public void handleMessage(Message param1Message) {
      SomeArgs someArgs;
      if (VideoCallImpl.this.mCallback == null)
        return; 
      switch (param1Message.what) {
        default:
          return;
        case 7:
          VideoCallImpl.access$302(VideoCallImpl.this, param1Message.arg1);
          VideoCallImpl.this.mCallback.onVideoQualityChanged(param1Message.arg1);
        case 6:
          VideoCallImpl.this.mCallback.onCameraCapabilitiesChanged((VideoProfile.CameraCapabilities)param1Message.obj);
        case 5:
          VideoCallImpl.this.mCallback.onCallDataUsageChanged(((Long)param1Message.obj).longValue());
        case 4:
          someArgs = (SomeArgs)param1Message.obj;
          try {
            int i = ((Integer)someArgs.arg1).intValue();
            int j = ((Integer)someArgs.arg2).intValue();
            VideoCallImpl.this.mCallback.onPeerDimensionsChanged(i, j);
          } finally {
            someArgs.recycle();
          } 
        case 3:
          VideoCallImpl.this.mCallback.onCallSessionEvent(((Integer)((Message)someArgs).obj).intValue());
        case 2:
          someArgs = (SomeArgs)((Message)someArgs).obj;
          try {
            int i = ((Integer)someArgs.arg1).intValue();
            VideoProfile videoProfile2 = (VideoProfile)someArgs.arg2;
            VideoProfile videoProfile1 = (VideoProfile)someArgs.arg3;
            VideoCallImpl.this.mCallback.onSessionModifyResponseReceived(i, videoProfile2, videoProfile1);
          } finally {
            someArgs.recycle();
          } 
        case 1:
          break;
      } 
      VideoCallImpl.this.mCallback.onSessionModifyRequestReceived((VideoProfile)((Message)someArgs).obj);
    }
  }
  
  VideoCallImpl(IVideoProvider paramIVideoProvider, String paramString, int paramInt) throws RemoteException {
    this.mVideoProvider = paramIVideoProvider;
    paramIVideoProvider.asBinder().linkToDeath(this.mDeathRecipient, 0);
    VideoCallListenerBinder videoCallListenerBinder = new VideoCallListenerBinder();
    this.mVideoProvider.addVideoCallback((IBinder)videoCallListenerBinder);
    this.mCallingPackageName = paramString;
    setTargetSdkVersion(paramInt);
  }
  
  public void setTargetSdkVersion(int paramInt) {
    this.mTargetSdkVersion = paramInt;
  }
  
  public void destroy() {
    unregisterCallback(this.mCallback);
    try {
      this.mVideoProvider.asBinder().unlinkToDeath(this.mDeathRecipient, 0);
    } catch (NoSuchElementException noSuchElementException) {}
  }
  
  public void registerCallback(InCallService.VideoCall.Callback paramCallback) {
    registerCallback(paramCallback, null);
  }
  
  public void registerCallback(InCallService.VideoCall.Callback paramCallback, Handler paramHandler) {
    this.mCallback = paramCallback;
    if (paramHandler == null) {
      this.mHandler = new MessageHandler(Looper.getMainLooper());
    } else {
      this.mHandler = new MessageHandler(paramHandler.getLooper());
    } 
  }
  
  public void unregisterCallback(InCallService.VideoCall.Callback paramCallback) {
    if (paramCallback != this.mCallback)
      return; 
    this.mCallback = null;
    try {
      this.mVideoProvider.removeVideoCallback((IBinder)this.mBinder);
    } catch (RemoteException remoteException) {}
  }
  
  public void setCamera(String paramString) {
    try {
      Log.w(this, "setCamera: cameraId=%s, calling=%s", new Object[] { paramString, this.mCallingPackageName });
      this.mVideoProvider.setCamera(paramString, this.mCallingPackageName, this.mTargetSdkVersion);
    } catch (RemoteException remoteException) {}
  }
  
  public void setPreviewSurface(Surface paramSurface) {
    try {
      this.mVideoProvider.setPreviewSurface(paramSurface);
    } catch (RemoteException remoteException) {}
  }
  
  public void setDisplaySurface(Surface paramSurface) {
    try {
      this.mVideoProvider.setDisplaySurface(paramSurface);
    } catch (RemoteException remoteException) {}
  }
  
  public void setDeviceOrientation(int paramInt) {
    try {
      this.mVideoProvider.setDeviceOrientation(paramInt);
    } catch (RemoteException remoteException) {}
  }
  
  public void setZoom(float paramFloat) {
    try {
      this.mVideoProvider.setZoom(paramFloat);
    } catch (RemoteException remoteException) {}
  }
  
  public void sendSessionModifyRequest(VideoProfile paramVideoProfile) {
    try {
      VideoProfile videoProfile = new VideoProfile();
      this(this.mVideoState, this.mVideoQuality);
      this.mVideoProvider.sendSessionModifyRequest(videoProfile, paramVideoProfile);
    } catch (RemoteException remoteException) {}
  }
  
  public void sendSessionModifyResponse(VideoProfile paramVideoProfile) {
    try {
      this.mVideoProvider.sendSessionModifyResponse(paramVideoProfile);
    } catch (RemoteException remoteException) {}
  }
  
  public void requestCameraCapabilities() {
    try {
      this.mVideoProvider.requestCameraCapabilities();
    } catch (RemoteException remoteException) {}
  }
  
  public void requestCallDataUsage() {
    try {
      this.mVideoProvider.requestCallDataUsage();
    } catch (RemoteException remoteException) {}
  }
  
  public void setPauseImage(Uri paramUri) {
    try {
      this.mVideoProvider.setPauseImage(paramUri);
    } catch (RemoteException remoteException) {}
  }
  
  public void setVideoState(int paramInt) {
    this.mVideoState = paramInt;
  }
  
  public IVideoProvider getVideoProvider() {
    return this.mVideoProvider;
  }
}
