package android.telecom;

import android.os.Parcel;

public class OplusBasePhoneAccountHandle {
  protected int mSubId = -1;
  
  protected int mSlotId = -1;
  
  protected void initSubAndSlotId(int paramInt1, int paramInt2) {
    this.mSubId = paramInt1;
    this.mSlotId = paramInt2;
  }
  
  public int getSubId() {
    return this.mSubId;
  }
  
  public int getSlotId() {
    return this.mSlotId;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mSubId);
    paramParcel.writeInt(this.mSlotId);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mSubId = paramParcel.readInt();
    this.mSlotId = paramParcel.readInt();
  }
}
