package android.telecom;

import android.os.Handler;

class CallbackRecord<T> {
  private final T mCallback;
  
  private final Handler mHandler;
  
  public CallbackRecord(T paramT, Handler paramHandler) {
    this.mCallback = paramT;
    this.mHandler = paramHandler;
  }
  
  public T getCallback() {
    return this.mCallback;
  }
  
  public Handler getHandler() {
    return this.mHandler;
  }
}
