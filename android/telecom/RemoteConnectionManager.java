package android.telecom;

import android.content.ComponentName;
import android.os.RemoteException;
import com.android.internal.telecom.IConnectionService;
import java.util.HashMap;
import java.util.Map;

public class RemoteConnectionManager {
  private final ConnectionService mOurConnectionServiceImpl;
  
  private final Map<ComponentName, RemoteConnectionService> mRemoteConnectionServices = new HashMap<>();
  
  public RemoteConnectionManager(ConnectionService paramConnectionService) {
    this.mOurConnectionServiceImpl = paramConnectionService;
  }
  
  void addConnectionService(ComponentName paramComponentName, IConnectionService paramIConnectionService) {
    if (!this.mRemoteConnectionServices.containsKey(paramComponentName))
      try {
        RemoteConnectionService remoteConnectionService = new RemoteConnectionService();
        this(paramIConnectionService, this.mOurConnectionServiceImpl);
        this.mRemoteConnectionServices.put(paramComponentName, remoteConnectionService);
      } catch (RemoteException remoteException) {} 
  }
  
  public RemoteConnection createRemoteConnection(PhoneAccountHandle paramPhoneAccountHandle, ConnectionRequest paramConnectionRequest, boolean paramBoolean) {
    PhoneAccountHandle phoneAccountHandle = paramConnectionRequest.getAccountHandle();
    if (phoneAccountHandle != null) {
      RemoteConnectionService remoteConnectionService;
      ComponentName componentName = paramConnectionRequest.getAccountHandle().getComponentName();
      if (this.mRemoteConnectionServices.containsKey(componentName)) {
        remoteConnectionService = this.mRemoteConnectionServices.get(componentName);
        if (remoteConnectionService != null)
          return remoteConnectionService.createRemoteConnection(paramPhoneAccountHandle, paramConnectionRequest, paramBoolean); 
        return null;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("accountHandle not supported: ");
      stringBuilder.append(remoteConnectionService);
      throw new UnsupportedOperationException(stringBuilder.toString());
    } 
    throw new IllegalArgumentException("accountHandle must be specified.");
  }
  
  public void conferenceRemoteConnections(RemoteConnection paramRemoteConnection1, RemoteConnection paramRemoteConnection2) {
    if (paramRemoteConnection1.getConnectionService() == paramRemoteConnection2.getConnectionService()) {
      try {
        paramRemoteConnection1.getConnectionService().conference(paramRemoteConnection1.getId(), paramRemoteConnection2.getId(), null);
      } catch (RemoteException remoteException) {}
    } else {
      IConnectionService iConnectionService2 = remoteException.getConnectionService();
      String str2 = remoteException.getId();
      IConnectionService iConnectionService1 = paramRemoteConnection2.getConnectionService();
      String str1 = paramRemoteConnection2.getId();
      Log.w(this, "Request to conference incompatible remote connections (%s,%s) (%s,%s)", new Object[] { iConnectionService2, str2, iConnectionService1, str1 });
    } 
  }
}
