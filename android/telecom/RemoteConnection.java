package android.telecom;

import android.annotation.SystemApi;
import android.net.Uri;
import android.os.BadParcelableException;
import android.os.Bundle;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.telecom.Logging.Session;
import android.view.Surface;
import com.android.internal.telecom.IConnectionService;
import com.android.internal.telecom.IVideoCallback;
import com.android.internal.telecom.IVideoProvider;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public final class RemoteConnection {
  private Uri mAddress;
  
  private int mAddressPresentation;
  
  public static abstract class Callback {
    public void onStateChanged(RemoteConnection param1RemoteConnection, int param1Int) {}
    
    public void onDisconnected(RemoteConnection param1RemoteConnection, DisconnectCause param1DisconnectCause) {}
    
    public void onRingbackRequested(RemoteConnection param1RemoteConnection, boolean param1Boolean) {}
    
    public void onConnectionCapabilitiesChanged(RemoteConnection param1RemoteConnection, int param1Int) {}
    
    public void onConnectionPropertiesChanged(RemoteConnection param1RemoteConnection, int param1Int) {}
    
    public void onPostDialWait(RemoteConnection param1RemoteConnection, String param1String) {}
    
    public void onPostDialChar(RemoteConnection param1RemoteConnection, char param1Char) {}
    
    public void onVoipAudioChanged(RemoteConnection param1RemoteConnection, boolean param1Boolean) {}
    
    public void onStatusHintsChanged(RemoteConnection param1RemoteConnection, StatusHints param1StatusHints) {}
    
    public void onAddressChanged(RemoteConnection param1RemoteConnection, Uri param1Uri, int param1Int) {}
    
    public void onCallerDisplayNameChanged(RemoteConnection param1RemoteConnection, String param1String, int param1Int) {}
    
    public void onVideoStateChanged(RemoteConnection param1RemoteConnection, int param1Int) {}
    
    public void onDestroyed(RemoteConnection param1RemoteConnection) {}
    
    public void onConferenceableConnectionsChanged(RemoteConnection param1RemoteConnection, List<RemoteConnection> param1List) {}
    
    public void onVideoProviderChanged(RemoteConnection param1RemoteConnection, RemoteConnection.VideoProvider param1VideoProvider) {}
    
    public void onConferenceChanged(RemoteConnection param1RemoteConnection, RemoteConference param1RemoteConference) {}
    
    public void onExtrasChanged(RemoteConnection param1RemoteConnection, Bundle param1Bundle) {}
    
    public void onConnectionEvent(RemoteConnection param1RemoteConnection, String param1String, Bundle param1Bundle) {}
    
    public void onRttInitiationSuccess(RemoteConnection param1RemoteConnection) {}
    
    public void onRttInitiationFailure(RemoteConnection param1RemoteConnection, int param1Int) {}
    
    public void onRttSessionRemotelyTerminated(RemoteConnection param1RemoteConnection) {}
    
    public void onRemoteRttRequest(RemoteConnection param1RemoteConnection) {}
  }
  
  public static class VideoProvider {
    private final Set<Callback> mCallbacks;
    
    private final String mCallingPackage;
    
    private final int mTargetSdkVersion;
    
    private final IVideoCallback mVideoCallbackDelegate;
    
    private final VideoCallbackServant mVideoCallbackServant;
    
    private final IVideoProvider mVideoProviderBinder;
    
    public static abstract class Callback {
      public void onSessionModifyRequestReceived(RemoteConnection.VideoProvider param2VideoProvider, VideoProfile param2VideoProfile) {}
      
      public void onSessionModifyResponseReceived(RemoteConnection.VideoProvider param2VideoProvider, int param2Int, VideoProfile param2VideoProfile1, VideoProfile param2VideoProfile2) {}
      
      public void onCallSessionEvent(RemoteConnection.VideoProvider param2VideoProvider, int param2Int) {}
      
      public void onPeerDimensionsChanged(RemoteConnection.VideoProvider param2VideoProvider, int param2Int1, int param2Int2) {}
      
      public void onCallDataUsageChanged(RemoteConnection.VideoProvider param2VideoProvider, long param2Long) {}
      
      public void onCameraCapabilitiesChanged(RemoteConnection.VideoProvider param2VideoProvider, VideoProfile.CameraCapabilities param2CameraCapabilities) {}
      
      public void onVideoQualityChanged(RemoteConnection.VideoProvider param2VideoProvider, int param2Int) {}
    }
    
    VideoProvider(IVideoProvider param1IVideoProvider, String param1String, int param1Int) {
      Object object = new Object(this);
      this.mVideoCallbackServant = new VideoCallbackServant((IVideoCallback)object);
      this.mCallbacks = Collections.newSetFromMap(new ConcurrentHashMap<>(8, 0.9F, 1));
      this.mVideoProviderBinder = param1IVideoProvider;
      this.mCallingPackage = param1String;
      this.mTargetSdkVersion = param1Int;
      try {
        param1IVideoProvider.addVideoCallback(this.mVideoCallbackServant.getStub().asBinder());
      } catch (RemoteException remoteException) {}
    }
    
    public void registerCallback(Callback param1Callback) {
      this.mCallbacks.add(param1Callback);
    }
    
    public void unregisterCallback(Callback param1Callback) {
      this.mCallbacks.remove(param1Callback);
    }
    
    public void setCamera(String param1String) {
      try {
        this.mVideoProviderBinder.setCamera(param1String, this.mCallingPackage, this.mTargetSdkVersion);
      } catch (RemoteException remoteException) {}
    }
    
    public void setPreviewSurface(Surface param1Surface) {
      try {
        this.mVideoProviderBinder.setPreviewSurface(param1Surface);
      } catch (RemoteException remoteException) {}
    }
    
    public void setDisplaySurface(Surface param1Surface) {
      try {
        this.mVideoProviderBinder.setDisplaySurface(param1Surface);
      } catch (RemoteException remoteException) {}
    }
    
    public void setDeviceOrientation(int param1Int) {
      try {
        this.mVideoProviderBinder.setDeviceOrientation(param1Int);
      } catch (RemoteException remoteException) {}
    }
    
    public void setZoom(float param1Float) {
      try {
        this.mVideoProviderBinder.setZoom(param1Float);
      } catch (RemoteException remoteException) {}
    }
    
    public void sendSessionModifyRequest(VideoProfile param1VideoProfile1, VideoProfile param1VideoProfile2) {
      try {
        this.mVideoProviderBinder.sendSessionModifyRequest(param1VideoProfile1, param1VideoProfile2);
      } catch (RemoteException remoteException) {}
    }
    
    public void sendSessionModifyResponse(VideoProfile param1VideoProfile) {
      try {
        this.mVideoProviderBinder.sendSessionModifyResponse(param1VideoProfile);
      } catch (RemoteException remoteException) {}
    }
    
    public void requestCameraCapabilities() {
      try {
        this.mVideoProviderBinder.requestCameraCapabilities();
      } catch (RemoteException remoteException) {}
    }
    
    public void requestCallDataUsage() {
      try {
        this.mVideoProviderBinder.requestCallDataUsage();
      } catch (RemoteException remoteException) {}
    }
    
    public void setPauseImage(Uri param1Uri) {
      try {
        this.mVideoProviderBinder.setPauseImage(param1Uri);
      } catch (RemoteException remoteException) {}
    }
  }
  
  public static abstract class Callback {
    public void onSessionModifyRequestReceived(RemoteConnection.VideoProvider param1VideoProvider, VideoProfile param1VideoProfile) {}
    
    public void onSessionModifyResponseReceived(RemoteConnection.VideoProvider param1VideoProvider, int param1Int, VideoProfile param1VideoProfile1, VideoProfile param1VideoProfile2) {}
    
    public void onCallSessionEvent(RemoteConnection.VideoProvider param1VideoProvider, int param1Int) {}
    
    public void onPeerDimensionsChanged(RemoteConnection.VideoProvider param1VideoProvider, int param1Int1, int param1Int2) {}
    
    public void onCallDataUsageChanged(RemoteConnection.VideoProvider param1VideoProvider, long param1Long) {}
    
    public void onCameraCapabilitiesChanged(RemoteConnection.VideoProvider param1VideoProvider, VideoProfile.CameraCapabilities param1CameraCapabilities) {}
    
    public void onVideoQualityChanged(RemoteConnection.VideoProvider param1VideoProvider, int param1Int) {}
  }
  
  private final Set<CallbackRecord> mCallbackRecords = Collections.newSetFromMap(new ConcurrentHashMap<>(8, 0.9F, 1));
  
  private String mCallerDisplayName;
  
  private int mCallerDisplayNamePresentation;
  
  private String mCallingPackageAbbreviation;
  
  private RemoteConference mConference;
  
  private final List<RemoteConnection> mConferenceableConnections;
  
  private boolean mConnected;
  
  private int mConnectionCapabilities;
  
  private final String mConnectionId;
  
  private int mConnectionProperties;
  
  private IConnectionService mConnectionService;
  
  private DisconnectCause mDisconnectCause;
  
  private Bundle mExtras;
  
  private boolean mIsVoipAudioMode;
  
  private boolean mRingbackRequested;
  
  private int mState;
  
  private StatusHints mStatusHints;
  
  private final List<RemoteConnection> mUnmodifiableconferenceableConnections;
  
  private VideoProvider mVideoProvider;
  
  private int mVideoState;
  
  RemoteConnection(DisconnectCause paramDisconnectCause) {
    ArrayList<RemoteConnection> arrayList = new ArrayList();
    this.mUnmodifiableconferenceableConnections = Collections.unmodifiableList(arrayList);
    this.mState = 1;
    this.mConnectionId = "NULL";
    this.mConnected = false;
    this.mState = 6;
    this.mDisconnectCause = paramDisconnectCause;
  }
  
  RemoteConnection(String paramString, IConnectionService paramIConnectionService, ConnectionRequest paramConnectionRequest) {
    ArrayList<RemoteConnection> arrayList = new ArrayList();
    this.mUnmodifiableconferenceableConnections = Collections.unmodifiableList(arrayList);
    this.mState = 1;
    this.mConnectionId = paramString;
    this.mConnectionService = paramIConnectionService;
    this.mConnected = true;
    this.mState = 0;
    if (paramConnectionRequest != null && paramConnectionRequest.getExtras() != null && paramConnectionRequest.getExtras().containsKey("android.telecom.extra.REMOTE_CONNECTION_ORIGINATING_PACKAGE_NAME")) {
      paramString = paramConnectionRequest.getExtras().getString("android.telecom.extra.REMOTE_CONNECTION_ORIGINATING_PACKAGE_NAME");
      this.mCallingPackageAbbreviation = Log.getPackageAbbreviation(paramString);
    } 
  }
  
  RemoteConnection(String paramString1, IConnectionService paramIConnectionService, ParcelableConnection paramParcelableConnection, String paramString2, int paramInt) {
    ArrayList<RemoteConnection> arrayList = new ArrayList();
    this.mUnmodifiableconferenceableConnections = Collections.unmodifiableList(arrayList);
    this.mState = 1;
    this.mConnectionId = paramString1;
    this.mConnectionService = paramIConnectionService;
    this.mConnected = true;
    this.mState = paramParcelableConnection.getState();
    this.mDisconnectCause = paramParcelableConnection.getDisconnectCause();
    this.mRingbackRequested = paramParcelableConnection.isRingbackRequested();
    this.mConnectionCapabilities = paramParcelableConnection.getConnectionCapabilities();
    this.mConnectionProperties = paramParcelableConnection.getConnectionProperties();
    this.mVideoState = paramParcelableConnection.getVideoState();
    IVideoProvider iVideoProvider = paramParcelableConnection.getVideoProvider();
    if (iVideoProvider != null) {
      this.mVideoProvider = new VideoProvider(iVideoProvider, paramString2, paramInt);
    } else {
      this.mVideoProvider = null;
    } 
    this.mIsVoipAudioMode = paramParcelableConnection.getIsVoipAudioMode();
    this.mStatusHints = paramParcelableConnection.getStatusHints();
    this.mAddress = paramParcelableConnection.getHandle();
    this.mAddressPresentation = paramParcelableConnection.getHandlePresentation();
    this.mCallerDisplayName = paramParcelableConnection.getCallerDisplayName();
    this.mCallerDisplayNamePresentation = paramParcelableConnection.getCallerDisplayNamePresentation();
    this.mConference = null;
    putExtras(paramParcelableConnection.getExtras());
    Bundle bundle = new Bundle();
    bundle.putString("android.telecom.extra.ORIGINAL_CONNECTION_ID", paramString1);
    putExtras(bundle);
    this.mCallingPackageAbbreviation = Log.getPackageAbbreviation(paramString2);
  }
  
  public void registerCallback(Callback paramCallback) {
    registerCallback(paramCallback, new Handler());
  }
  
  public void registerCallback(Callback paramCallback, Handler paramHandler) {
    unregisterCallback(paramCallback);
    if (paramCallback != null && paramHandler != null)
      this.mCallbackRecords.add(new CallbackRecord(paramCallback, paramHandler)); 
  }
  
  public void unregisterCallback(Callback paramCallback) {
    if (paramCallback != null)
      for (CallbackRecord callbackRecord : this.mCallbackRecords) {
        if (callbackRecord.getCallback() == paramCallback) {
          this.mCallbackRecords.remove(callbackRecord);
          break;
        } 
      }  
  }
  
  public int getState() {
    return this.mState;
  }
  
  public DisconnectCause getDisconnectCause() {
    return this.mDisconnectCause;
  }
  
  public int getConnectionCapabilities() {
    return this.mConnectionCapabilities;
  }
  
  public int getConnectionProperties() {
    return this.mConnectionProperties;
  }
  
  public boolean isVoipAudioMode() {
    return this.mIsVoipAudioMode;
  }
  
  public StatusHints getStatusHints() {
    return this.mStatusHints;
  }
  
  public Uri getAddress() {
    return this.mAddress;
  }
  
  public int getAddressPresentation() {
    return this.mAddressPresentation;
  }
  
  public CharSequence getCallerDisplayName() {
    return this.mCallerDisplayName;
  }
  
  public int getCallerDisplayNamePresentation() {
    return this.mCallerDisplayNamePresentation;
  }
  
  public int getVideoState() {
    return this.mVideoState;
  }
  
  public final VideoProvider getVideoProvider() {
    return this.mVideoProvider;
  }
  
  public final Bundle getExtras() {
    return this.mExtras;
  }
  
  public boolean isRingbackRequested() {
    return this.mRingbackRequested;
  }
  
  public void abort() {
    Log.startSession("RC.a", getActiveOwnerInfo());
    try {
      if (this.mConnected)
        this.mConnectionService.abort(this.mConnectionId, Log.getExternalSession(this.mCallingPackageAbbreviation)); 
    } catch (RemoteException remoteException) {
    
    } finally {
      Log.endSession();
    } 
  }
  
  public void answer() {
    Log.startSession("RC.an", getActiveOwnerInfo());
    try {
      if (this.mConnected)
        this.mConnectionService.answer(this.mConnectionId, Log.getExternalSession(this.mCallingPackageAbbreviation)); 
    } catch (RemoteException remoteException) {
    
    } finally {
      Log.endSession();
    } 
  }
  
  public void answer(int paramInt) {
    Log.startSession("RC.an2", getActiveOwnerInfo());
    try {
      if (this.mConnected) {
        IConnectionService iConnectionService = this.mConnectionService;
        String str1 = this.mConnectionId, str2 = this.mCallingPackageAbbreviation;
        Session.Info info = Log.getExternalSession(str2);
        iConnectionService.answerVideo(str1, paramInt, info);
      } 
    } catch (RemoteException remoteException) {
    
    } finally {
      Log.endSession();
    } 
  }
  
  public void reject() {
    Log.startSession("RC.r", getActiveOwnerInfo());
    try {
      if (this.mConnected)
        this.mConnectionService.reject(this.mConnectionId, Log.getExternalSession(this.mCallingPackageAbbreviation)); 
    } catch (RemoteException remoteException) {
    
    } finally {
      Log.endSession();
    } 
  }
  
  public void hold() {
    Log.startSession("RC.h", getActiveOwnerInfo());
    try {
      if (this.mConnected)
        this.mConnectionService.hold(this.mConnectionId, Log.getExternalSession(this.mCallingPackageAbbreviation)); 
    } catch (RemoteException remoteException) {
    
    } finally {
      Log.endSession();
    } 
  }
  
  public void unhold() {
    Log.startSession("RC.u", getActiveOwnerInfo());
    try {
      if (this.mConnected)
        this.mConnectionService.unhold(this.mConnectionId, Log.getExternalSession(this.mCallingPackageAbbreviation)); 
    } catch (RemoteException remoteException) {
    
    } finally {
      Log.endSession();
    } 
  }
  
  public void disconnect() {
    Log.startSession("RC.d", getActiveOwnerInfo());
    try {
      if (this.mConnected)
        this.mConnectionService.disconnect(this.mConnectionId, Log.getExternalSession(this.mCallingPackageAbbreviation)); 
    } catch (RemoteException remoteException) {
    
    } finally {
      Log.endSession();
    } 
  }
  
  public void playDtmfTone(char paramChar) {
    Log.startSession("RC.pDT", getActiveOwnerInfo());
    try {
      if (this.mConnected)
        this.mConnectionService.playDtmfTone(this.mConnectionId, paramChar, null); 
    } catch (RemoteException remoteException) {
    
    } finally {
      Log.endSession();
    } 
  }
  
  public void stopDtmfTone() {
    Log.startSession("RC.sDT", getActiveOwnerInfo());
    try {
      if (this.mConnected)
        this.mConnectionService.stopDtmfTone(this.mConnectionId, null); 
    } catch (RemoteException remoteException) {
    
    } finally {
      Log.endSession();
    } 
  }
  
  public void postDialContinue(boolean paramBoolean) {
    Log.startSession("RC.pDC", getActiveOwnerInfo());
    try {
      if (this.mConnected)
        this.mConnectionService.onPostDialContinue(this.mConnectionId, paramBoolean, null); 
    } catch (RemoteException remoteException) {
    
    } finally {
      Log.endSession();
    } 
  }
  
  public void pullExternalCall() {
    Log.startSession("RC.pEC", getActiveOwnerInfo());
    try {
      if (this.mConnected)
        this.mConnectionService.pullExternalCall(this.mConnectionId, null); 
    } catch (RemoteException remoteException) {
    
    } finally {
      Log.endSession();
    } 
  }
  
  @SystemApi
  @Deprecated
  public void setAudioState(AudioState paramAudioState) {
    setCallAudioState(new CallAudioState(paramAudioState));
  }
  
  public void setCallAudioState(CallAudioState paramCallAudioState) {
    Log.startSession("RC.sCAS", getActiveOwnerInfo());
    try {
      if (this.mConnected)
        this.mConnectionService.onCallAudioStateChanged(this.mConnectionId, paramCallAudioState, null); 
    } catch (RemoteException remoteException) {
    
    } finally {
      Log.endSession();
    } 
  }
  
  public void startRtt(Connection.RttTextStream paramRttTextStream) {
    Log.startSession("RC.sR", getActiveOwnerInfo());
    try {
      if (this.mConnected) {
        IConnectionService iConnectionService = this.mConnectionService;
        String str = this.mConnectionId;
        ParcelFileDescriptor parcelFileDescriptor2 = paramRttTextStream.getFdFromInCall();
        ParcelFileDescriptor parcelFileDescriptor1 = paramRttTextStream.getFdToInCall();
        iConnectionService.startRtt(str, parcelFileDescriptor2, parcelFileDescriptor1, null);
      } 
    } catch (RemoteException remoteException) {
    
    } finally {
      Log.endSession();
    } 
  }
  
  public void stopRtt() {
    Log.startSession("RC.stR", getActiveOwnerInfo());
    try {
      if (this.mConnected)
        this.mConnectionService.stopRtt(this.mConnectionId, null); 
    } catch (RemoteException remoteException) {
    
    } finally {
      Log.endSession();
    } 
  }
  
  public void sendRttUpgradeResponse(Connection.RttTextStream paramRttTextStream) {
    Log.startSession("RC.sRUR", getActiveOwnerInfo());
    try {
      if (this.mConnected)
        if (paramRttTextStream == null) {
          this.mConnectionService.respondToRttUpgradeRequest(this.mConnectionId, null, null, null);
        } else {
          IConnectionService iConnectionService = this.mConnectionService;
          String str = this.mConnectionId;
          ParcelFileDescriptor parcelFileDescriptor2 = paramRttTextStream.getFdFromInCall(), parcelFileDescriptor1 = paramRttTextStream.getFdToInCall();
          iConnectionService.respondToRttUpgradeRequest(str, parcelFileDescriptor2, parcelFileDescriptor1, null);
        }  
    } catch (RemoteException remoteException) {
    
    } finally {
      Log.endSession();
    } 
  }
  
  public List<RemoteConnection> getConferenceableConnections() {
    return this.mUnmodifiableconferenceableConnections;
  }
  
  public RemoteConference getConference() {
    return this.mConference;
  }
  
  private String getActiveOwnerInfo() {
    Session.Info info = Log.getExternalSession();
    if (info == null)
      return null; 
    return info.ownerInfo;
  }
  
  String getId() {
    return this.mConnectionId;
  }
  
  IConnectionService getConnectionService() {
    return this.mConnectionService;
  }
  
  void setState(final int state) {
    if (this.mState != state) {
      this.mState = state;
      for (CallbackRecord callbackRecord : this.mCallbackRecords) {
        final Callback callback = callbackRecord.getCallback();
        callbackRecord.getHandler().post(new Runnable() {
              final RemoteConnection this$0;
              
              final RemoteConnection.Callback val$callback;
              
              final RemoteConnection val$connection;
              
              final int val$state;
              
              public void run() {
                callback.onStateChanged(connection, state);
              }
            });
      } 
    } 
  }
  
  void setDisconnected(final DisconnectCause disconnectCause) {
    if (this.mState != 6) {
      this.mState = 6;
      this.mDisconnectCause = disconnectCause;
      for (CallbackRecord callbackRecord : this.mCallbackRecords) {
        final Callback callback = callbackRecord.getCallback();
        callbackRecord.getHandler().post(new Runnable() {
              final RemoteConnection this$0;
              
              final RemoteConnection.Callback val$callback;
              
              final RemoteConnection val$connection;
              
              final DisconnectCause val$disconnectCause;
              
              public void run() {
                callback.onDisconnected(connection, disconnectCause);
              }
            });
      } 
    } 
  }
  
  void setRingbackRequested(final boolean ringback) {
    if (this.mRingbackRequested != ringback) {
      this.mRingbackRequested = ringback;
      for (CallbackRecord callbackRecord : this.mCallbackRecords) {
        final Callback callback = callbackRecord.getCallback();
        callbackRecord.getHandler().post(new Runnable() {
              final RemoteConnection this$0;
              
              final RemoteConnection.Callback val$callback;
              
              final RemoteConnection val$connection;
              
              final boolean val$ringback;
              
              public void run() {
                callback.onRingbackRequested(connection, ringback);
              }
            });
      } 
    } 
  }
  
  void setConnectionCapabilities(final int connectionCapabilities) {
    this.mConnectionCapabilities = connectionCapabilities;
    for (CallbackRecord callbackRecord : this.mCallbackRecords) {
      final Callback callback = callbackRecord.getCallback();
      callbackRecord.getHandler().post(new Runnable() {
            final RemoteConnection this$0;
            
            final RemoteConnection.Callback val$callback;
            
            final RemoteConnection val$connection;
            
            final int val$connectionCapabilities;
            
            public void run() {
              callback.onConnectionCapabilitiesChanged(connection, connectionCapabilities);
            }
          });
    } 
  }
  
  void setConnectionProperties(final int connectionProperties) {
    this.mConnectionProperties = connectionProperties;
    for (CallbackRecord callbackRecord : this.mCallbackRecords) {
      final Callback callback = callbackRecord.getCallback();
      callbackRecord.getHandler().post(new Runnable() {
            final RemoteConnection this$0;
            
            final RemoteConnection.Callback val$callback;
            
            final RemoteConnection val$connection;
            
            final int val$connectionProperties;
            
            public void run() {
              callback.onConnectionPropertiesChanged(connection, connectionProperties);
            }
          });
    } 
  }
  
  void setDestroyed() {
    if (!this.mCallbackRecords.isEmpty()) {
      if (this.mState != 6)
        setDisconnected(new DisconnectCause(1, "Connection destroyed.")); 
      for (CallbackRecord callbackRecord : this.mCallbackRecords) {
        final Callback callback = callbackRecord.getCallback();
        callbackRecord.getHandler().post(new Runnable() {
              final RemoteConnection this$0;
              
              final RemoteConnection.Callback val$callback;
              
              final RemoteConnection val$connection;
              
              public void run() {
                callback.onDestroyed(connection);
              }
            });
      } 
      this.mCallbackRecords.clear();
      this.mConnected = false;
    } 
  }
  
  void setPostDialWait(final String remainingDigits) {
    for (CallbackRecord callbackRecord : this.mCallbackRecords) {
      final Callback callback = callbackRecord.getCallback();
      callbackRecord.getHandler().post(new Runnable() {
            final RemoteConnection this$0;
            
            final RemoteConnection.Callback val$callback;
            
            final RemoteConnection val$connection;
            
            final String val$remainingDigits;
            
            public void run() {
              callback.onPostDialWait(connection, remainingDigits);
            }
          });
    } 
  }
  
  void onPostDialChar(final char nextChar) {
    for (CallbackRecord callbackRecord : this.mCallbackRecords) {
      final Callback callback = callbackRecord.getCallback();
      callbackRecord.getHandler().post(new Runnable() {
            final RemoteConnection this$0;
            
            final RemoteConnection.Callback val$callback;
            
            final RemoteConnection val$connection;
            
            final char val$nextChar;
            
            public void run() {
              callback.onPostDialChar(connection, nextChar);
            }
          });
    } 
  }
  
  void setVideoState(final int videoState) {
    this.mVideoState = videoState;
    for (CallbackRecord callbackRecord : this.mCallbackRecords) {
      final Callback callback = callbackRecord.getCallback();
      callbackRecord.getHandler().post(new Runnable() {
            final RemoteConnection this$0;
            
            final RemoteConnection.Callback val$callback;
            
            final RemoteConnection val$connection;
            
            final int val$videoState;
            
            public void run() {
              callback.onVideoStateChanged(connection, videoState);
            }
          });
    } 
  }
  
  void setVideoProvider(final VideoProvider videoProvider) {
    this.mVideoProvider = videoProvider;
    for (CallbackRecord callbackRecord : this.mCallbackRecords) {
      final Callback callback = callbackRecord.getCallback();
      callbackRecord.getHandler().post(new Runnable() {
            final RemoteConnection this$0;
            
            final RemoteConnection.Callback val$callback;
            
            final RemoteConnection val$connection;
            
            final RemoteConnection.VideoProvider val$videoProvider;
            
            public void run() {
              callback.onVideoProviderChanged(connection, videoProvider);
            }
          });
    } 
  }
  
  void setIsVoipAudioMode(final boolean isVoip) {
    this.mIsVoipAudioMode = isVoip;
    for (CallbackRecord callbackRecord : this.mCallbackRecords) {
      final Callback callback = callbackRecord.getCallback();
      callbackRecord.getHandler().post(new Runnable() {
            final RemoteConnection this$0;
            
            final RemoteConnection.Callback val$callback;
            
            final RemoteConnection val$connection;
            
            final boolean val$isVoip;
            
            public void run() {
              callback.onVoipAudioChanged(connection, isVoip);
            }
          });
    } 
  }
  
  void setStatusHints(final StatusHints statusHints) {
    this.mStatusHints = statusHints;
    for (CallbackRecord callbackRecord : this.mCallbackRecords) {
      final Callback callback = callbackRecord.getCallback();
      callbackRecord.getHandler().post(new Runnable() {
            final RemoteConnection this$0;
            
            final RemoteConnection.Callback val$callback;
            
            final RemoteConnection val$connection;
            
            final StatusHints val$statusHints;
            
            public void run() {
              callback.onStatusHintsChanged(connection, statusHints);
            }
          });
    } 
  }
  
  void setAddress(final Uri address, final int presentation) {
    this.mAddress = address;
    this.mAddressPresentation = presentation;
    for (CallbackRecord callbackRecord : this.mCallbackRecords) {
      final Callback callback = callbackRecord.getCallback();
      callbackRecord.getHandler().post(new Runnable() {
            final RemoteConnection this$0;
            
            final Uri val$address;
            
            final RemoteConnection.Callback val$callback;
            
            final RemoteConnection val$connection;
            
            final int val$presentation;
            
            public void run() {
              callback.onAddressChanged(connection, address, presentation);
            }
          });
    } 
  }
  
  void setCallerDisplayName(final String callerDisplayName, final int presentation) {
    this.mCallerDisplayName = callerDisplayName;
    this.mCallerDisplayNamePresentation = presentation;
    for (CallbackRecord callbackRecord : this.mCallbackRecords) {
      final Callback callback = callbackRecord.getCallback();
      callbackRecord.getHandler().post(new Runnable() {
            final RemoteConnection this$0;
            
            final RemoteConnection.Callback val$callback;
            
            final String val$callerDisplayName;
            
            final RemoteConnection val$connection;
            
            final int val$presentation;
            
            public void run() {
              callback.onCallerDisplayNameChanged(connection, callerDisplayName, presentation);
            }
          });
    } 
  }
  
  void setConferenceableConnections(List<RemoteConnection> paramList) {
    this.mConferenceableConnections.clear();
    this.mConferenceableConnections.addAll(paramList);
    for (CallbackRecord callbackRecord : this.mCallbackRecords) {
      final Callback callback = callbackRecord.getCallback();
      callbackRecord.getHandler().post(new Runnable() {
            final RemoteConnection this$0;
            
            final RemoteConnection.Callback val$callback;
            
            final RemoteConnection val$connection;
            
            public void run() {
              RemoteConnection.Callback callback = callback;
              RemoteConnection remoteConnection1 = connection, remoteConnection2 = RemoteConnection.this;
              List<RemoteConnection> list = remoteConnection2.mUnmodifiableconferenceableConnections;
              callback.onConferenceableConnectionsChanged(remoteConnection1, list);
            }
          });
    } 
  }
  
  void setConference(final RemoteConference conference) {
    if (this.mConference != conference) {
      this.mConference = conference;
      for (CallbackRecord callbackRecord : this.mCallbackRecords) {
        final Callback callback = callbackRecord.getCallback();
        callbackRecord.getHandler().post(new Runnable() {
              final RemoteConnection this$0;
              
              final RemoteConnection.Callback val$callback;
              
              final RemoteConference val$conference;
              
              final RemoteConnection val$connection;
              
              public void run() {
                callback.onConferenceChanged(connection, conference);
              }
            });
      } 
    } 
  }
  
  void putExtras(Bundle paramBundle) {
    if (paramBundle == null)
      return; 
    if (this.mExtras == null)
      this.mExtras = new Bundle(); 
    try {
      this.mExtras.putAll(paramBundle);
    } catch (BadParcelableException badParcelableException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("putExtras: could not unmarshal extras; exception = ");
      stringBuilder.append(badParcelableException);
      Log.w(this, stringBuilder.toString(), new Object[0]);
    } 
    notifyExtrasChanged();
  }
  
  void removeExtras(List<String> paramList) {
    if (this.mExtras == null || paramList == null || paramList.isEmpty())
      return; 
    for (String str : paramList)
      this.mExtras.remove(str); 
    notifyExtrasChanged();
  }
  
  private void notifyExtrasChanged() {
    for (CallbackRecord callbackRecord : this.mCallbackRecords) {
      final Callback callback = callbackRecord.getCallback();
      callbackRecord.getHandler().post(new Runnable() {
            final RemoteConnection this$0;
            
            final RemoteConnection.Callback val$callback;
            
            final RemoteConnection val$connection;
            
            public void run() {
              callback.onExtrasChanged(connection, RemoteConnection.this.mExtras);
            }
          });
    } 
  }
  
  void onConnectionEvent(final String event, final Bundle extras) {
    for (CallbackRecord callbackRecord : this.mCallbackRecords) {
      final Callback callback = callbackRecord.getCallback();
      callbackRecord.getHandler().post(new Runnable() {
            final RemoteConnection this$0;
            
            final RemoteConnection.Callback val$callback;
            
            final RemoteConnection val$connection;
            
            final String val$event;
            
            final Bundle val$extras;
            
            public void run() {
              callback.onConnectionEvent(connection, event, extras);
            }
          });
    } 
  }
  
  void onRttInitiationSuccess() {
    for (CallbackRecord callbackRecord : this.mCallbackRecords) {
      Callback callback = callbackRecord.getCallback();
      callbackRecord.getHandler().post(new _$$Lambda$RemoteConnection$C4t0J6QK31Ef1UFsdPVwkew1VaQ(callback, this));
    } 
  }
  
  void onRttInitiationFailure(int paramInt) {
    for (CallbackRecord callbackRecord : this.mCallbackRecords) {
      Callback callback = callbackRecord.getCallback();
      callbackRecord.getHandler().post(new _$$Lambda$RemoteConnection$AwagQDJDcNDplrFif6DlYZldL5E(callback, this, paramInt));
    } 
  }
  
  void onRttSessionRemotelyTerminated() {
    for (CallbackRecord callbackRecord : this.mCallbackRecords) {
      Callback callback = callbackRecord.getCallback();
      callbackRecord.getHandler().post(new _$$Lambda$RemoteConnection$mmHouQhUco_u9PRJ9qkMqlkKzAs(callback, this));
    } 
  }
  
  void onRemoteRttRequest() {
    for (CallbackRecord callbackRecord : this.mCallbackRecords) {
      Callback callback = callbackRecord.getCallback();
      callbackRecord.getHandler().post(new _$$Lambda$RemoteConnection$yp1cNJ53RzQGFz3RZRlC3urzQv4(callback, this));
    } 
  }
  
  public static RemoteConnection failure(DisconnectCause paramDisconnectCause) {
    return new RemoteConnection(paramDisconnectCause);
  }
  
  class CallbackRecord extends Callback {
    private final RemoteConnection.Callback mCallback;
    
    private final Handler mHandler;
    
    public CallbackRecord(RemoteConnection this$0, Handler param1Handler) {
      this.mCallback = (RemoteConnection.Callback)this$0;
      this.mHandler = param1Handler;
    }
    
    public RemoteConnection.Callback getCallback() {
      return this.mCallback;
    }
    
    public Handler getHandler() {
      return this.mHandler;
    }
  }
}
