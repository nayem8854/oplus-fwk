package android.telecom;

import android.annotation.SystemApi;
import android.content.ComponentName;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;

public final class StatusHints implements Parcelable {
  @SystemApi
  @Deprecated
  public StatusHints(ComponentName paramComponentName, CharSequence paramCharSequence, int paramInt, Bundle paramBundle) {
    this(paramCharSequence, icon, paramBundle);
  }
  
  public StatusHints(CharSequence paramCharSequence, Icon paramIcon, Bundle paramBundle) {
    this.mLabel = paramCharSequence;
    this.mIcon = paramIcon;
    this.mExtras = paramBundle;
  }
  
  @SystemApi
  @Deprecated
  public ComponentName getPackageName() {
    return new ComponentName("", "");
  }
  
  public CharSequence getLabel() {
    return this.mLabel;
  }
  
  @SystemApi
  @Deprecated
  public int getIconResId() {
    return 0;
  }
  
  @SystemApi
  @Deprecated
  public Drawable getIcon(Context paramContext) {
    return this.mIcon.loadDrawable(paramContext);
  }
  
  public Icon getIcon() {
    return this.mIcon;
  }
  
  public Bundle getExtras() {
    return this.mExtras;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeCharSequence(this.mLabel);
    paramParcel.writeParcelable((Parcelable)this.mIcon, 0);
    paramParcel.writeParcelable((Parcelable)this.mExtras, 0);
  }
  
  public static final Parcelable.Creator<StatusHints> CREATOR = new Parcelable.Creator<StatusHints>() {
      public StatusHints createFromParcel(Parcel param1Parcel) {
        return new StatusHints(param1Parcel);
      }
      
      public StatusHints[] newArray(int param1Int) {
        return new StatusHints[param1Int];
      }
    };
  
  private final Bundle mExtras;
  
  private final Icon mIcon;
  
  private final CharSequence mLabel;
  
  private StatusHints(Parcel paramParcel) {
    this.mLabel = paramParcel.readCharSequence();
    this.mIcon = (Icon)paramParcel.readParcelable(getClass().getClassLoader());
    this.mExtras = (Bundle)paramParcel.readParcelable(getClass().getClassLoader());
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = false;
    if (paramObject != null && paramObject instanceof StatusHints) {
      paramObject = paramObject;
      if (Objects.equals(paramObject.getLabel(), getLabel()) && 
        Objects.equals(paramObject.getIcon(), getIcon()) && 
        Objects.equals(paramObject.getExtras(), getExtras()))
        bool = true; 
      return bool;
    } 
    return false;
  }
  
  public int hashCode() {
    return Objects.hashCode(this.mLabel) + Objects.hashCode(this.mIcon) + Objects.hashCode(this.mExtras);
  }
}
