package android.telecom.Logging;

import android.os.Parcel;
import android.os.Parcelable;
import android.telecom.Log;
import android.util.Slog;
import java.util.ArrayList;

public class Session {
  class Info implements Parcelable {
    private Info(Session this$0, String param1String1, String param1String2) {
      this.sessionId = (String)this$0;
      this.methodPath = param1String1;
      this.ownerInfo = param1String2;
    }
    
    public static Info getInfo(Session param1Session) {
      boolean bool;
      String str1 = param1Session.getFullSessionId();
      if (!Log.DEBUG && 
        param1Session.isSessionExternal()) {
        bool = true;
      } else {
        bool = false;
      } 
      String str2 = param1Session.getFullMethodPath(bool);
      return new Info(str2, param1Session.getOwnerInfo());
    }
    
    public static Info getExternalInfo(Session param1Session, String param1String) {
      boolean bool;
      if (param1String != null && param1Session.getOwnerInfo() != null) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(param1Session.getOwnerInfo());
        stringBuilder.append("/");
        stringBuilder.append(param1String);
        param1String = stringBuilder.toString();
      } else if (param1String == null) {
        param1String = param1Session.getOwnerInfo();
      } 
      String str = param1Session.getFullSessionId();
      if (!Log.DEBUG && 
        param1Session.isSessionExternal()) {
        bool = true;
      } else {
        bool = false;
      } 
      return new Info(param1Session.getFullMethodPath(bool), param1String);
    }
    
    public static final Parcelable.Creator<Info> CREATOR = new Parcelable.Creator<Info>() {
        public Session.Info createFromParcel(Parcel param2Parcel) {
          String str2 = param2Parcel.readString();
          String str3 = param2Parcel.readString();
          String str1 = param2Parcel.readString();
          return new Session.Info(str3, str1);
        }
        
        public Session.Info[] newArray(int param2Int) {
          return new Session.Info[param2Int];
        }
      };
    
    public final String methodPath;
    
    public final String ownerInfo;
    
    public final String sessionId;
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeString(this.sessionId);
      param1Parcel.writeString(this.methodPath);
      param1Parcel.writeString(this.ownerInfo);
    }
  }
  
  private long mExecutionEndTimeMs = -1L;
  
  private boolean mIsCompleted = false;
  
  private boolean mIsExternal = false;
  
  private int mChildCounter = 0;
  
  private boolean mIsStartedFromActiveSession = false;
  
  public static final String CONTINUE_SUBSESSION = "CONTINUE_SUBSESSION";
  
  public static final String CREATE_SUBSESSION = "CREATE_SUBSESSION";
  
  public static final String END_SESSION = "END_SESSION";
  
  public static final String END_SUBSESSION = "END_SUBSESSION";
  
  public static final String EXTERNAL_INDICATOR = "E-";
  
  public static final String LOG_TAG = "Session";
  
  private static final int SESSION_RECURSION_LIMIT = 25;
  
  public static final String SESSION_SEPARATION_CHAR_CHILD = "_";
  
  public static final String START_EXTERNAL_SESSION = "START_EXTERNAL_SESSION";
  
  public static final String START_SESSION = "START_SESSION";
  
  public static final String SUBSESSION_SEPARATION_CHAR = "->";
  
  public static final String TRUNCATE_STRING = "...";
  
  public static final int UNDEFINED = -1;
  
  private ArrayList<Session> mChildSessions;
  
  private long mExecutionStartTimeMs;
  
  private String mFullMethodPathCache;
  
  private String mOwnerInfo;
  
  private Session mParentSession;
  
  private String mSessionId;
  
  private String mShortMethodName;
  
  public Session(String paramString1, String paramString2, long paramLong, boolean paramBoolean, String paramString3) {
    setSessionId(paramString1);
    setShortMethodName(paramString2);
    this.mExecutionStartTimeMs = paramLong;
    this.mParentSession = null;
    this.mChildSessions = new ArrayList<>(5);
    this.mIsStartedFromActiveSession = paramBoolean;
    this.mOwnerInfo = paramString3;
  }
  
  public void setSessionId(String paramString) {
    if (paramString == null)
      this.mSessionId = "?"; 
    this.mSessionId = paramString;
  }
  
  public String getShortMethodName() {
    return this.mShortMethodName;
  }
  
  public void setShortMethodName(String paramString) {
    String str = paramString;
    if (paramString == null)
      str = ""; 
    this.mShortMethodName = str;
  }
  
  public void setIsExternal(boolean paramBoolean) {
    this.mIsExternal = paramBoolean;
  }
  
  public boolean isExternal() {
    return this.mIsExternal;
  }
  
  public void setParentSession(Session paramSession) {
    this.mParentSession = paramSession;
  }
  
  public void addChild(Session paramSession) {
    if (paramSession != null)
      this.mChildSessions.add(paramSession); 
  }
  
  public void removeChild(Session paramSession) {
    if (paramSession != null)
      this.mChildSessions.remove(paramSession); 
  }
  
  public long getExecutionStartTimeMilliseconds() {
    return this.mExecutionStartTimeMs;
  }
  
  public void setExecutionStartTimeMs(long paramLong) {
    this.mExecutionStartTimeMs = paramLong;
  }
  
  public Session getParentSession() {
    return this.mParentSession;
  }
  
  public ArrayList<Session> getChildSessions() {
    return this.mChildSessions;
  }
  
  public boolean isSessionCompleted() {
    return this.mIsCompleted;
  }
  
  public boolean isStartedFromActiveSession() {
    return this.mIsStartedFromActiveSession;
  }
  
  public Info getInfo() {
    return Info.getInfo(this);
  }
  
  public Info getExternalInfo(String paramString) {
    return Info.getExternalInfo(this, paramString);
  }
  
  public String getOwnerInfo() {
    return this.mOwnerInfo;
  }
  
  public String getSessionId() {
    return this.mSessionId;
  }
  
  public void markSessionCompleted(long paramLong) {
    this.mExecutionEndTimeMs = paramLong;
    this.mIsCompleted = true;
  }
  
  public long getLocalExecutionTime() {
    long l = this.mExecutionEndTimeMs;
    if (l == -1L)
      return -1L; 
    return l - this.mExecutionStartTimeMs;
  }
  
  public String getNextChildId() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mChildCounter : I
    //   6: istore_1
    //   7: aload_0
    //   8: iload_1
    //   9: iconst_1
    //   10: iadd
    //   11: putfield mChildCounter : I
    //   14: aload_0
    //   15: monitorexit
    //   16: iload_1
    //   17: invokestatic valueOf : (I)Ljava/lang/String;
    //   20: areturn
    //   21: astore_2
    //   22: aload_0
    //   23: monitorexit
    //   24: aload_2
    //   25: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #263	-> 2
    //   #263	-> 21
    // Exception table:
    //   from	to	target	type
    //   2	14	21	finally
  }
  
  private String getFullSessionId() {
    return getFullSessionId(0);
  }
  
  private String getFullSessionId(int paramInt) {
    if (paramInt >= 25) {
      Slog.w("Session", "getFullSessionId: Hit recursion limit!");
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("...");
      stringBuilder.append(this.mSessionId);
      return stringBuilder.toString();
    } 
    Session session = this.mParentSession;
    if (session == null)
      return this.mSessionId; 
    if (Log.VERBOSE) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(session.getFullSessionId(paramInt + 1));
      stringBuilder.append("_");
      stringBuilder.append(this.mSessionId);
      return stringBuilder.toString();
    } 
    return session.getFullSessionId(paramInt + 1);
  }
  
  private Session getRootSession(String paramString) {
    byte b = 0;
    Session session = this;
    while (session.getParentSession() != null) {
      if (b >= 25) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("getRootSession: Hit recursion limit from ");
        stringBuilder.append(paramString);
        Slog.w("Session", stringBuilder.toString());
        break;
      } 
      session = session.getParentSession();
      b++;
    } 
    return session;
  }
  
  public String printFullSessionTree() {
    return getRootSession("printFullSessionTree").printSessionTree();
  }
  
  private String printSessionTree() {
    StringBuilder stringBuilder = new StringBuilder();
    printSessionTree(0, stringBuilder, 0);
    return stringBuilder.toString();
  }
  
  private void printSessionTree(int paramInt1, StringBuilder paramStringBuilder, int paramInt2) {
    if (paramInt2 >= 25) {
      Slog.w("Session", "printSessionTree: Hit recursion limit!");
      paramStringBuilder.append("...");
      return;
    } 
    paramStringBuilder.append(toString());
    for (Session session : this.mChildSessions) {
      paramStringBuilder.append("\n");
      for (byte b = 0; b <= paramInt1; b++)
        paramStringBuilder.append("\t"); 
      session.printSessionTree(paramInt1 + 1, paramStringBuilder, paramInt2 + 1);
    } 
  }
  
  public String getFullMethodPath(boolean paramBoolean) {
    StringBuilder stringBuilder = new StringBuilder();
    getFullMethodPath(stringBuilder, paramBoolean, 0);
    return stringBuilder.toString();
  }
  
  private void getFullMethodPath(StringBuilder paramStringBuilder, boolean paramBoolean, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: iload_3
    //   3: bipush #25
    //   5: if_icmplt -> 26
    //   8: ldc 'Session'
    //   10: ldc 'getFullMethodPath: Hit recursion limit!'
    //   12: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   15: pop
    //   16: aload_1
    //   17: ldc '...'
    //   19: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   22: pop
    //   23: aload_0
    //   24: monitorexit
    //   25: return
    //   26: aload_0
    //   27: getfield mFullMethodPathCache : Ljava/lang/String;
    //   30: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
    //   33: ifne -> 52
    //   36: iload_2
    //   37: ifne -> 52
    //   40: aload_1
    //   41: aload_0
    //   42: getfield mFullMethodPathCache : Ljava/lang/String;
    //   45: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   48: pop
    //   49: aload_0
    //   50: monitorexit
    //   51: return
    //   52: aload_0
    //   53: invokevirtual getParentSession : ()Landroid/telecom/Logging/Session;
    //   56: astore #4
    //   58: iconst_0
    //   59: istore #5
    //   61: aload #4
    //   63: ifnull -> 107
    //   66: aload_0
    //   67: getfield mShortMethodName : Ljava/lang/String;
    //   70: aload #4
    //   72: getfield mShortMethodName : Ljava/lang/String;
    //   75: invokevirtual equals : (Ljava/lang/Object;)Z
    //   78: ifne -> 87
    //   81: iconst_1
    //   82: istore #5
    //   84: goto -> 90
    //   87: iconst_0
    //   88: istore #5
    //   90: aload #4
    //   92: aload_1
    //   93: iload_2
    //   94: iload_3
    //   95: iconst_1
    //   96: iadd
    //   97: invokespecial getFullMethodPath : (Ljava/lang/StringBuilder;ZI)V
    //   100: aload_1
    //   101: ldc '->'
    //   103: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   106: pop
    //   107: aload_0
    //   108: invokevirtual isExternal : ()Z
    //   111: ifeq -> 154
    //   114: iload_2
    //   115: ifeq -> 128
    //   118: aload_1
    //   119: ldc '...'
    //   121: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   124: pop
    //   125: goto -> 163
    //   128: aload_1
    //   129: ldc '('
    //   131: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   134: pop
    //   135: aload_1
    //   136: aload_0
    //   137: getfield mShortMethodName : Ljava/lang/String;
    //   140: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   143: pop
    //   144: aload_1
    //   145: ldc ')'
    //   147: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   150: pop
    //   151: goto -> 163
    //   154: aload_1
    //   155: aload_0
    //   156: getfield mShortMethodName : Ljava/lang/String;
    //   159: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   162: pop
    //   163: iload #5
    //   165: ifeq -> 180
    //   168: iload_2
    //   169: ifne -> 180
    //   172: aload_0
    //   173: aload_1
    //   174: invokevirtual toString : ()Ljava/lang/String;
    //   177: putfield mFullMethodPathCache : Ljava/lang/String;
    //   180: aload_0
    //   181: monitorexit
    //   182: return
    //   183: astore_1
    //   184: aload_0
    //   185: monitorexit
    //   186: aload_1
    //   187: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #361	-> 2
    //   #365	-> 8
    //   #366	-> 16
    //   #367	-> 23
    //   #371	-> 26
    //   #372	-> 40
    //   #373	-> 49
    //   #375	-> 52
    //   #376	-> 58
    //   #377	-> 61
    //   #380	-> 66
    //   #381	-> 90
    //   #382	-> 100
    //   #386	-> 107
    //   #387	-> 114
    //   #388	-> 118
    //   #390	-> 128
    //   #391	-> 135
    //   #392	-> 144
    //   #395	-> 154
    //   #398	-> 163
    //   #401	-> 172
    //   #403	-> 180
    //   #360	-> 183
    // Exception table:
    //   from	to	target	type
    //   8	16	183	finally
    //   16	23	183	finally
    //   26	36	183	finally
    //   40	49	183	finally
    //   52	58	183	finally
    //   66	81	183	finally
    //   90	100	183	finally
    //   100	107	183	finally
    //   107	114	183	finally
    //   118	125	183	finally
    //   128	135	183	finally
    //   135	144	183	finally
    //   144	151	183	finally
    //   154	163	183	finally
    //   172	180	183	finally
  }
  
  private boolean isSessionExternal() {
    return getRootSession("isSessionExternal").isExternal();
  }
  
  public int hashCode() {
    byte b1, b2, b3, b4;
    String str2 = this.mSessionId;
    int i = 0;
    if (str2 != null) {
      b1 = str2.hashCode();
    } else {
      b1 = 0;
    } 
    str2 = this.mShortMethodName;
    if (str2 != null) {
      b2 = str2.hashCode();
    } else {
      b2 = 0;
    } 
    long l = this.mExecutionStartTimeMs;
    int j = (int)(l ^ l >>> 32L);
    l = this.mExecutionEndTimeMs;
    int k = (int)(l ^ l >>> 32L);
    Session session = this.mParentSession;
    if (session != null) {
      b3 = session.hashCode();
    } else {
      b3 = 0;
    } 
    ArrayList<Session> arrayList = this.mChildSessions;
    if (arrayList != null) {
      b4 = arrayList.hashCode();
    } else {
      b4 = 0;
    } 
    boolean bool1 = this.mIsCompleted;
    int m = this.mChildCounter;
    boolean bool2 = this.mIsStartedFromActiveSession;
    String str1 = this.mOwnerInfo;
    if (str1 != null)
      i = str1.hashCode(); 
    return ((((((((b1 * 31 + b2) * 31 + j) * 31 + k) * 31 + b3) * 31 + b4) * 31 + bool1) * 31 + m) * 31 + bool2) * 31 + i;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mExecutionStartTimeMs != ((Session)paramObject).mExecutionStartTimeMs)
      return false; 
    if (this.mExecutionEndTimeMs != ((Session)paramObject).mExecutionEndTimeMs)
      return false; 
    if (this.mIsCompleted != ((Session)paramObject).mIsCompleted)
      return false; 
    if (this.mChildCounter != ((Session)paramObject).mChildCounter)
      return false; 
    if (this.mIsStartedFromActiveSession != ((Session)paramObject).mIsStartedFromActiveSession)
      return false; 
    String str1 = this.mSessionId;
    if (str1 != null) {
      String str = ((Session)paramObject).mSessionId;
      if (!str1.equals(str))
        return false; 
    } else if (((Session)paramObject).mSessionId != null) {
      return false;
    } 
    String str3 = this.mShortMethodName;
    if ((str3 != null) ? !str3.equals(((Session)paramObject).mShortMethodName) : (((Session)paramObject).mShortMethodName != null))
      return false; 
    Session session = this.mParentSession;
    if ((session != null) ? !session.equals(((Session)paramObject).mParentSession) : (((Session)paramObject).mParentSession != null))
      return false; 
    ArrayList<Session> arrayList = this.mChildSessions;
    if ((arrayList != null) ? !arrayList.equals(((Session)paramObject).mChildSessions) : (((Session)paramObject).mChildSessions != null))
      return false; 
    String str2 = this.mOwnerInfo;
    if (str2 != null) {
      bool = str2.equals(((Session)paramObject).mOwnerInfo);
    } else if (((Session)paramObject).mOwnerInfo != null) {
      bool = false;
    } 
    return bool;
  }
  
  public String toString() {
    Session session = this.mParentSession;
    if (session != null && this.mIsStartedFromActiveSession)
      return session.toString(); 
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append(getFullMethodPath(false));
    String str = this.mOwnerInfo;
    if (str != null && !str.isEmpty()) {
      stringBuilder1.append("(");
      stringBuilder1.append(this.mOwnerInfo);
      stringBuilder1.append(")");
    } 
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(stringBuilder1.toString());
    stringBuilder2.append("@");
    stringBuilder2.append(getFullSessionId());
    return stringBuilder2.toString();
  }
}
