package android.telecom.Logging;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.telecom.Log;
import android.util.Base64;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class SessionManager {
  private int sCodeEntryCounter = 0;
  
  public ConcurrentHashMap<Integer, Session> mSessionMapper = new ConcurrentHashMap<>(100);
  
  public java.lang.Runnable mCleanStaleSessions = new _$$Lambda$SessionManager$VyH2gT1EjIvzDy_C9JfTT60CISM(this);
  
  private Handler mSessionCleanupHandler = new Handler(Looper.getMainLooper());
  
  public ICurrentThreadId mCurrentThreadId = (ICurrentThreadId)_$$Lambda$L5F_SL2jOCUETYvgdB36aGwY50E.INSTANCE;
  
  private ISessionCleanupTimeoutMs mSessionCleanupTimeoutMs = new _$$Lambda$SessionManager$hhtZwTEbvO_fLNlAvB6Do9_2gW4(this);
  
  private List<ISessionListener> mSessionListeners = new ArrayList<>();
  
  private static final long DEFAULT_SESSION_TIMEOUT_MS = 30000L;
  
  private static final String LOGGING_TAG = "Logging";
  
  private static final long SESSION_ID_ROLLOVER_THRESHOLD = 262144L;
  
  private static final String TIMEOUTS_PREFIX = "telecom.";
  
  private Context mContext;
  
  public void setContext(Context paramContext) {
    this.mContext = paramContext;
  }
  
  private long getSessionCleanupTimeoutMs() {
    return this.mSessionCleanupTimeoutMs.get();
  }
  
  private void resetStaleSessionTimer() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mSessionCleanupHandler : Landroid/os/Handler;
    //   6: aconst_null
    //   7: invokevirtual removeCallbacksAndMessages : (Ljava/lang/Object;)V
    //   10: aload_0
    //   11: getfield mCleanStaleSessions : Ljava/lang/Runnable;
    //   14: ifnull -> 33
    //   17: aload_0
    //   18: getfield mSessionCleanupHandler : Landroid/os/Handler;
    //   21: aload_0
    //   22: getfield mCleanStaleSessions : Ljava/lang/Runnable;
    //   25: aload_0
    //   26: invokespecial getSessionCleanupTimeoutMs : ()J
    //   29: invokevirtual postDelayed : (Ljava/lang/Runnable;J)Z
    //   32: pop
    //   33: aload_0
    //   34: monitorexit
    //   35: return
    //   36: astore_1
    //   37: aload_0
    //   38: monitorexit
    //   39: aload_1
    //   40: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #112	-> 2
    //   #114	-> 10
    //   #115	-> 17
    //   #117	-> 33
    //   #111	-> 36
    // Exception table:
    //   from	to	target	type
    //   2	10	36	finally
    //   10	17	36	finally
    //   17	33	36	finally
  }
  
  public void startSession(Session.Info paramInfo, String paramString1, String paramString2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_1
    //   3: ifnonnull -> 15
    //   6: aload_0
    //   7: aload_2
    //   8: aload_3
    //   9: invokevirtual startSession : (Ljava/lang/String;Ljava/lang/String;)V
    //   12: goto -> 21
    //   15: aload_0
    //   16: aload_1
    //   17: aload_2
    //   18: invokevirtual startExternalSession : (Landroid/telecom/Logging/Session$Info;Ljava/lang/String;)V
    //   21: aload_0
    //   22: monitorexit
    //   23: return
    //   24: astore_1
    //   25: aload_0
    //   26: monitorexit
    //   27: aload_1
    //   28: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #127	-> 2
    //   #128	-> 6
    //   #130	-> 15
    //   #132	-> 21
    //   #126	-> 24
    // Exception table:
    //   from	to	target	type
    //   6	12	24	finally
    //   15	21	24	finally
  }
  
  public void startSession(String paramString1, String paramString2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial resetStaleSessionTimer : ()V
    //   6: aload_0
    //   7: invokespecial getCallingThreadId : ()I
    //   10: istore_3
    //   11: aload_0
    //   12: getfield mSessionMapper : Ljava/util/concurrent/ConcurrentHashMap;
    //   15: iload_3
    //   16: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   19: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   22: checkcast android/telecom/Logging/Session
    //   25: astore #4
    //   27: aload #4
    //   29: ifnull -> 47
    //   32: aload_0
    //   33: iconst_1
    //   34: invokevirtual createSubsession : (Z)Landroid/telecom/Logging/Session;
    //   37: astore_2
    //   38: aload_0
    //   39: aload_2
    //   40: aload_1
    //   41: invokevirtual continueSession : (Landroid/telecom/Logging/Session;Ljava/lang/String;)V
    //   44: aload_0
    //   45: monitorexit
    //   46: return
    //   47: getstatic android/telecom/Log.OPPO_DEBUG : Z
    //   50: ifeq -> 65
    //   53: ldc 'Logging'
    //   55: ldc_w 'START_SESSION'
    //   58: iconst_0
    //   59: anewarray java/lang/Object
    //   62: invokestatic d : (Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    //   65: new android/telecom/Logging/Session
    //   68: astore #5
    //   70: aload_0
    //   71: invokespecial getNextSessionID : ()Ljava/lang/String;
    //   74: astore #4
    //   76: aload #5
    //   78: aload #4
    //   80: aload_1
    //   81: invokestatic currentTimeMillis : ()J
    //   84: iconst_0
    //   85: aload_2
    //   86: invokespecial <init> : (Ljava/lang/String;Ljava/lang/String;JZLjava/lang/String;)V
    //   89: aload_0
    //   90: getfield mSessionMapper : Ljava/util/concurrent/ConcurrentHashMap;
    //   93: iload_3
    //   94: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   97: aload #5
    //   99: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   102: pop
    //   103: aload_0
    //   104: monitorexit
    //   105: return
    //   106: astore_1
    //   107: aload_0
    //   108: monitorexit
    //   109: aload_1
    //   110: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #140	-> 2
    //   #141	-> 6
    //   #142	-> 11
    //   #145	-> 27
    //   #146	-> 32
    //   #147	-> 38
    //   #148	-> 44
    //   #153	-> 47
    //   #154	-> 53
    //   #158	-> 65
    //   #159	-> 76
    //   #160	-> 89
    //   #161	-> 103
    //   #139	-> 106
    // Exception table:
    //   from	to	target	type
    //   2	6	106	finally
    //   6	11	106	finally
    //   11	27	106	finally
    //   32	38	106	finally
    //   38	44	106	finally
    //   47	53	106	finally
    //   53	65	106	finally
    //   65	76	106	finally
    //   76	89	106	finally
    //   89	103	106	finally
  }
  
  public void startExternalSession(Session.Info paramInfo, String paramString) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_1
    //   3: ifnonnull -> 9
    //   6: aload_0
    //   7: monitorexit
    //   8: return
    //   9: aload_0
    //   10: invokespecial getCallingThreadId : ()I
    //   13: istore_3
    //   14: aload_0
    //   15: getfield mSessionMapper : Ljava/util/concurrent/ConcurrentHashMap;
    //   18: iload_3
    //   19: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   22: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   25: checkcast android/telecom/Logging/Session
    //   28: astore #4
    //   30: aload #4
    //   32: ifnull -> 50
    //   35: ldc 'Logging'
    //   37: ldc_w 'trying to start an external session with a session already active.'
    //   40: iconst_0
    //   41: anewarray java/lang/Object
    //   44: invokestatic w : (Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    //   47: aload_0
    //   48: monitorexit
    //   49: return
    //   50: getstatic android/telecom/Log.OPPO_DEBUG : Z
    //   53: ifeq -> 68
    //   56: ldc 'Logging'
    //   58: ldc_w 'START_EXTERNAL_SESSION'
    //   61: iconst_0
    //   62: anewarray java/lang/Object
    //   65: invokestatic d : (Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    //   68: new android/telecom/Logging/Session
    //   71: astore #4
    //   73: new java/lang/StringBuilder
    //   76: astore #5
    //   78: aload #5
    //   80: invokespecial <init> : ()V
    //   83: aload #5
    //   85: ldc_w 'E-'
    //   88: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   91: pop
    //   92: aload #5
    //   94: aload_1
    //   95: getfield sessionId : Ljava/lang/String;
    //   98: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   101: pop
    //   102: aload #5
    //   104: invokevirtual toString : ()Ljava/lang/String;
    //   107: astore #6
    //   109: aload_1
    //   110: getfield methodPath : Ljava/lang/String;
    //   113: astore #5
    //   115: aload #4
    //   117: aload #6
    //   119: aload #5
    //   121: invokestatic currentTimeMillis : ()J
    //   124: iconst_0
    //   125: aload_1
    //   126: getfield ownerInfo : Ljava/lang/String;
    //   129: invokespecial <init> : (Ljava/lang/String;Ljava/lang/String;JZLjava/lang/String;)V
    //   132: aload #4
    //   134: iconst_1
    //   135: invokevirtual setIsExternal : (Z)V
    //   138: aload #4
    //   140: ldc2_w -1
    //   143: invokevirtual markSessionCompleted : (J)V
    //   146: aload_0
    //   147: getfield mSessionMapper : Ljava/util/concurrent/ConcurrentHashMap;
    //   150: iload_3
    //   151: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   154: aload #4
    //   156: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   159: pop
    //   160: aload_0
    //   161: invokevirtual createSubsession : ()Landroid/telecom/Logging/Session;
    //   164: astore_1
    //   165: aload_0
    //   166: aload_1
    //   167: aload_2
    //   168: invokevirtual continueSession : (Landroid/telecom/Logging/Session;Ljava/lang/String;)V
    //   171: aload_0
    //   172: monitorexit
    //   173: return
    //   174: astore_1
    //   175: aload_0
    //   176: monitorexit
    //   177: aload_1
    //   178: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #171	-> 2
    //   #172	-> 6
    //   #175	-> 9
    //   #176	-> 14
    //   #177	-> 30
    //   #180	-> 35
    //   #182	-> 47
    //   #188	-> 50
    //   #189	-> 56
    //   #192	-> 68
    //   #193	-> 115
    //   #195	-> 132
    //   #198	-> 138
    //   #201	-> 146
    //   #203	-> 160
    //   #204	-> 165
    //   #205	-> 171
    //   #170	-> 174
    // Exception table:
    //   from	to	target	type
    //   9	14	174	finally
    //   14	30	174	finally
    //   35	47	174	finally
    //   50	56	174	finally
    //   56	68	174	finally
    //   68	115	174	finally
    //   115	132	174	finally
    //   132	138	174	finally
    //   138	146	174	finally
    //   146	160	174	finally
    //   160	165	174	finally
    //   165	171	174	finally
  }
  
  public Session createSubsession() {
    return createSubsession(false);
  }
  
  public Session createSubsession(boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial getCallingThreadId : ()I
    //   6: istore_2
    //   7: aload_0
    //   8: getfield mSessionMapper : Ljava/util/concurrent/ConcurrentHashMap;
    //   11: iload_2
    //   12: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   15: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   18: checkcast android/telecom/Logging/Session
    //   21: astore_3
    //   22: aload_3
    //   23: ifnonnull -> 48
    //   26: getstatic android/telecom/Log.OPPO_DEBUG : Z
    //   29: ifeq -> 44
    //   32: ldc 'Logging'
    //   34: ldc_w 'Log.createSubsession was called with no session active.'
    //   37: iconst_0
    //   38: anewarray java/lang/Object
    //   41: invokestatic d : (Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    //   44: aload_0
    //   45: monitorexit
    //   46: aconst_null
    //   47: areturn
    //   48: new android/telecom/Logging/Session
    //   51: astore #4
    //   53: aload_3
    //   54: invokevirtual getNextChildId : ()Ljava/lang/String;
    //   57: astore #5
    //   59: aload_3
    //   60: invokevirtual getShortMethodName : ()Ljava/lang/String;
    //   63: astore #6
    //   65: invokestatic currentTimeMillis : ()J
    //   68: lstore #7
    //   70: aload #4
    //   72: aload #5
    //   74: aload #6
    //   76: lload #7
    //   78: iload_1
    //   79: aload_3
    //   80: invokevirtual getOwnerInfo : ()Ljava/lang/String;
    //   83: invokespecial <init> : (Ljava/lang/String;Ljava/lang/String;JZLjava/lang/String;)V
    //   86: aload_3
    //   87: aload #4
    //   89: invokevirtual addChild : (Landroid/telecom/Logging/Session;)V
    //   92: aload #4
    //   94: aload_3
    //   95: invokevirtual setParentSession : (Landroid/telecom/Logging/Session;)V
    //   98: iload_1
    //   99: ifne -> 146
    //   102: new java/lang/StringBuilder
    //   105: astore_3
    //   106: aload_3
    //   107: invokespecial <init> : ()V
    //   110: aload_3
    //   111: ldc_w 'CREATE_SUBSESSION '
    //   114: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   117: pop
    //   118: aload_3
    //   119: aload #4
    //   121: invokevirtual toString : ()Ljava/lang/String;
    //   124: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   127: pop
    //   128: aload_3
    //   129: invokevirtual toString : ()Ljava/lang/String;
    //   132: astore_3
    //   133: ldc 'Logging'
    //   135: aload_3
    //   136: iconst_0
    //   137: anewarray java/lang/Object
    //   140: invokestatic v : (Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    //   143: goto -> 158
    //   146: ldc 'Logging'
    //   148: ldc_w 'CREATE_SUBSESSION (Invisible subsession)'
    //   151: iconst_0
    //   152: anewarray java/lang/Object
    //   155: invokestatic v : (Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    //   158: aload_0
    //   159: monitorexit
    //   160: aload #4
    //   162: areturn
    //   163: astore #4
    //   165: aload_0
    //   166: monitorexit
    //   167: aload #4
    //   169: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #228	-> 2
    //   #229	-> 7
    //   #230	-> 22
    //   #233	-> 26
    //   #234	-> 32
    //   #238	-> 44
    //   #241	-> 48
    //   #242	-> 59
    //   #243	-> 70
    //   #244	-> 86
    //   #245	-> 92
    //   #247	-> 98
    //   #248	-> 102
    //   #249	-> 118
    //   #248	-> 133
    //   #251	-> 146
    //   #254	-> 158
    //   #227	-> 163
    // Exception table:
    //   from	to	target	type
    //   2	7	163	finally
    //   7	22	163	finally
    //   26	32	163	finally
    //   32	44	163	finally
    //   48	59	163	finally
    //   59	70	163	finally
    //   70	86	163	finally
    //   86	92	163	finally
    //   92	98	163	finally
    //   102	118	163	finally
    //   118	133	163	finally
    //   133	143	163	finally
    //   146	158	163	finally
  }
  
  public Session.Info getExternalSession() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aconst_null
    //   4: invokevirtual getExternalSession : (Ljava/lang/String;)Landroid/telecom/Logging/Session$Info;
    //   7: astore_1
    //   8: aload_0
    //   9: monitorexit
    //   10: aload_1
    //   11: areturn
    //   12: astore_1
    //   13: aload_0
    //   14: monitorexit
    //   15: aload_1
    //   16: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #258	-> 2
    //   #258	-> 12
    // Exception table:
    //   from	to	target	type
    //   2	8	12	finally
  }
  
  public Session.Info getExternalSession(String paramString) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial getCallingThreadId : ()I
    //   6: istore_2
    //   7: aload_0
    //   8: getfield mSessionMapper : Ljava/util/concurrent/ConcurrentHashMap;
    //   11: iload_2
    //   12: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   15: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   18: checkcast android/telecom/Logging/Session
    //   21: astore_3
    //   22: aload_3
    //   23: ifnonnull -> 48
    //   26: getstatic android/telecom/Log.OPPO_DEBUG : Z
    //   29: ifeq -> 44
    //   32: ldc 'Logging'
    //   34: ldc_w 'Log.getExternalSession was called with no session active.'
    //   37: iconst_0
    //   38: anewarray java/lang/Object
    //   41: invokestatic d : (Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    //   44: aload_0
    //   45: monitorexit
    //   46: aconst_null
    //   47: areturn
    //   48: aload_3
    //   49: aload_1
    //   50: invokevirtual getExternalInfo : (Ljava/lang/String;)Landroid/telecom/Logging/Session$Info;
    //   53: astore_1
    //   54: aload_0
    //   55: monitorexit
    //   56: aload_1
    //   57: areturn
    //   58: astore_1
    //   59: aload_0
    //   60: monitorexit
    //   61: aload_1
    //   62: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #269	-> 2
    //   #270	-> 7
    //   #271	-> 22
    //   #274	-> 26
    //   #275	-> 32
    //   #279	-> 44
    //   #281	-> 48
    //   #268	-> 58
    // Exception table:
    //   from	to	target	type
    //   2	7	58	finally
    //   7	22	58	finally
    //   26	32	58	finally
    //   32	44	58	finally
    //   48	54	58	finally
  }
  
  public void cancelSubsession(Session paramSession) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_1
    //   3: ifnonnull -> 9
    //   6: aload_0
    //   7: monitorexit
    //   8: return
    //   9: aload_1
    //   10: ldc2_w -1
    //   13: invokevirtual markSessionCompleted : (J)V
    //   16: aload_0
    //   17: aload_1
    //   18: invokespecial endParentSessions : (Landroid/telecom/Logging/Session;)V
    //   21: aload_0
    //   22: monitorexit
    //   23: return
    //   24: astore_1
    //   25: aload_0
    //   26: monitorexit
    //   27: aload_1
    //   28: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #290	-> 2
    //   #291	-> 6
    //   #294	-> 9
    //   #295	-> 16
    //   #296	-> 21
    //   #289	-> 24
    // Exception table:
    //   from	to	target	type
    //   9	16	24	finally
    //   16	21	24	finally
  }
  
  public void continueSession(Session paramSession, String paramString) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_1
    //   3: ifnonnull -> 9
    //   6: aload_0
    //   7: monitorexit
    //   8: return
    //   9: aload_0
    //   10: invokespecial resetStaleSessionTimer : ()V
    //   13: aload_1
    //   14: aload_2
    //   15: invokevirtual setShortMethodName : (Ljava/lang/String;)V
    //   18: aload_1
    //   19: invokestatic currentTimeMillis : ()J
    //   22: invokevirtual setExecutionStartTimeMs : (J)V
    //   25: aload_1
    //   26: invokevirtual getParentSession : ()Landroid/telecom/Logging/Session;
    //   29: astore_3
    //   30: aload_3
    //   31: ifnonnull -> 72
    //   34: new java/lang/StringBuilder
    //   37: astore_1
    //   38: aload_1
    //   39: invokespecial <init> : ()V
    //   42: aload_1
    //   43: ldc_w 'Log.continueSession was called with no session active for method '
    //   46: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   49: pop
    //   50: aload_1
    //   51: aload_2
    //   52: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   55: pop
    //   56: ldc 'Logging'
    //   58: aload_1
    //   59: invokevirtual toString : ()Ljava/lang/String;
    //   62: iconst_0
    //   63: anewarray java/lang/Object
    //   66: invokestatic i : (Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    //   69: aload_0
    //   70: monitorexit
    //   71: return
    //   72: aload_0
    //   73: getfield mSessionMapper : Ljava/util/concurrent/ConcurrentHashMap;
    //   76: aload_0
    //   77: invokespecial getCallingThreadId : ()I
    //   80: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   83: aload_1
    //   84: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   87: pop
    //   88: aload_1
    //   89: invokevirtual isStartedFromActiveSession : ()Z
    //   92: ifne -> 110
    //   95: ldc 'Logging'
    //   97: ldc_w 'CONTINUE_SUBSESSION'
    //   100: iconst_0
    //   101: anewarray java/lang/Object
    //   104: invokestatic v : (Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    //   107: goto -> 145
    //   110: new java/lang/StringBuilder
    //   113: astore_1
    //   114: aload_1
    //   115: invokespecial <init> : ()V
    //   118: aload_1
    //   119: ldc_w 'CONTINUE_SUBSESSION (Invisible Subsession) with Method '
    //   122: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   125: pop
    //   126: aload_1
    //   127: aload_2
    //   128: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   131: pop
    //   132: ldc 'Logging'
    //   134: aload_1
    //   135: invokevirtual toString : ()Ljava/lang/String;
    //   138: iconst_0
    //   139: anewarray java/lang/Object
    //   142: invokestatic v : (Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    //   145: aload_0
    //   146: monitorexit
    //   147: return
    //   148: astore_1
    //   149: aload_0
    //   150: monitorexit
    //   151: aload_1
    //   152: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #304	-> 2
    //   #305	-> 6
    //   #307	-> 9
    //   #308	-> 13
    //   #309	-> 18
    //   #310	-> 25
    //   #311	-> 30
    //   #312	-> 34
    //   #314	-> 69
    //   #317	-> 72
    //   #318	-> 88
    //   #319	-> 95
    //   #321	-> 110
    //   #324	-> 145
    //   #303	-> 148
    // Exception table:
    //   from	to	target	type
    //   9	13	148	finally
    //   13	18	148	finally
    //   18	25	148	finally
    //   25	30	148	finally
    //   34	69	148	finally
    //   72	88	148	finally
    //   88	95	148	finally
    //   95	107	148	finally
    //   110	145	148	finally
  }
  
  public void endSession() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial getCallingThreadId : ()I
    //   6: istore_1
    //   7: aload_0
    //   8: getfield mSessionMapper : Ljava/util/concurrent/ConcurrentHashMap;
    //   11: iload_1
    //   12: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   15: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   18: checkcast android/telecom/Logging/Session
    //   21: astore_2
    //   22: aload_2
    //   23: ifnonnull -> 41
    //   26: ldc 'Logging'
    //   28: ldc_w 'Log.endSession was called with no session active.'
    //   31: iconst_0
    //   32: anewarray java/lang/Object
    //   35: invokestatic w : (Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    //   38: aload_0
    //   39: monitorexit
    //   40: return
    //   41: aload_2
    //   42: invokestatic currentTimeMillis : ()J
    //   45: invokevirtual markSessionCompleted : (J)V
    //   48: aload_2
    //   49: invokevirtual isStartedFromActiveSession : ()Z
    //   52: ifne -> 106
    //   55: new java/lang/StringBuilder
    //   58: astore_3
    //   59: aload_3
    //   60: invokespecial <init> : ()V
    //   63: aload_3
    //   64: ldc_w 'END_SUBSESSION (dur: '
    //   67: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   70: pop
    //   71: aload_3
    //   72: aload_2
    //   73: invokevirtual getLocalExecutionTime : ()J
    //   76: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   79: pop
    //   80: aload_3
    //   81: ldc_w ' mS)'
    //   84: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   87: pop
    //   88: aload_3
    //   89: invokevirtual toString : ()Ljava/lang/String;
    //   92: astore_3
    //   93: ldc 'Logging'
    //   95: aload_3
    //   96: iconst_0
    //   97: anewarray java/lang/Object
    //   100: invokestatic v : (Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    //   103: goto -> 154
    //   106: new java/lang/StringBuilder
    //   109: astore_3
    //   110: aload_3
    //   111: invokespecial <init> : ()V
    //   114: aload_3
    //   115: ldc_w 'END_SUBSESSION (Invisible Subsession) (dur: '
    //   118: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   121: pop
    //   122: aload_3
    //   123: aload_2
    //   124: invokevirtual getLocalExecutionTime : ()J
    //   127: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   130: pop
    //   131: aload_3
    //   132: ldc_w ' ms)'
    //   135: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   138: pop
    //   139: aload_3
    //   140: invokevirtual toString : ()Ljava/lang/String;
    //   143: astore_3
    //   144: ldc 'Logging'
    //   146: aload_3
    //   147: iconst_0
    //   148: anewarray java/lang/Object
    //   151: invokestatic v : (Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    //   154: aload_2
    //   155: invokevirtual getParentSession : ()Landroid/telecom/Logging/Session;
    //   158: astore_3
    //   159: aload_0
    //   160: getfield mSessionMapper : Ljava/util/concurrent/ConcurrentHashMap;
    //   163: iload_1
    //   164: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   167: invokevirtual remove : (Ljava/lang/Object;)Ljava/lang/Object;
    //   170: pop
    //   171: aload_0
    //   172: aload_2
    //   173: invokespecial endParentSessions : (Landroid/telecom/Logging/Session;)V
    //   176: aload_3
    //   177: ifnull -> 207
    //   180: aload_3
    //   181: invokevirtual isSessionCompleted : ()Z
    //   184: ifne -> 207
    //   187: aload_2
    //   188: invokevirtual isStartedFromActiveSession : ()Z
    //   191: ifeq -> 207
    //   194: aload_0
    //   195: getfield mSessionMapper : Ljava/util/concurrent/ConcurrentHashMap;
    //   198: iload_1
    //   199: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   202: aload_3
    //   203: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   206: pop
    //   207: aload_0
    //   208: monitorexit
    //   209: return
    //   210: astore_2
    //   211: aload_0
    //   212: monitorexit
    //   213: aload_2
    //   214: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #331	-> 2
    //   #332	-> 7
    //   #333	-> 22
    //   #334	-> 26
    //   #335	-> 38
    //   #338	-> 41
    //   #339	-> 48
    //   #340	-> 55
    //   #341	-> 71
    //   #340	-> 93
    //   #343	-> 106
    //   #344	-> 122
    //   #343	-> 144
    //   #348	-> 154
    //   #349	-> 159
    //   #350	-> 171
    //   #353	-> 176
    //   #354	-> 187
    //   #355	-> 194
    //   #357	-> 207
    //   #330	-> 210
    // Exception table:
    //   from	to	target	type
    //   2	7	210	finally
    //   7	22	210	finally
    //   26	38	210	finally
    //   41	48	210	finally
    //   48	55	210	finally
    //   55	71	210	finally
    //   71	93	210	finally
    //   93	103	210	finally
    //   106	122	210	finally
    //   122	144	210	finally
    //   144	154	210	finally
    //   154	159	210	finally
    //   159	171	210	finally
    //   171	176	210	finally
    //   180	187	210	finally
    //   187	194	210	finally
    //   194	207	210	finally
  }
  
  private void endParentSessions(Session paramSession) {
    if (!paramSession.isSessionCompleted() || paramSession.getChildSessions().size() != 0)
      return; 
    Session session = paramSession.getParentSession();
    if (session != null) {
      paramSession.setParentSession(null);
      session.removeChild(paramSession);
      if (session.isExternal()) {
        long l1 = System.currentTimeMillis(), l2 = paramSession.getExecutionStartTimeMilliseconds();
        notifySessionCompleteListeners(paramSession.getShortMethodName(), l1 - l2);
      } 
      endParentSessions(session);
    } else {
      long l = System.currentTimeMillis() - paramSession.getExecutionStartTimeMilliseconds();
      if (Log.OPPO_DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("END_SESSION (dur: ");
        stringBuilder.append(l);
        stringBuilder.append(" ms): ");
        stringBuilder.append(paramSession.toString());
        String str = stringBuilder.toString();
        Log.d("Logging", str, new Object[0]);
      } 
      if (!paramSession.isExternal())
        notifySessionCompleteListeners(paramSession.getShortMethodName(), l); 
    } 
  }
  
  private void notifySessionCompleteListeners(String paramString, long paramLong) {
    for (ISessionListener iSessionListener : this.mSessionListeners)
      iSessionListener.sessionComplete(paramString, paramLong); 
  }
  
  public String getSessionId() {
    String str;
    Session session = this.mSessionMapper.get(Integer.valueOf(getCallingThreadId()));
    if (session != null) {
      str = session.toString();
    } else {
      str = "";
    } 
    return str;
  }
  
  public void registerSessionListener(ISessionListener paramISessionListener) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_1
    //   3: ifnull -> 25
    //   6: aload_0
    //   7: getfield mSessionListeners : Ljava/util/List;
    //   10: aload_1
    //   11: invokeinterface add : (Ljava/lang/Object;)Z
    //   16: pop
    //   17: goto -> 25
    //   20: astore_1
    //   21: aload_0
    //   22: monitorexit
    //   23: aload_1
    //   24: athrow
    //   25: aload_0
    //   26: monitorexit
    //   27: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #408	-> 2
    //   #409	-> 6
    //   #407	-> 20
    //   #411	-> 25
    // Exception table:
    //   from	to	target	type
    //   6	17	20	finally
  }
  
  private String getNextSessionID() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield sCodeEntryCounter : I
    //   6: istore_1
    //   7: aload_0
    //   8: iload_1
    //   9: iconst_1
    //   10: iadd
    //   11: putfield sCodeEntryCounter : I
    //   14: iload_1
    //   15: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   18: astore_2
    //   19: aload_2
    //   20: astore_3
    //   21: aload_2
    //   22: invokevirtual intValue : ()I
    //   25: i2l
    //   26: ldc2_w 262144
    //   29: lcmp
    //   30: iflt -> 54
    //   33: aload_0
    //   34: invokespecial restartSessionCounter : ()V
    //   37: aload_0
    //   38: getfield sCodeEntryCounter : I
    //   41: istore_1
    //   42: aload_0
    //   43: iload_1
    //   44: iconst_1
    //   45: iadd
    //   46: putfield sCodeEntryCounter : I
    //   49: iload_1
    //   50: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   53: astore_3
    //   54: aload_0
    //   55: aload_3
    //   56: invokevirtual intValue : ()I
    //   59: invokespecial getBase64Encoding : (I)Ljava/lang/String;
    //   62: astore_3
    //   63: aload_0
    //   64: monitorexit
    //   65: aload_3
    //   66: areturn
    //   67: astore_3
    //   68: aload_0
    //   69: monitorexit
    //   70: aload_3
    //   71: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #414	-> 2
    //   #415	-> 19
    //   #416	-> 33
    //   #417	-> 37
    //   #419	-> 54
    //   #413	-> 67
    // Exception table:
    //   from	to	target	type
    //   2	19	67	finally
    //   21	33	67	finally
    //   33	37	67	finally
    //   37	54	67	finally
    //   54	63	67	finally
  }
  
  private void restartSessionCounter() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iconst_0
    //   4: putfield sCodeEntryCounter : I
    //   7: aload_0
    //   8: monitorexit
    //   9: return
    //   10: astore_1
    //   11: aload_0
    //   12: monitorexit
    //   13: aload_1
    //   14: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #423	-> 2
    //   #424	-> 7
    //   #422	-> 10
    // Exception table:
    //   from	to	target	type
    //   2	7	10	finally
  }
  
  private String getBase64Encoding(int paramInt) {
    byte[] arrayOfByte = ByteBuffer.allocate(4).putInt(paramInt).array();
    arrayOfByte = Arrays.copyOfRange(arrayOfByte, 2, 4);
    return Base64.encodeToString(arrayOfByte, 3);
  }
  
  private int getCallingThreadId() {
    return this.mCurrentThreadId.get();
  }
  
  public String printActiveSessions() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: new java/lang/StringBuilder
    //   5: astore_1
    //   6: aload_1
    //   7: invokespecial <init> : ()V
    //   10: aload_0
    //   11: getfield mSessionMapper : Ljava/util/concurrent/ConcurrentHashMap;
    //   14: invokevirtual entrySet : ()Ljava/util/Set;
    //   17: invokeinterface iterator : ()Ljava/util/Iterator;
    //   22: astore_2
    //   23: aload_2
    //   24: invokeinterface hasNext : ()Z
    //   29: ifeq -> 70
    //   32: aload_2
    //   33: invokeinterface next : ()Ljava/lang/Object;
    //   38: checkcast java/util/Map$Entry
    //   41: astore_3
    //   42: aload_1
    //   43: aload_3
    //   44: invokeinterface getValue : ()Ljava/lang/Object;
    //   49: checkcast android/telecom/Logging/Session
    //   52: invokevirtual printFullSessionTree : ()Ljava/lang/String;
    //   55: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   58: pop
    //   59: aload_1
    //   60: ldc_w '\\n'
    //   63: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   66: pop
    //   67: goto -> 23
    //   70: aload_1
    //   71: invokevirtual toString : ()Ljava/lang/String;
    //   74: astore_1
    //   75: aload_0
    //   76: monitorexit
    //   77: aload_1
    //   78: areturn
    //   79: astore_1
    //   80: aload_0
    //   81: monitorexit
    //   82: aload_1
    //   83: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #442	-> 2
    //   #443	-> 10
    //   #444	-> 42
    //   #445	-> 59
    //   #446	-> 67
    //   #447	-> 70
    //   #441	-> 79
    // Exception table:
    //   from	to	target	type
    //   2	10	79	finally
    //   10	23	79	finally
    //   23	42	79	finally
    //   42	59	79	finally
    //   59	67	79	finally
    //   70	75	79	finally
  }
  
  public void cleanupStaleSessions(long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: ldc_w 'Stale Sessions Cleaned:\\n'
    //   5: astore_3
    //   6: iconst_0
    //   7: istore #4
    //   9: invokestatic currentTimeMillis : ()J
    //   12: lstore #5
    //   14: aload_0
    //   15: getfield mSessionMapper : Ljava/util/concurrent/ConcurrentHashMap;
    //   18: astore #7
    //   20: aload #7
    //   22: invokevirtual entrySet : ()Ljava/util/Set;
    //   25: invokeinterface iterator : ()Ljava/util/Iterator;
    //   30: astore #8
    //   32: aload #8
    //   34: invokeinterface hasNext : ()Z
    //   39: ifeq -> 142
    //   42: aload #8
    //   44: invokeinterface next : ()Ljava/lang/Object;
    //   49: checkcast java/util/Map$Entry
    //   52: astore #7
    //   54: aload #7
    //   56: invokeinterface getValue : ()Ljava/lang/Object;
    //   61: checkcast android/telecom/Logging/Session
    //   64: astore #9
    //   66: aload_3
    //   67: astore #7
    //   69: lload #5
    //   71: aload #9
    //   73: invokevirtual getExecutionStartTimeMilliseconds : ()J
    //   76: lsub
    //   77: lload_1
    //   78: lcmp
    //   79: ifle -> 136
    //   82: aload #8
    //   84: invokeinterface remove : ()V
    //   89: new java/lang/StringBuilder
    //   92: astore #7
    //   94: aload #7
    //   96: invokespecial <init> : ()V
    //   99: aload #7
    //   101: aload_3
    //   102: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   105: pop
    //   106: aload #7
    //   108: aload #9
    //   110: invokevirtual printFullSessionTree : ()Ljava/lang/String;
    //   113: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   116: pop
    //   117: aload #7
    //   119: ldc_w '\\n'
    //   122: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   125: pop
    //   126: aload #7
    //   128: invokevirtual toString : ()Ljava/lang/String;
    //   131: astore #7
    //   133: iconst_1
    //   134: istore #4
    //   136: aload #7
    //   138: astore_3
    //   139: goto -> 32
    //   142: iload #4
    //   144: ifeq -> 160
    //   147: ldc 'Logging'
    //   149: aload_3
    //   150: iconst_0
    //   151: anewarray java/lang/Object
    //   154: invokestatic w : (Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    //   157: goto -> 172
    //   160: ldc 'Logging'
    //   162: ldc_w 'No stale logging sessions needed to be cleaned...'
    //   165: iconst_0
    //   166: anewarray java/lang/Object
    //   169: invokestatic v : (Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    //   172: aload_0
    //   173: monitorexit
    //   174: return
    //   175: astore_3
    //   176: aload_0
    //   177: monitorexit
    //   178: aload_3
    //   179: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #452	-> 2
    //   #453	-> 6
    //   #454	-> 9
    //   #459	-> 14
    //   #460	-> 20
    //   #461	-> 42
    //   #462	-> 54
    //   #463	-> 66
    //   #464	-> 82
    //   #465	-> 89
    //   #466	-> 133
    //   #468	-> 136
    //   #469	-> 142
    //   #470	-> 147
    //   #472	-> 160
    //   #474	-> 172
    //   #451	-> 175
    // Exception table:
    //   from	to	target	type
    //   9	14	175	finally
    //   14	20	175	finally
    //   20	32	175	finally
    //   32	42	175	finally
    //   42	54	175	finally
    //   54	66	175	finally
    //   69	82	175	finally
    //   82	89	175	finally
    //   89	133	175	finally
    //   147	157	175	finally
    //   160	172	175	finally
  }
  
  private long getCleanupTimeout(Context paramContext) {
    return Settings.Secure.getLong(paramContext.getContentResolver(), "telecom.stale_session_cleanup_timeout_millis", 30000L);
  }
  
  public static interface ICurrentThreadId {
    int get();
  }
  
  private static interface ISessionCleanupTimeoutMs {
    long get();
  }
  
  public static interface ISessionIdQueryHandler {
    String getSessionId();
  }
  
  public static interface ISessionListener {
    void sessionComplete(String param1String, long param1Long);
  }
}
