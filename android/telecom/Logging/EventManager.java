package android.telecom.Logging;

import android.telecom.Log;
import android.text.TextUtils;
import android.util.Pair;
import com.android.internal.util.IndentingPrintWriter;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.IllegalFormatException;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.ToLongFunction;

public class EventManager {
  public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm:ss.SSS");
  
  private final Map<Loggable, EventRecord> mCallEventRecordMap = new HashMap<>();
  
  private LinkedBlockingQueue<EventRecord> mEventRecords = new LinkedBlockingQueue<>(10);
  
  private List<EventListener> mEventListeners = new ArrayList<>();
  
  private final Map<String, List<TimedEventPair>> requestResponsePairs = new HashMap<>();
  
  private static final Object mSync = new Object();
  
  public static final int DEFAULT_EVENTS_TO_CACHE = 10;
  
  public static final String TAG = "Logging.Events";
  
  private SessionManager.ISessionIdQueryHandler mSessionIdHandler;
  
  public static class TimedEventPair {
    private static final long DEFAULT_TIMEOUT = 3000L;
    
    String mName;
    
    String mRequest;
    
    String mResponse;
    
    long mTimeoutMillis = 3000L;
    
    public TimedEventPair(String param1String1, String param1String2, String param1String3) {
      this.mRequest = param1String1;
      this.mResponse = param1String2;
      this.mName = param1String3;
    }
    
    public TimedEventPair(String param1String1, String param1String2, String param1String3, long param1Long) {
      this.mRequest = param1String1;
      this.mResponse = param1String2;
      this.mName = param1String3;
      this.mTimeoutMillis = param1Long;
    }
  }
  
  public void addRequestResponsePair(TimedEventPair paramTimedEventPair) {
    if (this.requestResponsePairs.containsKey(paramTimedEventPair.mRequest)) {
      ((List<TimedEventPair>)this.requestResponsePairs.get(paramTimedEventPair.mRequest)).add(paramTimedEventPair);
    } else {
      ArrayList<TimedEventPair> arrayList = new ArrayList();
      arrayList.add(paramTimedEventPair);
      this.requestResponsePairs.put(paramTimedEventPair.mRequest, arrayList);
    } 
  }
  
  public static class Event {
    public Object data;
    
    public String eventId;
    
    public String sessionId;
    
    public long time;
    
    public final String timestampString;
    
    public Event(String param1String1, String param1String2, long param1Long, Object param1Object) {
      this.eventId = param1String1;
      this.sessionId = param1String2;
      this.time = param1Long;
      ZonedDateTime zonedDateTime = ZonedDateTime.ofInstant(Instant.ofEpochMilli(param1Long), ZoneId.systemDefault());
      DateTimeFormatter dateTimeFormatter = EventManager.DATE_TIME_FORMATTER;
      this.timestampString = zonedDateTime.format(dateTimeFormatter);
      this.data = param1Object;
    }
  }
  
  public class EventRecord {
    class EventTiming extends TimedEvent<String> {
      public String name;
      
      final EventManager.EventRecord this$1;
      
      public long time;
      
      public EventTiming(String param2String, long param2Long) {
        this.name = param2String;
        this.time = param2Long;
      }
      
      public String getKey() {
        return this.name;
      }
      
      public long getTime() {
        return this.time;
      }
    }
    
    private class PendingResponse {
      String name;
      
      String requestEventId;
      
      long requestEventTimeMillis;
      
      final EventManager.EventRecord this$1;
      
      long timeoutMillis;
      
      public PendingResponse(String param2String1, long param2Long1, long param2Long2, String param2String2) {
        this.requestEventId = param2String1;
        this.requestEventTimeMillis = param2Long1;
        this.timeoutMillis = param2Long2;
        this.name = param2String2;
      }
    }
    
    private final List<EventManager.Event> mEvents = Collections.synchronizedList(new LinkedList<>());
    
    private final EventManager.Loggable mRecordEntry;
    
    final EventManager this$0;
    
    public EventRecord(EventManager.Loggable param1Loggable) {
      this.mRecordEntry = param1Loggable;
    }
    
    public EventManager.Loggable getRecordEntry() {
      return this.mRecordEntry;
    }
    
    public void addEvent(String param1String1, String param1String2, Object param1Object) {
      this.mEvents.add(new EventManager.Event(param1String1, param1String2, System.currentTimeMillis(), param1Object));
      Log.i("Event", "RecordEntry %s: %s, %s", new Object[] { this.mRecordEntry.getId(), param1String1, param1Object });
    }
    
    public List<EventManager.Event> getEvents() {
      return new LinkedList<>(this.mEvents);
    }
    
    public List<EventTiming> extractEventTimings() {
      if (this.mEvents == null)
        return Collections.emptyList(); 
      LinkedList<EventTiming> linkedList = new LinkedList();
      HashMap<Object, Object> hashMap = new HashMap<>();
      synchronized (this.mEvents) {
        for (EventManager.Event event : this.mEvents) {
          if (EventManager.this.requestResponsePairs.containsKey(event.eventId))
            for (EventManager.TimedEventPair timedEventPair : EventManager.this.requestResponsePairs.get(event.eventId)) {
              String str = timedEventPair.mResponse;
              PendingResponse pendingResponse1 = new PendingResponse();
              this(this, event.eventId, event.time, timedEventPair.mTimeoutMillis, timedEventPair.mName);
              hashMap.put(str, pendingResponse1);
            }  
          PendingResponse pendingResponse = (PendingResponse)hashMap.remove(event.eventId);
          if (pendingResponse != null) {
            long l = event.time - pendingResponse.requestEventTimeMillis;
            if (l < pendingResponse.timeoutMillis) {
              EventTiming eventTiming = new EventTiming();
              this(this, pendingResponse.name, l);
              linkedList.add(eventTiming);
            } 
          } 
        } 
        return linkedList;
      } 
    }
    
    public void dump(IndentingPrintWriter param1IndentingPrintWriter) {
      param1IndentingPrintWriter.print(this.mRecordEntry.getDescription());
      param1IndentingPrintWriter.increaseIndent();
      for (EventManager.Event event : getEvents()) {
        param1IndentingPrintWriter.print(event.timestampString);
        param1IndentingPrintWriter.print(" - ");
        param1IndentingPrintWriter.print(event.eventId);
        if (event.data != null) {
          param1IndentingPrintWriter.print(" (");
          Object object1 = event.data;
          Object object2 = object1;
          if (object1 instanceof EventManager.Loggable) {
            EventRecord eventRecord = (EventRecord)EventManager.this.mCallEventRecordMap.get(object1);
            object2 = object1;
            if (eventRecord != null) {
              object2 = new StringBuilder();
              object2.append("RecordEntry ");
              object2.append(eventRecord.mRecordEntry.getId());
              object2 = object2.toString();
            } 
          } 
          param1IndentingPrintWriter.print(object2);
          param1IndentingPrintWriter.print(")");
        } 
        if (!TextUtils.isEmpty(event.sessionId)) {
          param1IndentingPrintWriter.print(":");
          param1IndentingPrintWriter.print(event.sessionId);
        } 
        param1IndentingPrintWriter.println();
      } 
      param1IndentingPrintWriter.println("Timings (average for this call, milliseconds):");
      param1IndentingPrintWriter.increaseIndent();
      Map<String, Double> map = EventTiming.averageTimings((Collection)extractEventTimings());
      ArrayList<Comparable> arrayList = new ArrayList(map.keySet());
      Collections.sort(arrayList);
      for (String str : arrayList) {
        param1IndentingPrintWriter.printf("%s: %.2f\n", new Object[] { str, map.get(str) });
      } 
      param1IndentingPrintWriter.decreaseIndent();
      param1IndentingPrintWriter.decreaseIndent();
    }
  }
  
  class EventTiming extends TimedEvent<String> {
    public String name;
    
    final EventManager.EventRecord this$1;
    
    public long time;
    
    public EventTiming(String param1String, long param1Long) {
      this.name = param1String;
      this.time = param1Long;
    }
    
    public String getKey() {
      return this.name;
    }
    
    public long getTime() {
      return this.time;
    }
  }
  
  private class PendingResponse {
    String name;
    
    String requestEventId;
    
    long requestEventTimeMillis;
    
    final EventManager.EventRecord this$1;
    
    long timeoutMillis;
    
    public PendingResponse(String param1String1, long param1Long1, long param1Long2, String param1String2) {
      this.requestEventId = param1String1;
      this.requestEventTimeMillis = param1Long1;
      this.timeoutMillis = param1Long2;
      this.name = param1String2;
    }
  }
  
  public EventManager(SessionManager.ISessionIdQueryHandler paramISessionIdQueryHandler) {
    this.mSessionIdHandler = paramISessionIdQueryHandler;
  }
  
  public void event(Loggable paramLoggable, String paramString, Object paramObject) {
    String str = this.mSessionIdHandler.getSessionId();
    if (paramLoggable == null) {
      Log.i("Logging.Events", "Non-call EVENT: %s, %s", new Object[] { paramString, paramObject });
      return;
    } 
    synchronized (this.mEventRecords) {
      if (!this.mCallEventRecordMap.containsKey(paramLoggable)) {
        EventRecord eventRecord1 = new EventRecord();
        this(this, paramLoggable);
        addEventRecord(eventRecord1);
      } 
      EventRecord eventRecord = this.mCallEventRecordMap.get(paramLoggable);
      eventRecord.addEvent(paramString, str, paramObject);
      return;
    } 
  }
  
  public void event(Loggable paramLoggable, String paramString1, String paramString2, Object... paramVarArgs) {
    if (paramVarArgs != null)
      try {
        if (paramVarArgs.length != 0)
          String str = String.format(Locale.US, paramString2, paramVarArgs); 
      } catch (IllegalFormatException illegalFormatException) {
        int i = paramVarArgs.length;
        Log.e(this, illegalFormatException, "IllegalFormatException: formatString='%s' numArgs=%d", new Object[] { paramString2, Integer.valueOf(i) });
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(paramString2);
        stringBuilder.append(" (An error occurred while formatting the message.)");
        paramString2 = stringBuilder.toString();
      }  
    event(paramLoggable, paramString1, paramString2);
  }
  
  public void dumpEvents(IndentingPrintWriter paramIndentingPrintWriter) {
    paramIndentingPrintWriter.println("Historical Events:");
    paramIndentingPrintWriter.increaseIndent();
    for (EventRecord eventRecord : this.mEventRecords)
      eventRecord.dump(paramIndentingPrintWriter); 
    paramIndentingPrintWriter.decreaseIndent();
  }
  
  public void dumpEventsTimeline(IndentingPrintWriter paramIndentingPrintWriter) {
    paramIndentingPrintWriter.println("Historical Events (sorted by time):");
    ArrayList<?> arrayList = new ArrayList();
    for (EventRecord eventRecord : this.mEventRecords) {
      for (Event event : eventRecord.getEvents())
        arrayList.add(new Pair<>(eventRecord.getRecordEntry(), event)); 
    } 
    -$.Lambda.EventManager.weOtitr8e1cZeiy1aDSqzNoKaY8 weOtitr8e1cZeiy1aDSqzNoKaY8 = _$$Lambda$EventManager$weOtitr8e1cZeiy1aDSqzNoKaY8.INSTANCE;
    Comparator<?> comparator = Comparator.comparingLong((ToLongFunction<?>)weOtitr8e1cZeiy1aDSqzNoKaY8);
    arrayList.sort(comparator);
    paramIndentingPrintWriter.increaseIndent();
    for (Pair pair : arrayList) {
      paramIndentingPrintWriter.print(((Event)pair.second).timestampString);
      paramIndentingPrintWriter.print(",");
      paramIndentingPrintWriter.print(((Loggable)pair.first).getId());
      paramIndentingPrintWriter.print(",");
      paramIndentingPrintWriter.print(((Event)pair.second).eventId);
      paramIndentingPrintWriter.print(",");
      paramIndentingPrintWriter.println(((Event)pair.second).data);
    } 
    paramIndentingPrintWriter.decreaseIndent();
  }
  
  public void changeEventCacheSize(int paramInt) {
    LinkedBlockingQueue<EventRecord> linkedBlockingQueue = this.mEventRecords;
    this.mEventRecords = new LinkedBlockingQueue<>(paramInt);
    this.mCallEventRecordMap.clear();
    linkedBlockingQueue.forEach(new _$$Lambda$EventManager$uddp6XAJ90VBwdTiuzBdSYelntQ(this));
  }
  
  public void registerEventListener(EventListener paramEventListener) {
    if (paramEventListener != null)
      synchronized (mSync) {
        this.mEventListeners.add(paramEventListener);
      }  
  }
  
  public LinkedBlockingQueue<EventRecord> getEventRecords() {
    return this.mEventRecords;
  }
  
  public Map<Loggable, EventRecord> getCallEventRecordMap() {
    return this.mCallEventRecordMap;
  }
  
  private void addEventRecord(EventRecord paramEventRecord) {
    Loggable loggable = paramEventRecord.getRecordEntry();
    if (this.mEventRecords.remainingCapacity() == 0) {
      EventRecord eventRecord = this.mEventRecords.poll();
      if (eventRecord != null)
        this.mCallEventRecordMap.remove(eventRecord.getRecordEntry()); 
    } 
    this.mEventRecords.add(paramEventRecord);
    this.mCallEventRecordMap.put(loggable, paramEventRecord);
    synchronized (mSync) {
      for (EventListener eventListener : this.mEventListeners)
        eventListener.eventRecordAdded(paramEventRecord); 
      return;
    } 
  }
  
  public static interface EventListener {
    void eventRecordAdded(EventManager.EventRecord param1EventRecord);
  }
  
  public static interface Loggable {
    String getDescription();
    
    String getId();
  }
}
