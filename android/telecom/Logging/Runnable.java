package android.telecom.Logging;

import android.telecom.Log;

public abstract class Runnable {
  private final Object mLock;
  
  private final java.lang.Runnable mRunnable = new java.lang.Runnable() {
      final Runnable this$0;
      
      public void run() {
        Object object = Runnable.this.mLock;
        /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
        try {
          Log.continueSession(Runnable.this.mSubsession, Runnable.this.mSubsessionName);
          Runnable.this.loggedRun();
        } finally {
          if (Runnable.this.mSubsession != null) {
            Log.endSession();
            Runnable.access$102(Runnable.this, null);
          } 
        } 
        /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
        throw SYNTHETIC_LOCAL_VARIABLE_2;
      }
    };
  
  private Session mSubsession;
  
  private final String mSubsessionName;
  
  public Runnable(String paramString, Object paramObject) {
    if (paramObject == null) {
      this.mLock = new Object();
    } else {
      this.mLock = paramObject;
    } 
    this.mSubsessionName = paramString;
  }
  
  public final java.lang.Runnable getRunnableToCancel() {
    return this.mRunnable;
  }
  
  public java.lang.Runnable prepare() {
    cancel();
    this.mSubsession = Log.createSubsession();
    return this.mRunnable;
  }
  
  public void cancel() {
    synchronized (this.mLock) {
      Log.cancelSubsession(this.mSubsession);
      this.mSubsession = null;
      return;
    } 
  }
  
  public abstract void loggedRun();
}
