package android.telecom;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.NetworkErrorException;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;

public class AuthenticatorService extends Service {
  private static Authenticator mAuthenticator;
  
  public void onCreate() {
    mAuthenticator = new Authenticator((Context)this);
  }
  
  public IBinder onBind(Intent paramIntent) {
    return mAuthenticator.getIBinder();
  }
  
  class Authenticator extends AbstractAccountAuthenticator {
    final AuthenticatorService this$0;
    
    public Authenticator(Context param1Context) {
      super(param1Context);
    }
    
    public Bundle editProperties(AccountAuthenticatorResponse param1AccountAuthenticatorResponse, String param1String) {
      throw new UnsupportedOperationException();
    }
    
    public Bundle addAccount(AccountAuthenticatorResponse param1AccountAuthenticatorResponse, String param1String1, String param1String2, String[] param1ArrayOfString, Bundle param1Bundle) throws NetworkErrorException {
      return null;
    }
    
    public Bundle confirmCredentials(AccountAuthenticatorResponse param1AccountAuthenticatorResponse, Account param1Account, Bundle param1Bundle) throws NetworkErrorException {
      return null;
    }
    
    public Bundle getAuthToken(AccountAuthenticatorResponse param1AccountAuthenticatorResponse, Account param1Account, String param1String, Bundle param1Bundle) throws NetworkErrorException {
      throw new UnsupportedOperationException();
    }
    
    public String getAuthTokenLabel(String param1String) {
      throw new UnsupportedOperationException();
    }
    
    public Bundle updateCredentials(AccountAuthenticatorResponse param1AccountAuthenticatorResponse, Account param1Account, String param1String, Bundle param1Bundle) throws NetworkErrorException {
      throw new UnsupportedOperationException();
    }
    
    public Bundle hasFeatures(AccountAuthenticatorResponse param1AccountAuthenticatorResponse, Account param1Account, String[] param1ArrayOfString) throws NetworkErrorException {
      throw new UnsupportedOperationException();
    }
  }
}
