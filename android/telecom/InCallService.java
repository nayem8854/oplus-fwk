package android.telecom;

import android.annotation.SystemApi;
import android.app.Service;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.view.Surface;
import com.android.internal.os.SomeArgs;
import com.android.internal.telecom.IInCallAdapter;
import com.android.internal.telecom.IInCallService;
import java.util.Collections;
import java.util.List;

public abstract class InCallService extends Service {
  private static final int MSG_ADD_CALL = 2;
  
  private static final int MSG_BRING_TO_FOREGROUND = 6;
  
  private static final int MSG_ON_CALL_AUDIO_STATE_CHANGED = 5;
  
  private static final int MSG_ON_CAN_ADD_CALL_CHANGED = 7;
  
  private static final int MSG_ON_CONNECTION_EVENT = 9;
  
  private static final int MSG_ON_HANDOVER_COMPLETE = 13;
  
  private static final int MSG_ON_HANDOVER_FAILED = 12;
  
  private static final int MSG_ON_RTT_INITIATION_FAILURE = 11;
  
  private static final int MSG_ON_RTT_UPGRADE_REQUEST = 10;
  
  private static final int MSG_SET_IN_CALL_ADAPTER = 1;
  
  private static final int MSG_SET_POST_DIAL_WAIT = 4;
  
  private static final int MSG_SILENCE_RINGER = 8;
  
  private static final int MSG_UPDATE_CALL = 3;
  
  public static final String SERVICE_INTERFACE = "android.telecom.InCallService";
  
  private final String LOG_TAG_FROM;
  
  private final Handler mHandler = (Handler)new Object(this, Looper.getMainLooper());
  
  private Phone mPhone;
  
  private final class InCallServiceBinder extends IInCallService.Stub {
    final InCallService this$0;
    
    private InCallServiceBinder() {}
    
    public void setInCallAdapter(IInCallAdapter param1IInCallAdapter) {
      InCallService.this.logDebug("InCallServiceBinder setInCallAdapter");
      InCallService.this.mHandler.obtainMessage(1, param1IInCallAdapter).sendToTarget();
    }
    
    public void addCall(ParcelableCall param1ParcelableCall) {
      InCallService.this.mHandler.obtainMessage(2, param1ParcelableCall).sendToTarget();
    }
    
    public void updateCall(ParcelableCall param1ParcelableCall) {
      InCallService.this.mHandler.obtainMessage(3, param1ParcelableCall).sendToTarget();
    }
    
    public void setPostDial(String param1String1, String param1String2) {}
    
    public void setPostDialWait(String param1String1, String param1String2) {
      SomeArgs someArgs = SomeArgs.obtain();
      someArgs.arg1 = param1String1;
      someArgs.arg2 = param1String2;
      InCallService.this.mHandler.obtainMessage(4, someArgs).sendToTarget();
    }
    
    public void onCallAudioStateChanged(CallAudioState param1CallAudioState) {
      InCallService.this.mHandler.obtainMessage(5, param1CallAudioState).sendToTarget();
    }
    
    public void bringToForeground(boolean param1Boolean) {
      InCallService.this.mHandler.obtainMessage(6, param1Boolean, 0).sendToTarget();
    }
    
    public void onCanAddCallChanged(boolean param1Boolean) {
      Message message = InCallService.this.mHandler.obtainMessage(7, param1Boolean, 0);
      message.sendToTarget();
    }
    
    public void silenceRinger() {
      InCallService.this.mHandler.obtainMessage(8).sendToTarget();
    }
    
    public void onConnectionEvent(String param1String1, String param1String2, Bundle param1Bundle) {
      SomeArgs someArgs = SomeArgs.obtain();
      someArgs.arg1 = param1String1;
      someArgs.arg2 = param1String2;
      someArgs.arg3 = param1Bundle;
      InCallService.this.mHandler.obtainMessage(9, someArgs).sendToTarget();
    }
    
    public void onRttUpgradeRequest(String param1String, int param1Int) {
      InCallService.this.mHandler.obtainMessage(10, param1Int, 0, param1String).sendToTarget();
    }
    
    public void onRttInitiationFailure(String param1String, int param1Int) {
      InCallService.this.mHandler.obtainMessage(11, param1Int, 0, param1String).sendToTarget();
    }
    
    public void onHandoverFailed(String param1String, int param1Int) {
      InCallService.this.mHandler.obtainMessage(12, param1Int, 0, param1String).sendToTarget();
    }
    
    public void onHandoverComplete(String param1String) {
      InCallService.this.mHandler.obtainMessage(13, param1String).sendToTarget();
    }
  }
  
  private Phone.Listener mPhoneListener = (Phone.Listener)new Object(this);
  
  public IBinder onBind(Intent paramIntent) {
    return (IBinder)new InCallServiceBinder();
  }
  
  public boolean onUnbind(Intent paramIntent) {
    logDebug("onUnbind");
    if (this.mPhone != null) {
      Phone phone = this.mPhone;
      this.mPhone = null;
      phone.destroy();
      phone.removeListener(this.mPhoneListener);
      onPhoneDestroyed(phone);
    } 
    return false;
  }
  
  @SystemApi
  @Deprecated
  public Phone getPhone() {
    return this.mPhone;
  }
  
  public final List<Call> getCalls() {
    List<?> list;
    Phone phone = this.mPhone;
    if (phone == null) {
      list = Collections.emptyList();
    } else {
      list = list.getCalls();
    } 
    return (List)list;
  }
  
  public final boolean canAddCall() {
    boolean bool;
    Phone phone = this.mPhone;
    if (phone == null) {
      bool = false;
    } else {
      bool = phone.canAddCall();
    } 
    return bool;
  }
  
  @Deprecated
  public final AudioState getAudioState() {
    AudioState audioState;
    Phone phone = this.mPhone;
    if (phone == null) {
      phone = null;
    } else {
      audioState = phone.getAudioState();
    } 
    return audioState;
  }
  
  public final CallAudioState getCallAudioState() {
    CallAudioState callAudioState;
    Phone phone = this.mPhone;
    if (phone == null) {
      phone = null;
    } else {
      callAudioState = phone.getCallAudioState();
    } 
    return callAudioState;
  }
  
  public final void setMuted(boolean paramBoolean) {
    Phone phone = this.mPhone;
    if (phone != null)
      phone.setMuted(paramBoolean); 
  }
  
  public final void setAudioRoute(int paramInt) {
    Phone phone = this.mPhone;
    if (phone != null)
      phone.setAudioRoute(paramInt); 
  }
  
  public final void requestBluetoothAudio(BluetoothDevice paramBluetoothDevice) {
    Phone phone = this.mPhone;
    if (phone != null)
      phone.requestBluetoothAudio(paramBluetoothDevice.getAddress()); 
  }
  
  @SystemApi
  @Deprecated
  public void onPhoneCreated(Phone paramPhone) {}
  
  @SystemApi
  @Deprecated
  public void onPhoneDestroyed(Phone paramPhone) {}
  
  @Deprecated
  public void onAudioStateChanged(AudioState paramAudioState) {}
  
  public void onCallAudioStateChanged(CallAudioState paramCallAudioState) {}
  
  public void onBringToForeground(boolean paramBoolean) {}
  
  public void onCallAdded(Call paramCall) {}
  
  public void onCallRemoved(Call paramCall) {}
  
  public void onCanAddCallChanged(boolean paramBoolean) {}
  
  public void onSilenceRinger() {}
  
  public void onConnectionEvent(Call paramCall, String paramString, Bundle paramBundle) {}
  
  class VideoCall {
    public abstract void destroy();
    
    public abstract void registerCallback(Callback param1Callback);
    
    public abstract void registerCallback(Callback param1Callback, Handler param1Handler);
    
    public abstract void requestCallDataUsage();
    
    public abstract void requestCameraCapabilities();
    
    public abstract void sendSessionModifyRequest(VideoProfile param1VideoProfile);
    
    public abstract void sendSessionModifyResponse(VideoProfile param1VideoProfile);
    
    public abstract void setCamera(String param1String);
    
    public abstract void setDeviceOrientation(int param1Int);
    
    public abstract void setDisplaySurface(Surface param1Surface);
    
    public abstract void setPauseImage(Uri param1Uri);
    
    public abstract void setPreviewSurface(Surface param1Surface);
    
    public abstract void setZoom(float param1Float);
    
    public abstract void unregisterCallback(Callback param1Callback);
    
    public static abstract class Callback {
      public abstract void onCallDataUsageChanged(long param2Long);
      
      public abstract void onCallSessionEvent(int param2Int);
      
      public abstract void onCameraCapabilitiesChanged(VideoProfile.CameraCapabilities param2CameraCapabilities);
      
      public abstract void onPeerDimensionsChanged(int param2Int1, int param2Int2);
      
      public abstract void onSessionModifyRequestReceived(VideoProfile param2VideoProfile);
      
      public abstract void onSessionModifyResponseReceived(int param2Int, VideoProfile param2VideoProfile1, VideoProfile param2VideoProfile2);
      
      public abstract void onVideoQualityChanged(int param2Int);
    }
  }
  
  public InCallService() {
    this.LOG_TAG_FROM = "IInCallService-->";
  }
  
  void logDebug(String paramString) {
    Log.i("IInCallService-->", paramString, new Object[0]);
  }
}
