package android.telecom;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@SystemApi
public class ParcelableCallAnalytics implements Parcelable {
  public static final class VideoEvent implements Parcelable {
    public static final Parcelable.Creator<VideoEvent> CREATOR = new Parcelable.Creator<VideoEvent>() {
        public ParcelableCallAnalytics.VideoEvent createFromParcel(Parcel param2Parcel) {
          return new ParcelableCallAnalytics.VideoEvent(param2Parcel);
        }
        
        public ParcelableCallAnalytics.VideoEvent[] newArray(int param2Int) {
          return new ParcelableCallAnalytics.VideoEvent[param2Int];
        }
      };
    
    public static final int RECEIVE_REMOTE_SESSION_MODIFY_REQUEST = 2;
    
    public static final int RECEIVE_REMOTE_SESSION_MODIFY_RESPONSE = 3;
    
    public static final int SEND_LOCAL_SESSION_MODIFY_REQUEST = 0;
    
    public static final int SEND_LOCAL_SESSION_MODIFY_RESPONSE = 1;
    
    private int mEventName;
    
    private long mTimeSinceLastEvent;
    
    private int mVideoState;
    
    public VideoEvent(int param1Int1, long param1Long, int param1Int2) {
      this.mEventName = param1Int1;
      this.mTimeSinceLastEvent = param1Long;
      this.mVideoState = param1Int2;
    }
    
    VideoEvent(Parcel param1Parcel) {
      this.mEventName = param1Parcel.readInt();
      this.mTimeSinceLastEvent = param1Parcel.readLong();
      this.mVideoState = param1Parcel.readInt();
    }
    
    public int getEventName() {
      return this.mEventName;
    }
    
    public long getTimeSinceLastEvent() {
      return this.mTimeSinceLastEvent;
    }
    
    public int getVideoState() {
      return this.mVideoState;
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.mEventName);
      param1Parcel.writeLong(this.mTimeSinceLastEvent);
      param1Parcel.writeInt(this.mVideoState);
    }
  }
  
  class null implements Parcelable.Creator<VideoEvent> {
    public ParcelableCallAnalytics.VideoEvent createFromParcel(Parcel param1Parcel) {
      return new ParcelableCallAnalytics.VideoEvent(param1Parcel);
    }
    
    public ParcelableCallAnalytics.VideoEvent[] newArray(int param1Int) {
      return new ParcelableCallAnalytics.VideoEvent[param1Int];
    }
  }
  
  public static final class AnalyticsEvent implements Parcelable {
    public static final int AUDIO_ROUTE_BT = 204;
    
    public static final int AUDIO_ROUTE_EARPIECE = 205;
    
    public static final int AUDIO_ROUTE_HEADSET = 206;
    
    public static final int AUDIO_ROUTE_SPEAKER = 207;
    
    public static final int BIND_CS = 5;
    
    public static final int BLOCK_CHECK_FINISHED = 105;
    
    public static final int BLOCK_CHECK_INITIATED = 104;
    
    public static final int CONFERENCE_WITH = 300;
    
    public static final Parcelable.Creator<AnalyticsEvent> CREATOR = new Parcelable.Creator<AnalyticsEvent>() {
        public ParcelableCallAnalytics.AnalyticsEvent createFromParcel(Parcel param2Parcel) {
          return new ParcelableCallAnalytics.AnalyticsEvent(param2Parcel);
        }
        
        public ParcelableCallAnalytics.AnalyticsEvent[] newArray(int param2Int) {
          return new ParcelableCallAnalytics.AnalyticsEvent[param2Int];
        }
      };
    
    public static final int CS_BOUND = 6;
    
    public static final int DIRECT_TO_VM_FINISHED = 103;
    
    public static final int DIRECT_TO_VM_INITIATED = 102;
    
    public static final int FILTERING_COMPLETED = 107;
    
    public static final int FILTERING_INITIATED = 106;
    
    public static final int FILTERING_TIMED_OUT = 108;
    
    public static final int MUTE = 202;
    
    public static final int REMOTELY_HELD = 402;
    
    public static final int REMOTELY_UNHELD = 403;
    
    public static final int REQUEST_ACCEPT = 7;
    
    public static final int REQUEST_HOLD = 400;
    
    public static final int REQUEST_PULL = 500;
    
    public static final int REQUEST_REJECT = 8;
    
    public static final int REQUEST_UNHOLD = 401;
    
    public static final int SCREENING_COMPLETED = 101;
    
    public static final int SCREENING_SENT = 100;
    
    public static final int SET_ACTIVE = 1;
    
    public static final int SET_DIALING = 4;
    
    public static final int SET_DISCONNECTED = 2;
    
    public static final int SET_HOLD = 404;
    
    public static final int SET_PARENT = 302;
    
    public static final int SET_SELECT_PHONE_ACCOUNT = 0;
    
    public static final int SILENCE = 201;
    
    public static final int SKIP_RINGING = 200;
    
    public static final int SPLIT_CONFERENCE = 301;
    
    public static final int START_CONNECTION = 3;
    
    public static final int SWAP = 405;
    
    public static final int UNMUTE = 203;
    
    private int mEventName;
    
    private long mTimeSinceLastEvent;
    
    public AnalyticsEvent(int param1Int, long param1Long) {
      this.mEventName = param1Int;
      this.mTimeSinceLastEvent = param1Long;
    }
    
    AnalyticsEvent(Parcel param1Parcel) {
      this.mEventName = param1Parcel.readInt();
      this.mTimeSinceLastEvent = param1Parcel.readLong();
    }
    
    public int getEventName() {
      return this.mEventName;
    }
    
    public long getTimeSinceLastEvent() {
      return this.mTimeSinceLastEvent;
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.mEventName);
      param1Parcel.writeLong(this.mTimeSinceLastEvent);
    }
  }
  
  class null implements Parcelable.Creator<AnalyticsEvent> {
    public ParcelableCallAnalytics.AnalyticsEvent createFromParcel(Parcel param1Parcel) {
      return new ParcelableCallAnalytics.AnalyticsEvent(param1Parcel);
    }
    
    public ParcelableCallAnalytics.AnalyticsEvent[] newArray(int param1Int) {
      return new ParcelableCallAnalytics.AnalyticsEvent[param1Int];
    }
  }
  
  public static final class EventTiming implements Parcelable {
    public static final int ACCEPT_TIMING = 0;
    
    public static final int BIND_CS_TIMING = 6;
    
    public static final int BLOCK_CHECK_FINISHED_TIMING = 9;
    
    public static final Parcelable.Creator<EventTiming> CREATOR = new Parcelable.Creator<EventTiming>() {
        public ParcelableCallAnalytics.EventTiming createFromParcel(Parcel param2Parcel) {
          return new ParcelableCallAnalytics.EventTiming(param2Parcel);
        }
        
        public ParcelableCallAnalytics.EventTiming[] newArray(int param2Int) {
          return new ParcelableCallAnalytics.EventTiming[param2Int];
        }
      };
    
    public static final int DIRECT_TO_VM_FINISHED_TIMING = 8;
    
    public static final int DISCONNECT_TIMING = 2;
    
    public static final int FILTERING_COMPLETED_TIMING = 10;
    
    public static final int FILTERING_TIMED_OUT_TIMING = 11;
    
    public static final int HOLD_TIMING = 3;
    
    public static final int INVALID = 999999;
    
    public static final int OUTGOING_TIME_TO_DIALING_TIMING = 5;
    
    public static final int REJECT_TIMING = 1;
    
    public static final int SCREENING_COMPLETED_TIMING = 7;
    
    public static final int START_CONNECTION_TO_REQUEST_DISCONNECT_TIMING = 12;
    
    public static final int UNHOLD_TIMING = 4;
    
    private int mName;
    
    private long mTime;
    
    public EventTiming(int param1Int, long param1Long) {
      this.mName = param1Int;
      this.mTime = param1Long;
    }
    
    private EventTiming(Parcel param1Parcel) {
      this.mName = param1Parcel.readInt();
      this.mTime = param1Parcel.readLong();
    }
    
    public int getName() {
      return this.mName;
    }
    
    public long getTime() {
      return this.mTime;
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.mName);
      param1Parcel.writeLong(this.mTime);
    }
  }
  
  class null implements Parcelable.Creator<EventTiming> {
    public ParcelableCallAnalytics.EventTiming createFromParcel(Parcel param1Parcel) {
      return new ParcelableCallAnalytics.EventTiming(param1Parcel);
    }
    
    public ParcelableCallAnalytics.EventTiming[] newArray(int param1Int) {
      return new ParcelableCallAnalytics.EventTiming[param1Int];
    }
  }
  
  public static final Parcelable.Creator<ParcelableCallAnalytics> CREATOR = new Parcelable.Creator<ParcelableCallAnalytics>() {
      public ParcelableCallAnalytics createFromParcel(Parcel param1Parcel) {
        return new ParcelableCallAnalytics(param1Parcel);
      }
      
      public ParcelableCallAnalytics[] newArray(int param1Int) {
        return new ParcelableCallAnalytics[param1Int];
      }
    };
  
  private boolean isVideoCall = false;
  
  private int callSource = 0;
  
  public static final int CALLTYPE_INCOMING = 1;
  
  public static final int CALLTYPE_OUTGOING = 2;
  
  public static final int CALLTYPE_UNKNOWN = 0;
  
  public static final int CDMA_PHONE = 1;
  
  public static final int GSM_PHONE = 2;
  
  public static final int IMS_PHONE = 4;
  
  public static final long MILLIS_IN_1_SECOND = 1000L;
  
  public static final long MILLIS_IN_5_MINUTES = 300000L;
  
  public static final int SIP_PHONE = 8;
  
  public static final int STILL_CONNECTED = -1;
  
  public static final int THIRD_PARTY_PHONE = 16;
  
  private final List<AnalyticsEvent> analyticsEvents;
  
  private final long callDurationMillis;
  
  private final int callTechnologies;
  
  private final int callTerminationCode;
  
  private final int callType;
  
  private final String connectionService;
  
  private final List<EventTiming> eventTimings;
  
  private final boolean isAdditionalCall;
  
  private final boolean isCreatedFromExistingConnection;
  
  private final boolean isEmergencyCall;
  
  private final boolean isInterrupted;
  
  private final long startTimeMillis;
  
  private List<VideoEvent> videoEvents;
  
  public ParcelableCallAnalytics(long paramLong1, long paramLong2, int paramInt1, boolean paramBoolean1, boolean paramBoolean2, int paramInt2, int paramInt3, boolean paramBoolean3, String paramString, boolean paramBoolean4, List<AnalyticsEvent> paramList, List<EventTiming> paramList1) {
    this.startTimeMillis = paramLong1;
    this.callDurationMillis = paramLong2;
    this.callType = paramInt1;
    this.isAdditionalCall = paramBoolean1;
    this.isInterrupted = paramBoolean2;
    this.callTechnologies = paramInt2;
    this.callTerminationCode = paramInt3;
    this.isEmergencyCall = paramBoolean3;
    this.connectionService = paramString;
    this.isCreatedFromExistingConnection = paramBoolean4;
    this.analyticsEvents = paramList;
    this.eventTimings = paramList1;
  }
  
  public ParcelableCallAnalytics(Parcel paramParcel) {
    this.startTimeMillis = paramParcel.readLong();
    this.callDurationMillis = paramParcel.readLong();
    this.callType = paramParcel.readInt();
    this.isAdditionalCall = readByteAsBoolean(paramParcel);
    this.isInterrupted = readByteAsBoolean(paramParcel);
    this.callTechnologies = paramParcel.readInt();
    this.callTerminationCode = paramParcel.readInt();
    this.isEmergencyCall = readByteAsBoolean(paramParcel);
    this.connectionService = paramParcel.readString();
    this.isCreatedFromExistingConnection = readByteAsBoolean(paramParcel);
    ArrayList<AnalyticsEvent> arrayList = new ArrayList();
    paramParcel.readTypedList(arrayList, AnalyticsEvent.CREATOR);
    this.eventTimings = (List)(arrayList = new ArrayList<>());
    paramParcel.readTypedList(arrayList, EventTiming.CREATOR);
    this.isVideoCall = readByteAsBoolean(paramParcel);
    LinkedList<VideoEvent> linkedList = new LinkedList();
    paramParcel.readTypedList(linkedList, VideoEvent.CREATOR);
    this.callSource = paramParcel.readInt();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeLong(this.startTimeMillis);
    paramParcel.writeLong(this.callDurationMillis);
    paramParcel.writeInt(this.callType);
    writeBooleanAsByte(paramParcel, this.isAdditionalCall);
    writeBooleanAsByte(paramParcel, this.isInterrupted);
    paramParcel.writeInt(this.callTechnologies);
    paramParcel.writeInt(this.callTerminationCode);
    writeBooleanAsByte(paramParcel, this.isEmergencyCall);
    paramParcel.writeString(this.connectionService);
    writeBooleanAsByte(paramParcel, this.isCreatedFromExistingConnection);
    paramParcel.writeTypedList(this.analyticsEvents);
    paramParcel.writeTypedList(this.eventTimings);
    writeBooleanAsByte(paramParcel, this.isVideoCall);
    paramParcel.writeTypedList(this.videoEvents);
    paramParcel.writeInt(this.callSource);
  }
  
  public void setIsVideoCall(boolean paramBoolean) {
    this.isVideoCall = paramBoolean;
  }
  
  public void setVideoEvents(List<VideoEvent> paramList) {
    this.videoEvents = paramList;
  }
  
  public void setCallSource(int paramInt) {
    this.callSource = paramInt;
  }
  
  public long getStartTimeMillis() {
    return this.startTimeMillis;
  }
  
  public long getCallDurationMillis() {
    return this.callDurationMillis;
  }
  
  public int getCallType() {
    return this.callType;
  }
  
  public boolean isAdditionalCall() {
    return this.isAdditionalCall;
  }
  
  public boolean isInterrupted() {
    return this.isInterrupted;
  }
  
  public int getCallTechnologies() {
    return this.callTechnologies;
  }
  
  public int getCallTerminationCode() {
    return this.callTerminationCode;
  }
  
  public boolean isEmergencyCall() {
    return this.isEmergencyCall;
  }
  
  public String getConnectionService() {
    return this.connectionService;
  }
  
  public boolean isCreatedFromExistingConnection() {
    return this.isCreatedFromExistingConnection;
  }
  
  public List<AnalyticsEvent> analyticsEvents() {
    return this.analyticsEvents;
  }
  
  public List<EventTiming> getEventTimings() {
    return this.eventTimings;
  }
  
  public boolean isVideoCall() {
    return this.isVideoCall;
  }
  
  public List<VideoEvent> getVideoEvents() {
    return this.videoEvents;
  }
  
  public int getCallSource() {
    return this.callSource;
  }
  
  public int describeContents() {
    return 0;
  }
  
  private static void writeBooleanAsByte(Parcel paramParcel, boolean paramBoolean) {
    paramParcel.writeByte((byte)paramBoolean);
  }
  
  private static boolean readByteAsBoolean(Parcel paramParcel) {
    byte b = paramParcel.readByte();
    boolean bool = true;
    if (b != 1)
      bool = false; 
    return bool;
  }
}
