package android.telecom;

import android.annotation.SystemApi;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.UserHandle;
import android.telephony.SubscriptionManager;
import android.text.TextUtils;
import android.util.Log;
import com.android.internal.telecom.ITelecomService;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TelecomManager implements IOplusTelecomManagerEx {
  public static final String ACTION_CALL_TYPE = "codeaurora.telecom.action.CALL_TYPE";
  
  public static final String ACTION_CHANGE_DEFAULT_DIALER = "android.telecom.action.CHANGE_DEFAULT_DIALER";
  
  public static final String ACTION_CHANGE_PHONE_ACCOUNTS = "android.telecom.action.CHANGE_PHONE_ACCOUNTS";
  
  public static final String ACTION_CONFIGURE_PHONE_ACCOUNT = "android.telecom.action.CONFIGURE_PHONE_ACCOUNT";
  
  public static final String ACTION_CURRENT_TTY_MODE_CHANGED = "android.telecom.action.CURRENT_TTY_MODE_CHANGED";
  
  public static final String ACTION_DEFAULT_CALL_SCREENING_APP_CHANGED = "android.telecom.action.DEFAULT_CALL_SCREENING_APP_CHANGED";
  
  public static final String ACTION_DEFAULT_DIALER_CHANGED = "android.telecom.action.DEFAULT_DIALER_CHANGED";
  
  public static final String ACTION_INCOMING_CALL = "android.telecom.action.INCOMING_CALL";
  
  public static final String ACTION_NEW_UNKNOWN_CALL = "android.telecom.action.NEW_UNKNOWN_CALL";
  
  public static final String ACTION_PHONE_ACCOUNT_REGISTERED = "android.telecom.action.PHONE_ACCOUNT_REGISTERED";
  
  public static final String ACTION_PHONE_ACCOUNT_UNREGISTERED = "android.telecom.action.PHONE_ACCOUNT_UNREGISTERED";
  
  public static final String ACTION_POST_CALL = "android.telecom.action.POST_CALL";
  
  public static final String ACTION_SHOW_CALL_ACCESSIBILITY_SETTINGS = "android.telecom.action.SHOW_CALL_ACCESSIBILITY_SETTINGS";
  
  public static final String ACTION_SHOW_CALL_SETTINGS = "android.telecom.action.SHOW_CALL_SETTINGS";
  
  public static final String ACTION_SHOW_MISSED_CALLS_NOTIFICATION = "android.telecom.action.SHOW_MISSED_CALLS_NOTIFICATION";
  
  public static final String ACTION_SHOW_RESPOND_VIA_SMS_SETTINGS = "android.telecom.action.SHOW_RESPOND_VIA_SMS_SETTINGS";
  
  public static final String ACTION_TTY_PREFERRED_MODE_CHANGED = "android.telecom.action.TTY_PREFERRED_MODE_CHANGED";
  
  public static final int AUDIO_OUTPUT_DEFAULT = 0;
  
  public static final int AUDIO_OUTPUT_DISABLE_SPEAKER = 1;
  
  public static final int AUDIO_OUTPUT_ENABLE_SPEAKER = 0;
  
  public static final int CALL_SOURCE_EMERGENCY_DIALPAD = 1;
  
  public static final int CALL_SOURCE_EMERGENCY_SHORTCUT = 2;
  
  public static final int CALL_SOURCE_UNSPECIFIED = 0;
  
  public static final char DTMF_CHARACTER_PAUSE = ',';
  
  public static final char DTMF_CHARACTER_WAIT = ';';
  
  public static final int DURATION_LONG = 3;
  
  public static final int DURATION_MEDIUM = 2;
  
  public static final int DURATION_SHORT = 1;
  
  public static final int DURATION_VERY_SHORT = 0;
  
  public static final ComponentName EMERGENCY_DIALER_COMPONENT = ComponentName.createRelative("com.android.phone", ".EmergencyDialer");
  
  public static final String EXTRA_CALL_AUDIO_STATE = "android.telecom.extra.CALL_AUDIO_STATE";
  
  @SystemApi
  public static final String EXTRA_CALL_BACK_INTENT = "android.telecom.extra.CALL_BACK_INTENT";
  
  public static final String EXTRA_CALL_BACK_NUMBER = "android.telecom.extra.CALL_BACK_NUMBER";
  
  public static final String EXTRA_CALL_CREATED_EPOCH_TIME_MILLIS = "android.telecom.extra.CALL_CREATED_EPOCH_TIME_MILLIS";
  
  public static final String EXTRA_CALL_CREATED_TIME_MILLIS = "android.telecom.extra.CALL_CREATED_TIME_MILLIS";
  
  public static final String EXTRA_CALL_DISCONNECT_CAUSE = "android.telecom.extra.CALL_DISCONNECT_CAUSE";
  
  public static final String EXTRA_CALL_DISCONNECT_MESSAGE = "android.telecom.extra.CALL_DISCONNECT_MESSAGE";
  
  public static final String EXTRA_CALL_DURATION = "android.telecom.extra.CALL_DURATION";
  
  public static final String EXTRA_CALL_EXTERNAL_RINGER = "android.telecom.extra.CALL_EXTERNAL_RINGER";
  
  public static final String EXTRA_CALL_NETWORK_TYPE = "android.telecom.extra.CALL_NETWORK_TYPE";
  
  public static final String EXTRA_CALL_SOURCE = "android.telecom.extra.CALL_SOURCE";
  
  public static final String EXTRA_CALL_SUBJECT = "android.telecom.extra.CALL_SUBJECT";
  
  public static final String EXTRA_CALL_TECHNOLOGY_TYPE = "android.telecom.extra.CALL_TECHNOLOGY_TYPE";
  
  public static final String EXTRA_CALL_TELECOM_ROUTING_END_TIME_MILLIS = "android.telecom.extra.CALL_TELECOM_ROUTING_END_TIME_MILLIS";
  
  public static final String EXTRA_CALL_TELECOM_ROUTING_START_TIME_MILLIS = "android.telecom.extra.CALL_TELECOM_ROUTING_START_TIME_MILLIS";
  
  public static final String EXTRA_CALL_TYPE_CS = "codeaurora.telecom.extra.CALL_TYPE_CS";
  
  public static final String EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME = "android.telecom.extra.CHANGE_DEFAULT_DIALER_PACKAGE_NAME";
  
  @SystemApi
  public static final String EXTRA_CLEAR_MISSED_CALLS_INTENT = "android.telecom.extra.CLEAR_MISSED_CALLS_INTENT";
  
  @SystemApi
  public static final String EXTRA_CONNECTION_SERVICE = "android.telecom.extra.CONNECTION_SERVICE";
  
  public static final String EXTRA_CURRENT_TTY_MODE = "android.telecom.extra.CURRENT_TTY_MODE";
  
  public static final String EXTRA_DEFAULT_CALL_SCREENING_APP_COMPONENT_NAME = "android.telecom.extra.DEFAULT_CALL_SCREENING_APP_COMPONENT_NAME";
  
  public static final String EXTRA_DISCONNECT_CAUSE = "android.telecom.extra.DISCONNECT_CAUSE";
  
  public static final String EXTRA_HANDLE = "android.telecom.extra.HANDLE";
  
  public static final String EXTRA_HANDOVER_FROM_PHONE_ACCOUNT = "android.telecom.extra.HANDOVER_FROM_PHONE_ACCOUNT";
  
  public static final String EXTRA_INCOMING_CALL_ADDRESS = "android.telecom.extra.INCOMING_CALL_ADDRESS";
  
  public static final String EXTRA_INCOMING_CALL_EXTRAS = "android.telecom.extra.INCOMING_CALL_EXTRAS";
  
  public static final String EXTRA_INCOMING_VIDEO_STATE = "android.telecom.extra.INCOMING_VIDEO_STATE";
  
  public static final String EXTRA_IS_DEFAULT_CALL_SCREENING_APP = "android.telecom.extra.IS_DEFAULT_CALL_SCREENING_APP";
  
  public static final String EXTRA_IS_HANDOVER = "android.telecom.extra.IS_HANDOVER";
  
  public static final String EXTRA_IS_HANDOVER_CONNECTION = "android.telecom.extra.IS_HANDOVER_CONNECTION";
  
  @SystemApi
  public static final String EXTRA_IS_USER_INTENT_EMERGENCY_CALL = "android.telecom.extra.IS_USER_INTENT_EMERGENCY_CALL";
  
  public static final String EXTRA_NEW_OUTGOING_CALL_CANCEL_TIMEOUT = "android.telecom.extra.NEW_OUTGOING_CALL_CANCEL_TIMEOUT";
  
  public static final String EXTRA_NOTIFICATION_COUNT = "android.telecom.extra.NOTIFICATION_COUNT";
  
  public static final String EXTRA_NOTIFICATION_PHONE_NUMBER = "android.telecom.extra.NOTIFICATION_PHONE_NUMBER";
  
  public static final String EXTRA_OUTGOING_CALL_EXTRAS = "android.telecom.extra.OUTGOING_CALL_EXTRAS";
  
  public static final String EXTRA_PHONE_ACCOUNT_HANDLE = "android.telecom.extra.PHONE_ACCOUNT_HANDLE";
  
  public static final String EXTRA_START_CALL_WITH_RTT = "android.telecom.extra.START_CALL_WITH_RTT";
  
  public static final String EXTRA_START_CALL_WITH_SPEAKERPHONE = "android.telecom.extra.START_CALL_WITH_SPEAKERPHONE";
  
  public static final String EXTRA_START_CALL_WITH_VIDEO_STATE = "android.telecom.extra.START_CALL_WITH_VIDEO_STATE";
  
  public static final String EXTRA_TTY_PREFERRED_MODE = "android.telecom.extra.TTY_PREFERRED_MODE";
  
  public static final String EXTRA_UNKNOWN_CALL_HANDLE = "android.telecom.extra.UNKNOWN_CALL_HANDLE";
  
  public static final String EXTRA_USE_ASSISTED_DIALING = "android.telecom.extra.USE_ASSISTED_DIALING";
  
  public static final String GATEWAY_ORIGINAL_ADDRESS = "android.telecom.extra.GATEWAY_ORIGINAL_ADDRESS";
  
  public static final String GATEWAY_PROVIDER_PACKAGE = "android.telecom.extra.GATEWAY_PROVIDER_PACKAGE";
  
  public static final long MEDIUM_CALL_TIME_MS = 120000L;
  
  public static final String METADATA_INCLUDE_EXTERNAL_CALLS = "android.telecom.INCLUDE_EXTERNAL_CALLS";
  
  public static final String METADATA_INCLUDE_SELF_MANAGED_CALLS = "android.telecom.INCLUDE_SELF_MANAGED_CALLS";
  
  public static final String METADATA_IN_CALL_SERVICE_CAR_MODE_UI = "android.telecom.IN_CALL_SERVICE_CAR_MODE_UI";
  
  public static final String METADATA_IN_CALL_SERVICE_RINGING = "android.telecom.IN_CALL_SERVICE_RINGING";
  
  public static final String METADATA_IN_CALL_SERVICE_UI = "android.telecom.IN_CALL_SERVICE_UI";
  
  private static final String PACKAGE_NAME_KEY = "oppo_package_name";
  
  public static final int PRESENTATION_ALLOWED = 1;
  
  public static final int PRESENTATION_PAYPHONE = 4;
  
  public static final int PRESENTATION_RESTRICTED = 2;
  
  public static final int PRESENTATION_UNKNOWN = 3;
  
  public static final long SHORT_CALL_TIME_MS = 60000L;
  
  private static final String TAG = "TelecomManager";
  
  @SystemApi
  public static final int TTY_MODE_FULL = 1;
  
  @SystemApi
  public static final int TTY_MODE_HCO = 2;
  
  @SystemApi
  public static final int TTY_MODE_OFF = 0;
  
  @SystemApi
  public static final int TTY_MODE_VCO = 3;
  
  public static final long VERY_SHORT_CALL_TIME_MS = 3000L;
  
  private final Context mContext;
  
  private final ITelecomService mTelecomServiceOverride;
  
  public static TelecomManager from(Context paramContext) {
    return (TelecomManager)paramContext.getSystemService("telecom");
  }
  
  public TelecomManager(Context paramContext) {
    this(paramContext, null);
  }
  
  public TelecomManager(Context paramContext, ITelecomService paramITelecomService) {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial <init> : ()V
    //   4: aload_1
    //   5: invokevirtual getApplicationContext : ()Landroid/content/Context;
    //   8: astore_3
    //   9: aload_3
    //   10: ifnull -> 43
    //   13: aload_1
    //   14: invokevirtual getAttributionTag : ()Ljava/lang/String;
    //   17: astore #4
    //   19: aload_3
    //   20: invokevirtual getAttributionTag : ()Ljava/lang/String;
    //   23: astore #5
    //   25: aload #4
    //   27: aload #5
    //   29: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   32: ifeq -> 43
    //   35: aload_0
    //   36: aload_3
    //   37: putfield mContext : Landroid/content/Context;
    //   40: goto -> 48
    //   43: aload_0
    //   44: aload_1
    //   45: putfield mContext : Landroid/content/Context;
    //   48: aload_0
    //   49: aload_2
    //   50: putfield mTelecomServiceOverride : Lcom/android/internal/telecom/ITelecomService;
    //   53: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #960	-> 0
    //   #961	-> 4
    //   #962	-> 9
    //   #963	-> 19
    //   #962	-> 25
    //   #964	-> 35
    //   #966	-> 43
    //   #968	-> 48
    //   #969	-> 53
  }
  
  public PhoneAccountHandle getDefaultOutgoingPhoneAccount(String paramString) {
    try {
      if (isServiceConnected()) {
        ITelecomService iTelecomService = getTelecomService();
        Context context = this.mContext;
        String str2 = context.getOpPackageName(), str1 = this.mContext.getAttributionTag();
        return iTelecomService.getDefaultOutgoingPhoneAccount(paramString, str2, str1);
      } 
    } catch (RemoteException remoteException) {
      Log.e("TelecomManager", "Error calling ITelecomService#getDefaultOutgoingPhoneAccount", (Throwable)remoteException);
    } 
    return null;
  }
  
  public PhoneAccountHandle getUserSelectedOutgoingPhoneAccount() {
    try {
      if (isServiceConnected()) {
        ITelecomService iTelecomService = getTelecomService();
        Context context = this.mContext;
        String str = context.getOpPackageName();
        return iTelecomService.getUserSelectedOutgoingPhoneAccount(str);
      } 
    } catch (RemoteException remoteException) {
      Log.e("TelecomManager", "Error calling ITelecomService#getUserSelectedOutgoingPhoneAccount", (Throwable)remoteException);
    } 
    return null;
  }
  
  @SystemApi
  public void setUserSelectedOutgoingPhoneAccount(PhoneAccountHandle paramPhoneAccountHandle) {
    try {
      if (isServiceConnected())
        getTelecomService().setUserSelectedOutgoingPhoneAccount(paramPhoneAccountHandle); 
    } catch (RemoteException remoteException) {
      Log.e("TelecomManager", "Error calling ITelecomService#setUserSelectedOutgoingPhoneAccount");
    } 
  }
  
  public PhoneAccountHandle getSimCallManager() {
    try {
      if (isServiceConnected()) {
        ITelecomService iTelecomService = getTelecomService();
        int i = SubscriptionManager.getDefaultSubscriptionId();
        return iTelecomService.getSimCallManager(i);
      } 
    } catch (RemoteException remoteException) {
      Log.e("TelecomManager", "Error calling ITelecomService#getSimCallManager");
    } 
    return null;
  }
  
  public PhoneAccountHandle getSimCallManagerForSubscription(int paramInt) {
    try {
      if (isServiceConnected())
        return getTelecomService().getSimCallManager(paramInt); 
    } catch (RemoteException remoteException) {
      Log.e("TelecomManager", "Error calling ITelecomService#getSimCallManager");
    } 
    return null;
  }
  
  public PhoneAccountHandle getSimCallManager(int paramInt) {
    try {
      if (isServiceConnected())
        return getTelecomService().getSimCallManagerForUser(paramInt); 
    } catch (RemoteException remoteException) {
      Log.e("TelecomManager", "Error calling ITelecomService#getSimCallManagerForUser");
    } 
    return null;
  }
  
  @SystemApi
  public PhoneAccountHandle getConnectionManager() {
    return getSimCallManager();
  }
  
  @SystemApi
  public List<PhoneAccountHandle> getPhoneAccountsSupportingScheme(String paramString) {
    try {
      if (isServiceConnected()) {
        ITelecomService iTelecomService = getTelecomService();
        Context context = this.mContext;
        String str = context.getOpPackageName();
        return iTelecomService.getPhoneAccountsSupportingScheme(paramString, str);
      } 
    } catch (RemoteException remoteException) {
      Log.e("TelecomManager", "Error calling ITelecomService#getPhoneAccountsSupportingScheme", (Throwable)remoteException);
    } 
    return new ArrayList<>();
  }
  
  public List<PhoneAccountHandle> getCallCapablePhoneAccounts() {
    return getCallCapablePhoneAccounts(false);
  }
  
  public List<PhoneAccountHandle> getSelfManagedPhoneAccounts() {
    try {
      if (isServiceConnected()) {
        ITelecomService iTelecomService = getTelecomService();
        String str1 = this.mContext.getOpPackageName();
        Context context = this.mContext;
        String str2 = context.getAttributionTag();
        return iTelecomService.getSelfManagedPhoneAccounts(str1, str2);
      } 
    } catch (RemoteException remoteException) {
      Log.e("TelecomManager", "Error calling ITelecomService#getSelfManagedPhoneAccounts()", (Throwable)remoteException);
    } 
    return new ArrayList<>();
  }
  
  @SystemApi
  public List<PhoneAccountHandle> getCallCapablePhoneAccounts(boolean paramBoolean) {
    try {
      if (isServiceConnected()) {
        ITelecomService iTelecomService = getTelecomService();
        Context context = this.mContext;
        String str2 = context.getOpPackageName(), str1 = this.mContext.getAttributionTag();
        return iTelecomService.getCallCapablePhoneAccounts(paramBoolean, str2, str1);
      } 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error calling ITelecomService#getCallCapablePhoneAccounts(");
      stringBuilder.append(paramBoolean);
      stringBuilder.append(")");
      Log.e("TelecomManager", stringBuilder.toString(), (Throwable)remoteException);
    } 
    return new ArrayList<>();
  }
  
  @SystemApi
  public List<PhoneAccountHandle> getPhoneAccountsForPackage() {
    try {
      if (isServiceConnected())
        return getTelecomService().getPhoneAccountsForPackage(this.mContext.getPackageName()); 
    } catch (RemoteException remoteException) {
      Log.e("TelecomManager", "Error calling ITelecomService#getPhoneAccountsForPackage", (Throwable)remoteException);
    } 
    return null;
  }
  
  public PhoneAccount getPhoneAccount(PhoneAccountHandle paramPhoneAccountHandle) {
    try {
      if (isServiceConnected())
        return getTelecomService().getPhoneAccount(paramPhoneAccountHandle); 
    } catch (RemoteException remoteException) {
      Log.e("TelecomManager", "Error calling ITelecomService#getPhoneAccount", (Throwable)remoteException);
    } 
    return null;
  }
  
  @SystemApi
  public int getAllPhoneAccountsCount() {
    try {
      if (isServiceConnected())
        return getTelecomService().getAllPhoneAccountsCount(); 
    } catch (RemoteException remoteException) {
      Log.e("TelecomManager", "Error calling ITelecomService#getAllPhoneAccountsCount", (Throwable)remoteException);
    } 
    return 0;
  }
  
  @SystemApi
  public List<PhoneAccount> getAllPhoneAccounts() {
    try {
      if (isServiceConnected())
        return getTelecomService().getAllPhoneAccounts(); 
    } catch (RemoteException remoteException) {
      Log.e("TelecomManager", "Error calling ITelecomService#getAllPhoneAccounts", (Throwable)remoteException);
    } 
    return Collections.EMPTY_LIST;
  }
  
  @SystemApi
  public List<PhoneAccountHandle> getAllPhoneAccountHandles() {
    try {
      if (isServiceConnected())
        return getTelecomService().getAllPhoneAccountHandles(); 
    } catch (RemoteException remoteException) {
      Log.e("TelecomManager", "Error calling ITelecomService#getAllPhoneAccountHandles", (Throwable)remoteException);
    } 
    return Collections.EMPTY_LIST;
  }
  
  public void registerPhoneAccount(PhoneAccount paramPhoneAccount) {
    try {
      if (isServiceConnected())
        getTelecomService().registerPhoneAccount(paramPhoneAccount); 
    } catch (RemoteException remoteException) {
      Log.e("TelecomManager", "Error calling ITelecomService#registerPhoneAccount", (Throwable)remoteException);
    } 
  }
  
  public void unregisterPhoneAccount(PhoneAccountHandle paramPhoneAccountHandle) {
    try {
      if (isServiceConnected())
        getTelecomService().unregisterPhoneAccount(paramPhoneAccountHandle); 
    } catch (RemoteException remoteException) {
      Log.e("TelecomManager", "Error calling ITelecomService#unregisterPhoneAccount", (Throwable)remoteException);
    } 
  }
  
  @SystemApi
  public void clearPhoneAccounts() {
    clearAccounts();
  }
  
  @SystemApi
  public void clearAccounts() {
    try {
      if (isServiceConnected())
        getTelecomService().clearAccounts(this.mContext.getPackageName()); 
    } catch (RemoteException remoteException) {
      Log.e("TelecomManager", "Error calling ITelecomService#clearAccounts", (Throwable)remoteException);
    } 
  }
  
  public void clearAccountsForPackage(String paramString) {
    try {
      if (isServiceConnected() && !TextUtils.isEmpty(paramString))
        getTelecomService().clearAccounts(paramString); 
    } catch (RemoteException remoteException) {
      Log.e("TelecomManager", "Error calling ITelecomService#clearAccountsForPackage", (Throwable)remoteException);
    } 
  }
  
  @SystemApi
  public ComponentName getDefaultPhoneApp() {
    try {
      if (isServiceConnected())
        return getTelecomService().getDefaultPhoneApp(); 
    } catch (RemoteException remoteException) {
      Log.e("TelecomManager", "RemoteException attempting to get the default phone app.", (Throwable)remoteException);
    } 
    return null;
  }
  
  public String getDefaultDialerPackage() {
    try {
      if (isServiceConnected())
        return getTelecomService().getDefaultDialerPackage(); 
    } catch (RemoteException remoteException) {
      Log.e("TelecomManager", "RemoteException attempting to get the default dialer package name.", (Throwable)remoteException);
    } 
    return null;
  }
  
  @SystemApi
  public String getDefaultDialerPackage(UserHandle paramUserHandle) {
    try {
      if (isServiceConnected()) {
        ITelecomService iTelecomService = getTelecomService();
        int i = paramUserHandle.getIdentifier();
        return iTelecomService.getDefaultDialerPackageForUser(i);
      } 
    } catch (RemoteException remoteException) {
      Log.e("TelecomManager", "RemoteException attempting to get the default dialer package name.", (Throwable)remoteException);
    } 
    return null;
  }
  
  @SystemApi
  @Deprecated
  public boolean setDefaultDialer(String paramString) {
    try {
      if (isServiceConnected())
        return getTelecomService().setDefaultDialer(paramString); 
    } catch (RemoteException remoteException) {
      Log.e("TelecomManager", "RemoteException attempting to set the default dialer.", (Throwable)remoteException);
    } 
    return false;
  }
  
  public String getSystemDialerPackage() {
    try {
      if (isServiceConnected())
        return getTelecomService().getSystemDialerPackage(); 
    } catch (RemoteException remoteException) {
      Log.e("TelecomManager", "RemoteException attempting to get the system dialer package name.", (Throwable)remoteException);
    } 
    return null;
  }
  
  public boolean isVoiceMailNumber(PhoneAccountHandle paramPhoneAccountHandle, String paramString) {
    try {
      if (isServiceConnected()) {
        ITelecomService iTelecomService = getTelecomService();
        Context context = this.mContext;
        String str2 = context.getOpPackageName(), str1 = this.mContext.getAttributionTag();
        return iTelecomService.isVoiceMailNumber(paramPhoneAccountHandle, paramString, str2, str1);
      } 
    } catch (RemoteException remoteException) {
      Log.e("TelecomManager", "RemoteException calling ITelecomService#isVoiceMailNumber.", (Throwable)remoteException);
    } 
    return false;
  }
  
  public String getVoiceMailNumber(PhoneAccountHandle paramPhoneAccountHandle) {
    try {
      if (isServiceConnected()) {
        ITelecomService iTelecomService = getTelecomService();
        Context context = this.mContext;
        String str1 = context.getOpPackageName(), str2 = this.mContext.getAttributionTag();
        return iTelecomService.getVoiceMailNumber(paramPhoneAccountHandle, str1, str2);
      } 
    } catch (RemoteException remoteException) {
      Log.e("TelecomManager", "RemoteException calling ITelecomService#hasVoiceMailNumber.", (Throwable)remoteException);
    } 
    return null;
  }
  
  public String getLine1Number(PhoneAccountHandle paramPhoneAccountHandle) {
    try {
      if (isServiceConnected()) {
        ITelecomService iTelecomService = getTelecomService();
        Context context = this.mContext;
        String str1 = context.getOpPackageName(), str2 = this.mContext.getAttributionTag();
        return iTelecomService.getLine1Number(paramPhoneAccountHandle, str1, str2);
      } 
    } catch (RemoteException remoteException) {
      Log.e("TelecomManager", "RemoteException calling ITelecomService#getLine1Number.", (Throwable)remoteException);
    } 
    return null;
  }
  
  public boolean isInCall() {
    try {
      if (isServiceConnected()) {
        ITelecomService iTelecomService = getTelecomService();
        String str1 = this.mContext.getOpPackageName();
        Context context = this.mContext;
        String str2 = context.getAttributionTag();
        return iTelecomService.isInCall(str1, str2);
      } 
    } catch (RemoteException remoteException) {
      Log.e("TelecomManager", "RemoteException calling isInCall().", (Throwable)remoteException);
    } 
    return false;
  }
  
  public boolean isInManagedCall() {
    try {
      if (isServiceConnected()) {
        ITelecomService iTelecomService = getTelecomService();
        String str1 = this.mContext.getOpPackageName();
        Context context = this.mContext;
        String str2 = context.getAttributionTag();
        return iTelecomService.isInManagedCall(str1, str2);
      } 
    } catch (RemoteException remoteException) {
      Log.e("TelecomManager", "RemoteException calling isInManagedCall().", (Throwable)remoteException);
    } 
    return false;
  }
  
  @SystemApi
  public int getCallState() {
    try {
      if (isServiceConnected())
        return getTelecomService().getCallState(); 
    } catch (RemoteException remoteException) {
      Log.d("TelecomManager", "RemoteException calling getCallState().", (Throwable)remoteException);
    } 
    return 0;
  }
  
  @SystemApi
  public boolean isRinging() {
    try {
      if (isServiceConnected())
        return getTelecomService().isRinging(this.mContext.getOpPackageName()); 
    } catch (RemoteException remoteException) {
      Log.e("TelecomManager", "RemoteException attempting to get ringing state of phone app.", (Throwable)remoteException);
    } 
    return false;
  }
  
  @Deprecated
  public boolean endCall() {
    try {
      if (isServiceConnected())
        return getTelecomService().endCall(this.mContext.getPackageName()); 
    } catch (RemoteException remoteException) {
      Log.e("TelecomManager", "Error calling ITelecomService#endCall", (Throwable)remoteException);
    } 
    return false;
  }
  
  @Deprecated
  public void acceptRingingCall() {
    try {
      if (isServiceConnected())
        getTelecomService().acceptRingingCall(this.mContext.getPackageName()); 
    } catch (RemoteException remoteException) {
      Log.e("TelecomManager", "Error calling ITelecomService#acceptRingingCall", (Throwable)remoteException);
    } 
  }
  
  @Deprecated
  public void acceptRingingCall(int paramInt) {
    try {
      if (isServiceConnected()) {
        ITelecomService iTelecomService = getTelecomService();
        Context context = this.mContext;
        String str = context.getPackageName();
        iTelecomService.acceptRingingCallWithVideoState(str, paramInt);
      } 
    } catch (RemoteException remoteException) {
      Log.e("TelecomManager", "Error calling ITelecomService#acceptRingingCallWithVideoState", (Throwable)remoteException);
    } 
  }
  
  public void silenceRinger() {
    try {
      if (isServiceConnected())
        getTelecomService().silenceRinger(this.mContext.getOpPackageName()); 
    } catch (RemoteException remoteException) {
      Log.e("TelecomManager", "Error calling ITelecomService#silenceRinger", (Throwable)remoteException);
    } 
  }
  
  public boolean isTtySupported() {
    try {
      if (isServiceConnected()) {
        ITelecomService iTelecomService = getTelecomService();
        String str1 = this.mContext.getOpPackageName();
        Context context = this.mContext;
        String str2 = context.getAttributionTag();
        return iTelecomService.isTtySupported(str1, str2);
      } 
    } catch (RemoteException remoteException) {
      Log.e("TelecomManager", "RemoteException attempting to get TTY supported state.", (Throwable)remoteException);
    } 
    return false;
  }
  
  @SystemApi
  public int getCurrentTtyMode() {
    try {
      if (isServiceConnected()) {
        ITelecomService iTelecomService = getTelecomService();
        String str1 = this.mContext.getOpPackageName();
        Context context = this.mContext;
        String str2 = context.getAttributionTag();
        return iTelecomService.getCurrentTtyMode(str1, str2);
      } 
    } catch (RemoteException remoteException) {
      Log.e("TelecomManager", "RemoteException attempting to get the current TTY mode.", (Throwable)remoteException);
    } 
    return 0;
  }
  
  public void addNewIncomingCall(PhoneAccountHandle paramPhoneAccountHandle, Bundle paramBundle) {
    try {
      if (isServiceConnected()) {
        if (paramBundle != null && paramBundle.getBoolean("android.telecom.extra.IS_HANDOVER")) {
          Context context = this.mContext;
          if ((context.getApplicationContext().getApplicationInfo()).targetSdkVersion > 27) {
            Log.e("TAG", "addNewIncomingCall failed. Use public api acceptHandover for API > O-MR1");
            return;
          } 
        } 
        ITelecomService iTelecomService = getTelecomService();
        if (paramBundle == null) {
          paramBundle = new Bundle();
          this();
        } 
        iTelecomService.addNewIncomingCall(paramPhoneAccountHandle, paramBundle);
      } 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("RemoteException adding a new incoming call: ");
      stringBuilder.append(paramPhoneAccountHandle);
      Log.e("TelecomManager", stringBuilder.toString(), (Throwable)remoteException);
    } 
  }
  
  public void addNewIncomingConference(PhoneAccountHandle paramPhoneAccountHandle, Bundle paramBundle) {
    try {
      if (isServiceConnected()) {
        ITelecomService iTelecomService = getTelecomService();
        if (paramBundle == null) {
          paramBundle = new Bundle();
          this();
        } 
        iTelecomService.addNewIncomingConference(paramPhoneAccountHandle, paramBundle);
      } 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("RemoteException adding a new incoming conference: ");
      stringBuilder.append(paramPhoneAccountHandle);
      Log.e("TelecomManager", stringBuilder.toString(), (Throwable)remoteException);
    } 
  }
  
  @SystemApi
  public void addNewUnknownCall(PhoneAccountHandle paramPhoneAccountHandle, Bundle paramBundle) {
    try {
      if (isServiceConnected()) {
        ITelecomService iTelecomService = getTelecomService();
        if (paramBundle == null) {
          paramBundle = new Bundle();
          this();
        } 
        iTelecomService.addNewUnknownCall(paramPhoneAccountHandle, paramBundle);
      } 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("RemoteException adding a new unknown call: ");
      stringBuilder.append(paramPhoneAccountHandle);
      Log.e("TelecomManager", stringBuilder.toString(), (Throwable)remoteException);
    } 
  }
  
  public boolean handleMmi(String paramString) {
    ITelecomService iTelecomService = getTelecomService();
    if (iTelecomService != null)
      try {
        return iTelecomService.handlePinMmi(paramString, this.mContext.getOpPackageName());
      } catch (RemoteException remoteException) {
        Log.e("TelecomManager", "Error calling ITelecomService#handlePinMmi", (Throwable)remoteException);
      }  
    return false;
  }
  
  public boolean handleMmi(String paramString, PhoneAccountHandle paramPhoneAccountHandle) {
    ITelecomService iTelecomService = getTelecomService();
    if (iTelecomService != null)
      try {
        Context context = this.mContext;
        String str = context.getOpPackageName();
        return iTelecomService.handlePinMmiForPhoneAccount(paramPhoneAccountHandle, paramString, str);
      } catch (RemoteException remoteException) {
        Log.e("TelecomManager", "Error calling ITelecomService#handlePinMmi", (Throwable)remoteException);
      }  
    return false;
  }
  
  public Uri getAdnUriForPhoneAccount(PhoneAccountHandle paramPhoneAccountHandle) {
    ITelecomService iTelecomService = getTelecomService();
    if (iTelecomService != null && paramPhoneAccountHandle != null)
      try {
        return iTelecomService.getAdnUriForPhoneAccount(paramPhoneAccountHandle, this.mContext.getOpPackageName());
      } catch (RemoteException remoteException) {
        Log.e("TelecomManager", "Error calling ITelecomService#getAdnUriForPhoneAccount", (Throwable)remoteException);
      }  
    return Uri.parse("content://icc/adn");
  }
  
  public void cancelMissedCallsNotification() {
    ITelecomService iTelecomService = getTelecomService();
    if (iTelecomService != null)
      try {
        iTelecomService.cancelMissedCallsNotification(this.mContext.getOpPackageName());
      } catch (RemoteException remoteException) {
        Log.e("TelecomManager", "Error calling ITelecomService#cancelMissedCallsNotification", (Throwable)remoteException);
      }  
  }
  
  public void showInCallScreen(boolean paramBoolean) {
    ITelecomService iTelecomService = getTelecomService();
    if (iTelecomService != null)
      try {
        String str1 = this.mContext.getOpPackageName();
        Context context = this.mContext;
        String str2 = context.getAttributionTag();
        iTelecomService.showInCallScreen(paramBoolean, str1, str2);
      } catch (RemoteException remoteException) {
        Log.e("TelecomManager", "Error calling ITelecomService#showCallScreen", (Throwable)remoteException);
      }  
  }
  
  public void placeCall(Uri paramUri, Bundle paramBundle) {
    ITelecomService iTelecomService = getTelecomService();
    if (iTelecomService != null) {
      if (paramUri == null)
        Log.w("TelecomManager", "Cannot place call to empty address."); 
      if (paramBundle == null)
        try {
          paramBundle = new Bundle();
          this();
        } catch (RemoteException remoteException) {} 
      Context context = this.mContext;
      String str2 = context.getOpPackageName(), str1 = this.mContext.getAttributionTag();
      iTelecomService.placeCall((Uri)remoteException, paramBundle, str2, str1);
    } 
  }
  
  public void startConference(List<Uri> paramList, Bundle paramBundle) {
    ITelecomService iTelecomService = getTelecomService();
    if (iTelecomService != null)
      try {
        Context context = this.mContext;
        String str = context.getOpPackageName();
        iTelecomService.startConference(paramList, paramBundle, str);
      } catch (RemoteException remoteException) {
        Log.e("TelecomManager", "Error calling ITelecomService#placeCall", (Throwable)remoteException);
      }  
  }
  
  @SystemApi
  public void enablePhoneAccount(PhoneAccountHandle paramPhoneAccountHandle, boolean paramBoolean) {
    ITelecomService iTelecomService = getTelecomService();
    if (iTelecomService != null)
      try {
        iTelecomService.enablePhoneAccount(paramPhoneAccountHandle, paramBoolean);
      } catch (RemoteException remoteException) {
        Log.e("TelecomManager", "Error enablePhoneAbbount", (Throwable)remoteException);
      }  
  }
  
  @SystemApi
  public TelecomAnalytics dumpAnalytics() {
    ITelecomService iTelecomService = getTelecomService();
    TelecomAnalytics telecomAnalytics1 = null;
    TelecomAnalytics telecomAnalytics2 = telecomAnalytics1;
    if (iTelecomService != null)
      try {
        telecomAnalytics2 = iTelecomService.dumpCallAnalytics();
      } catch (RemoteException remoteException) {
        Log.e("TelecomManager", "Error dumping call analytics", (Throwable)remoteException);
        telecomAnalytics2 = telecomAnalytics1;
      }  
    return telecomAnalytics2;
  }
  
  public Intent createManageBlockedNumbersIntent() {
    ITelecomService iTelecomService = getTelecomService();
    Intent intent1 = null;
    Intent intent2 = intent1;
    if (iTelecomService != null)
      try {
        intent2 = iTelecomService.createManageBlockedNumbersIntent();
      } catch (RemoteException remoteException) {
        Log.e("TelecomManager", "Error calling ITelecomService#createManageBlockedNumbersIntent", (Throwable)remoteException);
        intent2 = intent1;
      }  
    return intent2;
  }
  
  @SystemApi
  public Intent createLaunchEmergencyDialerIntent(String paramString) {
    ITelecomService iTelecomService = getTelecomService();
    if (iTelecomService != null) {
      try {
        return iTelecomService.createLaunchEmergencyDialerIntent(paramString);
      } catch (RemoteException remoteException) {
        Log.e("TelecomManager", "Error createLaunchEmergencyDialerIntent", (Throwable)remoteException);
      } 
    } else {
      Log.w("TelecomManager", "createLaunchEmergencyDialerIntent - Telecom service not available.");
    } 
    Intent intent = new Intent("android.intent.action.DIAL_EMERGENCY");
    if (!TextUtils.isEmpty(paramString) && TextUtils.isDigitsOnly(paramString))
      intent.setData(Uri.fromParts("tel", paramString, null)); 
    return intent;
  }
  
  public boolean isIncomingCallPermitted(PhoneAccountHandle paramPhoneAccountHandle) {
    if (paramPhoneAccountHandle == null)
      return false; 
    ITelecomService iTelecomService = getTelecomService();
    if (iTelecomService != null)
      try {
        return iTelecomService.isIncomingCallPermitted(paramPhoneAccountHandle);
      } catch (RemoteException remoteException) {
        Log.e("TelecomManager", "Error isIncomingCallPermitted", (Throwable)remoteException);
      }  
    return false;
  }
  
  public boolean isOutgoingCallPermitted(PhoneAccountHandle paramPhoneAccountHandle) {
    ITelecomService iTelecomService = getTelecomService();
    if (iTelecomService != null)
      try {
        return iTelecomService.isOutgoingCallPermitted(paramPhoneAccountHandle);
      } catch (RemoteException remoteException) {
        Log.e("TelecomManager", "Error isOutgoingCallPermitted", (Throwable)remoteException);
      }  
    return false;
  }
  
  public void acceptHandover(Uri paramUri, int paramInt, PhoneAccountHandle paramPhoneAccountHandle) {
    try {
      if (isServiceConnected())
        getTelecomService().acceptHandover(paramUri, paramInt, paramPhoneAccountHandle); 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("RemoteException acceptHandover: ");
      stringBuilder.append(remoteException);
      Log.e("TelecomManager", stringBuilder.toString());
    } 
  }
  
  @SystemApi
  public boolean isInEmergencyCall() {
    try {
      if (isServiceConnected())
        return getTelecomService().isInEmergencyCall(); 
      return false;
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("RemoteException isInEmergencyCall: ");
      stringBuilder.append(remoteException);
      Log.e("TelecomManager", stringBuilder.toString());
      return false;
    } 
  }
  
  public void handleCallIntent(Intent paramIntent, String paramString) {
    try {
      if (isServiceConnected())
        getTelecomService().handleCallIntent(paramIntent, paramString); 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("RemoteException handleCallIntent: ");
      stringBuilder.append(remoteException);
      Log.e("TelecomManager", stringBuilder.toString());
    } 
  }
  
  private ITelecomService getTelecomService() {
    ITelecomService iTelecomService = this.mTelecomServiceOverride;
    if (iTelecomService != null)
      return iTelecomService; 
    return ITelecomService.Stub.asInterface(ServiceManager.getService("telecom"));
  }
  
  private boolean isServiceConnected() {
    boolean bool;
    if (getTelecomService() != null) {
      bool = true;
    } else {
      bool = false;
    } 
    if (!bool)
      Log.w("TelecomManager", "Telecom Service not found."); 
    return bool;
  }
  
  public String oplusInteractWithTelecomService(int paramInt, String paramString) {
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("service colorInteractWithTelecomService which = ");
    stringBuilder2.append(paramInt);
    Log.i("TelecomManager", stringBuilder2.toString());
    StringBuilder stringBuilder3 = null;
    stringBuilder2 = null;
    try {
      if (isServiceConnected())
        String str = getTelecomService().oplusInteractWithTelecomService(paramInt, paramString); 
    } catch (RemoteException remoteException) {
      Log.e("TelecomManager", "Error calling ITelecomService#colorInteractWithTelecomService", (Throwable)remoteException);
      stringBuilder2 = stringBuilder3;
    } 
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("colorInteractWithTelecomService ret = ");
    stringBuilder1.append((String)stringBuilder2);
    Log.i("TelecomManager", stringBuilder1.toString());
    return (String)stringBuilder2;
  }
  
  public void addNewOutgoingCall(Intent paramIntent) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("addNewOutgoingCall intent = ");
    stringBuilder.append(paramIntent);
    Log.i("TelecomManager", stringBuilder.toString());
    try {
      stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("addNewOutgoingCall packageName = ");
      stringBuilder.append(this.mContext.getOpPackageName());
      Log.i("TelecomManager", stringBuilder.toString());
      paramIntent.putExtra("oppo_package_name", this.mContext.getOpPackageName());
      if (isServiceConnected())
        getTelecomService().addNewOutgoingCall(paramIntent); 
    } catch (RemoteException remoteException) {
      Log.e("TelecomManager", "Error calling ITelecomService#addNewOutgoingCall", (Throwable)remoteException);
    } 
  }
  
  public void oplusCancelMissedCallsNotification(Bundle paramBundle) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("oppoCancelMissedCallsNotification intent = ");
    stringBuilder.append(paramBundle);
    Log.i("TelecomManager", stringBuilder.toString());
    ITelecomService iTelecomService = getTelecomService();
    if (iTelecomService != null)
      try {
        iTelecomService.oplusCancelMissedCallsNotification(this.mContext.getOpPackageName(), paramBundle);
      } catch (RemoteException remoteException) {
        Log.e("TelecomManager", "Error calling ITelecomService#oppoCancelMissedCallsNotification", (Throwable)remoteException);
      }  
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class Presentation implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class TtyMode implements Annotation {}
}
