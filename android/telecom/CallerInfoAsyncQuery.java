package android.telecom;

import android.app.ActivityManager;
import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.SQLException;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.os.UserHandle;
import android.os.UserManager;
import android.provider.ContactsContract;
import android.telephony.PhoneNumberUtils;
import android.telephony.SubscriptionManager;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;

public class CallerInfoAsyncQuery {
  private static final boolean DBG = false;
  
  private static final boolean ENABLE_UNKNOWN_NUMBER_GEO_DESCRIPTION = true;
  
  private static final int EVENT_ADD_LISTENER = 2;
  
  private static final int EVENT_EMERGENCY_NUMBER = 4;
  
  private static final int EVENT_END_OF_QUEUE = 3;
  
  private static final int EVENT_GET_GEO_DESCRIPTION = 6;
  
  private static final int EVENT_NEW_QUERY = 1;
  
  private static final int EVENT_VOICEMAIL_NUMBER = 5;
  
  private static final String LOG_TAG = "CallerInfoAsyncQuery";
  
  private CallerInfoAsyncQueryHandler mHandler;
  
  private static final class CookieWrapper {
    public Object cookie;
    
    public int event;
    
    public String geoDescription;
    
    public CallerInfoAsyncQuery.OnQueryCompleteListener listener;
    
    public String number;
    
    public int subId;
    
    private CookieWrapper() {}
  }
  
  class QueryPoolException extends SQLException {
    public QueryPoolException(CallerInfoAsyncQuery this$0) {
      super((String)this$0);
    }
  }
  
  static ContentResolver getCurrentProfileContentResolver(Context paramContext) {
    int i = ActivityManager.getCurrentUser();
    int j = UserManager.get(paramContext).getUserHandle();
    if (j != i)
      try {
        String str = paramContext.getPackageName();
        UserHandle userHandle = UserHandle.of(i);
        Context context = paramContext.createPackageContextAsUser(str, 0, userHandle);
        return context.getContentResolver();
      } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
        Log.e("CallerInfoAsyncQuery", (Throwable)nameNotFoundException, "Can't find self package", new Object[0]);
      }  
    return paramContext.getContentResolver();
  }
  
  class CallerInfoAsyncQueryHandler extends AsyncQueryHandler {
    private CallerInfo mCallerInfo;
    
    private Context mContext;
    
    private List<Runnable> mPendingListenerCallbacks = new ArrayList<>();
    
    private Uri mQueryUri;
    
    final CallerInfoAsyncQuery this$0;
    
    protected class CallerInfoWorkerHandler extends AsyncQueryHandler.WorkerHandler {
      final CallerInfoAsyncQuery.CallerInfoAsyncQueryHandler this$1;
      
      public CallerInfoWorkerHandler(Looper param2Looper) {
        super(CallerInfoAsyncQuery.CallerInfoAsyncQueryHandler.this, param2Looper);
      }
      
      public void handleMessage(Message param2Message) {
        StringBuilder stringBuilder;
        Message message;
        AsyncQueryHandler.WorkerArgs workerArgs = (AsyncQueryHandler.WorkerArgs)param2Message.obj;
        CallerInfoAsyncQuery.CookieWrapper cookieWrapper = (CallerInfoAsyncQuery.CookieWrapper)workerArgs.cookie;
        if (cookieWrapper == null) {
          stringBuilder = new StringBuilder();
          stringBuilder.append("Unexpected command (CookieWrapper is null): ");
          stringBuilder.append(param2Message.what);
          stringBuilder.append(" ignored by CallerInfoWorkerHandler, passing onto parent.");
          Log.i("CallerInfoAsyncQuery", stringBuilder.toString(), new Object[0]);
          super.handleMessage(param2Message);
        } 
        switch (cookieWrapper.event) {
          default:
            return;
          case 6:
            handleGeoDescription(param2Message);
          case 2:
          case 3:
          case 4:
          case 5:
            message = ((AsyncQueryHandler.WorkerArgs)stringBuilder).handler.obtainMessage(param2Message.what);
            message.obj = stringBuilder;
            message.arg1 = param2Message.arg1;
            message.sendToTarget();
          case 1:
            break;
        } 
        super.handleMessage(param2Message);
      }
      
      private void handleGeoDescription(Message param2Message) {
        AsyncQueryHandler.WorkerArgs workerArgs = (AsyncQueryHandler.WorkerArgs)param2Message.obj;
        CallerInfoAsyncQuery.CookieWrapper cookieWrapper = (CallerInfoAsyncQuery.CookieWrapper)workerArgs.cookie;
        if (!TextUtils.isEmpty(cookieWrapper.number) && cookieWrapper.cookie != null && CallerInfoAsyncQuery.CallerInfoAsyncQueryHandler.this.mContext != null) {
          SystemClock.elapsedRealtime();
          cookieWrapper.geoDescription = CallerInfo.getGeoDescription(CallerInfoAsyncQuery.CallerInfoAsyncQueryHandler.this.mContext, cookieWrapper.number);
          SystemClock.elapsedRealtime();
        } 
        Message message = workerArgs.handler.obtainMessage(param2Message.what);
        message.obj = workerArgs;
        message.arg1 = param2Message.arg1;
        message.sendToTarget();
      }
    }
    
    private CallerInfoAsyncQueryHandler(Context param1Context) {
      super(CallerInfoAsyncQuery.getCurrentProfileContentResolver(param1Context));
      this.mContext = param1Context;
    }
    
    protected Handler createHandler(Looper param1Looper) {
      return (Handler)new CallerInfoWorkerHandler(param1Looper);
    }
    
    protected void onQueryComplete(final int token, final Object<Runnable> cw, Cursor param1Cursor) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("##### onQueryComplete() #####   query complete for token: ");
      stringBuilder.append(token);
      Log.d("CallerInfoAsyncQuery", stringBuilder.toString(), new Object[0]);
      cw = cw;
      if (cw == null) {
        Log.i("CallerInfoAsyncQuery", "Cookie is null, ignoring onQueryComplete() request.", new Object[0]);
        if (param1Cursor != null)
          param1Cursor.close(); 
        return;
      } 
      if (((CallerInfoAsyncQuery.CookieWrapper)cw).event == 3) {
        for (cw = (Object<Runnable>)this.mPendingListenerCallbacks.iterator(); cw.hasNext(); ) {
          Runnable runnable = cw.next();
          runnable.run();
        } 
        this.mPendingListenerCallbacks.clear();
        CallerInfoAsyncQuery.this.release();
        if (param1Cursor != null)
          param1Cursor.close(); 
        return;
      } 
      if (((CallerInfoAsyncQuery.CookieWrapper)cw).event == 6) {
        CallerInfo callerInfo = this.mCallerInfo;
        if (callerInfo != null)
          callerInfo.geoDescription = ((CallerInfoAsyncQuery.CookieWrapper)cw).geoDescription; 
        CallerInfoAsyncQuery.CookieWrapper cookieWrapper = new CallerInfoAsyncQuery.CookieWrapper();
        cookieWrapper.event = 3;
        startQuery(token, cookieWrapper, null, null, null, null, null);
      } 
      if (this.mCallerInfo == null)
        if (this.mContext != null && this.mQueryUri != null) {
          if (((CallerInfoAsyncQuery.CookieWrapper)cw).event == 4) {
            this.mCallerInfo = (new CallerInfo()).markAsEmergency(this.mContext);
          } else if (((CallerInfoAsyncQuery.CookieWrapper)cw).event == 5) {
            this.mCallerInfo = (new CallerInfo()).markAsVoiceMail(this.mContext, ((CallerInfoAsyncQuery.CookieWrapper)cw).subId);
          } else {
            this.mCallerInfo = CallerInfo.getCallerInfo(this.mContext, this.mQueryUri, param1Cursor);
            CallerInfo callerInfo = CallerInfo.doSecondaryLookupIfNecessary(this.mContext, ((CallerInfoAsyncQuery.CookieWrapper)cw).number, this.mCallerInfo);
            if (callerInfo != this.mCallerInfo)
              this.mCallerInfo = callerInfo; 
            if (!TextUtils.isEmpty(((CallerInfoAsyncQuery.CookieWrapper)cw).number)) {
              CallerInfo callerInfo1 = this.mCallerInfo;
              String str1 = ((CallerInfoAsyncQuery.CookieWrapper)cw).number, str2 = this.mCallerInfo.normalizedNumber;
              Context context = this.mContext;
              String str3 = CallerInfo.getCurrentCountryIso(context);
              callerInfo1.setPhoneNumber(PhoneNumberUtils.formatNumber(str1, str2, str3));
            } 
            if (TextUtils.isEmpty(this.mCallerInfo.getName())) {
              ((CallerInfoAsyncQuery.CookieWrapper)cw).event = 6;
              startQuery(token, cw, null, null, null, null, null);
              return;
            } 
          } 
          CallerInfoAsyncQuery.CookieWrapper cookieWrapper = new CallerInfoAsyncQuery.CookieWrapper();
          cookieWrapper.event = 3;
          startQuery(token, cookieWrapper, null, null, null, null, null);
        } else {
          throw new CallerInfoAsyncQuery.QueryPoolException("Bad context or query uri, or CallerInfoAsyncQuery already released.");
        }  
      if (((CallerInfoAsyncQuery.CookieWrapper)cw).listener != null) {
        this.mPendingListenerCallbacks.add(new Runnable() {
              final CallerInfoAsyncQuery.CallerInfoAsyncQueryHandler this$1;
              
              final CallerInfoAsyncQuery.CookieWrapper val$cw;
              
              final int val$token;
              
              public void run() {
                cw.listener.onQueryComplete(token, cw.cookie, this.this$1.mCallerInfo);
              }
            });
      } else {
        Log.w("CallerInfoAsyncQuery", "There is no listener to notify for this query.", new Object[0]);
      } 
      if (param1Cursor != null)
        param1Cursor.close(); 
    }
  }
  
  class null implements Runnable {
    final CallerInfoAsyncQuery.CallerInfoAsyncQueryHandler this$1;
    
    final CallerInfoAsyncQuery.CookieWrapper val$cw;
    
    final int val$token;
    
    public void run() {
      cw.listener.onQueryComplete(token, cw.cookie, this.this$1.mCallerInfo);
    }
  }
  
  public static CallerInfoAsyncQuery startQuery(int paramInt, Context paramContext, Uri paramUri, OnQueryCompleteListener paramOnQueryCompleteListener, Object paramObject) {
    CallerInfoAsyncQuery callerInfoAsyncQuery = new CallerInfoAsyncQuery();
    callerInfoAsyncQuery.allocate(paramContext, paramUri);
    CookieWrapper cookieWrapper = new CookieWrapper();
    cookieWrapper.listener = paramOnQueryCompleteListener;
    cookieWrapper.cookie = paramObject;
    cookieWrapper.event = 1;
    callerInfoAsyncQuery.mHandler.startQuery(paramInt, cookieWrapper, paramUri, null, null, null, null);
    return callerInfoAsyncQuery;
  }
  
  public static CallerInfoAsyncQuery startQuery(int paramInt, Context paramContext, String paramString, OnQueryCompleteListener paramOnQueryCompleteListener, Object paramObject) {
    int i = SubscriptionManager.getDefaultSubscriptionId();
    return startQuery(paramInt, paramContext, paramString, paramOnQueryCompleteListener, paramObject, i);
  }
  
  public static CallerInfoAsyncQuery startQuery(int paramInt1, Context paramContext, String paramString, OnQueryCompleteListener paramOnQueryCompleteListener, Object paramObject, int paramInt2) {
    Uri.Builder builder = ContactsContract.PhoneLookup.ENTERPRISE_CONTENT_FILTER_URI.buildUpon();
    builder = builder.appendPath(paramString);
    boolean bool = PhoneNumberUtils.isUriNumber(paramString);
    builder = builder.appendQueryParameter("sip", String.valueOf(bool));
    Uri uri = builder.build();
    CallerInfoAsyncQuery callerInfoAsyncQuery = new CallerInfoAsyncQuery();
    callerInfoAsyncQuery.allocate(paramContext, uri);
    CookieWrapper cookieWrapper = new CookieWrapper();
    cookieWrapper.listener = paramOnQueryCompleteListener;
    cookieWrapper.cookie = paramObject;
    cookieWrapper.number = paramString;
    cookieWrapper.subId = paramInt2;
    if (PhoneNumberUtils.isLocalEmergencyNumber(paramContext, paramString)) {
      cookieWrapper.event = 4;
    } else if (PhoneNumberUtils.isVoiceMailNumber(paramContext, paramInt2, paramString)) {
      cookieWrapper.event = 5;
    } else {
      cookieWrapper.event = 1;
    } 
    CallerInfoAsyncQueryHandler callerInfoAsyncQueryHandler = callerInfoAsyncQuery.mHandler;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("(CASE WHEN number='");
    stringBuilder.append(paramString);
    stringBuilder.append("' THEN 0 ELSE 1 END) ");
    callerInfoAsyncQueryHandler.startQuery(paramInt1, cookieWrapper, uri, null, null, null, stringBuilder.toString());
    return callerInfoAsyncQuery;
  }
  
  public void addQueryListener(int paramInt, OnQueryCompleteListener paramOnQueryCompleteListener, Object paramObject) {
    CookieWrapper cookieWrapper = new CookieWrapper();
    cookieWrapper.listener = paramOnQueryCompleteListener;
    cookieWrapper.cookie = paramObject;
    cookieWrapper.event = 2;
    this.mHandler.startQuery(paramInt, cookieWrapper, null, null, null, null, null);
  }
  
  private void allocate(Context paramContext, Uri paramUri) {
    if (paramContext != null && paramUri != null) {
      CallerInfoAsyncQueryHandler callerInfoAsyncQueryHandler = new CallerInfoAsyncQueryHandler(paramContext);
      CallerInfoAsyncQueryHandler.access$502(callerInfoAsyncQueryHandler, paramUri);
      return;
    } 
    throw new QueryPoolException("Bad context or query uri.");
  }
  
  private void release() {
    CallerInfoAsyncQueryHandler.access$002(this.mHandler, (Context)null);
    CallerInfoAsyncQueryHandler.access$502(this.mHandler, (Uri)null);
    CallerInfoAsyncQueryHandler.access$302(this.mHandler, (CallerInfo)null);
    this.mHandler = null;
  }
  
  private static String sanitizeUriToString(Uri paramUri) {
    if (paramUri != null) {
      String str = paramUri.toString();
      int i = str.lastIndexOf('/');
      if (i > 0) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(str.substring(0, i));
        stringBuilder.append("/xxxxxxx");
        return stringBuilder.toString();
      } 
      return str;
    } 
    return "";
  }
  
  public static interface OnQueryCompleteListener {
    void onQueryComplete(int param1Int, Object param1Object, CallerInfo param1CallerInfo);
  }
}
