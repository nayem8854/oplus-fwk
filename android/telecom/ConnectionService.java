package android.telecom;

import android.annotation.SystemApi;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.telecom.Logging.Session;
import com.android.internal.os.SomeArgs;
import com.android.internal.telecom.IConnectionService;
import com.android.internal.telecom.IConnectionServiceAdapter;
import com.android.internal.telecom.IVideoProvider;
import com.android.internal.telecom.RemoteServiceCallback;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public abstract class ConnectionService extends Service {
  private static final boolean PII_DEBUG = Log.isLoggable(3);
  
  private final Map<String, Connection> mConnectionById = new ConcurrentHashMap<>();
  
  private final Map<Connection, String> mIdByConnection = new ConcurrentHashMap<>();
  
  private final Map<String, Conference> mConferenceById = new ConcurrentHashMap<>();
  
  private final Map<Conference, String> mIdByConference = new ConcurrentHashMap<>();
  
  private final RemoteConnectionManager mRemoteConnectionManager = new RemoteConnectionManager(this);
  
  private final List<Runnable> mPreInitializationConnectionRequests = new ArrayList<>();
  
  private final ConnectionServiceAdapter mAdapter = new ConnectionServiceAdapter();
  
  private boolean mAreAccountsInitialized = false;
  
  private Object mIdSyncRoot = new Object();
  
  private int mId = 0;
  
  private final IBinder mBinder = (IBinder)new IConnectionService.Stub() {
      final ConnectionService this$0;
      
      public void addConnectionServiceAdapter(IConnectionServiceAdapter param1IConnectionServiceAdapter, Session.Info param1Info) {
        Log.startSession(param1Info, "CS.aCSA");
        try {
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1IConnectionServiceAdapter;
          someArgs.arg2 = Log.createSubsession();
          ConnectionService.this.mHandler.obtainMessage(1, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void removeConnectionServiceAdapter(IConnectionServiceAdapter param1IConnectionServiceAdapter, Session.Info param1Info) {
        Log.startSession(param1Info, "CS.rCSA");
        try {
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1IConnectionServiceAdapter;
          someArgs.arg2 = Log.createSubsession();
          ConnectionService.this.mHandler.obtainMessage(16, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void createConnection(PhoneAccountHandle param1PhoneAccountHandle, String param1String, ConnectionRequest param1ConnectionRequest, boolean param1Boolean1, boolean param1Boolean2, Session.Info param1Info) {
        Log.startSession(param1Info, "CS.crCo");
        try {
          boolean bool2;
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1PhoneAccountHandle;
          someArgs.arg2 = param1String;
          someArgs.arg3 = param1ConnectionRequest;
          someArgs.arg4 = Log.createSubsession();
          boolean bool1 = true;
          if (param1Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          someArgs.argi1 = bool2;
          if (param1Boolean2) {
            bool2 = bool1;
          } else {
            bool2 = false;
          } 
          someArgs.argi2 = bool2;
          ConnectionService.this.mHandler.obtainMessage(2, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void createConnectionComplete(String param1String, Session.Info param1Info) {
        Log.startSession(param1Info, "CS.crCoC");
        try {
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1String;
          someArgs.arg2 = Log.createSubsession();
          ConnectionService.this.mHandler.obtainMessage(29, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void createConnectionFailed(PhoneAccountHandle param1PhoneAccountHandle, String param1String, ConnectionRequest param1ConnectionRequest, boolean param1Boolean, Session.Info param1Info) {
        Log.startSession(param1Info, "CS.crCoF");
        try {
          boolean bool;
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1String;
          someArgs.arg2 = param1ConnectionRequest;
          someArgs.arg3 = Log.createSubsession();
          someArgs.arg4 = param1PhoneAccountHandle;
          if (param1Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          someArgs.argi1 = bool;
          ConnectionService.this.mHandler.obtainMessage(25, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void createConference(PhoneAccountHandle param1PhoneAccountHandle, String param1String, ConnectionRequest param1ConnectionRequest, boolean param1Boolean1, boolean param1Boolean2, Session.Info param1Info) {
        Log.startSession(param1Info, "CS.crConf");
        try {
          boolean bool2;
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1PhoneAccountHandle;
          someArgs.arg2 = param1String;
          someArgs.arg3 = param1ConnectionRequest;
          someArgs.arg4 = Log.createSubsession();
          boolean bool1 = true;
          if (param1Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          someArgs.argi1 = bool2;
          if (param1Boolean2) {
            bool2 = bool1;
          } else {
            bool2 = false;
          } 
          someArgs.argi2 = bool2;
          ConnectionService.this.mHandler.obtainMessage(35, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void createConferenceComplete(String param1String, Session.Info param1Info) {
        Log.startSession(param1Info, "CS.crConfC");
        try {
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1String;
          someArgs.arg2 = Log.createSubsession();
          ConnectionService.this.mHandler.obtainMessage(36, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void createConferenceFailed(PhoneAccountHandle param1PhoneAccountHandle, String param1String, ConnectionRequest param1ConnectionRequest, boolean param1Boolean, Session.Info param1Info) {
        Log.startSession(param1Info, "CS.crConfF");
        try {
          boolean bool;
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1String;
          someArgs.arg2 = param1ConnectionRequest;
          someArgs.arg3 = Log.createSubsession();
          someArgs.arg4 = param1PhoneAccountHandle;
          if (param1Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          someArgs.argi1 = bool;
          ConnectionService.this.mHandler.obtainMessage(37, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void handoverFailed(String param1String, ConnectionRequest param1ConnectionRequest, int param1Int, Session.Info param1Info) {
        Log.startSession(param1Info, "CS.haF");
        try {
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1String;
          someArgs.arg2 = param1ConnectionRequest;
          someArgs.arg3 = Log.createSubsession();
          someArgs.arg4 = Integer.valueOf(param1Int);
          ConnectionService.this.mHandler.obtainMessage(32, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void handoverComplete(String param1String, Session.Info param1Info) {
        Log.startSession(param1Info, "CS.hC");
        try {
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1String;
          someArgs.arg2 = Log.createSubsession();
          ConnectionService.this.mHandler.obtainMessage(33, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void abort(String param1String, Session.Info param1Info) {
        Log.startSession(param1Info, "CS.ab");
        try {
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1String;
          someArgs.arg2 = Log.createSubsession();
          ConnectionService.this.mHandler.obtainMessage(3, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void answerVideo(String param1String, int param1Int, Session.Info param1Info) {
        Log.startSession(param1Info, "CS.anV");
        try {
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1String;
          someArgs.arg2 = Log.createSubsession();
          someArgs.argi1 = param1Int;
          ConnectionService.this.mHandler.obtainMessage(17, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void answer(String param1String, Session.Info param1Info) {
        Log.startSession(param1Info, "CS.an");
        try {
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1String;
          someArgs.arg2 = Log.createSubsession();
          ConnectionService.this.mHandler.obtainMessage(4, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void deflect(String param1String, Uri param1Uri, Session.Info param1Info) {
        Log.startSession(param1Info, "CS.def");
        try {
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1String;
          someArgs.arg2 = param1Uri;
          someArgs.arg3 = Log.createSubsession();
          ConnectionService.this.mHandler.obtainMessage(34, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void reject(String param1String, Session.Info param1Info) {
        Log.startSession(param1Info, "CS.r");
        try {
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1String;
          someArgs.arg2 = Log.createSubsession();
          ConnectionService.this.mHandler.obtainMessage(5, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void rejectWithReason(String param1String, int param1Int, Session.Info param1Info) {
        Log.startSession(param1Info, "CS.r");
        try {
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1String;
          someArgs.argi1 = param1Int;
          someArgs.arg2 = Log.createSubsession();
          ConnectionService.this.mHandler.obtainMessage(38, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void rejectWithMessage(String param1String1, String param1String2, Session.Info param1Info) {
        Log.startSession(param1Info, "CS.rWM");
        try {
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1String1;
          someArgs.arg2 = param1String2;
          someArgs.arg3 = Log.createSubsession();
          ConnectionService.this.mHandler.obtainMessage(20, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void transfer(String param1String, Uri param1Uri, boolean param1Boolean, Session.Info param1Info) {
        Log.startSession(param1Info, "CS.trans");
        try {
          boolean bool;
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1String;
          someArgs.arg2 = param1Uri;
          if (param1Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          someArgs.argi1 = bool;
          someArgs.arg3 = Log.createSubsession();
          ConnectionService.this.mHandler.obtainMessage(40, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void consultativeTransfer(String param1String1, String param1String2, Session.Info param1Info) {
        Log.startSession(param1Info, "CS.cTrans");
        try {
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1String1;
          someArgs.arg2 = param1String2;
          someArgs.arg3 = Log.createSubsession();
          Message message = ConnectionService.this.mHandler.obtainMessage(41, someArgs);
          message.sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void silence(String param1String, Session.Info param1Info) {
        Log.startSession(param1Info, "CS.s");
        try {
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1String;
          someArgs.arg2 = Log.createSubsession();
          ConnectionService.this.mHandler.obtainMessage(21, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void disconnect(String param1String, Session.Info param1Info) {
        Log.startSession(param1Info, "CS.d");
        try {
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1String;
          someArgs.arg2 = Log.createSubsession();
          ConnectionService.this.mHandler.obtainMessage(6, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void hold(String param1String, Session.Info param1Info) {
        Log.startSession(param1Info, "CS.h");
        try {
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1String;
          someArgs.arg2 = Log.createSubsession();
          ConnectionService.this.mHandler.obtainMessage(7, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void unhold(String param1String, Session.Info param1Info) {
        Log.startSession(param1Info, "CS.u");
        try {
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1String;
          someArgs.arg2 = Log.createSubsession();
          ConnectionService.this.mHandler.obtainMessage(8, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void onCallAudioStateChanged(String param1String, CallAudioState param1CallAudioState, Session.Info param1Info) {
        Log.startSession(param1Info, "CS.cASC");
        try {
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1String;
          someArgs.arg2 = param1CallAudioState;
          someArgs.arg3 = Log.createSubsession();
          ConnectionService.this.mHandler.obtainMessage(9, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void playDtmfTone(String param1String, char param1Char, Session.Info param1Info) {
        Log.startSession(param1Info, "CS.pDT");
        try {
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = Character.valueOf(param1Char);
          someArgs.arg2 = param1String;
          someArgs.arg3 = Log.createSubsession();
          ConnectionService.this.mHandler.obtainMessage(10, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void stopDtmfTone(String param1String, Session.Info param1Info) {
        Log.startSession(param1Info, "CS.sDT");
        try {
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1String;
          someArgs.arg2 = Log.createSubsession();
          ConnectionService.this.mHandler.obtainMessage(11, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void conference(String param1String1, String param1String2, Session.Info param1Info) {
        Log.startSession(param1Info, "CS.c");
        try {
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1String1;
          someArgs.arg2 = param1String2;
          someArgs.arg3 = Log.createSubsession();
          ConnectionService.this.mHandler.obtainMessage(12, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void splitFromConference(String param1String, Session.Info param1Info) {
        Log.startSession(param1Info, "CS.sFC");
        try {
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1String;
          someArgs.arg2 = Log.createSubsession();
          ConnectionService.this.mHandler.obtainMessage(13, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void mergeConference(String param1String, Session.Info param1Info) {
        Log.startSession(param1Info, "CS.mC");
        try {
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1String;
          someArgs.arg2 = Log.createSubsession();
          ConnectionService.this.mHandler.obtainMessage(18, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void swapConference(String param1String, Session.Info param1Info) {
        Log.startSession(param1Info, "CS.sC");
        try {
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1String;
          someArgs.arg2 = Log.createSubsession();
          ConnectionService.this.mHandler.obtainMessage(19, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void addConferenceParticipants(String param1String, List<Uri> param1List, Session.Info param1Info) {
        Log.startSession(param1Info, "CS.aP");
        try {
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1String;
          someArgs.arg2 = param1List;
          someArgs.arg3 = Log.createSubsession();
          ConnectionService.this.mHandler.obtainMessage(39, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void onPostDialContinue(String param1String, boolean param1Boolean, Session.Info param1Info) {
        Log.startSession(param1Info, "CS.oPDC");
        try {
          boolean bool;
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1String;
          someArgs.arg2 = Log.createSubsession();
          if (param1Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          someArgs.argi1 = bool;
          ConnectionService.this.mHandler.obtainMessage(14, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void pullExternalCall(String param1String, Session.Info param1Info) {
        Log.startSession(param1Info, "CS.pEC");
        try {
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1String;
          someArgs.arg2 = Log.createSubsession();
          ConnectionService.this.mHandler.obtainMessage(22, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void sendCallEvent(String param1String1, String param1String2, Bundle param1Bundle, Session.Info param1Info) {
        Log.startSession(param1Info, "CS.sCE");
        try {
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1String1;
          someArgs.arg2 = param1String2;
          someArgs.arg3 = param1Bundle;
          someArgs.arg4 = Log.createSubsession();
          ConnectionService.this.mHandler.obtainMessage(23, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void onExtrasChanged(String param1String, Bundle param1Bundle, Session.Info param1Info) {
        Log.startSession(param1Info, "CS.oEC");
        try {
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1String;
          someArgs.arg2 = param1Bundle;
          someArgs.arg3 = Log.createSubsession();
          ConnectionService.this.mHandler.obtainMessage(24, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void startRtt(String param1String, ParcelFileDescriptor param1ParcelFileDescriptor1, ParcelFileDescriptor param1ParcelFileDescriptor2, Session.Info param1Info) throws RemoteException {
        Log.startSession(param1Info, "CS.+RTT");
        try {
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1String;
          Connection.RttTextStream rttTextStream = new Connection.RttTextStream();
          this(param1ParcelFileDescriptor2, param1ParcelFileDescriptor1);
          someArgs.arg2 = rttTextStream;
          someArgs.arg3 = Log.createSubsession();
          ConnectionService.this.mHandler.obtainMessage(26, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void stopRtt(String param1String, Session.Info param1Info) throws RemoteException {
        Log.startSession(param1Info, "CS.-RTT");
        try {
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1String;
          someArgs.arg2 = Log.createSubsession();
          ConnectionService.this.mHandler.obtainMessage(27, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void respondToRttUpgradeRequest(String param1String, ParcelFileDescriptor param1ParcelFileDescriptor1, ParcelFileDescriptor param1ParcelFileDescriptor2, Session.Info param1Info) throws RemoteException {
        Log.startSession(param1Info, "CS.rTRUR");
        try {
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1String;
          if (param1ParcelFileDescriptor2 == null || param1ParcelFileDescriptor1 == null) {
            someArgs.arg2 = null;
          } else {
            Connection.RttTextStream rttTextStream = new Connection.RttTextStream();
            this(param1ParcelFileDescriptor2, param1ParcelFileDescriptor1);
            someArgs.arg2 = rttTextStream;
          } 
          someArgs.arg3 = Log.createSubsession();
          ConnectionService.this.mHandler.obtainMessage(28, someArgs).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void connectionServiceFocusLost(Session.Info param1Info) throws RemoteException {
        Log.startSession(param1Info, "CS.cSFL");
        try {
          ConnectionService.this.mHandler.obtainMessage(30).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
      
      public void connectionServiceFocusGained(Session.Info param1Info) throws RemoteException {
        Log.startSession(param1Info, "CS.cSFG");
        try {
          ConnectionService.this.mHandler.obtainMessage(31).sendToTarget();
          return;
        } finally {
          Log.endSession();
        } 
      }
    };
  
  private final Handler mHandler = (Handler)new Object(this, Looper.getMainLooper());
  
  private final Conference.Listener mConferenceListener = (Conference.Listener)new Object(this);
  
  private final Connection.Listener mConnectionListener = (Connection.Listener)new Object(this);
  
  public final IBinder onBind(Intent paramIntent) {
    return this.mBinder;
  }
  
  public boolean onUnbind(Intent paramIntent) {
    endAllConnections();
    return super.onUnbind(paramIntent);
  }
  
  private void createConference(PhoneAccountHandle paramPhoneAccountHandle, String paramString, ConnectionRequest paramConnectionRequest, boolean paramBoolean1, boolean paramBoolean2) {
    IVideoProvider iVideoProvider;
    if (paramBoolean1) {
      conference1 = onCreateIncomingConference(paramPhoneAccountHandle, paramConnectionRequest);
    } else {
      conference1 = onCreateOutgoingConference((PhoneAccountHandle)conference1, paramConnectionRequest);
    } 
    Conference conference2 = conference1;
    Log.d(this, "createConference, conference: %s", new Object[] { conference2 });
    Conference conference1 = conference2;
    if (conference2 == null) {
      Log.i(this, "createConference, implementation returned null conference.", new Object[0]);
      DisconnectCause disconnectCause = new DisconnectCause(1, "IMPL_RETURNED_NULL_CONFERENCE");
      PhoneAccountHandle phoneAccountHandle = paramConnectionRequest.getAccountHandle();
      conference1 = Conference.createFailedConference(disconnectCause, phoneAccountHandle);
    } 
    Bundle bundle2 = paramConnectionRequest.getExtras();
    Bundle bundle1 = new Bundle();
    bundle1.putString("android.telecom.extra.ORIGINAL_CONNECTION_ID", paramString);
    if (bundle2 != null)
      if (bundle2.containsKey("android.telecom.extra.REMOTE_CONNECTION_ORIGINATING_PACKAGE_NAME")) {
        String str1 = bundle2.getString("android.telecom.extra.REMOTE_CONNECTION_ORIGINATING_PACKAGE_NAME");
        bundle1.putString("android.telecom.extra.REMOTE_CONNECTION_ORIGINATING_PACKAGE_NAME", str1);
        PhoneAccountHandle phoneAccountHandle = paramConnectionRequest.getAccountHandle();
        bundle1.putParcelable("android.telecom.extra.REMOTE_PHONE_ACCOUNT_HANDLE", phoneAccountHandle);
      }  
    conference1.putExtras(bundle1);
    this.mConferenceById.put(paramString, conference1);
    this.mIdByConference.put(conference1, paramString);
    conference1.addListener(this.mConferenceListener);
    ParcelableConference.Builder builder2 = new ParcelableConference.Builder(paramConnectionRequest.getAccountHandle(), conference1.getState());
    builder2 = builder2.setConnectionCapabilities(conference1.getConnectionCapabilities());
    ParcelableConference.Builder builder3 = builder2.setConnectionProperties(conference1.getConnectionProperties());
    if (conference1.getVideoProvider() == null) {
      builder2 = null;
    } else {
      iVideoProvider = conference1.getVideoProvider().getInterface();
    } 
    int i = conference1.getVideoState();
    ParcelableConference.Builder builder1 = builder3.setVideoAttributes(iVideoProvider, i);
    long l1 = conference1.getConnectTimeMillis();
    long l2 = conference1.getConnectionStartElapsedRealtimeMillis();
    builder1 = builder1.setConnectTimeMillis(l1, l2);
    builder1 = builder1.setStatusHints(conference1.getStatusHints());
    builder1 = builder1.setExtras(conference1.getExtras());
    builder1 = builder1.setAddress(conference1.getAddress(), conference1.getAddressPresentation());
    String str = conference1.getCallerDisplayName();
    i = conference1.getCallerDisplayNamePresentation();
    builder1 = builder1.setCallerDisplayName(str, i);
    builder1 = builder1.setDisconnectCause(conference1.getDisconnectCause());
    builder1 = builder1.setRingbackRequested(conference1.isRingbackRequested());
    ParcelableConference parcelableConference = builder1.build();
    if (conference1.getState() != 6) {
      conference1.setTelecomCallId(paramString);
      this.mAdapter.setVideoProvider(paramString, conference1.getVideoProvider());
      this.mAdapter.setVideoState(paramString, conference1.getVideoState());
      onConferenceAdded(conference1);
    } 
    Log.d(this, "createConference, calling handleCreateConferenceSuccessful %s", new Object[] { paramString });
    this.mAdapter.handleCreateConferenceComplete(paramString, paramConnectionRequest, parcelableConference);
  }
  
  private void createConnection(PhoneAccountHandle paramPhoneAccountHandle, String paramString, ConnectionRequest paramConnectionRequest, boolean paramBoolean1, boolean paramBoolean2) {
    Connection connection;
    IVideoProvider iVideoProvider;
    boolean bool;
    if (paramConnectionRequest.getExtras() != null && 
      paramConnectionRequest.getExtras().getBoolean("android.telecom.extra.IS_HANDOVER", false)) {
      bool1 = true;
    } else {
      bool1 = false;
    } 
    if (paramConnectionRequest.getExtras() != null && paramConnectionRequest.getExtras().getBoolean("android.telecom.extra.IS_HANDOVER_CONNECTION", false)) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    Log.d(this, "createConnection, callManagerAccount: %s, callId: %s, request: %s, isIncoming: %b, isUnknown: %b, isLegacyHandover: %b, isHandover: %b", new Object[] { paramPhoneAccountHandle, paramString, paramConnectionRequest, Boolean.valueOf(paramBoolean1), Boolean.valueOf(paramBoolean2), Boolean.valueOf(bool1), Boolean.valueOf(bool2) });
    if (bool2) {
      if (paramConnectionRequest.getExtras() != null) {
        paramPhoneAccountHandle = (PhoneAccountHandle)paramConnectionRequest.getExtras().getParcelable("android.telecom.extra.HANDOVER_FROM_PHONE_ACCOUNT");
      } else {
        paramPhoneAccountHandle = null;
      } 
      if (!paramBoolean1) {
        connection = onCreateOutgoingHandoverConnection(paramPhoneAccountHandle, paramConnectionRequest);
      } else {
        connection = onCreateIncomingHandoverConnection((PhoneAccountHandle)connection, paramConnectionRequest);
      } 
    } else if (paramBoolean2) {
      connection = onCreateUnknownConnection((PhoneAccountHandle)connection, paramConnectionRequest);
    } else if (paramBoolean1) {
      connection = onCreateIncomingConnection((PhoneAccountHandle)connection, paramConnectionRequest);
    } else {
      connection = onCreateOutgoingConnection((PhoneAccountHandle)connection, paramConnectionRequest);
    } 
    Log.d(this, "createConnection, connection: %s", new Object[] { connection });
    if (connection == null) {
      Log.i(this, "createConnection, implementation returned null connection.", new Object[0]);
      connection = Connection.createFailedConnection(new DisconnectCause(1, "IMPL_RETURNED_NULL_CONNECTION"));
    } else {
      try {
        Bundle bundle1 = paramConnectionRequest.getExtras();
        if (bundle1 != null)
          if (bundle1.containsKey("android.telecom.extra.REMOTE_CONNECTION_ORIGINATING_PACKAGE_NAME")) {
            Bundle bundle2 = new Bundle();
            this();
            String str = bundle1.getString("android.telecom.extra.REMOTE_CONNECTION_ORIGINATING_PACKAGE_NAME");
            bundle2.putString("android.telecom.extra.REMOTE_CONNECTION_ORIGINATING_PACKAGE_NAME", str);
            PhoneAccountHandle phoneAccountHandle1 = paramConnectionRequest.getAccountHandle();
            bundle2.putParcelable("android.telecom.extra.REMOTE_PHONE_ACCOUNT_HANDLE", phoneAccountHandle1);
            connection.putExtras(bundle2);
          }  
      } catch (UnsupportedOperationException unsupportedOperationException) {}
    } 
    if ((connection.getConnectionProperties() & 0x80) == 128) {
      bool = true;
    } else {
      bool = false;
    } 
    if (bool)
      connection.setAudioModeIsVoip(true); 
    connection.setTelecomCallId(paramString);
    if (connection.getState() != 6)
      addConnection(paramConnectionRequest.getAccountHandle(), paramString, connection); 
    Uri uri1 = connection.getAddress();
    if (uri1 == null) {
      str2 = "null";
    } else {
      str2 = str2.getSchemeSpecificPart();
    } 
    String str3 = Connection.toLogSafePhoneNumber(str2);
    String str1 = Connection.stateToString(connection.getState());
    String str2 = Connection.capabilitiesToString(connection.getConnectionCapabilities());
    String str4 = Connection.propertiesToString(connection.getConnectionProperties());
    Log.v(this, "createConnection, number: %s, state: %s, capabilities: %s, properties: %s", new Object[] { str3, str1, str2, str4 });
    Log.d(this, "createConnection, calling handleCreateConnectionSuccessful %s", new Object[] { paramString });
    ConnectionServiceAdapter connectionServiceAdapter = this.mAdapter;
    PhoneAccountHandle phoneAccountHandle = paramConnectionRequest.getAccountHandle();
    int i = connection.getState();
    int j = connection.getConnectionCapabilities();
    int k = connection.getConnectionProperties();
    int m = connection.getSupportedAudioRoutes();
    Uri uri2 = connection.getAddress();
    int n = connection.getAddressPresentation();
    String str5 = connection.getCallerDisplayName();
    int i1 = connection.getCallerDisplayNamePresentation();
    if (connection.getVideoProvider() == null) {
      str2 = null;
    } else {
      iVideoProvider = connection.getVideoProvider().getInterface();
    } 
    int i2 = connection.getVideoState();
    boolean bool1 = connection.isRingbackRequested();
    boolean bool2 = connection.getAudioModeIsVoip();
    long l1 = connection.getConnectTimeMillis();
    long l2 = connection.getConnectionStartElapsedRealtimeMillis();
    StatusHints statusHints = connection.getStatusHints();
    DisconnectCause disconnectCause = connection.getDisconnectCause();
    List<String> list = createIdList(connection.getConferenceables());
    Bundle bundle = connection.getExtras();
    ParcelableConnection parcelableConnection = new ParcelableConnection(phoneAccountHandle, i, j, k, m, uri2, n, str5, i1, iVideoProvider, i2, bool1, bool2, l1, l2, statusHints, disconnectCause, list, bundle, connection.getCallerNumberVerificationStatus());
    connectionServiceAdapter.handleCreateConnectionComplete(paramString, paramConnectionRequest, parcelableConnection);
    if (paramBoolean1 && paramConnectionRequest.shouldShowIncomingCallUi() && bool)
      connection.onShowIncomingCallUi(); 
    if (paramBoolean2)
      triggerConferenceRecalculate(); 
  }
  
  private void createConnectionFailed(PhoneAccountHandle paramPhoneAccountHandle, String paramString, ConnectionRequest paramConnectionRequest, boolean paramBoolean) {
    Log.i(this, "createConnectionFailed %s", new Object[] { paramString });
    if (paramBoolean) {
      onCreateIncomingConnectionFailed(paramPhoneAccountHandle, paramConnectionRequest);
    } else {
      onCreateOutgoingConnectionFailed(paramPhoneAccountHandle, paramConnectionRequest);
    } 
  }
  
  private void createConferenceFailed(PhoneAccountHandle paramPhoneAccountHandle, String paramString, ConnectionRequest paramConnectionRequest, boolean paramBoolean) {
    Log.i(this, "createConferenceFailed %s", new Object[] { paramString });
    if (paramBoolean) {
      onCreateIncomingConferenceFailed(paramPhoneAccountHandle, paramConnectionRequest);
    } else {
      onCreateOutgoingConferenceFailed(paramPhoneAccountHandle, paramConnectionRequest);
    } 
  }
  
  private void handoverFailed(String paramString, ConnectionRequest paramConnectionRequest, int paramInt) {
    Log.i(this, "handoverFailed %s", new Object[] { paramString });
    onHandoverFailed(paramConnectionRequest, paramInt);
  }
  
  private void notifyCreateConnectionComplete(String paramString) {
    Log.i(this, "notifyCreateConnectionComplete %s", new Object[] { paramString });
    if (paramString == null) {
      Log.w(this, "notifyCreateConnectionComplete: callId is null.", new Object[0]);
      return;
    } 
    onCreateConnectionComplete(findConnectionForAction(paramString, "notifyCreateConnectionComplete"));
  }
  
  private void notifyCreateConferenceComplete(String paramString) {
    Log.i(this, "notifyCreateConferenceComplete %s", new Object[] { paramString });
    if (paramString == null) {
      Log.w(this, "notifyCreateConferenceComplete: callId is null.", new Object[0]);
      return;
    } 
    onCreateConferenceComplete(findConferenceForAction(paramString, "notifyCreateConferenceComplete"));
  }
  
  private void abort(String paramString) {
    Log.i(this, "abort %s", new Object[] { paramString });
    if (!this.mConnectionById.containsKey(paramString)) {
      List<Runnable> list = this.mPreInitializationConnectionRequests;
      if (list != null)
        list.clear(); 
    } 
    findConnectionForAction(paramString, "abort").onAbort();
  }
  
  private void answerVideo(String paramString, int paramInt) {
    Log.i(this, "answerVideo %s", new Object[] { paramString });
    doAnswer(paramString, paramInt);
  }
  
  private void answer(String paramString) {
    Log.i(this, "answer %s", new Object[] { paramString });
    doAnswer(paramString, 0);
  }
  
  public void doAnswer(String paramString, int paramInt) {
    if (this.mConnectionById.containsKey(paramString)) {
      findConnectionForAction(paramString, "answer").onAnswer(paramInt);
    } else {
      findConferenceForAction(paramString, "answer").onAnswer(paramInt);
    } 
  }
  
  private void deflect(String paramString, Uri paramUri) {
    Log.i(this, "deflect %s", new Object[] { paramString });
    findConnectionForAction(paramString, "deflect").onDeflect(paramUri);
  }
  
  private void reject(String paramString) {
    Log.i(this, "reject %s", new Object[] { paramString });
    if (this.mConnectionById.containsKey(paramString)) {
      findConnectionForAction(paramString, "reject").onReject();
    } else {
      findConferenceForAction(paramString, "reject").onReject();
    } 
  }
  
  private void reject(String paramString1, String paramString2) {
    Log.i(this, "reject %s with message", new Object[] { paramString1 });
    findConnectionForAction(paramString1, "reject").onReject(paramString2);
  }
  
  private void reject(String paramString, int paramInt) {
    Log.i(this, "reject %s with reason %d", new Object[] { paramString, Integer.valueOf(paramInt) });
    findConnectionForAction(paramString, "reject").onReject(paramInt);
  }
  
  private void transfer(String paramString, Uri paramUri, boolean paramBoolean) {
    Log.i(this, "transfer %s", new Object[] { paramString });
    findConnectionForAction(paramString, "transfer").onTransfer(paramUri, paramBoolean);
  }
  
  private void consultativeTransfer(String paramString1, String paramString2) {
    Log.i(this, "consultativeTransfer %s", new Object[] { paramString1 });
    Connection connection1 = findConnectionForAction(paramString1, "consultativeTransfer");
    Connection connection2 = findConnectionForAction(paramString2, " consultativeTransfer");
    connection1.onTransfer(connection2);
  }
  
  private void silence(String paramString) {
    Log.i(this, "silence %s", new Object[] { paramString });
    findConnectionForAction(paramString, "silence").onSilence();
  }
  
  private void disconnect(String paramString) {
    Log.i(this, "disconnect %s", new Object[] { paramString });
    if (this.mConnectionById.containsKey(paramString)) {
      findConnectionForAction(paramString, "disconnect").onDisconnect();
    } else {
      findConferenceForAction(paramString, "disconnect").onDisconnect();
    } 
  }
  
  private void hold(String paramString) {
    Log.i(this, "hold %s", new Object[] { paramString });
    if (this.mConnectionById.containsKey(paramString)) {
      findConnectionForAction(paramString, "hold").onHold();
    } else {
      findConferenceForAction(paramString, "hold").onHold();
    } 
  }
  
  private void unhold(String paramString) {
    Log.i(this, "unhold %s", new Object[] { paramString });
    if (this.mConnectionById.containsKey(paramString)) {
      findConnectionForAction(paramString, "unhold").onUnhold();
    } else {
      findConferenceForAction(paramString, "unhold").onUnhold();
    } 
  }
  
  private void onCallAudioStateChanged(String paramString, CallAudioState paramCallAudioState) {
    Log.i(this, "onAudioStateChanged %s %s", new Object[] { paramString, paramCallAudioState });
    if (this.mConnectionById.containsKey(paramString)) {
      findConnectionForAction(paramString, "onCallAudioStateChanged").setCallAudioState(paramCallAudioState);
    } else {
      findConferenceForAction(paramString, "onCallAudioStateChanged").setCallAudioState(paramCallAudioState);
    } 
  }
  
  private void playDtmfTone(String paramString, char paramChar) {
    Log.i(this, "playDtmfTone %s %c", new Object[] { paramString, Character.valueOf(paramChar) });
    if (this.mConnectionById.containsKey(paramString)) {
      findConnectionForAction(paramString, "playDtmfTone").onPlayDtmfTone(paramChar);
    } else {
      findConferenceForAction(paramString, "playDtmfTone").onPlayDtmfTone(paramChar);
    } 
  }
  
  private void stopDtmfTone(String paramString) {
    Log.i(this, "stopDtmfTone %s", new Object[] { paramString });
    if (this.mConnectionById.containsKey(paramString)) {
      findConnectionForAction(paramString, "stopDtmfTone").onStopDtmfTone();
    } else {
      findConferenceForAction(paramString, "stopDtmfTone").onStopDtmfTone();
    } 
  }
  
  private void conference(String paramString1, String paramString2) {
    Conference conference1;
    Log.i(this, "conference %s, %s", new Object[] { paramString1, paramString2 });
    Connection connection2 = findConnectionForAction(paramString2, "conference");
    Conference conference2 = getNullConference();
    if (connection2 == getNullConnection()) {
      Conference conference = findConferenceForAction(paramString2, "conference");
      conference2 = conference;
      if (conference == getNullConference()) {
        Log.w(this, "Connection2 or Conference2 missing in conference request %s.", new Object[] { paramString2 });
        return;
      } 
    } 
    Connection connection1 = findConnectionForAction(paramString1, "conference");
    if (connection1 == getNullConnection()) {
      conference1 = findConferenceForAction(paramString1, "addConnection");
      if (conference1 == getNullConference()) {
        Log.w(this, "Connection1 or Conference1 missing in conference request %s.", new Object[] { paramString1 });
      } else if (connection2 != getNullConnection()) {
        conference1.onMerge(connection2);
      } else {
        Log.wtf(this, "There can only be one conference and an attempt was made to merge two conferences.", new Object[0]);
        return;
      } 
    } else if (conference2 != getNullConference()) {
      conference2.onMerge((Connection)conference1);
    } else {
      onConference((Connection)conference1, connection2);
    } 
  }
  
  private void splitFromConference(String paramString) {
    Log.i(this, "splitFromConference(%s)", new Object[] { paramString });
    Connection connection = findConnectionForAction(paramString, "splitFromConference");
    if (connection == getNullConnection()) {
      Log.w(this, "Connection missing in conference request %s.", new Object[] { paramString });
      return;
    } 
    Conference conference = connection.getConference();
    if (conference != null)
      conference.onSeparate(connection); 
  }
  
  private void mergeConference(String paramString) {
    Log.i(this, "mergeConference(%s)", new Object[] { paramString });
    Conference conference = findConferenceForAction(paramString, "mergeConference");
    if (conference != null)
      conference.onMerge(); 
  }
  
  private void swapConference(String paramString) {
    Log.i(this, "swapConference(%s)", new Object[] { paramString });
    Conference conference = findConferenceForAction(paramString, "swapConference");
    if (conference != null)
      conference.onSwap(); 
  }
  
  private void addConferenceParticipants(String paramString, List<Uri> paramList) {
    Connection connection;
    Log.i(this, "addConferenceParticipants(%s)", new Object[] { paramString });
    if (this.mConnectionById.containsKey(paramString)) {
      connection = findConnectionForAction(paramString, "addConferenceParticipants");
      connection.onAddConferenceParticipants(paramList);
    } else {
      Conference conference = findConferenceForAction((String)connection, "addConferenceParticipants");
      conference.onAddConferenceParticipants(paramList);
    } 
  }
  
  private void pullExternalCall(String paramString) {
    Log.i(this, "pullExternalCall(%s)", new Object[] { paramString });
    Connection connection = findConnectionForAction(paramString, "pullExternalCall");
    if (connection != null)
      connection.onPullExternalCall(); 
  }
  
  private void sendCallEvent(String paramString1, String paramString2, Bundle paramBundle) {
    Log.i(this, "sendCallEvent(%s, %s)", new Object[] { paramString1, paramString2 });
    Connection connection = findConnectionForAction(paramString1, "sendCallEvent");
    if (connection != null)
      connection.onCallEvent(paramString2, paramBundle); 
  }
  
  private void notifyHandoverComplete(String paramString) {
    Log.i(this, "notifyHandoverComplete(%s)", new Object[] { paramString });
    Connection connection = findConnectionForAction(paramString, "notifyHandoverComplete");
    if (connection != null)
      connection.onHandoverComplete(); 
  }
  
  private void handleExtrasChanged(String paramString, Bundle paramBundle) {
    Log.i(this, "handleExtrasChanged(%s, %s)", new Object[] { paramString, paramBundle });
    if (this.mConnectionById.containsKey(paramString)) {
      findConnectionForAction(paramString, "handleExtrasChanged").handleExtrasChanged(paramBundle);
    } else if (this.mConferenceById.containsKey(paramString)) {
      findConferenceForAction(paramString, "handleExtrasChanged").handleExtrasChanged(paramBundle);
    } 
  }
  
  private void startRtt(String paramString, Connection.RttTextStream paramRttTextStream) {
    Log.i(this, "startRtt(%s)", new Object[] { paramString });
    if (this.mConnectionById.containsKey(paramString)) {
      findConnectionForAction(paramString, "startRtt").onStartRtt(paramRttTextStream);
    } else if (this.mConferenceById.containsKey(paramString)) {
      Log.w(this, "startRtt called on a conference.", new Object[0]);
    } 
  }
  
  private void stopRtt(String paramString) {
    Log.i(this, "stopRtt(%s)", new Object[] { paramString });
    if (this.mConnectionById.containsKey(paramString)) {
      findConnectionForAction(paramString, "stopRtt").onStopRtt();
    } else if (this.mConferenceById.containsKey(paramString)) {
      Log.w(this, "stopRtt called on a conference.", new Object[0]);
    } 
  }
  
  private void handleRttUpgradeResponse(String paramString, Connection.RttTextStream paramRttTextStream) {
    Connection connection;
    boolean bool;
    if (paramRttTextStream == null) {
      bool = true;
    } else {
      bool = false;
    } 
    Log.i(this, "handleRttUpgradeResponse(%s, %s)", new Object[] { paramString, Boolean.valueOf(bool) });
    if (this.mConnectionById.containsKey(paramString)) {
      connection = findConnectionForAction(paramString, "handleRttUpgradeResponse");
      connection.handleRttUpgradeResponse(paramRttTextStream);
    } else if (this.mConferenceById.containsKey(connection)) {
      Log.w(this, "handleRttUpgradeResponse called on a conference.", new Object[0]);
    } 
  }
  
  private void onPostDialContinue(String paramString, boolean paramBoolean) {
    Log.i(this, "onPostDialContinue(%s)", new Object[] { paramString });
    findConnectionForAction(paramString, "stopDtmfTone").onPostDialContinue(paramBoolean);
  }
  
  private void onAdapterAttached() {
    if (this.mAreAccountsInitialized)
      return; 
    String str = getOpPackageName();
    this.mAdapter.queryRemoteConnectionServices((RemoteServiceCallback)new RemoteServiceCallback.Stub() {
          final ConnectionService this$0;
          
          public void onResult(List<ComponentName> param1List, List<IBinder> param1List1) {
            Handler handler = ConnectionService.this.mHandler;
            Object object = new Object(this, "oAA.qRCS.oR", null, param1List, param1List1);
            object = object.prepare();
            handler.post((Runnable)object);
          }
          
          public void onError() {
            Handler handler = ConnectionService.this.mHandler;
            Object object = new Object(this, "oAA.qRCS.oE", null);
            object = object.prepare();
            handler.post((Runnable)object);
          }
        }str);
  }
  
  public final RemoteConnection createRemoteIncomingConnection(PhoneAccountHandle paramPhoneAccountHandle, ConnectionRequest paramConnectionRequest) {
    return this.mRemoteConnectionManager.createRemoteConnection(paramPhoneAccountHandle, paramConnectionRequest, true);
  }
  
  public final RemoteConnection createRemoteOutgoingConnection(PhoneAccountHandle paramPhoneAccountHandle, ConnectionRequest paramConnectionRequest) {
    return this.mRemoteConnectionManager.createRemoteConnection(paramPhoneAccountHandle, paramConnectionRequest, false);
  }
  
  public final void conferenceRemoteConnections(RemoteConnection paramRemoteConnection1, RemoteConnection paramRemoteConnection2) {
    this.mRemoteConnectionManager.conferenceRemoteConnections(paramRemoteConnection1, paramRemoteConnection2);
  }
  
  public final void addConference(Conference paramConference) {
    Log.d(this, "addConference: conference=%s", new Object[] { paramConference });
    String str = addConferenceInternal(paramConference);
    if (str != null) {
      IVideoProvider iVideoProvider;
      ArrayList<String> arrayList = new ArrayList(2);
      for (Connection connection : paramConference.getConnections()) {
        if (this.mIdByConnection.containsKey(connection))
          arrayList.add(this.mIdByConnection.get(connection)); 
      } 
      paramConference.setTelecomCallId(str);
      ParcelableConference.Builder builder3 = new ParcelableConference.Builder(paramConference.getPhoneAccountHandle(), paramConference.getState());
      builder3 = builder3.setConnectionCapabilities(paramConference.getConnectionCapabilities());
      builder3 = builder3.setConnectionProperties(paramConference.getConnectionProperties());
      builder3 = builder3.setConnectionIds(arrayList);
      if (paramConference.getVideoProvider() == null) {
        arrayList = null;
      } else {
        iVideoProvider = paramConference.getVideoProvider().getInterface();
      } 
      int i = paramConference.getVideoState();
      ParcelableConference.Builder builder2 = builder3.setVideoAttributes(iVideoProvider, i);
      long l1 = paramConference.getConnectTimeMillis();
      long l2 = paramConference.getConnectionStartElapsedRealtimeMillis();
      builder2 = builder2.setConnectTimeMillis(l1, l2);
      builder2 = builder2.setStatusHints(paramConference.getStatusHints());
      builder2 = builder2.setExtras(paramConference.getExtras());
      builder3 = builder2.setAddress(paramConference.getAddress(), paramConference.getAddressPresentation());
      String str1 = paramConference.getCallerDisplayName();
      i = paramConference.getCallerDisplayNamePresentation();
      ParcelableConference.Builder builder1 = builder3.setCallerDisplayName(str1, i);
      builder1 = builder1.setDisconnectCause(paramConference.getDisconnectCause());
      builder1 = builder1.setRingbackRequested(paramConference.isRingbackRequested());
      builder1 = builder1.setCallDirection(paramConference.getCallDirection());
      ParcelableConference parcelableConference = builder1.build();
      this.mAdapter.addConferenceCall(str, parcelableConference);
      this.mAdapter.setVideoProvider(str, paramConference.getVideoProvider());
      this.mAdapter.setVideoState(str, paramConference.getVideoState());
      if (!paramConference.isMultiparty())
        this.mAdapter.setConferenceState(str, paramConference.isMultiparty()); 
      for (Connection connection : paramConference.getConnections()) {
        String str2 = this.mIdByConnection.get(connection);
        if (str2 != null)
          this.mAdapter.setIsConferenced(str2, str); 
      } 
      onConferenceAdded(paramConference);
    } 
  }
  
  public final void addExistingConnection(PhoneAccountHandle paramPhoneAccountHandle, Connection paramConnection) {
    addExistingConnection(paramPhoneAccountHandle, paramConnection, null);
  }
  
  public final void connectionServiceFocusReleased() {
    this.mAdapter.onConnectionServiceFocusReleased();
  }
  
  @SystemApi
  public final void addExistingConnection(PhoneAccountHandle paramPhoneAccountHandle, Connection paramConnection, Conference paramConference) {
    String str = addExistingConnectionInternal(paramPhoneAccountHandle, paramConnection);
    if (str != null) {
      IVideoProvider iVideoProvider;
      ArrayList<String> arrayList = new ArrayList(0);
      if (paramConference != null) {
        String str2 = this.mIdByConference.get(paramConference);
      } else {
        paramConference = null;
      } 
      int i = paramConnection.getState();
      int j = paramConnection.getConnectionCapabilities();
      int k = paramConnection.getConnectionProperties();
      int m = paramConnection.getSupportedAudioRoutes();
      Uri uri = paramConnection.getAddress();
      int n = paramConnection.getAddressPresentation();
      String str1 = paramConnection.getCallerDisplayName();
      int i1 = paramConnection.getCallerDisplayNamePresentation();
      if (paramConnection.getVideoProvider() == null) {
        iVideoProvider = null;
      } else {
        iVideoProvider = paramConnection.getVideoProvider().getInterface();
      } 
      int i2 = paramConnection.getVideoState();
      boolean bool1 = paramConnection.isRingbackRequested();
      boolean bool2 = paramConnection.getAudioModeIsVoip();
      long l1 = paramConnection.getConnectTimeMillis();
      long l2 = paramConnection.getConnectionStartElapsedRealtimeMillis();
      StatusHints statusHints = paramConnection.getStatusHints();
      DisconnectCause disconnectCause = paramConnection.getDisconnectCause();
      Bundle bundle = paramConnection.getExtras();
      ParcelableConnection parcelableConnection = new ParcelableConnection(paramPhoneAccountHandle, i, j, k, m, uri, n, str1, i1, iVideoProvider, i2, bool1, bool2, l1, l2, statusHints, disconnectCause, arrayList, bundle, (String)paramConference, paramConnection.getCallDirection(), 0);
      this.mAdapter.addExistingConnection(str, parcelableConnection);
    } 
  }
  
  public final Collection<Connection> getAllConnections() {
    return this.mConnectionById.values();
  }
  
  public final Collection<Conference> getAllConferences() {
    return this.mConferenceById.values();
  }
  
  public Connection onCreateIncomingConnection(PhoneAccountHandle paramPhoneAccountHandle, ConnectionRequest paramConnectionRequest) {
    return null;
  }
  
  public Conference onCreateIncomingConference(PhoneAccountHandle paramPhoneAccountHandle, ConnectionRequest paramConnectionRequest) {
    return null;
  }
  
  public void onCreateConnectionComplete(Connection paramConnection) {}
  
  public void onCreateConferenceComplete(Conference paramConference) {}
  
  public void onCreateIncomingConnectionFailed(PhoneAccountHandle paramPhoneAccountHandle, ConnectionRequest paramConnectionRequest) {}
  
  public void onCreateOutgoingConnectionFailed(PhoneAccountHandle paramPhoneAccountHandle, ConnectionRequest paramConnectionRequest) {}
  
  public void onCreateIncomingConferenceFailed(PhoneAccountHandle paramPhoneAccountHandle, ConnectionRequest paramConnectionRequest) {}
  
  public void onCreateOutgoingConferenceFailed(PhoneAccountHandle paramPhoneAccountHandle, ConnectionRequest paramConnectionRequest) {}
  
  public void triggerConferenceRecalculate() {}
  
  public Connection onCreateOutgoingConnection(PhoneAccountHandle paramPhoneAccountHandle, ConnectionRequest paramConnectionRequest) {
    return null;
  }
  
  public Conference onCreateOutgoingConference(PhoneAccountHandle paramPhoneAccountHandle, ConnectionRequest paramConnectionRequest) {
    return null;
  }
  
  public Connection onCreateOutgoingHandoverConnection(PhoneAccountHandle paramPhoneAccountHandle, ConnectionRequest paramConnectionRequest) {
    return null;
  }
  
  public Connection onCreateIncomingHandoverConnection(PhoneAccountHandle paramPhoneAccountHandle, ConnectionRequest paramConnectionRequest) {
    return null;
  }
  
  public void onHandoverFailed(ConnectionRequest paramConnectionRequest, int paramInt) {}
  
  public Connection onCreateUnknownConnection(PhoneAccountHandle paramPhoneAccountHandle, ConnectionRequest paramConnectionRequest) {
    return null;
  }
  
  public void onConference(Connection paramConnection1, Connection paramConnection2) {}
  
  public void onConnectionAdded(Connection paramConnection) {}
  
  public void onConnectionRemoved(Connection paramConnection) {}
  
  public void onConferenceAdded(Conference paramConference) {}
  
  public void onConferenceRemoved(Conference paramConference) {}
  
  public void onRemoteConferenceAdded(RemoteConference paramRemoteConference) {}
  
  public void onRemoteExistingConnectionAdded(RemoteConnection paramRemoteConnection) {}
  
  public void onConnectionServiceFocusLost() {}
  
  public void onConnectionServiceFocusGained() {}
  
  public boolean containsConference(Conference paramConference) {
    return this.mIdByConference.containsKey(paramConference);
  }
  
  void addRemoteConference(RemoteConference paramRemoteConference) {
    onRemoteConferenceAdded(paramRemoteConference);
  }
  
  void addRemoteExistingConnection(RemoteConnection paramRemoteConnection) {
    onRemoteExistingConnectionAdded(paramRemoteConnection);
  }
  
  private void onAccountsInitialized() {
    this.mAreAccountsInitialized = true;
    for (Runnable runnable : this.mPreInitializationConnectionRequests)
      runnable.run(); 
    this.mPreInitializationConnectionRequests.clear();
  }
  
  private String addExistingConnectionInternal(PhoneAccountHandle paramPhoneAccountHandle, Connection paramConnection) {
    String str;
    if (paramConnection.getExtras() != null) {
      Bundle bundle = paramConnection.getExtras();
      if (bundle.containsKey("android.telecom.extra.ORIGINAL_CONNECTION_ID")) {
        str = paramConnection.getExtras().getString("android.telecom.extra.ORIGINAL_CONNECTION_ID");
        String str1 = paramConnection.getTelecomCallId();
        Log.d(this, "addExistingConnectionInternal - conn %s reusing original id %s", new Object[] { str1, str });
        addConnection(paramPhoneAccountHandle, str, paramConnection);
        return str;
      } 
    } 
    if (paramPhoneAccountHandle == null) {
      str = UUID.randomUUID().toString();
    } else {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramPhoneAccountHandle.getComponentName().getClassName());
      stringBuilder.append("@");
      stringBuilder.append(getNextCallId());
      str = stringBuilder.toString();
    } 
    addConnection(paramPhoneAccountHandle, str, paramConnection);
    return str;
  }
  
  private void addConnection(PhoneAccountHandle paramPhoneAccountHandle, String paramString, Connection paramConnection) {
    paramConnection.setTelecomCallId(paramString);
    this.mConnectionById.put(paramString, paramConnection);
    this.mIdByConnection.put(paramConnection, paramString);
    paramConnection.addConnectionListener(this.mConnectionListener);
    paramConnection.setConnectionService(this);
    paramConnection.setPhoneAccountHandle(paramPhoneAccountHandle);
    onConnectionAdded(paramConnection);
  }
  
  protected void removeConnection(Connection paramConnection) {
    paramConnection.unsetConnectionService(this);
    paramConnection.removeConnectionListener(this.mConnectionListener);
    String str = this.mIdByConnection.get(paramConnection);
    if (str != null) {
      this.mConnectionById.remove(str);
      this.mIdByConnection.remove(paramConnection);
      this.mAdapter.removeCall(str);
      onConnectionRemoved(paramConnection);
    } 
  }
  
  private String addConferenceInternal(Conference paramConference) {
    String str1 = null;
    String str2 = str1;
    if (paramConference.getExtras() != null) {
      Bundle bundle = paramConference.getExtras();
      str2 = str1;
      if (bundle.containsKey("android.telecom.extra.ORIGINAL_CONNECTION_ID")) {
        str2 = paramConference.getExtras().getString("android.telecom.extra.ORIGINAL_CONNECTION_ID");
        str1 = paramConference.getTelecomCallId();
        Log.d(this, "addConferenceInternal: conf %s reusing original id %s", new Object[] { str1, str2 });
      } 
    } 
    if (this.mIdByConference.containsKey(paramConference)) {
      Log.w(this, "Re-adding an existing conference: %s.", new Object[] { paramConference });
    } else if (paramConference != null) {
      if (str2 == null)
        str2 = UUID.randomUUID().toString(); 
      this.mConferenceById.put(str2, paramConference);
      this.mIdByConference.put(paramConference, str2);
      paramConference.addListener(this.mConferenceListener);
      return str2;
    } 
    return null;
  }
  
  private void removeConference(Conference paramConference) {
    if (this.mIdByConference.containsKey(paramConference)) {
      paramConference.removeListener(this.mConferenceListener);
      String str = this.mIdByConference.get(paramConference);
      this.mConferenceById.remove(str);
      this.mIdByConference.remove(paramConference);
      this.mAdapter.removeCall(str);
      onConferenceRemoved(paramConference);
    } 
  }
  
  private Connection findConnectionForAction(String paramString1, String paramString2) {
    if (paramString1 != null && this.mConnectionById.containsKey(paramString1))
      return this.mConnectionById.get(paramString1); 
    notFindConnectionThroughCallId(false, paramString1, paramString2);
    Log.w(this, "%s - Cannot find Connection %s", new Object[] { paramString2, paramString1 });
    return getNullConnection();
  }
  
  static Connection getNullConnection() {
    // Byte code:
    //   0: ldc android/telecom/ConnectionService
    //   2: monitorenter
    //   3: getstatic android/telecom/ConnectionService.sNullConnection : Landroid/telecom/Connection;
    //   6: ifnonnull -> 21
    //   9: new android/telecom/ConnectionService$6
    //   12: astore_0
    //   13: aload_0
    //   14: invokespecial <init> : ()V
    //   17: aload_0
    //   18: putstatic android/telecom/ConnectionService.sNullConnection : Landroid/telecom/Connection;
    //   21: getstatic android/telecom/ConnectionService.sNullConnection : Landroid/telecom/Connection;
    //   24: astore_0
    //   25: ldc android/telecom/ConnectionService
    //   27: monitorexit
    //   28: aload_0
    //   29: areturn
    //   30: astore_0
    //   31: ldc android/telecom/ConnectionService
    //   33: monitorexit
    //   34: aload_0
    //   35: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #3204	-> 3
    //   #3205	-> 9
    //   #3207	-> 21
    //   #3203	-> 30
    // Exception table:
    //   from	to	target	type
    //   3	9	30	finally
    //   9	21	30	finally
    //   21	25	30	finally
  }
  
  private Conference findConferenceForAction(String paramString1, String paramString2) {
    if (this.mConferenceById.containsKey(paramString1))
      return this.mConferenceById.get(paramString1); 
    notFindConnectionThroughCallId(true, paramString1, paramString2);
    Log.w(this, "%s - Cannot find conference %s", new Object[] { paramString2, paramString1 });
    return getNullConference();
  }
  
  private List<String> createConnectionIdList(List<Connection> paramList) {
    ArrayList<String> arrayList = new ArrayList();
    for (Connection connection : paramList) {
      if (this.mIdByConnection.containsKey(connection))
        arrayList.add(this.mIdByConnection.get(connection)); 
    } 
    Collections.sort(arrayList);
    return arrayList;
  }
  
  private List<String> createIdList(List<Conferenceable> paramList) {
    ArrayList<String> arrayList = new ArrayList();
    for (Conferenceable conferenceable : paramList) {
      if (conferenceable instanceof Connection) {
        conferenceable = conferenceable;
        if (this.mIdByConnection.containsKey(conferenceable))
          arrayList.add(this.mIdByConnection.get(conferenceable)); 
        continue;
      } 
      if (conferenceable instanceof Conference) {
        conferenceable = conferenceable;
        if (this.mIdByConference.containsKey(conferenceable))
          arrayList.add(this.mIdByConference.get(conferenceable)); 
      } 
    } 
    Collections.sort(arrayList);
    return arrayList;
  }
  
  private Conference getNullConference() {
    if (this.sNullConference == null)
      this.sNullConference = (Conference)new Object(this, null); 
    return this.sNullConference;
  }
  
  private void endAllConnections() {
    for (Connection connection : this.mIdByConnection.keySet()) {
      if (connection.getConference() == null)
        connection.onDisconnect(); 
    } 
    for (Conference conference : this.mIdByConference.keySet())
      conference.onDisconnect(); 
  }
  
  private int getNextCallId() {
    synchronized (this.mIdSyncRoot) {
      int i = this.mId + 1;
      return i;
    } 
  }
  
  public Handler getHandler() {
    return this.mHandler;
  }
  
  private final String LOG_TAG_FROM = "IConnectionService-->";
  
  public static final String EXTRA_IS_HANDOVER = "android.telecom.extra.IS_HANDOVER";
  
  private static final int MSG_ABORT = 3;
  
  private static final int MSG_ADD_CONNECTION_SERVICE_ADAPTER = 1;
  
  private static final int MSG_ADD_PARTICIPANT = 39;
  
  private static final int MSG_ANSWER = 4;
  
  private static final int MSG_ANSWER_VIDEO = 17;
  
  private static final int MSG_CONFERENCE = 12;
  
  private static final int MSG_CONNECTION_SERVICE_FOCUS_GAINED = 31;
  
  private static final int MSG_CONNECTION_SERVICE_FOCUS_LOST = 30;
  
  private static final int MSG_CREATE_CONFERENCE = 35;
  
  private static final int MSG_CREATE_CONFERENCE_COMPLETE = 36;
  
  private static final int MSG_CREATE_CONFERENCE_FAILED = 37;
  
  private static final int MSG_CREATE_CONNECTION = 2;
  
  private static final int MSG_CREATE_CONNECTION_COMPLETE = 29;
  
  private static final int MSG_CREATE_CONNECTION_FAILED = 25;
  
  private static final int MSG_DEFLECT = 34;
  
  private static final int MSG_DISCONNECT = 6;
  
  private static final int MSG_EXPLICIT_CALL_TRANSFER = 40;
  
  private static final int MSG_EXPLICIT_CALL_TRANSFER_CONSULTATIVE = 41;
  
  private static final int MSG_HANDOVER_COMPLETE = 33;
  
  private static final int MSG_HANDOVER_FAILED = 32;
  
  private static final int MSG_HOLD = 7;
  
  private static final int MSG_MERGE_CONFERENCE = 18;
  
  private static final int MSG_ON_CALL_AUDIO_STATE_CHANGED = 9;
  
  private static final int MSG_ON_EXTRAS_CHANGED = 24;
  
  private static final int MSG_ON_POST_DIAL_CONTINUE = 14;
  
  private static final int MSG_ON_START_RTT = 26;
  
  private static final int MSG_ON_STOP_RTT = 27;
  
  private static final int MSG_PLAY_DTMF_TONE = 10;
  
  private static final int MSG_PULL_EXTERNAL_CALL = 22;
  
  private static final int MSG_REJECT = 5;
  
  private static final int MSG_REJECT_WITH_MESSAGE = 20;
  
  private static final int MSG_REJECT_WITH_REASON = 38;
  
  private static final int MSG_REMOVE_CONNECTION_SERVICE_ADAPTER = 16;
  
  private static final int MSG_RTT_UPGRADE_RESPONSE = 28;
  
  private static final int MSG_SEND_CALL_EVENT = 23;
  
  private static final int MSG_SILENCE = 21;
  
  private static final int MSG_SPLIT_FROM_CONFERENCE = 13;
  
  private static final int MSG_STOP_DTMF_TONE = 11;
  
  private static final int MSG_SWAP_CONFERENCE = 19;
  
  private static final int MSG_UNHOLD = 8;
  
  public static final String SERVICE_INTERFACE = "android.telecom.ConnectionService";
  
  private static final String SESSION_ABORT = "CS.ab";
  
  private static final String SESSION_ADD_CS_ADAPTER = "CS.aCSA";
  
  private static final String SESSION_ADD_PARTICIPANT = "CS.aP";
  
  private static final String SESSION_ANSWER = "CS.an";
  
  private static final String SESSION_ANSWER_VIDEO = "CS.anV";
  
  private static final String SESSION_CALL_AUDIO_SC = "CS.cASC";
  
  private static final String SESSION_CONFERENCE = "CS.c";
  
  private static final String SESSION_CONNECTION_SERVICE_FOCUS_GAINED = "CS.cSFG";
  
  private static final String SESSION_CONNECTION_SERVICE_FOCUS_LOST = "CS.cSFL";
  
  private static final String SESSION_CONSULTATIVE_TRANSFER = "CS.cTrans";
  
  private static final String SESSION_CREATE_CONF = "CS.crConf";
  
  private static final String SESSION_CREATE_CONF_COMPLETE = "CS.crConfC";
  
  private static final String SESSION_CREATE_CONF_FAILED = "CS.crConfF";
  
  private static final String SESSION_CREATE_CONN = "CS.crCo";
  
  private static final String SESSION_CREATE_CONN_COMPLETE = "CS.crCoC";
  
  private static final String SESSION_CREATE_CONN_FAILED = "CS.crCoF";
  
  private static final String SESSION_DEFLECT = "CS.def";
  
  private static final String SESSION_DISCONNECT = "CS.d";
  
  private static final String SESSION_EXTRAS_CHANGED = "CS.oEC";
  
  private static final String SESSION_HANDLER = "H.";
  
  private static final String SESSION_HANDOVER_COMPLETE = "CS.hC";
  
  private static final String SESSION_HANDOVER_FAILED = "CS.haF";
  
  private static final String SESSION_HOLD = "CS.h";
  
  private static final String SESSION_MERGE_CONFERENCE = "CS.mC";
  
  private static final String SESSION_PLAY_DTMF = "CS.pDT";
  
  private static final String SESSION_POST_DIAL_CONT = "CS.oPDC";
  
  private static final String SESSION_PULL_EXTERNAL_CALL = "CS.pEC";
  
  private static final String SESSION_REJECT = "CS.r";
  
  private static final String SESSION_REJECT_MESSAGE = "CS.rWM";
  
  private static final String SESSION_REMOVE_CS_ADAPTER = "CS.rCSA";
  
  private static final String SESSION_RTT_UPGRADE_RESPONSE = "CS.rTRUR";
  
  private static final String SESSION_SEND_CALL_EVENT = "CS.sCE";
  
  private static final String SESSION_SILENCE = "CS.s";
  
  private static final String SESSION_SPLIT_CONFERENCE = "CS.sFC";
  
  private static final String SESSION_START_RTT = "CS.+RTT";
  
  private static final String SESSION_STOP_DTMF = "CS.sDT";
  
  private static final String SESSION_STOP_RTT = "CS.-RTT";
  
  private static final String SESSION_SWAP_CONFERENCE = "CS.sC";
  
  private static final String SESSION_TRANSFER = "CS.trans";
  
  private static final String SESSION_UNHOLD = "CS.u";
  
  private static final String SESSION_UPDATE_RTT_PIPES = "CS.uRTT";
  
  private static Connection sNullConnection;
  
  private Conference sNullConference;
  
  void logDebug(String paramString) {
    Log.i("IConnectionService-->", paramString, new Object[0]);
  }
  
  public void notFindConnectionThroughCallId(boolean paramBoolean, String paramString1, String paramString2) {}
}
