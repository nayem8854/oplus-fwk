package android.telecom;

import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Objects;

public final class PhoneAccountSuggestion implements Parcelable {
  public PhoneAccountSuggestion(PhoneAccountHandle paramPhoneAccountHandle, int paramInt, boolean paramBoolean) {
    this.mHandle = paramPhoneAccountHandle;
    this.mReason = paramInt;
    this.mShouldAutoSelect = paramBoolean;
  }
  
  private PhoneAccountSuggestion(Parcel paramParcel) {
    boolean bool;
    this.mHandle = (PhoneAccountHandle)paramParcel.readParcelable(PhoneAccountHandle.class.getClassLoader());
    this.mReason = paramParcel.readInt();
    if (paramParcel.readByte() != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mShouldAutoSelect = bool;
  }
  
  public static final Parcelable.Creator<PhoneAccountSuggestion> CREATOR = new Parcelable.Creator<PhoneAccountSuggestion>() {
      public PhoneAccountSuggestion createFromParcel(Parcel param1Parcel) {
        return new PhoneAccountSuggestion(param1Parcel);
      }
      
      public PhoneAccountSuggestion[] newArray(int param1Int) {
        return new PhoneAccountSuggestion[param1Int];
      }
    };
  
  public static final int REASON_FREQUENT = 2;
  
  public static final int REASON_INTRA_CARRIER = 1;
  
  public static final int REASON_NONE = 0;
  
  public static final int REASON_OTHER = 4;
  
  public static final int REASON_USER_SET = 3;
  
  private PhoneAccountHandle mHandle;
  
  private int mReason;
  
  private boolean mShouldAutoSelect;
  
  public PhoneAccountHandle getPhoneAccountHandle() {
    return this.mHandle;
  }
  
  public int getReason() {
    return this.mReason;
  }
  
  public boolean shouldAutoSelect() {
    return this.mShouldAutoSelect;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable(this.mHandle, paramInt);
    paramParcel.writeInt(this.mReason);
    paramParcel.writeByte((byte)this.mShouldAutoSelect);
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    PhoneAccountSuggestion phoneAccountSuggestion = (PhoneAccountSuggestion)paramObject;
    if (this.mReason == phoneAccountSuggestion.mReason && this.mShouldAutoSelect == phoneAccountSuggestion.mShouldAutoSelect) {
      paramObject = this.mHandle;
      PhoneAccountHandle phoneAccountHandle = phoneAccountSuggestion.mHandle;
      if (Objects.equals(paramObject, phoneAccountHandle))
        return null; 
    } 
    return false;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { this.mHandle, Integer.valueOf(this.mReason), Boolean.valueOf(this.mShouldAutoSelect) });
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class SuggestionReason implements Annotation {}
}
