package android.telecom;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public abstract class TimedEvent<T> {
  public static <T> Map<T, Double> averageTimings(Collection<? extends TimedEvent<T>> paramCollection) {
    HashMap<Object, Object> hashMap1 = new HashMap<>();
    HashMap<Object, Object> hashMap2 = new HashMap<>();
    for (TimedEvent<T> timedEvent : paramCollection) {
      if (hashMap1.containsKey(timedEvent.getKey())) {
        hashMap1.put(timedEvent.getKey(), Integer.valueOf(((Integer)hashMap1.get(timedEvent.getKey())).intValue() + 1));
        hashMap2.put(timedEvent.getKey(), Double.valueOf(((Double)hashMap2.get(timedEvent.getKey())).doubleValue() + timedEvent.getTime()));
        continue;
      } 
      hashMap1.put(timedEvent.getKey(), Integer.valueOf(1));
      hashMap2.put(timedEvent.getKey(), Double.valueOf(timedEvent.getTime()));
    } 
    for (Map.Entry<Object, Object> entry : hashMap2.entrySet())
      hashMap2.put(entry.getKey(), Double.valueOf(((Double)entry.getValue()).doubleValue() / ((Integer)hashMap1.get(entry.getKey())).intValue())); 
    return (Map)hashMap2;
  }
  
  public abstract T getKey();
  
  public abstract long getTime();
}
