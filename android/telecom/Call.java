package android.telecom;

import android.annotation.SystemApi;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;

public final class Call {
  @Deprecated
  public static final String AVAILABLE_PHONE_ACCOUNTS = "selectPhoneAccountAccounts";
  
  public static final String EVENT_HANDOVER_COMPLETE = "android.telecom.event.HANDOVER_COMPLETE";
  
  public static final String EVENT_HANDOVER_FAILED = "android.telecom.event.HANDOVER_FAILED";
  
  public static final String EVENT_HANDOVER_SOURCE_DISCONNECTED = "android.telecom.event.HANDOVER_SOURCE_DISCONNECTED";
  
  public static final String EVENT_REQUEST_HANDOVER = "android.telecom.event.REQUEST_HANDOVER";
  
  public static final String EXTRA_HANDOVER_EXTRAS = "android.telecom.extra.HANDOVER_EXTRAS";
  
  public static final String EXTRA_HANDOVER_PHONE_ACCOUNT_HANDLE = "android.telecom.extra.HANDOVER_PHONE_ACCOUNT_HANDLE";
  
  public static final String EXTRA_HANDOVER_VIDEO_STATE = "android.telecom.extra.HANDOVER_VIDEO_STATE";
  
  public static final String EXTRA_LAST_EMERGENCY_CALLBACK_TIME_MILLIS = "android.telecom.extra.LAST_EMERGENCY_CALLBACK_TIME_MILLIS";
  
  public static final String EXTRA_SILENT_RINGING_REQUESTED = "android.telecom.extra.SILENT_RINGING_REQUESTED";
  
  public static final String EXTRA_SUGGESTED_PHONE_ACCOUNTS = "android.telecom.extra.SUGGESTED_PHONE_ACCOUNTS";
  
  public static final int REJECT_REASON_DECLINED = 1;
  
  public static final int REJECT_REASON_UNWANTED = 2;
  
  public static final int STATE_ACTIVE = 4;
  
  public static final int STATE_AUDIO_PROCESSING = 12;
  
  public static final int STATE_CONNECTING = 9;
  
  public static final int STATE_DIALING = 1;
  
  public static final int STATE_DISCONNECTED = 7;
  
  public static final int STATE_DISCONNECTING = 10;
  
  public static final int STATE_HOLDING = 3;
  
  public static final int STATE_NEW = 0;
  
  @SystemApi
  @Deprecated
  public static final int STATE_PRE_DIAL_WAIT = 8;
  
  public static final int STATE_PULLING_CALL = 11;
  
  public static final int STATE_RINGING = 2;
  
  public static final int STATE_SELECT_PHONE_ACCOUNT = 8;
  
  public static final int STATE_SIMULATED_RINGING = 13;
  
  private String mActiveGenericConferenceChild;
  
  private final List<CallbackRecord<Callback>> mCallbackRecords;
  
  private String mCallingPackage;
  
  private List<String> mCannedTextResponses;
  
  private final List<Call> mChildren;
  
  private boolean mChildrenCached;
  
  public static class Details {
    public static final int CAPABILITY_ADD_PARTICIPANT = 33554432;
    
    public static final int CAPABILITY_CANNOT_DOWNGRADE_VIDEO_TO_AUDIO = 4194304;
    
    public static final int CAPABILITY_CAN_PAUSE_VIDEO = 1048576;
    
    public static final int CAPABILITY_CAN_PULL_CALL = 8388608;
    
    public static final int CAPABILITY_CAN_SEND_RESPONSE_VIA_CONNECTION = 2097152;
    
    public static final int CAPABILITY_CAN_UPGRADE_TO_VIDEO = 524288;
    
    public static final int CAPABILITY_DISCONNECT_FROM_CONFERENCE = 8192;
    
    public static final int CAPABILITY_HOLD = 1;
    
    public static final int CAPABILITY_MANAGE_CONFERENCE = 128;
    
    public static final int CAPABILITY_MERGE_CONFERENCE = 4;
    
    public static final int CAPABILITY_MUTE = 64;
    
    public static final int CAPABILITY_RESPOND_VIA_TEXT = 32;
    
    public static final int CAPABILITY_SEPARATE_FROM_CONFERENCE = 4096;
    
    public static final int CAPABILITY_SPEED_UP_MT_AUDIO = 262144;
    
    public static final int CAPABILITY_SUPPORTS_RTT_REMOTE = 268435456;
    
    public static final int CAPABILITY_SUPPORTS_VT_LOCAL_BIDIRECTIONAL = 768;
    
    public static final int CAPABILITY_SUPPORTS_VT_LOCAL_RX = 256;
    
    public static final int CAPABILITY_SUPPORTS_VT_LOCAL_TX = 512;
    
    public static final int CAPABILITY_SUPPORTS_VT_REMOTE_BIDIRECTIONAL = 3072;
    
    public static final int CAPABILITY_SUPPORTS_VT_REMOTE_RX = 1024;
    
    public static final int CAPABILITY_SUPPORTS_VT_REMOTE_TX = 2048;
    
    public static final int CAPABILITY_SUPPORT_DEFLECT = 16777216;
    
    public static final int CAPABILITY_SUPPORT_HOLD = 2;
    
    public static final int CAPABILITY_SWAP_CONFERENCE = 8;
    
    public static final int CAPABILITY_TRANSFER = 67108864;
    
    public static final int CAPABILITY_TRANSFER_CONSULTATIVE = 134217728;
    
    public static final int CAPABILITY_UNUSED_1 = 16;
    
    public static final int DIRECTION_INCOMING = 0;
    
    public static final int DIRECTION_OUTGOING = 1;
    
    public static final int DIRECTION_UNKNOWN = -1;
    
    public static final int PROPERTY_ASSISTED_DIALING = 512;
    
    public static final int PROPERTY_CONFERENCE = 1;
    
    public static final int PROPERTY_EMERGENCY_CALLBACK_MODE = 4;
    
    public static final int PROPERTY_ENTERPRISE_CALL = 32;
    
    public static final int PROPERTY_GENERIC_CONFERENCE = 2;
    
    public static final int PROPERTY_HAS_CDMA_VOICE_PRIVACY = 128;
    
    public static final int PROPERTY_HIGH_DEF_AUDIO = 16;
    
    public static final int PROPERTY_IS_ADHOC_CONFERENCE = 8192;
    
    public static final int PROPERTY_IS_EXTERNAL_CALL = 64;
    
    public static final int PROPERTY_NETWORK_IDENTIFIED_EMERGENCY_CALL = 2048;
    
    public static final int PROPERTY_RTT = 1024;
    
    public static final int PROPERTY_SELF_MANAGED = 256;
    
    public static final int PROPERTY_VOIP_AUDIO_MODE = 4096;
    
    public static final int PROPERTY_WIFI = 8;
    
    private final PhoneAccountHandle mAccountHandle;
    
    private final int mCallCapabilities;
    
    private final int mCallDirection;
    
    private final int mCallProperties;
    
    private final String mCallerDisplayName;
    
    private final int mCallerDisplayNamePresentation;
    
    private final int mCallerNumberVerificationStatus;
    
    private final long mConnectTimeMillis;
    
    private final String mContactDisplayName;
    
    private final long mCreationTimeMillis;
    
    private final DisconnectCause mDisconnectCause;
    
    private final Bundle mExtras;
    
    private final GatewayInfo mGatewayInfo;
    
    private final Uri mHandle;
    
    private final int mHandlePresentation;
    
    private final Bundle mIntentExtras;
    
    private final StatusHints mStatusHints;
    
    private final int mSupportedAudioRoutes = 15;
    
    private final String mTelecomCallId;
    
    private final int mVideoState;
    
    public static boolean can(int param1Int1, int param1Int2) {
      boolean bool;
      if ((param1Int1 & param1Int2) == param1Int2) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean can(int param1Int) {
      return can(this.mCallCapabilities, param1Int);
    }
    
    public static String capabilitiesToString(int param1Int) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("[Capabilities:");
      if (can(param1Int, 1))
        stringBuilder.append(" CAPABILITY_HOLD"); 
      if (can(param1Int, 2))
        stringBuilder.append(" CAPABILITY_SUPPORT_HOLD"); 
      if (can(param1Int, 4))
        stringBuilder.append(" CAPABILITY_MERGE_CONFERENCE"); 
      if (can(param1Int, 8))
        stringBuilder.append(" CAPABILITY_SWAP_CONFERENCE"); 
      if (can(param1Int, 32))
        stringBuilder.append(" CAPABILITY_RESPOND_VIA_TEXT"); 
      if (can(param1Int, 64))
        stringBuilder.append(" CAPABILITY_MUTE"); 
      if (can(param1Int, 128))
        stringBuilder.append(" CAPABILITY_MANAGE_CONFERENCE"); 
      if (can(param1Int, 256))
        stringBuilder.append(" CAPABILITY_SUPPORTS_VT_LOCAL_RX"); 
      if (can(param1Int, 512))
        stringBuilder.append(" CAPABILITY_SUPPORTS_VT_LOCAL_TX"); 
      if (can(param1Int, 768))
        stringBuilder.append(" CAPABILITY_SUPPORTS_VT_LOCAL_BIDIRECTIONAL"); 
      if (can(param1Int, 1024))
        stringBuilder.append(" CAPABILITY_SUPPORTS_VT_REMOTE_RX"); 
      if (can(param1Int, 2048))
        stringBuilder.append(" CAPABILITY_SUPPORTS_VT_REMOTE_TX"); 
      if (can(param1Int, 4194304))
        stringBuilder.append(" CAPABILITY_CANNOT_DOWNGRADE_VIDEO_TO_AUDIO"); 
      if (can(param1Int, 3072))
        stringBuilder.append(" CAPABILITY_SUPPORTS_VT_REMOTE_BIDIRECTIONAL"); 
      if (can(param1Int, 262144))
        stringBuilder.append(" CAPABILITY_SPEED_UP_MT_AUDIO"); 
      if (can(param1Int, 524288))
        stringBuilder.append(" CAPABILITY_CAN_UPGRADE_TO_VIDEO"); 
      if (can(param1Int, 1048576))
        stringBuilder.append(" CAPABILITY_CAN_PAUSE_VIDEO"); 
      if (can(param1Int, 8388608))
        stringBuilder.append(" CAPABILITY_CAN_PULL_CALL"); 
      if (can(param1Int, 16777216))
        stringBuilder.append(" CAPABILITY_SUPPORT_DEFLECT"); 
      if (can(param1Int, 33554432))
        stringBuilder.append(" CAPABILITY_ADD_PARTICIPANT"); 
      if (can(param1Int, 67108864))
        stringBuilder.append(" CAPABILITY_TRANSFER"); 
      if (can(param1Int, 134217728))
        stringBuilder.append(" CAPABILITY_TRANSFER_CONSULTATIVE"); 
      if (can(param1Int, 268435456))
        stringBuilder.append(" CAPABILITY_SUPPORTS_RTT_REMOTE"); 
      stringBuilder.append("]");
      return stringBuilder.toString();
    }
    
    public static boolean hasProperty(int param1Int1, int param1Int2) {
      boolean bool;
      if ((param1Int1 & param1Int2) == param1Int2) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean hasProperty(int param1Int) {
      return hasProperty(this.mCallProperties, param1Int);
    }
    
    public static String propertiesToString(int param1Int) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("[Properties:");
      if (hasProperty(param1Int, 1))
        stringBuilder.append(" PROPERTY_CONFERENCE"); 
      if (hasProperty(param1Int, 2))
        stringBuilder.append(" PROPERTY_GENERIC_CONFERENCE"); 
      if (hasProperty(param1Int, 8))
        stringBuilder.append(" PROPERTY_WIFI"); 
      if (hasProperty(param1Int, 16))
        stringBuilder.append(" PROPERTY_HIGH_DEF_AUDIO"); 
      if (hasProperty(param1Int, 4))
        stringBuilder.append(" PROPERTY_EMERGENCY_CALLBACK_MODE"); 
      if (hasProperty(param1Int, 64))
        stringBuilder.append(" PROPERTY_IS_EXTERNAL_CALL"); 
      if (hasProperty(param1Int, 128))
        stringBuilder.append(" PROPERTY_HAS_CDMA_VOICE_PRIVACY"); 
      if (hasProperty(param1Int, 512))
        stringBuilder.append(" PROPERTY_ASSISTED_DIALING_USED"); 
      if (hasProperty(param1Int, 2048))
        stringBuilder.append(" PROPERTY_NETWORK_IDENTIFIED_EMERGENCY_CALL"); 
      if (hasProperty(param1Int, 1024))
        stringBuilder.append(" PROPERTY_RTT"); 
      if (hasProperty(param1Int, 4096))
        stringBuilder.append(" PROPERTY_VOIP_AUDIO_MODE"); 
      if (hasProperty(param1Int, 8192))
        stringBuilder.append(" PROPERTY_IS_ADHOC_CONFERENCE"); 
      stringBuilder.append("]");
      return stringBuilder.toString();
    }
    
    public String getTelecomCallId() {
      return this.mTelecomCallId;
    }
    
    public Uri getHandle() {
      return this.mHandle;
    }
    
    public int getHandlePresentation() {
      return this.mHandlePresentation;
    }
    
    public String getCallerDisplayName() {
      return this.mCallerDisplayName;
    }
    
    public int getCallerDisplayNamePresentation() {
      return this.mCallerDisplayNamePresentation;
    }
    
    public PhoneAccountHandle getAccountHandle() {
      return this.mAccountHandle;
    }
    
    public int getCallCapabilities() {
      return this.mCallCapabilities;
    }
    
    public int getCallProperties() {
      return this.mCallProperties;
    }
    
    public int getSupportedAudioRoutes() {
      return 15;
    }
    
    public DisconnectCause getDisconnectCause() {
      return this.mDisconnectCause;
    }
    
    public final long getConnectTimeMillis() {
      return this.mConnectTimeMillis;
    }
    
    public GatewayInfo getGatewayInfo() {
      return this.mGatewayInfo;
    }
    
    public int getVideoState() {
      return this.mVideoState;
    }
    
    public StatusHints getStatusHints() {
      return this.mStatusHints;
    }
    
    public Bundle getExtras() {
      return this.mExtras;
    }
    
    public Bundle getIntentExtras() {
      return this.mIntentExtras;
    }
    
    public long getCreationTimeMillis() {
      return this.mCreationTimeMillis;
    }
    
    public String getContactDisplayName() {
      return this.mContactDisplayName;
    }
    
    public int getCallDirection() {
      return this.mCallDirection;
    }
    
    public int getCallerNumberVerificationStatus() {
      return this.mCallerNumberVerificationStatus;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = param1Object instanceof Details;
      boolean bool1 = false;
      if (bool) {
        param1Object = param1Object;
        Uri uri1 = this.mHandle, uri2 = ((Details)param1Object).mHandle;
        if (Objects.equals(uri1, uri2)) {
          int i = this.mHandlePresentation;
          if (Objects.equals(Integer.valueOf(i), Integer.valueOf(((Details)param1Object).mHandlePresentation))) {
            String str1 = this.mCallerDisplayName, str2 = ((Details)param1Object).mCallerDisplayName;
            if (Objects.equals(str1, str2)) {
              int j = this.mCallerDisplayNamePresentation;
              i = ((Details)param1Object).mCallerDisplayNamePresentation;
              if (Objects.equals(Integer.valueOf(j), Integer.valueOf(i))) {
                PhoneAccountHandle phoneAccountHandle2 = this.mAccountHandle, phoneAccountHandle1 = ((Details)param1Object).mAccountHandle;
                if (Objects.equals(phoneAccountHandle2, phoneAccountHandle1)) {
                  i = this.mCallCapabilities;
                  if (Objects.equals(Integer.valueOf(i), Integer.valueOf(((Details)param1Object).mCallCapabilities))) {
                    i = this.mCallProperties;
                    if (Objects.equals(Integer.valueOf(i), Integer.valueOf(((Details)param1Object).mCallProperties))) {
                      DisconnectCause disconnectCause2 = this.mDisconnectCause, disconnectCause1 = ((Details)param1Object).mDisconnectCause;
                      if (Objects.equals(disconnectCause2, disconnectCause1)) {
                        long l = this.mConnectTimeMillis;
                        if (Objects.equals(Long.valueOf(l), Long.valueOf(((Details)param1Object).mConnectTimeMillis))) {
                          GatewayInfo gatewayInfo1 = this.mGatewayInfo, gatewayInfo2 = ((Details)param1Object).mGatewayInfo;
                          if (Objects.equals(gatewayInfo1, gatewayInfo2)) {
                            i = this.mVideoState;
                            if (Objects.equals(Integer.valueOf(i), Integer.valueOf(((Details)param1Object).mVideoState))) {
                              StatusHints statusHints1 = this.mStatusHints, statusHints2 = ((Details)param1Object).mStatusHints;
                              if (Objects.equals(statusHints1, statusHints2)) {
                                Bundle bundle2 = this.mExtras, bundle1 = ((Details)param1Object).mExtras;
                                if (Call.areBundlesEqual(bundle2, bundle1)) {
                                  bundle1 = this.mIntentExtras;
                                  bundle2 = ((Details)param1Object).mIntentExtras;
                                  if (Call.areBundlesEqual(bundle1, bundle2)) {
                                    l = this.mCreationTimeMillis;
                                    if (Objects.equals(Long.valueOf(l), Long.valueOf(((Details)param1Object).mCreationTimeMillis))) {
                                      String str4 = this.mContactDisplayName, str3 = ((Details)param1Object).mContactDisplayName;
                                      if (Objects.equals(str4, str3)) {
                                        i = this.mCallDirection;
                                        if (Objects.equals(Integer.valueOf(i), Integer.valueOf(((Details)param1Object).mCallDirection))) {
                                          i = this.mCallerNumberVerificationStatus;
                                          j = ((Details)param1Object).mCallerNumberVerificationStatus;
                                          if (Objects.equals(Integer.valueOf(i), Integer.valueOf(j)))
                                            bool1 = true; 
                                        } 
                                      } 
                                    } 
                                  } 
                                } 
                              } 
                            } 
                          } 
                        } 
                      } 
                    } 
                  } 
                } 
              } 
            } 
          } 
        } 
        return bool1;
      } 
      return false;
    }
    
    public int hashCode() {
      Uri uri = this.mHandle;
      int i = this.mHandlePresentation;
      String str1 = this.mCallerDisplayName;
      int j = this.mCallerDisplayNamePresentation;
      PhoneAccountHandle phoneAccountHandle = this.mAccountHandle;
      int k = this.mCallCapabilities;
      int m = this.mCallProperties;
      DisconnectCause disconnectCause = this.mDisconnectCause;
      long l1 = this.mConnectTimeMillis;
      GatewayInfo gatewayInfo = this.mGatewayInfo;
      int n = this.mVideoState;
      StatusHints statusHints = this.mStatusHints;
      Bundle bundle1 = this.mExtras, bundle2 = this.mIntentExtras;
      long l2 = this.mCreationTimeMillis;
      String str2 = this.mContactDisplayName;
      int i1 = this.mCallDirection;
      int i2 = this.mCallerNumberVerificationStatus;
      return Objects.hash(new Object[] { 
            uri, Integer.valueOf(i), str1, Integer.valueOf(j), phoneAccountHandle, Integer.valueOf(k), Integer.valueOf(m), disconnectCause, Long.valueOf(l1), gatewayInfo, 
            Integer.valueOf(n), statusHints, bundle1, bundle2, Long.valueOf(l2), str2, Integer.valueOf(i1), Integer.valueOf(i2) });
    }
    
    public Details(String param1String1, Uri param1Uri, int param1Int1, String param1String2, int param1Int2, PhoneAccountHandle param1PhoneAccountHandle, int param1Int3, int param1Int4, DisconnectCause param1DisconnectCause, long param1Long1, GatewayInfo param1GatewayInfo, int param1Int5, StatusHints param1StatusHints, Bundle param1Bundle1, Bundle param1Bundle2, long param1Long2, String param1String3, int param1Int6, int param1Int7) {
      this.mTelecomCallId = param1String1;
      this.mHandle = param1Uri;
      this.mHandlePresentation = param1Int1;
      this.mCallerDisplayName = param1String2;
      this.mCallerDisplayNamePresentation = param1Int2;
      this.mAccountHandle = param1PhoneAccountHandle;
      this.mCallCapabilities = param1Int3;
      this.mCallProperties = param1Int4;
      this.mDisconnectCause = param1DisconnectCause;
      this.mConnectTimeMillis = param1Long1;
      this.mGatewayInfo = param1GatewayInfo;
      this.mVideoState = param1Int5;
      this.mStatusHints = param1StatusHints;
      this.mExtras = param1Bundle1;
      this.mIntentExtras = param1Bundle2;
      this.mCreationTimeMillis = param1Long2;
      this.mContactDisplayName = param1String3;
      this.mCallDirection = param1Int6;
      this.mCallerNumberVerificationStatus = param1Int7;
    }
    
    public static Details createFromParcelableCall(ParcelableCall param1ParcelableCall) {
      String str1 = param1ParcelableCall.getId();
      Uri uri = param1ParcelableCall.getHandle();
      int i = param1ParcelableCall.getHandlePresentation();
      String str2 = param1ParcelableCall.getCallerDisplayName();
      int j = param1ParcelableCall.getCallerDisplayNamePresentation();
      PhoneAccountHandle phoneAccountHandle = param1ParcelableCall.getAccountHandle();
      int k = param1ParcelableCall.getCapabilities();
      int m = param1ParcelableCall.getProperties();
      DisconnectCause disconnectCause = param1ParcelableCall.getDisconnectCause();
      long l1 = param1ParcelableCall.getConnectTimeMillis();
      GatewayInfo gatewayInfo = param1ParcelableCall.getGatewayInfo();
      int n = param1ParcelableCall.getVideoState();
      StatusHints statusHints = param1ParcelableCall.getStatusHints();
      Bundle bundle1 = param1ParcelableCall.getExtras();
      Bundle bundle2 = param1ParcelableCall.getIntentExtras();
      long l2 = param1ParcelableCall.getCreationTimeMillis();
      String str3 = param1ParcelableCall.getContactDisplayName();
      int i1 = param1ParcelableCall.getCallDirection();
      return new Details(str1, uri, i, str2, j, phoneAccountHandle, k, m, disconnectCause, l1, gatewayInfo, n, statusHints, bundle1, bundle2, l2, str3, i1, param1ParcelableCall.getCallerNumberVerificationStatus());
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("[id: ");
      stringBuilder.append(this.mTelecomCallId);
      stringBuilder.append(", pa: ");
      stringBuilder.append(this.mAccountHandle);
      stringBuilder.append(", hdl: ");
      stringBuilder.append(Log.piiHandle(this.mHandle));
      stringBuilder.append(", hdlPres: ");
      stringBuilder.append(this.mHandlePresentation);
      stringBuilder.append(", videoState: ");
      stringBuilder.append(VideoProfile.videoStateToString(this.mVideoState));
      stringBuilder.append(", caps: ");
      stringBuilder.append(capabilitiesToString(this.mCallCapabilities));
      stringBuilder.append(", props: ");
      stringBuilder.append(propertiesToString(this.mCallProperties));
      stringBuilder.append("]");
      return stringBuilder.toString();
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public static @interface CallDirection {}
  }
  
  public static abstract class Callback {
    public static final int HANDOVER_FAILURE_DEST_APP_REJECTED = 1;
    
    public static final int HANDOVER_FAILURE_NOT_SUPPORTED = 2;
    
    public static final int HANDOVER_FAILURE_ONGOING_EMERGENCY_CALL = 4;
    
    public static final int HANDOVER_FAILURE_UNKNOWN = 5;
    
    public static final int HANDOVER_FAILURE_USER_REJECTED = 3;
    
    public void onStateChanged(Call param1Call, int param1Int) {}
    
    public void onParentChanged(Call param1Call1, Call param1Call2) {}
    
    public void onChildrenChanged(Call param1Call, List<Call> param1List) {}
    
    public void onDetailsChanged(Call param1Call, Call.Details param1Details) {}
    
    public void onCannedTextResponsesLoaded(Call param1Call, List<String> param1List) {}
    
    public void onPostDialWait(Call param1Call, String param1String) {}
    
    public void onVideoCallChanged(Call param1Call, InCallService.VideoCall param1VideoCall) {}
    
    public void onCallDestroyed(Call param1Call) {}
    
    public void onConferenceableCallsChanged(Call param1Call, List<Call> param1List) {}
    
    public void onConnectionEvent(Call param1Call, String param1String, Bundle param1Bundle) {}
    
    public void onRttModeChanged(Call param1Call, int param1Int) {}
    
    public void onRttStatusChanged(Call param1Call, boolean param1Boolean, Call.RttCall param1RttCall) {}
    
    public void onRttRequest(Call param1Call, int param1Int) {}
    
    public void onRttInitiationFailure(Call param1Call, int param1Int) {}
    
    public void onHandoverComplete(Call param1Call) {}
    
    public void onHandoverFailed(Call param1Call, int param1Int) {}
    
    @Retention(RetentionPolicy.SOURCE)
    public static @interface HandoverFailureErrors {}
  }
  
  public static final class RttCall {
    private static final int READ_BUFFER_SIZE = 1000;
    
    public static final int RTT_MODE_FULL = 1;
    
    public static final int RTT_MODE_HCO = 2;
    
    public static final int RTT_MODE_INVALID = 0;
    
    public static final int RTT_MODE_VCO = 3;
    
    private final InCallAdapter mInCallAdapter;
    
    private char[] mReadBuffer = new char[1000];
    
    private InputStreamReader mReceiveStream;
    
    private int mRttMode;
    
    private final String mTelecomCallId;
    
    private OutputStreamWriter mTransmitStream;
    
    public RttCall(String param1String, InputStreamReader param1InputStreamReader, OutputStreamWriter param1OutputStreamWriter, int param1Int, InCallAdapter param1InCallAdapter) {
      this.mTelecomCallId = param1String;
      this.mReceiveStream = param1InputStreamReader;
      this.mTransmitStream = param1OutputStreamWriter;
      this.mRttMode = param1Int;
      this.mInCallAdapter = param1InCallAdapter;
    }
    
    public int getRttAudioMode() {
      return this.mRttMode;
    }
    
    public void setRttMode(int param1Int) {
      this.mInCallAdapter.setRttMode(this.mTelecomCallId, param1Int);
    }
    
    public void write(String param1String) throws IOException {
      this.mTransmitStream.write(param1String);
      this.mTransmitStream.flush();
    }
    
    public String read() {
      try {
        int i = this.mReceiveStream.read(this.mReadBuffer, 0, 1000);
        if (i < 0)
          return null; 
        return new String(this.mReadBuffer, 0, i);
      } catch (IOException iOException) {
        Log.w(this, "Exception encountered when reading from InputStreamReader: %s", new Object[] { iOException });
        return null;
      } 
    }
    
    public String readImmediately() throws IOException {
      if (this.mReceiveStream.ready()) {
        int i = this.mReceiveStream.read(this.mReadBuffer, 0, 1000);
        if (i < 0)
          return null; 
        return new String(this.mReadBuffer, 0, i);
      } 
      return null;
    }
    
    public void close() {
      try {
        this.mReceiveStream.close();
      } catch (IOException iOException) {}
      try {
        this.mTransmitStream.close();
      } catch (IOException iOException) {}
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public static @interface RttAudioMode {}
  }
  
  @SystemApi
  @Deprecated
  class Listener extends Callback {}
  
  private final List<String> mChildrenIds = new ArrayList<>();
  
  private final List<Call> mConferenceableCalls;
  
  private Details mDetails;
  
  private Bundle mExtras;
  
  private final InCallAdapter mInCallAdapter;
  
  private String mParentId;
  
  private final Phone mPhone;
  
  private String mRemainingPostDialSequence;
  
  private RttCall mRttCall;
  
  private int mState;
  
  private int mTargetSdkVersion;
  
  private final String mTelecomCallId;
  
  private final List<Call> mUnmodifiableChildren;
  
  private final List<Call> mUnmodifiableConferenceableCalls;
  
  private VideoCallImpl mVideoCallImpl;
  
  Call(Phone paramPhone, String paramString1, InCallAdapter paramInCallAdapter, int paramInt1, String paramString2, int paramInt2) {
    ArrayList<Call> arrayList = new ArrayList();
    this.mUnmodifiableChildren = Collections.unmodifiableList(arrayList);
    this.mCallbackRecords = new CopyOnWriteArrayList<>();
    this.mConferenceableCalls = arrayList = new ArrayList<>();
    this.mUnmodifiableConferenceableCalls = Collections.unmodifiableList(arrayList);
    this.mParentId = null;
    this.mActiveGenericConferenceChild = null;
    this.mCannedTextResponses = null;
    this.mPhone = paramPhone;
    this.mTelecomCallId = paramString1;
    this.mInCallAdapter = paramInCallAdapter;
    this.mState = paramInt1;
    this.mCallingPackage = paramString2;
    this.mTargetSdkVersion = paramInt2;
  }
  
  Call(Phone paramPhone, String paramString1, InCallAdapter paramInCallAdapter, String paramString2, int paramInt) {
    ArrayList<Call> arrayList = new ArrayList();
    this.mUnmodifiableChildren = Collections.unmodifiableList(arrayList);
    this.mCallbackRecords = new CopyOnWriteArrayList<>();
    this.mConferenceableCalls = arrayList = new ArrayList<>();
    this.mUnmodifiableConferenceableCalls = Collections.unmodifiableList(arrayList);
    this.mParentId = null;
    this.mActiveGenericConferenceChild = null;
    this.mCannedTextResponses = null;
    this.mPhone = paramPhone;
    this.mTelecomCallId = paramString1;
    this.mInCallAdapter = paramInCallAdapter;
    this.mState = 0;
    this.mCallingPackage = paramString2;
    this.mTargetSdkVersion = paramInt;
  }
  
  public String getRemainingPostDialSequence() {
    return this.mRemainingPostDialSequence;
  }
  
  public void answer(int paramInt) {
    this.mInCallAdapter.answerCall(this.mTelecomCallId, paramInt);
  }
  
  public void deflect(Uri paramUri) {
    this.mInCallAdapter.deflectCall(this.mTelecomCallId, paramUri);
  }
  
  public void reject(boolean paramBoolean, String paramString) {
    this.mInCallAdapter.rejectCall(this.mTelecomCallId, paramBoolean, paramString);
  }
  
  public void reject(int paramInt) {
    this.mInCallAdapter.rejectCall(this.mTelecomCallId, paramInt);
  }
  
  public void transfer(Uri paramUri, boolean paramBoolean) {
    this.mInCallAdapter.transferCall(this.mTelecomCallId, paramUri, paramBoolean);
  }
  
  public void transfer(Call paramCall) {
    this.mInCallAdapter.transferCall(this.mTelecomCallId, paramCall.mTelecomCallId);
  }
  
  public void disconnect() {
    this.mInCallAdapter.disconnectCall(this.mTelecomCallId);
  }
  
  public void hold() {
    this.mInCallAdapter.holdCall(this.mTelecomCallId);
  }
  
  public void unhold() {
    this.mInCallAdapter.unholdCall(this.mTelecomCallId);
  }
  
  @SystemApi
  public void enterBackgroundAudioProcessing() {
    int i = this.mState;
    if (i == 4 || i == 2) {
      this.mInCallAdapter.enterBackgroundAudioProcessing(this.mTelecomCallId);
      return;
    } 
    throw new IllegalStateException("Call must be active or ringing");
  }
  
  @SystemApi
  public void exitBackgroundAudioProcessing(boolean paramBoolean) {
    if (this.mState == 12) {
      this.mInCallAdapter.exitBackgroundAudioProcessing(this.mTelecomCallId, paramBoolean);
      return;
    } 
    throw new IllegalStateException("Call must in the audio processing state");
  }
  
  public void playDtmfTone(char paramChar) {
    this.mInCallAdapter.playDtmfTone(this.mTelecomCallId, paramChar);
  }
  
  public void stopDtmfTone() {
    this.mInCallAdapter.stopDtmfTone(this.mTelecomCallId);
  }
  
  public void postDialContinue(boolean paramBoolean) {
    this.mInCallAdapter.postDialContinue(this.mTelecomCallId, paramBoolean);
  }
  
  public void phoneAccountSelected(PhoneAccountHandle paramPhoneAccountHandle, boolean paramBoolean) {
    this.mInCallAdapter.phoneAccountSelected(this.mTelecomCallId, paramPhoneAccountHandle, paramBoolean);
  }
  
  public void conference(Call paramCall) {
    if (paramCall != null)
      this.mInCallAdapter.conference(this.mTelecomCallId, paramCall.mTelecomCallId); 
  }
  
  public void splitFromConference() {
    this.mInCallAdapter.splitFromConference(this.mTelecomCallId);
  }
  
  public void mergeConference() {
    this.mInCallAdapter.mergeConference(this.mTelecomCallId);
  }
  
  public void swapConference() {
    this.mInCallAdapter.swapConference(this.mTelecomCallId);
  }
  
  public void addConferenceParticipants(List<Uri> paramList) {
    this.mInCallAdapter.addConferenceParticipants(this.mTelecomCallId, paramList);
  }
  
  public void pullExternalCall() {
    if (!this.mDetails.hasProperty(64))
      return; 
    this.mInCallAdapter.pullExternalCall(this.mTelecomCallId);
  }
  
  public void sendCallEvent(String paramString, Bundle paramBundle) {
    this.mInCallAdapter.sendCallEvent(this.mTelecomCallId, paramString, this.mTargetSdkVersion, paramBundle);
  }
  
  public void sendRttRequest() {
    this.mInCallAdapter.sendRttRequest(this.mTelecomCallId);
  }
  
  public void respondToRttRequest(int paramInt, boolean paramBoolean) {
    this.mInCallAdapter.respondToRttRequest(this.mTelecomCallId, paramInt, paramBoolean);
  }
  
  public void handoverTo(PhoneAccountHandle paramPhoneAccountHandle, int paramInt, Bundle paramBundle) {
    this.mInCallAdapter.handoverTo(this.mTelecomCallId, paramPhoneAccountHandle, paramInt, paramBundle);
  }
  
  public void stopRtt() {
    this.mInCallAdapter.stopRtt(this.mTelecomCallId);
  }
  
  public final void putExtras(Bundle paramBundle) {
    if (paramBundle == null)
      return; 
    if (this.mExtras == null)
      this.mExtras = new Bundle(); 
    this.mExtras.putAll(paramBundle);
    this.mInCallAdapter.putExtras(this.mTelecomCallId, paramBundle);
  }
  
  public final void putExtra(String paramString, boolean paramBoolean) {
    if (this.mExtras == null)
      this.mExtras = new Bundle(); 
    this.mExtras.putBoolean(paramString, paramBoolean);
    this.mInCallAdapter.putExtra(this.mTelecomCallId, paramString, paramBoolean);
  }
  
  public final void putExtra(String paramString, int paramInt) {
    if (this.mExtras == null)
      this.mExtras = new Bundle(); 
    this.mExtras.putInt(paramString, paramInt);
    this.mInCallAdapter.putExtra(this.mTelecomCallId, paramString, paramInt);
  }
  
  public final void putExtra(String paramString1, String paramString2) {
    if (this.mExtras == null)
      this.mExtras = new Bundle(); 
    this.mExtras.putString(paramString1, paramString2);
    this.mInCallAdapter.putExtra(this.mTelecomCallId, paramString1, paramString2);
  }
  
  public final void removeExtras(List<String> paramList) {
    if (this.mExtras != null) {
      for (String str : paramList)
        this.mExtras.remove(str); 
      if (this.mExtras.size() == 0)
        this.mExtras = null; 
    } 
    this.mInCallAdapter.removeExtras(this.mTelecomCallId, paramList);
  }
  
  public final void removeExtras(String... paramVarArgs) {
    removeExtras(Arrays.asList(paramVarArgs));
  }
  
  public Call getParent() {
    String str = this.mParentId;
    if (str != null)
      return this.mPhone.internalGetCallByTelecomId(str); 
    return null;
  }
  
  public List<Call> getChildren() {
    if (!this.mChildrenCached) {
      this.mChildrenCached = true;
      this.mChildren.clear();
      for (String str : this.mChildrenIds) {
        Call call = this.mPhone.internalGetCallByTelecomId(str);
        if (call == null) {
          this.mChildrenCached = false;
          continue;
        } 
        this.mChildren.add(call);
      } 
    } 
    return this.mUnmodifiableChildren;
  }
  
  public List<Call> getConferenceableCalls() {
    return this.mUnmodifiableConferenceableCalls;
  }
  
  public int getState() {
    return this.mState;
  }
  
  public Call getGenericConferenceActiveChildCall() {
    String str = this.mActiveGenericConferenceChild;
    if (str != null)
      return this.mPhone.internalGetCallByTelecomId(str); 
    return null;
  }
  
  public List<String> getCannedTextResponses() {
    return this.mCannedTextResponses;
  }
  
  public InCallService.VideoCall getVideoCall() {
    return this.mVideoCallImpl;
  }
  
  public Details getDetails() {
    return this.mDetails;
  }
  
  public RttCall getRttCall() {
    return this.mRttCall;
  }
  
  public boolean isRttActive() {
    boolean bool;
    if (this.mRttCall != null && this.mDetails.hasProperty(1024)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void registerCallback(Callback paramCallback) {
    registerCallback(paramCallback, new Handler());
  }
  
  public void registerCallback(Callback paramCallback, Handler paramHandler) {
    unregisterCallback(paramCallback);
    if (paramCallback != null && paramHandler != null && this.mState != 7)
      this.mCallbackRecords.add(new CallbackRecord<>(paramCallback, paramHandler)); 
  }
  
  public void unregisterCallback(Callback paramCallback) {
    if (paramCallback != null && this.mState != 7)
      for (CallbackRecord<Callback> callbackRecord : this.mCallbackRecords) {
        if (callbackRecord.getCallback() == paramCallback) {
          this.mCallbackRecords.remove(callbackRecord);
          break;
        } 
      }  
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Call [id: ");
    String str = this.mTelecomCallId;
    stringBuilder.append(str);
    stringBuilder.append(", state: ");
    int i = this.mState;
    stringBuilder.append(stateToString(i));
    stringBuilder.append(", details: ");
    Details details = this.mDetails;
    stringBuilder.append(details);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  private static String stateToString(int paramInt) {
    switch (paramInt) {
      default:
        Log.w(Call.class, "Unknown state %d", new Object[] { Integer.valueOf(paramInt) });
        return "UNKNOWN";
      case 13:
        return "SIMULATED_RINGING";
      case 12:
        return "AUDIO_PROCESSING";
      case 10:
        return "DISCONNECTING";
      case 9:
        return "CONNECTING";
      case 8:
        return "SELECT_PHONE_ACCOUNT";
      case 7:
        return "DISCONNECTED";
      case 4:
        return "ACTIVE";
      case 3:
        return "HOLDING";
      case 2:
        return "RINGING";
      case 1:
        return "DIALING";
      case 0:
        break;
    } 
    return "NEW";
  }
  
  @SystemApi
  @Deprecated
  public void addListener(Listener paramListener) {
    registerCallback(paramListener);
  }
  
  @SystemApi
  @Deprecated
  public void removeListener(Listener paramListener) {
    unregisterCallback(paramListener);
  }
  
  public final String internalGetCallId() {
    return this.mTelecomCallId;
  }
  
  final void internalUpdate(ParcelableCall paramParcelableCall, Map<String, Call> paramMap) {
    // Byte code:
    //   0: aload_1
    //   1: invokestatic createFromParcelableCall : (Landroid/telecom/ParcelableCall;)Landroid/telecom/Call$Details;
    //   4: astore_3
    //   5: aload_0
    //   6: getfield mDetails : Landroid/telecom/Call$Details;
    //   9: aload_3
    //   10: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   13: iconst_1
    //   14: ixor
    //   15: istore #4
    //   17: iload #4
    //   19: ifeq -> 27
    //   22: aload_0
    //   23: aload_3
    //   24: putfield mDetails : Landroid/telecom/Call$Details;
    //   27: iconst_0
    //   28: istore #5
    //   30: iload #5
    //   32: istore #6
    //   34: aload_0
    //   35: getfield mCannedTextResponses : Ljava/util/List;
    //   38: ifnonnull -> 82
    //   41: iload #5
    //   43: istore #6
    //   45: aload_1
    //   46: invokevirtual getCannedSmsResponses : ()Ljava/util/List;
    //   49: ifnull -> 82
    //   52: iload #5
    //   54: istore #6
    //   56: aload_1
    //   57: invokevirtual getCannedSmsResponses : ()Ljava/util/List;
    //   60: invokeinterface isEmpty : ()Z
    //   65: ifne -> 82
    //   68: aload_0
    //   69: aload_1
    //   70: invokevirtual getCannedSmsResponses : ()Ljava/util/List;
    //   73: invokestatic unmodifiableList : (Ljava/util/List;)Ljava/util/List;
    //   76: putfield mCannedTextResponses : Ljava/util/List;
    //   79: iconst_1
    //   80: istore #6
    //   82: aload_0
    //   83: getfield mVideoCallImpl : Landroid/telecom/VideoCallImpl;
    //   86: astore_3
    //   87: aload_3
    //   88: ifnonnull -> 96
    //   91: aconst_null
    //   92: astore_3
    //   93: goto -> 101
    //   96: aload_3
    //   97: invokevirtual getVideoProvider : ()Lcom/android/internal/telecom/IVideoProvider;
    //   100: astore_3
    //   101: aload_1
    //   102: invokevirtual getVideoProvider : ()Lcom/android/internal/telecom/IVideoProvider;
    //   105: astore #7
    //   107: aload_1
    //   108: invokevirtual isVideoCallProviderChanged : ()Z
    //   111: ifeq -> 129
    //   114: aload_3
    //   115: aload #7
    //   117: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   120: ifne -> 129
    //   123: iconst_1
    //   124: istore #8
    //   126: goto -> 132
    //   129: iconst_0
    //   130: istore #8
    //   132: iload #8
    //   134: ifeq -> 183
    //   137: aload_0
    //   138: getfield mVideoCallImpl : Landroid/telecom/VideoCallImpl;
    //   141: astore_3
    //   142: aload_3
    //   143: ifnull -> 150
    //   146: aload_3
    //   147: invokevirtual destroy : ()V
    //   150: aload_1
    //   151: invokevirtual isVideoCallProviderChanged : ()Z
    //   154: ifeq -> 173
    //   157: aload_1
    //   158: aload_0
    //   159: getfield mCallingPackage : Ljava/lang/String;
    //   162: aload_0
    //   163: getfield mTargetSdkVersion : I
    //   166: invokevirtual getVideoCallImpl : (Ljava/lang/String;I)Landroid/telecom/VideoCallImpl;
    //   169: astore_3
    //   170: goto -> 175
    //   173: aconst_null
    //   174: astore_3
    //   175: aload_0
    //   176: aload_3
    //   177: putfield mVideoCallImpl : Landroid/telecom/VideoCallImpl;
    //   180: goto -> 183
    //   183: aload_0
    //   184: getfield mVideoCallImpl : Landroid/telecom/VideoCallImpl;
    //   187: astore_3
    //   188: aload_3
    //   189: ifnull -> 203
    //   192: aload_3
    //   193: aload_0
    //   194: invokevirtual getDetails : ()Landroid/telecom/Call$Details;
    //   197: invokevirtual getVideoState : ()I
    //   200: invokevirtual setVideoState : (I)V
    //   203: aload_1
    //   204: invokevirtual getState : ()I
    //   207: istore #9
    //   209: iload #9
    //   211: istore #5
    //   213: aload_0
    //   214: getfield mTargetSdkVersion : I
    //   217: bipush #30
    //   219: if_icmpge -> 236
    //   222: iload #9
    //   224: istore #5
    //   226: iload #9
    //   228: bipush #13
    //   230: if_icmpne -> 236
    //   233: iconst_2
    //   234: istore #5
    //   236: aload_0
    //   237: getfield mState : I
    //   240: iload #5
    //   242: if_icmpeq -> 251
    //   245: iconst_1
    //   246: istore #10
    //   248: goto -> 254
    //   251: iconst_0
    //   252: istore #10
    //   254: iload #10
    //   256: ifeq -> 265
    //   259: aload_0
    //   260: iload #5
    //   262: putfield mState : I
    //   265: aload_1
    //   266: invokevirtual getParentCallId : ()Ljava/lang/String;
    //   269: astore_3
    //   270: aload_0
    //   271: getfield mParentId : Ljava/lang/String;
    //   274: aload_3
    //   275: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   278: iconst_1
    //   279: ixor
    //   280: istore #11
    //   282: iload #11
    //   284: ifeq -> 292
    //   287: aload_0
    //   288: aload_3
    //   289: putfield mParentId : Ljava/lang/String;
    //   292: aload_1
    //   293: invokevirtual getChildCallIds : ()Ljava/util/List;
    //   296: astore_3
    //   297: aload_3
    //   298: aload_0
    //   299: getfield mChildrenIds : Ljava/util/List;
    //   302: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   305: iconst_1
    //   306: ixor
    //   307: istore #12
    //   309: iload #12
    //   311: ifeq -> 345
    //   314: aload_0
    //   315: getfield mChildrenIds : Ljava/util/List;
    //   318: invokeinterface clear : ()V
    //   323: aload_0
    //   324: getfield mChildrenIds : Ljava/util/List;
    //   327: aload_1
    //   328: invokevirtual getChildCallIds : ()Ljava/util/List;
    //   331: invokeinterface addAll : (Ljava/util/Collection;)Z
    //   336: pop
    //   337: aload_0
    //   338: iconst_0
    //   339: putfield mChildrenCached : Z
    //   342: goto -> 345
    //   345: aload_1
    //   346: invokevirtual getActiveChildCallId : ()Ljava/lang/String;
    //   349: astore_3
    //   350: aload_3
    //   351: aload_0
    //   352: getfield mActiveGenericConferenceChild : Ljava/lang/String;
    //   355: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   358: iconst_1
    //   359: ixor
    //   360: istore #13
    //   362: iload #13
    //   364: ifeq -> 372
    //   367: aload_0
    //   368: aload_3
    //   369: putfield mActiveGenericConferenceChild : Ljava/lang/String;
    //   372: aload_1
    //   373: invokevirtual getConferenceableCallIds : ()Ljava/util/List;
    //   376: astore_3
    //   377: new java/util/ArrayList
    //   380: dup
    //   381: aload_3
    //   382: invokeinterface size : ()I
    //   387: invokespecial <init> : (I)V
    //   390: astore #7
    //   392: aload_3
    //   393: invokeinterface iterator : ()Ljava/util/Iterator;
    //   398: astore_3
    //   399: aload_3
    //   400: invokeinterface hasNext : ()Z
    //   405: ifeq -> 452
    //   408: aload_3
    //   409: invokeinterface next : ()Ljava/lang/Object;
    //   414: checkcast java/lang/String
    //   417: astore #14
    //   419: aload_2
    //   420: aload #14
    //   422: invokeinterface containsKey : (Ljava/lang/Object;)Z
    //   427: ifeq -> 449
    //   430: aload #7
    //   432: aload_2
    //   433: aload #14
    //   435: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   440: checkcast android/telecom/Call
    //   443: invokeinterface add : (Ljava/lang/Object;)Z
    //   448: pop
    //   449: goto -> 399
    //   452: aload_0
    //   453: getfield mConferenceableCalls : Ljava/util/List;
    //   456: aload #7
    //   458: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   461: iconst_1
    //   462: ixor
    //   463: istore #15
    //   465: iload #15
    //   467: ifeq -> 495
    //   470: aload_0
    //   471: getfield mConferenceableCalls : Ljava/util/List;
    //   474: invokeinterface clear : ()V
    //   479: aload_0
    //   480: getfield mConferenceableCalls : Ljava/util/List;
    //   483: aload #7
    //   485: invokeinterface addAll : (Ljava/util/Collection;)Z
    //   490: pop
    //   491: aload_0
    //   492: invokespecial fireConferenceableCallsChanged : ()V
    //   495: iconst_0
    //   496: istore #5
    //   498: iconst_0
    //   499: istore #9
    //   501: aload_1
    //   502: invokevirtual getIsRttCallChanged : ()Z
    //   505: ifeq -> 661
    //   508: aload_0
    //   509: getfield mDetails : Landroid/telecom/Call$Details;
    //   512: astore_2
    //   513: iconst_0
    //   514: istore #16
    //   516: aload_2
    //   517: sipush #1024
    //   520: invokevirtual hasProperty : (I)Z
    //   523: ifeq -> 658
    //   526: aload_1
    //   527: invokevirtual getParcelableRttCall : ()Landroid/telecom/ParcelableRttCall;
    //   530: astore_2
    //   531: new java/io/InputStreamReader
    //   534: dup
    //   535: new android/os/ParcelFileDescriptor$AutoCloseInputStream
    //   538: dup
    //   539: aload_2
    //   540: invokevirtual getReceiveStream : ()Landroid/os/ParcelFileDescriptor;
    //   543: invokespecial <init> : (Landroid/os/ParcelFileDescriptor;)V
    //   546: getstatic java/nio/charset/StandardCharsets.UTF_8 : Ljava/nio/charset/Charset;
    //   549: invokespecial <init> : (Ljava/io/InputStream;Ljava/nio/charset/Charset;)V
    //   552: astore_1
    //   553: new java/io/OutputStreamWriter
    //   556: dup
    //   557: new android/os/ParcelFileDescriptor$AutoCloseOutputStream
    //   560: dup
    //   561: aload_2
    //   562: invokevirtual getTransmitStream : ()Landroid/os/ParcelFileDescriptor;
    //   565: invokespecial <init> : (Landroid/os/ParcelFileDescriptor;)V
    //   568: getstatic java/nio/charset/StandardCharsets.UTF_8 : Ljava/nio/charset/Charset;
    //   571: invokespecial <init> : (Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V
    //   574: astore #7
    //   576: aload_0
    //   577: getfield mTelecomCallId : Ljava/lang/String;
    //   580: astore_3
    //   581: new android/telecom/Call$RttCall
    //   584: dup
    //   585: aload_3
    //   586: aload_1
    //   587: aload #7
    //   589: aload_2
    //   590: invokevirtual getRttMode : ()I
    //   593: aload_0
    //   594: getfield mInCallAdapter : Landroid/telecom/InCallAdapter;
    //   597: invokespecial <init> : (Ljava/lang/String;Ljava/io/InputStreamReader;Ljava/io/OutputStreamWriter;ILandroid/telecom/InCallAdapter;)V
    //   600: astore_2
    //   601: aload_0
    //   602: getfield mRttCall : Landroid/telecom/Call$RttCall;
    //   605: astore_1
    //   606: aload_1
    //   607: ifnonnull -> 624
    //   610: iconst_1
    //   611: istore #16
    //   613: iload #9
    //   615: istore #5
    //   617: iload #16
    //   619: istore #9
    //   621: goto -> 650
    //   624: iload #9
    //   626: istore #5
    //   628: iload #16
    //   630: istore #9
    //   632: aload_1
    //   633: invokevirtual getRttAudioMode : ()I
    //   636: aload_2
    //   637: invokevirtual getRttAudioMode : ()I
    //   640: if_icmpeq -> 650
    //   643: iconst_1
    //   644: istore #5
    //   646: iload #16
    //   648: istore #9
    //   650: aload_0
    //   651: aload_2
    //   652: putfield mRttCall : Landroid/telecom/Call$RttCall;
    //   655: goto -> 696
    //   658: goto -> 661
    //   661: aload_0
    //   662: getfield mRttCall : Landroid/telecom/Call$RttCall;
    //   665: ifnull -> 693
    //   668: aload_1
    //   669: invokevirtual getParcelableRttCall : ()Landroid/telecom/ParcelableRttCall;
    //   672: ifnonnull -> 693
    //   675: aload_1
    //   676: invokevirtual getIsRttCallChanged : ()Z
    //   679: ifeq -> 693
    //   682: iconst_1
    //   683: istore #9
    //   685: aload_0
    //   686: aconst_null
    //   687: putfield mRttCall : Landroid/telecom/Call$RttCall;
    //   690: goto -> 696
    //   693: iconst_0
    //   694: istore #9
    //   696: iload #10
    //   698: ifeq -> 709
    //   701: aload_0
    //   702: aload_0
    //   703: getfield mState : I
    //   706: invokespecial fireStateChanged : (I)V
    //   709: iload #4
    //   711: ifeq -> 722
    //   714: aload_0
    //   715: aload_0
    //   716: getfield mDetails : Landroid/telecom/Call$Details;
    //   719: invokespecial fireDetailsChanged : (Landroid/telecom/Call$Details;)V
    //   722: iload #6
    //   724: ifeq -> 735
    //   727: aload_0
    //   728: aload_0
    //   729: getfield mCannedTextResponses : Ljava/util/List;
    //   732: invokespecial fireCannedTextResponsesLoaded : (Ljava/util/List;)V
    //   735: iload #8
    //   737: ifeq -> 748
    //   740: aload_0
    //   741: aload_0
    //   742: getfield mVideoCallImpl : Landroid/telecom/VideoCallImpl;
    //   745: invokespecial fireVideoCallChanged : (Landroid/telecom/InCallService$VideoCall;)V
    //   748: iload #11
    //   750: ifeq -> 761
    //   753: aload_0
    //   754: aload_0
    //   755: invokevirtual getParent : ()Landroid/telecom/Call;
    //   758: invokespecial fireParentChanged : (Landroid/telecom/Call;)V
    //   761: iload #12
    //   763: ifne -> 771
    //   766: iload #13
    //   768: ifeq -> 779
    //   771: aload_0
    //   772: aload_0
    //   773: invokevirtual getChildren : ()Ljava/util/List;
    //   776: invokespecial fireChildrenChanged : (Ljava/util/List;)V
    //   779: iload #9
    //   781: ifeq -> 810
    //   784: aload_0
    //   785: getfield mRttCall : Landroid/telecom/Call$RttCall;
    //   788: ifnull -> 797
    //   791: iconst_1
    //   792: istore #17
    //   794: goto -> 800
    //   797: iconst_0
    //   798: istore #17
    //   800: aload_0
    //   801: iload #17
    //   803: aload_0
    //   804: getfield mRttCall : Landroid/telecom/Call$RttCall;
    //   807: invokespecial fireOnIsRttChanged : (ZLandroid/telecom/Call$RttCall;)V
    //   810: iload #5
    //   812: ifeq -> 826
    //   815: aload_0
    //   816: aload_0
    //   817: getfield mRttCall : Landroid/telecom/Call$RttCall;
    //   820: invokevirtual getRttAudioMode : ()I
    //   823: invokespecial fireOnRttModeChanged : (I)V
    //   826: getstatic android/telecom/Log.DEBUG : Z
    //   829: ifeq -> 1110
    //   832: new java/lang/StringBuilder
    //   835: dup
    //   836: invokespecial <init> : ()V
    //   839: astore_1
    //   840: aload_1
    //   841: ldc_w '\\n Call_internalUpdate \\n{ '
    //   844: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   847: pop
    //   848: new java/lang/StringBuilder
    //   851: dup
    //   852: invokespecial <init> : ()V
    //   855: astore_2
    //   856: aload_2
    //   857: ldc_w '\\n conferenceableChanged: '
    //   860: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   863: pop
    //   864: aload_2
    //   865: iload #15
    //   867: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   870: pop
    //   871: aload_2
    //   872: invokevirtual toString : ()Ljava/lang/String;
    //   875: astore_2
    //   876: aload_1
    //   877: aload_2
    //   878: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   881: pop
    //   882: new java/lang/StringBuilder
    //   885: dup
    //   886: invokespecial <init> : ()V
    //   889: astore_2
    //   890: aload_2
    //   891: ldc_w '\\n stateChanged: '
    //   894: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   897: pop
    //   898: aload_2
    //   899: iload #10
    //   901: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   904: pop
    //   905: aload_2
    //   906: invokevirtual toString : ()Ljava/lang/String;
    //   909: astore_2
    //   910: aload_1
    //   911: aload_2
    //   912: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   915: pop
    //   916: new java/lang/StringBuilder
    //   919: dup
    //   920: invokespecial <init> : ()V
    //   923: astore_2
    //   924: aload_2
    //   925: ldc_w '\\n detailsChanged: '
    //   928: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   931: pop
    //   932: aload_2
    //   933: iload #4
    //   935: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   938: pop
    //   939: aload_2
    //   940: invokevirtual toString : ()Ljava/lang/String;
    //   943: astore_2
    //   944: aload_1
    //   945: aload_2
    //   946: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   949: pop
    //   950: new java/lang/StringBuilder
    //   953: dup
    //   954: invokespecial <init> : ()V
    //   957: astore_2
    //   958: aload_2
    //   959: ldc_w '\\n videoCallChanged: '
    //   962: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   965: pop
    //   966: aload_2
    //   967: iload #8
    //   969: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   972: pop
    //   973: aload_2
    //   974: invokevirtual toString : ()Ljava/lang/String;
    //   977: astore_2
    //   978: aload_1
    //   979: aload_2
    //   980: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   983: pop
    //   984: new java/lang/StringBuilder
    //   987: dup
    //   988: invokespecial <init> : ()V
    //   991: astore_2
    //   992: aload_2
    //   993: ldc_w '\\n parentChanged: '
    //   996: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   999: pop
    //   1000: aload_2
    //   1001: iload #11
    //   1003: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   1006: pop
    //   1007: aload_2
    //   1008: invokevirtual toString : ()Ljava/lang/String;
    //   1011: astore_2
    //   1012: aload_1
    //   1013: aload_2
    //   1014: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1017: pop
    //   1018: new java/lang/StringBuilder
    //   1021: dup
    //   1022: invokespecial <init> : ()V
    //   1025: astore_2
    //   1026: aload_2
    //   1027: ldc_w '\\n childrenChanged: '
    //   1030: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1033: pop
    //   1034: aload_2
    //   1035: iload #12
    //   1037: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   1040: pop
    //   1041: aload_2
    //   1042: invokevirtual toString : ()Ljava/lang/String;
    //   1045: astore_2
    //   1046: aload_1
    //   1047: aload_2
    //   1048: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1051: pop
    //   1052: new java/lang/StringBuilder
    //   1055: dup
    //   1056: invokespecial <init> : ()V
    //   1059: astore_2
    //   1060: aload_2
    //   1061: ldc_w '\\n mState: '
    //   1064: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1067: pop
    //   1068: aload_2
    //   1069: aload_0
    //   1070: getfield mState : I
    //   1073: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1076: pop
    //   1077: aload_2
    //   1078: invokevirtual toString : ()Ljava/lang/String;
    //   1081: astore_2
    //   1082: aload_1
    //   1083: aload_2
    //   1084: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1087: pop
    //   1088: aload_1
    //   1089: ldc_w ' }\\n'
    //   1092: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1095: pop
    //   1096: aload_1
    //   1097: invokevirtual toString : ()Ljava/lang/String;
    //   1100: astore_1
    //   1101: aload_0
    //   1102: aload_1
    //   1103: iconst_0
    //   1104: anewarray java/lang/Object
    //   1107: invokestatic d : (Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V
    //   1110: iload #10
    //   1112: ifeq -> 1128
    //   1115: aload_0
    //   1116: getfield mState : I
    //   1119: bipush #7
    //   1121: if_icmpne -> 1128
    //   1124: aload_0
    //   1125: invokespecial fireCallDestroyed : ()V
    //   1128: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2317	-> 0
    //   #2318	-> 5
    //   #2319	-> 17
    //   #2320	-> 22
    //   #2323	-> 27
    //   #2324	-> 30
    //   #2325	-> 52
    //   #2326	-> 68
    //   #2327	-> 68
    //   #2328	-> 79
    //   #2331	-> 82
    //   #2332	-> 96
    //   #2333	-> 101
    //   #2337	-> 107
    //   #2338	-> 114
    //   #2339	-> 132
    //   #2340	-> 137
    //   #2341	-> 146
    //   #2343	-> 150
    //   #2344	-> 157
    //   #2339	-> 183
    //   #2347	-> 183
    //   #2348	-> 192
    //   #2351	-> 203
    //   #2352	-> 209
    //   #2353	-> 233
    //   #2355	-> 236
    //   #2356	-> 254
    //   #2357	-> 259
    //   #2360	-> 265
    //   #2361	-> 270
    //   #2362	-> 282
    //   #2363	-> 287
    //   #2366	-> 292
    //   #2367	-> 297
    //   #2368	-> 309
    //   #2369	-> 314
    //   #2370	-> 323
    //   #2371	-> 337
    //   #2368	-> 345
    //   #2374	-> 345
    //   #2375	-> 350
    //   #2377	-> 362
    //   #2378	-> 367
    //   #2381	-> 372
    //   #2382	-> 377
    //   #2383	-> 392
    //   #2384	-> 419
    //   #2385	-> 430
    //   #2387	-> 449
    //   #2394	-> 452
    //   #2395	-> 465
    //   #2397	-> 470
    //   #2398	-> 479
    //   #2399	-> 491
    //   #2402	-> 495
    //   #2403	-> 495
    //   #2404	-> 501
    //   #2405	-> 516
    //   #2406	-> 526
    //   #2407	-> 531
    //   #2409	-> 531
    //   #2411	-> 553
    //   #2413	-> 553
    //   #2415	-> 576
    //   #2416	-> 581
    //   #2417	-> 601
    //   #2418	-> 610
    //   #2419	-> 624
    //   #2420	-> 643
    //   #2422	-> 650
    //   #2423	-> 655
    //   #2405	-> 658
    //   #2404	-> 661
    //   #2423	-> 661
    //   #2424	-> 675
    //   #2425	-> 682
    //   #2426	-> 685
    //   #2432	-> 693
    //   #2433	-> 701
    //   #2435	-> 709
    //   #2436	-> 714
    //   #2438	-> 722
    //   #2439	-> 727
    //   #2441	-> 735
    //   #2442	-> 740
    //   #2444	-> 748
    //   #2445	-> 753
    //   #2447	-> 761
    //   #2448	-> 771
    //   #2450	-> 779
    //   #2451	-> 784
    //   #2453	-> 810
    //   #2454	-> 815
    //   #2459	-> 826
    //   #2460	-> 832
    //   #2461	-> 840
    //   #2462	-> 876
    //   #2463	-> 910
    //   #2464	-> 944
    //   #2465	-> 978
    //   #2466	-> 1012
    //   #2467	-> 1046
    //   #2468	-> 1082
    //   #2469	-> 1088
    //   #2471	-> 1101
    //   #2486	-> 1110
    //   #2488	-> 1124
    //   #2490	-> 1128
  }
  
  final void internalSetPostDialWait(String paramString) {
    this.mRemainingPostDialSequence = paramString;
    firePostDialWait(paramString);
  }
  
  final void internalSetDisconnected() {
    if (this.mState != 7) {
      this.mState = 7;
      fireStateChanged(7);
      fireCallDestroyed();
    } 
  }
  
  final void internalOnConnectionEvent(String paramString, Bundle paramBundle) {
    fireOnConnectionEvent(paramString, paramBundle);
  }
  
  final void internalOnRttUpgradeRequest(int paramInt) {
    for (CallbackRecord<Callback> callbackRecord : this.mCallbackRecords) {
      Callback callback = callbackRecord.getCallback();
      callbackRecord.getHandler().post(new _$$Lambda$Call$hgXdGxKbb9IRpCeFrYieOwUuElE(callback, this, paramInt));
    } 
  }
  
  final void internalOnRttInitiationFailure(int paramInt) {
    for (CallbackRecord<Callback> callbackRecord : this.mCallbackRecords) {
      Callback callback = callbackRecord.getCallback();
      callbackRecord.getHandler().post(new _$$Lambda$Call$JyYlHynNNc3DTrfrP5aXatJNft4(callback, this, paramInt));
    } 
  }
  
  final void internalOnHandoverFailed(int paramInt) {
    for (CallbackRecord<Callback> callbackRecord : this.mCallbackRecords) {
      Callback callback = callbackRecord.getCallback();
      callbackRecord.getHandler().post(new _$$Lambda$Call$aPdcAxyKfpxcuraTjET8ce3xApc(callback, this, paramInt));
    } 
  }
  
  final void internalOnHandoverComplete() {
    for (CallbackRecord<Callback> callbackRecord : this.mCallbackRecords) {
      Callback callback = callbackRecord.getCallback();
      callbackRecord.getHandler().post(new _$$Lambda$Call$bt1B6cq3ylYqEtzOXnJWMeJ_ojc(callback, this));
    } 
  }
  
  private void fireStateChanged(final int newState) {
    for (CallbackRecord<Callback> callbackRecord : this.mCallbackRecords) {
      final Callback callback = callbackRecord.getCallback();
      callbackRecord.getHandler().post(new Runnable() {
            final Call this$0;
            
            final Call val$call;
            
            final Call.Callback val$callback;
            
            final int val$newState;
            
            public void run() {
              callback.onStateChanged(call, newState);
            }
          });
    } 
  }
  
  private void fireParentChanged(final Call newParent) {
    for (CallbackRecord<Callback> callbackRecord : this.mCallbackRecords) {
      final Callback callback = callbackRecord.getCallback();
      callbackRecord.getHandler().post(new Runnable() {
            final Call this$0;
            
            final Call val$call;
            
            final Call.Callback val$callback;
            
            final Call val$newParent;
            
            public void run() {
              callback.onParentChanged(call, newParent);
            }
          });
    } 
  }
  
  private void fireChildrenChanged(final List<Call> children) {
    for (CallbackRecord<Callback> callbackRecord : this.mCallbackRecords) {
      final Callback callback = callbackRecord.getCallback();
      callbackRecord.getHandler().post(new Runnable() {
            final Call this$0;
            
            final Call val$call;
            
            final Call.Callback val$callback;
            
            final List val$children;
            
            public void run() {
              callback.onChildrenChanged(call, children);
            }
          });
    } 
  }
  
  private void fireDetailsChanged(final Details details) {
    for (CallbackRecord<Callback> callbackRecord : this.mCallbackRecords) {
      final Callback callback = callbackRecord.getCallback();
      callbackRecord.getHandler().post(new Runnable() {
            final Call this$0;
            
            final Call val$call;
            
            final Call.Callback val$callback;
            
            final Call.Details val$details;
            
            public void run() {
              callback.onDetailsChanged(call, details);
            }
          });
    } 
  }
  
  private void fireCannedTextResponsesLoaded(final List<String> cannedTextResponses) {
    for (CallbackRecord<Callback> callbackRecord : this.mCallbackRecords) {
      final Callback callback = callbackRecord.getCallback();
      callbackRecord.getHandler().post(new Runnable() {
            final Call this$0;
            
            final Call val$call;
            
            final Call.Callback val$callback;
            
            final List val$cannedTextResponses;
            
            public void run() {
              callback.onCannedTextResponsesLoaded(call, cannedTextResponses);
            }
          });
    } 
  }
  
  private void fireVideoCallChanged(final InCallService.VideoCall videoCall) {
    for (CallbackRecord<Callback> callbackRecord : this.mCallbackRecords) {
      final Callback callback = callbackRecord.getCallback();
      callbackRecord.getHandler().post(new Runnable() {
            final Call this$0;
            
            final Call val$call;
            
            final Call.Callback val$callback;
            
            final InCallService.VideoCall val$videoCall;
            
            public void run() {
              callback.onVideoCallChanged(call, videoCall);
            }
          });
    } 
  }
  
  private void firePostDialWait(final String remainingPostDialSequence) {
    for (CallbackRecord<Callback> callbackRecord : this.mCallbackRecords) {
      final Callback callback = callbackRecord.getCallback();
      callbackRecord.getHandler().post(new Runnable() {
            final Call this$0;
            
            final Call val$call;
            
            final Call.Callback val$callback;
            
            final String val$remainingPostDialSequence;
            
            public void run() {
              callback.onPostDialWait(call, remainingPostDialSequence);
            }
          });
    } 
  }
  
  private void fireCallDestroyed() {
    if (this.mCallbackRecords.isEmpty())
      this.mPhone.internalRemoveCall(this); 
    for (CallbackRecord<Callback> callbackRecord : this.mCallbackRecords) {
      final Callback callback = callbackRecord.getCallback();
      callbackRecord.getHandler().post(new Runnable() {
            final Call this$0;
            
            final Call val$call;
            
            final Call.Callback val$callback;
            
            final CallbackRecord val$record;
            
            public void run() {
              boolean bool = false;
              null = null;
              try {
                callback.onCallDestroyed(call);
              } catch (RuntimeException null) {}
              synchronized (Call.this) {
                Call.this.mCallbackRecords.remove(record);
                if (Call.this.mCallbackRecords.isEmpty())
                  bool = true; 
                if (bool)
                  Call.this.mPhone.internalRemoveCall(call); 
                if (null == null)
                  return; 
                throw null;
              } 
            }
          });
    } 
  }
  
  private void fireConferenceableCallsChanged() {
    for (CallbackRecord<Callback> callbackRecord : this.mCallbackRecords) {
      final Callback callback = callbackRecord.getCallback();
      callbackRecord.getHandler().post(new Runnable() {
            final Call this$0;
            
            final Call val$call;
            
            final Call.Callback val$callback;
            
            public void run() {
              callback.onConferenceableCallsChanged(call, Call.this.mUnmodifiableConferenceableCalls);
            }
          });
    } 
  }
  
  private void fireOnConnectionEvent(final String event, final Bundle extras) {
    for (CallbackRecord<Callback> callbackRecord : this.mCallbackRecords) {
      final Callback callback = callbackRecord.getCallback();
      callbackRecord.getHandler().post(new Runnable() {
            final Call this$0;
            
            final Call val$call;
            
            final Call.Callback val$callback;
            
            final String val$event;
            
            final Bundle val$extras;
            
            public void run() {
              callback.onConnectionEvent(call, event, extras);
            }
          });
    } 
  }
  
  private void fireOnIsRttChanged(boolean paramBoolean, RttCall paramRttCall) {
    for (CallbackRecord<Callback> callbackRecord : this.mCallbackRecords) {
      Callback callback = callbackRecord.getCallback();
      callbackRecord.getHandler().post(new _$$Lambda$Call$5JdbCgV1DP_WhiljnHJKKAJdCu0(callback, this, paramBoolean, paramRttCall));
    } 
  }
  
  private void fireOnRttModeChanged(int paramInt) {
    for (CallbackRecord<Callback> callbackRecord : this.mCallbackRecords) {
      Callback callback = callbackRecord.getCallback();
      callbackRecord.getHandler().post(new _$$Lambda$Call$qjo4awib5yVZC_4Qe_hhqUSk7ho(callback, this, paramInt));
    } 
  }
  
  private static boolean areBundlesEqual(Bundle paramBundle1, Bundle paramBundle2) {
    boolean bool = true;
    if (paramBundle1 == null || paramBundle2 == null) {
      if (paramBundle1 != paramBundle2)
        bool = false; 
      return bool;
    } 
    if (paramBundle1.size() != paramBundle2.size())
      return false; 
    for (String str : paramBundle1.keySet()) {
      if (str != null) {
        Object object2 = paramBundle1.get(str);
        Object object1 = paramBundle2.get(str);
        if (!Objects.equals(object2, object1))
          return false; 
      } 
    } 
    return true;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface HandoverFailureErrors {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface CallDirection {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface RejectReason {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface RttAudioMode {}
}
