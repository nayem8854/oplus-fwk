package android.telecom;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public class GatewayInfo implements Parcelable {
  public GatewayInfo(String paramString, Uri paramUri1, Uri paramUri2) {
    this.mGatewayProviderPackageName = paramString;
    this.mGatewayAddress = paramUri1;
    this.mOriginalAddress = paramUri2;
  }
  
  public String getGatewayProviderPackageName() {
    return this.mGatewayProviderPackageName;
  }
  
  public Uri getGatewayAddress() {
    return this.mGatewayAddress;
  }
  
  public Uri getOriginalAddress() {
    return this.mOriginalAddress;
  }
  
  public boolean isEmpty() {
    return (TextUtils.isEmpty(this.mGatewayProviderPackageName) || this.mGatewayAddress == null);
  }
  
  public static final Parcelable.Creator<GatewayInfo> CREATOR = new Parcelable.Creator<GatewayInfo>() {
      public GatewayInfo createFromParcel(Parcel param1Parcel) {
        String str = param1Parcel.readString();
        Uri uri2 = (Uri)Uri.CREATOR.createFromParcel(param1Parcel);
        Uri uri1 = (Uri)Uri.CREATOR.createFromParcel(param1Parcel);
        return new GatewayInfo(str, uri2, uri1);
      }
      
      public GatewayInfo[] newArray(int param1Int) {
        return new GatewayInfo[param1Int];
      }
    };
  
  private final Uri mGatewayAddress;
  
  private final String mGatewayProviderPackageName;
  
  private final Uri mOriginalAddress;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mGatewayProviderPackageName);
    Uri.writeToParcel(paramParcel, this.mGatewayAddress);
    Uri.writeToParcel(paramParcel, this.mOriginalAddress);
  }
}
