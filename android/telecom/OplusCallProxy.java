package android.telecom;

public class OplusCallProxy {
  private static final String TAG = "OppoCallProxy";
  
  private Call mCall;
  
  private OplusCallProxy(Call paramCall) {
    this.mCall = paramCall;
  }
  
  public static OplusCallProxy map(Call paramCall) {
    return new OplusCallProxy(paramCall);
  }
  
  public String internalGetCallId() {
    Call call = this.mCall;
    if (call != null)
      call.internalGetCallId(); 
    return null;
  }
}
