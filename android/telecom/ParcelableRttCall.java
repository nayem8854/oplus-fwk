package android.telecom;

import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;

public class ParcelableRttCall implements Parcelable {
  public ParcelableRttCall(int paramInt, ParcelFileDescriptor paramParcelFileDescriptor1, ParcelFileDescriptor paramParcelFileDescriptor2) {
    this.mRttMode = paramInt;
    this.mTransmitStream = paramParcelFileDescriptor1;
    this.mReceiveStream = paramParcelFileDescriptor2;
  }
  
  protected ParcelableRttCall(Parcel paramParcel) {
    this.mRttMode = paramParcel.readInt();
    this.mTransmitStream = (ParcelFileDescriptor)paramParcel.readParcelable(ParcelFileDescriptor.class.getClassLoader());
    this.mReceiveStream = (ParcelFileDescriptor)paramParcel.readParcelable(ParcelFileDescriptor.class.getClassLoader());
  }
  
  public static final Parcelable.Creator<ParcelableRttCall> CREATOR = new Parcelable.Creator<ParcelableRttCall>() {
      public ParcelableRttCall createFromParcel(Parcel param1Parcel) {
        return new ParcelableRttCall(param1Parcel);
      }
      
      public ParcelableRttCall[] newArray(int param1Int) {
        return new ParcelableRttCall[param1Int];
      }
    };
  
  private final ParcelFileDescriptor mReceiveStream;
  
  private final int mRttMode;
  
  private final ParcelFileDescriptor mTransmitStream;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mRttMode);
    paramParcel.writeParcelable((Parcelable)this.mTransmitStream, paramInt);
    paramParcel.writeParcelable((Parcelable)this.mReceiveStream, paramInt);
  }
  
  public int getRttMode() {
    return this.mRttMode;
  }
  
  public ParcelFileDescriptor getReceiveStream() {
    return this.mReceiveStream;
  }
  
  public ParcelFileDescriptor getTransmitStream() {
    return this.mTransmitStream;
  }
}
