package android.telecom;

import android.annotation.SystemApi;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public final class PhoneAccount implements Parcelable {
  public static final int CAPABILITY_ADHOC_CONFERENCE_CALLING = 16384;
  
  public static final int CAPABILITY_CALL_PROVIDER = 2;
  
  public static final int CAPABILITY_CALL_SUBJECT = 64;
  
  public static final int CAPABILITY_CONNECTION_MANAGER = 1;
  
  public static final int CAPABILITY_EMERGENCY_CALLS_ONLY = 128;
  
  public static final int CAPABILITY_EMERGENCY_PREFERRED = 8192;
  
  public static final int CAPABILITY_EMERGENCY_VIDEO_CALLING = 512;
  
  @SystemApi
  public static final int CAPABILITY_MULTI_USER = 32;
  
  public static final int CAPABILITY_PLACE_EMERGENCY_CALLS = 16;
  
  public static final int CAPABILITY_RTT = 4096;
  
  public static final int CAPABILITY_SELF_MANAGED = 2048;
  
  public static final int CAPABILITY_SIM_SUBSCRIPTION = 4;
  
  public static final int CAPABILITY_SUPPORTS_VIDEO_CALLING = 1024;
  
  public static final int CAPABILITY_VIDEO_CALLING = 8;
  
  public static final int CAPABILITY_VIDEO_CALLING_RELIES_ON_PRESENCE = 256;
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mCapabilities == ((PhoneAccount)paramObject).mCapabilities && this.mHighlightColor == ((PhoneAccount)paramObject).mHighlightColor && this.mSupportedAudioRoutes == ((PhoneAccount)paramObject).mSupportedAudioRoutes && this.mIsEnabled == ((PhoneAccount)paramObject).mIsEnabled) {
      PhoneAccountHandle phoneAccountHandle1 = this.mAccountHandle, phoneAccountHandle2 = ((PhoneAccount)paramObject).mAccountHandle;
      if (Objects.equals(phoneAccountHandle1, phoneAccountHandle2)) {
        Uri uri2 = this.mAddress, uri1 = ((PhoneAccount)paramObject).mAddress;
        if (Objects.equals(uri2, uri1)) {
          uri2 = this.mSubscriptionAddress;
          uri1 = ((PhoneAccount)paramObject).mSubscriptionAddress;
          if (Objects.equals(uri2, uri1)) {
            CharSequence charSequence1 = this.mLabel, charSequence2 = ((PhoneAccount)paramObject).mLabel;
            if (Objects.equals(charSequence1, charSequence2)) {
              charSequence2 = this.mShortDescription;
              charSequence1 = ((PhoneAccount)paramObject).mShortDescription;
              if (Objects.equals(charSequence2, charSequence1)) {
                List<String> list2 = this.mSupportedUriSchemes, list1 = ((PhoneAccount)paramObject).mSupportedUriSchemes;
                if (Objects.equals(list2, list1)) {
                  Bundle bundle1 = this.mExtras, bundle2 = ((PhoneAccount)paramObject).mExtras;
                  if (areBundlesEqual(bundle1, bundle2)) {
                    String str = this.mGroupId;
                    paramObject = ((PhoneAccount)paramObject).mGroupId;
                    if (Objects.equals(str, paramObject))
                      return null; 
                  } 
                } 
              } 
            } 
          } 
        } 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    PhoneAccountHandle phoneAccountHandle = this.mAccountHandle;
    Uri uri1 = this.mAddress, uri2 = this.mSubscriptionAddress;
    int i = this.mCapabilities, j = this.mHighlightColor;
    CharSequence charSequence1 = this.mLabel, charSequence2 = this.mShortDescription;
    List<String> list = this.mSupportedUriSchemes;
    int k = this.mSupportedAudioRoutes;
    Bundle bundle = this.mExtras;
    boolean bool = this.mIsEnabled;
    String str = this.mGroupId;
    return Objects.hash(new Object[] { 
          phoneAccountHandle, uri1, uri2, Integer.valueOf(i), Integer.valueOf(j), charSequence1, charSequence2, list, Integer.valueOf(k), bundle, 
          Boolean.valueOf(bool), str });
  }
  
  class Builder {
    private int mSupportedAudioRoutes = 15;
    
    private int mHighlightColor = 0;
    
    private List<String> mSupportedUriSchemes = new ArrayList<>();
    
    private boolean mIsEnabled = false;
    
    private String mGroupId = "";
    
    private PhoneAccountHandle mAccountHandle;
    
    private Uri mAddress;
    
    private int mCapabilities;
    
    private Bundle mExtras;
    
    private Icon mIcon;
    
    private CharSequence mLabel;
    
    private CharSequence mShortDescription;
    
    private Uri mSubscriptionAddress;
    
    public Builder(PhoneAccount this$0, CharSequence param1CharSequence) {
      this.mAccountHandle = (PhoneAccountHandle)this$0;
      this.mLabel = param1CharSequence;
    }
    
    public Builder(PhoneAccount this$0) {
      this.mAccountHandle = this$0.getAccountHandle();
      this.mAddress = this$0.getAddress();
      this.mSubscriptionAddress = this$0.getSubscriptionAddress();
      this.mCapabilities = this$0.getCapabilities();
      this.mHighlightColor = this$0.getHighlightColor();
      this.mLabel = this$0.getLabel();
      this.mShortDescription = this$0.getShortDescription();
      this.mSupportedUriSchemes.addAll(this$0.getSupportedUriSchemes());
      this.mIcon = this$0.getIcon();
      this.mIsEnabled = this$0.isEnabled();
      this.mExtras = this$0.getExtras();
      this.mGroupId = this$0.getGroupId();
      this.mSupportedAudioRoutes = this$0.getSupportedAudioRoutes();
    }
    
    public Builder setLabel(CharSequence param1CharSequence) {
      this.mLabel = param1CharSequence;
      return this;
    }
    
    public Builder setAddress(Uri param1Uri) {
      this.mAddress = param1Uri;
      return this;
    }
    
    public Builder setSubscriptionAddress(Uri param1Uri) {
      this.mSubscriptionAddress = param1Uri;
      return this;
    }
    
    public Builder setCapabilities(int param1Int) {
      this.mCapabilities = param1Int;
      return this;
    }
    
    public Builder setIcon(Icon param1Icon) {
      this.mIcon = param1Icon;
      return this;
    }
    
    public Builder setHighlightColor(int param1Int) {
      this.mHighlightColor = param1Int;
      return this;
    }
    
    public Builder setShortDescription(CharSequence param1CharSequence) {
      this.mShortDescription = param1CharSequence;
      return this;
    }
    
    public Builder addSupportedUriScheme(String param1String) {
      if (!TextUtils.isEmpty(param1String) && !this.mSupportedUriSchemes.contains(param1String))
        this.mSupportedUriSchemes.add(param1String); 
      return this;
    }
    
    public Builder setSupportedUriSchemes(List<String> param1List) {
      this.mSupportedUriSchemes.clear();
      if (param1List != null && !param1List.isEmpty())
        for (String str : param1List)
          addSupportedUriScheme(str);  
      return this;
    }
    
    public Builder setExtras(Bundle param1Bundle) {
      this.mExtras = param1Bundle;
      return this;
    }
    
    public Builder setIsEnabled(boolean param1Boolean) {
      this.mIsEnabled = param1Boolean;
      return this;
    }
    
    @SystemApi
    public Builder setGroupId(String param1String) {
      if (param1String != null) {
        this.mGroupId = param1String;
      } else {
        this.mGroupId = "";
      } 
      return this;
    }
    
    public Builder setSupportedAudioRoutes(int param1Int) {
      this.mSupportedAudioRoutes = param1Int;
      return this;
    }
    
    public PhoneAccount build() {
      if (this.mSupportedUriSchemes.isEmpty())
        addSupportedUriScheme("tel"); 
      return new PhoneAccount(this.mAccountHandle, this.mAddress, this.mSubscriptionAddress, this.mCapabilities, this.mIcon, this.mHighlightColor, this.mLabel, this.mShortDescription, this.mSupportedUriSchemes, this.mExtras, this.mSupportedAudioRoutes, this.mIsEnabled, this.mGroupId);
    }
  }
  
  private PhoneAccount(PhoneAccountHandle paramPhoneAccountHandle, Uri paramUri1, Uri paramUri2, int paramInt1, Icon paramIcon, int paramInt2, CharSequence paramCharSequence1, CharSequence paramCharSequence2, List<String> paramList, Bundle paramBundle, int paramInt3, boolean paramBoolean, String paramString) {
    this.mAccountHandle = paramPhoneAccountHandle;
    this.mAddress = paramUri1;
    this.mSubscriptionAddress = paramUri2;
    this.mCapabilities = paramInt1;
    this.mIcon = paramIcon;
    this.mHighlightColor = paramInt2;
    this.mLabel = paramCharSequence1;
    this.mShortDescription = paramCharSequence2;
    this.mSupportedUriSchemes = Collections.unmodifiableList(paramList);
    this.mExtras = paramBundle;
    this.mSupportedAudioRoutes = paramInt3;
    this.mIsEnabled = paramBoolean;
    this.mGroupId = paramString;
  }
  
  public static Builder builder(PhoneAccountHandle paramPhoneAccountHandle, CharSequence paramCharSequence) {
    return new Builder(paramPhoneAccountHandle, paramCharSequence);
  }
  
  public Builder toBuilder() {
    return new Builder(this);
  }
  
  public PhoneAccountHandle getAccountHandle() {
    return this.mAccountHandle;
  }
  
  public Uri getAddress() {
    return this.mAddress;
  }
  
  public Uri getSubscriptionAddress() {
    return this.mSubscriptionAddress;
  }
  
  public int getCapabilities() {
    return this.mCapabilities;
  }
  
  public boolean hasCapabilities(int paramInt) {
    boolean bool;
    if ((this.mCapabilities & paramInt) == paramInt) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean hasAudioRoutes(int paramInt) {
    boolean bool;
    if ((this.mSupportedAudioRoutes & paramInt) == paramInt) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public CharSequence getLabel() {
    return this.mLabel;
  }
  
  public CharSequence getShortDescription() {
    return this.mShortDescription;
  }
  
  public List<String> getSupportedUriSchemes() {
    return this.mSupportedUriSchemes;
  }
  
  public Bundle getExtras() {
    return this.mExtras;
  }
  
  public int getSupportedAudioRoutes() {
    return this.mSupportedAudioRoutes;
  }
  
  public Icon getIcon() {
    return this.mIcon;
  }
  
  public boolean isEnabled() {
    return this.mIsEnabled;
  }
  
  public String getGroupId() {
    return this.mGroupId;
  }
  
  public boolean supportsUriScheme(String paramString) {
    List<String> list = this.mSupportedUriSchemes;
    if (list == null || paramString == null)
      return false; 
    for (String str : list) {
      if (str != null && str.equals(paramString))
        return true; 
    } 
    return false;
  }
  
  public int getHighlightColor() {
    return this.mHighlightColor;
  }
  
  public void setIsEnabled(boolean paramBoolean) {
    this.mIsEnabled = paramBoolean;
  }
  
  public boolean isSelfManaged() {
    boolean bool;
    if ((this.mCapabilities & 0x800) == 2048) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    if (this.mAccountHandle == null) {
      paramParcel.writeInt(0);
    } else {
      paramParcel.writeInt(1);
      this.mAccountHandle.writeToParcel(paramParcel, paramInt);
    } 
    if (this.mAddress == null) {
      paramParcel.writeInt(0);
    } else {
      paramParcel.writeInt(1);
      this.mAddress.writeToParcel(paramParcel, paramInt);
    } 
    if (this.mSubscriptionAddress == null) {
      paramParcel.writeInt(0);
    } else {
      paramParcel.writeInt(1);
      this.mSubscriptionAddress.writeToParcel(paramParcel, paramInt);
    } 
    paramParcel.writeInt(this.mCapabilities);
    paramParcel.writeInt(this.mHighlightColor);
    paramParcel.writeCharSequence(this.mLabel);
    paramParcel.writeCharSequence(this.mShortDescription);
    paramParcel.writeStringList(this.mSupportedUriSchemes);
    if (this.mIcon == null) {
      paramParcel.writeInt(0);
    } else {
      paramParcel.writeInt(1);
      this.mIcon.writeToParcel(paramParcel, paramInt);
    } 
    paramParcel.writeByte((byte)this.mIsEnabled);
    paramParcel.writeBundle(this.mExtras);
    paramParcel.writeString(this.mGroupId);
    paramParcel.writeInt(this.mSupportedAudioRoutes);
  }
  
  public static final Parcelable.Creator<PhoneAccount> CREATOR = new Parcelable.Creator<PhoneAccount>() {
      public PhoneAccount createFromParcel(Parcel param1Parcel) {
        return new PhoneAccount(param1Parcel);
      }
      
      public PhoneAccount[] newArray(int param1Int) {
        return new PhoneAccount[param1Int];
      }
    };
  
  public static final String EXTRA_ALWAYS_USE_VOIP_AUDIO_MODE = "android.telecom.extra.ALWAYS_USE_VOIP_AUDIO_MODE";
  
  public static final String EXTRA_CALL_SUBJECT_CHARACTER_ENCODING = "android.telecom.extra.CALL_SUBJECT_CHARACTER_ENCODING";
  
  public static final String EXTRA_CALL_SUBJECT_MAX_LENGTH = "android.telecom.extra.CALL_SUBJECT_MAX_LENGTH";
  
  public static final String EXTRA_LOG_SELF_MANAGED_CALLS = "android.telecom.extra.LOG_SELF_MANAGED_CALLS";
  
  public static final String EXTRA_PLAY_CALL_RECORDING_TONE = "android.telecom.extra.PLAY_CALL_RECORDING_TONE";
  
  public static final String EXTRA_SKIP_CALL_FILTERING = "android.telecom.extra.SKIP_CALL_FILTERING";
  
  public static final String EXTRA_SORT_ORDER = "android.telecom.extra.SORT_ORDER";
  
  public static final String EXTRA_SUPPORTS_HANDOVER_FROM = "android.telecom.extra.SUPPORTS_HANDOVER_FROM";
  
  public static final String EXTRA_SUPPORTS_HANDOVER_TO = "android.telecom.extra.SUPPORTS_HANDOVER_TO";
  
  public static final String EXTRA_SUPPORTS_VIDEO_CALLING_FALLBACK = "android.telecom.extra.SUPPORTS_VIDEO_CALLING_FALLBACK";
  
  public static final int NO_HIGHLIGHT_COLOR = 0;
  
  public static final int NO_ICON_TINT = 0;
  
  public static final int NO_RESOURCE_ID = -1;
  
  public static final String SCHEME_SIP = "sip";
  
  public static final String SCHEME_TEL = "tel";
  
  public static final String SCHEME_VOICEMAIL = "voicemail";
  
  private final PhoneAccountHandle mAccountHandle;
  
  private final Uri mAddress;
  
  private final int mCapabilities;
  
  private final Bundle mExtras;
  
  private String mGroupId;
  
  private final int mHighlightColor;
  
  private final Icon mIcon;
  
  private boolean mIsEnabled;
  
  private final CharSequence mLabel;
  
  private final CharSequence mShortDescription;
  
  private final Uri mSubscriptionAddress;
  
  private final int mSupportedAudioRoutes;
  
  private final List<String> mSupportedUriSchemes;
  
  private PhoneAccount(Parcel paramParcel) {
    if (paramParcel.readInt() > 0) {
      this.mAccountHandle = (PhoneAccountHandle)PhoneAccountHandle.CREATOR.createFromParcel(paramParcel);
    } else {
      this.mAccountHandle = null;
    } 
    if (paramParcel.readInt() > 0) {
      this.mAddress = (Uri)Uri.CREATOR.createFromParcel(paramParcel);
    } else {
      this.mAddress = null;
    } 
    if (paramParcel.readInt() > 0) {
      this.mSubscriptionAddress = (Uri)Uri.CREATOR.createFromParcel(paramParcel);
    } else {
      this.mSubscriptionAddress = null;
    } 
    this.mCapabilities = paramParcel.readInt();
    this.mHighlightColor = paramParcel.readInt();
    this.mLabel = paramParcel.readCharSequence();
    this.mShortDescription = paramParcel.readCharSequence();
    this.mSupportedUriSchemes = Collections.unmodifiableList(paramParcel.createStringArrayList());
    if (paramParcel.readInt() > 0) {
      this.mIcon = (Icon)Icon.CREATOR.createFromParcel(paramParcel);
    } else {
      this.mIcon = null;
    } 
    byte b = paramParcel.readByte();
    boolean bool = true;
    if (b != 1)
      bool = false; 
    this.mIsEnabled = bool;
    this.mExtras = paramParcel.readBundle();
    this.mGroupId = paramParcel.readString();
    this.mSupportedAudioRoutes = paramParcel.readInt();
  }
  
  public String toString() {
    byte b;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("[[");
    if (this.mIsEnabled) {
      b = 88;
    } else {
      b = 32;
    } 
    stringBuilder.append(b);
    stringBuilder.append("] PhoneAccount: ");
    PhoneAccountHandle phoneAccountHandle = this.mAccountHandle;
    stringBuilder.append(phoneAccountHandle);
    stringBuilder.append(" Capabilities: ");
    stringBuilder.append(capabilitiesToString());
    stringBuilder.append(" Audio Routes: ");
    stringBuilder.append(audioRoutesToString());
    stringBuilder = stringBuilder.append(" Schemes: ");
    for (String str : this.mSupportedUriSchemes) {
      stringBuilder.append(str);
      stringBuilder.append(" ");
    } 
    stringBuilder.append(" Extras: ");
    stringBuilder.append(this.mExtras);
    stringBuilder.append(" GroupId: ");
    stringBuilder.append(Log.pii(this.mGroupId));
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public String capabilitiesToString() {
    StringBuilder stringBuilder = new StringBuilder();
    if (hasCapabilities(2048))
      stringBuilder.append("SelfManaged "); 
    if (hasCapabilities(1024))
      stringBuilder.append("SuppVideo "); 
    if (hasCapabilities(8))
      stringBuilder.append("Video "); 
    if (hasCapabilities(256))
      stringBuilder.append("Presence "); 
    if (hasCapabilities(2))
      stringBuilder.append("CallProvider "); 
    if (hasCapabilities(64))
      stringBuilder.append("CallSubject "); 
    if (hasCapabilities(1))
      stringBuilder.append("ConnectionMgr "); 
    if (hasCapabilities(128))
      stringBuilder.append("EmergOnly "); 
    if (hasCapabilities(32))
      stringBuilder.append("MultiUser "); 
    if (hasCapabilities(16))
      stringBuilder.append("PlaceEmerg "); 
    if (hasCapabilities(8192))
      stringBuilder.append("EmerPrefer "); 
    if (hasCapabilities(512))
      stringBuilder.append("EmergVideo "); 
    if (hasCapabilities(4))
      stringBuilder.append("SimSub "); 
    if (hasCapabilities(4096))
      stringBuilder.append("Rtt"); 
    if (hasCapabilities(16384))
      stringBuilder.append("AdhocConf"); 
    return stringBuilder.toString();
  }
  
  private String audioRoutesToString() {
    StringBuilder stringBuilder = new StringBuilder();
    if (hasAudioRoutes(2))
      stringBuilder.append("B"); 
    if (hasAudioRoutes(1))
      stringBuilder.append("E"); 
    if (hasAudioRoutes(8))
      stringBuilder.append("S"); 
    if (hasAudioRoutes(4))
      stringBuilder.append("W"); 
    return stringBuilder.toString();
  }
  
  private static boolean areBundlesEqual(Bundle paramBundle1, Bundle paramBundle2) {
    boolean bool = true;
    if (paramBundle1 == null || paramBundle2 == null) {
      if (paramBundle1 != paramBundle2)
        bool = false; 
      return bool;
    } 
    if (paramBundle1.size() != paramBundle2.size())
      return false; 
    for (String str : paramBundle1.keySet()) {
      if (str != null) {
        Object object2 = paramBundle1.get(str);
        Object object1 = paramBundle2.get(str);
        if (!Objects.equals(object2, object1))
          return false; 
      } 
    } 
    return true;
  }
}
