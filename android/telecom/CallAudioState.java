package android.telecom;

import android.bluetooth.BluetoothDevice;
import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Locale;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class CallAudioState implements Parcelable {
  public CallAudioState(boolean paramBoolean, int paramInt1, int paramInt2) {
    this(paramBoolean, paramInt1, paramInt2, null, Collections.emptyList());
  }
  
  public CallAudioState(boolean paramBoolean, int paramInt1, int paramInt2, BluetoothDevice paramBluetoothDevice, Collection<BluetoothDevice> paramCollection) {
    this.isMuted = paramBoolean;
    this.route = paramInt1;
    this.supportedRouteMask = paramInt2;
    this.activeBluetoothDevice = paramBluetoothDevice;
    this.supportedBluetoothDevices = paramCollection;
  }
  
  public CallAudioState(CallAudioState paramCallAudioState) {
    this.isMuted = paramCallAudioState.isMuted();
    this.route = paramCallAudioState.getRoute();
    this.supportedRouteMask = paramCallAudioState.getSupportedRouteMask();
    this.activeBluetoothDevice = paramCallAudioState.activeBluetoothDevice;
    this.supportedBluetoothDevices = paramCallAudioState.getSupportedBluetoothDevices();
  }
  
  public CallAudioState(AudioState paramAudioState) {
    this.isMuted = paramAudioState.isMuted();
    this.route = paramAudioState.getRoute();
    this.supportedRouteMask = paramAudioState.getSupportedRouteMask();
    this.activeBluetoothDevice = null;
    this.supportedBluetoothDevices = Collections.emptyList();
  }
  
  public boolean equals(BluetoothDevice paramObject) {
    boolean bool = false;
    if (paramObject == null)
      return false; 
    if (!(paramObject instanceof CallAudioState))
      return false; 
    CallAudioState callAudioState = (CallAudioState)paramObject;
    if (this.supportedBluetoothDevices.size() != callAudioState.supportedBluetoothDevices.size())
      return false; 
    for (BluetoothDevice paramObject : this.supportedBluetoothDevices) {
      if (!callAudioState.supportedBluetoothDevices.contains(paramObject))
        return false; 
    } 
    if (Objects.equals(this.activeBluetoothDevice, callAudioState.activeBluetoothDevice)) {
      boolean bool1 = isMuted();
      if (bool1 == callAudioState.isMuted() && getRoute() == callAudioState.getRoute()) {
        int i = getSupportedRouteMask();
        if (i == callAudioState.getSupportedRouteMask())
          bool = true; 
      } 
    } 
    return bool;
  }
  
  public String toString() {
    Stream<BluetoothDevice> stream = this.supportedBluetoothDevices.stream();
    -$.Lambda.cyYWqCYT05eM23eLVm4oQ5DrYjw cyYWqCYT05eM23eLVm4oQ5DrYjw = _$$Lambda$cyYWqCYT05eM23eLVm4oQ5DrYjw.INSTANCE;
    String str1 = stream.<CharSequence>map((Function<? super BluetoothDevice, ? extends CharSequence>)cyYWqCYT05eM23eLVm4oQ5DrYjw).collect(Collectors.joining(", "));
    Locale locale = Locale.US;
    boolean bool = this.isMuted;
    int i = this.route;
    String str2 = audioRouteToString(i);
    i = this.supportedRouteMask;
    String str3 = audioRouteToString(i);
    BluetoothDevice bluetoothDevice = this.activeBluetoothDevice;
    return String.format(locale, "[AudioState isMuted: %b, route: %s, supportedRouteMask: %s, activeBluetoothDevice: [%s], supportedBluetoothDevices: [%s]]", new Object[] { Boolean.valueOf(bool), str2, str3, bluetoothDevice, str1 });
  }
  
  public boolean isMuted() {
    return this.isMuted;
  }
  
  public int getRoute() {
    return this.route;
  }
  
  public int getSupportedRouteMask() {
    return this.supportedRouteMask;
  }
  
  public BluetoothDevice getActiveBluetoothDevice() {
    return this.activeBluetoothDevice;
  }
  
  public Collection<BluetoothDevice> getSupportedBluetoothDevices() {
    return this.supportedBluetoothDevices;
  }
  
  public static String audioRouteToString(int paramInt) {
    if (paramInt == 0 || (paramInt & 0xFFFFFFF0) != 0)
      return "UNKNOWN"; 
    StringBuffer stringBuffer = new StringBuffer();
    if ((paramInt & 0x1) == 1)
      listAppend(stringBuffer, "EARPIECE"); 
    if ((paramInt & 0x2) == 2)
      listAppend(stringBuffer, "BLUETOOTH"); 
    if ((paramInt & 0x4) == 4)
      listAppend(stringBuffer, "WIRED_HEADSET"); 
    if ((paramInt & 0x8) == 8)
      listAppend(stringBuffer, "SPEAKER"); 
    return stringBuffer.toString();
  }
  
  public static final Parcelable.Creator<CallAudioState> CREATOR = new Parcelable.Creator<CallAudioState>() {
      public CallAudioState createFromParcel(Parcel param1Parcel) {
        boolean bool;
        if (param1Parcel.readByte() == 0) {
          bool = false;
        } else {
          bool = true;
        } 
        int i = param1Parcel.readInt();
        int j = param1Parcel.readInt();
        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        BluetoothDevice bluetoothDevice = (BluetoothDevice)param1Parcel.readParcelable(classLoader);
        ArrayList<BluetoothDevice> arrayList = new ArrayList();
        classLoader = ClassLoader.getSystemClassLoader();
        param1Parcel.readParcelableList(arrayList, classLoader);
        return new CallAudioState(bool, i, j, bluetoothDevice, arrayList);
      }
      
      public CallAudioState[] newArray(int param1Int) {
        return new CallAudioState[param1Int];
      }
    };
  
  public static final int ROUTE_ALL = 15;
  
  public static final int ROUTE_BLUETOOTH = 2;
  
  public static final int ROUTE_EARPIECE = 1;
  
  public static final int ROUTE_SPEAKER = 8;
  
  public static final int ROUTE_WIRED_HEADSET = 4;
  
  public static final int ROUTE_WIRED_OR_EARPIECE = 5;
  
  private final BluetoothDevice activeBluetoothDevice;
  
  private final boolean isMuted;
  
  private final int route;
  
  private final Collection<BluetoothDevice> supportedBluetoothDevices;
  
  private final int supportedRouteMask;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeByte((byte)this.isMuted);
    paramParcel.writeInt(this.route);
    paramParcel.writeInt(this.supportedRouteMask);
    paramParcel.writeParcelable((Parcelable)this.activeBluetoothDevice, 0);
    paramParcel.writeParcelableList(new ArrayList<>(this.supportedBluetoothDevices), 0);
  }
  
  private static void listAppend(StringBuffer paramStringBuffer, String paramString) {
    if (paramStringBuffer.length() > 0)
      paramStringBuffer.append(", "); 
    paramStringBuffer.append(paramString);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class CallAudioRoute implements Annotation {}
}
