package android.telecom;

import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class VideoProfile implements Parcelable {
  public VideoProfile(int paramInt) {
    this(paramInt, 4);
  }
  
  public VideoProfile(int paramInt1, int paramInt2) {
    this.mVideoState = paramInt1;
    this.mQuality = paramInt2;
  }
  
  public int getVideoState() {
    return this.mVideoState;
  }
  
  public int getQuality() {
    return this.mQuality;
  }
  
  public static final Parcelable.Creator<VideoProfile> CREATOR = new Parcelable.Creator<VideoProfile>() {
      public VideoProfile createFromParcel(Parcel param1Parcel) {
        int i = param1Parcel.readInt();
        int j = param1Parcel.readInt();
        VideoProfile.class.getClassLoader();
        return new VideoProfile(i, j);
      }
      
      public VideoProfile[] newArray(int param1Int) {
        return new VideoProfile[param1Int];
      }
    };
  
  public static final int QUALITY_DEFAULT = 4;
  
  public static final int QUALITY_HIGH = 1;
  
  public static final int QUALITY_LOW = 3;
  
  public static final int QUALITY_MEDIUM = 2;
  
  public static final int QUALITY_UNKNOWN = 0;
  
  public static final int STATE_AUDIO_ONLY = 0;
  
  public static final int STATE_BIDIRECTIONAL = 3;
  
  public static final int STATE_PAUSED = 4;
  
  public static final int STATE_RX_ENABLED = 2;
  
  public static final int STATE_TX_ENABLED = 1;
  
  private final int mQuality;
  
  private final int mVideoState;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mVideoState);
    paramParcel.writeInt(this.mQuality);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("[VideoProfile videoState = ");
    stringBuilder.append(videoStateToString(this.mVideoState));
    stringBuilder.append(" videoQuality = ");
    stringBuilder.append(this.mQuality);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public static String videoStateToString(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Audio");
    if (paramInt == 0) {
      stringBuilder.append(" Only");
    } else {
      if (isTransmissionEnabled(paramInt))
        stringBuilder.append(" Tx"); 
      if (isReceptionEnabled(paramInt))
        stringBuilder.append(" Rx"); 
      if (isPaused(paramInt))
        stringBuilder.append(" Pause"); 
    } 
    return stringBuilder.toString();
  }
  
  public static boolean isAudioOnly(int paramInt) {
    boolean bool = true;
    if (hasState(paramInt, 1) || 
      hasState(paramInt, 2))
      bool = false; 
    return bool;
  }
  
  public static boolean isVideo(int paramInt) {
    boolean bool = true;
    if (!hasState(paramInt, 1) && 
      !hasState(paramInt, 2) && 
      !hasState(paramInt, 3))
      bool = false; 
    return bool;
  }
  
  public static boolean isTransmissionEnabled(int paramInt) {
    return hasState(paramInt, 1);
  }
  
  public static boolean isReceptionEnabled(int paramInt) {
    return hasState(paramInt, 2);
  }
  
  public static boolean isBidirectional(int paramInt) {
    return hasState(paramInt, 3);
  }
  
  public static boolean isPaused(int paramInt) {
    return hasState(paramInt, 4);
  }
  
  private static boolean hasState(int paramInt1, int paramInt2) {
    boolean bool;
    if ((paramInt1 & paramInt2) == paramInt2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static final class CameraCapabilities implements Parcelable {
    public CameraCapabilities(int param1Int1, int param1Int2) {
      this(param1Int1, param1Int2, false, 1.0F);
    }
    
    public CameraCapabilities(int param1Int1, int param1Int2, boolean param1Boolean, float param1Float) {
      this.mWidth = param1Int1;
      this.mHeight = param1Int2;
      this.mZoomSupported = param1Boolean;
      this.mMaxZoom = param1Float;
    }
    
    public static final Parcelable.Creator<CameraCapabilities> CREATOR = new Parcelable.Creator<CameraCapabilities>() {
        public VideoProfile.CameraCapabilities createFromParcel(Parcel param2Parcel) {
          boolean bool;
          int i = param2Parcel.readInt();
          int j = param2Parcel.readInt();
          if (param2Parcel.readByte() != 0) {
            bool = true;
          } else {
            bool = false;
          } 
          float f = param2Parcel.readFloat();
          return new VideoProfile.CameraCapabilities(i, j, bool, f);
        }
        
        public VideoProfile.CameraCapabilities[] newArray(int param2Int) {
          return new VideoProfile.CameraCapabilities[param2Int];
        }
      };
    
    private final int mHeight;
    
    private final float mMaxZoom;
    
    private final int mWidth;
    
    private final boolean mZoomSupported;
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(getWidth());
      param1Parcel.writeInt(getHeight());
      param1Parcel.writeByte((byte)isZoomSupported());
      param1Parcel.writeFloat(getMaxZoom());
    }
    
    public int getWidth() {
      return this.mWidth;
    }
    
    public int getHeight() {
      return this.mHeight;
    }
    
    public boolean isZoomSupported() {
      return this.mZoomSupported;
    }
    
    public float getMaxZoom() {
      return this.mMaxZoom;
    }
  }
  
  class null implements Parcelable.Creator<CameraCapabilities> {
    public VideoProfile.CameraCapabilities createFromParcel(Parcel param1Parcel) {
      boolean bool;
      int i = param1Parcel.readInt();
      int j = param1Parcel.readInt();
      if (param1Parcel.readByte() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      float f = param1Parcel.readFloat();
      return new VideoProfile.CameraCapabilities(i, j, bool, f);
    }
    
    public VideoProfile.CameraCapabilities[] newArray(int param1Int) {
      return new VideoProfile.CameraCapabilities[param1Int];
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class VideoQuality implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class VideoState implements Annotation {}
}
