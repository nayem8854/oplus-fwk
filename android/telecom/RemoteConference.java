package android.telecom;

import android.annotation.SystemApi;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import com.android.internal.telecom.IConnectionService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;

public final class RemoteConference {
  public static abstract class Callback {
    public void onStateChanged(RemoteConference param1RemoteConference, int param1Int1, int param1Int2) {}
    
    public void onDisconnected(RemoteConference param1RemoteConference, DisconnectCause param1DisconnectCause) {}
    
    public void onConnectionAdded(RemoteConference param1RemoteConference, RemoteConnection param1RemoteConnection) {}
    
    public void onConnectionRemoved(RemoteConference param1RemoteConference, RemoteConnection param1RemoteConnection) {}
    
    public void onConnectionCapabilitiesChanged(RemoteConference param1RemoteConference, int param1Int) {}
    
    public void onConnectionPropertiesChanged(RemoteConference param1RemoteConference, int param1Int) {}
    
    public void onConferenceableConnectionsChanged(RemoteConference param1RemoteConference, List<RemoteConnection> param1List) {}
    
    public void onDestroyed(RemoteConference param1RemoteConference) {}
    
    public void onExtrasChanged(RemoteConference param1RemoteConference, Bundle param1Bundle) {}
  }
  
  private final Set<CallbackRecord<Callback>> mCallbackRecords = new CopyOnWriteArraySet<>();
  
  private final List<RemoteConnection> mChildConnections;
  
  private final List<RemoteConnection> mConferenceableConnections;
  
  private int mConnectionCapabilities;
  
  private int mConnectionProperties;
  
  private final IConnectionService mConnectionService;
  
  private DisconnectCause mDisconnectCause;
  
  private Bundle mExtras;
  
  private final String mId;
  
  private int mState;
  
  private final List<RemoteConnection> mUnmodifiableChildConnections;
  
  private final List<RemoteConnection> mUnmodifiableConferenceableConnections;
  
  RemoteConference(String paramString, IConnectionService paramIConnectionService) {
    CopyOnWriteArrayList<RemoteConnection> copyOnWriteArrayList = new CopyOnWriteArrayList();
    this.mUnmodifiableChildConnections = Collections.unmodifiableList(copyOnWriteArrayList);
    ArrayList<RemoteConnection> arrayList = new ArrayList();
    this.mUnmodifiableConferenceableConnections = Collections.unmodifiableList(arrayList);
    this.mState = 1;
    this.mId = paramString;
    this.mConnectionService = paramIConnectionService;
  }
  
  String getId() {
    return this.mId;
  }
  
  void setDestroyed() {
    for (RemoteConnection remoteConnection : this.mChildConnections)
      remoteConnection.setConference(null); 
    for (CallbackRecord<Callback> callbackRecord : this.mCallbackRecords) {
      final Callback callback = callbackRecord.getCallback();
      callbackRecord.getHandler().post(new Runnable() {
            final RemoteConference this$0;
            
            final RemoteConference.Callback val$callback;
            
            final RemoteConference val$conference;
            
            public void run() {
              callback.onDestroyed(conference);
            }
          });
    } 
  }
  
  void setState(final int newState) {
    if (newState != 4 && newState != 5 && newState != 6) {
      String str = Connection.stateToString(newState);
      Log.w(this, "Unsupported state transition for Conference call.", new Object[] { str });
      return;
    } 
    if (this.mState != newState) {
      final int oldState = this.mState;
      this.mState = newState;
      for (CallbackRecord<Callback> callbackRecord : this.mCallbackRecords) {
        final Callback callback = callbackRecord.getCallback();
        callbackRecord.getHandler().post(new Runnable() {
              final RemoteConference this$0;
              
              final RemoteConference.Callback val$callback;
              
              final RemoteConference val$conference;
              
              final int val$newState;
              
              final int val$oldState;
              
              public void run() {
                callback.onStateChanged(conference, oldState, newState);
              }
            });
      } 
    } 
  }
  
  void addConnection(final RemoteConnection connection) {
    if (!this.mChildConnections.contains(connection)) {
      this.mChildConnections.add(connection);
      connection.setConference(this);
      for (CallbackRecord<Callback> callbackRecord : this.mCallbackRecords) {
        final Callback callback = callbackRecord.getCallback();
        callbackRecord.getHandler().post(new Runnable() {
              final RemoteConference this$0;
              
              final RemoteConference.Callback val$callback;
              
              final RemoteConference val$conference;
              
              final RemoteConnection val$connection;
              
              public void run() {
                callback.onConnectionAdded(conference, connection);
              }
            });
      } 
    } 
  }
  
  void removeConnection(final RemoteConnection connection) {
    if (this.mChildConnections.contains(connection)) {
      this.mChildConnections.remove(connection);
      connection.setConference(null);
      for (CallbackRecord<Callback> callbackRecord : this.mCallbackRecords) {
        final Callback callback = callbackRecord.getCallback();
        callbackRecord.getHandler().post(new Runnable() {
              final RemoteConference this$0;
              
              final RemoteConference.Callback val$callback;
              
              final RemoteConference val$conference;
              
              final RemoteConnection val$connection;
              
              public void run() {
                callback.onConnectionRemoved(conference, connection);
              }
            });
      } 
    } 
  }
  
  void setConnectionCapabilities(int paramInt) {
    if (this.mConnectionCapabilities != paramInt) {
      this.mConnectionCapabilities = paramInt;
      for (CallbackRecord<Callback> callbackRecord : this.mCallbackRecords) {
        final Callback callback = callbackRecord.getCallback();
        callbackRecord.getHandler().post(new Runnable() {
              final RemoteConference this$0;
              
              final RemoteConference.Callback val$callback;
              
              final RemoteConference val$conference;
              
              public void run() {
                RemoteConference.Callback callback = callback;
                RemoteConference remoteConference1 = conference, remoteConference2 = RemoteConference.this;
                int i = remoteConference2.mConnectionCapabilities;
                callback.onConnectionCapabilitiesChanged(remoteConference1, i);
              }
            });
      } 
    } 
  }
  
  void setConnectionProperties(int paramInt) {
    if (this.mConnectionProperties != paramInt) {
      this.mConnectionProperties = paramInt;
      for (CallbackRecord<Callback> callbackRecord : this.mCallbackRecords) {
        final Callback callback = callbackRecord.getCallback();
        callbackRecord.getHandler().post(new Runnable() {
              final RemoteConference this$0;
              
              final RemoteConference.Callback val$callback;
              
              final RemoteConference val$conference;
              
              public void run() {
                RemoteConference.Callback callback = callback;
                RemoteConference remoteConference1 = conference, remoteConference2 = RemoteConference.this;
                int i = remoteConference2.mConnectionProperties;
                callback.onConnectionPropertiesChanged(remoteConference1, i);
              }
            });
      } 
    } 
  }
  
  void setConferenceableConnections(List<RemoteConnection> paramList) {
    this.mConferenceableConnections.clear();
    this.mConferenceableConnections.addAll(paramList);
    for (CallbackRecord<Callback> callbackRecord : this.mCallbackRecords) {
      final Callback callback = callbackRecord.getCallback();
      callbackRecord.getHandler().post(new Runnable() {
            final RemoteConference this$0;
            
            final RemoteConference.Callback val$callback;
            
            final RemoteConference val$conference;
            
            public void run() {
              RemoteConference.Callback callback = callback;
              RemoteConference remoteConference1 = conference, remoteConference2 = RemoteConference.this;
              List<RemoteConnection> list = remoteConference2.mUnmodifiableConferenceableConnections;
              callback.onConferenceableConnectionsChanged(remoteConference1, list);
            }
          });
    } 
  }
  
  void setDisconnected(final DisconnectCause disconnectCause) {
    if (this.mState != 6) {
      this.mDisconnectCause = disconnectCause;
      setState(6);
      for (CallbackRecord<Callback> callbackRecord : this.mCallbackRecords) {
        final Callback callback = callbackRecord.getCallback();
        callbackRecord.getHandler().post(new Runnable() {
              final RemoteConference this$0;
              
              final RemoteConference.Callback val$callback;
              
              final RemoteConference val$conference;
              
              final DisconnectCause val$disconnectCause;
              
              public void run() {
                callback.onDisconnected(conference, disconnectCause);
              }
            });
      } 
    } 
  }
  
  void putExtras(Bundle paramBundle) {
    if (paramBundle == null)
      return; 
    if (this.mExtras == null)
      this.mExtras = new Bundle(); 
    this.mExtras.putAll(paramBundle);
    notifyExtrasChanged();
  }
  
  void removeExtras(List<String> paramList) {
    if (this.mExtras == null || paramList == null || paramList.isEmpty())
      return; 
    for (String str : paramList)
      this.mExtras.remove(str); 
    notifyExtrasChanged();
  }
  
  private void notifyExtrasChanged() {
    for (CallbackRecord<Callback> callbackRecord : this.mCallbackRecords) {
      final Callback callback = callbackRecord.getCallback();
      callbackRecord.getHandler().post(new Runnable() {
            final RemoteConference this$0;
            
            final RemoteConference.Callback val$callback;
            
            final RemoteConference val$conference;
            
            public void run() {
              callback.onExtrasChanged(conference, RemoteConference.this.mExtras);
            }
          });
    } 
  }
  
  public final List<RemoteConnection> getConnections() {
    return this.mUnmodifiableChildConnections;
  }
  
  public final int getState() {
    return this.mState;
  }
  
  public final int getConnectionCapabilities() {
    return this.mConnectionCapabilities;
  }
  
  public final int getConnectionProperties() {
    return this.mConnectionProperties;
  }
  
  public final Bundle getExtras() {
    return this.mExtras;
  }
  
  public void disconnect() {
    try {
      this.mConnectionService.disconnect(this.mId, null);
    } catch (RemoteException remoteException) {}
  }
  
  public void separate(RemoteConnection paramRemoteConnection) {
    if (this.mChildConnections.contains(paramRemoteConnection))
      try {
        this.mConnectionService.splitFromConference(paramRemoteConnection.getId(), null);
      } catch (RemoteException remoteException) {} 
  }
  
  public void merge() {
    try {
      this.mConnectionService.mergeConference(this.mId, null);
    } catch (RemoteException remoteException) {}
  }
  
  public void swap() {
    try {
      this.mConnectionService.swapConference(this.mId, null);
    } catch (RemoteException remoteException) {}
  }
  
  public void hold() {
    try {
      this.mConnectionService.hold(this.mId, null);
    } catch (RemoteException remoteException) {}
  }
  
  public void unhold() {
    try {
      this.mConnectionService.unhold(this.mId, null);
    } catch (RemoteException remoteException) {}
  }
  
  public DisconnectCause getDisconnectCause() {
    return this.mDisconnectCause;
  }
  
  public void playDtmfTone(char paramChar) {
    try {
      this.mConnectionService.playDtmfTone(this.mId, paramChar, null);
    } catch (RemoteException remoteException) {}
  }
  
  public void stopDtmfTone() {
    try {
      this.mConnectionService.stopDtmfTone(this.mId, null);
    } catch (RemoteException remoteException) {}
  }
  
  @SystemApi
  @Deprecated
  public void setAudioState(AudioState paramAudioState) {
    setCallAudioState(new CallAudioState(paramAudioState));
  }
  
  public void setCallAudioState(CallAudioState paramCallAudioState) {
    try {
      this.mConnectionService.onCallAudioStateChanged(this.mId, paramCallAudioState, null);
    } catch (RemoteException remoteException) {}
  }
  
  public List<RemoteConnection> getConferenceableConnections() {
    return this.mUnmodifiableConferenceableConnections;
  }
  
  public final void registerCallback(Callback paramCallback) {
    registerCallback(paramCallback, new Handler());
  }
  
  public final void registerCallback(Callback paramCallback, Handler paramHandler) {
    unregisterCallback(paramCallback);
    if (paramCallback != null && paramHandler != null)
      this.mCallbackRecords.add(new CallbackRecord<>(paramCallback, paramHandler)); 
  }
  
  public final void unregisterCallback(Callback paramCallback) {
    if (paramCallback != null)
      for (CallbackRecord<Callback> callbackRecord : this.mCallbackRecords) {
        if (callbackRecord.getCallback() == paramCallback) {
          this.mCallbackRecords.remove(callbackRecord);
          break;
        } 
      }  
  }
}
