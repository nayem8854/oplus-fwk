package android.telecom;

import android.content.ComponentName;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Process;
import android.os.UserHandle;
import android.util.Log;
import java.util.Objects;

public final class PhoneAccountHandle extends OplusBasePhoneAccountHandle implements Parcelable {
  public PhoneAccountHandle(ComponentName paramComponentName, String paramString, int paramInt1, int paramInt2) {
    this(paramComponentName, paramString, Process.myUserHandle());
    initSubAndSlotId(paramInt1, paramInt2);
  }
  
  public PhoneAccountHandle(ComponentName paramComponentName, String paramString) {
    this(paramComponentName, paramString, Process.myUserHandle());
  }
  
  public PhoneAccountHandle(ComponentName paramComponentName, String paramString, UserHandle paramUserHandle) {
    checkParameters(paramComponentName, paramUserHandle);
    this.mComponentName = paramComponentName;
    this.mId = paramString;
    this.mUserHandle = paramUserHandle;
  }
  
  public ComponentName getComponentName() {
    return this.mComponentName;
  }
  
  public String getId() {
    return this.mId;
  }
  
  public UserHandle getUserHandle() {
    return this.mUserHandle;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { this.mComponentName, this.mId, this.mUserHandle });
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(this.mComponentName);
    stringBuilder.append(", ");
    String str = this.mId;
    stringBuilder.append(Log.pii(str));
    stringBuilder.append(", ");
    UserHandle userHandle = this.mUserHandle;
    stringBuilder.append(userHandle);
    stringBuilder.append(", ");
    int i = this.mSubId;
    stringBuilder.append(i);
    stringBuilder.append(",SlotId = ");
    i = this.mSlotId;
    stringBuilder.append(i);
    return stringBuilder.toString();
  }
  
  public boolean equals(Object paramObject) {
    if (paramObject != null && paramObject instanceof PhoneAccountHandle) {
      PhoneAccountHandle phoneAccountHandle = (PhoneAccountHandle)paramObject;
      ComponentName componentName1 = phoneAccountHandle.getComponentName();
      ComponentName componentName2 = getComponentName();
      if (Objects.equals(componentName1, componentName2)) {
        PhoneAccountHandle phoneAccountHandle1 = (PhoneAccountHandle)paramObject;
        if (Objects.equals(phoneAccountHandle1.getId(), getId())) {
          paramObject = paramObject;
          if (Objects.equals(paramObject.getUserHandle(), getUserHandle()))
            return true; 
        } 
      } 
    } 
    return false;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    this.mComponentName.writeToParcel(paramParcel, paramInt);
    paramParcel.writeString(this.mId);
    this.mUserHandle.writeToParcel(paramParcel, paramInt);
    super.writeToParcel(paramParcel, paramInt);
  }
  
  private void checkParameters(ComponentName paramComponentName, UserHandle paramUserHandle) {
    if (paramComponentName == null)
      Log.w("PhoneAccountHandle", new Exception("PhoneAccountHandle has been created with null ComponentName!")); 
    if (paramUserHandle == null)
      Log.w("PhoneAccountHandle", new Exception("PhoneAccountHandle has been created with null UserHandle!")); 
  }
  
  public static final Parcelable.Creator<PhoneAccountHandle> CREATOR = new Parcelable.Creator<PhoneAccountHandle>() {
      public PhoneAccountHandle createFromParcel(Parcel param1Parcel) {
        return new PhoneAccountHandle(param1Parcel);
      }
      
      public PhoneAccountHandle[] newArray(int param1Int) {
        return new PhoneAccountHandle[param1Int];
      }
    };
  
  private final ComponentName mComponentName;
  
  private final String mId;
  
  private final UserHandle mUserHandle;
  
  private PhoneAccountHandle(Parcel paramParcel) {
    this(componentName, str, userHandle);
    readFromParcel(paramParcel);
  }
  
  public static boolean areFromSamePackage(PhoneAccountHandle paramPhoneAccountHandle1, PhoneAccountHandle paramPhoneAccountHandle2) {
    String str = null;
    if (paramPhoneAccountHandle1 != null) {
      String str1 = paramPhoneAccountHandle1.getComponentName().getPackageName();
    } else {
      paramPhoneAccountHandle1 = null;
    } 
    if (paramPhoneAccountHandle2 != null)
      str = paramPhoneAccountHandle2.getComponentName().getPackageName(); 
    return Objects.equals(paramPhoneAccountHandle1, str);
  }
}
