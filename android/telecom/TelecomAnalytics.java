package android.telecom;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;

@SystemApi
public final class TelecomAnalytics implements Parcelable {
  public static final Parcelable.Creator<TelecomAnalytics> CREATOR = (Parcelable.Creator<TelecomAnalytics>)new Object();
  
  private List<ParcelableCallAnalytics> mCallAnalytics;
  
  private List<SessionTiming> mSessionTimings;
  
  public static final class SessionTiming extends TimedEvent<Integer> implements Parcelable {
    public static final Parcelable.Creator<SessionTiming> CREATOR = new Parcelable.Creator<SessionTiming>() {
        public TelecomAnalytics.SessionTiming createFromParcel(Parcel param2Parcel) {
          return new TelecomAnalytics.SessionTiming(param2Parcel);
        }
        
        public TelecomAnalytics.SessionTiming[] newArray(int param2Int) {
          return new TelecomAnalytics.SessionTiming[param2Int];
        }
      };
    
    public static final int CSW_ADD_CONFERENCE_CALL = 108;
    
    public static final int CSW_HANDLE_CREATE_CONNECTION_COMPLETE = 100;
    
    public static final int CSW_REMOVE_CALL = 106;
    
    public static final int CSW_SET_ACTIVE = 101;
    
    public static final int CSW_SET_DIALING = 103;
    
    public static final int CSW_SET_DISCONNECTED = 104;
    
    public static final int CSW_SET_IS_CONFERENCED = 107;
    
    public static final int CSW_SET_ON_HOLD = 105;
    
    public static final int CSW_SET_RINGING = 102;
    
    public static final int ICA_ANSWER_CALL = 1;
    
    public static final int ICA_CONFERENCE = 8;
    
    public static final int ICA_DISCONNECT_CALL = 3;
    
    public static final int ICA_HOLD_CALL = 4;
    
    public static final int ICA_MUTE = 6;
    
    public static final int ICA_REJECT_CALL = 2;
    
    public static final int ICA_SET_AUDIO_ROUTE = 7;
    
    public static final int ICA_UNHOLD_CALL = 5;
    
    private int mId;
    
    private long mTime;
    
    public SessionTiming(int param1Int, long param1Long) {
      this.mId = param1Int;
      this.mTime = param1Long;
    }
    
    private SessionTiming(Parcel param1Parcel) {
      this.mId = param1Parcel.readInt();
      this.mTime = param1Parcel.readLong();
    }
    
    public Integer getKey() {
      return Integer.valueOf(this.mId);
    }
    
    public long getTime() {
      return this.mTime;
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.mId);
      param1Parcel.writeLong(this.mTime);
    }
  }
  
  class null implements Parcelable.Creator<SessionTiming> {
    public TelecomAnalytics.SessionTiming createFromParcel(Parcel param1Parcel) {
      return new TelecomAnalytics.SessionTiming(param1Parcel);
    }
    
    public TelecomAnalytics.SessionTiming[] newArray(int param1Int) {
      return new TelecomAnalytics.SessionTiming[param1Int];
    }
  }
  
  public TelecomAnalytics(List<SessionTiming> paramList, List<ParcelableCallAnalytics> paramList1) {
    this.mSessionTimings = paramList;
    this.mCallAnalytics = paramList1;
  }
  
  private TelecomAnalytics(Parcel paramParcel) {
    ArrayList<SessionTiming> arrayList = new ArrayList();
    paramParcel.readTypedList(arrayList, SessionTiming.CREATOR);
    this.mCallAnalytics = (List)(arrayList = new ArrayList<>());
    paramParcel.readTypedList(arrayList, ParcelableCallAnalytics.CREATOR);
  }
  
  public List<SessionTiming> getSessionTimings() {
    return this.mSessionTimings;
  }
  
  public List<ParcelableCallAnalytics> getCallAnalytics() {
    return this.mCallAnalytics;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeTypedList(this.mSessionTimings);
    paramParcel.writeTypedList(this.mCallAnalytics);
  }
}
