package android.telecom;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

public class Voicemail implements Parcelable {
  private Voicemail(Long paramLong1, String paramString1, PhoneAccountHandle paramPhoneAccountHandle, Long paramLong2, Long paramLong3, String paramString2, String paramString3, Uri paramUri, Boolean paramBoolean1, Boolean paramBoolean2, String paramString4) {
    this.mTimestamp = paramLong1;
    this.mNumber = paramString1;
    this.mPhoneAccount = paramPhoneAccountHandle;
    this.mId = paramLong2;
    this.mDuration = paramLong3;
    this.mSource = paramString2;
    this.mProviderData = paramString3;
    this.mUri = paramUri;
    this.mIsRead = paramBoolean1;
    this.mHasContent = paramBoolean2;
    this.mTranscription = paramString4;
  }
  
  public static Builder createForInsertion(long paramLong, String paramString) {
    return (new Builder()).setNumber(paramString).setTimestamp(paramLong);
  }
  
  public static Builder createForUpdate(long paramLong, String paramString) {
    return (new Builder()).setId(paramLong).setSourceData(paramString);
  }
  
  class Builder {
    private Long mBuilderDuration;
    
    private boolean mBuilderHasContent;
    
    private Long mBuilderId;
    
    private Boolean mBuilderIsRead;
    
    private String mBuilderNumber;
    
    private PhoneAccountHandle mBuilderPhoneAccount;
    
    private String mBuilderSourceData;
    
    private String mBuilderSourcePackage;
    
    private Long mBuilderTimestamp;
    
    private String mBuilderTranscription;
    
    private Uri mBuilderUri;
    
    private Builder() {}
    
    public Builder setNumber(String param1String) {
      this.mBuilderNumber = param1String;
      return this;
    }
    
    public Builder setTimestamp(long param1Long) {
      this.mBuilderTimestamp = Long.valueOf(param1Long);
      return this;
    }
    
    public Builder setPhoneAccount(PhoneAccountHandle param1PhoneAccountHandle) {
      this.mBuilderPhoneAccount = param1PhoneAccountHandle;
      return this;
    }
    
    public Builder setId(long param1Long) {
      this.mBuilderId = Long.valueOf(param1Long);
      return this;
    }
    
    public Builder setDuration(long param1Long) {
      this.mBuilderDuration = Long.valueOf(param1Long);
      return this;
    }
    
    public Builder setSourcePackage(String param1String) {
      this.mBuilderSourcePackage = param1String;
      return this;
    }
    
    public Builder setSourceData(String param1String) {
      this.mBuilderSourceData = param1String;
      return this;
    }
    
    public Builder setUri(Uri param1Uri) {
      this.mBuilderUri = param1Uri;
      return this;
    }
    
    public Builder setIsRead(boolean param1Boolean) {
      this.mBuilderIsRead = Boolean.valueOf(param1Boolean);
      return this;
    }
    
    public Builder setHasContent(boolean param1Boolean) {
      this.mBuilderHasContent = param1Boolean;
      return this;
    }
    
    public Builder setTranscription(String param1String) {
      this.mBuilderTranscription = param1String;
      return this;
    }
    
    public Voicemail build() {
      long l1;
      Long long_1 = this.mBuilderId;
      if (long_1 == null) {
        l1 = -1L;
      } else {
        l1 = long_1.longValue();
      } 
      this.mBuilderId = Long.valueOf(l1);
      long_1 = this.mBuilderTimestamp;
      long l2 = 0L;
      if (long_1 == null) {
        l1 = 0L;
      } else {
        l1 = long_1.longValue();
      } 
      this.mBuilderTimestamp = Long.valueOf(l1);
      long_1 = this.mBuilderDuration;
      if (long_1 == null) {
        l1 = l2;
      } else {
        l1 = long_1.longValue();
      } 
      this.mBuilderDuration = Long.valueOf(l1);
      Boolean bool1 = this.mBuilderIsRead;
      if (bool1 == null) {
        bool = false;
      } else {
        bool = bool1.booleanValue();
      } 
      this.mBuilderIsRead = Boolean.valueOf(bool);
      Long long_2 = this.mBuilderTimestamp;
      String str2 = this.mBuilderNumber;
      PhoneAccountHandle phoneAccountHandle = this.mBuilderPhoneAccount;
      Long long_3 = this.mBuilderId, long_4 = this.mBuilderDuration;
      String str1 = this.mBuilderSourcePackage, str3 = this.mBuilderSourceData;
      Uri uri = this.mBuilderUri;
      Boolean bool2 = this.mBuilderIsRead;
      boolean bool = this.mBuilderHasContent;
      return 
        
        new Voicemail(long_2, str2, phoneAccountHandle, long_3, long_4, str1, str3, uri, bool2, Boolean.valueOf(bool), this.mBuilderTranscription);
    }
  }
  
  public long getId() {
    return this.mId.longValue();
  }
  
  public String getNumber() {
    return this.mNumber;
  }
  
  public PhoneAccountHandle getPhoneAccount() {
    return this.mPhoneAccount;
  }
  
  public long getTimestampMillis() {
    return this.mTimestamp.longValue();
  }
  
  public long getDuration() {
    return this.mDuration.longValue();
  }
  
  public String getSourcePackage() {
    return this.mSource;
  }
  
  public String getSourceData() {
    return this.mProviderData;
  }
  
  public Uri getUri() {
    return this.mUri;
  }
  
  public boolean isRead() {
    return this.mIsRead.booleanValue();
  }
  
  public boolean hasContent() {
    return this.mHasContent.booleanValue();
  }
  
  public String getTranscription() {
    return this.mTranscription;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeLong(this.mTimestamp.longValue());
    paramParcel.writeCharSequence(this.mNumber);
    if (this.mPhoneAccount == null) {
      paramParcel.writeInt(0);
    } else {
      paramParcel.writeInt(1);
      this.mPhoneAccount.writeToParcel(paramParcel, paramInt);
    } 
    paramParcel.writeLong(this.mId.longValue());
    paramParcel.writeLong(this.mDuration.longValue());
    paramParcel.writeCharSequence(this.mSource);
    paramParcel.writeCharSequence(this.mProviderData);
    if (this.mUri == null) {
      paramParcel.writeInt(0);
    } else {
      paramParcel.writeInt(1);
      this.mUri.writeToParcel(paramParcel, paramInt);
    } 
    if (this.mIsRead.booleanValue()) {
      paramParcel.writeInt(1);
    } else {
      paramParcel.writeInt(0);
    } 
    if (this.mHasContent.booleanValue()) {
      paramParcel.writeInt(1);
    } else {
      paramParcel.writeInt(0);
    } 
    paramParcel.writeCharSequence(this.mTranscription);
  }
  
  public static final Parcelable.Creator<Voicemail> CREATOR = new Parcelable.Creator<Voicemail>() {
      public Voicemail createFromParcel(Parcel param1Parcel) {
        return new Voicemail(param1Parcel);
      }
      
      public Voicemail[] newArray(int param1Int) {
        return new Voicemail[param1Int];
      }
    };
  
  private final Long mDuration;
  
  private final Boolean mHasContent;
  
  private final Long mId;
  
  private final Boolean mIsRead;
  
  private final String mNumber;
  
  private final PhoneAccountHandle mPhoneAccount;
  
  private final String mProviderData;
  
  private final String mSource;
  
  private final Long mTimestamp;
  
  private final String mTranscription;
  
  private final Uri mUri;
  
  private Voicemail(Parcel paramParcel) {
    boolean bool2;
    this.mTimestamp = Long.valueOf(paramParcel.readLong());
    this.mNumber = (String)paramParcel.readCharSequence();
    if (paramParcel.readInt() > 0) {
      this.mPhoneAccount = (PhoneAccountHandle)PhoneAccountHandle.CREATOR.createFromParcel(paramParcel);
    } else {
      this.mPhoneAccount = null;
    } 
    this.mId = Long.valueOf(paramParcel.readLong());
    this.mDuration = Long.valueOf(paramParcel.readLong());
    this.mSource = (String)paramParcel.readCharSequence();
    this.mProviderData = (String)paramParcel.readCharSequence();
    if (paramParcel.readInt() > 0) {
      this.mUri = (Uri)Uri.CREATOR.createFromParcel(paramParcel);
    } else {
      this.mUri = null;
    } 
    int i = paramParcel.readInt();
    boolean bool1 = true;
    if (i > 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mIsRead = Boolean.valueOf(bool2);
    if (paramParcel.readInt() > 0) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    this.mHasContent = Boolean.valueOf(bool2);
    this.mTranscription = (String)paramParcel.readCharSequence();
  }
}
