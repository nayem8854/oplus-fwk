package android.telecom;

import android.annotation.SystemApi;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;

public final class ConnectionRequest implements Parcelable {
  class Builder {
    private int mVideoState = 0;
    
    private String mTelecomCallId;
    
    private boolean mShouldShowIncomingCallUi = false;
    
    private ParcelFileDescriptor mRttPipeToInCall;
    
    private ParcelFileDescriptor mRttPipeFromInCall;
    
    private List<Uri> mParticipants;
    
    private boolean mIsAdhocConference = false;
    
    private Bundle mExtras;
    
    private Uri mAddress;
    
    private PhoneAccountHandle mAccountHandle;
    
    public Builder setAccountHandle(PhoneAccountHandle param1PhoneAccountHandle) {
      this.mAccountHandle = param1PhoneAccountHandle;
      return this;
    }
    
    public Builder setParticipants(List<Uri> param1List) {
      this.mParticipants = param1List;
      return this;
    }
    
    public Builder setAddress(Uri param1Uri) {
      this.mAddress = param1Uri;
      return this;
    }
    
    public Builder setExtras(Bundle param1Bundle) {
      this.mExtras = param1Bundle;
      return this;
    }
    
    public Builder setVideoState(int param1Int) {
      this.mVideoState = param1Int;
      return this;
    }
    
    public Builder setTelecomCallId(String param1String) {
      this.mTelecomCallId = param1String;
      return this;
    }
    
    public Builder setShouldShowIncomingCallUi(boolean param1Boolean) {
      this.mShouldShowIncomingCallUi = param1Boolean;
      return this;
    }
    
    public Builder setIsAdhocConferenceCall(boolean param1Boolean) {
      this.mIsAdhocConference = param1Boolean;
      return this;
    }
    
    public Builder setRttPipeFromInCall(ParcelFileDescriptor param1ParcelFileDescriptor) {
      this.mRttPipeFromInCall = param1ParcelFileDescriptor;
      return this;
    }
    
    public Builder setRttPipeToInCall(ParcelFileDescriptor param1ParcelFileDescriptor) {
      this.mRttPipeToInCall = param1ParcelFileDescriptor;
      return this;
    }
    
    public ConnectionRequest build() {
      return new ConnectionRequest(this.mAccountHandle, this.mAddress, this.mExtras, this.mVideoState, this.mTelecomCallId, this.mShouldShowIncomingCallUi, this.mRttPipeFromInCall, this.mRttPipeToInCall, this.mParticipants, this.mIsAdhocConference);
    }
  }
  
  public ConnectionRequest(PhoneAccountHandle paramPhoneAccountHandle, Uri paramUri, Bundle paramBundle) {
    this(paramPhoneAccountHandle, paramUri, paramBundle, 0, null, false, null, null);
  }
  
  public ConnectionRequest(PhoneAccountHandle paramPhoneAccountHandle, Uri paramUri, Bundle paramBundle, int paramInt) {
    this(paramPhoneAccountHandle, paramUri, paramBundle, paramInt, null, false, null, null);
  }
  
  public ConnectionRequest(PhoneAccountHandle paramPhoneAccountHandle, Uri paramUri, Bundle paramBundle, int paramInt, String paramString, boolean paramBoolean) {
    this(paramPhoneAccountHandle, paramUri, paramBundle, paramInt, paramString, paramBoolean, null, null);
  }
  
  private ConnectionRequest(PhoneAccountHandle paramPhoneAccountHandle, Uri paramUri, Bundle paramBundle, int paramInt, String paramString, boolean paramBoolean, ParcelFileDescriptor paramParcelFileDescriptor1, ParcelFileDescriptor paramParcelFileDescriptor2) {
    this(paramPhoneAccountHandle, paramUri, paramBundle, paramInt, paramString, paramBoolean, paramParcelFileDescriptor1, paramParcelFileDescriptor2, null, false);
  }
  
  private ConnectionRequest(PhoneAccountHandle paramPhoneAccountHandle, Uri paramUri, Bundle paramBundle, int paramInt, String paramString, boolean paramBoolean1, ParcelFileDescriptor paramParcelFileDescriptor1, ParcelFileDescriptor paramParcelFileDescriptor2, List<Uri> paramList, boolean paramBoolean2) {
    this.mAccountHandle = paramPhoneAccountHandle;
    this.mAddress = paramUri;
    this.mExtras = paramBundle;
    this.mVideoState = paramInt;
    this.mTelecomCallId = paramString;
    this.mShouldShowIncomingCallUi = paramBoolean1;
    this.mRttPipeFromInCall = paramParcelFileDescriptor1;
    this.mRttPipeToInCall = paramParcelFileDescriptor2;
    this.mParticipants = paramList;
    this.mIsAdhocConference = paramBoolean2;
  }
  
  private ConnectionRequest(Parcel paramParcel) {
    this.mAccountHandle = (PhoneAccountHandle)paramParcel.readParcelable(getClass().getClassLoader());
    this.mAddress = (Uri)paramParcel.readParcelable(getClass().getClassLoader());
    this.mExtras = (Bundle)paramParcel.readParcelable(getClass().getClassLoader());
    this.mVideoState = paramParcel.readInt();
    this.mTelecomCallId = paramParcel.readString();
    int i = paramParcel.readInt();
    boolean bool1 = false;
    if (i == 1) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mShouldShowIncomingCallUi = bool2;
    this.mRttPipeFromInCall = (ParcelFileDescriptor)paramParcel.readParcelable(getClass().getClassLoader());
    this.mRttPipeToInCall = (ParcelFileDescriptor)paramParcel.readParcelable(getClass().getClassLoader());
    ArrayList<Uri> arrayList = new ArrayList();
    paramParcel.readList(arrayList, getClass().getClassLoader());
    boolean bool2 = bool1;
    if (paramParcel.readInt() == 1)
      bool2 = true; 
    this.mIsAdhocConference = bool2;
  }
  
  public PhoneAccountHandle getAccountHandle() {
    return this.mAccountHandle;
  }
  
  public Uri getAddress() {
    return this.mAddress;
  }
  
  public List<Uri> getParticipants() {
    return this.mParticipants;
  }
  
  public Bundle getExtras() {
    return this.mExtras;
  }
  
  public int getVideoState() {
    return this.mVideoState;
  }
  
  @SystemApi
  public String getTelecomCallId() {
    return this.mTelecomCallId;
  }
  
  public boolean shouldShowIncomingCallUi() {
    return this.mShouldShowIncomingCallUi;
  }
  
  public boolean isAdhocConferenceCall() {
    return this.mIsAdhocConference;
  }
  
  public ParcelFileDescriptor getRttPipeToInCall() {
    return this.mRttPipeToInCall;
  }
  
  public ParcelFileDescriptor getRttPipeFromInCall() {
    return this.mRttPipeFromInCall;
  }
  
  public Connection.RttTextStream getRttTextStream() {
    if (isRequestingRtt()) {
      if (this.mRttTextStream == null)
        this.mRttTextStream = new Connection.RttTextStream(this.mRttPipeToInCall, this.mRttPipeFromInCall); 
      return this.mRttTextStream;
    } 
    return null;
  }
  
  public boolean isRequestingRtt() {
    boolean bool;
    if (this.mRttPipeFromInCall != null && this.mRttPipeToInCall != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public String toString() {
    String str1, str2;
    Uri uri = this.mAddress;
    if (uri == null) {
      uri = Uri.EMPTY;
    } else {
      str1 = Connection.toLogSafePhoneNumber(uri.toString());
    } 
    Bundle bundle = this.mExtras;
    String str3 = bundleToString(bundle);
    if (isAdhocConferenceCall()) {
      str2 = "Y";
    } else {
      str2 = "N";
    } 
    return String.format("ConnectionRequest %s %s isAdhocConf: %s", new Object[] { str1, str3, str2 });
  }
  
  private static String bundleToString(Bundle paramBundle) {
    if (paramBundle == null)
      return ""; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Bundle[");
    for (String str : paramBundle.keySet()) {
      stringBuilder.append(str);
      stringBuilder.append("=");
      byte b = -1;
      int i = str.hashCode();
      if (i != -1582002592) {
        if (i == -1513984200 && str.equals("android.telecom.extra.INCOMING_CALL_ADDRESS"))
          b = 0; 
      } else if (str.equals("android.telecom.extra.UNKNOWN_CALL_HANDLE")) {
        b = 1;
      } 
      if (b != 0 && b != 1) {
        stringBuilder.append(paramBundle.get(str));
      } else {
        stringBuilder.append(Log.pii(paramBundle.get(str)));
      } 
      stringBuilder.append(", ");
    } 
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public static final Parcelable.Creator<ConnectionRequest> CREATOR = new Parcelable.Creator<ConnectionRequest>() {
      public ConnectionRequest createFromParcel(Parcel param1Parcel) {
        return new ConnectionRequest(param1Parcel);
      }
      
      public ConnectionRequest[] newArray(int param1Int) {
        return new ConnectionRequest[param1Int];
      }
    };
  
  private final PhoneAccountHandle mAccountHandle;
  
  private final Uri mAddress;
  
  private final Bundle mExtras;
  
  private final boolean mIsAdhocConference;
  
  private List<Uri> mParticipants;
  
  private final ParcelFileDescriptor mRttPipeFromInCall;
  
  private final ParcelFileDescriptor mRttPipeToInCall;
  
  private Connection.RttTextStream mRttTextStream;
  
  private final boolean mShouldShowIncomingCallUi;
  
  private final String mTelecomCallId;
  
  private final int mVideoState;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable(this.mAccountHandle, 0);
    paramParcel.writeParcelable((Parcelable)this.mAddress, 0);
    paramParcel.writeParcelable((Parcelable)this.mExtras, 0);
    paramParcel.writeInt(this.mVideoState);
    paramParcel.writeString(this.mTelecomCallId);
    paramParcel.writeInt(this.mShouldShowIncomingCallUi);
    paramParcel.writeParcelable((Parcelable)this.mRttPipeFromInCall, 0);
    paramParcel.writeParcelable((Parcelable)this.mRttPipeToInCall, 0);
    paramParcel.writeList(this.mParticipants);
    paramParcel.writeInt(this.mIsAdhocConference);
  }
}
