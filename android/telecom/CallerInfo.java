package android.telecom;

import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.location.Country;
import android.location.CountryDetector;
import android.net.Uri;
import android.os.SystemProperties;
import android.provider.ContactsContract;
import android.telephony.PhoneNumberUtils;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.android.i18n.phonenumbers.NumberParseException;
import com.android.i18n.phonenumbers.PhoneNumberUtil;
import com.android.i18n.phonenumbers.Phonenumber;
import com.android.i18n.phonenumbers.geocoding.PhoneNumberOfflineGeocoder;
import java.util.Locale;

public class CallerInfo {
  public static final String PAYPHONE_NUMBER = "-3";
  
  public static final String PRIVATE_NUMBER = "-2";
  
  private static final String TAG = "CallerInfo";
  
  public static final String UNKNOWN_NUMBER = "-1";
  
  public static final long USER_TYPE_CURRENT = 0L;
  
  public static final long USER_TYPE_WORK = 1L;
  
  private static final boolean VDBG = Log.VERBOSE;
  
  public Drawable cachedPhoto;
  
  public Bitmap cachedPhotoIcon;
  
  public String cnapName;
  
  private Uri contactDisplayPhotoUri;
  
  public boolean contactExists;
  
  private long contactIdOrZero;
  
  public Uri contactRefUri;
  
  public Uri contactRingtoneUri;
  
  public String geoDescription;
  
  public boolean isCachedPhotoCurrent;
  
  public String lookupKey;
  
  private boolean mIsEmergency = false;
  
  private boolean mIsVoiceMail = false;
  
  private String name;
  
  public int namePresentation;
  
  public boolean needUpdate;
  
  public String normalizedNumber;
  
  public String numberLabel;
  
  public int numberPresentation;
  
  public int numberType;
  
  public String phoneLabel;
  
  private String phoneNumber;
  
  public int photoResource;
  
  public ComponentName preferredPhoneAccountComponent;
  
  public String preferredPhoneAccountId;
  
  public boolean shouldSendToVoicemail;
  
  public long userType = 0L;
  
  public static CallerInfo getCallerInfo(Context paramContext, Uri paramUri, Cursor paramCursor) {
    CallerInfo callerInfo = new CallerInfo();
    callerInfo.photoResource = 0;
    callerInfo.phoneLabel = null;
    callerInfo.numberType = 0;
    callerInfo.numberLabel = null;
    callerInfo.cachedPhoto = null;
    callerInfo.isCachedPhotoCurrent = false;
    callerInfo.contactExists = false;
    callerInfo.userType = 0L;
    if (VDBG)
      Log.v("CallerInfo", "getCallerInfo() based on cursor...", new Object[0]); 
    if (paramCursor != null) {
      if (paramCursor.moveToFirst()) {
        boolean bool;
        int i = paramCursor.getColumnIndex("display_name");
        if (i != -1)
          callerInfo.name = paramCursor.getString(i); 
        i = paramCursor.getColumnIndex("number");
        if (i != -1)
          callerInfo.phoneNumber = paramCursor.getString(i); 
        i = paramCursor.getColumnIndex("normalized_number");
        if (i != -1)
          callerInfo.normalizedNumber = paramCursor.getString(i); 
        i = paramCursor.getColumnIndex("label");
        if (i != -1) {
          int j = paramCursor.getColumnIndex("type");
          if (j != -1) {
            callerInfo.numberType = paramCursor.getInt(j);
            String str = paramCursor.getString(i);
            CharSequence charSequence = ContactsContract.CommonDataKinds.Phone.getDisplayLabel(paramContext, callerInfo.numberType, str);
            callerInfo.phoneLabel = charSequence.toString();
          } 
        } 
        i = getColumnIndexForPersonId(paramUri, paramCursor);
        if (i != -1) {
          long l = paramCursor.getLong(i);
          if (l != 0L && !ContactsContract.Contacts.isEnterpriseContactId(l)) {
            callerInfo.contactIdOrZero = l;
            if (VDBG) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("==> got info.contactIdOrZero: ");
              stringBuilder.append(callerInfo.contactIdOrZero);
              Log.v("CallerInfo", stringBuilder.toString(), new Object[0]);
            } 
          } 
          if (ContactsContract.Contacts.isEnterpriseContactId(l))
            callerInfo.userType = 1L; 
        } else if (VDBG) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Couldn't find contact_id column for ");
          stringBuilder.append(paramUri);
          Log.w("CallerInfo", stringBuilder.toString(), new Object[0]);
        } 
        i = paramCursor.getColumnIndex("lookup");
        if (i != -1)
          callerInfo.lookupKey = paramCursor.getString(i); 
        i = paramCursor.getColumnIndex("photo_uri");
        if (i != -1 && paramCursor.getString(i) != null) {
          callerInfo.contactDisplayPhotoUri = Uri.parse(paramCursor.getString(i));
        } else {
          callerInfo.contactDisplayPhotoUri = null;
        } 
        i = paramCursor.getColumnIndex("preferred_phone_account_component_name");
        if (i != -1 && paramCursor.getString(i) != null)
          callerInfo.preferredPhoneAccountComponent = ComponentName.unflattenFromString(paramCursor.getString(i)); 
        i = paramCursor.getColumnIndex("preferred_phone_account_id");
        if (i != -1 && paramCursor.getString(i) != null)
          callerInfo.preferredPhoneAccountId = paramCursor.getString(i); 
        i = paramCursor.getColumnIndex("custom_ringtone");
        if (i != -1 && paramCursor.getString(i) != null) {
          if (TextUtils.isEmpty(paramCursor.getString(i))) {
            callerInfo.contactRingtoneUri = Uri.EMPTY;
          } else {
            callerInfo.contactRingtoneUri = Uri.parse(paramCursor.getString(i));
          } 
        } else {
          callerInfo.contactRingtoneUri = null;
        } 
        i = paramCursor.getColumnIndex("send_to_voicemail");
        if (i != -1 && 
          paramCursor.getInt(i) == 1) {
          bool = true;
        } else {
          bool = false;
        } 
        callerInfo.shouldSendToVoicemail = bool;
        callerInfo.contactExists = true;
      } 
      paramCursor.close();
    } 
    callerInfo.needUpdate = false;
    callerInfo.name = normalize(callerInfo.name);
    callerInfo.contactRefUri = paramUri;
    return callerInfo;
  }
  
  public static CallerInfo getCallerInfo(Context paramContext, Uri paramUri) {
    Cursor cursor1 = null;
    ContentResolver contentResolver = CallerInfoAsyncQuery.getCurrentProfileContentResolver(paramContext);
    Cursor cursor2 = cursor1;
    if (contentResolver != null)
      try {
        cursor2 = contentResolver.query(paramUri, null, null, null, null);
        CallerInfo callerInfo = getCallerInfo(paramContext, paramUri, cursor2);
      } catch (RuntimeException runtimeException) {
        Log.e("CallerInfo", runtimeException, "Error getting caller info.", new Object[0]);
        cursor2 = cursor1;
      }  
    return (CallerInfo)cursor2;
  }
  
  public static CallerInfo getCallerInfo(Context paramContext, String paramString) {
    if (VDBG)
      Log.v("CallerInfo", "getCallerInfo() based on number...", new Object[0]); 
    int i = SubscriptionManager.getDefaultSubscriptionId();
    return getCallerInfo(paramContext, paramString, i);
  }
  
  public static CallerInfo getCallerInfo(Context paramContext, String paramString, int paramInt) {
    if (TextUtils.isEmpty(paramString))
      return null; 
    if (PhoneNumberUtils.isLocalEmergencyNumber(paramContext, paramString))
      return (new CallerInfo()).markAsEmergency(paramContext); 
    if (PhoneNumberUtils.isVoiceMailNumber(null, paramInt, paramString))
      return (new CallerInfo()).markAsVoiceMail(paramContext, paramInt); 
    Uri uri = ContactsContract.PhoneLookup.ENTERPRISE_CONTENT_FILTER_URI;
    String str = Uri.encode(paramString);
    uri = Uri.withAppendedPath(uri, str);
    CallerInfo callerInfo2 = getCallerInfo(paramContext, uri);
    CallerInfo callerInfo1 = doSecondaryLookupIfNecessary(paramContext, paramString, callerInfo2);
    if (TextUtils.isEmpty(callerInfo1.phoneNumber))
      callerInfo1.phoneNumber = paramString; 
    return callerInfo1;
  }
  
  public String getName() {
    return this.name;
  }
  
  public void setName(String paramString) {
    this.name = paramString;
  }
  
  public String getPhoneNumber() {
    return this.phoneNumber;
  }
  
  public void setPhoneNumber(String paramString) {
    this.phoneNumber = paramString;
  }
  
  public long getContactId() {
    return this.contactIdOrZero;
  }
  
  public Uri getContactDisplayPhotoUri() {
    return this.contactDisplayPhotoUri;
  }
  
  public void SetContactDisplayPhotoUri(Uri paramUri) {
    this.contactDisplayPhotoUri = paramUri;
  }
  
  static CallerInfo doSecondaryLookupIfNecessary(Context paramContext, String paramString, CallerInfo paramCallerInfo) {
    CallerInfo callerInfo = paramCallerInfo;
    if (!paramCallerInfo.contactExists) {
      callerInfo = paramCallerInfo;
      if (PhoneNumberUtils.isUriNumber(paramString)) {
        paramString = PhoneNumberUtils.getUsernameFromUriNumber(paramString);
        callerInfo = paramCallerInfo;
        if (PhoneNumberUtils.isGlobalPhoneNumber(paramString)) {
          Uri uri2 = ContactsContract.PhoneLookup.ENTERPRISE_CONTENT_FILTER_URI;
          paramString = Uri.encode(paramString);
          Uri uri1 = Uri.withAppendedPath(uri2, paramString);
          callerInfo = getCallerInfo(paramContext, uri1);
        } 
      } 
    } 
    return callerInfo;
  }
  
  public boolean isEmergencyNumber() {
    return this.mIsEmergency;
  }
  
  public boolean isVoiceMailNumber() {
    return this.mIsVoiceMail;
  }
  
  CallerInfo markAsEmergency(Context paramContext) {
    this.phoneNumber = paramContext.getString(17040108);
    this.photoResource = 17303164;
    this.mIsEmergency = true;
    return this;
  }
  
  CallerInfo markAsVoiceMail(Context paramContext, int paramInt) {
    this.mIsVoiceMail = true;
    try {
      TelephonyManager telephonyManager = (TelephonyManager)paramContext.getSystemService(TelephonyManager.class);
      telephonyManager = telephonyManager.createForSubscriptionId(paramInt);
      this.phoneNumber = telephonyManager.getVoiceMailAlphaTag();
    } catch (SecurityException securityException) {
      Log.e("CallerInfo", securityException, "Cannot access VoiceMail.", new Object[0]);
    } 
    return this;
  }
  
  private static String normalize(String paramString) {
    if (paramString == null || paramString.length() > 0)
      return paramString; 
    return null;
  }
  
  private static int getColumnIndexForPersonId(Uri paramUri, Cursor paramCursor) {
    Uri uri1;
    byte b;
    if (VDBG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("- getColumnIndexForPersonId: contactRef URI = '");
      stringBuilder.append(paramUri);
      stringBuilder.append("'...");
      Log.v("CallerInfo", stringBuilder.toString(), new Object[0]);
    } 
    String str = paramUri.toString();
    Uri uri2 = null;
    if (str.startsWith("content://com.android.contacts/data/phones")) {
      if (VDBG)
        Log.v("CallerInfo", "'data/phones' URI; using RawContacts.CONTACT_ID", new Object[0]); 
      String str1 = "contact_id";
    } else if (str.startsWith("content://com.android.contacts/data")) {
      if (VDBG)
        Log.v("CallerInfo", "'data' URI; using Data.CONTACT_ID", new Object[0]); 
      String str1 = "contact_id";
    } else if (str.startsWith("content://com.android.contacts/phone_lookup")) {
      if (VDBG)
        Log.v("CallerInfo", "'phone_lookup' URI; using PhoneLookup._ID", new Object[0]); 
      String str1 = "_id";
    } else {
      paramUri = uri2;
      if (VDBG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unexpected prefix for contactRef '");
        stringBuilder.append(str);
        stringBuilder.append("'");
        Log.w("CallerInfo", stringBuilder.toString(), new Object[0]);
        uri1 = uri2;
      } 
    } 
    if (uri1 != null) {
      b = paramCursor.getColumnIndex((String)uri1);
    } else {
      b = -1;
    } 
    if (VDBG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("==> Using column '");
      stringBuilder.append((String)uri1);
      stringBuilder.append("' (columnIndex = ");
      stringBuilder.append(b);
      stringBuilder.append(") for person_id lookup...");
      Log.v("CallerInfo", stringBuilder.toString(), new Object[0]);
    } 
    return b;
  }
  
  public void updateGeoDescription(Context paramContext, String paramString) {
    if (!TextUtils.isEmpty(this.phoneNumber))
      paramString = this.phoneNumber; 
    this.geoDescription = getGeoDescription(paramContext, paramString);
  }
  
  public static String getGeoDescription(Context paramContext, String paramString) {
    Phonenumber.PhoneNumber phoneNumber;
    if (VDBG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getGeoDescription('");
      stringBuilder.append(Log.garbleMiddle(paramString));
      stringBuilder.append("')...");
      Log.v("CallerInfo", stringBuilder.toString(), new Object[0]);
    } 
    if (TextUtils.isEmpty(paramString))
      return null; 
    PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
    PhoneNumberOfflineGeocoder phoneNumberOfflineGeocoder = PhoneNumberOfflineGeocoder.getInstance();
    Locale locale = (paramContext.getResources().getConfiguration()).locale;
    String str = getCurrentCountryIso(paramContext, locale);
    Context context = null;
    paramContext = context;
    try {
      if (VDBG) {
        paramContext = context;
        StringBuilder stringBuilder = new StringBuilder();
        paramContext = context;
        this();
        paramContext = context;
        stringBuilder.append("parsing '");
        paramContext = context;
        stringBuilder.append(Log.garbleMiddle(paramString));
        paramContext = context;
        stringBuilder.append("' for countryIso '");
        paramContext = context;
        stringBuilder.append(str);
        paramContext = context;
        stringBuilder.append("'...");
        paramContext = context;
        Log.v("CallerInfo", stringBuilder.toString(), new Object[0]);
      } 
      paramContext = context;
      Phonenumber.PhoneNumber phoneNumber1 = phoneNumberUtil.parse(paramString, str);
      phoneNumber = phoneNumber1;
      if (VDBG) {
        phoneNumber = phoneNumber1;
        StringBuilder stringBuilder = new StringBuilder();
        phoneNumber = phoneNumber1;
        this();
        phoneNumber = phoneNumber1;
        stringBuilder.append("- parsed number: ");
        phoneNumber = phoneNumber1;
        stringBuilder.append(phoneNumber1);
        phoneNumber = phoneNumber1;
        Log.v("CallerInfo", stringBuilder.toString(), new Object[0]);
      } 
      phoneNumber = phoneNumber1;
    } catch (NumberParseException numberParseException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getGeoDescription: NumberParseException for incoming number '");
      stringBuilder.append(Log.pii(paramString));
      stringBuilder.append("'");
      paramString = stringBuilder.toString();
      Log.w("CallerInfo", paramString, new Object[0]);
    } 
    if (phoneNumber != null) {
      String str1;
      if (SystemProperties.get("ro.oppo.version", "CN").equalsIgnoreCase("US")) {
        str1 = getCountryNameForNumber(phoneNumber, locale);
      } else {
        str1 = phoneNumberOfflineGeocoder.getDescriptionForNumber((Phonenumber.PhoneNumber)str1, locale);
      } 
      if (VDBG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("- got description: '");
        stringBuilder.append(str1);
        stringBuilder.append("'");
        Log.v("CallerInfo", stringBuilder.toString(), new Object[0]);
      } 
      return str1;
    } 
    return null;
  }
  
  private static String getCurrentCountryIso(Context paramContext, Locale paramLocale) {
    Context context1;
    String str;
    Context context2 = null;
    CountryDetector countryDetector = (CountryDetector)paramContext.getSystemService("country_detector");
    paramContext = context2;
    if (countryDetector != null) {
      Country country = countryDetector.detectCountry();
      if (country != null) {
        String str1 = country.getCountryIso();
      } else {
        Log.e("CallerInfo", new Exception(), "CountryDetector.detectCountry() returned null.", new Object[0]);
        context1 = context2;
      } 
    } 
    context2 = context1;
    if (context1 == null) {
      String str1 = paramLocale.getCountry();
      str = str1;
      if (VDBG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("No CountryDetector; falling back to countryIso based on locale: ");
        stringBuilder.append(str1);
        Log.w("CallerInfo", stringBuilder.toString(), new Object[0]);
        str = str1;
      } 
    } 
    return str;
  }
  
  protected static String getCurrentCountryIso(Context paramContext) {
    return getCurrentCountryIso(paramContext, Locale.getDefault());
  }
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder(128), stringBuilder2 = new StringBuilder();
    stringBuilder2.append(super.toString());
    stringBuilder2.append(" { ");
    stringBuilder1.append(stringBuilder2.toString());
    StringBuilder stringBuilder3 = new StringBuilder();
    stringBuilder3.append("name ");
    null = this.name;
    String str = "null";
    if (null == null) {
      null = "null";
    } else {
      null = "non-null";
    } 
    stringBuilder3.append(null);
    stringBuilder1.append(stringBuilder3.toString());
    stringBuilder3 = new StringBuilder();
    stringBuilder3.append(", phoneNumber ");
    if (this.phoneNumber == null) {
      null = str;
    } else {
      null = "non-null";
    } 
    stringBuilder3.append(null);
    stringBuilder1.append(stringBuilder3.toString());
    stringBuilder1.append(" }");
    return stringBuilder1.toString();
  }
  
  public static String getCountryNameForNumber(Phonenumber.PhoneNumber paramPhoneNumber, Locale paramLocale) {
    PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
    PhoneNumberUtil.PhoneNumberType phoneNumberType1 = phoneNumberUtil.getNumberType(paramPhoneNumber);
    PhoneNumberUtil.PhoneNumberType phoneNumberType2 = PhoneNumberUtil.PhoneNumberType.UNKNOWN;
    String str = "";
    if (phoneNumberType1 == phoneNumberType2)
      return ""; 
    null = phoneNumberUtil.getRegionCodeForNumber(paramPhoneNumber);
    return (null == null || null.equals("ZZ") || 
      null.equals("001")) ? 
      str : (new Locale("", null)).getDisplayCountry(paramLocale);
  }
}
