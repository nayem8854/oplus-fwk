package android.telecom;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import java.util.Objects;

public final class DisconnectCause implements Parcelable {
  public static final int ANSWERED_ELSEWHERE = 11;
  
  public static final int BUSY = 7;
  
  public static final int CALL_PULLED = 12;
  
  public static final int CANCELED = 4;
  
  public static final int CONNECTION_MANAGER_NOT_SUPPORTED = 10;
  
  public DisconnectCause(int paramInt) {
    this(paramInt, null, null, null, -1);
  }
  
  public DisconnectCause(int paramInt, String paramString) {
    this(paramInt, null, null, paramString, -1);
  }
  
  public DisconnectCause(int paramInt, CharSequence paramCharSequence1, CharSequence paramCharSequence2, String paramString) {
    this(paramInt, paramCharSequence1, paramCharSequence2, paramString, -1);
  }
  
  public DisconnectCause(int paramInt1, CharSequence paramCharSequence1, CharSequence paramCharSequence2, String paramString, int paramInt2) {
    this.mDisconnectCode = paramInt1;
    this.mDisconnectLabel = paramCharSequence1;
    this.mDisconnectDescription = paramCharSequence2;
    this.mDisconnectReason = paramString;
    this.mToneToPlay = paramInt2;
  }
  
  public int getCode() {
    return this.mDisconnectCode;
  }
  
  public CharSequence getLabel() {
    return this.mDisconnectLabel;
  }
  
  public CharSequence getDescription() {
    return this.mDisconnectDescription;
  }
  
  public String getReason() {
    return this.mDisconnectReason;
  }
  
  public int getTone() {
    return this.mToneToPlay;
  }
  
  public static final Parcelable.Creator<DisconnectCause> CREATOR = new Parcelable.Creator<DisconnectCause>() {
      public DisconnectCause createFromParcel(Parcel param1Parcel) {
        int i = param1Parcel.readInt();
        CharSequence charSequence1 = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(param1Parcel);
        CharSequence charSequence2 = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(param1Parcel);
        String str = param1Parcel.readString();
        int j = param1Parcel.readInt();
        return new DisconnectCause(i, charSequence1, charSequence2, str, j);
      }
      
      public DisconnectCause[] newArray(int param1Int) {
        return new DisconnectCause[param1Int];
      }
    };
  
  public static final int ERROR = 1;
  
  public static final int LOCAL = 2;
  
  public static final int MISSED = 5;
  
  public static final int OTHER = 9;
  
  public static final String REASON_EMERGENCY_CALL_PLACED = "REASON_EMERGENCY_CALL_PLACED";
  
  public static final String REASON_EMULATING_SINGLE_CALL = "EMULATING_SINGLE_CALL";
  
  public static final String REASON_IMS_ACCESS_BLOCKED = "REASON_IMS_ACCESS_BLOCKED";
  
  public static final String REASON_WIFI_ON_BUT_WFC_OFF = "REASON_WIFI_ON_BUT_WFC_OFF";
  
  public static final int REJECTED = 6;
  
  public static final int REMOTE = 3;
  
  public static final int RESTRICTED = 8;
  
  public static final int UNKNOWN = 0;
  
  private int mDisconnectCode;
  
  private CharSequence mDisconnectDescription;
  
  private CharSequence mDisconnectLabel;
  
  private String mDisconnectReason;
  
  private int mToneToPlay;
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mDisconnectCode);
    TextUtils.writeToParcel(this.mDisconnectLabel, paramParcel, paramInt);
    TextUtils.writeToParcel(this.mDisconnectDescription, paramParcel, paramInt);
    paramParcel.writeString(this.mDisconnectReason);
    paramParcel.writeInt(this.mToneToPlay);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public int hashCode() {
    int i = Objects.hashCode(Integer.valueOf(this.mDisconnectCode));
    CharSequence charSequence = this.mDisconnectLabel;
    int j = Objects.hashCode(charSequence);
    charSequence = this.mDisconnectDescription;
    int k = Objects.hashCode(charSequence);
    charSequence = this.mDisconnectReason;
    int m = Objects.hashCode(charSequence), n = this.mToneToPlay;
    n = Objects.hashCode(Integer.valueOf(n));
    return i + j + k + m + n;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof DisconnectCause;
    boolean bool1 = false;
    if (bool) {
      paramObject = paramObject;
      if (Objects.equals(Integer.valueOf(this.mDisconnectCode), Integer.valueOf(paramObject.getCode()))) {
        CharSequence charSequence = this.mDisconnectLabel;
        if (Objects.equals(charSequence, paramObject.getLabel())) {
          charSequence = this.mDisconnectDescription;
          if (Objects.equals(charSequence, paramObject.getDescription())) {
            charSequence = this.mDisconnectReason;
            if (Objects.equals(charSequence, paramObject.getReason())) {
              int i = this.mToneToPlay;
              if (Objects.equals(Integer.valueOf(i), Integer.valueOf(paramObject.getTone())))
                bool1 = true; 
            } 
          } 
        } 
      } 
      return bool1;
    } 
    return false;
  }
  
  public String toString() {
    StringBuilder stringBuilder1;
    String str1;
    switch (this.mDisconnectCode) {
      default:
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append("invalid code: ");
        stringBuilder1.append(this.mDisconnectCode);
        str1 = stringBuilder1.toString();
        break;
      case 12:
        str1 = "CALL_PULLED";
        break;
      case 11:
        str1 = "ANSWERED_ELSEWHERE";
        break;
      case 10:
        str1 = "CONNECTION_MANAGER_NOT_SUPPORTED";
        break;
      case 9:
        str1 = "OTHER";
        break;
      case 8:
        str1 = "RESTRICTED";
        break;
      case 7:
        str1 = "BUSY";
        break;
      case 6:
        str1 = "REJECTED";
        break;
      case 5:
        str1 = "MISSED";
        break;
      case 4:
        str1 = "CANCELED";
        break;
      case 3:
        str1 = "REMOTE";
        break;
      case 2:
        str1 = "LOCAL";
        break;
      case 1:
        str1 = "ERROR";
        break;
      case 0:
        str1 = "UNKNOWN";
        break;
    } 
    CharSequence charSequence1 = this.mDisconnectLabel;
    String str2 = "";
    if (charSequence1 == null) {
      charSequence1 = "";
    } else {
      charSequence1 = charSequence1.toString();
    } 
    CharSequence charSequence2 = this.mDisconnectDescription;
    if (charSequence2 == null) {
      charSequence2 = "";
    } else {
      charSequence2 = charSequence2.toString();
    } 
    String str3 = this.mDisconnectReason;
    if (str3 != null)
      str2 = str3; 
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("DisconnectCause [ Code: (");
    stringBuilder2.append(str1);
    stringBuilder2.append(") Label: (");
    stringBuilder2.append((String)charSequence1);
    stringBuilder2.append(") Description: (");
    stringBuilder2.append((String)charSequence2);
    stringBuilder2.append(") Reason: (");
    stringBuilder2.append(str2);
    stringBuilder2.append(") Tone: (");
    stringBuilder2.append(this.mToneToPlay);
    stringBuilder2.append(") ]");
    return stringBuilder2.toString();
  }
}
