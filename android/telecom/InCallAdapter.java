package android.telecom;

import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import com.android.internal.telecom.IInCallAdapter;
import java.util.List;

public final class InCallAdapter {
  private final IInCallAdapter mAdapter;
  
  public InCallAdapter(IInCallAdapter paramIInCallAdapter) {
    this.mAdapter = paramIInCallAdapter;
  }
  
  public void answerCall(String paramString, int paramInt) {
    try {
      this.mAdapter.answerCall(paramString, paramInt);
    } catch (RemoteException remoteException) {}
  }
  
  public void deflectCall(String paramString, Uri paramUri) {
    try {
      this.mAdapter.deflectCall(paramString, paramUri);
    } catch (RemoteException remoteException) {}
  }
  
  public void rejectCall(String paramString1, boolean paramBoolean, String paramString2) {
    try {
      this.mAdapter.rejectCall(paramString1, paramBoolean, paramString2);
    } catch (RemoteException remoteException) {}
  }
  
  public void rejectCall(String paramString, int paramInt) {
    try {
      this.mAdapter.rejectCallWithReason(paramString, paramInt);
    } catch (RemoteException remoteException) {}
  }
  
  public void transferCall(String paramString, Uri paramUri, boolean paramBoolean) {
    try {
      this.mAdapter.transferCall(paramString, paramUri, paramBoolean);
    } catch (RemoteException remoteException) {}
  }
  
  public void transferCall(String paramString1, String paramString2) {
    try {
      this.mAdapter.consultativeTransfer(paramString1, paramString2);
    } catch (RemoteException remoteException) {}
  }
  
  public void disconnectCall(String paramString) {
    try {
      this.mAdapter.disconnectCall(paramString);
    } catch (RemoteException remoteException) {}
  }
  
  public void holdCall(String paramString) {
    try {
      this.mAdapter.holdCall(paramString);
    } catch (RemoteException remoteException) {}
  }
  
  public void unholdCall(String paramString) {
    try {
      this.mAdapter.unholdCall(paramString);
    } catch (RemoteException remoteException) {}
  }
  
  public void mute(boolean paramBoolean) {
    try {
      this.mAdapter.mute(paramBoolean);
    } catch (RemoteException remoteException) {}
  }
  
  public void setAudioRoute(int paramInt) {
    try {
      this.mAdapter.setAudioRoute(paramInt, null);
    } catch (RemoteException remoteException) {}
  }
  
  public void enterBackgroundAudioProcessing(String paramString) {
    try {
      this.mAdapter.enterBackgroundAudioProcessing(paramString);
    } catch (RemoteException remoteException) {}
  }
  
  public void exitBackgroundAudioProcessing(String paramString, boolean paramBoolean) {
    try {
      this.mAdapter.exitBackgroundAudioProcessing(paramString, paramBoolean);
    } catch (RemoteException remoteException) {}
  }
  
  public void requestBluetoothAudio(String paramString) {
    try {
      this.mAdapter.setAudioRoute(2, paramString);
    } catch (RemoteException remoteException) {}
  }
  
  public void playDtmfTone(String paramString, char paramChar) {
    try {
      this.mAdapter.playDtmfTone(paramString, paramChar);
    } catch (RemoteException remoteException) {}
  }
  
  public void stopDtmfTone(String paramString) {
    try {
      this.mAdapter.stopDtmfTone(paramString);
    } catch (RemoteException remoteException) {}
  }
  
  public void postDialContinue(String paramString, boolean paramBoolean) {
    try {
      this.mAdapter.postDialContinue(paramString, paramBoolean);
    } catch (RemoteException remoteException) {}
  }
  
  public void phoneAccountSelected(String paramString, PhoneAccountHandle paramPhoneAccountHandle, boolean paramBoolean) {
    try {
      this.mAdapter.phoneAccountSelected(paramString, paramPhoneAccountHandle, paramBoolean);
    } catch (RemoteException remoteException) {}
  }
  
  public void conference(String paramString1, String paramString2) {
    try {
      this.mAdapter.conference(paramString1, paramString2);
    } catch (RemoteException remoteException) {}
  }
  
  public void addConferenceParticipants(String paramString, List<Uri> paramList) {
    try {
      this.mAdapter.addConferenceParticipants(paramString, paramList);
    } catch (RemoteException remoteException) {}
  }
  
  public void splitFromConference(String paramString) {
    try {
      this.mAdapter.splitFromConference(paramString);
    } catch (RemoteException remoteException) {}
  }
  
  public void mergeConference(String paramString) {
    try {
      this.mAdapter.mergeConference(paramString);
    } catch (RemoteException remoteException) {}
  }
  
  public void swapConference(String paramString) {
    try {
      this.mAdapter.swapConference(paramString);
    } catch (RemoteException remoteException) {}
  }
  
  public void pullExternalCall(String paramString) {
    try {
      this.mAdapter.pullExternalCall(paramString);
    } catch (RemoteException remoteException) {}
  }
  
  public void sendCallEvent(String paramString1, String paramString2, int paramInt, Bundle paramBundle) {
    try {
      this.mAdapter.sendCallEvent(paramString1, paramString2, paramInt, paramBundle);
    } catch (RemoteException remoteException) {}
  }
  
  public void putExtras(String paramString, Bundle paramBundle) {
    try {
      this.mAdapter.putExtras(paramString, paramBundle);
    } catch (RemoteException remoteException) {}
  }
  
  public void putExtra(String paramString1, String paramString2, boolean paramBoolean) {
    try {
      Bundle bundle = new Bundle();
      this();
      bundle.putBoolean(paramString2, paramBoolean);
      this.mAdapter.putExtras(paramString1, bundle);
    } catch (RemoteException remoteException) {}
  }
  
  public void putExtra(String paramString1, String paramString2, int paramInt) {
    try {
      Bundle bundle = new Bundle();
      this();
      bundle.putInt(paramString2, paramInt);
      this.mAdapter.putExtras(paramString1, bundle);
    } catch (RemoteException remoteException) {}
  }
  
  public void putExtra(String paramString1, String paramString2, String paramString3) {
    try {
      Bundle bundle = new Bundle();
      this();
      bundle.putString(paramString2, paramString3);
      this.mAdapter.putExtras(paramString1, bundle);
    } catch (RemoteException remoteException) {}
  }
  
  public void removeExtras(String paramString, List<String> paramList) {
    try {
      this.mAdapter.removeExtras(paramString, paramList);
    } catch (RemoteException remoteException) {}
  }
  
  public void turnProximitySensorOn() {
    try {
      this.mAdapter.turnOnProximitySensor();
    } catch (RemoteException remoteException) {}
  }
  
  public void turnProximitySensorOff(boolean paramBoolean) {
    try {
      this.mAdapter.turnOffProximitySensor(paramBoolean);
    } catch (RemoteException remoteException) {}
  }
  
  public void sendRttRequest(String paramString) {
    try {
      this.mAdapter.sendRttRequest(paramString);
    } catch (RemoteException remoteException) {}
  }
  
  public void respondToRttRequest(String paramString, int paramInt, boolean paramBoolean) {
    try {
      this.mAdapter.respondToRttRequest(paramString, paramInt, paramBoolean);
    } catch (RemoteException remoteException) {}
  }
  
  public void stopRtt(String paramString) {
    try {
      this.mAdapter.stopRtt(paramString);
    } catch (RemoteException remoteException) {}
  }
  
  public void setRttMode(String paramString, int paramInt) {
    try {
      this.mAdapter.setRttMode(paramString, paramInt);
    } catch (RemoteException remoteException) {}
  }
  
  public void handoverTo(String paramString, PhoneAccountHandle paramPhoneAccountHandle, int paramInt, Bundle paramBundle) {
    try {
      this.mAdapter.handoverTo(paramString, paramPhoneAccountHandle, paramInt, paramBundle);
    } catch (RemoteException remoteException) {}
  }
}
