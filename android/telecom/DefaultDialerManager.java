package android.telecom;

import android.app.ActivityManager;
import android.app.role.RoleManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Process;
import android.os.UserHandle;
import android.text.TextUtils;
import android.util.Slog;
import com.android.internal.util.CollectionUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

public class DefaultDialerManager {
  private static final String TAG = "DefaultDialerManager";
  
  public static boolean setDefaultDialerApplication(Context paramContext, String paramString) {
    return setDefaultDialerApplication(paramContext, paramString, ActivityManager.getCurrentUser());
  }
  
  public static boolean setDefaultDialerApplication(Context paramContext, String paramString, int paramInt) {
    long l = Binder.clearCallingIdentity();
    try {
      CompletableFuture completableFuture = new CompletableFuture();
      this();
      _$$Lambda$DefaultDialerManager$csTSL_1G9gDs8ZsH7BZ6UatLUF0 _$$Lambda$DefaultDialerManager$csTSL_1G9gDs8ZsH7BZ6UatLUF0 = new _$$Lambda$DefaultDialerManager$csTSL_1G9gDs8ZsH7BZ6UatLUF0();
      this(completableFuture);
      RoleManager roleManager = (RoleManager)paramContext.getSystemService(RoleManager.class);
      UserHandle userHandle = UserHandle.of(paramInt);
      Executor executor = AsyncTask.THREAD_POOL_EXECUTOR;
      roleManager.addRoleHolderAsUser("android.app.role.DIALER", paramString, 0, userHandle, executor, _$$Lambda$DefaultDialerManager$csTSL_1G9gDs8ZsH7BZ6UatLUF0);
      completableFuture.get(5L, TimeUnit.SECONDS);
      Binder.restoreCallingIdentity(l);
      return true;
    } catch (InterruptedException|java.util.concurrent.ExecutionException|java.util.concurrent.TimeoutException interruptedException) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Failed to set default dialer to ");
      stringBuilder.append(paramString);
      stringBuilder.append(" for user ");
      stringBuilder.append(paramInt);
      Slog.e("DefaultDialerManager", stringBuilder.toString(), interruptedException);
      Binder.restoreCallingIdentity(l);
      return false;
    } finally {}
    Binder.restoreCallingIdentity(l);
    throw paramContext;
  }
  
  public static String getDefaultDialerApplication(Context paramContext) {
    return getDefaultDialerApplication(paramContext, paramContext.getUserId());
  }
  
  public static String getDefaultDialerApplication(Context paramContext, int paramInt) {
    long l = Binder.clearCallingIdentity();
    try {
      RoleManager roleManager = (RoleManager)paramContext.getSystemService(RoleManager.class);
      List list = roleManager.getRoleHoldersAsUser("android.app.role.DIALER", UserHandle.of(paramInt));
      return (String)CollectionUtils.firstOrNull(list);
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  public static List<String> getInstalledDialerApplications(Context paramContext, int paramInt) {
    PackageManager packageManager = paramContext.getPackageManager();
    Intent intent2 = new Intent("android.intent.action.DIAL");
    List list = packageManager.queryIntentActivitiesAsUser(intent2, 0, paramInt);
    ArrayList<String> arrayList = new ArrayList();
    for (ResolveInfo resolveInfo : list) {
      ActivityInfo activityInfo = resolveInfo.activityInfo;
      if (activityInfo != null) {
        String str = activityInfo.packageName;
        if (!arrayList.contains(str) && resolveInfo.targetUserId == -2)
          arrayList.add(activityInfo.packageName); 
      } 
    } 
    Intent intent1 = new Intent("android.intent.action.DIAL");
    intent1.setData(Uri.fromParts("tel", "", null));
    return filterByIntent(paramContext, arrayList, intent1, paramInt);
  }
  
  public static List<String> getInstalledDialerApplications(Context paramContext) {
    return getInstalledDialerApplications(paramContext, Process.myUserHandle().getIdentifier());
  }
  
  public static boolean isDefaultOrSystemDialer(Context paramContext, String paramString) {
    boolean bool = TextUtils.isEmpty(paramString);
    boolean bool1 = false;
    if (bool)
      return false; 
    TelecomManager telecomManager = getTelecomManager(paramContext);
    if (paramString.equals(telecomManager.getDefaultDialerPackage()) || 
      paramString.equals(telecomManager.getSystemDialerPackage()))
      bool1 = true; 
    return bool1;
  }
  
  private static List<String> filterByIntent(Context paramContext, List<String> paramList, Intent paramIntent, int paramInt) {
    if (paramList == null || paramList.isEmpty())
      return new ArrayList<>(); 
    ArrayList<String> arrayList = new ArrayList();
    PackageManager packageManager = paramContext.getPackageManager();
    List list = packageManager.queryIntentActivitiesAsUser(paramIntent, 0, paramInt);
    int i = list.size();
    for (paramInt = 0; paramInt < i; paramInt++) {
      ActivityInfo activityInfo = ((ResolveInfo)list.get(paramInt)).activityInfo;
      if (activityInfo != null && paramList.contains(activityInfo.packageName)) {
        String str = activityInfo.packageName;
        if (!arrayList.contains(str))
          arrayList.add(activityInfo.packageName); 
      } 
    } 
    return arrayList;
  }
  
  private static TelecomManager getTelecomManager(Context paramContext) {
    return (TelecomManager)paramContext.getSystemService("telecom");
  }
}
