package android.telecom;

import android.annotation.SystemApi;
import android.net.Uri;
import android.os.Bundle;
import android.util.ArraySet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;

public abstract class Conference extends Conferenceable {
  public static final long CONNECT_TIME_NOT_SPECIFIED = 0L;
  
  private static final boolean DEBUG = false;
  
  private Uri mAddress;
  
  private int mAddressPresentation;
  
  private CallAudioState mCallAudioState;
  
  private int mCallDirection;
  
  private String mCallerDisplayName;
  
  private int mCallerDisplayNamePresentation;
  
  private final List<Connection> mChildConnections;
  
  private final List<Connection> mConferenceableConnections;
  
  private long mConnectTimeMillis;
  
  private int mConnectionCapabilities;
  
  private final Connection.Listener mConnectionDeathListener;
  
  private int mConnectionProperties;
  
  private long mConnectionStartElapsedRealTime;
  
  private DisconnectCause mDisconnectCause;
  
  private String mDisconnectMessage;
  
  private Bundle mExtras;
  
  private final Object mExtrasLock;
  
  private boolean mIsMultiparty;
  
  class Listener {
    public void onStateChanged(Conference param1Conference, int param1Int1, int param1Int2) {}
    
    public void onDisconnected(Conference param1Conference, DisconnectCause param1DisconnectCause) {}
    
    public void onConnectionAdded(Conference param1Conference, Connection param1Connection) {}
    
    public void onConnectionRemoved(Conference param1Conference, Connection param1Connection) {}
    
    public void onConferenceableConnectionsChanged(Conference param1Conference, List<Connection> param1List) {}
    
    public void onDestroyed(Conference param1Conference) {}
    
    public void onConnectionCapabilitiesChanged(Conference param1Conference, int param1Int) {}
    
    public void onConnectionPropertiesChanged(Conference param1Conference, int param1Int) {}
    
    public void onVideoStateChanged(Conference param1Conference, int param1Int) {}
    
    public void onVideoProviderChanged(Conference param1Conference, Connection.VideoProvider param1VideoProvider) {}
    
    public void onStatusHintsChanged(Conference param1Conference, StatusHints param1StatusHints) {}
    
    public void onExtrasChanged(Conference param1Conference, Bundle param1Bundle) {}
    
    public void onExtrasRemoved(Conference param1Conference, List<String> param1List) {}
    
    public void onConferenceStateChanged(Conference param1Conference, boolean param1Boolean) {}
    
    public void onAddressChanged(Conference param1Conference, Uri param1Uri, int param1Int) {}
    
    public void onConnectionEvent(Conference param1Conference, String param1String, Bundle param1Bundle) {}
    
    public void onCallerDisplayNameChanged(Conference param1Conference, String param1String, int param1Int) {}
    
    public void onCallDirectionChanged(Conference param1Conference, int param1Int) {}
    
    public void onRingbackRequested(Conference param1Conference, boolean param1Boolean) {}
  }
  
  private final Set<Listener> mListeners = new CopyOnWriteArraySet<>();
  
  private PhoneAccountHandle mPhoneAccount;
  
  private Set<String> mPreviousExtraKeys;
  
  private boolean mRingbackRequested;
  
  private int mState;
  
  private StatusHints mStatusHints;
  
  private String mTelecomCallId;
  
  private final List<Connection> mUnmodifiableChildConnections;
  
  private final List<Connection> mUnmodifiableConferenceableConnections;
  
  public Conference(PhoneAccountHandle paramPhoneAccountHandle) {
    CopyOnWriteArrayList<Connection> copyOnWriteArrayList = new CopyOnWriteArrayList();
    this.mUnmodifiableChildConnections = Collections.unmodifiableList(copyOnWriteArrayList);
    ArrayList<Connection> arrayList = new ArrayList();
    this.mUnmodifiableConferenceableConnections = Collections.unmodifiableList(arrayList);
    this.mState = 1;
    this.mConnectTimeMillis = 0L;
    this.mConnectionStartElapsedRealTime = 0L;
    this.mExtrasLock = new Object();
    this.mRingbackRequested = false;
    this.mIsMultiparty = true;
    this.mConnectionDeathListener = new Connection.Listener() {
        final Conference this$0;
        
        public void onDestroyed(Connection param1Connection) {
          if (Conference.this.mConferenceableConnections.remove(param1Connection))
            Conference.this.fireOnConferenceableConnectionsChanged(); 
        }
      };
    this.mPhoneAccount = paramPhoneAccountHandle;
  }
  
  @SystemApi
  public final String getTelecomCallId() {
    return this.mTelecomCallId;
  }
  
  public final void setTelecomCallId(String paramString) {
    this.mTelecomCallId = paramString;
  }
  
  public final PhoneAccountHandle getPhoneAccountHandle() {
    return this.mPhoneAccount;
  }
  
  public final List<Connection> getConnections() {
    return this.mUnmodifiableChildConnections;
  }
  
  public final int getState() {
    return this.mState;
  }
  
  public final boolean isRingbackRequested() {
    return this.mRingbackRequested;
  }
  
  public final int getConnectionCapabilities() {
    return this.mConnectionCapabilities;
  }
  
  public final int getConnectionProperties() {
    return this.mConnectionProperties;
  }
  
  @SystemApi
  @Deprecated
  public final AudioState getAudioState() {
    return new AudioState(this.mCallAudioState);
  }
  
  public final CallAudioState getCallAudioState() {
    return this.mCallAudioState;
  }
  
  public Connection.VideoProvider getVideoProvider() {
    return null;
  }
  
  public int getVideoState() {
    return 0;
  }
  
  public void onDisconnect() {}
  
  public void onSeparate(Connection paramConnection) {}
  
  public void onMerge(Connection paramConnection) {}
  
  public void onHold() {}
  
  public void onUnhold() {}
  
  public void onMerge() {}
  
  public void onSwap() {}
  
  public void onPlayDtmfTone(char paramChar) {}
  
  public void onStopDtmfTone() {}
  
  @SystemApi
  @Deprecated
  public void onAudioStateChanged(AudioState paramAudioState) {}
  
  public void onCallAudioStateChanged(CallAudioState paramCallAudioState) {}
  
  public void onConnectionAdded(Connection paramConnection) {}
  
  public void onAddConferenceParticipants(List<Uri> paramList) {}
  
  public void onAnswer(int paramInt) {}
  
  public final void onAnswer() {
    onAnswer(0);
  }
  
  public void onReject() {}
  
  public final void setOnHold() {
    setState(5);
  }
  
  public final void setDialing() {
    setState(3);
  }
  
  public final void setRinging() {
    setState(2);
  }
  
  public final void setActive() {
    setRingbackRequested(false);
    setState(4);
  }
  
  public final void setDisconnected(DisconnectCause paramDisconnectCause) {
    this.mDisconnectCause = paramDisconnectCause;
    setState(6);
    for (Listener listener : this.mListeners)
      listener.onDisconnected(this, this.mDisconnectCause); 
  }
  
  public final DisconnectCause getDisconnectCause() {
    return this.mDisconnectCause;
  }
  
  public final void setConnectionCapabilities(int paramInt) {
    if (paramInt != this.mConnectionCapabilities) {
      this.mConnectionCapabilities = paramInt;
      for (Listener listener : this.mListeners)
        listener.onConnectionCapabilitiesChanged(this, this.mConnectionCapabilities); 
    } 
  }
  
  public final void setConnectionProperties(int paramInt) {
    if (paramInt != this.mConnectionProperties) {
      this.mConnectionProperties = paramInt;
      for (Listener listener : this.mListeners)
        listener.onConnectionPropertiesChanged(this, this.mConnectionProperties); 
    } 
  }
  
  public final boolean addConnection(Connection paramConnection) {
    Log.d(this, "Connection=%s, connection=", new Object[] { paramConnection });
    if (paramConnection != null && !this.mChildConnections.contains(paramConnection) && 
      paramConnection.setConference(this)) {
      this.mChildConnections.add(paramConnection);
      onConnectionAdded(paramConnection);
      for (Listener listener : this.mListeners)
        listener.onConnectionAdded(this, paramConnection); 
      return true;
    } 
    return false;
  }
  
  public final void removeConnection(Connection paramConnection) {
    Log.d(this, "removing %s from %s", new Object[] { paramConnection, this.mChildConnections });
    if (paramConnection != null && this.mChildConnections.remove(paramConnection)) {
      paramConnection.resetConference();
      for (Listener listener : this.mListeners)
        listener.onConnectionRemoved(this, paramConnection); 
    } 
  }
  
  public final void setConferenceableConnections(List<Connection> paramList) {
    clearConferenceableList();
    for (Connection connection : paramList) {
      if (!this.mConferenceableConnections.contains(connection)) {
        connection.addConnectionListener(this.mConnectionDeathListener);
        this.mConferenceableConnections.add(connection);
      } 
    } 
    fireOnConferenceableConnectionsChanged();
  }
  
  public final void setRingbackRequested(boolean paramBoolean) {
    if (this.mRingbackRequested != paramBoolean) {
      this.mRingbackRequested = paramBoolean;
      for (Listener listener : this.mListeners)
        listener.onRingbackRequested(this, paramBoolean); 
    } 
  }
  
  public final void setVideoState(Connection paramConnection, int paramInt) {
    Log.d(this, "setVideoState Conference: %s Connection: %s VideoState: %s", new Object[] { this, paramConnection, Integer.valueOf(paramInt) });
    for (Listener listener : this.mListeners)
      listener.onVideoStateChanged(this, paramInt); 
  }
  
  public final void setVideoProvider(Connection paramConnection, Connection.VideoProvider paramVideoProvider) {
    Log.d(this, "setVideoProvider Conference: %s Connection: %s VideoState: %s", new Object[] { this, paramConnection, paramVideoProvider });
    for (Listener listener : this.mListeners)
      listener.onVideoProviderChanged(this, paramVideoProvider); 
  }
  
  private final void fireOnConferenceableConnectionsChanged() {
    for (Listener listener : this.mListeners)
      listener.onConferenceableConnectionsChanged(this, getConferenceableConnections()); 
  }
  
  public final List<Connection> getConferenceableConnections() {
    return this.mUnmodifiableConferenceableConnections;
  }
  
  public final void destroy() {
    Log.d(this, "destroying conference : %s", new Object[] { this });
    for (Connection connection : this.mChildConnections) {
      Log.d(this, "removing connection %s", new Object[] { connection });
      removeConnection(connection);
    } 
    if (this.mState != 6) {
      Log.d(this, "setting to disconnected", new Object[0]);
      setDisconnected(new DisconnectCause(2));
    } 
    for (Listener listener : this.mListeners)
      listener.onDestroyed(this); 
  }
  
  final Conference addListener(Listener paramListener) {
    this.mListeners.add(paramListener);
    return this;
  }
  
  final Conference removeListener(Listener paramListener) {
    this.mListeners.remove(paramListener);
    return this;
  }
  
  @SystemApi
  public Connection getPrimaryConnection() {
    List<Connection> list = this.mUnmodifiableChildConnections;
    if (list == null || list.isEmpty())
      return null; 
    return this.mUnmodifiableChildConnections.get(0);
  }
  
  @SystemApi
  @Deprecated
  public final void setConnectTimeMillis(long paramLong) {
    setConnectionTime(paramLong);
  }
  
  public final void setConnectionTime(long paramLong) {
    this.mConnectTimeMillis = paramLong;
  }
  
  @Deprecated
  public final void setConnectionStartElapsedRealTime(long paramLong) {
    setConnectionStartElapsedRealtimeMillis(paramLong);
  }
  
  public final void setConnectionStartElapsedRealtimeMillis(long paramLong) {
    this.mConnectionStartElapsedRealTime = paramLong;
  }
  
  @SystemApi
  @Deprecated
  public final long getConnectTimeMillis() {
    return getConnectionTime();
  }
  
  public final long getConnectionTime() {
    return this.mConnectTimeMillis;
  }
  
  public final long getConnectionStartElapsedRealtimeMillis() {
    return this.mConnectionStartElapsedRealTime;
  }
  
  final void setCallAudioState(CallAudioState paramCallAudioState) {
    Log.d(this, "setCallAudioState %s", new Object[] { paramCallAudioState });
    this.mCallAudioState = paramCallAudioState;
    onAudioStateChanged(getAudioState());
    onCallAudioStateChanged(paramCallAudioState);
  }
  
  private void setState(int paramInt) {
    if (this.mState != paramInt) {
      int i = this.mState;
      this.mState = paramInt;
      for (Listener listener : this.mListeners)
        listener.onStateChanged(this, i, paramInt); 
    } 
  }
  
  class FailureSignalingConference extends Conference {
    private boolean mImmutable = false;
    
    public FailureSignalingConference(Conference this$0, PhoneAccountHandle param1PhoneAccountHandle) {
      super(param1PhoneAccountHandle);
      setDisconnected((DisconnectCause)this$0);
      this.mImmutable = true;
    }
    
    public void checkImmutable() {
      if (!this.mImmutable)
        return; 
      throw new UnsupportedOperationException("Conference is immutable");
    }
  }
  
  public static Conference createFailedConference(DisconnectCause paramDisconnectCause, PhoneAccountHandle paramPhoneAccountHandle) {
    return new FailureSignalingConference(paramDisconnectCause, paramPhoneAccountHandle);
  }
  
  private final void clearConferenceableList() {
    for (Connection connection : this.mConferenceableConnections)
      connection.removeConnectionListener(this.mConnectionDeathListener); 
    this.mConferenceableConnections.clear();
  }
  
  public String toString() {
    String str3;
    Locale locale = Locale.US;
    int i = this.mState;
    String str1 = Connection.stateToString(i);
    i = this.mConnectionCapabilities;
    String str2 = Call.Details.capabilitiesToString(i);
    i = getVideoState();
    Connection.VideoProvider videoProvider = getVideoProvider();
    if (isRingbackRequested()) {
      str3 = "Y";
    } else {
      str3 = "N";
    } 
    String str4 = super.toString();
    return String.format(locale, "[State: %s,Capabilites: %s, VideoState: %s, VideoProvider: %s,isRingbackRequested: %s, ThisObject %s]", new Object[] { str1, str2, Integer.valueOf(i), videoProvider, str3, str4 });
  }
  
  public final void setStatusHints(StatusHints paramStatusHints) {
    this.mStatusHints = paramStatusHints;
    for (Listener listener : this.mListeners)
      listener.onStatusHintsChanged(this, paramStatusHints); 
  }
  
  public final StatusHints getStatusHints() {
    return this.mStatusHints;
  }
  
  public final void setExtras(Bundle paramBundle) {
    synchronized (this.mExtrasLock) {
      putExtras(paramBundle);
      if (this.mPreviousExtraKeys != null) {
        ArrayList<String> arrayList = new ArrayList();
        this();
        for (String str : this.mPreviousExtraKeys) {
          if (paramBundle == null || !paramBundle.containsKey(str))
            arrayList.add(str); 
        } 
        if (!arrayList.isEmpty())
          removeExtras(arrayList); 
      } 
      if (this.mPreviousExtraKeys == null) {
        ArraySet<String> arraySet = new ArraySet();
        this();
        this.mPreviousExtraKeys = arraySet;
      } 
      this.mPreviousExtraKeys.clear();
      if (paramBundle != null)
        this.mPreviousExtraKeys.addAll(paramBundle.keySet()); 
      return;
    } 
  }
  
  public final void putExtras(Bundle paramBundle) {
    if (paramBundle == null)
      return; 
    synchronized (this.mExtrasLock) {
      if (this.mExtras == null) {
        Bundle bundle = new Bundle();
        this();
        this.mExtras = bundle;
      } 
      this.mExtras.putAll(paramBundle);
      paramBundle = new Bundle();
      this(this.mExtras);
      for (Listener listener : this.mListeners)
        listener.onExtrasChanged(this, new Bundle(paramBundle)); 
      return;
    } 
  }
  
  public final void putExtra(String paramString, boolean paramBoolean) {
    Bundle bundle = new Bundle();
    bundle.putBoolean(paramString, paramBoolean);
    putExtras(bundle);
  }
  
  public final void putExtra(String paramString, int paramInt) {
    Bundle bundle = new Bundle();
    bundle.putInt(paramString, paramInt);
    putExtras(bundle);
  }
  
  public final void putExtra(String paramString1, String paramString2) {
    Bundle bundle = new Bundle();
    bundle.putString(paramString1, paramString2);
    putExtras(bundle);
  }
  
  public final void removeExtras(List<String> paramList) {
    if (paramList == null || paramList.isEmpty())
      return; 
    synchronized (this.mExtrasLock) {
      if (this.mExtras != null)
        for (String str : paramList)
          this.mExtras.remove(str);  
      List<String> list = Collections.unmodifiableList(paramList);
      for (Listener listener : this.mListeners)
        listener.onExtrasRemoved(this, list); 
      return;
    } 
  }
  
  public final void removeExtras(String... paramVarArgs) {
    removeExtras(Arrays.asList(paramVarArgs));
  }
  
  public final Bundle getExtras() {
    return this.mExtras;
  }
  
  public void onExtrasChanged(Bundle paramBundle) {}
  
  @SystemApi
  public void setConferenceState(boolean paramBoolean) {
    this.mIsMultiparty = paramBoolean;
    for (Listener listener : this.mListeners)
      listener.onConferenceStateChanged(this, paramBoolean); 
  }
  
  public final void setCallDirection(int paramInt) {
    Log.d(this, "setDirection %d", new Object[] { Integer.valueOf(paramInt) });
    this.mCallDirection = paramInt;
    for (Listener listener : this.mListeners)
      listener.onCallDirectionChanged(this, paramInt); 
  }
  
  public boolean isMultiparty() {
    return this.mIsMultiparty;
  }
  
  @SystemApi
  public final void setAddress(Uri paramUri, int paramInt) {
    this.mAddress = paramUri;
    this.mAddressPresentation = paramInt;
    for (Listener listener : this.mListeners)
      listener.onAddressChanged(this, paramUri, paramInt); 
  }
  
  public final Uri getAddress() {
    return this.mAddress;
  }
  
  public final int getAddressPresentation() {
    return this.mAddressPresentation;
  }
  
  public final String getCallerDisplayName() {
    return this.mCallerDisplayName;
  }
  
  public final int getCallerDisplayNamePresentation() {
    return this.mCallerDisplayNamePresentation;
  }
  
  public final int getCallDirection() {
    return this.mCallDirection;
  }
  
  @SystemApi
  public final void setCallerDisplayName(String paramString, int paramInt) {
    this.mCallerDisplayName = paramString;
    this.mCallerDisplayNamePresentation = paramInt;
    for (Listener listener : this.mListeners)
      listener.onCallerDisplayNameChanged(this, paramString, paramInt); 
  }
  
  final void handleExtrasChanged(Bundle paramBundle) {
    Bundle bundle = null;
    synchronized (this.mExtrasLock) {
      this.mExtras = paramBundle;
      if (paramBundle != null) {
        bundle = new Bundle();
        this(this.mExtras);
      } 
      onExtrasChanged(bundle);
      return;
    } 
  }
  
  public void sendConferenceEvent(String paramString, Bundle paramBundle) {
    for (Listener listener : this.mListeners)
      listener.onConnectionEvent(this, paramString, paramBundle); 
  }
}
