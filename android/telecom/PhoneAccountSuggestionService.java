package android.telecom;

import android.annotation.SystemApi;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import com.android.internal.telecom.IPhoneAccountSuggestionCallback;
import com.android.internal.telecom.IPhoneAccountSuggestionService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SystemApi
public class PhoneAccountSuggestionService extends Service {
  private IPhoneAccountSuggestionService mInterface = (IPhoneAccountSuggestionService)new IPhoneAccountSuggestionService.Stub() {
      final PhoneAccountSuggestionService this$0;
      
      public void onAccountSuggestionRequest(IPhoneAccountSuggestionCallback param1IPhoneAccountSuggestionCallback, String param1String) {
        PhoneAccountSuggestionService.this.mCallbackMap.put(param1String, param1IPhoneAccountSuggestionCallback);
        PhoneAccountSuggestionService.this.onAccountSuggestionRequest(param1String);
      }
    };
  
  private final Map<String, IPhoneAccountSuggestionCallback> mCallbackMap = new HashMap<>();
  
  public static final String SERVICE_INTERFACE = "android.telecom.PhoneAccountSuggestionService";
  
  public IBinder onBind(Intent paramIntent) {
    return this.mInterface.asBinder();
  }
  
  public void onAccountSuggestionRequest(String paramString) {}
  
  public final void suggestPhoneAccounts(String paramString, List<PhoneAccountSuggestion> paramList) {
    IPhoneAccountSuggestionCallback iPhoneAccountSuggestionCallback = this.mCallbackMap.remove(paramString);
    if (iPhoneAccountSuggestionCallback == null) {
      Log.w(this, "No suggestions requested for the number %s", new Object[] { Log.pii(paramString) });
      return;
    } 
    try {
      iPhoneAccountSuggestionCallback.suggestPhoneAccounts(paramString, paramList);
    } catch (RemoteException remoteException) {
      Log.w(this, "Remote exception calling suggestPhoneAccounts", new Object[0]);
    } 
  }
}
