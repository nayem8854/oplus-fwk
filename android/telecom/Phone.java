package android.telecom;

import android.annotation.SystemApi;
import android.os.Bundle;
import android.util.ArrayMap;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;

@SystemApi
@Deprecated
public final class Phone {
  public static final int SDK_VERSION_R = 30;
  
  private CallAudioState mCallAudioState;
  
  public static abstract class Listener {
    @Deprecated
    public void onAudioStateChanged(Phone param1Phone, AudioState param1AudioState) {}
    
    public void onCallAudioStateChanged(Phone param1Phone, CallAudioState param1CallAudioState) {}
    
    public void onBringToForeground(Phone param1Phone, boolean param1Boolean) {}
    
    public void onCallAdded(Phone param1Phone, Call param1Call) {}
    
    public void onCallRemoved(Phone param1Phone, Call param1Call) {}
    
    public void onCanAddCallChanged(Phone param1Phone, boolean param1Boolean) {}
    
    public void onSilenceRinger(Phone param1Phone) {}
  }
  
  private final Map<String, Call> mCallByTelecomCallId = new ArrayMap<>();
  
  private final String mCallingPackage;
  
  private final List<Call> mCalls;
  
  private boolean mCanAddCall;
  
  private final InCallAdapter mInCallAdapter;
  
  private final List<Listener> mListeners;
  
  private final int mTargetSdkVersion;
  
  private final List<Call> mUnmodifiableCalls;
  
  Phone(InCallAdapter paramInCallAdapter, String paramString, int paramInt) {
    CopyOnWriteArrayList<Call> copyOnWriteArrayList = new CopyOnWriteArrayList();
    this.mUnmodifiableCalls = Collections.unmodifiableList(copyOnWriteArrayList);
    this.mListeners = new CopyOnWriteArrayList<>();
    this.mCanAddCall = false;
    this.mInCallAdapter = paramInCallAdapter;
    this.mCallingPackage = paramString;
    this.mTargetSdkVersion = paramInt;
  }
  
  final void internalAddCall(ParcelableCall paramParcelableCall) {
    if (this.mTargetSdkVersion < 30 && 
      paramParcelableCall.getState() == 12) {
      Log.i(this, "Skipping adding audio processing call for sdk compatibility", new Object[0]);
      return;
    } 
    Call call = this.mCallByTelecomCallId.get(paramParcelableCall.getId());
    if (call == null) {
      String str = paramParcelableCall.getId();
      InCallAdapter inCallAdapter = this.mInCallAdapter;
      call = new Call(this, str, inCallAdapter, paramParcelableCall.getState(), this.mCallingPackage, this.mTargetSdkVersion);
      this.mCallByTelecomCallId.put(paramParcelableCall.getId(), call);
      this.mCalls.add(call);
      checkCallTree(paramParcelableCall);
      call.internalUpdate(paramParcelableCall, this.mCallByTelecomCallId);
      fireCallAdded(call);
    } else {
      Log.w(this, "Call %s added, but it was already present", new Object[] { call.internalGetCallId() });
      checkCallTree(paramParcelableCall);
      call.internalUpdate(paramParcelableCall, this.mCallByTelecomCallId);
    } 
  }
  
  final void internalRemoveCall(Call paramCall) {
    this.mCallByTelecomCallId.remove(paramCall.internalGetCallId());
    this.mCalls.remove(paramCall);
    InCallService.VideoCall videoCall = paramCall.getVideoCall();
    if (videoCall != null)
      videoCall.destroy(); 
    fireCallRemoved(paramCall);
  }
  
  final void internalUpdateCall(ParcelableCall paramParcelableCall) {
    Call call1;
    if (this.mTargetSdkVersion < 30 && 
      paramParcelableCall.getState() == 12) {
      Log.i(this, "removing audio processing call during update for sdk compatibility", new Object[0]);
      call1 = this.mCallByTelecomCallId.get(paramParcelableCall.getId());
      if (call1 != null)
        internalRemoveCall(call1); 
      return;
    } 
    Call call2 = this.mCallByTelecomCallId.get(call1.getId());
    if (call2 != null) {
      checkCallTree((ParcelableCall)call1);
      call2.internalUpdate((ParcelableCall)call1, this.mCallByTelecomCallId);
    } else if (this.mTargetSdkVersion < 30 && (call1.getState() == 4 || 
      call1.getState() == 13)) {
      Log.i(this, "adding call during update for sdk compatibility", new Object[0]);
      internalAddCall((ParcelableCall)call1);
    } 
  }
  
  final void internalSetPostDialWait(String paramString1, String paramString2) {
    Call call = this.mCallByTelecomCallId.get(paramString1);
    if (call != null)
      call.internalSetPostDialWait(paramString2); 
  }
  
  final void internalCallAudioStateChanged(CallAudioState paramCallAudioState) {
    if (!Objects.equals(this.mCallAudioState, paramCallAudioState)) {
      this.mCallAudioState = paramCallAudioState;
      fireCallAudioStateChanged(paramCallAudioState);
    } 
  }
  
  final Call internalGetCallByTelecomId(String paramString) {
    return this.mCallByTelecomCallId.get(paramString);
  }
  
  final void internalBringToForeground(boolean paramBoolean) {
    fireBringToForeground(paramBoolean);
  }
  
  final void internalSetCanAddCall(boolean paramBoolean) {
    if (this.mCanAddCall != paramBoolean) {
      this.mCanAddCall = paramBoolean;
      fireCanAddCallChanged(paramBoolean);
    } 
  }
  
  final void internalSilenceRinger() {
    fireSilenceRinger();
  }
  
  final void internalOnConnectionEvent(String paramString1, String paramString2, Bundle paramBundle) {
    Call call = this.mCallByTelecomCallId.get(paramString1);
    if (call != null)
      call.internalOnConnectionEvent(paramString2, paramBundle); 
  }
  
  final void internalOnRttUpgradeRequest(String paramString, int paramInt) {
    Call call = this.mCallByTelecomCallId.get(paramString);
    if (call != null)
      call.internalOnRttUpgradeRequest(paramInt); 
  }
  
  final void internalOnRttInitiationFailure(String paramString, int paramInt) {
    Call call = this.mCallByTelecomCallId.get(paramString);
    if (call != null)
      call.internalOnRttInitiationFailure(paramInt); 
  }
  
  final void internalOnHandoverFailed(String paramString, int paramInt) {
    Call call = this.mCallByTelecomCallId.get(paramString);
    if (call != null)
      call.internalOnHandoverFailed(paramInt); 
  }
  
  final void internalOnHandoverComplete(String paramString) {
    Call call = this.mCallByTelecomCallId.get(paramString);
    if (call != null)
      call.internalOnHandoverComplete(); 
  }
  
  final void destroy() {
    for (Call call : this.mCalls) {
      InCallService.VideoCall videoCall = call.getVideoCall();
      if (videoCall != null)
        videoCall.destroy(); 
      if (call.getState() != 7)
        call.internalSetDisconnected(); 
    } 
  }
  
  public final void addListener(Listener paramListener) {
    this.mListeners.add(paramListener);
  }
  
  public final void removeListener(Listener paramListener) {
    if (paramListener != null)
      this.mListeners.remove(paramListener); 
  }
  
  public final List<Call> getCalls() {
    return this.mUnmodifiableCalls;
  }
  
  public final boolean canAddCall() {
    return this.mCanAddCall;
  }
  
  public final void setMuted(boolean paramBoolean) {
    this.mInCallAdapter.mute(paramBoolean);
  }
  
  public final void setAudioRoute(int paramInt) {
    this.mInCallAdapter.setAudioRoute(paramInt);
  }
  
  public void requestBluetoothAudio(String paramString) {
    this.mInCallAdapter.requestBluetoothAudio(paramString);
  }
  
  public final void setProximitySensorOn() {
    this.mInCallAdapter.turnProximitySensorOn();
  }
  
  public final void setProximitySensorOff(boolean paramBoolean) {
    this.mInCallAdapter.turnProximitySensorOff(paramBoolean);
  }
  
  @Deprecated
  public final AudioState getAudioState() {
    return new AudioState(this.mCallAudioState);
  }
  
  public final CallAudioState getCallAudioState() {
    return this.mCallAudioState;
  }
  
  private void fireCallAdded(Call paramCall) {
    for (Listener listener : this.mListeners)
      listener.onCallAdded(this, paramCall); 
  }
  
  private void fireCallRemoved(Call paramCall) {
    for (Listener listener : this.mListeners)
      listener.onCallRemoved(this, paramCall); 
  }
  
  private void fireCallAudioStateChanged(CallAudioState paramCallAudioState) {
    for (Listener listener : this.mListeners) {
      listener.onCallAudioStateChanged(this, paramCallAudioState);
      listener.onAudioStateChanged(this, new AudioState(paramCallAudioState));
    } 
  }
  
  private void fireBringToForeground(boolean paramBoolean) {
    for (Listener listener : this.mListeners)
      listener.onBringToForeground(this, paramBoolean); 
  }
  
  private void fireCanAddCallChanged(boolean paramBoolean) {
    for (Listener listener : this.mListeners)
      listener.onCanAddCallChanged(this, paramBoolean); 
  }
  
  private void fireSilenceRinger() {
    for (Listener listener : this.mListeners)
      listener.onSilenceRinger(this); 
  }
  
  private void checkCallTree(ParcelableCall paramParcelableCall) {
    if (paramParcelableCall.getChildCallIds() != null)
      for (byte b = 0; b < paramParcelableCall.getChildCallIds().size(); b++) {
        if (!this.mCallByTelecomCallId.containsKey(paramParcelableCall.getChildCallIds().get(b))) {
          String str = paramParcelableCall.getId();
          Object object = paramParcelableCall.getChildCallIds().get(b);
          Log.wtf(this, "ParcelableCall %s has nonexistent child %s", new Object[] { str, object });
        } 
      }  
  }
}
