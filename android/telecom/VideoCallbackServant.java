package android.telecom;

import android.os.Handler;
import com.android.internal.telecom.IVideoCallback;

final class VideoCallbackServant {
  private static final int MSG_CHANGE_CALL_DATA_USAGE = 4;
  
  private static final int MSG_CHANGE_CAMERA_CAPABILITIES = 5;
  
  private static final int MSG_CHANGE_PEER_DIMENSIONS = 3;
  
  private static final int MSG_CHANGE_VIDEO_QUALITY = 6;
  
  private static final int MSG_HANDLE_CALL_SESSION_EVENT = 2;
  
  private static final int MSG_RECEIVE_SESSION_MODIFY_REQUEST = 0;
  
  private static final int MSG_RECEIVE_SESSION_MODIFY_RESPONSE = 1;
  
  private final IVideoCallback mDelegate;
  
  private final Handler mHandler = (Handler)new Object(this);
  
  private final IVideoCallback mStub = (IVideoCallback)new Object(this);
  
  public VideoCallbackServant(IVideoCallback paramIVideoCallback) {
    this.mDelegate = paramIVideoCallback;
  }
  
  public IVideoCallback getStub() {
    return this.mStub;
  }
}
