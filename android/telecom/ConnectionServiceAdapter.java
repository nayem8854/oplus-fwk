package android.telecom;

import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.telecom.Logging.Session;
import com.android.internal.telecom.IConnectionServiceAdapter;
import com.android.internal.telecom.RemoteServiceCallback;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

final class ConnectionServiceAdapter implements IBinder.DeathRecipient {
  private final Set<IConnectionServiceAdapter> mAdapters = Collections.newSetFromMap(new ConcurrentHashMap<>(8, 0.9F, 1));
  
  void addAdapter(IConnectionServiceAdapter paramIConnectionServiceAdapter) {
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      if (iConnectionServiceAdapter.asBinder() == paramIConnectionServiceAdapter.asBinder()) {
        Log.w(this, "Ignoring duplicate adapter addition.", new Object[0]);
        return;
      } 
    } 
    if (this.mAdapters.add(paramIConnectionServiceAdapter))
      try {
        paramIConnectionServiceAdapter.asBinder().linkToDeath(this, 0);
      } catch (RemoteException remoteException) {
        this.mAdapters.remove(paramIConnectionServiceAdapter);
      }  
  }
  
  void removeAdapter(IConnectionServiceAdapter paramIConnectionServiceAdapter) {
    if (paramIConnectionServiceAdapter != null)
      for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
        if (iConnectionServiceAdapter.asBinder() == paramIConnectionServiceAdapter.asBinder() && this.mAdapters.remove(iConnectionServiceAdapter)) {
          paramIConnectionServiceAdapter.asBinder().unlinkToDeath(this, 0);
          break;
        } 
      }  
  }
  
  public void binderDied() {
    Iterator<IConnectionServiceAdapter> iterator = this.mAdapters.iterator();
    while (iterator.hasNext()) {
      IConnectionServiceAdapter iConnectionServiceAdapter = iterator.next();
      if (!iConnectionServiceAdapter.asBinder().isBinderAlive()) {
        iterator.remove();
        iConnectionServiceAdapter.asBinder().unlinkToDeath(this, 0);
      } 
    } 
  }
  
  void handleCreateConnectionComplete(String paramString, ConnectionRequest paramConnectionRequest, ParcelableConnection paramParcelableConnection) {
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        Session.Info info = Log.getExternalSession();
        iConnectionServiceAdapter.handleCreateConnectionComplete(paramString, paramConnectionRequest, paramParcelableConnection, info);
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void handleCreateConferenceComplete(String paramString, ConnectionRequest paramConnectionRequest, ParcelableConference paramParcelableConference) {
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        Session.Info info = Log.getExternalSession();
        iConnectionServiceAdapter.handleCreateConferenceComplete(paramString, paramConnectionRequest, paramParcelableConference, info);
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void setActive(String paramString) {
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        iConnectionServiceAdapter.setActive(paramString, Log.getExternalSession());
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void setRinging(String paramString) {
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        iConnectionServiceAdapter.setRinging(paramString, Log.getExternalSession());
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void setDialing(String paramString) {
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        iConnectionServiceAdapter.setDialing(paramString, Log.getExternalSession());
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void setPulling(String paramString) {
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        iConnectionServiceAdapter.setPulling(paramString, Log.getExternalSession());
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void setDisconnected(String paramString, DisconnectCause paramDisconnectCause) {
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        iConnectionServiceAdapter.setDisconnected(paramString, paramDisconnectCause, Log.getExternalSession());
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void setOnHold(String paramString) {
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        iConnectionServiceAdapter.setOnHold(paramString, Log.getExternalSession());
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void setRingbackRequested(String paramString, boolean paramBoolean) {
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        iConnectionServiceAdapter.setRingbackRequested(paramString, paramBoolean, Log.getExternalSession());
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void setConnectionCapabilities(String paramString, int paramInt) {
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        iConnectionServiceAdapter.setConnectionCapabilities(paramString, paramInt, Log.getExternalSession());
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void setConnectionProperties(String paramString, int paramInt) {
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        iConnectionServiceAdapter.setConnectionProperties(paramString, paramInt, Log.getExternalSession());
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void setIsConferenced(String paramString1, String paramString2) {
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        Log.d(this, "sending connection %s with conference %s", new Object[] { paramString1, paramString2 });
        iConnectionServiceAdapter.setIsConferenced(paramString1, paramString2, Log.getExternalSession());
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void onConferenceMergeFailed(String paramString) {
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        Log.d(this, "merge failed for call %s", new Object[] { paramString });
        iConnectionServiceAdapter.setConferenceMergeFailed(paramString, Log.getExternalSession());
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void resetConnectionTime(String paramString) {
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        iConnectionServiceAdapter.resetConnectionTime(paramString, Log.getExternalSession());
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void removeCall(String paramString) {
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        iConnectionServiceAdapter.removeCall(paramString, Log.getExternalSession());
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void onPostDialWait(String paramString1, String paramString2) {
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        iConnectionServiceAdapter.onPostDialWait(paramString1, paramString2, Log.getExternalSession());
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void onPostDialChar(String paramString, char paramChar) {
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        iConnectionServiceAdapter.onPostDialChar(paramString, paramChar, Log.getExternalSession());
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void addConferenceCall(String paramString, ParcelableConference paramParcelableConference) {
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        iConnectionServiceAdapter.addConferenceCall(paramString, paramParcelableConference, Log.getExternalSession());
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void queryRemoteConnectionServices(RemoteServiceCallback paramRemoteServiceCallback, String paramString) {
    if (this.mAdapters.size() == 1) {
      try {
        IConnectionServiceAdapter iConnectionServiceAdapter = this.mAdapters.iterator().next();
        Session.Info info = Log.getExternalSession();
        iConnectionServiceAdapter.queryRemoteConnectionServices(paramRemoteServiceCallback, paramString, info);
      } catch (RemoteException remoteException) {
        Log.e(this, (Throwable)remoteException, "Exception trying to query for remote CSs", new Object[0]);
      } 
    } else {
      try {
        remoteException.onResult(Collections.EMPTY_LIST, Collections.EMPTY_LIST);
      } catch (RemoteException remoteException1) {
        Log.e(this, (Throwable)remoteException1, "Exception trying to query for remote CSs", new Object[0]);
      } 
    } 
  }
  
  void setVideoProvider(String paramString, Connection.VideoProvider paramVideoProvider) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mAdapters : Ljava/util/Set;
    //   4: invokeinterface iterator : ()Ljava/util/Iterator;
    //   9: astore_3
    //   10: aload_3
    //   11: invokeinterface hasNext : ()Z
    //   16: ifeq -> 71
    //   19: aload_3
    //   20: invokeinterface next : ()Ljava/lang/Object;
    //   25: checkcast com/android/internal/telecom/IConnectionServiceAdapter
    //   28: astore #4
    //   30: aload_2
    //   31: ifnonnull -> 40
    //   34: aconst_null
    //   35: astore #5
    //   37: goto -> 46
    //   40: aload_2
    //   41: invokevirtual getInterface : ()Lcom/android/internal/telecom/IVideoProvider;
    //   44: astore #5
    //   46: invokestatic getExternalSession : ()Landroid/telecom/Logging/Session$Info;
    //   49: astore #6
    //   51: aload #4
    //   53: aload_1
    //   54: aload #5
    //   56: aload #6
    //   58: invokeinterface setVideoProvider : (Ljava/lang/String;Lcom/android/internal/telecom/IVideoProvider;Landroid/telecom/Logging/Session$Info;)V
    //   63: goto -> 68
    //   66: astore #5
    //   68: goto -> 10
    //   71: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #361	-> 0
    //   #363	-> 30
    //   #365	-> 30
    //   #366	-> 46
    //   #363	-> 51
    //   #368	-> 63
    //   #367	-> 66
    //   #369	-> 68
    //   #370	-> 71
    // Exception table:
    //   from	to	target	type
    //   40	46	66	android/os/RemoteException
    //   46	51	66	android/os/RemoteException
    //   51	63	66	android/os/RemoteException
  }
  
  void setIsVoipAudioMode(String paramString, boolean paramBoolean) {
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        iConnectionServiceAdapter.setIsVoipAudioMode(paramString, paramBoolean, Log.getExternalSession());
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void setStatusHints(String paramString, StatusHints paramStatusHints) {
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        iConnectionServiceAdapter.setStatusHints(paramString, paramStatusHints, Log.getExternalSession());
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void setAddress(String paramString, Uri paramUri, int paramInt) {
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        iConnectionServiceAdapter.setAddress(paramString, paramUri, paramInt, Log.getExternalSession());
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void setCallerDisplayName(String paramString1, String paramString2, int paramInt) {
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        Session.Info info = Log.getExternalSession();
        iConnectionServiceAdapter.setCallerDisplayName(paramString1, paramString2, paramInt, info);
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void setVideoState(String paramString, int paramInt) {
    Log.v(this, "setVideoState: %d", new Object[] { Integer.valueOf(paramInt) });
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        iConnectionServiceAdapter.setVideoState(paramString, paramInt, Log.getExternalSession());
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void setConferenceableConnections(String paramString, List<String> paramList) {
    Log.v(this, "setConferenceableConnections: %s, %s", new Object[] { paramString, paramList });
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        Session.Info info = Log.getExternalSession();
        iConnectionServiceAdapter.setConferenceableConnections(paramString, paramList, info);
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void addExistingConnection(String paramString, ParcelableConnection paramParcelableConnection) {
    Log.v(this, "addExistingConnection: %s", new Object[] { paramString });
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        iConnectionServiceAdapter.addExistingConnection(paramString, paramParcelableConnection, Log.getExternalSession());
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void putExtras(String paramString, Bundle paramBundle) {
    Log.v(this, "putExtras: %s", new Object[] { paramString });
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        iConnectionServiceAdapter.putExtras(paramString, paramBundle, Log.getExternalSession());
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void putExtra(String paramString1, String paramString2, boolean paramBoolean) {
    Log.v(this, "putExtra: %s %s=%b", new Object[] { paramString1, paramString2, Boolean.valueOf(paramBoolean) });
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        Bundle bundle = new Bundle();
        this();
        bundle.putBoolean(paramString2, paramBoolean);
        iConnectionServiceAdapter.putExtras(paramString1, bundle, Log.getExternalSession());
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void putExtra(String paramString1, String paramString2, int paramInt) {
    Log.v(this, "putExtra: %s %s=%d", new Object[] { paramString1, paramString2, Integer.valueOf(paramInt) });
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        Bundle bundle = new Bundle();
        this();
        bundle.putInt(paramString2, paramInt);
        iConnectionServiceAdapter.putExtras(paramString1, bundle, Log.getExternalSession());
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void putExtra(String paramString1, String paramString2, String paramString3) {
    Log.v(this, "putExtra: %s %s=%s", new Object[] { paramString1, paramString2, paramString3 });
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        Bundle bundle = new Bundle();
        this();
        bundle.putString(paramString2, paramString3);
        iConnectionServiceAdapter.putExtras(paramString1, bundle, Log.getExternalSession());
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void removeExtras(String paramString, List<String> paramList) {
    Log.v(this, "removeExtras: %s %s", new Object[] { paramString, paramList });
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        iConnectionServiceAdapter.removeExtras(paramString, paramList, Log.getExternalSession());
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void setAudioRoute(String paramString1, int paramInt, String paramString2) {
    String str = CallAudioState.audioRouteToString(paramInt);
    Log.v(this, "setAudioRoute: %s %s %s", new Object[] { paramString1, str, paramString2 });
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        Session.Info info = Log.getExternalSession();
        iConnectionServiceAdapter.setAudioRoute(paramString1, paramInt, paramString2, info);
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void onConnectionEvent(String paramString1, String paramString2, Bundle paramBundle) {
    Log.v(this, "onConnectionEvent: %s", new Object[] { paramString2 });
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        iConnectionServiceAdapter.onConnectionEvent(paramString1, paramString2, paramBundle, Log.getExternalSession());
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void onRttInitiationSuccess(String paramString) {
    Log.v(this, "onRttInitiationSuccess: %s", new Object[] { paramString });
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        iConnectionServiceAdapter.onRttInitiationSuccess(paramString, Log.getExternalSession());
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void onRttInitiationFailure(String paramString, int paramInt) {
    Log.v(this, "onRttInitiationFailure: %s", new Object[] { paramString });
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        iConnectionServiceAdapter.onRttInitiationFailure(paramString, paramInt, Log.getExternalSession());
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void onRttSessionRemotelyTerminated(String paramString) {
    Log.v(this, "onRttSessionRemotelyTerminated: %s", new Object[] { paramString });
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        iConnectionServiceAdapter.onRttSessionRemotelyTerminated(paramString, Log.getExternalSession());
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void onRemoteRttRequest(String paramString) {
    Log.v(this, "onRemoteRttRequest: %s", new Object[] { paramString });
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        iConnectionServiceAdapter.onRemoteRttRequest(paramString, Log.getExternalSession());
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void onPhoneAccountChanged(String paramString, PhoneAccountHandle paramPhoneAccountHandle) {
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        Log.d(this, "onPhoneAccountChanged %s", new Object[] { paramString });
        iConnectionServiceAdapter.onPhoneAccountChanged(paramString, paramPhoneAccountHandle, Log.getExternalSession());
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void onConnectionServiceFocusReleased() {
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        Log.d(this, "onConnectionServiceFocusReleased", new Object[0]);
        iConnectionServiceAdapter.onConnectionServiceFocusReleased(Log.getExternalSession());
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void setConferenceState(String paramString, boolean paramBoolean) {
    Log.v(this, "setConferenceState: %s %b", new Object[] { paramString, Boolean.valueOf(paramBoolean) });
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        iConnectionServiceAdapter.setConferenceState(paramString, paramBoolean, Log.getExternalSession());
      } catch (RemoteException remoteException) {}
    } 
  }
  
  void setCallDirection(String paramString, int paramInt) {
    for (IConnectionServiceAdapter iConnectionServiceAdapter : this.mAdapters) {
      try {
        iConnectionServiceAdapter.setCallDirection(paramString, paramInt, Log.getExternalSession());
      } catch (RemoteException remoteException) {}
    } 
  }
}
