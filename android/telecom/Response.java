package android.telecom;

public interface Response<IN, OUT> {
  void onError(IN paramIN, int paramInt, String paramString);
  
  void onResult(IN paramIN, OUT... paramVarArgs);
}
