package android.telecom;

import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import com.android.internal.os.SomeArgs;
import com.android.internal.telecom.ICallRedirectionAdapter;
import com.android.internal.telecom.ICallRedirectionService;

public abstract class CallRedirectionService extends Service {
  private static final int MSG_PLACE_CALL = 1;
  
  public static final String SERVICE_INTERFACE = "android.telecom.CallRedirectionService";
  
  private ICallRedirectionAdapter mCallRedirectionAdapter;
  
  public final void placeCallUnmodified() {
    try {
      this.mCallRedirectionAdapter.placeCallUnmodified();
    } catch (RemoteException remoteException) {
      remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public final void redirectCall(Uri paramUri, PhoneAccountHandle paramPhoneAccountHandle, boolean paramBoolean) {
    try {
      this.mCallRedirectionAdapter.redirectCall(paramUri, paramPhoneAccountHandle, paramBoolean);
    } catch (RemoteException remoteException) {
      remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public final void cancelCall() {
    try {
      this.mCallRedirectionAdapter.cancelCall();
    } catch (RemoteException remoteException) {
      remoteException.rethrowAsRuntimeException();
    } 
  }
  
  private final Handler mHandler = (Handler)new Object(this, Looper.getMainLooper());
  
  private final class CallRedirectionBinder extends ICallRedirectionService.Stub {
    final CallRedirectionService this$0;
    
    private CallRedirectionBinder() {}
    
    public void placeCall(ICallRedirectionAdapter param1ICallRedirectionAdapter, Uri param1Uri, PhoneAccountHandle param1PhoneAccountHandle, boolean param1Boolean) {
      SomeArgs someArgs = SomeArgs.obtain();
      someArgs.arg1 = param1ICallRedirectionAdapter;
      someArgs.arg2 = param1Uri;
      someArgs.arg3 = param1PhoneAccountHandle;
      someArgs.arg4 = Boolean.valueOf(param1Boolean);
      CallRedirectionService.this.mHandler.obtainMessage(1, someArgs).sendToTarget();
    }
  }
  
  public final IBinder onBind(Intent paramIntent) {
    return (IBinder)new CallRedirectionBinder();
  }
  
  public final boolean onUnbind(Intent paramIntent) {
    return false;
  }
  
  public abstract void onPlaceCall(Uri paramUri, PhoneAccountHandle paramPhoneAccountHandle, boolean paramBoolean);
}
