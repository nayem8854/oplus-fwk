package android.telecom;

import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.android.internal.telecom.IVideoProvider;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class ParcelableCall implements Parcelable {
  class ParcelableCallBuilder {
    private PhoneAccountHandle mAccountHandle;
    
    private String mActiveChildCallId;
    
    private int mCallDirection;
    
    private String mCallerDisplayName;
    
    private int mCallerDisplayNamePresentation;
    
    private int mCallerNumberVerificationStatus;
    
    private List<String> mCannedSmsResponses;
    
    private int mCapabilities;
    
    private List<String> mChildCallIds;
    
    private List<String> mConferenceableCallIds;
    
    private long mConnectTimeMillis;
    
    private String mContactDisplayName;
    
    private long mCreationTimeMillis;
    
    private DisconnectCause mDisconnectCause;
    
    private Bundle mExtras;
    
    private GatewayInfo mGatewayInfo;
    
    private Uri mHandle;
    
    private int mHandlePresentation;
    
    private String mId;
    
    private Bundle mIntentExtras;
    
    private boolean mIsRttCallChanged;
    
    private boolean mIsVideoCallProviderChanged;
    
    private String mParentCallId;
    
    private int mProperties;
    
    private ParcelableRttCall mRttCall;
    
    private int mState;
    
    private StatusHints mStatusHints;
    
    private int mSupportedAudioRoutes;
    
    private IVideoProvider mVideoCallProvider;
    
    private int mVideoState;
    
    public ParcelableCallBuilder setId(String param1String) {
      this.mId = param1String;
      return this;
    }
    
    public ParcelableCallBuilder setState(int param1Int) {
      this.mState = param1Int;
      return this;
    }
    
    public ParcelableCallBuilder setDisconnectCause(DisconnectCause param1DisconnectCause) {
      this.mDisconnectCause = param1DisconnectCause;
      return this;
    }
    
    public ParcelableCallBuilder setCannedSmsResponses(List<String> param1List) {
      this.mCannedSmsResponses = param1List;
      return this;
    }
    
    public ParcelableCallBuilder setCapabilities(int param1Int) {
      this.mCapabilities = param1Int;
      return this;
    }
    
    public ParcelableCallBuilder setProperties(int param1Int) {
      this.mProperties = param1Int;
      return this;
    }
    
    public ParcelableCallBuilder setSupportedAudioRoutes(int param1Int) {
      this.mSupportedAudioRoutes = param1Int;
      return this;
    }
    
    public ParcelableCallBuilder setConnectTimeMillis(long param1Long) {
      this.mConnectTimeMillis = param1Long;
      return this;
    }
    
    public ParcelableCallBuilder setHandle(Uri param1Uri) {
      this.mHandle = param1Uri;
      return this;
    }
    
    public ParcelableCallBuilder setHandlePresentation(int param1Int) {
      this.mHandlePresentation = param1Int;
      return this;
    }
    
    public ParcelableCallBuilder setCallerDisplayName(String param1String) {
      this.mCallerDisplayName = param1String;
      return this;
    }
    
    public ParcelableCallBuilder setCallerDisplayNamePresentation(int param1Int) {
      this.mCallerDisplayNamePresentation = param1Int;
      return this;
    }
    
    public ParcelableCallBuilder setGatewayInfo(GatewayInfo param1GatewayInfo) {
      this.mGatewayInfo = param1GatewayInfo;
      return this;
    }
    
    public ParcelableCallBuilder setAccountHandle(PhoneAccountHandle param1PhoneAccountHandle) {
      this.mAccountHandle = param1PhoneAccountHandle;
      return this;
    }
    
    public ParcelableCallBuilder setIsVideoCallProviderChanged(boolean param1Boolean) {
      this.mIsVideoCallProviderChanged = param1Boolean;
      return this;
    }
    
    public ParcelableCallBuilder setVideoCallProvider(IVideoProvider param1IVideoProvider) {
      this.mVideoCallProvider = param1IVideoProvider;
      return this;
    }
    
    public ParcelableCallBuilder setIsRttCallChanged(boolean param1Boolean) {
      this.mIsRttCallChanged = param1Boolean;
      return this;
    }
    
    public ParcelableCallBuilder setRttCall(ParcelableRttCall param1ParcelableRttCall) {
      this.mRttCall = param1ParcelableRttCall;
      return this;
    }
    
    public ParcelableCallBuilder setParentCallId(String param1String) {
      this.mParentCallId = param1String;
      return this;
    }
    
    public ParcelableCallBuilder setChildCallIds(List<String> param1List) {
      this.mChildCallIds = param1List;
      return this;
    }
    
    public ParcelableCallBuilder setStatusHints(StatusHints param1StatusHints) {
      this.mStatusHints = param1StatusHints;
      return this;
    }
    
    public ParcelableCallBuilder setVideoState(int param1Int) {
      this.mVideoState = param1Int;
      return this;
    }
    
    public ParcelableCallBuilder setConferenceableCallIds(List<String> param1List) {
      this.mConferenceableCallIds = param1List;
      return this;
    }
    
    public ParcelableCallBuilder setIntentExtras(Bundle param1Bundle) {
      this.mIntentExtras = param1Bundle;
      return this;
    }
    
    public ParcelableCallBuilder setExtras(Bundle param1Bundle) {
      this.mExtras = param1Bundle;
      return this;
    }
    
    public ParcelableCallBuilder setCreationTimeMillis(long param1Long) {
      this.mCreationTimeMillis = param1Long;
      return this;
    }
    
    public ParcelableCallBuilder setCallDirection(int param1Int) {
      this.mCallDirection = param1Int;
      return this;
    }
    
    public ParcelableCallBuilder setCallerNumberVerificationStatus(int param1Int) {
      this.mCallerNumberVerificationStatus = param1Int;
      return this;
    }
    
    public ParcelableCallBuilder setContactDisplayName(String param1String) {
      this.mContactDisplayName = param1String;
      return this;
    }
    
    public ParcelableCallBuilder setActiveChildCallId(String param1String) {
      this.mActiveChildCallId = param1String;
      return this;
    }
    
    public ParcelableCall createParcelableCall() {
      return new ParcelableCall(this.mId, this.mState, this.mDisconnectCause, this.mCannedSmsResponses, this.mCapabilities, this.mProperties, this.mSupportedAudioRoutes, this.mConnectTimeMillis, this.mHandle, this.mHandlePresentation, this.mCallerDisplayName, this.mCallerDisplayNamePresentation, this.mGatewayInfo, this.mAccountHandle, this.mIsVideoCallProviderChanged, this.mVideoCallProvider, this.mIsRttCallChanged, this.mRttCall, this.mParentCallId, this.mChildCallIds, this.mStatusHints, this.mVideoState, this.mConferenceableCallIds, this.mIntentExtras, this.mExtras, this.mCreationTimeMillis, this.mCallDirection, this.mCallerNumberVerificationStatus, this.mContactDisplayName, this.mActiveChildCallId);
    }
    
    public static ParcelableCallBuilder fromParcelableCall(ParcelableCall param1ParcelableCall) {
      ParcelableCallBuilder parcelableCallBuilder = new ParcelableCallBuilder();
      parcelableCallBuilder.mId = param1ParcelableCall.mId;
      parcelableCallBuilder.mState = param1ParcelableCall.mState;
      parcelableCallBuilder.mDisconnectCause = param1ParcelableCall.mDisconnectCause;
      parcelableCallBuilder.mCannedSmsResponses = param1ParcelableCall.mCannedSmsResponses;
      parcelableCallBuilder.mCapabilities = param1ParcelableCall.mCapabilities;
      parcelableCallBuilder.mProperties = param1ParcelableCall.mProperties;
      parcelableCallBuilder.mSupportedAudioRoutes = param1ParcelableCall.mSupportedAudioRoutes;
      parcelableCallBuilder.mConnectTimeMillis = param1ParcelableCall.mConnectTimeMillis;
      parcelableCallBuilder.mHandle = param1ParcelableCall.mHandle;
      parcelableCallBuilder.mHandlePresentation = param1ParcelableCall.mHandlePresentation;
      parcelableCallBuilder.mCallerDisplayName = param1ParcelableCall.mCallerDisplayName;
      parcelableCallBuilder.mCallerDisplayNamePresentation = param1ParcelableCall.mCallerDisplayNamePresentation;
      parcelableCallBuilder.mGatewayInfo = param1ParcelableCall.mGatewayInfo;
      parcelableCallBuilder.mAccountHandle = param1ParcelableCall.mAccountHandle;
      parcelableCallBuilder.mIsVideoCallProviderChanged = param1ParcelableCall.mIsVideoCallProviderChanged;
      parcelableCallBuilder.mVideoCallProvider = param1ParcelableCall.mVideoCallProvider;
      parcelableCallBuilder.mIsRttCallChanged = param1ParcelableCall.mIsRttCallChanged;
      parcelableCallBuilder.mRttCall = param1ParcelableCall.mRttCall;
      parcelableCallBuilder.mParentCallId = param1ParcelableCall.mParentCallId;
      parcelableCallBuilder.mChildCallIds = param1ParcelableCall.mChildCallIds;
      parcelableCallBuilder.mStatusHints = param1ParcelableCall.mStatusHints;
      parcelableCallBuilder.mVideoState = param1ParcelableCall.mVideoState;
      parcelableCallBuilder.mConferenceableCallIds = param1ParcelableCall.mConferenceableCallIds;
      parcelableCallBuilder.mIntentExtras = param1ParcelableCall.mIntentExtras;
      parcelableCallBuilder.mExtras = param1ParcelableCall.mExtras;
      parcelableCallBuilder.mCreationTimeMillis = param1ParcelableCall.mCreationTimeMillis;
      parcelableCallBuilder.mCallDirection = param1ParcelableCall.mCallDirection;
      parcelableCallBuilder.mCallerNumberVerificationStatus = param1ParcelableCall.mCallerNumberVerificationStatus;
      parcelableCallBuilder.mContactDisplayName = param1ParcelableCall.mContactDisplayName;
      parcelableCallBuilder.mActiveChildCallId = param1ParcelableCall.mActiveChildCallId;
      return parcelableCallBuilder;
    }
  }
  
  public ParcelableCall(String paramString1, int paramInt1, DisconnectCause paramDisconnectCause, List<String> paramList1, int paramInt2, int paramInt3, int paramInt4, long paramLong1, Uri paramUri, int paramInt5, String paramString2, int paramInt6, GatewayInfo paramGatewayInfo, PhoneAccountHandle paramPhoneAccountHandle, boolean paramBoolean1, IVideoProvider paramIVideoProvider, boolean paramBoolean2, ParcelableRttCall paramParcelableRttCall, String paramString3, List<String> paramList2, StatusHints paramStatusHints, int paramInt7, List<String> paramList3, Bundle paramBundle1, Bundle paramBundle2, long paramLong2, int paramInt8, int paramInt9, String paramString4, String paramString5) {
    this.mId = paramString1;
    this.mState = paramInt1;
    this.mDisconnectCause = paramDisconnectCause;
    this.mCannedSmsResponses = paramList1;
    this.mCapabilities = paramInt2;
    this.mProperties = paramInt3;
    this.mSupportedAudioRoutes = paramInt4;
    this.mConnectTimeMillis = paramLong1;
    this.mHandle = paramUri;
    this.mHandlePresentation = paramInt5;
    this.mCallerDisplayName = paramString2;
    this.mCallerDisplayNamePresentation = paramInt6;
    this.mGatewayInfo = paramGatewayInfo;
    this.mAccountHandle = paramPhoneAccountHandle;
    this.mIsVideoCallProviderChanged = paramBoolean1;
    this.mVideoCallProvider = paramIVideoProvider;
    this.mIsRttCallChanged = paramBoolean2;
    this.mRttCall = paramParcelableRttCall;
    this.mParentCallId = paramString3;
    this.mChildCallIds = paramList2;
    this.mStatusHints = paramStatusHints;
    this.mVideoState = paramInt7;
    this.mConferenceableCallIds = Collections.unmodifiableList(paramList3);
    this.mIntentExtras = paramBundle1;
    this.mExtras = paramBundle2;
    this.mCreationTimeMillis = paramLong2;
    this.mCallDirection = paramInt8;
    this.mCallerNumberVerificationStatus = paramInt9;
    this.mContactDisplayName = paramString4;
    this.mActiveChildCallId = paramString5;
  }
  
  public String getId() {
    return this.mId;
  }
  
  public int getState() {
    return this.mState;
  }
  
  public DisconnectCause getDisconnectCause() {
    return this.mDisconnectCause;
  }
  
  public List<String> getCannedSmsResponses() {
    return this.mCannedSmsResponses;
  }
  
  public int getCapabilities() {
    return this.mCapabilities;
  }
  
  public int getProperties() {
    return this.mProperties;
  }
  
  public int getSupportedAudioRoutes() {
    return this.mSupportedAudioRoutes;
  }
  
  public long getConnectTimeMillis() {
    return this.mConnectTimeMillis;
  }
  
  public Uri getHandle() {
    return this.mHandle;
  }
  
  public int getHandlePresentation() {
    return this.mHandlePresentation;
  }
  
  public String getCallerDisplayName() {
    return this.mCallerDisplayName;
  }
  
  public int getCallerDisplayNamePresentation() {
    return this.mCallerDisplayNamePresentation;
  }
  
  public GatewayInfo getGatewayInfo() {
    return this.mGatewayInfo;
  }
  
  public PhoneAccountHandle getAccountHandle() {
    return this.mAccountHandle;
  }
  
  public VideoCallImpl getVideoCallImpl(String paramString, int paramInt) {
    if (this.mVideoCall == null) {
      IVideoProvider iVideoProvider = this.mVideoCallProvider;
      if (iVideoProvider != null)
        try {
          VideoCallImpl videoCallImpl = new VideoCallImpl();
          this(iVideoProvider, paramString, paramInt);
          this.mVideoCall = videoCallImpl;
        } catch (RemoteException remoteException) {} 
    } 
    return this.mVideoCall;
  }
  
  public IVideoProvider getVideoProvider() {
    return this.mVideoCallProvider;
  }
  
  public boolean getIsRttCallChanged() {
    return this.mIsRttCallChanged;
  }
  
  public ParcelableRttCall getParcelableRttCall() {
    return this.mRttCall;
  }
  
  public String getParentCallId() {
    return this.mParentCallId;
  }
  
  public List<String> getChildCallIds() {
    return this.mChildCallIds;
  }
  
  public List<String> getConferenceableCallIds() {
    return this.mConferenceableCallIds;
  }
  
  public StatusHints getStatusHints() {
    return this.mStatusHints;
  }
  
  public int getVideoState() {
    return this.mVideoState;
  }
  
  public Bundle getExtras() {
    return this.mExtras;
  }
  
  public Bundle getIntentExtras() {
    return this.mIntentExtras;
  }
  
  public boolean isVideoCallProviderChanged() {
    return this.mIsVideoCallProviderChanged;
  }
  
  public long getCreationTimeMillis() {
    return this.mCreationTimeMillis;
  }
  
  public int getCallDirection() {
    return this.mCallDirection;
  }
  
  public int getCallerNumberVerificationStatus() {
    return this.mCallerNumberVerificationStatus;
  }
  
  public String getContactDisplayName() {
    return this.mContactDisplayName;
  }
  
  public String getActiveChildCallId() {
    return this.mActiveChildCallId;
  }
  
  public static final Parcelable.Creator<ParcelableCall> CREATOR = new Parcelable.Creator<ParcelableCall>() {
      public ParcelableCall createFromParcel(Parcel param1Parcel) {
        boolean bool2;
        ClassLoader classLoader = ParcelableCall.class.getClassLoader();
        String str3 = param1Parcel.readString();
        int i = param1Parcel.readInt();
        DisconnectCause disconnectCause = (DisconnectCause)param1Parcel.readParcelable(classLoader);
        ArrayList<String> arrayList1 = new ArrayList();
        param1Parcel.readList(arrayList1, classLoader);
        int j = param1Parcel.readInt();
        int k = param1Parcel.readInt();
        long l1 = param1Parcel.readLong();
        Uri uri = (Uri)Uri.CREATOR.createFromParcel(param1Parcel);
        int m = param1Parcel.readInt();
        String str4 = param1Parcel.readString();
        int n = param1Parcel.readInt();
        GatewayInfo gatewayInfo = (GatewayInfo)param1Parcel.readParcelable(classLoader);
        PhoneAccountHandle phoneAccountHandle = (PhoneAccountHandle)param1Parcel.readParcelable(classLoader);
        byte b = param1Parcel.readByte();
        boolean bool1 = false;
        if (b == 1) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        IVideoProvider iVideoProvider = IVideoProvider.Stub.asInterface(param1Parcel.readStrongBinder());
        String str5 = param1Parcel.readString();
        ArrayList<String> arrayList2 = new ArrayList();
        param1Parcel.readList(arrayList2, classLoader);
        StatusHints statusHints = (StatusHints)param1Parcel.readParcelable(classLoader);
        int i2 = param1Parcel.readInt();
        ArrayList<String> arrayList3 = new ArrayList();
        param1Parcel.readList(arrayList3, classLoader);
        Bundle bundle1 = param1Parcel.readBundle(classLoader);
        Bundle bundle2 = param1Parcel.readBundle(classLoader);
        int i3 = param1Parcel.readInt();
        if (param1Parcel.readByte() == 1)
          bool1 = true; 
        ParcelableRttCall parcelableRttCall = (ParcelableRttCall)param1Parcel.readParcelable(classLoader);
        long l2 = param1Parcel.readLong();
        int i1 = param1Parcel.readInt();
        int i4 = param1Parcel.readInt();
        String str2 = param1Parcel.readString();
        String str1 = param1Parcel.readString();
        ParcelableCall.ParcelableCallBuilder parcelableCallBuilder18 = new ParcelableCall.ParcelableCallBuilder();
        ParcelableCall.ParcelableCallBuilder parcelableCallBuilder3 = parcelableCallBuilder18.setId(str3);
        parcelableCallBuilder3 = parcelableCallBuilder3.setState(i);
        ParcelableCall.ParcelableCallBuilder parcelableCallBuilder4 = parcelableCallBuilder3.setDisconnectCause(disconnectCause);
        ParcelableCall.ParcelableCallBuilder parcelableCallBuilder5 = parcelableCallBuilder4.setCannedSmsResponses(arrayList1);
        parcelableCallBuilder5 = parcelableCallBuilder5.setCapabilities(j);
        parcelableCallBuilder5 = parcelableCallBuilder5.setProperties(k);
        parcelableCallBuilder5 = parcelableCallBuilder5.setSupportedAudioRoutes(i3);
        parcelableCallBuilder5 = parcelableCallBuilder5.setConnectTimeMillis(l1);
        ParcelableCall.ParcelableCallBuilder parcelableCallBuilder6 = parcelableCallBuilder5.setHandle(uri);
        parcelableCallBuilder6 = parcelableCallBuilder6.setHandlePresentation(m);
        ParcelableCall.ParcelableCallBuilder parcelableCallBuilder7 = parcelableCallBuilder6.setCallerDisplayName(str4);
        parcelableCallBuilder7 = parcelableCallBuilder7.setCallerDisplayNamePresentation(n);
        ParcelableCall.ParcelableCallBuilder parcelableCallBuilder8 = parcelableCallBuilder7.setGatewayInfo(gatewayInfo);
        ParcelableCall.ParcelableCallBuilder parcelableCallBuilder9 = parcelableCallBuilder8.setAccountHandle(phoneAccountHandle);
        parcelableCallBuilder9 = parcelableCallBuilder9.setIsVideoCallProviderChanged(bool2);
        ParcelableCall.ParcelableCallBuilder parcelableCallBuilder10 = parcelableCallBuilder9.setVideoCallProvider(iVideoProvider);
        parcelableCallBuilder10 = parcelableCallBuilder10.setIsRttCallChanged(bool1);
        ParcelableCall.ParcelableCallBuilder parcelableCallBuilder17 = parcelableCallBuilder10.setRttCall(parcelableRttCall);
        ParcelableCall.ParcelableCallBuilder parcelableCallBuilder11 = parcelableCallBuilder17.setParentCallId(str5);
        ParcelableCall.ParcelableCallBuilder parcelableCallBuilder12 = parcelableCallBuilder11.setChildCallIds(arrayList2);
        ParcelableCall.ParcelableCallBuilder parcelableCallBuilder13 = parcelableCallBuilder12.setStatusHints(statusHints);
        parcelableCallBuilder13 = parcelableCallBuilder13.setVideoState(i2);
        ParcelableCall.ParcelableCallBuilder parcelableCallBuilder14 = parcelableCallBuilder13.setConferenceableCallIds(arrayList3);
        ParcelableCall.ParcelableCallBuilder parcelableCallBuilder15 = parcelableCallBuilder14.setIntentExtras(bundle1);
        ParcelableCall.ParcelableCallBuilder parcelableCallBuilder16 = parcelableCallBuilder15.setExtras(bundle2);
        parcelableCallBuilder16 = parcelableCallBuilder16.setCreationTimeMillis(l2);
        parcelableCallBuilder16 = parcelableCallBuilder16.setCallDirection(i1);
        parcelableCallBuilder16 = parcelableCallBuilder16.setCallerNumberVerificationStatus(i4);
        ParcelableCall.ParcelableCallBuilder parcelableCallBuilder2 = parcelableCallBuilder16.setContactDisplayName(str2);
        ParcelableCall.ParcelableCallBuilder parcelableCallBuilder1 = parcelableCallBuilder2.setActiveChildCallId(str1);
        return parcelableCallBuilder1.createParcelableCall();
      }
      
      public ParcelableCall[] newArray(int param1Int) {
        return new ParcelableCall[param1Int];
      }
    };
  
  private final PhoneAccountHandle mAccountHandle;
  
  private final String mActiveChildCallId;
  
  private final int mCallDirection;
  
  private final String mCallerDisplayName;
  
  private final int mCallerDisplayNamePresentation;
  
  private final int mCallerNumberVerificationStatus;
  
  private final List<String> mCannedSmsResponses;
  
  private final int mCapabilities;
  
  private final List<String> mChildCallIds;
  
  private final List<String> mConferenceableCallIds;
  
  private final long mConnectTimeMillis;
  
  private final String mContactDisplayName;
  
  private final long mCreationTimeMillis;
  
  private final DisconnectCause mDisconnectCause;
  
  private final Bundle mExtras;
  
  private final GatewayInfo mGatewayInfo;
  
  private final Uri mHandle;
  
  private final int mHandlePresentation;
  
  private final String mId;
  
  private final Bundle mIntentExtras;
  
  private final boolean mIsRttCallChanged;
  
  private final boolean mIsVideoCallProviderChanged;
  
  private final String mParentCallId;
  
  private final int mProperties;
  
  private final ParcelableRttCall mRttCall;
  
  private final int mState;
  
  private final StatusHints mStatusHints;
  
  private final int mSupportedAudioRoutes;
  
  private VideoCallImpl mVideoCall;
  
  private final IVideoProvider mVideoCallProvider;
  
  private final int mVideoState;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mId);
    paramParcel.writeInt(this.mState);
    paramParcel.writeParcelable(this.mDisconnectCause, 0);
    paramParcel.writeList(this.mCannedSmsResponses);
    paramParcel.writeInt(this.mCapabilities);
    paramParcel.writeInt(this.mProperties);
    paramParcel.writeLong(this.mConnectTimeMillis);
    Uri.writeToParcel(paramParcel, this.mHandle);
    paramParcel.writeInt(this.mHandlePresentation);
    paramParcel.writeString(this.mCallerDisplayName);
    paramParcel.writeInt(this.mCallerDisplayNamePresentation);
    paramParcel.writeParcelable(this.mGatewayInfo, 0);
    paramParcel.writeParcelable(this.mAccountHandle, 0);
    paramParcel.writeByte((byte)this.mIsVideoCallProviderChanged);
    IVideoProvider iVideoProvider = this.mVideoCallProvider;
    if (iVideoProvider != null) {
      IBinder iBinder = iVideoProvider.asBinder();
    } else {
      iVideoProvider = null;
    } 
    paramParcel.writeStrongBinder((IBinder)iVideoProvider);
    paramParcel.writeString(this.mParentCallId);
    paramParcel.writeList(this.mChildCallIds);
    paramParcel.writeParcelable(this.mStatusHints, 0);
    paramParcel.writeInt(this.mVideoState);
    paramParcel.writeList(this.mConferenceableCallIds);
    paramParcel.writeBundle(this.mIntentExtras);
    paramParcel.writeBundle(this.mExtras);
    paramParcel.writeInt(this.mSupportedAudioRoutes);
    paramParcel.writeByte((byte)this.mIsRttCallChanged);
    paramParcel.writeParcelable(this.mRttCall, 0);
    paramParcel.writeLong(this.mCreationTimeMillis);
    paramParcel.writeInt(this.mCallDirection);
    paramParcel.writeInt(this.mCallerNumberVerificationStatus);
    paramParcel.writeString(this.mContactDisplayName);
    paramParcel.writeString(this.mActiveChildCallId);
  }
  
  public String toString() {
    return String.format("[%s, parent:%s, children:%s]", new Object[] { this.mId, this.mParentCallId, this.mChildCallIds });
  }
}
