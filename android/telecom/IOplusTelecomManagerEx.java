package android.telecom;

import android.content.Intent;
import android.os.Bundle;

public interface IOplusTelecomManagerEx {
  void addNewOutgoingCall(Intent paramIntent);
  
  void oplusCancelMissedCallsNotification(Bundle paramBundle);
  
  String oplusInteractWithTelecomService(int paramInt, String paramString);
}
