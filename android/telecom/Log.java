package android.telecom;

import android.content.ComponentName;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.SystemProperties;
import android.telecom.Logging.EventManager;
import android.telecom.Logging.Session;
import android.telecom.Logging.SessionManager;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import com.android.internal.util.IndentingPrintWriter;
import java.util.Arrays;
import java.util.IllegalFormatException;
import java.util.Locale;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Log {
  public static boolean DEBUG = false;
  
  public static boolean ERROR = false;
  
  private static final int EVENTS_TO_CACHE = 10;
  
  private static final int EVENTS_TO_CACHE_DEBUG = 20;
  
  private static final long EXTENDED_LOGGING_DURATION_MILLIS = 1800000L;
  
  private static final boolean FORCE_LOGGING = false;
  
  public static boolean INFO;
  
  private static final int NUM_DIALABLE_DIGITS_TO_LOG;
  
  public static boolean OPPO_DEBUG;
  
  public static boolean OPPO_PANIC;
  
  public static boolean OPPO_PHONE_LOG_SWITCH;
  
  static {
    byte b;
    if (Build.IS_USER) {
      b = 0;
    } else {
      b = 2;
    } 
    NUM_DIALABLE_DIGITS_TO_LOG = b;
  }
  
  public static String TAG = "TelecomFramework";
  
  private static final boolean USER_BUILD;
  
  public static boolean VERBOSE;
  
  public static boolean WARN;
  
  private static EventManager sEventManager;
  
  private static boolean sIsUserExtendedLoggingEnabled;
  
  private static SessionManager sSessionManager;
  
  private static final Object sSingletonSync;
  
  private static long sUserExtendedLoggingStopTime;
  
  static {
    OPPO_PHONE_LOG_SWITCH = false;
    boolean bool = SystemProperties.getBoolean("persist.sys.assert.panic", false);
    DEBUG = bool;
    INFO = bool;
    VERBOSE = false;
    OPPO_DEBUG = false;
    WARN = isLoggable(5);
    ERROR = isLoggable(6);
    USER_BUILD = Build.IS_USER;
    sSingletonSync = new Object();
    sIsUserExtendedLoggingEnabled = false;
    sUserExtendedLoggingStopTime = 0L;
  }
  
  public static void oppoRefreshLogSwitch(Context paramContext) {
    // Byte code:
    //   0: aload_0
    //   1: ifnonnull -> 5
    //   4: return
    //   5: iconst_0
    //   6: istore_1
    //   7: ldc 'persist.sys.assert.panic'
    //   9: iconst_0
    //   10: invokestatic getBoolean : (Ljava/lang/String;Z)Z
    //   13: putstatic android/telecom/Log.OPPO_PANIC : Z
    //   16: aload_0
    //   17: invokevirtual getContentResolver : ()Landroid/content/ContentResolver;
    //   20: ldc_w 'phone.log.switch'
    //   23: iconst_0
    //   24: invokestatic getInt : (Landroid/content/ContentResolver;Ljava/lang/String;I)I
    //   27: iconst_1
    //   28: if_icmpne -> 36
    //   31: iconst_1
    //   32: istore_2
    //   33: goto -> 38
    //   36: iconst_0
    //   37: istore_2
    //   38: iload_2
    //   39: putstatic android/telecom/Log.OPPO_PHONE_LOG_SWITCH : Z
    //   42: getstatic android/telecom/Log.TAG : Ljava/lang/String;
    //   45: astore_0
    //   46: new java/lang/StringBuilder
    //   49: dup
    //   50: invokespecial <init> : ()V
    //   53: astore_3
    //   54: aload_3
    //   55: ldc_w 'OPPO_PANIC = '
    //   58: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   61: pop
    //   62: aload_3
    //   63: getstatic android/telecom/Log.OPPO_PANIC : Z
    //   66: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   69: pop
    //   70: aload_3
    //   71: ldc_w ', OPPO_PHONE_LOG_SWITCH = '
    //   74: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   77: pop
    //   78: aload_3
    //   79: getstatic android/telecom/Log.OPPO_PHONE_LOG_SWITCH : Z
    //   82: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   85: pop
    //   86: aload_0
    //   87: ldc 'TelecomFramework'
    //   89: aload_3
    //   90: invokevirtual toString : ()Ljava/lang/String;
    //   93: iconst_0
    //   94: anewarray java/lang/Object
    //   97: invokestatic buildMessage : (Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   100: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   103: pop
    //   104: getstatic android/telecom/Log.OPPO_PHONE_LOG_SWITCH : Z
    //   107: ifne -> 118
    //   110: iload_1
    //   111: istore_2
    //   112: getstatic android/telecom/Log.OPPO_PANIC : Z
    //   115: ifeq -> 120
    //   118: iconst_1
    //   119: istore_2
    //   120: iload_2
    //   121: putstatic android/telecom/Log.DEBUG : Z
    //   124: iload_2
    //   125: putstatic android/telecom/Log.INFO : Z
    //   128: getstatic android/telecom/Log.OPPO_PHONE_LOG_SWITCH : Z
    //   131: putstatic android/telecom/Log.OPPO_DEBUG : Z
    //   134: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #115	-> 0
    //   #116	-> 4
    //   #118	-> 5
    //   #120	-> 16
    //   #121	-> 42
    //   #124	-> 104
    //   #133	-> 124
    //   #134	-> 128
    //   #135	-> 134
  }
  
  public static void d(String paramString1, String paramString2, Object... paramVarArgs) {
    if (sIsUserExtendedLoggingEnabled) {
      maybeDisableLogging();
      android.util.Log.i(TAG, buildMessage(paramString1, paramString2, paramVarArgs));
    } else if (DEBUG) {
      android.util.Log.d(TAG, buildMessage(paramString1, paramString2, paramVarArgs));
    } 
  }
  
  public static void d(Object paramObject, String paramString, Object... paramVarArgs) {
    if (sIsUserExtendedLoggingEnabled) {
      maybeDisableLogging();
      android.util.Log.i(TAG, buildMessage(getPrefixFromObject(paramObject), paramString, paramVarArgs));
    } else if (DEBUG) {
      android.util.Log.d(TAG, buildMessage(getPrefixFromObject(paramObject), paramString, paramVarArgs));
    } 
  }
  
  public static void i(String paramString1, String paramString2, Object... paramVarArgs) {
    if (INFO)
      android.util.Log.i(TAG, buildMessage(paramString1, paramString2, paramVarArgs)); 
  }
  
  public static void i(Object paramObject, String paramString, Object... paramVarArgs) {
    if (INFO)
      android.util.Log.i(TAG, buildMessage(getPrefixFromObject(paramObject), paramString, paramVarArgs)); 
  }
  
  public static void v(String paramString1, String paramString2, Object... paramVarArgs) {
    if (sIsUserExtendedLoggingEnabled) {
      maybeDisableLogging();
      android.util.Log.i(TAG, buildMessage(paramString1, paramString2, paramVarArgs));
    } else if (VERBOSE) {
      android.util.Log.v(TAG, buildMessage(paramString1, paramString2, paramVarArgs));
    } 
  }
  
  public static void v(Object paramObject, String paramString, Object... paramVarArgs) {
    if (sIsUserExtendedLoggingEnabled) {
      maybeDisableLogging();
      android.util.Log.i(TAG, buildMessage(getPrefixFromObject(paramObject), paramString, paramVarArgs));
    } else if (VERBOSE) {
      android.util.Log.v(TAG, buildMessage(getPrefixFromObject(paramObject), paramString, paramVarArgs));
    } 
  }
  
  public static void w(String paramString1, String paramString2, Object... paramVarArgs) {
    if (WARN)
      android.util.Log.w(TAG, buildMessage(paramString1, paramString2, paramVarArgs)); 
  }
  
  public static void w(Object paramObject, String paramString, Object... paramVarArgs) {
    if (WARN)
      android.util.Log.w(TAG, buildMessage(getPrefixFromObject(paramObject), paramString, paramVarArgs)); 
  }
  
  public static void e(String paramString1, Throwable paramThrowable, String paramString2, Object... paramVarArgs) {
    if (ERROR)
      android.util.Log.e(TAG, buildMessage(paramString1, paramString2, paramVarArgs), paramThrowable); 
  }
  
  public static void e(Object paramObject, Throwable paramThrowable, String paramString, Object... paramVarArgs) {
    if (ERROR)
      android.util.Log.e(TAG, buildMessage(getPrefixFromObject(paramObject), paramString, paramVarArgs), paramThrowable); 
  }
  
  public static void wtf(String paramString1, Throwable paramThrowable, String paramString2, Object... paramVarArgs) {
    android.util.Log.wtf(TAG, buildMessage(paramString1, paramString2, paramVarArgs), paramThrowable);
  }
  
  public static void wtf(Object paramObject, Throwable paramThrowable, String paramString, Object... paramVarArgs) {
    android.util.Log.wtf(TAG, buildMessage(getPrefixFromObject(paramObject), paramString, paramVarArgs), paramThrowable);
  }
  
  public static void wtf(String paramString1, String paramString2, Object... paramVarArgs) {
    paramString1 = buildMessage(paramString1, paramString2, paramVarArgs);
    android.util.Log.wtf(TAG, paramString1, new IllegalStateException(paramString1));
  }
  
  public static void wtf(Object paramObject, String paramString, Object... paramVarArgs) {
    paramObject = buildMessage(getPrefixFromObject(paramObject), paramString, paramVarArgs);
    android.util.Log.wtf(TAG, (String)paramObject, new IllegalStateException((String)paramObject));
  }
  
  public static void setSessionContext(Context paramContext) {
    getSessionManager().setContext(paramContext);
  }
  
  public static void startSession(String paramString) {
    getSessionManager().startSession(paramString, null);
  }
  
  public static void startSession(Session.Info paramInfo, String paramString) {
    getSessionManager().startSession(paramInfo, paramString, null);
  }
  
  public static void startSession(String paramString1, String paramString2) {
    getSessionManager().startSession(paramString1, paramString2);
  }
  
  public static void startSession(Session.Info paramInfo, String paramString1, String paramString2) {
    getSessionManager().startSession(paramInfo, paramString1, paramString2);
  }
  
  public static Session createSubsession() {
    return getSessionManager().createSubsession();
  }
  
  public static Session.Info getExternalSession() {
    return getSessionManager().getExternalSession();
  }
  
  public static Session.Info getExternalSession(String paramString) {
    return getSessionManager().getExternalSession(paramString);
  }
  
  public static void cancelSubsession(Session paramSession) {
    getSessionManager().cancelSubsession(paramSession);
  }
  
  public static void continueSession(Session paramSession, String paramString) {
    getSessionManager().continueSession(paramSession, paramString);
  }
  
  public static void endSession() {
    getSessionManager().endSession();
  }
  
  public static void registerSessionListener(SessionManager.ISessionListener paramISessionListener) {
    getSessionManager().registerSessionListener(paramISessionListener);
  }
  
  public static String getSessionId() {
    synchronized (sSingletonSync) {
      if (sSessionManager != null)
        return getSessionManager().getSessionId(); 
      return "";
    } 
  }
  
  public static void addEvent(EventManager.Loggable paramLoggable, String paramString) {
    getEventManager().event(paramLoggable, paramString, null);
  }
  
  public static void addEvent(EventManager.Loggable paramLoggable, String paramString, Object paramObject) {
    getEventManager().event(paramLoggable, paramString, paramObject);
  }
  
  public static void addEvent(EventManager.Loggable paramLoggable, String paramString1, String paramString2, Object... paramVarArgs) {
    getEventManager().event(paramLoggable, paramString1, paramString2, paramVarArgs);
  }
  
  public static void registerEventListener(EventManager.EventListener paramEventListener) {
    getEventManager().registerEventListener(paramEventListener);
  }
  
  public static void addRequestResponsePair(EventManager.TimedEventPair paramTimedEventPair) {
    getEventManager().addRequestResponsePair(paramTimedEventPair);
  }
  
  public static void dumpEvents(IndentingPrintWriter paramIndentingPrintWriter) {
    synchronized (sSingletonSync) {
      if (sEventManager != null) {
        getEventManager().dumpEvents(paramIndentingPrintWriter);
      } else {
        paramIndentingPrintWriter.println("No Historical Events Logged.");
      } 
      return;
    } 
  }
  
  public static void dumpEventsTimeline(IndentingPrintWriter paramIndentingPrintWriter) {
    synchronized (sSingletonSync) {
      if (sEventManager != null) {
        getEventManager().dumpEventsTimeline(paramIndentingPrintWriter);
      } else {
        paramIndentingPrintWriter.println("No Historical Events Logged.");
      } 
      return;
    } 
  }
  
  public static void setIsExtendedLoggingEnabled(boolean paramBoolean) {
    if (sIsUserExtendedLoggingEnabled == paramBoolean)
      return; 
    EventManager eventManager = sEventManager;
    if (eventManager != null) {
      byte b;
      if (paramBoolean) {
        b = 20;
      } else {
        b = 10;
      } 
      eventManager.changeEventCacheSize(b);
    } 
    sIsUserExtendedLoggingEnabled = paramBoolean;
    if (paramBoolean) {
      sUserExtendedLoggingStopTime = System.currentTimeMillis() + 1800000L;
    } else {
      sUserExtendedLoggingStopTime = 0L;
    } 
  }
  
  private static EventManager getEventManager() {
    if (sEventManager == null)
      synchronized (sSingletonSync) {
        if (sEventManager == null) {
          EventManager eventManager = new EventManager();
          this((SessionManager.ISessionIdQueryHandler)_$$Lambda$qa4s1Fm2YuohEunaJUJcmJXDXG0.INSTANCE);
          sEventManager = eventManager;
          return eventManager;
        } 
      }  
    return sEventManager;
  }
  
  public static SessionManager getSessionManager() {
    if (sSessionManager == null)
      synchronized (sSingletonSync) {
        if (sSessionManager == null) {
          SessionManager sessionManager = new SessionManager();
          this();
          sSessionManager = sessionManager;
          return sessionManager;
        } 
      }  
    return sSessionManager;
  }
  
  public static void setTag(String paramString) {
    TAG = paramString;
    WARN = isLoggable(5);
    ERROR = isLoggable(6);
  }
  
  private static void maybeDisableLogging() {
    if (!sIsUserExtendedLoggingEnabled)
      return; 
    if (sUserExtendedLoggingStopTime < System.currentTimeMillis()) {
      sUserExtendedLoggingStopTime = 0L;
      sIsUserExtendedLoggingEnabled = false;
    } 
  }
  
  public static boolean isLoggable(int paramInt) {
    return android.util.Log.isLoggable(TAG, paramInt);
  }
  
  public static String piiHandle(Object paramObject) {
    if (paramObject == null || VERBOSE)
      return String.valueOf(paramObject); 
    StringBuilder stringBuilder = new StringBuilder();
    if (paramObject instanceof Uri) {
      Uri uri = (Uri)paramObject;
      String str2 = uri.getScheme();
      if (!TextUtils.isEmpty(str2)) {
        stringBuilder.append(str2);
        stringBuilder.append(":");
      } 
      String str1 = uri.getSchemeSpecificPart();
      if ("tel".equals(str2)) {
        obfuscatePhoneNumber(stringBuilder, str1);
      } else if ("sip".equals(str2)) {
        for (byte b = 0; b < str1.length(); b++) {
          char c = str1.charAt(b);
          char c1 = c;
          if (c != '@') {
            c1 = c;
            if (c != '.')
              c1 = '*'; 
          } 
          stringBuilder.append(c1);
        } 
      } else {
        stringBuilder.append(pii(paramObject));
      } 
    } else if (paramObject instanceof String) {
      paramObject = paramObject;
      obfuscatePhoneNumber(stringBuilder, (String)paramObject);
    } 
    return stringBuilder.toString();
  }
  
  private static void obfuscatePhoneNumber(StringBuilder paramStringBuilder, String paramString) {
    int i = getDialableCount(paramString) - NUM_DIALABLE_DIGITS_TO_LOG;
    for (byte b = 0; b < paramString.length(); b++, i = j) {
      Character character;
      char c = paramString.charAt(b);
      boolean bool = PhoneNumberUtils.isDialable(c);
      int j = i;
      if (bool)
        j = i - 1; 
      if (bool && j >= 0) {
        String str = "*";
      } else {
        character = Character.valueOf(c);
      } 
      paramStringBuilder.append(character);
    } 
  }
  
  private static int getDialableCount(String paramString) {
    int i = 0;
    char[] arrayOfChar;
    int j;
    byte b;
    for (arrayOfChar = paramString.toCharArray(), j = arrayOfChar.length, b = 0; b < j; ) {
      char c = arrayOfChar[b];
      int k = i;
      if (PhoneNumberUtils.isDialable(c))
        k = i + 1; 
      b++;
      i = k;
    } 
    return i;
  }
  
  public static String pii(Object paramObject) {
    if (paramObject == null || VERBOSE)
      return String.valueOf(paramObject); 
    return "***";
  }
  
  private static String getPrefixFromObject(Object paramObject) {
    if (paramObject == null) {
      paramObject = "<null>";
    } else {
      paramObject = paramObject.getClass().getSimpleName();
    } 
    return (String)paramObject;
  }
  
  private static String buildMessage(String paramString1, String paramString2, Object... paramVarArgs) {
    String str = null;
    if (OPPO_DEBUG)
      str = getSessionId(); 
    if (TextUtils.isEmpty(str)) {
      str = "";
    } else {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(": ");
      stringBuilder.append(str);
      str = stringBuilder.toString();
    } 
    if (paramVarArgs != null)
      try {
        if (paramVarArgs.length != 0)
          String str1 = String.format(Locale.US, paramString2, paramVarArgs); 
      } catch (IllegalFormatException illegalFormatException) {
        String str1 = TAG;
        int i = paramVarArgs.length;
        e(str1, illegalFormatException, "Log: IllegalFormatException: formatString='%s' numArgs=%d", new Object[] { paramString2, Integer.valueOf(i) });
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(paramString2);
        stringBuilder.append(" (An error occurred while formatting the message.)");
        paramString2 = stringBuilder.toString();
      }  
    return String.format(Locale.US, "%s: %s%s", new Object[] { paramString1, paramString2, str });
  }
  
  public static String getPackageAbbreviation(ComponentName paramComponentName) {
    if (paramComponentName == null)
      return ""; 
    return getPackageAbbreviation(paramComponentName.getPackageName());
  }
  
  public static String getPackageAbbreviation(String paramString) {
    if (paramString == null)
      return ""; 
    Stream<String> stream = Arrays.stream(paramString.split("\\."));
    -$.Lambda.Log.PMgs-iYBck7KnOoPeH-SfbZtCRA pMgs-iYBck7KnOoPeH-SfbZtCRA = _$$Lambda$Log$PMgs_iYBck7KnOoPeH_SfbZtCRA.INSTANCE;
    stream = stream.map((Function<? super String, ? extends String>)pMgs-iYBck7KnOoPeH-SfbZtCRA);
    return stream.collect(Collectors.joining(""));
  }
  
  public static String garbleMiddle(String paramString) {
    if (TextUtils.isEmpty(paramString) || paramString.length() < 2)
      return paramString; 
    int i = paramString.length();
    StringBuilder stringBuilder = new StringBuilder();
    if (i > 7) {
      stringBuilder.append(paramString.substring(0, 3));
      stringBuilder.append("****");
      stringBuilder.append(paramString.substring(i - 4));
      return stringBuilder.toString();
    } 
    if (i > 2) {
      stringBuilder.append(paramString.substring(0, 1));
      stringBuilder.append("****");
      stringBuilder.append(paramString.substring(i - 1));
      return stringBuilder.toString();
    } 
    stringBuilder.append(paramString.substring(0, 1));
    stringBuilder.append("*");
    return stringBuilder.toString();
  }
}
