package android.telecom;

import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import com.android.internal.telecom.IConnectionService;
import com.android.internal.telecom.IConnectionServiceAdapter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

final class RemoteConnectionService {
  private static final RemoteConference NULL_CONFERENCE;
  
  private static final RemoteConnection NULL_CONNECTION = new RemoteConnection("NULL", null, (ConnectionRequest)null);
  
  private final Map<String, RemoteConference> mConferenceById;
  
  private final Map<String, RemoteConnection> mConnectionById;
  
  private final IBinder.DeathRecipient mDeathRecipient;
  
  private final ConnectionService mOurConnectionServiceImpl;
  
  private final IConnectionService mOutgoingConnectionServiceRpc;
  
  private final Set<RemoteConnection> mPendingConnections;
  
  private final ConnectionServiceAdapterServant mServant;
  
  private final IConnectionServiceAdapter mServantDelegate;
  
  static {
    NULL_CONFERENCE = new RemoteConference("NULL", null);
  }
  
  RemoteConnectionService(IConnectionService paramIConnectionService, ConnectionService paramConnectionService) throws RemoteException {
    Object object = new Object(this);
    this.mServant = new ConnectionServiceAdapterServant((IConnectionServiceAdapter)object);
    this.mDeathRecipient = (IBinder.DeathRecipient)new Object(this);
    this.mConnectionById = new HashMap<>();
    this.mConferenceById = new HashMap<>();
    this.mPendingConnections = new HashSet<>();
    this.mOutgoingConnectionServiceRpc = paramIConnectionService;
    paramIConnectionService.asBinder().linkToDeath(this.mDeathRecipient, 0);
    this.mOurConnectionServiceImpl = paramConnectionService;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("[RemoteCS - ");
    stringBuilder.append(this.mOutgoingConnectionServiceRpc.asBinder().toString());
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  final RemoteConnection createRemoteConnection(PhoneAccountHandle paramPhoneAccountHandle, ConnectionRequest paramConnectionRequest, boolean paramBoolean) {
    String str1 = UUID.randomUUID().toString();
    Bundle bundle = new Bundle();
    if (paramConnectionRequest.getExtras() != null)
      bundle.putAll(paramConnectionRequest.getExtras()); 
    ConnectionService connectionService = this.mOurConnectionServiceImpl;
    String str2 = connectionService.getApplicationContext().getOpPackageName();
    bundle.putString("android.telecom.extra.REMOTE_CONNECTION_ORIGINATING_PACKAGE_NAME", str2);
    ConnectionRequest.Builder builder3 = new ConnectionRequest.Builder();
    builder3 = builder3.setAccountHandle(paramConnectionRequest.getAccountHandle());
    builder3 = builder3.setAddress(paramConnectionRequest.getAddress());
    ConnectionRequest.Builder builder2 = builder3.setExtras(bundle);
    builder2 = builder2.setVideoState(paramConnectionRequest.getVideoState());
    builder2 = builder2.setRttPipeFromInCall(paramConnectionRequest.getRttPipeFromInCall());
    ConnectionRequest.Builder builder1 = builder2.setRttPipeToInCall(paramConnectionRequest.getRttPipeToInCall());
    ConnectionRequest connectionRequest = builder1.build();
    try {
      if (this.mConnectionById.isEmpty())
        this.mOutgoingConnectionServiceRpc.addConnectionServiceAdapter(this.mServant.getStub(), null); 
      RemoteConnection remoteConnection = new RemoteConnection();
      this(str1, this.mOutgoingConnectionServiceRpc, connectionRequest);
      this.mPendingConnections.add(remoteConnection);
      this.mConnectionById.put(str1, remoteConnection);
      this.mOutgoingConnectionServiceRpc.createConnection(paramPhoneAccountHandle, str1, connectionRequest, paramBoolean, false, null);
      Object object = new Object();
      super(this, str1);
      remoteConnection.registerCallback((RemoteConnection.Callback)object);
      return remoteConnection;
    } catch (RemoteException remoteException) {
      DisconnectCause disconnectCause = new DisconnectCause(1, remoteException.toString());
      return RemoteConnection.failure(disconnectCause);
    } 
  }
  
  private boolean hasConnection(String paramString) {
    return this.mConnectionById.containsKey(paramString);
  }
  
  private RemoteConnection findConnectionForAction(String paramString1, String paramString2) {
    if (this.mConnectionById.containsKey(paramString1))
      return this.mConnectionById.get(paramString1); 
    Log.w(this, "%s - Cannot find Connection %s", new Object[] { paramString2, paramString1 });
    return NULL_CONNECTION;
  }
  
  private RemoteConference findConferenceForAction(String paramString1, String paramString2) {
    if (this.mConferenceById.containsKey(paramString1))
      return this.mConferenceById.get(paramString1); 
    Log.w(this, "%s - Cannot find Conference %s", new Object[] { paramString2, paramString1 });
    return NULL_CONFERENCE;
  }
  
  private void maybeDisconnectAdapter() {
    if (this.mConnectionById.isEmpty() && this.mConferenceById.isEmpty())
      try {
        this.mOutgoingConnectionServiceRpc.removeConnectionServiceAdapter(this.mServant.getStub(), null);
      } catch (RemoteException remoteException) {} 
  }
}
