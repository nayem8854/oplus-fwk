package android.telecom;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Locale;

@SystemApi
@Deprecated
public class AudioState implements Parcelable {
  public AudioState(boolean paramBoolean, int paramInt1, int paramInt2) {
    this.isMuted = paramBoolean;
    this.route = paramInt1;
    this.supportedRouteMask = paramInt2;
  }
  
  public AudioState(AudioState paramAudioState) {
    this.isMuted = paramAudioState.isMuted();
    this.route = paramAudioState.getRoute();
    this.supportedRouteMask = paramAudioState.getSupportedRouteMask();
  }
  
  public AudioState(CallAudioState paramCallAudioState) {
    this.isMuted = paramCallAudioState.isMuted();
    this.route = paramCallAudioState.getRoute();
    this.supportedRouteMask = paramCallAudioState.getSupportedRouteMask();
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = false;
    if (paramObject == null)
      return false; 
    if (!(paramObject instanceof AudioState))
      return false; 
    paramObject = paramObject;
    if (isMuted() == paramObject.isMuted() && getRoute() == paramObject.getRoute() && 
      getSupportedRouteMask() == paramObject.getSupportedRouteMask())
      bool = true; 
    return bool;
  }
  
  public String toString() {
    Locale locale = Locale.US;
    boolean bool = this.isMuted;
    int i = this.route;
    String str1 = audioRouteToString(i);
    i = this.supportedRouteMask;
    String str2 = audioRouteToString(i);
    return String.format(locale, "[AudioState isMuted: %b, route: %s, supportedRouteMask: %s]", new Object[] { Boolean.valueOf(bool), str1, str2 });
  }
  
  public static String audioRouteToString(int paramInt) {
    if (paramInt == 0 || (paramInt & 0xFFFFFFF0) != 0)
      return "UNKNOWN"; 
    StringBuffer stringBuffer = new StringBuffer();
    if ((paramInt & 0x1) == 1)
      listAppend(stringBuffer, "EARPIECE"); 
    if ((paramInt & 0x2) == 2)
      listAppend(stringBuffer, "BLUETOOTH"); 
    if ((paramInt & 0x4) == 4)
      listAppend(stringBuffer, "WIRED_HEADSET"); 
    if ((paramInt & 0x8) == 8)
      listAppend(stringBuffer, "SPEAKER"); 
    return stringBuffer.toString();
  }
  
  private static void listAppend(StringBuffer paramStringBuffer, String paramString) {
    if (paramStringBuffer.length() > 0)
      paramStringBuffer.append(", "); 
    paramStringBuffer.append(paramString);
  }
  
  public static final Parcelable.Creator<AudioState> CREATOR = new Parcelable.Creator<AudioState>() {
      public AudioState createFromParcel(Parcel param1Parcel) {
        boolean bool;
        if (param1Parcel.readByte() == 0) {
          bool = false;
        } else {
          bool = true;
        } 
        int i = param1Parcel.readInt();
        int j = param1Parcel.readInt();
        return new AudioState(bool, i, j);
      }
      
      public AudioState[] newArray(int param1Int) {
        return new AudioState[param1Int];
      }
    };
  
  private static final int ROUTE_ALL = 15;
  
  public static final int ROUTE_BLUETOOTH = 2;
  
  public static final int ROUTE_EARPIECE = 1;
  
  public static final int ROUTE_SPEAKER = 8;
  
  public static final int ROUTE_WIRED_HEADSET = 4;
  
  public static final int ROUTE_WIRED_OR_EARPIECE = 5;
  
  private final boolean isMuted;
  
  private final int route;
  
  private final int supportedRouteMask;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeByte((byte)this.isMuted);
    paramParcel.writeInt(this.route);
    paramParcel.writeInt(this.supportedRouteMask);
  }
  
  public boolean isMuted() {
    return this.isMuted;
  }
  
  public int getRoute() {
    return this.route;
  }
  
  public int getSupportedRouteMask() {
    return this.supportedRouteMask;
  }
}
