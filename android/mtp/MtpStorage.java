package android.mtp;

import android.os.storage.StorageVolume;

public class MtpStorage {
  private final String mDescription;
  
  private final long mMaxFileSize;
  
  private final String mPath;
  
  private final boolean mRemovable;
  
  private final int mStorageId;
  
  private final String mVolumeName;
  
  public MtpStorage(StorageVolume paramStorageVolume, int paramInt) {
    this.mStorageId = paramInt;
    this.mPath = paramStorageVolume.getPath();
    this.mDescription = paramStorageVolume.getDescription(null);
    this.mRemovable = paramStorageVolume.isRemovable();
    this.mMaxFileSize = paramStorageVolume.getMaxFileSize();
    this.mVolumeName = paramStorageVolume.getMediaStoreVolumeName();
  }
  
  public final int getStorageId() {
    return this.mStorageId;
  }
  
  public final String getPath() {
    return this.mPath;
  }
  
  public final String getDescription() {
    return this.mDescription;
  }
  
  public final boolean isRemovable() {
    return this.mRemovable;
  }
  
  public long getMaxFileSize() {
    return this.mMaxFileSize;
  }
  
  public String getVolumeName() {
    return this.mVolumeName;
  }
}
