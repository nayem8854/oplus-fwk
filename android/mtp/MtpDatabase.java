package android.mtp;

import android.content.BroadcastReceiver;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Message;
import android.os.SystemProperties;
import android.os.storage.StorageVolume;
import android.provider.MediaStore;
import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import android.util.Log;
import android.util.SparseArray;
import android.view.Display;
import android.view.WindowManager;
import com.google.android.collect.Sets;
import dalvik.system.CloseGuard;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

public class MtpDatabase extends OplusBaseMtpDatabase implements AutoCloseable {
  private static final int[] AUDIO_PROPERTIES;
  
  private static final boolean DEBUG;
  
  private static final int[] DEVICE_PROPERTIES;
  
  private static final int[] FILE_PROPERTIES;
  
  private static final int[] IMAGE_PROPERTIES;
  
  private static final int MAX_THUMB_SIZE = 204800;
  
  private static final String NO_MEDIA = ".nomedia";
  
  private static final String PATH_WHERE = "_data=?";
  
  private static final int[] PLAYBACK_FORMATS;
  
  private static final String TAG = MtpDatabase.class.getSimpleName();
  
  private static final int[] VIDEO_PROPERTIES;
  
  private int mBatteryLevel;
  
  private BroadcastReceiver mBatteryReceiver;
  
  private int mBatteryScale;
  
  private final CloseGuard mCloseGuard;
  
  private final AtomicBoolean mClosed;
  
  private final Context mContext;
  
  private SharedPreferences mDeviceProperties;
  
  private int mDeviceType;
  
  private MtpStorageManager mManager;
  
  private final ContentProviderClient mMediaProvider;
  
  private long mNativeContext;
  
  private final SparseArray<MtpPropertyGroup> mPropertyGroupsByFormat;
  
  private final SparseArray<MtpPropertyGroup> mPropertyGroupsByProperty;
  
  private MtpServer mServer;
  
  private final HashMap<String, MtpStorage> mStorageMap;
  
  static {
    DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
    System.loadLibrary("media_jni");
    PLAYBACK_FORMATS = new int[] { 
        12288, 12289, 12292, 12293, 12296, 12297, 12299, 14337, 14338, 14340, 
        14343, 14344, 14347, 14349, 47361, 47362, 47363, 47490, 47491, 47492, 
        47621, 47632, 47633, 47636, 47746, 47366, 14353, 14354 };
    FILE_PROPERTIES = new int[] { 
        56321, 56322, 56323, 56324, 56327, 56329, 56385, 56331, 56388, 56544, 
        56398 };
    AUDIO_PROPERTIES = new int[] { 
        56390, 56474, 56475, 56459, 56473, 56457, 56460, 56470, 56985, 56978, 
        56986, 56980, 56979 };
    VIDEO_PROPERTIES = new int[] { 56390, 56474, 56457, 56392 };
    IMAGE_PROPERTIES = new int[] { 56392 };
    DEVICE_PROPERTIES = new int[] { 54273, 54274, 20483, 20481, 54279 };
  }
  
  private int[] getSupportedObjectProperties(int paramInt) {
    // Byte code:
    //   0: iload_1
    //   1: sipush #12296
    //   4: if_icmpeq -> 170
    //   7: iload_1
    //   8: sipush #12297
    //   11: if_icmpeq -> 170
    //   14: iload_1
    //   15: sipush #12299
    //   18: if_icmpeq -> 139
    //   21: iload_1
    //   22: sipush #14337
    //   25: if_icmpeq -> 108
    //   28: iload_1
    //   29: sipush #14340
    //   32: if_icmpeq -> 108
    //   35: iload_1
    //   36: sipush #14343
    //   39: if_icmpeq -> 108
    //   42: iload_1
    //   43: sipush #14347
    //   46: if_icmpeq -> 108
    //   49: iload_1
    //   50: ldc_w 47489
    //   53: if_icmpeq -> 139
    //   56: iload_1
    //   57: ldc 47492
    //   59: if_icmpeq -> 139
    //   62: iload_1
    //   63: sipush #14353
    //   66: if_icmpeq -> 108
    //   69: iload_1
    //   70: sipush #14354
    //   73: if_icmpeq -> 108
    //   76: iload_1
    //   77: tableswitch default -> 104, 47361 -> 170, 47362 -> 170, 47363 -> 170
    //   104: getstatic android/mtp/MtpDatabase.FILE_PROPERTIES : [I
    //   107: areturn
    //   108: getstatic android/mtp/MtpDatabase.FILE_PROPERTIES : [I
    //   111: invokestatic stream : ([I)Ljava/util/stream/IntStream;
    //   114: astore_2
    //   115: getstatic android/mtp/MtpDatabase.IMAGE_PROPERTIES : [I
    //   118: astore_3
    //   119: aload_3
    //   120: invokestatic stream : ([I)Ljava/util/stream/IntStream;
    //   123: astore_3
    //   124: aload_2
    //   125: aload_3
    //   126: invokestatic concat : (Ljava/util/stream/IntStream;Ljava/util/stream/IntStream;)Ljava/util/stream/IntStream;
    //   129: astore_2
    //   130: aload_2
    //   131: invokeinterface toArray : ()[I
    //   136: astore_2
    //   137: aload_2
    //   138: areturn
    //   139: getstatic android/mtp/MtpDatabase.FILE_PROPERTIES : [I
    //   142: invokestatic stream : ([I)Ljava/util/stream/IntStream;
    //   145: astore_2
    //   146: getstatic android/mtp/MtpDatabase.VIDEO_PROPERTIES : [I
    //   149: astore_3
    //   150: aload_3
    //   151: invokestatic stream : ([I)Ljava/util/stream/IntStream;
    //   154: astore_3
    //   155: aload_2
    //   156: aload_3
    //   157: invokestatic concat : (Ljava/util/stream/IntStream;Ljava/util/stream/IntStream;)Ljava/util/stream/IntStream;
    //   160: astore_2
    //   161: aload_2
    //   162: invokeinterface toArray : ()[I
    //   167: astore_2
    //   168: aload_2
    //   169: areturn
    //   170: getstatic android/mtp/MtpDatabase.FILE_PROPERTIES : [I
    //   173: invokestatic stream : ([I)Ljava/util/stream/IntStream;
    //   176: astore_2
    //   177: getstatic android/mtp/MtpDatabase.AUDIO_PROPERTIES : [I
    //   180: astore_3
    //   181: aload_3
    //   182: invokestatic stream : ([I)Ljava/util/stream/IntStream;
    //   185: astore_3
    //   186: aload_2
    //   187: aload_3
    //   188: invokestatic concat : (Ljava/util/stream/IntStream;Ljava/util/stream/IntStream;)Ljava/util/stream/IntStream;
    //   191: astore_2
    //   192: aload_2
    //   193: invokeinterface toArray : ()[I
    //   198: astore_2
    //   199: aload_2
    //   200: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #209	-> 0
    //   #231	-> 104
    //   #228	-> 108
    //   #229	-> 119
    //   #228	-> 124
    //   #229	-> 130
    //   #228	-> 137
    //   #220	-> 139
    //   #221	-> 150
    //   #220	-> 155
    //   #221	-> 161
    //   #220	-> 168
    //   #215	-> 170
    //   #216	-> 181
    //   #215	-> 186
    //   #216	-> 192
    //   #215	-> 199
  }
  
  public static Uri getObjectPropertiesUri(int paramInt, String paramString) {
    if (paramInt != 12296 && paramInt != 12297)
      if (paramInt != 12299) {
        if (paramInt != 14337 && paramInt != 14340 && paramInt != 14343 && paramInt != 14347) {
          if (paramInt != 47489 && paramInt != 47492) {
            if (paramInt != 14353 && paramInt != 14354) {
              switch (paramInt) {
                default:
                  return MediaStore.Files.getContentUri(paramString);
                case 47361:
                case 47362:
                case 47363:
                  break;
              } 
            } else {
              return MediaStore.Images.Media.getContentUri(paramString);
            } 
          } else {
            return MediaStore.Video.Media.getContentUri(paramString);
          } 
        } else {
          return MediaStore.Images.Media.getContentUri(paramString);
        } 
      } else {
        return MediaStore.Video.Media.getContentUri(paramString);
      }  
    return MediaStore.Audio.Media.getContentUri(paramString);
  }
  
  private int[] getSupportedDeviceProperties() {
    return DEVICE_PROPERTIES;
  }
  
  private int[] getSupportedPlaybackFormats() {
    return PLAYBACK_FORMATS;
  }
  
  private int[] getSupportedCaptureFormats() {
    return null;
  }
  
  public MtpDatabase(Context paramContext, String[] paramArrayOfString) {
    super(paramContext);
    HashSet<String> hashSet;
    this.mClosed = new AtomicBoolean();
    this.mCloseGuard = CloseGuard.get();
    this.mStorageMap = new HashMap<>();
    this.mPropertyGroupsByProperty = new SparseArray();
    this.mPropertyGroupsByFormat = new SparseArray();
    this.mBatteryReceiver = new BroadcastReceiver() {
        final MtpDatabase this$0;
        
        public void onReceive(Context param1Context, Intent param1Intent) {
          String str = param1Intent.getAction();
          if (str.equals("android.intent.action.BATTERY_CHANGED")) {
            MtpDatabase.access$002(MtpDatabase.this, param1Intent.getIntExtra("scale", 0));
            int i = param1Intent.getIntExtra("level", 0);
            if (i != MtpDatabase.this.mBatteryLevel) {
              MtpDatabase.access$102(MtpDatabase.this, i);
              if (MtpDatabase.this.mServer != null)
                MtpDatabase.this.mServer.sendDevicePropertyChanged(20481); 
            } 
          } 
        }
      };
    native_setup();
    Objects.requireNonNull(paramContext);
    this.mContext = paramContext;
    ContentResolver contentResolver = paramContext.getContentResolver();
    this.mMediaProvider = contentResolver.acquireContentProviderClient("media");
    MtpStorageManager.MtpNotifier mtpNotifier = new MtpStorageManager.MtpNotifier() {
        final MtpDatabase this$0;
        
        public void sendObjectAdded(int param1Int) {
          if (MtpDatabase.this.mServer != null)
            MtpDatabase.this.mServer.sendObjectAdded(param1Int); 
        }
        
        public void sendObjectRemoved(int param1Int) {
          if (MtpDatabase.this.mServer != null)
            MtpDatabase.this.mServer.sendObjectRemoved(param1Int); 
        }
        
        public void sendObjectInfoChanged(int param1Int) {
          if (MtpDatabase.this.mServer != null)
            MtpDatabase.this.mServer.sendObjectInfoChanged(param1Int); 
        }
      };
    if (paramArrayOfString == null) {
      paramArrayOfString = null;
    } else {
      hashSet = Sets.newHashSet((Object[])paramArrayOfString);
    } 
    this.mManager = new MtpStorageManager(mtpNotifier, hashSet);
    initDeviceProperties(paramContext);
    this.mDeviceType = SystemProperties.getInt("sys.usb.mtp.device_type", 0);
    this.mCloseGuard.open("close");
  }
  
  public void setServer(MtpServer paramMtpServer) {
    this.mServer = paramMtpServer;
    try {
      this.mContext.unregisterReceiver(this.mBatteryReceiver);
    } catch (IllegalArgumentException illegalArgumentException) {}
    if (paramMtpServer != null)
      this.mContext.registerReceiver(this.mBatteryReceiver, new IntentFilter("android.intent.action.BATTERY_CHANGED")); 
  }
  
  public Context getContext() {
    return this.mContext;
  }
  
  public void close() {
    this.mManager.close();
    this.mCloseGuard.close();
    if (this.mClosed.compareAndSet(false, true)) {
      ContentProviderClient contentProviderClient = this.mMediaProvider;
      if (contentProviderClient != null)
        contentProviderClient.close(); 
      native_finalize();
    } 
  }
  
  public void releaseScanThread() {
    this.mHandler.sendEmptyMessage(2);
  }
  
  protected void finalize() throws Throwable {
    try {
      if (this.mCloseGuard != null)
        this.mCloseGuard.warnIfOpen(); 
      close();
      super.finalize();
      return;
    } finally {
      super.finalize();
    } 
  }
  
  public void addStorage(StorageVolume paramStorageVolume) {
    MtpStorage mtpStorage = this.mManager.addMtpStorage(paramStorageVolume);
    this.mStorageMap.put(paramStorageVolume.getPath(), mtpStorage);
    MtpServer mtpServer = this.mServer;
    if (mtpServer != null)
      mtpServer.addStorage(mtpStorage); 
  }
  
  public void removeStorage(StorageVolume paramStorageVolume) {
    MtpStorage mtpStorage = this.mStorageMap.get(paramStorageVolume.getPath());
    if (mtpStorage == null)
      return; 
    MtpServer mtpServer = this.mServer;
    if (mtpServer != null)
      mtpServer.removeStorage(mtpStorage); 
    this.mManager.removeMtpStorage(mtpStorage);
    this.mStorageMap.remove(paramStorageVolume.getPath());
  }
  
  private void initDeviceProperties(Context paramContext) {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: ldc_w 'device-properties'
    //   5: iconst_0
    //   6: invokevirtual getSharedPreferences : (Ljava/lang/String;I)Landroid/content/SharedPreferences;
    //   9: putfield mDeviceProperties : Landroid/content/SharedPreferences;
    //   12: aload_1
    //   13: ldc_w 'device-properties'
    //   16: invokevirtual getDatabasePath : (Ljava/lang/String;)Ljava/io/File;
    //   19: astore_2
    //   20: aload_2
    //   21: invokevirtual exists : ()Z
    //   24: ifeq -> 378
    //   27: aconst_null
    //   28: astore_2
    //   29: aconst_null
    //   30: astore_3
    //   31: aconst_null
    //   32: astore #4
    //   34: aconst_null
    //   35: astore #5
    //   37: aconst_null
    //   38: astore #6
    //   40: aload #4
    //   42: astore #7
    //   44: aload #5
    //   46: astore #8
    //   48: aload_1
    //   49: ldc_w 'device-properties'
    //   52: iconst_0
    //   53: aconst_null
    //   54: invokevirtual openOrCreateDatabase : (Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase$CursorFactory;)Landroid/database/sqlite/SQLiteDatabase;
    //   57: astore #9
    //   59: aload #6
    //   61: astore_2
    //   62: aload #9
    //   64: ifnull -> 277
    //   67: aload #9
    //   69: astore_3
    //   70: aload #4
    //   72: astore #7
    //   74: aload #9
    //   76: astore_2
    //   77: aload #5
    //   79: astore #8
    //   81: aload #9
    //   83: ldc_w 'properties'
    //   86: iconst_3
    //   87: anewarray java/lang/String
    //   90: dup
    //   91: iconst_0
    //   92: ldc_w '_id'
    //   95: aastore
    //   96: dup
    //   97: iconst_1
    //   98: ldc_w 'code'
    //   101: aastore
    //   102: dup
    //   103: iconst_2
    //   104: ldc_w 'value'
    //   107: aastore
    //   108: aconst_null
    //   109: aconst_null
    //   110: aconst_null
    //   111: aconst_null
    //   112: aconst_null
    //   113: invokevirtual query : (Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   116: astore #4
    //   118: aload #4
    //   120: astore_2
    //   121: aload #4
    //   123: ifnull -> 277
    //   126: aload #9
    //   128: astore_3
    //   129: aload #4
    //   131: astore #7
    //   133: aload #9
    //   135: astore_2
    //   136: aload #4
    //   138: astore #8
    //   140: aload_0
    //   141: getfield mDeviceProperties : Landroid/content/SharedPreferences;
    //   144: invokeinterface edit : ()Landroid/content/SharedPreferences$Editor;
    //   149: astore #5
    //   151: aload #9
    //   153: astore_3
    //   154: aload #4
    //   156: astore #7
    //   158: aload #9
    //   160: astore_2
    //   161: aload #4
    //   163: astore #8
    //   165: aload #4
    //   167: invokeinterface moveToNext : ()Z
    //   172: ifeq -> 252
    //   175: aload #9
    //   177: astore_3
    //   178: aload #4
    //   180: astore #7
    //   182: aload #9
    //   184: astore_2
    //   185: aload #4
    //   187: astore #8
    //   189: aload #4
    //   191: iconst_1
    //   192: invokeinterface getString : (I)Ljava/lang/String;
    //   197: astore #6
    //   199: aload #9
    //   201: astore_3
    //   202: aload #4
    //   204: astore #7
    //   206: aload #9
    //   208: astore_2
    //   209: aload #4
    //   211: astore #8
    //   213: aload #4
    //   215: iconst_2
    //   216: invokeinterface getString : (I)Ljava/lang/String;
    //   221: astore #10
    //   223: aload #9
    //   225: astore_3
    //   226: aload #4
    //   228: astore #7
    //   230: aload #9
    //   232: astore_2
    //   233: aload #4
    //   235: astore #8
    //   237: aload #5
    //   239: aload #6
    //   241: aload #10
    //   243: invokeinterface putString : (Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
    //   248: pop
    //   249: goto -> 151
    //   252: aload #9
    //   254: astore_3
    //   255: aload #4
    //   257: astore #7
    //   259: aload #9
    //   261: astore_2
    //   262: aload #4
    //   264: astore #8
    //   266: aload #5
    //   268: invokeinterface commit : ()Z
    //   273: pop
    //   274: aload #4
    //   276: astore_2
    //   277: aload_2
    //   278: ifnull -> 287
    //   281: aload_2
    //   282: invokeinterface close : ()V
    //   287: aload #9
    //   289: ifnull -> 345
    //   292: aload #9
    //   294: astore_2
    //   295: aload_2
    //   296: invokevirtual close : ()V
    //   299: goto -> 345
    //   302: astore_1
    //   303: goto -> 356
    //   306: astore #9
    //   308: aload_2
    //   309: astore_3
    //   310: aload #8
    //   312: astore #7
    //   314: getstatic android/mtp/MtpDatabase.TAG : Ljava/lang/String;
    //   317: ldc_w 'failed to migrate device properties'
    //   320: aload #9
    //   322: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   325: pop
    //   326: aload #8
    //   328: ifnull -> 338
    //   331: aload #8
    //   333: invokeinterface close : ()V
    //   338: aload_2
    //   339: ifnull -> 345
    //   342: goto -> 295
    //   345: aload_1
    //   346: ldc_w 'device-properties'
    //   349: invokevirtual deleteDatabase : (Ljava/lang/String;)Z
    //   352: pop
    //   353: goto -> 378
    //   356: aload #7
    //   358: ifnull -> 368
    //   361: aload #7
    //   363: invokeinterface close : ()V
    //   368: aload_3
    //   369: ifnull -> 376
    //   372: aload_3
    //   373: invokevirtual close : ()V
    //   376: aload_1
    //   377: athrow
    //   378: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #405	-> 0
    //   #406	-> 0
    //   #408	-> 12
    //   #410	-> 20
    //   #413	-> 27
    //   #414	-> 31
    //   #416	-> 40
    //   #417	-> 59
    //   #418	-> 67
    //   #420	-> 118
    //   #421	-> 126
    //   #422	-> 151
    //   #423	-> 175
    //   #424	-> 199
    //   #425	-> 223
    //   #426	-> 249
    //   #427	-> 252
    //   #433	-> 277
    //   #434	-> 287
    //   #433	-> 302
    //   #430	-> 306
    //   #431	-> 308
    //   #433	-> 326
    //   #434	-> 338
    //   #436	-> 345
    //   #433	-> 356
    //   #434	-> 368
    //   #435	-> 376
    //   #438	-> 378
    // Exception table:
    //   from	to	target	type
    //   48	59	306	java/lang/Exception
    //   48	59	302	finally
    //   81	118	306	java/lang/Exception
    //   81	118	302	finally
    //   140	151	306	java/lang/Exception
    //   140	151	302	finally
    //   165	175	306	java/lang/Exception
    //   165	175	302	finally
    //   189	199	306	java/lang/Exception
    //   189	199	302	finally
    //   213	223	306	java/lang/Exception
    //   213	223	302	finally
    //   237	249	306	java/lang/Exception
    //   237	249	302	finally
    //   266	274	306	java/lang/Exception
    //   266	274	302	finally
    //   314	326	302	finally
  }
  
  public int beginSendObject(String paramString, int paramInt1, int paramInt2, int paramInt3) {
    MtpStorageManager.MtpObject mtpObject;
    MtpStorageManager mtpStorageManager = this.mManager;
    if (paramInt2 == 0) {
      mtpObject = mtpStorageManager.getStorageRoot(paramInt3);
    } else {
      mtpObject = mtpObject.getObject(paramInt2);
    } 
    if (mtpObject == null)
      return -1; 
    Path path = Paths.get(paramString, new String[0]);
    return this.mManager.beginSendObject(mtpObject, path.getFileName().toString(), paramInt1);
  }
  
  private void endSendObject(int paramInt, boolean paramBoolean) {
    MtpStorageManager.MtpObject mtpObject = this.mManager.getObject(paramInt);
    if (mtpObject == null || !this.mManager.endSendObject(mtpObject, paramBoolean)) {
      Log.e(TAG, "Failed to successfully end send object");
      return;
    } 
    if (paramBoolean) {
      rescanFile(mtpObject.getPath().toString(), 0, 0);
      updateMediaStore(this.mContext, mtpObject.getPath().toFile());
    } 
  }
  
  private void rescanFile(String paramString, int paramInt1, int paramInt2) {
    Message message = this.mHandler.obtainMessage(0);
    message.obj = new OplusBaseMtpDatabase.HandlerParams(this, paramString);
    this.mHandler.sendMessage(message);
    String str = TAG;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("rescanFile sendMessage path");
    stringBuilder.append(paramString);
    Log.e(str, stringBuilder.toString());
  }
  
  private int[] getObjectList(int paramInt1, int paramInt2, int paramInt3) {
    List<MtpStorageManager.MtpObject> list = this.mManager.getObjects(paramInt3, paramInt2, paramInt1);
    if (list == null)
      return null; 
    int[] arrayOfInt = new int[list.size()];
    for (paramInt1 = 0; paramInt1 < list.size(); paramInt1++)
      arrayOfInt[paramInt1] = ((MtpStorageManager.MtpObject)list.get(paramInt1)).getId(); 
    return arrayOfInt;
  }
  
  public int getNumObjects(int paramInt1, int paramInt2, int paramInt3) {
    List<MtpStorageManager.MtpObject> list = this.mManager.getObjects(paramInt3, paramInt2, paramInt1);
    if (list == null)
      return -1; 
    return list.size();
  }
  
  private MtpPropertyList getObjectPropertyList(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    // Byte code:
    //   0: iload_1
    //   1: istore #6
    //   3: iload_2
    //   4: istore #7
    //   6: iload_3
    //   7: ifne -> 37
    //   10: iload #4
    //   12: ifne -> 26
    //   15: new android/mtp/MtpPropertyList
    //   18: dup
    //   19: sipush #8198
    //   22: invokespecial <init> : (I)V
    //   25: areturn
    //   26: new android/mtp/MtpPropertyList
    //   29: dup
    //   30: ldc_w 43015
    //   33: invokespecial <init> : (I)V
    //   36: areturn
    //   37: iload #6
    //   39: istore_1
    //   40: iload #5
    //   42: istore_2
    //   43: iload #5
    //   45: iconst_m1
    //   46: if_icmpne -> 70
    //   49: iload #6
    //   51: ifeq -> 66
    //   54: iload #6
    //   56: istore_1
    //   57: iload #5
    //   59: istore_2
    //   60: iload #6
    //   62: iconst_m1
    //   63: if_icmpne -> 70
    //   66: iconst_m1
    //   67: istore_1
    //   68: iconst_0
    //   69: istore_2
    //   70: iload_2
    //   71: ifeq -> 90
    //   74: iload_2
    //   75: iconst_1
    //   76: if_icmpeq -> 90
    //   79: new android/mtp/MtpPropertyList
    //   82: dup
    //   83: ldc_w 43016
    //   86: invokespecial <init> : (I)V
    //   89: areturn
    //   90: aconst_null
    //   91: astore #8
    //   93: aconst_null
    //   94: astore #9
    //   96: iload_1
    //   97: iconst_m1
    //   98: if_icmpne -> 138
    //   101: aload_0
    //   102: getfield mManager : Landroid/mtp/MtpStorageManager;
    //   105: iconst_0
    //   106: iload #7
    //   108: iconst_m1
    //   109: invokevirtual getObjects : (III)Ljava/util/List;
    //   112: astore #8
    //   114: aload #8
    //   116: astore #10
    //   118: aload #9
    //   120: astore #11
    //   122: aload #8
    //   124: ifnonnull -> 207
    //   127: new android/mtp/MtpPropertyList
    //   130: dup
    //   131: sipush #8201
    //   134: invokespecial <init> : (I)V
    //   137: areturn
    //   138: aload #8
    //   140: astore #10
    //   142: aload #9
    //   144: astore #11
    //   146: iload_1
    //   147: ifeq -> 207
    //   150: aload_0
    //   151: getfield mManager : Landroid/mtp/MtpStorageManager;
    //   154: iload_1
    //   155: invokevirtual getObject : (I)Landroid/mtp/MtpStorageManager$MtpObject;
    //   158: astore #12
    //   160: aload #12
    //   162: ifnonnull -> 176
    //   165: new android/mtp/MtpPropertyList
    //   168: dup
    //   169: sipush #8201
    //   172: invokespecial <init> : (I)V
    //   175: areturn
    //   176: aload #12
    //   178: invokevirtual getFormat : ()I
    //   181: iload #7
    //   183: if_icmpeq -> 199
    //   186: aload #8
    //   188: astore #10
    //   190: aload #9
    //   192: astore #11
    //   194: iload #7
    //   196: ifne -> 207
    //   199: aload #12
    //   201: astore #11
    //   203: aload #8
    //   205: astore #10
    //   207: iload_1
    //   208: ifeq -> 219
    //   211: iload_1
    //   212: istore #4
    //   214: iload_2
    //   215: iconst_1
    //   216: if_icmpne -> 263
    //   219: iload_1
    //   220: istore_2
    //   221: iload_1
    //   222: ifne -> 227
    //   225: iconst_m1
    //   226: istore_2
    //   227: aload_0
    //   228: getfield mManager : Landroid/mtp/MtpStorageManager;
    //   231: iload_2
    //   232: iload #7
    //   234: iconst_m1
    //   235: invokevirtual getObjects : (III)Ljava/util/List;
    //   238: astore #8
    //   240: iload_2
    //   241: istore #4
    //   243: aload #8
    //   245: astore #10
    //   247: aload #8
    //   249: ifnonnull -> 263
    //   252: new android/mtp/MtpPropertyList
    //   255: dup
    //   256: sipush #8201
    //   259: invokespecial <init> : (I)V
    //   262: areturn
    //   263: aload #10
    //   265: astore #8
    //   267: aload #10
    //   269: ifnonnull -> 281
    //   272: new java/util/ArrayList
    //   275: dup
    //   276: invokespecial <init> : ()V
    //   279: astore #8
    //   281: aload #11
    //   283: ifnull -> 296
    //   286: aload #8
    //   288: aload #11
    //   290: invokeinterface add : (Ljava/lang/Object;)Z
    //   295: pop
    //   296: new android/mtp/MtpPropertyList
    //   299: dup
    //   300: sipush #8193
    //   303: invokespecial <init> : (I)V
    //   306: astore #9
    //   308: aload #8
    //   310: invokeinterface iterator : ()Ljava/util/Iterator;
    //   315: astore #12
    //   317: iload #7
    //   319: istore_1
    //   320: aload #12
    //   322: invokeinterface hasNext : ()Z
    //   327: ifeq -> 528
    //   330: aload #12
    //   332: invokeinterface next : ()Ljava/lang/Object;
    //   337: checkcast android/mtp/MtpStorageManager$MtpObject
    //   340: astore #8
    //   342: iload_3
    //   343: iconst_m1
    //   344: if_icmpne -> 433
    //   347: iload_1
    //   348: istore_2
    //   349: iload_1
    //   350: ifne -> 374
    //   353: iload_1
    //   354: istore_2
    //   355: iload #4
    //   357: ifeq -> 374
    //   360: iload_1
    //   361: istore_2
    //   362: iload #4
    //   364: iconst_m1
    //   365: if_icmpeq -> 374
    //   368: aload #8
    //   370: invokevirtual getFormat : ()I
    //   373: istore_2
    //   374: aload_0
    //   375: getfield mPropertyGroupsByFormat : Landroid/util/SparseArray;
    //   378: iload_2
    //   379: invokevirtual get : (I)Ljava/lang/Object;
    //   382: checkcast android/mtp/MtpPropertyGroup
    //   385: astore #11
    //   387: iload_2
    //   388: istore #5
    //   390: aload #11
    //   392: astore #10
    //   394: aload #11
    //   396: ifnonnull -> 487
    //   399: aload_0
    //   400: iload_2
    //   401: invokespecial getSupportedObjectProperties : (I)[I
    //   404: astore #10
    //   406: new android/mtp/MtpPropertyGroup
    //   409: dup
    //   410: aload #10
    //   412: invokespecial <init> : ([I)V
    //   415: astore #10
    //   417: aload_0
    //   418: getfield mPropertyGroupsByFormat : Landroid/util/SparseArray;
    //   421: iload_2
    //   422: aload #10
    //   424: invokevirtual put : (ILjava/lang/Object;)V
    //   427: iload_2
    //   428: istore #5
    //   430: goto -> 487
    //   433: aload_0
    //   434: getfield mPropertyGroupsByProperty : Landroid/util/SparseArray;
    //   437: iload_3
    //   438: invokevirtual get : (I)Ljava/lang/Object;
    //   441: checkcast android/mtp/MtpPropertyGroup
    //   444: astore #11
    //   446: iload_1
    //   447: istore #5
    //   449: aload #11
    //   451: astore #10
    //   453: aload #11
    //   455: ifnonnull -> 487
    //   458: new android/mtp/MtpPropertyGroup
    //   461: dup
    //   462: iconst_1
    //   463: newarray int
    //   465: dup
    //   466: iconst_0
    //   467: iload_3
    //   468: iastore
    //   469: invokespecial <init> : ([I)V
    //   472: astore #10
    //   474: aload_0
    //   475: getfield mPropertyGroupsByProperty : Landroid/util/SparseArray;
    //   478: iload_3
    //   479: aload #10
    //   481: invokevirtual put : (ILjava/lang/Object;)V
    //   484: iload_1
    //   485: istore #5
    //   487: aload #10
    //   489: aload_0
    //   490: getfield mMediaProvider : Landroid/content/ContentProviderClient;
    //   493: aload #8
    //   495: invokevirtual getVolumeName : ()Ljava/lang/String;
    //   498: aload #8
    //   500: aload #9
    //   502: invokevirtual getPropertyList : (Landroid/content/ContentProviderClient;Ljava/lang/String;Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpPropertyList;)I
    //   505: istore_1
    //   506: iload_1
    //   507: sipush #8193
    //   510: if_icmpeq -> 522
    //   513: new android/mtp/MtpPropertyList
    //   516: dup
    //   517: iload_1
    //   518: invokespecial <init> : (I)V
    //   521: areturn
    //   522: iload #5
    //   524: istore_1
    //   525: goto -> 320
    //   528: aload #9
    //   530: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #519	-> 0
    //   #520	-> 10
    //   #521	-> 15
    //   #523	-> 26
    //   #525	-> 37
    //   #527	-> 66
    //   #528	-> 68
    //   #530	-> 70
    //   #533	-> 79
    //   #535	-> 90
    //   #536	-> 93
    //   #537	-> 96
    //   #539	-> 101
    //   #540	-> 114
    //   #541	-> 127
    //   #543	-> 138
    //   #545	-> 150
    //   #546	-> 160
    //   #547	-> 165
    //   #549	-> 176
    //   #550	-> 199
    //   #553	-> 207
    //   #554	-> 219
    //   #555	-> 225
    //   #558	-> 227
    //   #560	-> 240
    //   #561	-> 252
    //   #564	-> 263
    //   #565	-> 272
    //   #567	-> 281
    //   #568	-> 286
    //   #571	-> 296
    //   #573	-> 308
    //   #574	-> 342
    //   #575	-> 347
    //   #577	-> 368
    //   #581	-> 374
    //   #582	-> 387
    //   #583	-> 399
    //   #584	-> 406
    //   #585	-> 417
    //   #586	-> 427
    //   #589	-> 433
    //   #590	-> 446
    //   #591	-> 458
    //   #592	-> 458
    //   #593	-> 474
    //   #596	-> 487
    //   #597	-> 506
    //   #598	-> 513
    //   #600	-> 522
    //   #601	-> 528
  }
  
  private int renameFile(int paramInt, String paramString) {
    MtpStorageManager.MtpObject mtpObject = this.mManager.getObject(paramInt);
    if (mtpObject == null)
      return 8201; 
    Path path1 = mtpObject.getPath();
    if (!this.mManager.beginRenameObject(mtpObject, paramString))
      return 8194; 
    Path path2 = mtpObject.getPath();
    boolean bool = path1.toFile().renameTo(path2.toFile());
    try {
      Os.access(path1.toString(), OsConstants.F_OK);
      Os.access(path2.toString(), OsConstants.F_OK);
    } catch (ErrnoException errnoException) {}
    if (!this.mManager.endRenameObject(mtpObject, path1.getFileName().toString(), bool))
      Log.e(TAG, "Failed to end rename object"); 
    if (!bool)
      return 8194; 
    updateMediaStore(this.mContext, path1.toFile());
    updateMediaStore(this.mContext, path2.toFile());
    return 8193;
  }
  
  private int beginMoveObject(int paramInt1, int paramInt2, int paramInt3) {
    MtpStorageManager.MtpObject mtpObject2, mtpObject1 = this.mManager.getObject(paramInt1);
    if (paramInt2 == 0) {
      mtpObject2 = this.mManager.getStorageRoot(paramInt3);
    } else {
      mtpObject2 = this.mManager.getObject(paramInt2);
    } 
    if (mtpObject1 == null || mtpObject2 == null)
      return 8201; 
    boolean bool = this.mManager.beginMoveObject(mtpObject1, mtpObject2);
    if (bool) {
      paramInt1 = 8193;
    } else {
      paramInt1 = 8194;
    } 
    return paramInt1;
  }
  
  private void endMoveObject(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, boolean paramBoolean) {
    MtpStorageManager.MtpObject mtpObject1, mtpObject2;
    if (paramInt1 == 0) {
      mtpObject1 = this.mManager.getStorageRoot(paramInt3);
    } else {
      mtpObject1 = this.mManager.getObject(paramInt1);
    } 
    if (paramInt2 == 0) {
      mtpObject2 = this.mManager.getStorageRoot(paramInt4);
    } else {
      mtpObject2 = this.mManager.getObject(paramInt2);
    } 
    MtpStorageManager.MtpObject mtpObject3 = this.mManager.getObject(paramInt5);
    String str = mtpObject3.getName();
    if (mtpObject2 != null && mtpObject1 != null) {
      MtpStorageManager mtpStorageManager = this.mManager;
      if (mtpStorageManager.endMoveObject(mtpObject1, mtpObject2, str, paramBoolean)) {
        MtpStorageManager.MtpObject mtpObject = this.mManager.getObject(paramInt5);
        if (!paramBoolean || mtpObject == null)
          return; 
        Path path2 = mtpObject2.getPath().resolve(str);
        Path path1 = mtpObject1.getPath().resolve(str);
        updateMediaStore(this.mContext, path1.toFile());
        updateMediaStore(this.mContext, path2.toFile());
        return;
      } 
    } 
    Log.e(TAG, "Failed to end move object");
  }
  
  private int beginCopyObject(int paramInt1, int paramInt2, int paramInt3) {
    MtpStorageManager.MtpObject mtpObject2, mtpObject1 = this.mManager.getObject(paramInt1);
    if (paramInt2 == 0) {
      mtpObject2 = this.mManager.getStorageRoot(paramInt3);
    } else {
      mtpObject2 = this.mManager.getObject(paramInt2);
    } 
    if (mtpObject1 == null || mtpObject2 == null)
      return 8201; 
    return this.mManager.beginCopyObject(mtpObject1, mtpObject2);
  }
  
  private void endCopyObject(int paramInt, boolean paramBoolean) {
    MtpStorageManager.MtpObject mtpObject = this.mManager.getObject(paramInt);
    if (mtpObject == null || !this.mManager.endCopyObject(mtpObject, paramBoolean)) {
      Log.e(TAG, "Failed to end copy object");
      return;
    } 
    if (!paramBoolean)
      return; 
    updateMediaStore(this.mContext, mtpObject.getPath().toFile());
  }
  
  private static void updateMediaStore(Context paramContext, File paramFile) {
    ContentResolver contentResolver = paramContext.getContentResolver();
    if (!paramFile.isDirectory() && paramFile.getName().toLowerCase(Locale.ROOT).endsWith(".nomedia")) {
      MediaStore.scanFile(contentResolver, paramFile.getParentFile());
    } else {
      MediaStore.scanFile(contentResolver, paramFile);
    } 
  }
  
  private int setObjectProperty(int paramInt1, int paramInt2, long paramLong, String paramString) {
    if (paramInt2 != 56327)
      return 43018; 
    return renameFile(paramInt1, paramString);
  }
  
  private int getDeviceProperty(int paramInt, long[] paramArrayOflong, char[] paramArrayOfchar) {
    String str2;
    WindowManager windowManager;
    Display display;
    StringBuilder stringBuilder1;
    String str1;
    StringBuilder stringBuilder2;
    int i, j;
    switch (paramInt) {
      default:
        return 8202;
      case 54279:
        paramArrayOflong[0] = this.mDeviceType;
        return 8193;
      case 54273:
      case 54274:
        str2 = this.mDeviceProperties.getString(Integer.toString(paramInt), "");
        i = str2.length();
        j = i;
        if (i > 255)
          j = 255; 
        str2.getChars(0, j, paramArrayOfchar, 0);
        paramArrayOfchar[j] = Character.MIN_VALUE;
        if (j > 0) {
          String str3 = TAG;
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("getDeviceProperty  property = ");
          stringBuilder.append(Integer.toHexString(paramInt));
          Log.i(str3, stringBuilder.toString());
          String str4 = TAG;
          stringBuilder2 = new StringBuilder();
          stringBuilder2.append("getDeviceProperty  value = ");
          stringBuilder2.append(str2);
          stringBuilder2.append(", length = ");
          stringBuilder2.append(j);
          Log.i(str4, stringBuilder2.toString());
          return 8193;
        } 
        if (getOplusMarketName(j, paramInt) != null) {
          str2 = getOplusMarketName(j, paramInt);
          paramInt = str2.length();
          str2.getChars(0, paramInt, (char[])stringBuilder2, 0);
          stringBuilder2[paramInt] = false;
        } 
        return 8193;
      case 20483:
        windowManager = (WindowManager)this.mContext.getSystemService("window");
        display = windowManager.getDefaultDisplay();
        j = display.getMaximumSizeDimension();
        paramInt = display.getMaximumSizeDimension();
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append(Integer.toString(j));
        stringBuilder1.append("x");
        stringBuilder1.append(Integer.toString(paramInt));
        str1 = stringBuilder1.toString();
        str1.getChars(0, str1.length(), (char[])stringBuilder2, 0);
        stringBuilder2[str1.length()] = false;
        return 8193;
      case 20481:
        break;
    } 
    str1[0] = this.mBatteryLevel;
    str1[1] = this.mBatteryScale;
    return 8193;
  }
  
  private int setDeviceProperty(int paramInt, long paramLong, String paramString) {
    switch (paramInt) {
      default:
        return 8202;
      case 54273:
      case 54274:
        break;
    } 
    SharedPreferences.Editor editor = this.mDeviceProperties.edit();
    editor.putString(Integer.toString(paramInt), paramString);
    if (editor.commit()) {
      paramInt = 8193;
    } else {
      paramInt = 8194;
    } 
    return paramInt;
  }
  
  private boolean getObjectInfo(int paramInt, int[] paramArrayOfint, char[] paramArrayOfchar, long[] paramArrayOflong) {
    MtpStorageManager.MtpObject mtpObject = this.mManager.getObject(paramInt);
    if (mtpObject == null)
      return false; 
    paramArrayOfint[0] = mtpObject.getStorageId();
    paramArrayOfint[1] = mtpObject.getFormat();
    if (mtpObject.getParent().isRoot()) {
      paramInt = 0;
    } else {
      paramInt = mtpObject.getParent().getId();
    } 
    paramArrayOfint[2] = paramInt;
    paramInt = Integer.min(mtpObject.getName().length(), 255);
    mtpObject.getName().getChars(0, paramInt, paramArrayOfchar, 0);
    paramArrayOfchar[paramInt] = Character.MIN_VALUE;
    paramArrayOflong[0] = mtpObject.getModifiedTime();
    paramArrayOflong[1] = mtpObject.getModifiedTime();
    return true;
  }
  
  private int getObjectFilePath(int paramInt, char[] paramArrayOfchar, long[] paramArrayOflong) {
    MtpStorageManager.MtpObject mtpObject = this.mManager.getObject(paramInt);
    if (mtpObject == null)
      return 8201; 
    String str = mtpObject.getPath().toString();
    paramInt = Integer.min(str.length(), 4096);
    str.getChars(0, paramInt, paramArrayOfchar, 0);
    paramArrayOfchar[paramInt] = Character.MIN_VALUE;
    paramArrayOflong[0] = mtpObject.getSize();
    paramArrayOflong[1] = mtpObject.getFormat();
    return 8193;
  }
  
  private int getObjectFormat(int paramInt) {
    MtpStorageManager.MtpObject mtpObject = this.mManager.getObject(paramInt);
    if (mtpObject == null)
      return -1; 
    return mtpObject.getFormat();
  }
  
  private byte[] getThumbnailProcess(String paramString, Bitmap paramBitmap) {
    if (paramBitmap == null)
      try {
        Log.d(TAG, "getThumbnailProcess: Fail to generate thumbnail. Probably unsupported or corrupted image");
        return null;
      } catch (OutOfMemoryError outOfMemoryError) {
        paramString = TAG;
        stringBuilder = new StringBuilder();
        stringBuilder.append("OutOfMemoryError:");
        stringBuilder.append(outOfMemoryError);
        Log.w(paramString, stringBuilder.toString());
        return null;
      }  
    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    this();
    StringBuilder stringBuilder;
    stringBuilder.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
    if (byteArrayOutputStream.size() > 204800)
      return null; 
    return byteArrayOutputStream.toByteArray();
  }
  
  public boolean getThumbnailInfo(int paramInt, long[] paramArrayOflong) {
    MtpStorageManager.MtpObject mtpObject = this.mManager.getObject(paramInt);
    if (mtpObject == null)
      return false; 
    String str = mtpObject.getPath().toString();
    paramInt = mtpObject.getFormat();
    if (paramInt != 14337)
      if (paramInt != 14340 && paramInt != 14347) {
        if (paramInt != 14354)
          if (paramInt != 14343) {
            if (paramInt != 14344)
              return false; 
          } else {
            paramArrayOflong[0] = 204800L;
            paramArrayOflong[1] = 320L;
            paramArrayOflong[2] = 240L;
            return true;
          }  
      } else {
        paramArrayOflong[0] = 204800L;
        paramArrayOflong[1] = 320L;
        paramArrayOflong[2] = 240L;
        return true;
      }  
    try {
      long l;
      ExifInterface exifInterface = new ExifInterface();
      this(str);
      long[] arrayOfLong = exifInterface.getThumbnailRange();
      if (arrayOfLong != null) {
        l = arrayOfLong[1];
      } else {
        l = 0L;
      } 
      paramArrayOflong[0] = l;
      paramArrayOflong[1] = exifInterface.getAttributeInt("PixelXDimension", 0);
      paramArrayOflong[2] = exifInterface.getAttributeInt("PixelYDimension", 0);
      return true;
    } catch (IOException iOException) {}
    paramArrayOflong[0] = 204800L;
    paramArrayOflong[1] = 320L;
    paramArrayOflong[2] = 240L;
    return true;
  }
  
  public byte[] getThumbnailData(int paramInt) {
    MtpStorageManager.MtpObject mtpObject = this.mManager.getObject(paramInt);
    if (mtpObject == null)
      return null; 
    String str = mtpObject.getPath().toString();
    paramInt = mtpObject.getFormat();
    if (paramInt != 14337)
      if (paramInt != 14340 && paramInt != 14347) {
        if (paramInt != 14354)
          if (paramInt != 14343) {
            if (paramInt != 14344)
              return null; 
          } else {
            Bitmap bitmap1 = ThumbnailUtils.createImageThumbnail(str, 1);
            arrayOfByte = getThumbnailProcess(str, bitmap1);
            return arrayOfByte;
          }  
      } else {
        Bitmap bitmap1 = ThumbnailUtils.createImageThumbnail((String)arrayOfByte, 1);
        arrayOfByte = getThumbnailProcess((String)arrayOfByte, bitmap1);
        return arrayOfByte;
      }  
    try {
      ExifInterface exifInterface = new ExifInterface();
      this((String)arrayOfByte);
      return exifInterface.getThumbnail();
    } catch (IOException iOException) {}
    Bitmap bitmap = ThumbnailUtils.createImageThumbnail((String)arrayOfByte, 1);
    byte[] arrayOfByte = getThumbnailProcess((String)arrayOfByte, bitmap);
    return arrayOfByte;
  }
  
  private int beginDeleteObject(int paramInt) {
    MtpStorageManager.MtpObject mtpObject = this.mManager.getObject(paramInt);
    if (mtpObject == null)
      return 8201; 
    if (!this.mManager.beginRemoveObject(mtpObject))
      return 8194; 
    return 8193;
  }
  
  private void endDeleteObject(int paramInt, boolean paramBoolean) {
    MtpStorageManager.MtpObject mtpObject = this.mManager.getObject(paramInt);
    if (mtpObject == null)
      return; 
    if (!this.mManager.endRemoveObject(mtpObject, paramBoolean))
      Log.e(TAG, "Failed to end remove object"); 
    if (paramBoolean)
      deleteFromMedia(mtpObject, mtpObject.getPath(), mtpObject.isDir()); 
  }
  
  private void deleteFromMedia(MtpStorageManager.MtpObject paramMtpObject, Path paramPath, boolean paramBoolean) {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual getVolumeName : ()Ljava/lang/String;
    //   4: invokestatic getContentUri : (Ljava/lang/String;)Landroid/net/Uri;
    //   7: astore_1
    //   8: iload_3
    //   9: ifeq -> 134
    //   12: aload_0
    //   13: getfield mMediaProvider : Landroid/content/ContentProviderClient;
    //   16: astore #4
    //   18: new java/lang/StringBuilder
    //   21: astore #5
    //   23: aload #5
    //   25: invokespecial <init> : ()V
    //   28: aload #5
    //   30: aload_2
    //   31: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   34: pop
    //   35: aload #5
    //   37: ldc_w '/%'
    //   40: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   43: pop
    //   44: aload #5
    //   46: invokevirtual toString : ()Ljava/lang/String;
    //   49: astore #5
    //   51: aload_2
    //   52: invokeinterface toString : ()Ljava/lang/String;
    //   57: invokevirtual length : ()I
    //   60: iconst_1
    //   61: iadd
    //   62: invokestatic toString : (I)Ljava/lang/String;
    //   65: astore #6
    //   67: new java/lang/StringBuilder
    //   70: astore #7
    //   72: aload #7
    //   74: invokespecial <init> : ()V
    //   77: aload #7
    //   79: aload_2
    //   80: invokeinterface toString : ()Ljava/lang/String;
    //   85: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   88: pop
    //   89: aload #7
    //   91: ldc_w '/'
    //   94: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   97: pop
    //   98: aload #7
    //   100: invokevirtual toString : ()Ljava/lang/String;
    //   103: astore #7
    //   105: aload #4
    //   107: aload_1
    //   108: ldc_w '_data LIKE ?1 AND lower(substr(_data,1,?2))=lower(?3)'
    //   111: iconst_3
    //   112: anewarray java/lang/String
    //   115: dup
    //   116: iconst_0
    //   117: aload #5
    //   119: aastore
    //   120: dup
    //   121: iconst_1
    //   122: aload #6
    //   124: aastore
    //   125: dup
    //   126: iconst_2
    //   127: aload #7
    //   129: aastore
    //   130: invokevirtual delete : (Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    //   133: pop
    //   134: aload_2
    //   135: invokeinterface toString : ()Ljava/lang/String;
    //   140: astore #4
    //   142: aload_0
    //   143: getfield mMediaProvider : Landroid/content/ContentProviderClient;
    //   146: aload_1
    //   147: ldc '_data=?'
    //   149: iconst_1
    //   150: anewarray java/lang/String
    //   153: dup
    //   154: iconst_0
    //   155: aload #4
    //   157: aastore
    //   158: invokevirtual delete : (Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    //   161: ifne -> 201
    //   164: getstatic android/mtp/MtpDatabase.TAG : Ljava/lang/String;
    //   167: astore #4
    //   169: new java/lang/StringBuilder
    //   172: astore_1
    //   173: aload_1
    //   174: invokespecial <init> : ()V
    //   177: aload_1
    //   178: ldc_w 'MediaProvider didn't delete '
    //   181: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   184: pop
    //   185: aload_1
    //   186: aload_2
    //   187: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   190: pop
    //   191: aload #4
    //   193: aload_1
    //   194: invokevirtual toString : ()Ljava/lang/String;
    //   197: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   200: pop
    //   201: aload_0
    //   202: getfield mContext : Landroid/content/Context;
    //   205: aload_2
    //   206: invokeinterface toFile : ()Ljava/io/File;
    //   211: invokestatic updateMediaStore : (Landroid/content/Context;Ljava/io/File;)V
    //   214: goto -> 263
    //   217: astore_1
    //   218: getstatic android/mtp/MtpDatabase.TAG : Ljava/lang/String;
    //   221: astore #4
    //   223: new java/lang/StringBuilder
    //   226: dup
    //   227: invokespecial <init> : ()V
    //   230: astore_1
    //   231: aload_1
    //   232: ldc_w 'Failed to delete '
    //   235: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   238: pop
    //   239: aload_1
    //   240: aload_2
    //   241: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   244: pop
    //   245: aload_1
    //   246: ldc_w ' from MediaProvider'
    //   249: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   252: pop
    //   253: aload #4
    //   255: aload_1
    //   256: invokevirtual toString : ()Ljava/lang/String;
    //   259: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   262: pop
    //   263: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #964	-> 0
    //   #967	-> 8
    //   #969	-> 12
    //   #973	-> 51
    //   #974	-> 77
    //   #969	-> 105
    //   #977	-> 134
    //   #981	-> 142
    //   #982	-> 164
    //   #984	-> 201
    //   #988	-> 214
    //   #986	-> 217
    //   #987	-> 218
    //   #989	-> 263
    // Exception table:
    //   from	to	target	type
    //   12	51	217	java/lang/Exception
    //   51	77	217	java/lang/Exception
    //   77	105	217	java/lang/Exception
    //   105	134	217	java/lang/Exception
    //   134	142	217	java/lang/Exception
    //   142	164	217	java/lang/Exception
    //   164	201	217	java/lang/Exception
    //   201	214	217	java/lang/Exception
  }
  
  private int[] getObjectReferences(int paramInt) {
    return null;
  }
  
  private int setObjectReferences(int paramInt, int[] paramArrayOfint) {
    return 8197;
  }
  
  private final native void native_finalize();
  
  private final native void native_setup();
}
