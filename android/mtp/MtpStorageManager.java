package android.mtp;

import android.media.MediaFile;
import android.os.FileObserver;
import android.os.SystemProperties;
import android.os.storage.StorageVolume;
import android.util.Log;
import com.android.internal.util.Preconditions;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class MtpStorageManager {
  private static final int IN_IGNORED = 32768;
  
  private static final int IN_ISDIR = 1073741824;
  
  private static final int IN_ONLYDIR = 16777216;
  
  private static final int IN_Q_OVERFLOW = 16384;
  
  private static final String TAG = MtpStorageManager.class.getSimpleName();
  
  public static boolean sDebug = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  private volatile boolean mCheckConsistency;
  
  private Thread mConsistencyThread;
  
  private MtpNotifier mMtpNotifier;
  
  private int mNextObjectId;
  
  private int mNextStorageId;
  
  private HashMap<Integer, MtpObject> mObjects;
  
  private HashMap<Integer, MtpObject> mRoots;
  
  private Set<String> mSubdirectories;
  
  class MtpObjectObserver extends FileObserver {
    MtpStorageManager.MtpObject mObject;
    
    final MtpStorageManager this$0;
    
    MtpObjectObserver(MtpStorageManager.MtpObject param1MtpObject) {
      super(param1MtpObject.getPath().toString(), 16778184);
      this.mObject = param1MtpObject;
    }
    
    public void onEvent(int param1Int, String param1String) {
      // Byte code:
      //   0: aload_0
      //   1: getfield this$0 : Landroid/mtp/MtpStorageManager;
      //   4: astore_3
      //   5: aload_3
      //   6: monitorenter
      //   7: iload_1
      //   8: sipush #16384
      //   11: iand
      //   12: ifeq -> 24
      //   15: invokestatic access$000 : ()Ljava/lang/String;
      //   18: ldc 'Received Inotify overflow event!'
      //   20: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
      //   23: pop
      //   24: aload_0
      //   25: getfield mObject : Landroid/mtp/MtpStorageManager$MtpObject;
      //   28: aload_2
      //   29: invokestatic access$100 : (Landroid/mtp/MtpStorageManager$MtpObject;Ljava/lang/String;)Landroid/mtp/MtpStorageManager$MtpObject;
      //   32: astore #4
      //   34: iload_1
      //   35: sipush #128
      //   38: iand
      //   39: ifne -> 441
      //   42: iload_1
      //   43: sipush #256
      //   46: iand
      //   47: ifeq -> 53
      //   50: goto -> 441
      //   53: iload_1
      //   54: bipush #64
      //   56: iand
      //   57: ifne -> 318
      //   60: iload_1
      //   61: sipush #512
      //   64: iand
      //   65: ifeq -> 71
      //   68: goto -> 318
      //   71: ldc 32768
      //   73: iload_1
      //   74: iand
      //   75: ifeq -> 169
      //   78: getstatic android/mtp/MtpStorageManager.sDebug : Z
      //   81: ifeq -> 137
      //   84: invokestatic access$000 : ()Ljava/lang/String;
      //   87: astore_2
      //   88: new java/lang/StringBuilder
      //   91: astore #5
      //   93: aload #5
      //   95: invokespecial <init> : ()V
      //   98: aload #5
      //   100: ldc 'inotify for '
      //   102: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   105: pop
      //   106: aload #5
      //   108: aload_0
      //   109: getfield mObject : Landroid/mtp/MtpStorageManager$MtpObject;
      //   112: invokevirtual getPath : ()Ljava/nio/file/Path;
      //   115: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   118: pop
      //   119: aload #5
      //   121: ldc ' deleted'
      //   123: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   126: pop
      //   127: aload_2
      //   128: aload #5
      //   130: invokevirtual toString : ()Ljava/lang/String;
      //   133: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
      //   136: pop
      //   137: aload_0
      //   138: getfield mObject : Landroid/mtp/MtpStorageManager$MtpObject;
      //   141: invokestatic access$400 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/os/FileObserver;
      //   144: ifnull -> 157
      //   147: aload_0
      //   148: getfield mObject : Landroid/mtp/MtpStorageManager$MtpObject;
      //   151: invokestatic access$400 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/os/FileObserver;
      //   154: invokevirtual stopWatching : ()V
      //   157: aload_0
      //   158: getfield mObject : Landroid/mtp/MtpStorageManager$MtpObject;
      //   161: aconst_null
      //   162: invokestatic access$402 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/os/FileObserver;)Landroid/os/FileObserver;
      //   165: pop
      //   166: goto -> 541
      //   169: iload_1
      //   170: bipush #8
      //   172: iand
      //   173: ifeq -> 259
      //   176: getstatic android/mtp/MtpStorageManager.sDebug : Z
      //   179: ifeq -> 244
      //   182: invokestatic access$000 : ()Ljava/lang/String;
      //   185: astore #4
      //   187: new java/lang/StringBuilder
      //   190: astore #5
      //   192: aload #5
      //   194: invokespecial <init> : ()V
      //   197: aload #5
      //   199: ldc 'inotify for '
      //   201: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   204: pop
      //   205: aload #5
      //   207: aload_0
      //   208: getfield mObject : Landroid/mtp/MtpStorageManager$MtpObject;
      //   211: invokevirtual getPath : ()Ljava/nio/file/Path;
      //   214: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   217: pop
      //   218: aload #5
      //   220: ldc ' CLOSE_WRITE: '
      //   222: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   225: pop
      //   226: aload #5
      //   228: aload_2
      //   229: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   232: pop
      //   233: aload #4
      //   235: aload #5
      //   237: invokevirtual toString : ()Ljava/lang/String;
      //   240: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
      //   243: pop
      //   244: aload_0
      //   245: getfield this$0 : Landroid/mtp/MtpStorageManager;
      //   248: aload_0
      //   249: getfield mObject : Landroid/mtp/MtpStorageManager$MtpObject;
      //   252: aload_2
      //   253: invokestatic access$500 : (Landroid/mtp/MtpStorageManager;Landroid/mtp/MtpStorageManager$MtpObject;Ljava/lang/String;)V
      //   256: goto -> 541
      //   259: invokestatic access$000 : ()Ljava/lang/String;
      //   262: astore #4
      //   264: new java/lang/StringBuilder
      //   267: astore #5
      //   269: aload #5
      //   271: invokespecial <init> : ()V
      //   274: aload #5
      //   276: ldc 'Got unrecognized event '
      //   278: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   281: pop
      //   282: aload #5
      //   284: aload_2
      //   285: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   288: pop
      //   289: aload #5
      //   291: ldc ' '
      //   293: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   296: pop
      //   297: aload #5
      //   299: iload_1
      //   300: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   303: pop
      //   304: aload #4
      //   306: aload #5
      //   308: invokevirtual toString : ()Ljava/lang/String;
      //   311: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   314: pop
      //   315: goto -> 541
      //   318: aload #4
      //   320: ifnonnull -> 367
      //   323: invokestatic access$000 : ()Ljava/lang/String;
      //   326: astore #5
      //   328: new java/lang/StringBuilder
      //   331: astore #4
      //   333: aload #4
      //   335: invokespecial <init> : ()V
      //   338: aload #4
      //   340: ldc 'Object was null in event '
      //   342: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   345: pop
      //   346: aload #4
      //   348: aload_2
      //   349: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   352: pop
      //   353: aload #5
      //   355: aload #4
      //   357: invokevirtual toString : ()Ljava/lang/String;
      //   360: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   363: pop
      //   364: aload_3
      //   365: monitorexit
      //   366: return
      //   367: getstatic android/mtp/MtpStorageManager.sDebug : Z
      //   370: ifeq -> 429
      //   373: invokestatic access$000 : ()Ljava/lang/String;
      //   376: astore #6
      //   378: new java/lang/StringBuilder
      //   381: astore #5
      //   383: aload #5
      //   385: invokespecial <init> : ()V
      //   388: aload #5
      //   390: ldc 'Got inotify removed event for '
      //   392: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   395: pop
      //   396: aload #5
      //   398: aload_2
      //   399: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   402: pop
      //   403: aload #5
      //   405: ldc ' '
      //   407: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   410: pop
      //   411: aload #5
      //   413: iload_1
      //   414: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   417: pop
      //   418: aload #6
      //   420: aload #5
      //   422: invokevirtual toString : ()Ljava/lang/String;
      //   425: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
      //   428: pop
      //   429: aload_0
      //   430: getfield this$0 : Landroid/mtp/MtpStorageManager;
      //   433: aload #4
      //   435: invokestatic access$300 : (Landroid/mtp/MtpStorageManager;Landroid/mtp/MtpStorageManager$MtpObject;)V
      //   438: goto -> 541
      //   441: getstatic android/mtp/MtpStorageManager.sDebug : Z
      //   444: ifeq -> 503
      //   447: invokestatic access$000 : ()Ljava/lang/String;
      //   450: astore #4
      //   452: new java/lang/StringBuilder
      //   455: astore #5
      //   457: aload #5
      //   459: invokespecial <init> : ()V
      //   462: aload #5
      //   464: ldc 'Got inotify added event for '
      //   466: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   469: pop
      //   470: aload #5
      //   472: aload_2
      //   473: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   476: pop
      //   477: aload #5
      //   479: ldc ' '
      //   481: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   484: pop
      //   485: aload #5
      //   487: iload_1
      //   488: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   491: pop
      //   492: aload #4
      //   494: aload #5
      //   496: invokevirtual toString : ()Ljava/lang/String;
      //   499: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
      //   502: pop
      //   503: aload_0
      //   504: getfield this$0 : Landroid/mtp/MtpStorageManager;
      //   507: astore #5
      //   509: aload_0
      //   510: getfield mObject : Landroid/mtp/MtpStorageManager$MtpObject;
      //   513: astore #4
      //   515: ldc 1073741824
      //   517: iload_1
      //   518: iand
      //   519: ifeq -> 528
      //   522: iconst_1
      //   523: istore #7
      //   525: goto -> 531
      //   528: iconst_0
      //   529: istore #7
      //   531: aload #5
      //   533: aload #4
      //   535: aload_2
      //   536: iload #7
      //   538: invokestatic access$200 : (Landroid/mtp/MtpStorageManager;Landroid/mtp/MtpStorageManager$MtpObject;Ljava/lang/String;Z)V
      //   541: aload_3
      //   542: monitorexit
      //   543: return
      //   544: astore_2
      //   545: aload_3
      //   546: monitorexit
      //   547: aload_2
      //   548: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #76	-> 0
      //   #77	-> 7
      //   #79	-> 15
      //   #81	-> 24
      //   #82	-> 34
      //   #86	-> 53
      //   #94	-> 71
      //   #95	-> 78
      //   #96	-> 84
      //   #97	-> 137
      //   #98	-> 147
      //   #99	-> 157
      //   #100	-> 169
      //   #101	-> 176
      //   #102	-> 182
      //   #103	-> 244
      //   #105	-> 259
      //   #87	-> 318
      //   #88	-> 323
      //   #89	-> 364
      //   #91	-> 367
      //   #92	-> 373
      //   #93	-> 429
      //   #83	-> 441
      //   #84	-> 447
      //   #85	-> 503
      //   #107	-> 541
      //   #108	-> 543
      //   #107	-> 544
      // Exception table:
      //   from	to	target	type
      //   15	24	544	finally
      //   24	34	544	finally
      //   78	84	544	finally
      //   84	137	544	finally
      //   137	147	544	finally
      //   147	157	544	finally
      //   157	166	544	finally
      //   176	182	544	finally
      //   182	244	544	finally
      //   244	256	544	finally
      //   259	315	544	finally
      //   323	364	544	finally
      //   364	366	544	finally
      //   367	373	544	finally
      //   373	429	544	finally
      //   429	438	544	finally
      //   441	447	544	finally
      //   447	503	544	finally
      //   503	515	544	finally
      //   531	541	544	finally
      //   541	543	544	finally
      //   545	547	544	finally
    }
    
    public void finalize() {}
  }
  
  private enum MtpObjectState {
    FROZEN, FROZEN_ADDED, FROZEN_ONESHOT_ADD, FROZEN_ONESHOT_DEL, FROZEN_REMOVED, NORMAL;
    
    private static final MtpObjectState[] $VALUES;
    
    static {
      FROZEN_ONESHOT_ADD = new MtpObjectState("FROZEN_ONESHOT_ADD", 4);
      MtpObjectState mtpObjectState = new MtpObjectState("FROZEN_ONESHOT_DEL", 5);
      $VALUES = new MtpObjectState[] { NORMAL, FROZEN, FROZEN_ADDED, FROZEN_REMOVED, FROZEN_ONESHOT_ADD, mtpObjectState };
    }
  }
  
  private enum MtpOperation {
    ADD, COPY, DELETE, NONE, RENAME;
    
    private static final MtpOperation[] $VALUES;
    
    static {
      COPY = new MtpOperation("COPY", 3);
      MtpOperation mtpOperation = new MtpOperation("DELETE", 4);
      $VALUES = new MtpOperation[] { NONE, ADD, RENAME, COPY, mtpOperation };
    }
  }
  
  public static class MtpObject {
    private HashMap<String, MtpObject> mChildren;
    
    private int mId;
    
    private boolean mIsDir;
    
    private String mName;
    
    private FileObserver mObserver;
    
    private MtpStorageManager.MtpOperation mOp;
    
    private MtpObject mParent;
    
    private MtpStorageManager.MtpObjectState mState;
    
    private MtpStorage mStorage;
    
    private boolean mVisited;
    
    MtpObject(String param1String, int param1Int, MtpStorage param1MtpStorage, MtpObject param1MtpObject, boolean param1Boolean) {
      HashMap<Object, Object> hashMap;
      this.mId = param1Int;
      this.mName = param1String;
      this.mStorage = (MtpStorage)Preconditions.checkNotNull(param1MtpStorage);
      this.mParent = param1MtpObject;
      param1String = null;
      this.mObserver = null;
      this.mVisited = false;
      this.mState = MtpStorageManager.MtpObjectState.NORMAL;
      this.mIsDir = param1Boolean;
      this.mOp = MtpStorageManager.MtpOperation.NONE;
      if (this.mIsDir)
        hashMap = new HashMap<>(); 
      this.mChildren = (HashMap)hashMap;
    }
    
    public String getName() {
      return this.mName;
    }
    
    public int getId() {
      return this.mId;
    }
    
    public boolean isDir() {
      return this.mIsDir;
    }
    
    public int getFormat() {
      int i;
      if (this.mIsDir) {
        i = 12289;
      } else {
        i = MediaFile.getFormatCode(this.mName, null);
      } 
      return i;
    }
    
    public int getStorageId() {
      return getRoot().getId();
    }
    
    public long getModifiedTime() {
      return getPath().toFile().lastModified() / 1000L;
    }
    
    public MtpObject getParent() {
      return this.mParent;
    }
    
    public MtpObject getRoot() {
      MtpObject mtpObject;
      if (isRoot()) {
        mtpObject = this;
      } else {
        mtpObject = this.mParent.getRoot();
      } 
      return mtpObject;
    }
    
    public long getSize() {
      long l;
      if (this.mIsDir) {
        l = 0L;
      } else {
        l = getPath().toFile().length();
      } 
      return l;
    }
    
    public Path getPath() {
      Path path;
      if (isRoot()) {
        path = Paths.get(this.mName, new String[0]);
      } else {
        path = this.mParent.getPath().resolve(this.mName);
      } 
      return path;
    }
    
    public boolean isRoot() {
      boolean bool;
      if (this.mParent == null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public String getVolumeName() {
      return this.mStorage.getVolumeName();
    }
    
    private void setName(String param1String) {
      this.mName = param1String;
    }
    
    private void setId(int param1Int) {
      this.mId = param1Int;
    }
    
    private boolean isVisited() {
      return this.mVisited;
    }
    
    private void setParent(MtpObject param1MtpObject) {
      this.mParent = param1MtpObject;
    }
    
    private void setDir(boolean param1Boolean) {
      if (param1Boolean != this.mIsDir) {
        HashMap hashMap;
        this.mIsDir = param1Boolean;
        if (param1Boolean) {
          hashMap = new HashMap<>();
        } else {
          hashMap = null;
        } 
        this.mChildren = hashMap;
      } 
    }
    
    private void setVisited(boolean param1Boolean) {
      this.mVisited = param1Boolean;
    }
    
    private MtpStorageManager.MtpObjectState getState() {
      return this.mState;
    }
    
    private void setState(MtpStorageManager.MtpObjectState param1MtpObjectState) {
      this.mState = param1MtpObjectState;
      if (param1MtpObjectState == MtpStorageManager.MtpObjectState.NORMAL)
        this.mOp = MtpStorageManager.MtpOperation.NONE; 
    }
    
    private MtpStorageManager.MtpOperation getOperation() {
      return this.mOp;
    }
    
    private void setOperation(MtpStorageManager.MtpOperation param1MtpOperation) {
      this.mOp = param1MtpOperation;
    }
    
    private FileObserver getObserver() {
      return this.mObserver;
    }
    
    private void setObserver(FileObserver param1FileObserver) {
      this.mObserver = param1FileObserver;
    }
    
    private void addChild(MtpObject param1MtpObject) {
      this.mChildren.put(param1MtpObject.getName(), param1MtpObject);
    }
    
    private MtpObject getChild(String param1String) {
      return this.mChildren.get(param1String);
    }
    
    private Collection<MtpObject> getChildren() {
      return this.mChildren.values();
    }
    
    private boolean exists() {
      return getPath().toFile().exists();
    }
    
    private MtpObject copy(boolean param1Boolean) {
      HashMap hashMap;
      MtpObject mtpObject = new MtpObject(this.mName, this.mId, this.mStorage, this.mParent, this.mIsDir);
      mtpObject.mIsDir = this.mIsDir;
      mtpObject.mVisited = this.mVisited;
      mtpObject.mState = this.mState;
      if (this.mIsDir) {
        hashMap = new HashMap<>();
      } else {
        hashMap = null;
      } 
      mtpObject.mChildren = hashMap;
      if (param1Boolean && this.mIsDir)
        for (MtpObject mtpObject1 : this.mChildren.values()) {
          mtpObject1 = mtpObject1.copy(true);
          mtpObject1.setParent(mtpObject);
          mtpObject.addChild(mtpObject1);
        }  
      return mtpObject;
    }
  }
  
  public static abstract class MtpNotifier {
    public abstract void sendObjectAdded(int param1Int);
    
    public abstract void sendObjectInfoChanged(int param1Int);
    
    public abstract void sendObjectRemoved(int param1Int);
  }
  
  public MtpStorageManager(MtpNotifier paramMtpNotifier, Set<String> paramSet) {
    this.mMtpNotifier = paramMtpNotifier;
    this.mSubdirectories = paramSet;
    this.mObjects = new HashMap<>();
    this.mRoots = new HashMap<>();
    this.mNextObjectId = 1;
    this.mNextStorageId = 1;
    this.mCheckConsistency = false;
    this.mConsistencyThread = new Thread(new _$$Lambda$MtpStorageManager$HocvlaKIXOtuA3p8uOP9PA7UJtw(this));
    if (this.mCheckConsistency)
      this.mConsistencyThread.start(); 
  }
  
  public void close() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mObjects : Ljava/util/HashMap;
    //   6: invokevirtual values : ()Ljava/util/Collection;
    //   9: invokeinterface iterator : ()Ljava/util/Iterator;
    //   14: astore_1
    //   15: aload_1
    //   16: invokeinterface hasNext : ()Z
    //   21: ifeq -> 56
    //   24: aload_1
    //   25: invokeinterface next : ()Ljava/lang/Object;
    //   30: checkcast android/mtp/MtpStorageManager$MtpObject
    //   33: astore_2
    //   34: aload_2
    //   35: invokestatic access$600 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/os/FileObserver;
    //   38: ifnull -> 53
    //   41: aload_2
    //   42: invokestatic access$600 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/os/FileObserver;
    //   45: invokevirtual stopWatching : ()V
    //   48: aload_2
    //   49: aconst_null
    //   50: invokestatic access$700 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/os/FileObserver;)V
    //   53: goto -> 15
    //   56: aload_0
    //   57: getfield mRoots : Ljava/util/HashMap;
    //   60: invokevirtual values : ()Ljava/util/Collection;
    //   63: invokeinterface iterator : ()Ljava/util/Iterator;
    //   68: astore_2
    //   69: aload_2
    //   70: invokeinterface hasNext : ()Z
    //   75: ifeq -> 110
    //   78: aload_2
    //   79: invokeinterface next : ()Ljava/lang/Object;
    //   84: checkcast android/mtp/MtpStorageManager$MtpObject
    //   87: astore_1
    //   88: aload_1
    //   89: invokestatic access$600 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/os/FileObserver;
    //   92: ifnull -> 107
    //   95: aload_1
    //   96: invokestatic access$600 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/os/FileObserver;
    //   99: invokevirtual stopWatching : ()V
    //   102: aload_1
    //   103: aconst_null
    //   104: invokestatic access$700 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/os/FileObserver;)V
    //   107: goto -> 69
    //   110: aload_0
    //   111: getfield mCheckConsistency : Z
    //   114: ifeq -> 140
    //   117: aload_0
    //   118: iconst_0
    //   119: putfield mCheckConsistency : Z
    //   122: aload_0
    //   123: getfield mConsistencyThread : Ljava/lang/Thread;
    //   126: invokevirtual interrupt : ()V
    //   129: aload_0
    //   130: getfield mConsistencyThread : Ljava/lang/Thread;
    //   133: invokevirtual join : ()V
    //   136: goto -> 140
    //   139: astore_2
    //   140: aload_0
    //   141: monitorexit
    //   142: return
    //   143: astore_2
    //   144: aload_0
    //   145: monitorexit
    //   146: aload_2
    //   147: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #386	-> 2
    //   #387	-> 34
    //   #388	-> 41
    //   #389	-> 48
    //   #391	-> 53
    //   #392	-> 56
    //   #393	-> 88
    //   #394	-> 95
    //   #395	-> 102
    //   #397	-> 107
    //   #400	-> 110
    //   #401	-> 117
    //   #402	-> 122
    //   #404	-> 129
    //   #407	-> 136
    //   #405	-> 139
    //   #409	-> 140
    //   #385	-> 143
    // Exception table:
    //   from	to	target	type
    //   2	15	143	finally
    //   15	34	143	finally
    //   34	41	143	finally
    //   41	48	143	finally
    //   48	53	143	finally
    //   56	69	143	finally
    //   69	88	143	finally
    //   88	95	143	finally
    //   95	102	143	finally
    //   102	107	143	finally
    //   110	117	143	finally
    //   117	122	143	finally
    //   122	129	143	finally
    //   129	136	139	java/lang/InterruptedException
    //   129	136	143	finally
  }
  
  public void setSubdirectories(Set<String> paramSet) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: putfield mSubdirectories : Ljava/util/Set;
    //   7: aload_0
    //   8: monitorexit
    //   9: return
    //   10: astore_1
    //   11: aload_0
    //   12: monitorexit
    //   13: aload_1
    //   14: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #417	-> 2
    //   #418	-> 7
    //   #416	-> 10
    // Exception table:
    //   from	to	target	type
    //   2	7	10	finally
  }
  
  public MtpStorage addMtpStorage(StorageVolume paramStorageVolume) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial getNextStorageId : ()I
    //   6: ldc_w 65535
    //   9: iand
    //   10: bipush #16
    //   12: ishl
    //   13: iconst_1
    //   14: iadd
    //   15: istore_2
    //   16: new android/mtp/MtpStorage
    //   19: astore_3
    //   20: aload_3
    //   21: aload_1
    //   22: iload_2
    //   23: invokespecial <init> : (Landroid/os/storage/StorageVolume;I)V
    //   26: new android/mtp/MtpStorageManager$MtpObject
    //   29: astore_1
    //   30: aload_1
    //   31: aload_3
    //   32: invokevirtual getPath : ()Ljava/lang/String;
    //   35: iload_2
    //   36: aload_3
    //   37: aconst_null
    //   38: iconst_1
    //   39: invokespecial <init> : (Ljava/lang/String;ILandroid/mtp/MtpStorage;Landroid/mtp/MtpStorageManager$MtpObject;Z)V
    //   42: aload_0
    //   43: getfield mRoots : Ljava/util/HashMap;
    //   46: iload_2
    //   47: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   50: aload_1
    //   51: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   54: pop
    //   55: aload_0
    //   56: monitorexit
    //   57: aload_3
    //   58: areturn
    //   59: astore_1
    //   60: aload_0
    //   61: monitorexit
    //   62: aload_1
    //   63: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #426	-> 2
    //   #427	-> 16
    //   #428	-> 26
    //   #429	-> 42
    //   #430	-> 55
    //   #425	-> 59
    // Exception table:
    //   from	to	target	type
    //   2	16	59	finally
    //   16	26	59	finally
    //   26	42	59	finally
    //   42	55	59	finally
  }
  
  public void removeMtpStorage(MtpStorage paramMtpStorage) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_0
    //   4: aload_1
    //   5: invokevirtual getStorageId : ()I
    //   8: invokevirtual getStorageRoot : (I)Landroid/mtp/MtpStorageManager$MtpObject;
    //   11: iconst_1
    //   12: iconst_1
    //   13: invokespecial removeObjectFromCache : (Landroid/mtp/MtpStorageManager$MtpObject;ZZ)Z
    //   16: pop
    //   17: aload_0
    //   18: monitorexit
    //   19: return
    //   20: astore_1
    //   21: aload_0
    //   22: monitorexit
    //   23: aload_1
    //   24: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #438	-> 2
    //   #439	-> 17
    //   #437	-> 20
    // Exception table:
    //   from	to	target	type
    //   2	17	20	finally
  }
  
  private boolean isSpecialSubDir(MtpObject paramMtpObject) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_1
    //   3: invokevirtual getParent : ()Landroid/mtp/MtpStorageManager$MtpObject;
    //   6: invokevirtual isRoot : ()Z
    //   9: ifeq -> 44
    //   12: aload_0
    //   13: getfield mSubdirectories : Ljava/util/Set;
    //   16: ifnull -> 44
    //   19: aload_0
    //   20: getfield mSubdirectories : Ljava/util/Set;
    //   23: astore_2
    //   24: aload_2
    //   25: aload_1
    //   26: invokevirtual getName : ()Ljava/lang/String;
    //   29: invokeinterface contains : (Ljava/lang/Object;)Z
    //   34: istore_3
    //   35: iload_3
    //   36: ifne -> 44
    //   39: iconst_1
    //   40: istore_3
    //   41: goto -> 46
    //   44: iconst_0
    //   45: istore_3
    //   46: aload_0
    //   47: monitorexit
    //   48: iload_3
    //   49: ireturn
    //   50: astore_1
    //   51: aload_0
    //   52: monitorexit
    //   53: aload_1
    //   54: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #448	-> 2
    //   #449	-> 24
    //   #448	-> 46
    //   #447	-> 50
    // Exception table:
    //   from	to	target	type
    //   2	24	50	finally
    //   24	35	50	finally
  }
  
  public MtpObject getByPath(String paramString) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aconst_null
    //   3: astore_2
    //   4: aload_0
    //   5: getfield mRoots : Ljava/util/HashMap;
    //   8: invokevirtual values : ()Ljava/util/Collection;
    //   11: invokeinterface iterator : ()Ljava/util/Iterator;
    //   16: astore_3
    //   17: aload_1
    //   18: astore #4
    //   20: aload_2
    //   21: astore_1
    //   22: aload_3
    //   23: invokeinterface hasNext : ()Z
    //   28: ifeq -> 81
    //   31: aload_3
    //   32: invokeinterface next : ()Ljava/lang/Object;
    //   37: checkcast android/mtp/MtpStorageManager$MtpObject
    //   40: astore #5
    //   42: aload #4
    //   44: astore_2
    //   45: aload #4
    //   47: aload #5
    //   49: invokevirtual getName : ()Ljava/lang/String;
    //   52: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   55: ifeq -> 75
    //   58: aload #5
    //   60: astore_1
    //   61: aload #4
    //   63: aload #5
    //   65: invokevirtual getName : ()Ljava/lang/String;
    //   68: invokevirtual length : ()I
    //   71: invokevirtual substring : (I)Ljava/lang/String;
    //   74: astore_2
    //   75: aload_2
    //   76: astore #4
    //   78: goto -> 22
    //   81: aload #4
    //   83: ldc_w '/'
    //   86: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
    //   89: astore #4
    //   91: aload #4
    //   93: arraylength
    //   94: istore #6
    //   96: iconst_0
    //   97: istore #7
    //   99: iload #7
    //   101: iload #6
    //   103: if_icmpge -> 168
    //   106: aload #4
    //   108: iload #7
    //   110: aaload
    //   111: astore_2
    //   112: aload_1
    //   113: ifnull -> 164
    //   116: aload_1
    //   117: invokevirtual isDir : ()Z
    //   120: ifne -> 126
    //   123: goto -> 164
    //   126: ldc_w ''
    //   129: aload_2
    //   130: invokevirtual equals : (Ljava/lang/Object;)Z
    //   133: ifeq -> 139
    //   136: goto -> 158
    //   139: aload_1
    //   140: invokestatic access$800 : (Landroid/mtp/MtpStorageManager$MtpObject;)Z
    //   143: ifne -> 152
    //   146: aload_0
    //   147: aload_1
    //   148: invokespecial getChildren : (Landroid/mtp/MtpStorageManager$MtpObject;)Ljava/util/Collection;
    //   151: pop
    //   152: aload_1
    //   153: aload_2
    //   154: invokestatic access$100 : (Landroid/mtp/MtpStorageManager$MtpObject;Ljava/lang/String;)Landroid/mtp/MtpStorageManager$MtpObject;
    //   157: astore_1
    //   158: iinc #7, 1
    //   161: goto -> 99
    //   164: aload_0
    //   165: monitorexit
    //   166: aconst_null
    //   167: areturn
    //   168: aload_0
    //   169: monitorexit
    //   170: aload_1
    //   171: areturn
    //   172: astore_1
    //   173: aload_0
    //   174: monitorexit
    //   175: aload_1
    //   176: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #458	-> 2
    //   #459	-> 4
    //   #460	-> 42
    //   #461	-> 58
    //   #462	-> 61
    //   #464	-> 75
    //   #465	-> 81
    //   #466	-> 112
    //   #468	-> 126
    //   #469	-> 136
    //   #470	-> 139
    //   #471	-> 146
    //   #472	-> 152
    //   #465	-> 158
    //   #467	-> 164
    //   #474	-> 168
    //   #457	-> 172
    // Exception table:
    //   from	to	target	type
    //   4	17	172	finally
    //   22	42	172	finally
    //   45	58	172	finally
    //   61	75	172	finally
    //   81	96	172	finally
    //   116	123	172	finally
    //   126	136	172	finally
    //   139	146	172	finally
    //   146	152	172	finally
    //   152	158	172	finally
  }
  
  public MtpObject getObject(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: iload_1
    //   3: ifeq -> 94
    //   6: iload_1
    //   7: iconst_m1
    //   8: if_icmpne -> 14
    //   11: goto -> 94
    //   14: aload_0
    //   15: getfield mObjects : Ljava/util/HashMap;
    //   18: iload_1
    //   19: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   22: invokevirtual containsKey : (Ljava/lang/Object;)Z
    //   25: ifne -> 75
    //   28: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   31: astore_2
    //   32: new java/lang/StringBuilder
    //   35: astore_3
    //   36: aload_3
    //   37: invokespecial <init> : ()V
    //   40: aload_3
    //   41: ldc_w 'Id '
    //   44: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   47: pop
    //   48: aload_3
    //   49: iload_1
    //   50: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   53: pop
    //   54: aload_3
    //   55: ldc_w ' doesn't exist'
    //   58: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   61: pop
    //   62: aload_2
    //   63: aload_3
    //   64: invokevirtual toString : ()Ljava/lang/String;
    //   67: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   70: pop
    //   71: aload_0
    //   72: monitorexit
    //   73: aconst_null
    //   74: areturn
    //   75: aload_0
    //   76: getfield mObjects : Ljava/util/HashMap;
    //   79: iload_1
    //   80: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   83: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   86: checkcast android/mtp/MtpStorageManager$MtpObject
    //   89: astore_3
    //   90: aload_0
    //   91: monitorexit
    //   92: aload_3
    //   93: areturn
    //   94: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   97: ldc_w 'Can't get root storages with getObject()'
    //   100: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   103: pop
    //   104: aload_0
    //   105: monitorexit
    //   106: aconst_null
    //   107: areturn
    //   108: astore_3
    //   109: aload_0
    //   110: monitorexit
    //   111: aload_3
    //   112: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #483	-> 2
    //   #487	-> 14
    //   #488	-> 28
    //   #489	-> 71
    //   #491	-> 75
    //   #484	-> 94
    //   #485	-> 104
    //   #482	-> 108
    // Exception table:
    //   from	to	target	type
    //   14	28	108	finally
    //   28	71	108	finally
    //   75	90	108	finally
    //   94	104	108	finally
  }
  
  public MtpObject getStorageRoot(int paramInt) {
    if (!this.mRoots.containsKey(Integer.valueOf(paramInt))) {
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("StorageId ");
      stringBuilder.append(paramInt);
      stringBuilder.append(" doesn't exist");
      Log.w(str, stringBuilder.toString());
      return null;
    } 
    return this.mRoots.get(Integer.valueOf(paramInt));
  }
  
  private int getNextObjectId() {
    int i = this.mNextObjectId;
    this.mNextObjectId = (int)(this.mNextObjectId + 1L);
    return i;
  }
  
  private int getNextStorageId() {
    int i = this.mNextStorageId;
    this.mNextStorageId = i + 1;
    return i;
  }
  
  public List<MtpObject> getObjects(int paramInt1, int paramInt2, int paramInt3) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: iload_1
    //   3: ifne -> 12
    //   6: iconst_1
    //   7: istore #4
    //   9: goto -> 15
    //   12: iconst_0
    //   13: istore #4
    //   15: new java/util/ArrayList
    //   18: astore #5
    //   20: aload #5
    //   22: invokespecial <init> : ()V
    //   25: iconst_1
    //   26: istore #6
    //   28: iload_1
    //   29: istore #7
    //   31: iload_1
    //   32: iconst_m1
    //   33: if_icmpne -> 39
    //   36: iconst_0
    //   37: istore #7
    //   39: aconst_null
    //   40: astore #8
    //   42: aconst_null
    //   43: astore #9
    //   45: iload_3
    //   46: iconst_m1
    //   47: if_icmpne -> 128
    //   50: iload #7
    //   52: ifne -> 128
    //   55: aload_0
    //   56: getfield mRoots : Ljava/util/HashMap;
    //   59: invokevirtual values : ()Ljava/util/Collection;
    //   62: invokeinterface iterator : ()Ljava/util/Iterator;
    //   67: astore #8
    //   69: iload #6
    //   71: istore_1
    //   72: aload #8
    //   74: invokeinterface hasNext : ()Z
    //   79: ifeq -> 115
    //   82: aload #8
    //   84: invokeinterface next : ()Ljava/lang/Object;
    //   89: checkcast android/mtp/MtpStorageManager$MtpObject
    //   92: astore #10
    //   94: aload_0
    //   95: aload #5
    //   97: aload #10
    //   99: iload_2
    //   100: iload #4
    //   102: invokespecial getObjects : (Ljava/util/List;Landroid/mtp/MtpStorageManager$MtpObject;IZ)Z
    //   105: istore #11
    //   107: iload_1
    //   108: iload #11
    //   110: iand
    //   111: istore_1
    //   112: goto -> 72
    //   115: iload_1
    //   116: ifeq -> 123
    //   119: aload #5
    //   121: astore #9
    //   123: aload_0
    //   124: monitorexit
    //   125: aload #9
    //   127: areturn
    //   128: iload #7
    //   130: ifne -> 143
    //   133: aload_0
    //   134: iload_3
    //   135: invokevirtual getStorageRoot : (I)Landroid/mtp/MtpStorageManager$MtpObject;
    //   138: astore #9
    //   140: goto -> 151
    //   143: aload_0
    //   144: iload #7
    //   146: invokevirtual getObject : (I)Landroid/mtp/MtpStorageManager$MtpObject;
    //   149: astore #9
    //   151: aload #9
    //   153: ifnonnull -> 160
    //   156: aload_0
    //   157: monitorexit
    //   158: aconst_null
    //   159: areturn
    //   160: aload_0
    //   161: aload #5
    //   163: aload #9
    //   165: iload_2
    //   166: iload #4
    //   168: invokespecial getObjects : (Ljava/util/List;Landroid/mtp/MtpStorageManager$MtpObject;IZ)Z
    //   171: istore #4
    //   173: aload #8
    //   175: astore #9
    //   177: iload #4
    //   179: ifeq -> 186
    //   182: aload #5
    //   184: astore #9
    //   186: aload_0
    //   187: monitorexit
    //   188: aload #9
    //   190: areturn
    //   191: astore #5
    //   193: aload_0
    //   194: monitorexit
    //   195: aload #5
    //   197: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #526	-> 2
    //   #527	-> 15
    //   #528	-> 25
    //   #529	-> 28
    //   #530	-> 36
    //   #531	-> 39
    //   #533	-> 50
    //   #535	-> 55
    //   #536	-> 94
    //   #537	-> 112
    //   #538	-> 115
    //   #541	-> 128
    //   #542	-> 151
    //   #543	-> 156
    //   #544	-> 160
    //   #545	-> 173
    //   #525	-> 191
    // Exception table:
    //   from	to	target	type
    //   15	25	191	finally
    //   55	69	191	finally
    //   72	94	191	finally
    //   94	107	191	finally
    //   133	140	191	finally
    //   143	151	191	finally
    //   160	173	191	finally
  }
  
  private boolean getObjects(List<MtpObject> paramList, MtpObject paramMtpObject, int paramInt, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_2
    //   4: invokespecial getChildren : (Landroid/mtp/MtpStorageManager$MtpObject;)Ljava/util/Collection;
    //   7: astore #5
    //   9: aload #5
    //   11: ifnonnull -> 18
    //   14: aload_0
    //   15: monitorexit
    //   16: iconst_0
    //   17: ireturn
    //   18: aload #5
    //   20: invokeinterface iterator : ()Ljava/util/Iterator;
    //   25: astore_2
    //   26: aload_2
    //   27: invokeinterface hasNext : ()Z
    //   32: ifeq -> 71
    //   35: aload_2
    //   36: invokeinterface next : ()Ljava/lang/Object;
    //   41: checkcast android/mtp/MtpStorageManager$MtpObject
    //   44: astore #6
    //   46: iload_3
    //   47: ifeq -> 59
    //   50: aload #6
    //   52: invokevirtual getFormat : ()I
    //   55: iload_3
    //   56: if_icmpne -> 68
    //   59: aload_1
    //   60: aload #6
    //   62: invokeinterface add : (Ljava/lang/Object;)Z
    //   67: pop
    //   68: goto -> 26
    //   71: iconst_1
    //   72: istore #7
    //   74: iconst_1
    //   75: istore #8
    //   77: iload #4
    //   79: ifeq -> 155
    //   82: aload #5
    //   84: invokeinterface iterator : ()Ljava/util/Iterator;
    //   89: astore #5
    //   91: iload #8
    //   93: istore #4
    //   95: iload #4
    //   97: istore #7
    //   99: aload #5
    //   101: invokeinterface hasNext : ()Z
    //   106: ifeq -> 155
    //   109: aload #5
    //   111: invokeinterface next : ()Ljava/lang/Object;
    //   116: checkcast android/mtp/MtpStorageManager$MtpObject
    //   119: astore_2
    //   120: iload #4
    //   122: istore #7
    //   124: aload_2
    //   125: invokevirtual isDir : ()Z
    //   128: ifeq -> 148
    //   131: aload_0
    //   132: aload_1
    //   133: aload_2
    //   134: iload_3
    //   135: iconst_1
    //   136: invokespecial getObjects : (Ljava/util/List;Landroid/mtp/MtpStorageManager$MtpObject;IZ)Z
    //   139: istore #7
    //   141: iload #4
    //   143: iload #7
    //   145: iand
    //   146: istore #7
    //   148: iload #7
    //   150: istore #4
    //   152: goto -> 95
    //   155: aload_0
    //   156: monitorexit
    //   157: iload #7
    //   159: ireturn
    //   160: astore_1
    //   161: aload_0
    //   162: monitorexit
    //   163: aload_1
    //   164: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #549	-> 2
    //   #550	-> 9
    //   #551	-> 14
    //   #553	-> 18
    //   #554	-> 46
    //   #555	-> 59
    //   #557	-> 68
    //   #558	-> 71
    //   #559	-> 77
    //   #561	-> 82
    //   #562	-> 120
    //   #563	-> 131
    //   #564	-> 148
    //   #566	-> 155
    //   #548	-> 160
    // Exception table:
    //   from	to	target	type
    //   2	9	160	finally
    //   18	26	160	finally
    //   26	46	160	finally
    //   50	59	160	finally
    //   59	68	160	finally
    //   82	91	160	finally
    //   99	120	160	finally
    //   124	131	160	finally
    //   131	141	160	finally
  }
  
  private Collection<MtpObject> getChildren(MtpObject paramMtpObject) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_1
    //   3: ifnull -> 215
    //   6: aload_1
    //   7: invokevirtual isDir : ()Z
    //   10: ifne -> 16
    //   13: goto -> 215
    //   16: aload_1
    //   17: invokestatic access$800 : (Landroid/mtp/MtpStorageManager$MtpObject;)Z
    //   20: ifne -> 206
    //   23: aload_1
    //   24: invokevirtual getPath : ()Ljava/nio/file/Path;
    //   27: astore_2
    //   28: aload_1
    //   29: invokestatic access$600 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/os/FileObserver;
    //   32: ifnull -> 45
    //   35: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   38: ldc_w 'Observer is not null!'
    //   41: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   44: pop
    //   45: new android/mtp/MtpStorageManager$MtpObjectObserver
    //   48: astore_3
    //   49: aload_3
    //   50: aload_0
    //   51: aload_1
    //   52: invokespecial <init> : (Landroid/mtp/MtpStorageManager;Landroid/mtp/MtpStorageManager$MtpObject;)V
    //   55: aload_1
    //   56: aload_3
    //   57: invokestatic access$700 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/os/FileObserver;)V
    //   60: aload_1
    //   61: invokestatic access$600 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/os/FileObserver;
    //   64: invokevirtual startWatching : ()V
    //   67: aload_2
    //   68: invokestatic newDirectoryStream : (Ljava/nio/file/Path;)Ljava/nio/file/DirectoryStream;
    //   71: astore_2
    //   72: aload_2
    //   73: invokeinterface iterator : ()Ljava/util/Iterator;
    //   78: astore_3
    //   79: aload_3
    //   80: invokeinterface hasNext : ()Z
    //   85: ifeq -> 138
    //   88: aload_3
    //   89: invokeinterface next : ()Ljava/lang/Object;
    //   94: checkcast java/nio/file/Path
    //   97: astore #4
    //   99: aload #4
    //   101: invokeinterface getFileName : ()Ljava/nio/file/Path;
    //   106: invokeinterface toString : ()Ljava/lang/String;
    //   111: astore #5
    //   113: aload #4
    //   115: invokeinterface toFile : ()Ljava/io/File;
    //   120: invokevirtual isDirectory : ()Z
    //   123: istore #6
    //   125: aload_0
    //   126: aload_1
    //   127: aload #5
    //   129: iload #6
    //   131: invokespecial addObjectToCache : (Landroid/mtp/MtpStorageManager$MtpObject;Ljava/lang/String;Z)Landroid/mtp/MtpStorageManager$MtpObject;
    //   134: pop
    //   135: goto -> 79
    //   138: aload_2
    //   139: ifnull -> 148
    //   142: aload_2
    //   143: invokeinterface close : ()V
    //   148: aload_1
    //   149: iconst_1
    //   150: invokestatic access$900 : (Landroid/mtp/MtpStorageManager$MtpObject;Z)V
    //   153: goto -> 206
    //   156: astore_3
    //   157: aload_2
    //   158: ifnull -> 176
    //   161: aload_2
    //   162: invokeinterface close : ()V
    //   167: goto -> 176
    //   170: astore_2
    //   171: aload_3
    //   172: aload_2
    //   173: invokevirtual addSuppressed : (Ljava/lang/Throwable;)V
    //   176: aload_3
    //   177: athrow
    //   178: astore_2
    //   179: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   182: aload_2
    //   183: invokevirtual toString : ()Ljava/lang/String;
    //   186: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   189: pop
    //   190: aload_1
    //   191: invokestatic access$600 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/os/FileObserver;
    //   194: invokevirtual stopWatching : ()V
    //   197: aload_1
    //   198: aconst_null
    //   199: invokestatic access$700 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/os/FileObserver;)V
    //   202: aload_0
    //   203: monitorexit
    //   204: aconst_null
    //   205: areturn
    //   206: aload_1
    //   207: invokestatic access$1000 : (Landroid/mtp/MtpStorageManager$MtpObject;)Ljava/util/Collection;
    //   210: astore_1
    //   211: aload_0
    //   212: monitorexit
    //   213: aload_1
    //   214: areturn
    //   215: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   218: astore_2
    //   219: new java/lang/StringBuilder
    //   222: astore_3
    //   223: aload_3
    //   224: invokespecial <init> : ()V
    //   227: aload_3
    //   228: ldc_w 'Can't find children of '
    //   231: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   234: pop
    //   235: aload_1
    //   236: ifnonnull -> 246
    //   239: ldc_w 'null'
    //   242: astore_1
    //   243: goto -> 254
    //   246: aload_1
    //   247: invokevirtual getId : ()I
    //   250: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   253: astore_1
    //   254: aload_3
    //   255: aload_1
    //   256: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   259: pop
    //   260: aload_2
    //   261: aload_3
    //   262: invokevirtual toString : ()Ljava/lang/String;
    //   265: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   268: pop
    //   269: aload_0
    //   270: monitorexit
    //   271: aconst_null
    //   272: areturn
    //   273: astore_1
    //   274: aload_0
    //   275: monitorexit
    //   276: aload_1
    //   277: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #576	-> 2
    //   #580	-> 16
    //   #581	-> 23
    //   #588	-> 28
    //   #589	-> 35
    //   #590	-> 45
    //   #591	-> 60
    //   #592	-> 67
    //   #593	-> 72
    //   #594	-> 99
    //   #595	-> 113
    //   #594	-> 125
    //   #596	-> 135
    //   #597	-> 138
    //   #602	-> 148
    //   #603	-> 148
    //   #592	-> 156
    //   #597	-> 178
    //   #598	-> 179
    //   #599	-> 190
    //   #600	-> 197
    //   #601	-> 202
    //   #605	-> 206
    //   #577	-> 215
    //   #578	-> 269
    //   #575	-> 273
    // Exception table:
    //   from	to	target	type
    //   6	13	273	finally
    //   16	23	273	finally
    //   23	28	273	finally
    //   28	35	273	finally
    //   35	45	273	finally
    //   45	60	273	finally
    //   60	67	273	finally
    //   67	72	178	java/io/IOException
    //   67	72	178	java/nio/file/DirectoryIteratorException
    //   67	72	273	finally
    //   72	79	156	finally
    //   79	99	156	finally
    //   99	113	156	finally
    //   113	125	156	finally
    //   125	135	156	finally
    //   142	148	178	java/io/IOException
    //   142	148	178	java/nio/file/DirectoryIteratorException
    //   142	148	273	finally
    //   148	153	273	finally
    //   161	167	170	finally
    //   171	176	178	java/io/IOException
    //   171	176	178	java/nio/file/DirectoryIteratorException
    //   171	176	273	finally
    //   176	178	178	java/io/IOException
    //   176	178	178	java/nio/file/DirectoryIteratorException
    //   176	178	273	finally
    //   179	190	273	finally
    //   190	197	273	finally
    //   197	202	273	finally
    //   206	211	273	finally
    //   215	235	273	finally
    //   246	254	273	finally
    //   254	269	273	finally
  }
  
  private MtpObject addObjectToCache(MtpObject paramMtpObject, String paramString, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_1
    //   3: invokevirtual isRoot : ()Z
    //   6: ifne -> 29
    //   9: aload_0
    //   10: aload_1
    //   11: invokevirtual getId : ()I
    //   14: invokevirtual getObject : (I)Landroid/mtp/MtpStorageManager$MtpObject;
    //   17: astore #4
    //   19: aload #4
    //   21: aload_1
    //   22: if_acmpeq -> 29
    //   25: aload_0
    //   26: monitorexit
    //   27: aconst_null
    //   28: areturn
    //   29: aload_1
    //   30: aload_2
    //   31: invokestatic access$100 : (Landroid/mtp/MtpStorageManager$MtpObject;Ljava/lang/String;)Landroid/mtp/MtpStorageManager$MtpObject;
    //   34: astore #4
    //   36: aload #4
    //   38: ifnull -> 45
    //   41: aload_0
    //   42: monitorexit
    //   43: aconst_null
    //   44: areturn
    //   45: aload_0
    //   46: getfield mSubdirectories : Ljava/util/Set;
    //   49: ifnull -> 80
    //   52: aload_1
    //   53: invokevirtual isRoot : ()Z
    //   56: ifeq -> 80
    //   59: aload_0
    //   60: getfield mSubdirectories : Ljava/util/Set;
    //   63: aload_2
    //   64: invokeinterface contains : (Ljava/lang/Object;)Z
    //   69: istore #5
    //   71: iload #5
    //   73: ifne -> 80
    //   76: aload_0
    //   77: monitorexit
    //   78: aconst_null
    //   79: areturn
    //   80: new android/mtp/MtpStorageManager$MtpObject
    //   83: astore #4
    //   85: aload #4
    //   87: aload_2
    //   88: aload_0
    //   89: invokespecial getNextObjectId : ()I
    //   92: aload_1
    //   93: invokestatic access$1100 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/mtp/MtpStorage;
    //   96: aload_1
    //   97: iload_3
    //   98: invokespecial <init> : (Ljava/lang/String;ILandroid/mtp/MtpStorage;Landroid/mtp/MtpStorageManager$MtpObject;Z)V
    //   101: aload_0
    //   102: getfield mObjects : Ljava/util/HashMap;
    //   105: aload #4
    //   107: invokevirtual getId : ()I
    //   110: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   113: aload #4
    //   115: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   118: pop
    //   119: aload_1
    //   120: aload #4
    //   122: invokestatic access$1200 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObject;)V
    //   125: aload_0
    //   126: monitorexit
    //   127: aload #4
    //   129: areturn
    //   130: astore_1
    //   131: aload_0
    //   132: monitorexit
    //   133: aload_1
    //   134: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #616	-> 2
    //   #618	-> 25
    //   #619	-> 29
    //   #621	-> 41
    //   #623	-> 45
    //   #625	-> 76
    //   #628	-> 80
    //   #629	-> 101
    //   #630	-> 119
    //   #631	-> 125
    //   #615	-> 130
    // Exception table:
    //   from	to	target	type
    //   2	19	130	finally
    //   29	36	130	finally
    //   45	71	130	finally
    //   80	101	130	finally
    //   101	119	130	finally
    //   119	125	130	finally
  }
  
  private boolean removeObjectFromCache(MtpObject paramMtpObject, boolean paramBoolean1, boolean paramBoolean2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_1
    //   3: invokevirtual isRoot : ()Z
    //   6: ifne -> 36
    //   9: aload_1
    //   10: invokevirtual getParent : ()Landroid/mtp/MtpStorageManager$MtpObject;
    //   13: invokestatic access$1300 : (Landroid/mtp/MtpStorageManager$MtpObject;)Ljava/util/HashMap;
    //   16: aload_1
    //   17: invokevirtual getName : ()Ljava/lang/String;
    //   20: aload_1
    //   21: invokevirtual remove : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   24: ifeq -> 30
    //   27: goto -> 36
    //   30: iconst_0
    //   31: istore #4
    //   33: goto -> 39
    //   36: iconst_1
    //   37: istore #4
    //   39: iload #4
    //   41: ifne -> 95
    //   44: getstatic android/mtp/MtpStorageManager.sDebug : Z
    //   47: ifeq -> 95
    //   50: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   53: astore #5
    //   55: new java/lang/StringBuilder
    //   58: astore #6
    //   60: aload #6
    //   62: invokespecial <init> : ()V
    //   65: aload #6
    //   67: ldc_w 'Failed to remove from parent '
    //   70: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   73: pop
    //   74: aload #6
    //   76: aload_1
    //   77: invokevirtual getPath : ()Ljava/nio/file/Path;
    //   80: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   83: pop
    //   84: aload #5
    //   86: aload #6
    //   88: invokevirtual toString : ()Ljava/lang/String;
    //   91: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   94: pop
    //   95: aload_1
    //   96: invokevirtual isRoot : ()Z
    //   99: ifeq -> 137
    //   102: aload_0
    //   103: getfield mRoots : Ljava/util/HashMap;
    //   106: aload_1
    //   107: invokevirtual getId : ()I
    //   110: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   113: aload_1
    //   114: invokevirtual remove : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   117: ifeq -> 131
    //   120: iload #4
    //   122: ifeq -> 131
    //   125: iconst_1
    //   126: istore #7
    //   128: goto -> 134
    //   131: iconst_0
    //   132: istore #7
    //   134: goto -> 177
    //   137: iload #4
    //   139: istore #7
    //   141: iload_2
    //   142: ifeq -> 177
    //   145: aload_0
    //   146: getfield mObjects : Ljava/util/HashMap;
    //   149: aload_1
    //   150: invokevirtual getId : ()I
    //   153: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   156: aload_1
    //   157: invokevirtual remove : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   160: ifeq -> 174
    //   163: iload #4
    //   165: ifeq -> 174
    //   168: iconst_1
    //   169: istore #7
    //   171: goto -> 177
    //   174: iconst_0
    //   175: istore #7
    //   177: iload #7
    //   179: ifne -> 233
    //   182: getstatic android/mtp/MtpStorageManager.sDebug : Z
    //   185: ifeq -> 233
    //   188: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   191: astore #5
    //   193: new java/lang/StringBuilder
    //   196: astore #6
    //   198: aload #6
    //   200: invokespecial <init> : ()V
    //   203: aload #6
    //   205: ldc_w 'Failed to remove from global cache '
    //   208: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   211: pop
    //   212: aload #6
    //   214: aload_1
    //   215: invokevirtual getPath : ()Ljava/nio/file/Path;
    //   218: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   221: pop
    //   222: aload #5
    //   224: aload #6
    //   226: invokevirtual toString : ()Ljava/lang/String;
    //   229: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   232: pop
    //   233: aload_1
    //   234: invokestatic access$600 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/os/FileObserver;
    //   237: ifnull -> 252
    //   240: aload_1
    //   241: invokestatic access$600 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/os/FileObserver;
    //   244: invokevirtual stopWatching : ()V
    //   247: aload_1
    //   248: aconst_null
    //   249: invokestatic access$700 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/os/FileObserver;)V
    //   252: iload #7
    //   254: istore #4
    //   256: aload_1
    //   257: invokevirtual isDir : ()Z
    //   260: ifeq -> 348
    //   263: iload #7
    //   265: istore #4
    //   267: iload_3
    //   268: ifeq -> 348
    //   271: new java/util/ArrayList
    //   274: astore #6
    //   276: aload #6
    //   278: aload_1
    //   279: invokestatic access$1000 : (Landroid/mtp/MtpStorageManager$MtpObject;)Ljava/util/Collection;
    //   282: invokespecial <init> : (Ljava/util/Collection;)V
    //   285: aload #6
    //   287: invokeinterface iterator : ()Ljava/util/Iterator;
    //   292: astore_1
    //   293: iload #7
    //   295: istore #4
    //   297: aload_1
    //   298: invokeinterface hasNext : ()Z
    //   303: ifeq -> 348
    //   306: aload_1
    //   307: invokeinterface next : ()Ljava/lang/Object;
    //   312: checkcast android/mtp/MtpStorageManager$MtpObject
    //   315: astore #6
    //   317: aload_0
    //   318: aload #6
    //   320: iload_2
    //   321: iconst_1
    //   322: invokespecial removeObjectFromCache : (Landroid/mtp/MtpStorageManager$MtpObject;ZZ)Z
    //   325: istore_3
    //   326: iload_3
    //   327: ifeq -> 340
    //   330: iload #7
    //   332: ifeq -> 340
    //   335: iconst_1
    //   336: istore_3
    //   337: goto -> 342
    //   340: iconst_0
    //   341: istore_3
    //   342: iload_3
    //   343: istore #7
    //   345: goto -> 293
    //   348: aload_0
    //   349: monitorexit
    //   350: iload #4
    //   352: ireturn
    //   353: astore_1
    //   354: aload_0
    //   355: monitorexit
    //   356: aload_1
    //   357: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #643	-> 2
    //   #644	-> 9
    //   #645	-> 39
    //   #646	-> 50
    //   #647	-> 95
    //   #648	-> 102
    //   #649	-> 137
    //   #650	-> 145
    //   #652	-> 177
    //   #653	-> 188
    //   #654	-> 233
    //   #655	-> 240
    //   #656	-> 247
    //   #658	-> 252
    //   #660	-> 271
    //   #661	-> 285
    //   #662	-> 317
    //   #663	-> 342
    //   #665	-> 348
    //   #642	-> 353
    // Exception table:
    //   from	to	target	type
    //   2	9	353	finally
    //   9	27	353	finally
    //   44	50	353	finally
    //   50	95	353	finally
    //   95	102	353	finally
    //   102	120	353	finally
    //   145	163	353	finally
    //   182	188	353	finally
    //   188	233	353	finally
    //   233	240	353	finally
    //   240	247	353	finally
    //   247	252	353	finally
    //   256	263	353	finally
    //   271	285	353	finally
    //   285	293	353	finally
    //   297	317	353	finally
    //   317	326	353	finally
  }
  
  private void handleAddedObject(MtpObject paramMtpObject, String paramString, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: getstatic android/mtp/MtpStorageManager$MtpOperation.NONE : Landroid/mtp/MtpStorageManager$MtpOperation;
    //   5: astore #4
    //   7: aload_1
    //   8: aload_2
    //   9: invokestatic access$100 : (Landroid/mtp/MtpStorageManager$MtpObject;Ljava/lang/String;)Landroid/mtp/MtpStorageManager$MtpObject;
    //   12: astore #5
    //   14: aload #5
    //   16: ifnull -> 302
    //   19: aload #5
    //   21: invokestatic access$1400 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/mtp/MtpStorageManager$MtpObjectState;
    //   24: astore #4
    //   26: aload #5
    //   28: invokestatic access$1500 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/mtp/MtpStorageManager$MtpOperation;
    //   31: astore_1
    //   32: aload #5
    //   34: invokevirtual isDir : ()Z
    //   37: iload_3
    //   38: if_icmpeq -> 95
    //   41: aload #4
    //   43: getstatic android/mtp/MtpStorageManager$MtpObjectState.FROZEN_REMOVED : Landroid/mtp/MtpStorageManager$MtpObjectState;
    //   46: if_acmpeq -> 95
    //   49: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   52: astore #6
    //   54: new java/lang/StringBuilder
    //   57: astore #7
    //   59: aload #7
    //   61: invokespecial <init> : ()V
    //   64: aload #7
    //   66: ldc_w 'Inconsistent directory info! '
    //   69: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   72: pop
    //   73: aload #7
    //   75: aload #5
    //   77: invokevirtual getPath : ()Ljava/nio/file/Path;
    //   80: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   83: pop
    //   84: aload #6
    //   86: aload #7
    //   88: invokevirtual toString : ()Ljava/lang/String;
    //   91: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   94: pop
    //   95: aload #5
    //   97: iload_3
    //   98: invokestatic access$1600 : (Landroid/mtp/MtpStorageManager$MtpObject;Z)V
    //   101: getstatic android/mtp/MtpStorageManager$1.$SwitchMap$android$mtp$MtpStorageManager$MtpObjectState : [I
    //   104: aload #4
    //   106: invokevirtual ordinal : ()I
    //   109: iaload
    //   110: istore #8
    //   112: iload #8
    //   114: iconst_1
    //   115: if_icmpeq -> 218
    //   118: iload #8
    //   120: iconst_2
    //   121: if_icmpeq -> 218
    //   124: iload #8
    //   126: iconst_3
    //   127: if_icmpeq -> 207
    //   130: iload #8
    //   132: iconst_4
    //   133: if_icmpeq -> 204
    //   136: iload #8
    //   138: iconst_5
    //   139: if_icmpeq -> 204
    //   142: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   145: astore #7
    //   147: new java/lang/StringBuilder
    //   150: astore #6
    //   152: aload #6
    //   154: invokespecial <init> : ()V
    //   157: aload #6
    //   159: ldc_w 'Unexpected state in add '
    //   162: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   165: pop
    //   166: aload #6
    //   168: aload_2
    //   169: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   172: pop
    //   173: aload #6
    //   175: ldc_w ' '
    //   178: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   181: pop
    //   182: aload #6
    //   184: aload #4
    //   186: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   189: pop
    //   190: aload #7
    //   192: aload #6
    //   194: invokevirtual toString : ()Ljava/lang/String;
    //   197: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   200: pop
    //   201: goto -> 226
    //   204: aload_0
    //   205: monitorexit
    //   206: return
    //   207: aload #5
    //   209: getstatic android/mtp/MtpStorageManager$MtpObjectState.NORMAL : Landroid/mtp/MtpStorageManager$MtpObjectState;
    //   212: invokestatic access$1700 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObjectState;)V
    //   215: goto -> 226
    //   218: aload #5
    //   220: getstatic android/mtp/MtpStorageManager$MtpObjectState.FROZEN_ADDED : Landroid/mtp/MtpStorageManager$MtpObjectState;
    //   223: invokestatic access$1700 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObjectState;)V
    //   226: getstatic android/mtp/MtpStorageManager.sDebug : Z
    //   229: ifeq -> 294
    //   232: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   235: astore #7
    //   237: new java/lang/StringBuilder
    //   240: astore_2
    //   241: aload_2
    //   242: invokespecial <init> : ()V
    //   245: aload_2
    //   246: aload #4
    //   248: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   251: pop
    //   252: aload_2
    //   253: ldc_w ' transitioned to '
    //   256: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   259: pop
    //   260: aload_2
    //   261: aload #5
    //   263: invokestatic access$1400 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/mtp/MtpStorageManager$MtpObjectState;
    //   266: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   269: pop
    //   270: aload_2
    //   271: ldc_w ' in op '
    //   274: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   277: pop
    //   278: aload_2
    //   279: aload_1
    //   280: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   283: pop
    //   284: aload #7
    //   286: aload_2
    //   287: invokevirtual toString : ()Ljava/lang/String;
    //   290: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   293: pop
    //   294: aload_1
    //   295: astore_2
    //   296: aload #5
    //   298: astore_1
    //   299: goto -> 328
    //   302: aload_0
    //   303: aload_1
    //   304: aload_2
    //   305: iload_3
    //   306: invokespecial addObjectToCache : (Landroid/mtp/MtpStorageManager$MtpObject;Ljava/lang/String;Z)Landroid/mtp/MtpStorageManager$MtpObject;
    //   309: astore_1
    //   310: aload_1
    //   311: ifnull -> 610
    //   314: aload_0
    //   315: getfield mMtpNotifier : Landroid/mtp/MtpStorageManager$MtpNotifier;
    //   318: aload_1
    //   319: invokevirtual getId : ()I
    //   322: invokevirtual sendObjectAdded : (I)V
    //   325: aload #4
    //   327: astore_2
    //   328: iload_3
    //   329: ifeq -> 607
    //   332: getstatic android/mtp/MtpStorageManager$MtpOperation.RENAME : Landroid/mtp/MtpStorageManager$MtpOperation;
    //   335: astore #5
    //   337: aload_2
    //   338: aload #5
    //   340: if_acmpne -> 346
    //   343: aload_0
    //   344: monitorexit
    //   345: return
    //   346: aload_2
    //   347: getstatic android/mtp/MtpStorageManager$MtpOperation.COPY : Landroid/mtp/MtpStorageManager$MtpOperation;
    //   350: if_acmpne -> 365
    //   353: aload_1
    //   354: invokestatic access$800 : (Landroid/mtp/MtpStorageManager$MtpObject;)Z
    //   357: istore_3
    //   358: iload_3
    //   359: ifne -> 365
    //   362: aload_0
    //   363: monitorexit
    //   364: return
    //   365: aload_1
    //   366: invokestatic access$600 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/os/FileObserver;
    //   369: ifnull -> 385
    //   372: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   375: ldc_w 'Observer is not null!'
    //   378: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   381: pop
    //   382: aload_0
    //   383: monitorexit
    //   384: return
    //   385: new android/mtp/MtpStorageManager$MtpObjectObserver
    //   388: astore_2
    //   389: aload_2
    //   390: aload_0
    //   391: aload_1
    //   392: invokespecial <init> : (Landroid/mtp/MtpStorageManager;Landroid/mtp/MtpStorageManager$MtpObject;)V
    //   395: aload_1
    //   396: aload_2
    //   397: invokestatic access$700 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/os/FileObserver;)V
    //   400: aload_1
    //   401: invokestatic access$600 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/os/FileObserver;
    //   404: invokevirtual startWatching : ()V
    //   407: aload_1
    //   408: iconst_1
    //   409: invokestatic access$900 : (Landroid/mtp/MtpStorageManager$MtpObject;Z)V
    //   412: aload_1
    //   413: invokevirtual getPath : ()Ljava/nio/file/Path;
    //   416: invokestatic newDirectoryStream : (Ljava/nio/file/Path;)Ljava/nio/file/DirectoryStream;
    //   419: astore_2
    //   420: aload_2
    //   421: invokeinterface iterator : ()Ljava/util/Iterator;
    //   426: astore #5
    //   428: aload #5
    //   430: invokeinterface hasNext : ()Z
    //   435: ifeq -> 545
    //   438: aload #5
    //   440: invokeinterface next : ()Ljava/lang/Object;
    //   445: checkcast java/nio/file/Path
    //   448: astore #4
    //   450: getstatic android/mtp/MtpStorageManager.sDebug : Z
    //   453: ifeq -> 509
    //   456: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   459: astore #6
    //   461: new java/lang/StringBuilder
    //   464: astore #7
    //   466: aload #7
    //   468: invokespecial <init> : ()V
    //   471: aload #7
    //   473: ldc_w 'Manually handling event for '
    //   476: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   479: pop
    //   480: aload #7
    //   482: aload #4
    //   484: invokeinterface getFileName : ()Ljava/nio/file/Path;
    //   489: invokeinterface toString : ()Ljava/lang/String;
    //   494: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   497: pop
    //   498: aload #6
    //   500: aload #7
    //   502: invokevirtual toString : ()Ljava/lang/String;
    //   505: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   508: pop
    //   509: aload #4
    //   511: invokeinterface getFileName : ()Ljava/nio/file/Path;
    //   516: invokeinterface toString : ()Ljava/lang/String;
    //   521: astore #7
    //   523: aload #4
    //   525: invokeinterface toFile : ()Ljava/io/File;
    //   530: invokevirtual isDirectory : ()Z
    //   533: istore_3
    //   534: aload_0
    //   535: aload_1
    //   536: aload #7
    //   538: iload_3
    //   539: invokespecial handleAddedObject : (Landroid/mtp/MtpStorageManager$MtpObject;Ljava/lang/String;Z)V
    //   542: goto -> 428
    //   545: aload_2
    //   546: ifnull -> 555
    //   549: aload_2
    //   550: invokeinterface close : ()V
    //   555: goto -> 607
    //   558: astore #5
    //   560: aload_2
    //   561: ifnull -> 580
    //   564: aload_2
    //   565: invokeinterface close : ()V
    //   570: goto -> 580
    //   573: astore_2
    //   574: aload #5
    //   576: aload_2
    //   577: invokevirtual addSuppressed : (Ljava/lang/Throwable;)V
    //   580: aload #5
    //   582: athrow
    //   583: astore_2
    //   584: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   587: aload_2
    //   588: invokevirtual toString : ()Ljava/lang/String;
    //   591: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   594: pop
    //   595: aload_1
    //   596: invokestatic access$600 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/os/FileObserver;
    //   599: invokevirtual stopWatching : ()V
    //   602: aload_1
    //   603: aconst_null
    //   604: invokestatic access$700 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/os/FileObserver;)V
    //   607: aload_0
    //   608: monitorexit
    //   609: return
    //   610: getstatic android/mtp/MtpStorageManager.sDebug : Z
    //   613: ifeq -> 665
    //   616: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   619: astore_1
    //   620: new java/lang/StringBuilder
    //   623: astore #5
    //   625: aload #5
    //   627: invokespecial <init> : ()V
    //   630: aload #5
    //   632: ldc_w 'object '
    //   635: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   638: pop
    //   639: aload #5
    //   641: aload_2
    //   642: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   645: pop
    //   646: aload #5
    //   648: ldc_w ' already exists'
    //   651: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   654: pop
    //   655: aload_1
    //   656: aload #5
    //   658: invokevirtual toString : ()Ljava/lang/String;
    //   661: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   664: pop
    //   665: aload_0
    //   666: monitorexit
    //   667: return
    //   668: astore_1
    //   669: aload_0
    //   670: monitorexit
    //   671: aload_1
    //   672: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #669	-> 2
    //   #670	-> 7
    //   #671	-> 14
    //   #672	-> 19
    //   #673	-> 26
    //   #674	-> 32
    //   #675	-> 49
    //   #676	-> 95
    //   #677	-> 101
    //   #690	-> 142
    //   #688	-> 204
    //   #683	-> 207
    //   #684	-> 215
    //   #680	-> 218
    //   #681	-> 226
    //   #692	-> 226
    //   #693	-> 232
    //   #694	-> 294
    //   #695	-> 302
    //   #696	-> 310
    //   #697	-> 314
    //   #704	-> 328
    //   #706	-> 332
    //   #707	-> 343
    //   #710	-> 346
    //   #711	-> 362
    //   #713	-> 365
    //   #714	-> 372
    //   #715	-> 382
    //   #717	-> 385
    //   #718	-> 400
    //   #719	-> 407
    //   #723	-> 412
    //   #724	-> 420
    //   #725	-> 450
    //   #726	-> 456
    //   #727	-> 509
    //   #728	-> 523
    //   #727	-> 534
    //   #729	-> 542
    //   #730	-> 545
    //   #734	-> 555
    //   #723	-> 558
    //   #730	-> 583
    //   #731	-> 584
    //   #732	-> 595
    //   #733	-> 602
    //   #736	-> 607
    //   #699	-> 610
    //   #700	-> 616
    //   #701	-> 665
    //   #668	-> 668
    // Exception table:
    //   from	to	target	type
    //   2	7	668	finally
    //   7	14	668	finally
    //   19	26	668	finally
    //   26	32	668	finally
    //   32	49	668	finally
    //   49	95	668	finally
    //   95	101	668	finally
    //   101	112	668	finally
    //   142	201	668	finally
    //   207	215	668	finally
    //   218	226	668	finally
    //   226	232	668	finally
    //   232	294	668	finally
    //   302	310	668	finally
    //   314	325	668	finally
    //   332	337	668	finally
    //   346	358	668	finally
    //   365	372	668	finally
    //   372	382	668	finally
    //   385	400	668	finally
    //   400	407	668	finally
    //   407	412	668	finally
    //   412	420	583	java/io/IOException
    //   412	420	583	java/nio/file/DirectoryIteratorException
    //   412	420	668	finally
    //   420	428	558	finally
    //   428	450	558	finally
    //   450	456	558	finally
    //   456	509	558	finally
    //   509	523	558	finally
    //   523	534	558	finally
    //   534	542	558	finally
    //   549	555	583	java/io/IOException
    //   549	555	583	java/nio/file/DirectoryIteratorException
    //   549	555	668	finally
    //   564	570	573	finally
    //   574	580	583	java/io/IOException
    //   574	580	583	java/nio/file/DirectoryIteratorException
    //   574	580	668	finally
    //   580	583	583	java/io/IOException
    //   580	583	583	java/nio/file/DirectoryIteratorException
    //   580	583	668	finally
    //   584	595	668	finally
    //   595	602	668	finally
    //   602	607	668	finally
    //   610	616	668	finally
    //   616	665	668	finally
  }
  
  private void handleRemovedObject(MtpObject paramMtpObject) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_1
    //   3: invokestatic access$1400 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/mtp/MtpStorageManager$MtpObjectState;
    //   6: astore_2
    //   7: aload_1
    //   8: invokestatic access$1500 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/mtp/MtpStorageManager$MtpOperation;
    //   11: astore_3
    //   12: getstatic android/mtp/MtpStorageManager$1.$SwitchMap$android$mtp$MtpStorageManager$MtpObjectState : [I
    //   15: aload_2
    //   16: invokevirtual ordinal : ()I
    //   19: iaload
    //   20: istore #4
    //   22: iconst_1
    //   23: istore #5
    //   25: iload #4
    //   27: iconst_1
    //   28: if_icmpeq -> 157
    //   31: iload #4
    //   33: iconst_4
    //   34: if_icmpeq -> 133
    //   37: iload #4
    //   39: iconst_5
    //   40: if_icmpeq -> 123
    //   43: iload #4
    //   45: bipush #6
    //   47: if_icmpeq -> 98
    //   50: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   53: astore #6
    //   55: new java/lang/StringBuilder
    //   58: astore #7
    //   60: aload #7
    //   62: invokespecial <init> : ()V
    //   65: aload #7
    //   67: ldc_w 'Got unexpected object remove for '
    //   70: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   73: pop
    //   74: aload #7
    //   76: aload_1
    //   77: invokevirtual getName : ()Ljava/lang/String;
    //   80: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   83: pop
    //   84: aload #6
    //   86: aload #7
    //   88: invokevirtual toString : ()Ljava/lang/String;
    //   91: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   94: pop
    //   95: goto -> 164
    //   98: aload_3
    //   99: getstatic android/mtp/MtpStorageManager$MtpOperation.RENAME : Landroid/mtp/MtpStorageManager$MtpOperation;
    //   102: if_acmpeq -> 108
    //   105: goto -> 111
    //   108: iconst_0
    //   109: istore #5
    //   111: aload_0
    //   112: aload_1
    //   113: iload #5
    //   115: iconst_0
    //   116: invokespecial removeObjectFromCache : (Landroid/mtp/MtpStorageManager$MtpObject;ZZ)Z
    //   119: pop
    //   120: goto -> 164
    //   123: aload_1
    //   124: getstatic android/mtp/MtpStorageManager$MtpObjectState.FROZEN_REMOVED : Landroid/mtp/MtpStorageManager$MtpObjectState;
    //   127: invokestatic access$1700 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObjectState;)V
    //   130: goto -> 164
    //   133: aload_0
    //   134: aload_1
    //   135: iconst_1
    //   136: iconst_1
    //   137: invokespecial removeObjectFromCache : (Landroid/mtp/MtpStorageManager$MtpObject;ZZ)Z
    //   140: ifeq -> 164
    //   143: aload_0
    //   144: getfield mMtpNotifier : Landroid/mtp/MtpStorageManager$MtpNotifier;
    //   147: aload_1
    //   148: invokevirtual getId : ()I
    //   151: invokevirtual sendObjectRemoved : (I)V
    //   154: goto -> 164
    //   157: aload_1
    //   158: getstatic android/mtp/MtpStorageManager$MtpObjectState.FROZEN_REMOVED : Landroid/mtp/MtpStorageManager$MtpObjectState;
    //   161: invokestatic access$1700 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObjectState;)V
    //   164: getstatic android/mtp/MtpStorageManager.sDebug : Z
    //   167: ifeq -> 238
    //   170: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   173: astore #6
    //   175: new java/lang/StringBuilder
    //   178: astore #7
    //   180: aload #7
    //   182: invokespecial <init> : ()V
    //   185: aload #7
    //   187: aload_2
    //   188: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   191: pop
    //   192: aload #7
    //   194: ldc_w ' transitioned to '
    //   197: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   200: pop
    //   201: aload #7
    //   203: aload_1
    //   204: invokestatic access$1400 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/mtp/MtpStorageManager$MtpObjectState;
    //   207: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   210: pop
    //   211: aload #7
    //   213: ldc_w ' in op '
    //   216: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   219: pop
    //   220: aload #7
    //   222: aload_3
    //   223: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   226: pop
    //   227: aload #6
    //   229: aload #7
    //   231: invokevirtual toString : ()Ljava/lang/String;
    //   234: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   237: pop
    //   238: aload_0
    //   239: monitorexit
    //   240: return
    //   241: astore_1
    //   242: aload_0
    //   243: monitorexit
    //   244: aload_1
    //   245: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #739	-> 2
    //   #740	-> 7
    //   #741	-> 12
    //   #757	-> 50
    //   #746	-> 98
    //   #747	-> 120
    //   #743	-> 123
    //   #744	-> 130
    //   #752	-> 133
    //   #753	-> 143
    //   #749	-> 157
    //   #750	-> 164
    //   #759	-> 164
    //   #760	-> 170
    //   #761	-> 238
    //   #738	-> 241
    // Exception table:
    //   from	to	target	type
    //   2	7	241	finally
    //   7	12	241	finally
    //   12	22	241	finally
    //   50	95	241	finally
    //   98	105	241	finally
    //   111	120	241	finally
    //   123	130	241	finally
    //   133	143	241	finally
    //   143	154	241	finally
    //   157	164	241	finally
    //   164	170	241	finally
    //   170	238	241	finally
  }
  
  private void handleChangedObject(MtpObject paramMtpObject, String paramString) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: getstatic android/mtp/MtpStorageManager$MtpOperation.NONE : Landroid/mtp/MtpStorageManager$MtpOperation;
    //   5: astore_3
    //   6: aload_1
    //   7: aload_2
    //   8: invokestatic access$100 : (Landroid/mtp/MtpStorageManager$MtpObject;Ljava/lang/String;)Landroid/mtp/MtpStorageManager$MtpObject;
    //   11: astore_1
    //   12: aload_1
    //   13: ifnull -> 117
    //   16: aload_1
    //   17: invokevirtual isDir : ()Z
    //   20: ifne -> 166
    //   23: aload_1
    //   24: invokevirtual getSize : ()J
    //   27: lconst_0
    //   28: lcmp
    //   29: ifle -> 166
    //   32: aload_1
    //   33: invokestatic access$1400 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/mtp/MtpStorageManager$MtpObjectState;
    //   36: pop
    //   37: aload_1
    //   38: invokestatic access$1500 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/mtp/MtpStorageManager$MtpOperation;
    //   41: pop
    //   42: aload_0
    //   43: getfield mMtpNotifier : Landroid/mtp/MtpStorageManager$MtpNotifier;
    //   46: aload_1
    //   47: invokevirtual getId : ()I
    //   50: invokevirtual sendObjectInfoChanged : (I)V
    //   53: getstatic android/mtp/MtpStorageManager.sDebug : Z
    //   56: ifeq -> 114
    //   59: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   62: astore_3
    //   63: new java/lang/StringBuilder
    //   66: astore_2
    //   67: aload_2
    //   68: invokespecial <init> : ()V
    //   71: aload_2
    //   72: ldc_w 'sendObjectInfoChanged: id='
    //   75: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   78: pop
    //   79: aload_2
    //   80: aload_1
    //   81: invokevirtual getId : ()I
    //   84: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   87: pop
    //   88: aload_2
    //   89: ldc_w ',size='
    //   92: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   95: pop
    //   96: aload_2
    //   97: aload_1
    //   98: invokevirtual getSize : ()J
    //   101: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   104: pop
    //   105: aload_3
    //   106: aload_2
    //   107: invokevirtual toString : ()Ljava/lang/String;
    //   110: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   113: pop
    //   114: goto -> 166
    //   117: getstatic android/mtp/MtpStorageManager.sDebug : Z
    //   120: ifeq -> 166
    //   123: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   126: astore_3
    //   127: new java/lang/StringBuilder
    //   130: astore_1
    //   131: aload_1
    //   132: invokespecial <init> : ()V
    //   135: aload_1
    //   136: ldc_w 'object '
    //   139: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   142: pop
    //   143: aload_1
    //   144: aload_2
    //   145: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   148: pop
    //   149: aload_1
    //   150: ldc_w ' null'
    //   153: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   156: pop
    //   157: aload_3
    //   158: aload_1
    //   159: invokevirtual toString : ()Ljava/lang/String;
    //   162: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   165: pop
    //   166: aload_0
    //   167: monitorexit
    //   168: return
    //   169: astore_1
    //   170: aload_0
    //   171: monitorexit
    //   172: aload_1
    //   173: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #764	-> 2
    //   #765	-> 6
    //   #766	-> 12
    //   #768	-> 16
    //   #770	-> 32
    //   #771	-> 37
    //   #772	-> 42
    //   #773	-> 53
    //   #774	-> 59
    //   #775	-> 114
    //   #777	-> 117
    //   #778	-> 123
    //   #780	-> 166
    //   #763	-> 169
    // Exception table:
    //   from	to	target	type
    //   2	6	169	finally
    //   6	12	169	finally
    //   16	32	169	finally
    //   32	37	169	finally
    //   37	42	169	finally
    //   42	53	169	finally
    //   53	59	169	finally
    //   59	114	169	finally
    //   117	123	169	finally
    //   123	166	169	finally
  }
  
  public void flushEvents() {
    try {
      Thread.sleep(500L);
    } catch (InterruptedException interruptedException) {}
  }
  
  public void dump() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mObjects : Ljava/util/HashMap;
    //   6: invokevirtual keySet : ()Ljava/util/Set;
    //   9: invokeinterface iterator : ()Ljava/util/Iterator;
    //   14: astore_1
    //   15: aload_1
    //   16: invokeinterface hasNext : ()Z
    //   21: ifeq -> 248
    //   24: aload_1
    //   25: invokeinterface next : ()Ljava/lang/Object;
    //   30: checkcast java/lang/Integer
    //   33: invokevirtual intValue : ()I
    //   36: istore_2
    //   37: aload_0
    //   38: getfield mObjects : Ljava/util/HashMap;
    //   41: iload_2
    //   42: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   45: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   48: checkcast android/mtp/MtpStorageManager$MtpObject
    //   51: astore_3
    //   52: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   55: astore #4
    //   57: new java/lang/StringBuilder
    //   60: astore #5
    //   62: aload #5
    //   64: invokespecial <init> : ()V
    //   67: aload #5
    //   69: iload_2
    //   70: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   73: pop
    //   74: aload #5
    //   76: ldc_w ' | '
    //   79: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   82: pop
    //   83: aload_3
    //   84: invokevirtual getParent : ()Landroid/mtp/MtpStorageManager$MtpObject;
    //   87: ifnonnull -> 105
    //   90: aload_3
    //   91: invokevirtual getParent : ()Landroid/mtp/MtpStorageManager$MtpObject;
    //   94: invokevirtual getId : ()I
    //   97: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   100: astore #6
    //   102: goto -> 110
    //   105: ldc_w 'null'
    //   108: astore #6
    //   110: aload #5
    //   112: aload #6
    //   114: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   117: pop
    //   118: aload #5
    //   120: ldc_w ' | '
    //   123: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   126: pop
    //   127: aload #5
    //   129: aload_3
    //   130: invokevirtual getName : ()Ljava/lang/String;
    //   133: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   136: pop
    //   137: aload #5
    //   139: ldc_w ' | '
    //   142: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   145: pop
    //   146: aload_3
    //   147: invokevirtual isDir : ()Z
    //   150: ifeq -> 161
    //   153: ldc_w 'dir'
    //   156: astore #6
    //   158: goto -> 166
    //   161: ldc_w 'obj'
    //   164: astore #6
    //   166: aload #5
    //   168: aload #6
    //   170: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   173: pop
    //   174: aload #5
    //   176: ldc_w ' | '
    //   179: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   182: pop
    //   183: aload_3
    //   184: invokestatic access$800 : (Landroid/mtp/MtpStorageManager$MtpObject;)Z
    //   187: ifeq -> 198
    //   190: ldc_w 'v'
    //   193: astore #6
    //   195: goto -> 203
    //   198: ldc_w 'nv'
    //   201: astore #6
    //   203: aload #5
    //   205: aload #6
    //   207: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   210: pop
    //   211: aload #5
    //   213: ldc_w ' | '
    //   216: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   219: pop
    //   220: aload #5
    //   222: aload_3
    //   223: invokestatic access$1400 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/mtp/MtpStorageManager$MtpObjectState;
    //   226: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   229: pop
    //   230: aload #5
    //   232: invokevirtual toString : ()Ljava/lang/String;
    //   235: astore #6
    //   237: aload #4
    //   239: aload #6
    //   241: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   244: pop
    //   245: goto -> 15
    //   248: aload_0
    //   249: monitorexit
    //   250: return
    //   251: astore #6
    //   253: aload_0
    //   254: monitorexit
    //   255: aload #6
    //   257: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #799	-> 2
    //   #800	-> 37
    //   #801	-> 52
    //   #802	-> 127
    //   #803	-> 183
    //   #801	-> 237
    //   #804	-> 245
    //   #805	-> 248
    //   #798	-> 251
    // Exception table:
    //   from	to	target	type
    //   2	15	251	finally
    //   15	37	251	finally
    //   37	52	251	finally
    //   52	102	251	finally
    //   110	127	251	finally
    //   127	153	251	finally
    //   166	183	251	finally
    //   183	190	251	finally
    //   203	237	251	finally
    //   237	245	251	finally
  }
  
  public boolean checkConsistency() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: new java/util/ArrayList
    //   5: astore_1
    //   6: aload_1
    //   7: invokespecial <init> : ()V
    //   10: aload_1
    //   11: aload_0
    //   12: getfield mRoots : Ljava/util/HashMap;
    //   15: invokevirtual values : ()Ljava/util/Collection;
    //   18: invokeinterface addAll : (Ljava/util/Collection;)Z
    //   23: pop
    //   24: aload_1
    //   25: aload_0
    //   26: getfield mObjects : Ljava/util/HashMap;
    //   29: invokevirtual values : ()Ljava/util/Collection;
    //   32: invokeinterface addAll : (Ljava/util/Collection;)Z
    //   37: pop
    //   38: iconst_1
    //   39: istore_2
    //   40: aload_1
    //   41: invokeinterface iterator : ()Ljava/util/Iterator;
    //   46: astore_3
    //   47: aload_3
    //   48: invokeinterface hasNext : ()Z
    //   53: ifeq -> 1279
    //   56: aload_3
    //   57: invokeinterface next : ()Ljava/lang/Object;
    //   62: checkcast android/mtp/MtpStorageManager$MtpObject
    //   65: astore #4
    //   67: aload #4
    //   69: invokestatic access$1800 : (Landroid/mtp/MtpStorageManager$MtpObject;)Z
    //   72: ifne -> 141
    //   75: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   78: astore_1
    //   79: new java/lang/StringBuilder
    //   82: astore #5
    //   84: aload #5
    //   86: invokespecial <init> : ()V
    //   89: aload #5
    //   91: ldc_w 'Object doesn't exist '
    //   94: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   97: pop
    //   98: aload #5
    //   100: aload #4
    //   102: invokevirtual getPath : ()Ljava/nio/file/Path;
    //   105: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   108: pop
    //   109: aload #5
    //   111: ldc_w ' '
    //   114: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   117: pop
    //   118: aload #5
    //   120: aload #4
    //   122: invokevirtual getId : ()I
    //   125: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   128: pop
    //   129: aload_1
    //   130: aload #5
    //   132: invokevirtual toString : ()Ljava/lang/String;
    //   135: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   138: pop
    //   139: iconst_0
    //   140: istore_2
    //   141: aload #4
    //   143: invokestatic access$1400 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/mtp/MtpStorageManager$MtpObjectState;
    //   146: getstatic android/mtp/MtpStorageManager$MtpObjectState.NORMAL : Landroid/mtp/MtpStorageManager$MtpObjectState;
    //   149: if_acmpeq -> 213
    //   152: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   155: astore #5
    //   157: new java/lang/StringBuilder
    //   160: astore_1
    //   161: aload_1
    //   162: invokespecial <init> : ()V
    //   165: aload_1
    //   166: ldc_w 'Object '
    //   169: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   172: pop
    //   173: aload_1
    //   174: aload #4
    //   176: invokevirtual getPath : ()Ljava/nio/file/Path;
    //   179: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   182: pop
    //   183: aload_1
    //   184: ldc_w ' in state '
    //   187: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   190: pop
    //   191: aload_1
    //   192: aload #4
    //   194: invokestatic access$1400 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/mtp/MtpStorageManager$MtpObjectState;
    //   197: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   200: pop
    //   201: aload #5
    //   203: aload_1
    //   204: invokevirtual toString : ()Ljava/lang/String;
    //   207: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   210: pop
    //   211: iconst_0
    //   212: istore_2
    //   213: aload #4
    //   215: invokestatic access$1500 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/mtp/MtpStorageManager$MtpOperation;
    //   218: getstatic android/mtp/MtpStorageManager$MtpOperation.NONE : Landroid/mtp/MtpStorageManager$MtpOperation;
    //   221: if_acmpeq -> 285
    //   224: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   227: astore #5
    //   229: new java/lang/StringBuilder
    //   232: astore_1
    //   233: aload_1
    //   234: invokespecial <init> : ()V
    //   237: aload_1
    //   238: ldc_w 'Object '
    //   241: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   244: pop
    //   245: aload_1
    //   246: aload #4
    //   248: invokevirtual getPath : ()Ljava/nio/file/Path;
    //   251: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   254: pop
    //   255: aload_1
    //   256: ldc_w ' in operation '
    //   259: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   262: pop
    //   263: aload_1
    //   264: aload #4
    //   266: invokestatic access$1500 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/mtp/MtpStorageManager$MtpOperation;
    //   269: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   272: pop
    //   273: aload #5
    //   275: aload_1
    //   276: invokevirtual toString : ()Ljava/lang/String;
    //   279: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   282: pop
    //   283: iconst_0
    //   284: istore_2
    //   285: iload_2
    //   286: istore #6
    //   288: aload #4
    //   290: invokevirtual isRoot : ()Z
    //   293: ifne -> 375
    //   296: iload_2
    //   297: istore #6
    //   299: aload_0
    //   300: getfield mObjects : Ljava/util/HashMap;
    //   303: aload #4
    //   305: invokevirtual getId : ()I
    //   308: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   311: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   314: aload #4
    //   316: if_acmpeq -> 375
    //   319: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   322: astore_1
    //   323: new java/lang/StringBuilder
    //   326: astore #5
    //   328: aload #5
    //   330: invokespecial <init> : ()V
    //   333: aload #5
    //   335: ldc_w 'Object '
    //   338: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   341: pop
    //   342: aload #5
    //   344: aload #4
    //   346: invokevirtual getPath : ()Ljava/nio/file/Path;
    //   349: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   352: pop
    //   353: aload #5
    //   355: ldc_w ' is not in map correctly'
    //   358: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   361: pop
    //   362: aload_1
    //   363: aload #5
    //   365: invokevirtual toString : ()Ljava/lang/String;
    //   368: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   371: pop
    //   372: iconst_0
    //   373: istore #6
    //   375: iload #6
    //   377: istore_2
    //   378: aload #4
    //   380: invokevirtual getParent : ()Landroid/mtp/MtpStorageManager$MtpObject;
    //   383: ifnull -> 641
    //   386: iload #6
    //   388: istore_2
    //   389: aload #4
    //   391: invokevirtual getParent : ()Landroid/mtp/MtpStorageManager$MtpObject;
    //   394: invokevirtual isRoot : ()Z
    //   397: ifeq -> 481
    //   400: aload #4
    //   402: invokevirtual getParent : ()Landroid/mtp/MtpStorageManager$MtpObject;
    //   405: astore #5
    //   407: aload_0
    //   408: getfield mRoots : Ljava/util/HashMap;
    //   411: astore_1
    //   412: iload #6
    //   414: istore_2
    //   415: aload #5
    //   417: aload_1
    //   418: aload #4
    //   420: invokevirtual getParent : ()Landroid/mtp/MtpStorageManager$MtpObject;
    //   423: invokevirtual getId : ()I
    //   426: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   429: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   432: if_acmpeq -> 481
    //   435: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   438: astore_1
    //   439: new java/lang/StringBuilder
    //   442: astore #5
    //   444: aload #5
    //   446: invokespecial <init> : ()V
    //   449: aload #5
    //   451: ldc_w 'Root parent is not in root mapping '
    //   454: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   457: pop
    //   458: aload #5
    //   460: aload #4
    //   462: invokevirtual getPath : ()Ljava/nio/file/Path;
    //   465: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   468: pop
    //   469: aload_1
    //   470: aload #5
    //   472: invokevirtual toString : ()Ljava/lang/String;
    //   475: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   478: pop
    //   479: iconst_0
    //   480: istore_2
    //   481: iload_2
    //   482: istore #6
    //   484: aload #4
    //   486: invokevirtual getParent : ()Landroid/mtp/MtpStorageManager$MtpObject;
    //   489: invokevirtual isRoot : ()Z
    //   492: ifne -> 574
    //   495: aload #4
    //   497: invokevirtual getParent : ()Landroid/mtp/MtpStorageManager$MtpObject;
    //   500: astore #5
    //   502: aload_0
    //   503: getfield mObjects : Ljava/util/HashMap;
    //   506: astore_1
    //   507: iload_2
    //   508: istore #6
    //   510: aload #5
    //   512: aload_1
    //   513: aload #4
    //   515: invokevirtual getParent : ()Landroid/mtp/MtpStorageManager$MtpObject;
    //   518: invokevirtual getId : ()I
    //   521: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   524: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   527: if_acmpeq -> 574
    //   530: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   533: astore #5
    //   535: new java/lang/StringBuilder
    //   538: astore_1
    //   539: aload_1
    //   540: invokespecial <init> : ()V
    //   543: aload_1
    //   544: ldc_w 'Parent is not in object mapping '
    //   547: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   550: pop
    //   551: aload_1
    //   552: aload #4
    //   554: invokevirtual getPath : ()Ljava/nio/file/Path;
    //   557: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   560: pop
    //   561: aload #5
    //   563: aload_1
    //   564: invokevirtual toString : ()Ljava/lang/String;
    //   567: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   570: pop
    //   571: iconst_0
    //   572: istore #6
    //   574: iload #6
    //   576: istore_2
    //   577: aload #4
    //   579: invokevirtual getParent : ()Landroid/mtp/MtpStorageManager$MtpObject;
    //   582: aload #4
    //   584: invokevirtual getName : ()Ljava/lang/String;
    //   587: invokestatic access$100 : (Landroid/mtp/MtpStorageManager$MtpObject;Ljava/lang/String;)Landroid/mtp/MtpStorageManager$MtpObject;
    //   590: aload #4
    //   592: if_acmpeq -> 641
    //   595: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   598: astore_1
    //   599: new java/lang/StringBuilder
    //   602: astore #5
    //   604: aload #5
    //   606: invokespecial <init> : ()V
    //   609: aload #5
    //   611: ldc_w 'Child does not exist in parent '
    //   614: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   617: pop
    //   618: aload #5
    //   620: aload #4
    //   622: invokevirtual getPath : ()Ljava/nio/file/Path;
    //   625: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   628: pop
    //   629: aload_1
    //   630: aload #5
    //   632: invokevirtual toString : ()Ljava/lang/String;
    //   635: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   638: pop
    //   639: iconst_0
    //   640: istore_2
    //   641: iload_2
    //   642: istore #6
    //   644: aload #4
    //   646: invokevirtual isDir : ()Z
    //   649: ifeq -> 1273
    //   652: aload #4
    //   654: invokestatic access$800 : (Landroid/mtp/MtpStorageManager$MtpObject;)Z
    //   657: istore #7
    //   659: aload #4
    //   661: invokestatic access$600 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/os/FileObserver;
    //   664: ifnonnull -> 673
    //   667: iconst_1
    //   668: istore #6
    //   670: goto -> 676
    //   673: iconst_0
    //   674: istore #6
    //   676: iload #7
    //   678: iload #6
    //   680: if_icmpne -> 779
    //   683: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   686: astore #5
    //   688: new java/lang/StringBuilder
    //   691: astore #8
    //   693: aload #8
    //   695: invokespecial <init> : ()V
    //   698: aload #8
    //   700: aload #4
    //   702: invokevirtual getPath : ()Ljava/nio/file/Path;
    //   705: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   708: pop
    //   709: aload #8
    //   711: ldc_w ' is '
    //   714: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   717: pop
    //   718: aload #4
    //   720: invokestatic access$800 : (Landroid/mtp/MtpStorageManager$MtpObject;)Z
    //   723: ifeq -> 733
    //   726: ldc_w ''
    //   729: astore_1
    //   730: goto -> 737
    //   733: ldc_w 'not '
    //   736: astore_1
    //   737: aload #8
    //   739: aload_1
    //   740: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   743: pop
    //   744: aload #8
    //   746: ldc_w ' visited but observer is '
    //   749: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   752: pop
    //   753: aload #8
    //   755: aload #4
    //   757: invokestatic access$600 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/os/FileObserver;
    //   760: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   763: pop
    //   764: aload #8
    //   766: invokevirtual toString : ()Ljava/lang/String;
    //   769: astore_1
    //   770: aload #5
    //   772: aload_1
    //   773: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   776: pop
    //   777: iconst_0
    //   778: istore_2
    //   779: iload_2
    //   780: istore #6
    //   782: aload #4
    //   784: invokestatic access$800 : (Landroid/mtp/MtpStorageManager$MtpObject;)Z
    //   787: ifne -> 850
    //   790: iload_2
    //   791: istore #6
    //   793: aload #4
    //   795: invokestatic access$1000 : (Landroid/mtp/MtpStorageManager$MtpObject;)Ljava/util/Collection;
    //   798: invokeinterface size : ()I
    //   803: ifle -> 850
    //   806: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   809: astore #5
    //   811: new java/lang/StringBuilder
    //   814: astore_1
    //   815: aload_1
    //   816: invokespecial <init> : ()V
    //   819: aload_1
    //   820: aload #4
    //   822: invokevirtual getPath : ()Ljava/nio/file/Path;
    //   825: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   828: pop
    //   829: aload_1
    //   830: ldc_w ' is not visited but has children'
    //   833: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   836: pop
    //   837: aload #5
    //   839: aload_1
    //   840: invokevirtual toString : ()Ljava/lang/String;
    //   843: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   846: pop
    //   847: iconst_0
    //   848: istore #6
    //   850: aload #4
    //   852: invokevirtual getPath : ()Ljava/nio/file/Path;
    //   855: invokestatic newDirectoryStream : (Ljava/nio/file/Path;)Ljava/nio/file/DirectoryStream;
    //   858: astore_1
    //   859: new java/util/HashSet
    //   862: astore #5
    //   864: aload #5
    //   866: invokespecial <init> : ()V
    //   869: aload_1
    //   870: invokeinterface iterator : ()Ljava/util/Iterator;
    //   875: astore #8
    //   877: aload #8
    //   879: invokeinterface hasNext : ()Z
    //   884: ifeq -> 1045
    //   887: aload #8
    //   889: invokeinterface next : ()Ljava/lang/Object;
    //   894: checkcast java/nio/file/Path
    //   897: astore #9
    //   899: iload #6
    //   901: istore_2
    //   902: aload #4
    //   904: invokestatic access$800 : (Landroid/mtp/MtpStorageManager$MtpObject;)Z
    //   907: ifeq -> 1024
    //   910: iload #6
    //   912: istore_2
    //   913: aload #4
    //   915: aload #9
    //   917: invokeinterface getFileName : ()Ljava/nio/file/Path;
    //   922: invokeinterface toString : ()Ljava/lang/String;
    //   927: invokestatic access$100 : (Landroid/mtp/MtpStorageManager$MtpObject;Ljava/lang/String;)Landroid/mtp/MtpStorageManager$MtpObject;
    //   930: ifnonnull -> 1024
    //   933: aload_0
    //   934: getfield mSubdirectories : Ljava/util/Set;
    //   937: ifnull -> 979
    //   940: aload #4
    //   942: invokevirtual isRoot : ()Z
    //   945: ifeq -> 979
    //   948: aload_0
    //   949: getfield mSubdirectories : Ljava/util/Set;
    //   952: astore #10
    //   954: iload #6
    //   956: istore_2
    //   957: aload #10
    //   959: aload #9
    //   961: invokeinterface getFileName : ()Ljava/nio/file/Path;
    //   966: invokeinterface toString : ()Ljava/lang/String;
    //   971: invokeinterface contains : (Ljava/lang/Object;)Z
    //   976: ifeq -> 1024
    //   979: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   982: astore #10
    //   984: new java/lang/StringBuilder
    //   987: astore #11
    //   989: aload #11
    //   991: invokespecial <init> : ()V
    //   994: aload #11
    //   996: ldc_w 'File exists in fs but not in children '
    //   999: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1002: pop
    //   1003: aload #11
    //   1005: aload #9
    //   1007: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1010: pop
    //   1011: aload #10
    //   1013: aload #11
    //   1015: invokevirtual toString : ()Ljava/lang/String;
    //   1018: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   1021: pop
    //   1022: iconst_0
    //   1023: istore_2
    //   1024: aload #5
    //   1026: aload #9
    //   1028: invokeinterface toString : ()Ljava/lang/String;
    //   1033: invokeinterface add : (Ljava/lang/Object;)Z
    //   1038: pop
    //   1039: iload_2
    //   1040: istore #6
    //   1042: goto -> 877
    //   1045: aload #4
    //   1047: invokestatic access$1000 : (Landroid/mtp/MtpStorageManager$MtpObject;)Ljava/util/Collection;
    //   1050: invokeinterface iterator : ()Ljava/util/Iterator;
    //   1055: astore #8
    //   1057: aload #8
    //   1059: invokeinterface hasNext : ()Z
    //   1064: ifeq -> 1220
    //   1067: aload #8
    //   1069: invokeinterface next : ()Ljava/lang/Object;
    //   1074: checkcast android/mtp/MtpStorageManager$MtpObject
    //   1077: astore #4
    //   1079: aload #5
    //   1081: aload #4
    //   1083: invokevirtual getPath : ()Ljava/nio/file/Path;
    //   1086: invokeinterface toString : ()Ljava/lang/String;
    //   1091: invokeinterface contains : (Ljava/lang/Object;)Z
    //   1096: ifne -> 1148
    //   1099: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   1102: astore #9
    //   1104: new java/lang/StringBuilder
    //   1107: astore #10
    //   1109: aload #10
    //   1111: invokespecial <init> : ()V
    //   1114: aload #10
    //   1116: ldc_w 'File in children doesn't exist in fs '
    //   1119: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1122: pop
    //   1123: aload #10
    //   1125: aload #4
    //   1127: invokevirtual getPath : ()Ljava/nio/file/Path;
    //   1130: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1133: pop
    //   1134: aload #9
    //   1136: aload #10
    //   1138: invokevirtual toString : ()Ljava/lang/String;
    //   1141: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   1144: pop
    //   1145: iconst_0
    //   1146: istore #6
    //   1148: aload #4
    //   1150: aload_0
    //   1151: getfield mObjects : Ljava/util/HashMap;
    //   1154: aload #4
    //   1156: invokevirtual getId : ()I
    //   1159: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1162: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   1165: if_acmpeq -> 1217
    //   1168: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   1171: astore #10
    //   1173: new java/lang/StringBuilder
    //   1176: astore #9
    //   1178: aload #9
    //   1180: invokespecial <init> : ()V
    //   1183: aload #9
    //   1185: ldc_w 'Child is not in object map '
    //   1188: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1191: pop
    //   1192: aload #9
    //   1194: aload #4
    //   1196: invokevirtual getPath : ()Ljava/nio/file/Path;
    //   1199: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1202: pop
    //   1203: aload #10
    //   1205: aload #9
    //   1207: invokevirtual toString : ()Ljava/lang/String;
    //   1210: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   1213: pop
    //   1214: iconst_0
    //   1215: istore #6
    //   1217: goto -> 1057
    //   1220: aload_1
    //   1221: ifnull -> 1230
    //   1224: aload_1
    //   1225: invokeinterface close : ()V
    //   1230: goto -> 1273
    //   1233: astore #4
    //   1235: aload_1
    //   1236: ifnull -> 1255
    //   1239: aload_1
    //   1240: invokeinterface close : ()V
    //   1245: goto -> 1255
    //   1248: astore_1
    //   1249: aload #4
    //   1251: aload_1
    //   1252: invokevirtual addSuppressed : (Ljava/lang/Throwable;)V
    //   1255: aload #4
    //   1257: athrow
    //   1258: astore_1
    //   1259: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   1262: aload_1
    //   1263: invokevirtual toString : ()Ljava/lang/String;
    //   1266: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   1269: pop
    //   1270: iconst_0
    //   1271: istore #6
    //   1273: iload #6
    //   1275: istore_2
    //   1276: goto -> 47
    //   1279: aload_0
    //   1280: monitorexit
    //   1281: iload_2
    //   1282: ireturn
    //   1283: astore_1
    //   1284: aload_0
    //   1285: monitorexit
    //   1286: aload_1
    //   1287: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #813	-> 2
    //   #814	-> 10
    //   #815	-> 24
    //   #816	-> 38
    //   #817	-> 40
    //   #818	-> 67
    //   #819	-> 75
    //   #820	-> 139
    //   #822	-> 141
    //   #823	-> 152
    //   #824	-> 211
    //   #826	-> 213
    //   #827	-> 224
    //   #828	-> 283
    //   #830	-> 285
    //   #831	-> 319
    //   #832	-> 372
    //   #834	-> 375
    //   #835	-> 386
    //   #836	-> 412
    //   #837	-> 435
    //   #838	-> 479
    //   #840	-> 481
    //   #841	-> 507
    //   #842	-> 530
    //   #843	-> 571
    //   #845	-> 574
    //   #846	-> 595
    //   #847	-> 639
    //   #850	-> 641
    //   #851	-> 652
    //   #852	-> 683
    //   #853	-> 753
    //   #852	-> 770
    //   #854	-> 777
    //   #856	-> 779
    //   #857	-> 806
    //   #858	-> 847
    //   #860	-> 850
    //   #861	-> 859
    //   #862	-> 869
    //   #863	-> 899
    //   #864	-> 910
    //   #865	-> 940
    //   #866	-> 954
    //   #867	-> 979
    //   #868	-> 1022
    //   #870	-> 1024
    //   #871	-> 1039
    //   #872	-> 1045
    //   #873	-> 1079
    //   #874	-> 1099
    //   #875	-> 1145
    //   #877	-> 1148
    //   #878	-> 1168
    //   #879	-> 1214
    //   #881	-> 1217
    //   #882	-> 1220
    //   #885	-> 1230
    //   #860	-> 1233
    //   #882	-> 1258
    //   #883	-> 1259
    //   #884	-> 1270
    //   #887	-> 1273
    //   #888	-> 1279
    //   #812	-> 1283
    // Exception table:
    //   from	to	target	type
    //   2	10	1283	finally
    //   10	24	1283	finally
    //   24	38	1283	finally
    //   40	47	1283	finally
    //   47	67	1283	finally
    //   67	75	1283	finally
    //   75	139	1283	finally
    //   141	152	1283	finally
    //   152	211	1283	finally
    //   213	224	1283	finally
    //   224	283	1283	finally
    //   288	296	1283	finally
    //   299	319	1283	finally
    //   319	372	1283	finally
    //   378	386	1283	finally
    //   389	412	1283	finally
    //   415	435	1283	finally
    //   435	479	1283	finally
    //   484	507	1283	finally
    //   510	530	1283	finally
    //   530	571	1283	finally
    //   577	595	1283	finally
    //   595	639	1283	finally
    //   644	652	1283	finally
    //   652	667	1283	finally
    //   683	726	1283	finally
    //   737	753	1283	finally
    //   753	770	1283	finally
    //   770	777	1283	finally
    //   782	790	1283	finally
    //   793	806	1283	finally
    //   806	847	1283	finally
    //   850	859	1258	java/io/IOException
    //   850	859	1258	java/nio/file/DirectoryIteratorException
    //   850	859	1283	finally
    //   859	869	1233	finally
    //   869	877	1233	finally
    //   877	899	1233	finally
    //   902	910	1233	finally
    //   913	940	1233	finally
    //   940	954	1233	finally
    //   957	979	1233	finally
    //   979	1022	1233	finally
    //   1024	1039	1233	finally
    //   1045	1057	1233	finally
    //   1057	1079	1233	finally
    //   1079	1099	1233	finally
    //   1099	1145	1233	finally
    //   1148	1168	1233	finally
    //   1168	1214	1233	finally
    //   1224	1230	1258	java/io/IOException
    //   1224	1230	1258	java/nio/file/DirectoryIteratorException
    //   1224	1230	1283	finally
    //   1239	1245	1248	finally
    //   1249	1255	1258	java/io/IOException
    //   1249	1255	1258	java/nio/file/DirectoryIteratorException
    //   1249	1255	1283	finally
    //   1255	1258	1258	java/io/IOException
    //   1255	1258	1258	java/nio/file/DirectoryIteratorException
    //   1255	1258	1283	finally
    //   1259	1270	1283	finally
  }
  
  public int beginSendObject(MtpObject paramMtpObject, String paramString, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: getstatic android/mtp/MtpStorageManager.sDebug : Z
    //   5: ifeq -> 50
    //   8: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   11: astore #4
    //   13: new java/lang/StringBuilder
    //   16: astore #5
    //   18: aload #5
    //   20: invokespecial <init> : ()V
    //   23: aload #5
    //   25: ldc_w 'beginSendObject '
    //   28: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   31: pop
    //   32: aload #5
    //   34: aload_2
    //   35: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   38: pop
    //   39: aload #4
    //   41: aload #5
    //   43: invokevirtual toString : ()Ljava/lang/String;
    //   46: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   49: pop
    //   50: aload_1
    //   51: invokevirtual isDir : ()Z
    //   54: istore #6
    //   56: iload #6
    //   58: ifne -> 65
    //   61: aload_0
    //   62: monitorexit
    //   63: iconst_m1
    //   64: ireturn
    //   65: aload_1
    //   66: invokevirtual isRoot : ()Z
    //   69: ifeq -> 100
    //   72: aload_0
    //   73: getfield mSubdirectories : Ljava/util/Set;
    //   76: ifnull -> 100
    //   79: aload_0
    //   80: getfield mSubdirectories : Ljava/util/Set;
    //   83: aload_2
    //   84: invokeinterface contains : (Ljava/lang/Object;)Z
    //   89: istore #6
    //   91: iload #6
    //   93: ifne -> 100
    //   96: aload_0
    //   97: monitorexit
    //   98: iconst_m1
    //   99: ireturn
    //   100: aload_0
    //   101: aload_1
    //   102: invokespecial getChildren : (Landroid/mtp/MtpStorageManager$MtpObject;)Ljava/util/Collection;
    //   105: pop
    //   106: iload_3
    //   107: sipush #12289
    //   110: if_icmpne -> 119
    //   113: iconst_1
    //   114: istore #6
    //   116: goto -> 122
    //   119: iconst_0
    //   120: istore #6
    //   122: aload_0
    //   123: aload_1
    //   124: aload_2
    //   125: iload #6
    //   127: invokespecial addObjectToCache : (Landroid/mtp/MtpStorageManager$MtpObject;Ljava/lang/String;Z)Landroid/mtp/MtpStorageManager$MtpObject;
    //   130: astore_1
    //   131: aload_1
    //   132: ifnonnull -> 139
    //   135: aload_0
    //   136: monitorexit
    //   137: iconst_m1
    //   138: ireturn
    //   139: aload_1
    //   140: getstatic android/mtp/MtpStorageManager$MtpObjectState.FROZEN : Landroid/mtp/MtpStorageManager$MtpObjectState;
    //   143: invokestatic access$1700 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObjectState;)V
    //   146: aload_1
    //   147: getstatic android/mtp/MtpStorageManager$MtpOperation.ADD : Landroid/mtp/MtpStorageManager$MtpOperation;
    //   150: invokestatic access$1900 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpOperation;)V
    //   153: aload_1
    //   154: invokevirtual getId : ()I
    //   157: istore_3
    //   158: aload_0
    //   159: monitorexit
    //   160: iload_3
    //   161: ireturn
    //   162: astore_1
    //   163: aload_0
    //   164: monitorexit
    //   165: aload_1
    //   166: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #898	-> 2
    //   #899	-> 8
    //   #900	-> 50
    //   #901	-> 61
    //   #902	-> 65
    //   #903	-> 96
    //   #904	-> 100
    //   #905	-> 106
    //   #906	-> 131
    //   #907	-> 135
    //   #908	-> 139
    //   #909	-> 146
    //   #910	-> 153
    //   #897	-> 162
    // Exception table:
    //   from	to	target	type
    //   2	8	162	finally
    //   8	50	162	finally
    //   50	56	162	finally
    //   65	91	162	finally
    //   100	106	162	finally
    //   122	131	162	finally
    //   139	146	162	finally
    //   146	153	162	finally
    //   153	158	162	finally
  }
  
  public boolean endSendObject(MtpObject paramMtpObject, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: getstatic android/mtp/MtpStorageManager.sDebug : Z
    //   5: ifeq -> 48
    //   8: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   11: astore_3
    //   12: new java/lang/StringBuilder
    //   15: astore #4
    //   17: aload #4
    //   19: invokespecial <init> : ()V
    //   22: aload #4
    //   24: ldc_w 'endSendObject '
    //   27: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   30: pop
    //   31: aload #4
    //   33: iload_2
    //   34: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   37: pop
    //   38: aload_3
    //   39: aload #4
    //   41: invokevirtual toString : ()Ljava/lang/String;
    //   44: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   47: pop
    //   48: aload_0
    //   49: aload_1
    //   50: iload_2
    //   51: iconst_1
    //   52: invokespecial generalEndAddObject : (Landroid/mtp/MtpStorageManager$MtpObject;ZZ)Z
    //   55: istore_2
    //   56: aload_0
    //   57: monitorexit
    //   58: iload_2
    //   59: ireturn
    //   60: astore_1
    //   61: aload_0
    //   62: monitorexit
    //   63: aload_1
    //   64: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #920	-> 2
    //   #921	-> 8
    //   #922	-> 48
    //   #919	-> 60
    // Exception table:
    //   from	to	target	type
    //   2	8	60	finally
    //   8	48	60	finally
    //   48	56	60	finally
  }
  
  public boolean beginRenameObject(MtpObject paramMtpObject, String paramString) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: getstatic android/mtp/MtpStorageManager.sDebug : Z
    //   5: ifeq -> 67
    //   8: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   11: astore_3
    //   12: new java/lang/StringBuilder
    //   15: astore #4
    //   17: aload #4
    //   19: invokespecial <init> : ()V
    //   22: aload #4
    //   24: ldc_w 'beginRenameObject '
    //   27: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   30: pop
    //   31: aload #4
    //   33: aload_1
    //   34: invokevirtual getName : ()Ljava/lang/String;
    //   37: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   40: pop
    //   41: aload #4
    //   43: ldc_w ' '
    //   46: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   49: pop
    //   50: aload #4
    //   52: aload_2
    //   53: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   56: pop
    //   57: aload_3
    //   58: aload #4
    //   60: invokevirtual toString : ()Ljava/lang/String;
    //   63: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   66: pop
    //   67: aload_1
    //   68: invokevirtual isRoot : ()Z
    //   71: istore #5
    //   73: iload #5
    //   75: ifeq -> 82
    //   78: aload_0
    //   79: monitorexit
    //   80: iconst_0
    //   81: ireturn
    //   82: aload_0
    //   83: aload_1
    //   84: invokespecial isSpecialSubDir : (Landroid/mtp/MtpStorageManager$MtpObject;)Z
    //   87: istore #5
    //   89: iload #5
    //   91: ifeq -> 98
    //   94: aload_0
    //   95: monitorexit
    //   96: iconst_0
    //   97: ireturn
    //   98: aload_1
    //   99: invokevirtual getParent : ()Landroid/mtp/MtpStorageManager$MtpObject;
    //   102: aload_2
    //   103: invokestatic access$100 : (Landroid/mtp/MtpStorageManager$MtpObject;Ljava/lang/String;)Landroid/mtp/MtpStorageManager$MtpObject;
    //   106: astore #4
    //   108: aload #4
    //   110: ifnull -> 117
    //   113: aload_0
    //   114: monitorexit
    //   115: iconst_0
    //   116: ireturn
    //   117: aload_1
    //   118: iconst_0
    //   119: invokestatic access$2000 : (Landroid/mtp/MtpStorageManager$MtpObject;Z)Landroid/mtp/MtpStorageManager$MtpObject;
    //   122: astore #4
    //   124: aload_1
    //   125: aload_2
    //   126: invokestatic access$2100 : (Landroid/mtp/MtpStorageManager$MtpObject;Ljava/lang/String;)V
    //   129: aload_1
    //   130: invokevirtual getParent : ()Landroid/mtp/MtpStorageManager$MtpObject;
    //   133: aload_1
    //   134: invokestatic access$1200 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObject;)V
    //   137: aload #4
    //   139: invokevirtual getParent : ()Landroid/mtp/MtpStorageManager$MtpObject;
    //   142: aload #4
    //   144: invokestatic access$1200 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObject;)V
    //   147: aload_0
    //   148: aload #4
    //   150: aload_1
    //   151: invokespecial generalBeginRenameObject : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObject;)Z
    //   154: istore #5
    //   156: aload_0
    //   157: monitorexit
    //   158: iload #5
    //   160: ireturn
    //   161: astore_1
    //   162: aload_0
    //   163: monitorexit
    //   164: aload_1
    //   165: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #933	-> 2
    //   #934	-> 8
    //   #935	-> 67
    //   #936	-> 78
    //   #937	-> 82
    //   #938	-> 94
    //   #939	-> 98
    //   #941	-> 113
    //   #943	-> 117
    //   #944	-> 124
    //   #945	-> 129
    //   #946	-> 137
    //   #947	-> 147
    //   #932	-> 161
    // Exception table:
    //   from	to	target	type
    //   2	8	161	finally
    //   8	67	161	finally
    //   67	73	161	finally
    //   82	89	161	finally
    //   98	108	161	finally
    //   117	124	161	finally
    //   124	129	161	finally
    //   129	137	161	finally
    //   137	147	161	finally
    //   147	156	161	finally
  }
  
  public boolean endRenameObject(MtpObject paramMtpObject, String paramString, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: getstatic android/mtp/MtpStorageManager.sDebug : Z
    //   5: ifeq -> 50
    //   8: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   11: astore #4
    //   13: new java/lang/StringBuilder
    //   16: astore #5
    //   18: aload #5
    //   20: invokespecial <init> : ()V
    //   23: aload #5
    //   25: ldc_w 'endRenameObject '
    //   28: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   31: pop
    //   32: aload #5
    //   34: iload_3
    //   35: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   38: pop
    //   39: aload #4
    //   41: aload #5
    //   43: invokevirtual toString : ()Ljava/lang/String;
    //   46: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   49: pop
    //   50: aload_1
    //   51: invokevirtual getParent : ()Landroid/mtp/MtpStorageManager$MtpObject;
    //   54: astore #6
    //   56: aload #6
    //   58: aload_2
    //   59: invokestatic access$100 : (Landroid/mtp/MtpStorageManager$MtpObject;Ljava/lang/String;)Landroid/mtp/MtpStorageManager$MtpObject;
    //   62: astore #4
    //   64: aload #4
    //   66: astore #7
    //   68: aload_1
    //   69: astore #5
    //   71: iload_3
    //   72: ifne -> 131
    //   75: aload #4
    //   77: invokestatic access$1400 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/mtp/MtpStorageManager$MtpObjectState;
    //   80: astore #5
    //   82: aload #4
    //   84: aload_1
    //   85: invokevirtual getName : ()Ljava/lang/String;
    //   88: invokestatic access$2100 : (Landroid/mtp/MtpStorageManager$MtpObject;Ljava/lang/String;)V
    //   91: aload #4
    //   93: aload_1
    //   94: invokestatic access$1400 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/mtp/MtpStorageManager$MtpObjectState;
    //   97: invokestatic access$1700 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObjectState;)V
    //   100: aload_1
    //   101: aload_2
    //   102: invokestatic access$2100 : (Landroid/mtp/MtpStorageManager$MtpObject;Ljava/lang/String;)V
    //   105: aload_1
    //   106: aload #5
    //   108: invokestatic access$1700 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObjectState;)V
    //   111: aload #6
    //   113: aload #4
    //   115: invokestatic access$1200 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObject;)V
    //   118: aload #6
    //   120: aload_1
    //   121: invokestatic access$1200 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObject;)V
    //   124: aload #4
    //   126: astore #5
    //   128: aload_1
    //   129: astore #7
    //   131: aload_0
    //   132: aload #7
    //   134: aload #5
    //   136: iload_3
    //   137: invokespecial generalEndRenameObject : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObject;Z)Z
    //   140: istore_3
    //   141: aload_0
    //   142: monitorexit
    //   143: iload_3
    //   144: ireturn
    //   145: astore_1
    //   146: aload_0
    //   147: monitorexit
    //   148: aload_1
    //   149: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #958	-> 2
    //   #959	-> 8
    //   #960	-> 50
    //   #961	-> 56
    //   #962	-> 64
    //   #965	-> 75
    //   #966	-> 75
    //   #967	-> 82
    //   #968	-> 91
    //   #969	-> 100
    //   #970	-> 100
    //   #971	-> 105
    //   #972	-> 111
    //   #973	-> 111
    //   #974	-> 118
    //   #976	-> 131
    //   #957	-> 145
    // Exception table:
    //   from	to	target	type
    //   2	8	145	finally
    //   8	50	145	finally
    //   50	56	145	finally
    //   56	64	145	finally
    //   75	82	145	finally
    //   82	91	145	finally
    //   91	100	145	finally
    //   100	105	145	finally
    //   105	111	145	finally
    //   111	118	145	finally
    //   118	124	145	finally
    //   131	141	145	finally
  }
  
  public boolean beginRemoveObject(MtpObject paramMtpObject) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: getstatic android/mtp/MtpStorageManager.sDebug : Z
    //   5: ifeq -> 46
    //   8: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   11: astore_2
    //   12: new java/lang/StringBuilder
    //   15: astore_3
    //   16: aload_3
    //   17: invokespecial <init> : ()V
    //   20: aload_3
    //   21: ldc_w 'beginRemoveObject '
    //   24: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   27: pop
    //   28: aload_3
    //   29: aload_1
    //   30: invokevirtual getName : ()Ljava/lang/String;
    //   33: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   36: pop
    //   37: aload_2
    //   38: aload_3
    //   39: invokevirtual toString : ()Ljava/lang/String;
    //   42: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   45: pop
    //   46: aload_1
    //   47: invokevirtual isRoot : ()Z
    //   50: ifne -> 84
    //   53: aload_0
    //   54: aload_1
    //   55: invokespecial isSpecialSubDir : (Landroid/mtp/MtpStorageManager$MtpObject;)Z
    //   58: ifne -> 84
    //   61: getstatic android/mtp/MtpStorageManager$MtpOperation.DELETE : Landroid/mtp/MtpStorageManager$MtpOperation;
    //   64: astore_3
    //   65: aload_0
    //   66: aload_1
    //   67: aload_3
    //   68: invokespecial generalBeginRemoveObject : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpOperation;)Z
    //   71: istore #4
    //   73: iload #4
    //   75: ifeq -> 84
    //   78: iconst_1
    //   79: istore #4
    //   81: goto -> 87
    //   84: iconst_0
    //   85: istore #4
    //   87: aload_0
    //   88: monitorexit
    //   89: iload #4
    //   91: ireturn
    //   92: astore_1
    //   93: aload_0
    //   94: monitorexit
    //   95: aload_1
    //   96: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #986	-> 2
    //   #987	-> 8
    //   #988	-> 46
    //   #989	-> 65
    //   #988	-> 87
    //   #985	-> 92
    // Exception table:
    //   from	to	target	type
    //   2	8	92	finally
    //   8	46	92	finally
    //   46	65	92	finally
    //   65	73	92	finally
  }
  
  public boolean endRemoveObject(MtpObject paramMtpObject, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: getstatic android/mtp/MtpStorageManager.sDebug : Z
    //   5: ifeq -> 48
    //   8: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   11: astore_3
    //   12: new java/lang/StringBuilder
    //   15: astore #4
    //   17: aload #4
    //   19: invokespecial <init> : ()V
    //   22: aload #4
    //   24: ldc_w 'endRemoveObject '
    //   27: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   30: pop
    //   31: aload #4
    //   33: iload_2
    //   34: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   37: pop
    //   38: aload_3
    //   39: aload #4
    //   41: invokevirtual toString : ()Ljava/lang/String;
    //   44: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   47: pop
    //   48: iconst_1
    //   49: istore #5
    //   51: iconst_1
    //   52: istore #6
    //   54: aload_1
    //   55: invokevirtual isDir : ()Z
    //   58: istore #7
    //   60: iconst_0
    //   61: istore #8
    //   63: iload #7
    //   65: ifeq -> 159
    //   68: new java/util/ArrayList
    //   71: astore_3
    //   72: aload_3
    //   73: aload_1
    //   74: invokestatic access$1000 : (Landroid/mtp/MtpStorageManager$MtpObject;)Ljava/util/Collection;
    //   77: invokespecial <init> : (Ljava/util/Collection;)V
    //   80: aload_3
    //   81: invokevirtual iterator : ()Ljava/util/Iterator;
    //   84: astore_3
    //   85: iload #6
    //   87: istore #5
    //   89: aload_3
    //   90: invokeinterface hasNext : ()Z
    //   95: ifeq -> 159
    //   98: aload_3
    //   99: invokeinterface next : ()Ljava/lang/Object;
    //   104: checkcast android/mtp/MtpStorageManager$MtpObject
    //   107: astore #4
    //   109: iload #6
    //   111: istore #5
    //   113: aload #4
    //   115: invokestatic access$1500 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/mtp/MtpStorageManager$MtpOperation;
    //   118: getstatic android/mtp/MtpStorageManager$MtpOperation.DELETE : Landroid/mtp/MtpStorageManager$MtpOperation;
    //   121: if_acmpne -> 152
    //   124: aload_0
    //   125: aload #4
    //   127: iload_2
    //   128: invokevirtual endRemoveObject : (Landroid/mtp/MtpStorageManager$MtpObject;Z)Z
    //   131: ifeq -> 145
    //   134: iload #6
    //   136: ifeq -> 145
    //   139: iconst_1
    //   140: istore #6
    //   142: goto -> 148
    //   145: iconst_0
    //   146: istore #6
    //   148: iload #6
    //   150: istore #5
    //   152: iload #5
    //   154: istore #6
    //   156: goto -> 85
    //   159: aload_0
    //   160: aload_1
    //   161: iload_2
    //   162: iconst_1
    //   163: invokespecial generalEndRemoveObject : (Landroid/mtp/MtpStorageManager$MtpObject;ZZ)Z
    //   166: istore #7
    //   168: iload #8
    //   170: istore_2
    //   171: iload #7
    //   173: ifeq -> 186
    //   176: iload #8
    //   178: istore_2
    //   179: iload #5
    //   181: ifeq -> 186
    //   184: iconst_1
    //   185: istore_2
    //   186: aload_0
    //   187: monitorexit
    //   188: iload_2
    //   189: ireturn
    //   190: astore_1
    //   191: aload_0
    //   192: monitorexit
    //   193: aload_1
    //   194: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #999	-> 2
    //   #1000	-> 8
    //   #1001	-> 48
    //   #1002	-> 54
    //   #1003	-> 68
    //   #1004	-> 109
    //   #1005	-> 124
    //   #1004	-> 152
    //   #1007	-> 159
    //   #998	-> 190
    // Exception table:
    //   from	to	target	type
    //   2	8	190	finally
    //   8	48	190	finally
    //   54	60	190	finally
    //   68	85	190	finally
    //   89	109	190	finally
    //   113	124	190	finally
    //   124	134	190	finally
    //   159	168	190	finally
  }
  
  public boolean beginMoveObject(MtpObject paramMtpObject1, MtpObject paramMtpObject2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: getstatic android/mtp/MtpStorageManager.sDebug : Z
    //   5: ifeq -> 51
    //   8: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   11: astore_3
    //   12: new java/lang/StringBuilder
    //   15: astore #4
    //   17: aload #4
    //   19: invokespecial <init> : ()V
    //   22: aload #4
    //   24: ldc_w 'beginMoveObject '
    //   27: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   30: pop
    //   31: aload #4
    //   33: aload_2
    //   34: invokevirtual getPath : ()Ljava/nio/file/Path;
    //   37: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   40: pop
    //   41: aload_3
    //   42: aload #4
    //   44: invokevirtual toString : ()Ljava/lang/String;
    //   47: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   50: pop
    //   51: aload_1
    //   52: invokevirtual isRoot : ()Z
    //   55: istore #5
    //   57: iconst_0
    //   58: istore #6
    //   60: iload #5
    //   62: ifeq -> 69
    //   65: aload_0
    //   66: monitorexit
    //   67: iconst_0
    //   68: ireturn
    //   69: aload_0
    //   70: aload_1
    //   71: invokespecial isSpecialSubDir : (Landroid/mtp/MtpStorageManager$MtpObject;)Z
    //   74: istore #5
    //   76: iload #5
    //   78: ifeq -> 85
    //   81: aload_0
    //   82: monitorexit
    //   83: iconst_0
    //   84: ireturn
    //   85: aload_0
    //   86: aload_2
    //   87: invokespecial getChildren : (Landroid/mtp/MtpStorageManager$MtpObject;)Ljava/util/Collection;
    //   90: pop
    //   91: aload_2
    //   92: aload_1
    //   93: invokevirtual getName : ()Ljava/lang/String;
    //   96: invokestatic access$100 : (Landroid/mtp/MtpStorageManager$MtpObject;Ljava/lang/String;)Landroid/mtp/MtpStorageManager$MtpObject;
    //   99: astore #4
    //   101: aload #4
    //   103: ifnull -> 110
    //   106: aload_0
    //   107: monitorexit
    //   108: iconst_0
    //   109: ireturn
    //   110: aload_1
    //   111: invokevirtual getStorageId : ()I
    //   114: aload_2
    //   115: invokevirtual getStorageId : ()I
    //   118: if_icmpeq -> 176
    //   121: aload_1
    //   122: iconst_1
    //   123: invokestatic access$2000 : (Landroid/mtp/MtpStorageManager$MtpObject;Z)Landroid/mtp/MtpStorageManager$MtpObject;
    //   126: astore #4
    //   128: aload #4
    //   130: aload_2
    //   131: invokestatic access$2200 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObject;)V
    //   134: aload_2
    //   135: aload #4
    //   137: invokestatic access$1200 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObject;)V
    //   140: aload_0
    //   141: aload_1
    //   142: getstatic android/mtp/MtpStorageManager$MtpOperation.RENAME : Landroid/mtp/MtpStorageManager$MtpOperation;
    //   145: invokespecial generalBeginRemoveObject : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpOperation;)Z
    //   148: ifeq -> 171
    //   151: aload_0
    //   152: aload #4
    //   154: iconst_0
    //   155: invokespecial generalBeginCopyObject : (Landroid/mtp/MtpStorageManager$MtpObject;Z)Z
    //   158: istore #5
    //   160: iload #5
    //   162: ifeq -> 171
    //   165: iconst_1
    //   166: istore #6
    //   168: goto -> 171
    //   171: aload_0
    //   172: monitorexit
    //   173: iload #6
    //   175: ireturn
    //   176: aload_1
    //   177: iconst_0
    //   178: invokestatic access$2000 : (Landroid/mtp/MtpStorageManager$MtpObject;Z)Landroid/mtp/MtpStorageManager$MtpObject;
    //   181: astore #4
    //   183: aload_1
    //   184: aload_2
    //   185: invokestatic access$2200 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObject;)V
    //   188: aload #4
    //   190: invokevirtual getParent : ()Landroid/mtp/MtpStorageManager$MtpObject;
    //   193: aload #4
    //   195: invokestatic access$1200 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObject;)V
    //   198: aload_1
    //   199: invokevirtual getParent : ()Landroid/mtp/MtpStorageManager$MtpObject;
    //   202: aload_1
    //   203: invokestatic access$1200 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObject;)V
    //   206: aload_0
    //   207: aload #4
    //   209: aload_1
    //   210: invokespecial generalBeginRenameObject : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObject;)Z
    //   213: istore #6
    //   215: aload_0
    //   216: monitorexit
    //   217: iload #6
    //   219: ireturn
    //   220: astore_1
    //   221: aload_0
    //   222: monitorexit
    //   223: aload_1
    //   224: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1017	-> 2
    //   #1018	-> 8
    //   #1019	-> 51
    //   #1020	-> 65
    //   #1021	-> 69
    //   #1022	-> 81
    //   #1023	-> 85
    //   #1024	-> 91
    //   #1026	-> 106
    //   #1027	-> 110
    //   #1033	-> 121
    //   #1034	-> 128
    //   #1035	-> 134
    //   #1036	-> 140
    //   #1037	-> 151
    //   #1036	-> 171
    //   #1040	-> 176
    //   #1041	-> 183
    //   #1042	-> 188
    //   #1043	-> 198
    //   #1044	-> 206
    //   #1016	-> 220
    // Exception table:
    //   from	to	target	type
    //   2	8	220	finally
    //   8	51	220	finally
    //   51	57	220	finally
    //   69	76	220	finally
    //   85	91	220	finally
    //   91	101	220	finally
    //   110	121	220	finally
    //   121	128	220	finally
    //   128	134	220	finally
    //   134	140	220	finally
    //   140	151	220	finally
    //   151	160	220	finally
    //   176	183	220	finally
    //   183	188	220	finally
    //   188	198	220	finally
    //   198	206	220	finally
    //   206	215	220	finally
  }
  
  public boolean endMoveObject(MtpObject paramMtpObject1, MtpObject paramMtpObject2, String paramString, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: getstatic android/mtp/MtpStorageManager.sDebug : Z
    //   5: ifeq -> 51
    //   8: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   11: astore #5
    //   13: new java/lang/StringBuilder
    //   16: astore #6
    //   18: aload #6
    //   20: invokespecial <init> : ()V
    //   23: aload #6
    //   25: ldc_w 'endMoveObject '
    //   28: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   31: pop
    //   32: aload #6
    //   34: iload #4
    //   36: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   39: pop
    //   40: aload #5
    //   42: aload #6
    //   44: invokevirtual toString : ()Ljava/lang/String;
    //   47: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   50: pop
    //   51: aload_1
    //   52: aload_3
    //   53: invokestatic access$100 : (Landroid/mtp/MtpStorageManager$MtpObject;Ljava/lang/String;)Landroid/mtp/MtpStorageManager$MtpObject;
    //   56: astore #5
    //   58: aload_2
    //   59: aload_3
    //   60: invokestatic access$100 : (Landroid/mtp/MtpStorageManager$MtpObject;Ljava/lang/String;)Landroid/mtp/MtpStorageManager$MtpObject;
    //   63: astore_2
    //   64: iconst_0
    //   65: istore #7
    //   67: aload #5
    //   69: ifnull -> 216
    //   72: aload_2
    //   73: ifnonnull -> 79
    //   76: goto -> 216
    //   79: aload_1
    //   80: invokevirtual getStorageId : ()I
    //   83: aload_2
    //   84: invokevirtual getStorageId : ()I
    //   87: if_icmpeq -> 136
    //   90: aload_0
    //   91: aload #5
    //   93: iload #4
    //   95: invokevirtual endRemoveObject : (Landroid/mtp/MtpStorageManager$MtpObject;Z)Z
    //   98: istore #8
    //   100: aload_0
    //   101: aload_2
    //   102: iload #4
    //   104: iconst_1
    //   105: invokespecial generalEndCopyObject : (Landroid/mtp/MtpStorageManager$MtpObject;ZZ)Z
    //   108: istore #9
    //   110: iload #7
    //   112: istore #4
    //   114: iload #9
    //   116: ifeq -> 131
    //   119: iload #7
    //   121: istore #4
    //   123: iload #8
    //   125: ifeq -> 131
    //   128: iconst_1
    //   129: istore #4
    //   131: aload_0
    //   132: monitorexit
    //   133: iload #4
    //   135: ireturn
    //   136: aload #5
    //   138: astore #6
    //   140: aload_2
    //   141: astore_3
    //   142: iload #4
    //   144: ifne -> 200
    //   147: aload #5
    //   149: invokestatic access$1400 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/mtp/MtpStorageManager$MtpObjectState;
    //   152: astore_3
    //   153: aload #5
    //   155: aload_2
    //   156: invokevirtual getParent : ()Landroid/mtp/MtpStorageManager$MtpObject;
    //   159: invokestatic access$2200 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObject;)V
    //   162: aload #5
    //   164: aload_2
    //   165: invokestatic access$1400 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/mtp/MtpStorageManager$MtpObjectState;
    //   168: invokestatic access$1700 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObjectState;)V
    //   171: aload_2
    //   172: aload_1
    //   173: invokestatic access$2200 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObject;)V
    //   176: aload_2
    //   177: aload_3
    //   178: invokestatic access$1700 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObjectState;)V
    //   181: aload #5
    //   183: astore_3
    //   184: aload_3
    //   185: invokevirtual getParent : ()Landroid/mtp/MtpStorageManager$MtpObject;
    //   188: aload_3
    //   189: invokestatic access$1200 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObject;)V
    //   192: aload_1
    //   193: aload_2
    //   194: invokestatic access$1200 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObject;)V
    //   197: aload_2
    //   198: astore #6
    //   200: aload_0
    //   201: aload #6
    //   203: aload_3
    //   204: iload #4
    //   206: invokespecial generalEndRenameObject : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObject;Z)Z
    //   209: istore #4
    //   211: aload_0
    //   212: monitorexit
    //   213: iload #4
    //   215: ireturn
    //   216: aload_0
    //   217: monitorexit
    //   218: iconst_0
    //   219: ireturn
    //   220: astore_1
    //   221: aload_0
    //   222: monitorexit
    //   223: aload_1
    //   224: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1057	-> 2
    //   #1058	-> 8
    //   #1059	-> 51
    //   #1060	-> 58
    //   #1061	-> 64
    //   #1063	-> 79
    //   #1064	-> 90
    //   #1065	-> 100
    //   #1067	-> 136
    //   #1070	-> 147
    //   #1071	-> 147
    //   #1072	-> 153
    //   #1073	-> 162
    //   #1074	-> 171
    //   #1075	-> 171
    //   #1076	-> 176
    //   #1077	-> 181
    //   #1078	-> 184
    //   #1079	-> 192
    //   #1081	-> 200
    //   #1062	-> 216
    //   #1056	-> 220
    // Exception table:
    //   from	to	target	type
    //   2	8	220	finally
    //   8	51	220	finally
    //   51	58	220	finally
    //   58	64	220	finally
    //   79	90	220	finally
    //   90	100	220	finally
    //   100	110	220	finally
    //   147	153	220	finally
    //   153	162	220	finally
    //   162	171	220	finally
    //   171	176	220	finally
    //   176	181	220	finally
    //   184	192	220	finally
    //   192	197	220	finally
    //   200	211	220	finally
  }
  
  public int beginCopyObject(MtpObject paramMtpObject1, MtpObject paramMtpObject2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: getstatic android/mtp/MtpStorageManager.sDebug : Z
    //   5: ifeq -> 70
    //   8: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   11: astore_3
    //   12: new java/lang/StringBuilder
    //   15: astore #4
    //   17: aload #4
    //   19: invokespecial <init> : ()V
    //   22: aload #4
    //   24: ldc_w 'beginCopyObject '
    //   27: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   30: pop
    //   31: aload #4
    //   33: aload_1
    //   34: invokevirtual getName : ()Ljava/lang/String;
    //   37: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   40: pop
    //   41: aload #4
    //   43: ldc_w ' to '
    //   46: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   49: pop
    //   50: aload #4
    //   52: aload_2
    //   53: invokevirtual getPath : ()Ljava/nio/file/Path;
    //   56: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   59: pop
    //   60: aload_3
    //   61: aload #4
    //   63: invokevirtual toString : ()Ljava/lang/String;
    //   66: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   69: pop
    //   70: aload_1
    //   71: invokevirtual getName : ()Ljava/lang/String;
    //   74: astore_3
    //   75: aload_2
    //   76: invokevirtual isDir : ()Z
    //   79: istore #5
    //   81: iload #5
    //   83: ifne -> 90
    //   86: aload_0
    //   87: monitorexit
    //   88: iconst_m1
    //   89: ireturn
    //   90: aload_2
    //   91: invokevirtual isRoot : ()Z
    //   94: ifeq -> 125
    //   97: aload_0
    //   98: getfield mSubdirectories : Ljava/util/Set;
    //   101: ifnull -> 125
    //   104: aload_0
    //   105: getfield mSubdirectories : Ljava/util/Set;
    //   108: aload_3
    //   109: invokeinterface contains : (Ljava/lang/Object;)Z
    //   114: istore #5
    //   116: iload #5
    //   118: ifne -> 125
    //   121: aload_0
    //   122: monitorexit
    //   123: iconst_m1
    //   124: ireturn
    //   125: aload_0
    //   126: aload_2
    //   127: invokespecial getChildren : (Landroid/mtp/MtpStorageManager$MtpObject;)Ljava/util/Collection;
    //   130: pop
    //   131: aload_2
    //   132: aload_3
    //   133: invokestatic access$100 : (Landroid/mtp/MtpStorageManager$MtpObject;Ljava/lang/String;)Landroid/mtp/MtpStorageManager$MtpObject;
    //   136: astore_3
    //   137: aload_3
    //   138: ifnull -> 145
    //   141: aload_0
    //   142: monitorexit
    //   143: iconst_m1
    //   144: ireturn
    //   145: aload_1
    //   146: aload_1
    //   147: invokevirtual isDir : ()Z
    //   150: invokestatic access$2000 : (Landroid/mtp/MtpStorageManager$MtpObject;Z)Landroid/mtp/MtpStorageManager$MtpObject;
    //   153: astore_1
    //   154: aload_2
    //   155: aload_1
    //   156: invokestatic access$1200 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObject;)V
    //   159: aload_1
    //   160: aload_2
    //   161: invokestatic access$2200 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObject;)V
    //   164: aload_0
    //   165: aload_1
    //   166: iconst_1
    //   167: invokespecial generalBeginCopyObject : (Landroid/mtp/MtpStorageManager$MtpObject;Z)Z
    //   170: istore #5
    //   172: iload #5
    //   174: ifne -> 181
    //   177: aload_0
    //   178: monitorexit
    //   179: iconst_m1
    //   180: ireturn
    //   181: aload_1
    //   182: invokevirtual getId : ()I
    //   185: istore #6
    //   187: aload_0
    //   188: monitorexit
    //   189: iload #6
    //   191: ireturn
    //   192: astore_1
    //   193: aload_0
    //   194: monitorexit
    //   195: aload_1
    //   196: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1091	-> 2
    //   #1092	-> 8
    //   #1093	-> 70
    //   #1094	-> 75
    //   #1095	-> 86
    //   #1096	-> 90
    //   #1097	-> 121
    //   #1098	-> 125
    //   #1099	-> 131
    //   #1100	-> 141
    //   #1101	-> 145
    //   #1102	-> 154
    //   #1103	-> 159
    //   #1104	-> 164
    //   #1105	-> 177
    //   #1106	-> 181
    //   #1090	-> 192
    // Exception table:
    //   from	to	target	type
    //   2	8	192	finally
    //   8	70	192	finally
    //   70	75	192	finally
    //   75	81	192	finally
    //   90	116	192	finally
    //   125	131	192	finally
    //   131	137	192	finally
    //   145	154	192	finally
    //   154	159	192	finally
    //   159	164	192	finally
    //   164	172	192	finally
    //   181	187	192	finally
  }
  
  public boolean endCopyObject(MtpObject paramMtpObject, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: getstatic android/mtp/MtpStorageManager.sDebug : Z
    //   5: ifeq -> 67
    //   8: getstatic android/mtp/MtpStorageManager.TAG : Ljava/lang/String;
    //   11: astore_3
    //   12: new java/lang/StringBuilder
    //   15: astore #4
    //   17: aload #4
    //   19: invokespecial <init> : ()V
    //   22: aload #4
    //   24: ldc_w 'endCopyObject '
    //   27: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   30: pop
    //   31: aload #4
    //   33: aload_1
    //   34: invokevirtual getName : ()Ljava/lang/String;
    //   37: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   40: pop
    //   41: aload #4
    //   43: ldc_w ' '
    //   46: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   49: pop
    //   50: aload #4
    //   52: iload_2
    //   53: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   56: pop
    //   57: aload_3
    //   58: aload #4
    //   60: invokevirtual toString : ()Ljava/lang/String;
    //   63: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   66: pop
    //   67: aload_0
    //   68: aload_1
    //   69: iload_2
    //   70: iconst_0
    //   71: invokespecial generalEndCopyObject : (Landroid/mtp/MtpStorageManager$MtpObject;ZZ)Z
    //   74: istore_2
    //   75: aload_0
    //   76: monitorexit
    //   77: iload_2
    //   78: ireturn
    //   79: astore_1
    //   80: aload_0
    //   81: monitorexit
    //   82: aload_1
    //   83: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1116	-> 2
    //   #1117	-> 8
    //   #1118	-> 67
    //   #1115	-> 79
    // Exception table:
    //   from	to	target	type
    //   2	8	79	finally
    //   8	67	79	finally
    //   67	75	79	finally
  }
  
  private boolean generalEndAddObject(MtpObject paramMtpObject, boolean paramBoolean1, boolean paramBoolean2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: getstatic android/mtp/MtpStorageManager$1.$SwitchMap$android$mtp$MtpStorageManager$MtpObjectState : [I
    //   5: aload_1
    //   6: invokestatic access$1400 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/mtp/MtpStorageManager$MtpObjectState;
    //   9: invokevirtual ordinal : ()I
    //   12: iaload
    //   13: istore #4
    //   15: iload #4
    //   17: iconst_1
    //   18: if_icmpeq -> 121
    //   21: iload #4
    //   23: iconst_2
    //   24: if_icmpeq -> 87
    //   27: iload #4
    //   29: iconst_5
    //   30: if_icmpeq -> 37
    //   33: aload_0
    //   34: monitorexit
    //   35: iconst_0
    //   36: ireturn
    //   37: aload_1
    //   38: getstatic android/mtp/MtpStorageManager$MtpObjectState.NORMAL : Landroid/mtp/MtpStorageManager$MtpObjectState;
    //   41: invokestatic access$1700 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObjectState;)V
    //   44: iload_2
    //   45: ifne -> 151
    //   48: aload_1
    //   49: invokevirtual getParent : ()Landroid/mtp/MtpStorageManager$MtpObject;
    //   52: astore #5
    //   54: aload_0
    //   55: aload_1
    //   56: iload_3
    //   57: iconst_0
    //   58: invokespecial removeObjectFromCache : (Landroid/mtp/MtpStorageManager$MtpObject;ZZ)Z
    //   61: istore_2
    //   62: iload_2
    //   63: ifne -> 70
    //   66: aload_0
    //   67: monitorexit
    //   68: iconst_0
    //   69: ireturn
    //   70: aload_0
    //   71: aload #5
    //   73: aload_1
    //   74: invokevirtual getName : ()Ljava/lang/String;
    //   77: aload_1
    //   78: invokevirtual isDir : ()Z
    //   81: invokespecial handleAddedObject : (Landroid/mtp/MtpStorageManager$MtpObject;Ljava/lang/String;Z)V
    //   84: goto -> 151
    //   87: aload_0
    //   88: aload_1
    //   89: iload_3
    //   90: iconst_0
    //   91: invokespecial removeObjectFromCache : (Landroid/mtp/MtpStorageManager$MtpObject;ZZ)Z
    //   94: istore_3
    //   95: iload_3
    //   96: ifne -> 103
    //   99: aload_0
    //   100: monitorexit
    //   101: iconst_0
    //   102: ireturn
    //   103: iload_2
    //   104: ifeq -> 151
    //   107: aload_0
    //   108: getfield mMtpNotifier : Landroid/mtp/MtpStorageManager$MtpNotifier;
    //   111: aload_1
    //   112: invokevirtual getId : ()I
    //   115: invokevirtual sendObjectRemoved : (I)V
    //   118: goto -> 151
    //   121: iload_2
    //   122: ifeq -> 135
    //   125: aload_1
    //   126: getstatic android/mtp/MtpStorageManager$MtpObjectState.FROZEN_ONESHOT_ADD : Landroid/mtp/MtpStorageManager$MtpObjectState;
    //   129: invokestatic access$1700 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObjectState;)V
    //   132: goto -> 151
    //   135: aload_0
    //   136: aload_1
    //   137: iload_3
    //   138: iconst_0
    //   139: invokespecial removeObjectFromCache : (Landroid/mtp/MtpStorageManager$MtpObject;ZZ)Z
    //   142: istore_2
    //   143: iload_2
    //   144: ifne -> 151
    //   147: aload_0
    //   148: monitorexit
    //   149: iconst_0
    //   150: ireturn
    //   151: aload_0
    //   152: monitorexit
    //   153: iconst_1
    //   154: ireturn
    //   155: astore_1
    //   156: aload_0
    //   157: monitorexit
    //   158: aload_1
    //   159: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1123	-> 2
    //   #1157	-> 33
    //   #1137	-> 37
    //   #1138	-> 44
    //   #1139	-> 48
    //   #1141	-> 54
    //   #1142	-> 66
    //   #1143	-> 70
    //   #1144	-> 84
    //   #1148	-> 87
    //   #1149	-> 99
    //   #1150	-> 103
    //   #1152	-> 107
    //   #1126	-> 121
    //   #1128	-> 125
    //   #1131	-> 135
    //   #1132	-> 147
    //   #1159	-> 151
    //   #1122	-> 155
    // Exception table:
    //   from	to	target	type
    //   2	15	155	finally
    //   37	44	155	finally
    //   48	54	155	finally
    //   54	62	155	finally
    //   70	84	155	finally
    //   87	95	155	finally
    //   107	118	155	finally
    //   125	132	155	finally
    //   135	143	155	finally
  }
  
  private boolean generalEndRemoveObject(MtpObject paramMtpObject, boolean paramBoolean1, boolean paramBoolean2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: getstatic android/mtp/MtpStorageManager$1.$SwitchMap$android$mtp$MtpStorageManager$MtpObjectState : [I
    //   5: aload_1
    //   6: invokestatic access$1400 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/mtp/MtpStorageManager$MtpObjectState;
    //   9: invokevirtual ordinal : ()I
    //   12: iaload
    //   13: istore #4
    //   15: iload #4
    //   17: iconst_1
    //   18: if_icmpeq -> 121
    //   21: iload #4
    //   23: iconst_2
    //   24: if_icmpeq -> 87
    //   27: iload #4
    //   29: iconst_5
    //   30: if_icmpeq -> 37
    //   33: aload_0
    //   34: monitorexit
    //   35: iconst_0
    //   36: ireturn
    //   37: aload_1
    //   38: getstatic android/mtp/MtpStorageManager$MtpObjectState.NORMAL : Landroid/mtp/MtpStorageManager$MtpObjectState;
    //   41: invokestatic access$1700 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObjectState;)V
    //   44: iload_2
    //   45: ifeq -> 142
    //   48: aload_1
    //   49: invokevirtual getParent : ()Landroid/mtp/MtpStorageManager$MtpObject;
    //   52: astore #5
    //   54: aload_0
    //   55: aload_1
    //   56: iload_3
    //   57: iconst_0
    //   58: invokespecial removeObjectFromCache : (Landroid/mtp/MtpStorageManager$MtpObject;ZZ)Z
    //   61: istore_2
    //   62: iload_2
    //   63: ifne -> 70
    //   66: aload_0
    //   67: monitorexit
    //   68: iconst_0
    //   69: ireturn
    //   70: aload_0
    //   71: aload #5
    //   73: aload_1
    //   74: invokevirtual getName : ()Ljava/lang/String;
    //   77: aload_1
    //   78: invokevirtual isDir : ()Z
    //   81: invokespecial handleAddedObject : (Landroid/mtp/MtpStorageManager$MtpObject;Ljava/lang/String;Z)V
    //   84: goto -> 142
    //   87: aload_0
    //   88: aload_1
    //   89: iload_3
    //   90: iconst_0
    //   91: invokespecial removeObjectFromCache : (Landroid/mtp/MtpStorageManager$MtpObject;ZZ)Z
    //   94: istore_3
    //   95: iload_3
    //   96: ifne -> 103
    //   99: aload_0
    //   100: monitorexit
    //   101: iconst_0
    //   102: ireturn
    //   103: iload_2
    //   104: ifne -> 142
    //   107: aload_0
    //   108: getfield mMtpNotifier : Landroid/mtp/MtpStorageManager$MtpNotifier;
    //   111: aload_1
    //   112: invokevirtual getId : ()I
    //   115: invokevirtual sendObjectRemoved : (I)V
    //   118: goto -> 142
    //   121: iload_2
    //   122: ifeq -> 135
    //   125: aload_1
    //   126: getstatic android/mtp/MtpStorageManager$MtpObjectState.FROZEN_ONESHOT_DEL : Landroid/mtp/MtpStorageManager$MtpObjectState;
    //   129: invokestatic access$1700 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObjectState;)V
    //   132: goto -> 142
    //   135: aload_1
    //   136: getstatic android/mtp/MtpStorageManager$MtpObjectState.NORMAL : Landroid/mtp/MtpStorageManager$MtpObjectState;
    //   139: invokestatic access$1700 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObjectState;)V
    //   142: aload_0
    //   143: monitorexit
    //   144: iconst_1
    //   145: ireturn
    //   146: astore_1
    //   147: aload_0
    //   148: monitorexit
    //   149: aload_1
    //   150: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1164	-> 2
    //   #1196	-> 33
    //   #1176	-> 37
    //   #1177	-> 44
    //   #1179	-> 48
    //   #1180	-> 54
    //   #1181	-> 66
    //   #1182	-> 70
    //   #1183	-> 84
    //   #1187	-> 87
    //   #1188	-> 99
    //   #1189	-> 103
    //   #1191	-> 107
    //   #1166	-> 121
    //   #1168	-> 125
    //   #1171	-> 135
    //   #1173	-> 142
    //   #1198	-> 142
    //   #1163	-> 146
    // Exception table:
    //   from	to	target	type
    //   2	15	146	finally
    //   37	44	146	finally
    //   48	54	146	finally
    //   54	62	146	finally
    //   70	84	146	finally
    //   87	95	146	finally
    //   107	118	146	finally
    //   125	132	146	finally
    //   135	142	146	finally
  }
  
  private boolean generalBeginRenameObject(MtpObject paramMtpObject1, MtpObject paramMtpObject2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_1
    //   3: getstatic android/mtp/MtpStorageManager$MtpObjectState.FROZEN : Landroid/mtp/MtpStorageManager$MtpObjectState;
    //   6: invokestatic access$1700 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObjectState;)V
    //   9: aload_2
    //   10: getstatic android/mtp/MtpStorageManager$MtpObjectState.FROZEN : Landroid/mtp/MtpStorageManager$MtpObjectState;
    //   13: invokestatic access$1700 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObjectState;)V
    //   16: aload_1
    //   17: getstatic android/mtp/MtpStorageManager$MtpOperation.RENAME : Landroid/mtp/MtpStorageManager$MtpOperation;
    //   20: invokestatic access$1900 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpOperation;)V
    //   23: aload_2
    //   24: getstatic android/mtp/MtpStorageManager$MtpOperation.RENAME : Landroid/mtp/MtpStorageManager$MtpOperation;
    //   27: invokestatic access$1900 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpOperation;)V
    //   30: aload_0
    //   31: monitorexit
    //   32: iconst_1
    //   33: ireturn
    //   34: astore_1
    //   35: aload_0
    //   36: monitorexit
    //   37: aload_1
    //   38: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1202	-> 2
    //   #1203	-> 9
    //   #1204	-> 16
    //   #1205	-> 23
    //   #1206	-> 30
    //   #1201	-> 34
    // Exception table:
    //   from	to	target	type
    //   2	9	34	finally
    //   9	16	34	finally
    //   16	23	34	finally
    //   23	30	34	finally
  }
  
  private boolean generalEndRenameObject(MtpObject paramMtpObject1, MtpObject paramMtpObject2, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: iconst_1
    //   3: istore #4
    //   5: iload_3
    //   6: ifne -> 15
    //   9: iconst_1
    //   10: istore #5
    //   12: goto -> 18
    //   15: iconst_0
    //   16: istore #5
    //   18: aload_0
    //   19: aload_1
    //   20: iload_3
    //   21: iload #5
    //   23: invokespecial generalEndRemoveObject : (Landroid/mtp/MtpStorageManager$MtpObject;ZZ)Z
    //   26: istore #5
    //   28: aload_0
    //   29: aload_2
    //   30: iload_3
    //   31: iload_3
    //   32: invokespecial generalEndAddObject : (Landroid/mtp/MtpStorageManager$MtpObject;ZZ)Z
    //   35: istore_3
    //   36: iload_3
    //   37: ifeq -> 51
    //   40: iload #5
    //   42: ifeq -> 51
    //   45: iload #4
    //   47: istore_3
    //   48: goto -> 53
    //   51: iconst_0
    //   52: istore_3
    //   53: aload_0
    //   54: monitorexit
    //   55: iload_3
    //   56: ireturn
    //   57: astore_1
    //   58: aload_0
    //   59: monitorexit
    //   60: aload_1
    //   61: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1211	-> 2
    //   #1212	-> 28
    //   #1210	-> 57
    // Exception table:
    //   from	to	target	type
    //   18	28	57	finally
    //   28	36	57	finally
  }
  
  private boolean generalBeginRemoveObject(MtpObject paramMtpObject, MtpOperation paramMtpOperation) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_1
    //   3: getstatic android/mtp/MtpStorageManager$MtpObjectState.FROZEN : Landroid/mtp/MtpStorageManager$MtpObjectState;
    //   6: invokestatic access$1700 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObjectState;)V
    //   9: aload_1
    //   10: aload_2
    //   11: invokestatic access$1900 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpOperation;)V
    //   14: aload_1
    //   15: invokevirtual isDir : ()Z
    //   18: ifeq -> 60
    //   21: aload_1
    //   22: invokestatic access$1000 : (Landroid/mtp/MtpStorageManager$MtpObject;)Ljava/util/Collection;
    //   25: invokeinterface iterator : ()Ljava/util/Iterator;
    //   30: astore_3
    //   31: aload_3
    //   32: invokeinterface hasNext : ()Z
    //   37: ifeq -> 60
    //   40: aload_3
    //   41: invokeinterface next : ()Ljava/lang/Object;
    //   46: checkcast android/mtp/MtpStorageManager$MtpObject
    //   49: astore_1
    //   50: aload_0
    //   51: aload_1
    //   52: aload_2
    //   53: invokespecial generalBeginRemoveObject : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpOperation;)Z
    //   56: pop
    //   57: goto -> 31
    //   60: aload_0
    //   61: monitorexit
    //   62: iconst_1
    //   63: ireturn
    //   64: astore_1
    //   65: aload_0
    //   66: monitorexit
    //   67: aload_1
    //   68: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1216	-> 2
    //   #1217	-> 9
    //   #1218	-> 14
    //   #1219	-> 21
    //   #1220	-> 50
    //   #1222	-> 60
    //   #1215	-> 64
    // Exception table:
    //   from	to	target	type
    //   2	9	64	finally
    //   9	14	64	finally
    //   14	21	64	finally
    //   21	31	64	finally
    //   31	50	64	finally
    //   50	57	64	finally
  }
  
  private boolean generalBeginCopyObject(MtpObject paramMtpObject, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_1
    //   3: getstatic android/mtp/MtpStorageManager$MtpObjectState.FROZEN : Landroid/mtp/MtpStorageManager$MtpObjectState;
    //   6: invokestatic access$1700 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpObjectState;)V
    //   9: aload_1
    //   10: getstatic android/mtp/MtpStorageManager$MtpOperation.COPY : Landroid/mtp/MtpStorageManager$MtpOperation;
    //   13: invokestatic access$1900 : (Landroid/mtp/MtpStorageManager$MtpObject;Landroid/mtp/MtpStorageManager$MtpOperation;)V
    //   16: iload_2
    //   17: ifeq -> 44
    //   20: aload_1
    //   21: aload_0
    //   22: invokespecial getNextObjectId : ()I
    //   25: invokestatic access$2300 : (Landroid/mtp/MtpStorageManager$MtpObject;I)V
    //   28: aload_0
    //   29: getfield mObjects : Ljava/util/HashMap;
    //   32: aload_1
    //   33: invokevirtual getId : ()I
    //   36: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   39: aload_1
    //   40: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   43: pop
    //   44: aload_1
    //   45: invokevirtual isDir : ()Z
    //   48: ifeq -> 100
    //   51: aload_1
    //   52: invokestatic access$1000 : (Landroid/mtp/MtpStorageManager$MtpObject;)Ljava/util/Collection;
    //   55: invokeinterface iterator : ()Ljava/util/Iterator;
    //   60: astore_1
    //   61: aload_1
    //   62: invokeinterface hasNext : ()Z
    //   67: ifeq -> 100
    //   70: aload_1
    //   71: invokeinterface next : ()Ljava/lang/Object;
    //   76: checkcast android/mtp/MtpStorageManager$MtpObject
    //   79: astore_3
    //   80: aload_0
    //   81: aload_3
    //   82: iload_2
    //   83: invokespecial generalBeginCopyObject : (Landroid/mtp/MtpStorageManager$MtpObject;Z)Z
    //   86: istore #4
    //   88: iload #4
    //   90: ifne -> 97
    //   93: aload_0
    //   94: monitorexit
    //   95: iconst_0
    //   96: ireturn
    //   97: goto -> 61
    //   100: aload_0
    //   101: monitorexit
    //   102: iconst_1
    //   103: ireturn
    //   104: astore_1
    //   105: aload_0
    //   106: monitorexit
    //   107: aload_1
    //   108: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1226	-> 2
    //   #1227	-> 9
    //   #1228	-> 16
    //   #1229	-> 20
    //   #1230	-> 28
    //   #1232	-> 44
    //   #1233	-> 51
    //   #1234	-> 80
    //   #1235	-> 93
    //   #1234	-> 97
    //   #1236	-> 100
    //   #1225	-> 104
    // Exception table:
    //   from	to	target	type
    //   2	9	104	finally
    //   9	16	104	finally
    //   20	28	104	finally
    //   28	44	104	finally
    //   44	51	104	finally
    //   51	61	104	finally
    //   61	80	104	finally
    //   80	88	104	finally
  }
  
  private boolean generalEndCopyObject(MtpObject paramMtpObject, boolean paramBoolean1, boolean paramBoolean2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: iload_2
    //   3: ifeq -> 26
    //   6: iload_3
    //   7: ifeq -> 26
    //   10: aload_0
    //   11: getfield mObjects : Ljava/util/HashMap;
    //   14: aload_1
    //   15: invokevirtual getId : ()I
    //   18: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   21: aload_1
    //   22: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   25: pop
    //   26: iconst_1
    //   27: istore #4
    //   29: iconst_1
    //   30: istore #5
    //   32: aload_1
    //   33: invokevirtual isDir : ()Z
    //   36: istore #6
    //   38: iconst_0
    //   39: istore #7
    //   41: iload #6
    //   43: ifeq -> 144
    //   46: new java/util/ArrayList
    //   49: astore #8
    //   51: aload #8
    //   53: aload_1
    //   54: invokestatic access$1000 : (Landroid/mtp/MtpStorageManager$MtpObject;)Ljava/util/Collection;
    //   57: invokespecial <init> : (Ljava/util/Collection;)V
    //   60: aload #8
    //   62: invokevirtual iterator : ()Ljava/util/Iterator;
    //   65: astore #8
    //   67: iload #5
    //   69: istore #4
    //   71: aload #8
    //   73: invokeinterface hasNext : ()Z
    //   78: ifeq -> 144
    //   81: aload #8
    //   83: invokeinterface next : ()Ljava/lang/Object;
    //   88: checkcast android/mtp/MtpStorageManager$MtpObject
    //   91: astore #9
    //   93: iload #5
    //   95: istore #4
    //   97: aload #9
    //   99: invokestatic access$1500 : (Landroid/mtp/MtpStorageManager$MtpObject;)Landroid/mtp/MtpStorageManager$MtpOperation;
    //   102: getstatic android/mtp/MtpStorageManager$MtpOperation.COPY : Landroid/mtp/MtpStorageManager$MtpOperation;
    //   105: if_acmpne -> 137
    //   108: aload_0
    //   109: aload #9
    //   111: iload_2
    //   112: iload_3
    //   113: invokespecial generalEndCopyObject : (Landroid/mtp/MtpStorageManager$MtpObject;ZZ)Z
    //   116: ifeq -> 130
    //   119: iload #5
    //   121: ifeq -> 130
    //   124: iconst_1
    //   125: istore #5
    //   127: goto -> 133
    //   130: iconst_0
    //   131: istore #5
    //   133: iload #5
    //   135: istore #4
    //   137: iload #4
    //   139: istore #5
    //   141: goto -> 67
    //   144: iload_2
    //   145: ifne -> 160
    //   148: iload_3
    //   149: ifne -> 155
    //   152: goto -> 160
    //   155: iconst_0
    //   156: istore_3
    //   157: goto -> 162
    //   160: iconst_1
    //   161: istore_3
    //   162: aload_0
    //   163: aload_1
    //   164: iload_2
    //   165: iload_3
    //   166: invokespecial generalEndAddObject : (Landroid/mtp/MtpStorageManager$MtpObject;ZZ)Z
    //   169: istore_3
    //   170: iload #7
    //   172: istore_2
    //   173: iload_3
    //   174: ifeq -> 187
    //   177: iload #7
    //   179: istore_2
    //   180: iload #4
    //   182: ifeq -> 187
    //   185: iconst_1
    //   186: istore_2
    //   187: aload_0
    //   188: monitorexit
    //   189: iload_2
    //   190: ireturn
    //   191: astore_1
    //   192: aload_0
    //   193: monitorexit
    //   194: aload_1
    //   195: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1240	-> 2
    //   #1241	-> 10
    //   #1242	-> 26
    //   #1243	-> 32
    //   #1244	-> 46
    //   #1245	-> 93
    //   #1246	-> 108
    //   #1247	-> 137
    //   #1249	-> 144
    //   #1250	-> 187
    //   #1239	-> 191
    // Exception table:
    //   from	to	target	type
    //   10	26	191	finally
    //   32	38	191	finally
    //   46	67	191	finally
    //   71	93	191	finally
    //   97	108	191	finally
    //   108	119	191	finally
    //   162	170	191	finally
  }
}
