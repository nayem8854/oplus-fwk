package android.mtp;

import android.content.Context;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.os.UserManager;
import com.android.internal.util.Preconditions;
import dalvik.system.CloseGuard;
import java.io.IOException;

public final class MtpDevice {
  private static final String TAG = "MtpDevice";
  
  static {
    System.loadLibrary("media_jni");
  }
  
  private CloseGuard mCloseGuard = CloseGuard.get();
  
  private UsbDeviceConnection mConnection;
  
  private final UsbDevice mDevice;
  
  private final Object mLock = new Object();
  
  private long mNativeContext;
  
  public MtpDevice(UsbDevice paramUsbDevice) {
    Preconditions.checkNotNull(paramUsbDevice);
    this.mDevice = paramUsbDevice;
  }
  
  public boolean open(UsbDeviceConnection paramUsbDeviceConnection) {
    boolean bool1 = false;
    Context context = paramUsbDeviceConnection.getContext();
    Object object = this.mLock;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    boolean bool2 = bool1;
    if (context != null)
      try {
        UserManager userManager = (UserManager)context.getSystemService("user");
        bool2 = bool1;
        if (!userManager.hasUserRestriction("no_usb_file_transfer"))
          bool2 = native_open(this.mDevice.getDeviceName(), paramUsbDeviceConnection.getFileDescriptor()); 
      } finally {} 
    if (!bool2) {
      paramUsbDeviceConnection.close();
    } else {
      this.mConnection = paramUsbDeviceConnection;
      this.mCloseGuard.open("close");
    } 
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    return bool2;
  }
  
  public void close() {
    synchronized (this.mLock) {
      if (this.mConnection != null) {
        this.mCloseGuard.close();
        native_close();
        this.mConnection.close();
        this.mConnection = null;
      } 
      return;
    } 
  }
  
  protected void finalize() throws Throwable {
    try {
      if (this.mCloseGuard != null)
        this.mCloseGuard.warnIfOpen(); 
      close();
      return;
    } finally {
      super.finalize();
    } 
  }
  
  public String getDeviceName() {
    return this.mDevice.getDeviceName();
  }
  
  public int getDeviceId() {
    return this.mDevice.getDeviceId();
  }
  
  public String toString() {
    return this.mDevice.getDeviceName();
  }
  
  public MtpDeviceInfo getDeviceInfo() {
    return native_get_device_info();
  }
  
  public int[] getStorageIds() {
    return native_get_storage_ids();
  }
  
  public int[] getObjectHandles(int paramInt1, int paramInt2, int paramInt3) {
    return native_get_object_handles(paramInt1, paramInt2, paramInt3);
  }
  
  public byte[] getObject(int paramInt1, int paramInt2) {
    Preconditions.checkArgumentNonnegative(paramInt2, "objectSize should not be negative");
    return native_get_object(paramInt1, paramInt2);
  }
  
  public long getPartialObject(int paramInt, long paramLong1, long paramLong2, byte[] paramArrayOfbyte) throws IOException {
    return native_get_partial_object(paramInt, paramLong1, paramLong2, paramArrayOfbyte);
  }
  
  public long getPartialObject64(int paramInt, long paramLong1, long paramLong2, byte[] paramArrayOfbyte) throws IOException {
    return native_get_partial_object_64(paramInt, paramLong1, paramLong2, paramArrayOfbyte);
  }
  
  public byte[] getThumbnail(int paramInt) {
    return native_get_thumbnail(paramInt);
  }
  
  public MtpStorageInfo getStorageInfo(int paramInt) {
    return native_get_storage_info(paramInt);
  }
  
  public MtpObjectInfo getObjectInfo(int paramInt) {
    return native_get_object_info(paramInt);
  }
  
  public boolean deleteObject(int paramInt) {
    return native_delete_object(paramInt);
  }
  
  public long getParent(int paramInt) {
    return native_get_parent(paramInt);
  }
  
  public long getStorageId(int paramInt) {
    return native_get_storage_id(paramInt);
  }
  
  public boolean importFile(int paramInt, String paramString) {
    return native_import_file(paramInt, paramString);
  }
  
  public boolean importFile(int paramInt, ParcelFileDescriptor paramParcelFileDescriptor) {
    return native_import_file(paramInt, paramParcelFileDescriptor.getFd());
  }
  
  public boolean sendObject(int paramInt, long paramLong, ParcelFileDescriptor paramParcelFileDescriptor) {
    return native_send_object(paramInt, paramLong, paramParcelFileDescriptor.getFd());
  }
  
  public MtpObjectInfo sendObjectInfo(MtpObjectInfo paramMtpObjectInfo) {
    return native_send_object_info(paramMtpObjectInfo);
  }
  
  public MtpEvent readEvent(CancellationSignal paramCancellationSignal) throws IOException {
    boolean bool;
    int i = native_submit_event_request();
    if (i >= 0) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkState(bool, "Other thread is reading an event.");
    if (paramCancellationSignal != null)
      paramCancellationSignal.setOnCancelListener((CancellationSignal.OnCancelListener)new Object(this, i)); 
    try {
      return native_reap_event_request(i);
    } finally {
      if (paramCancellationSignal != null)
        paramCancellationSignal.setOnCancelListener(null); 
    } 
  }
  
  public long getObjectSizeLong(int paramInt1, int paramInt2) throws IOException {
    return native_get_object_size_long(paramInt1, paramInt2);
  }
  
  private native void native_close();
  
  private native boolean native_delete_object(int paramInt);
  
  private native void native_discard_event_request(int paramInt);
  
  private native MtpDeviceInfo native_get_device_info();
  
  private native byte[] native_get_object(int paramInt, long paramLong);
  
  private native int[] native_get_object_handles(int paramInt1, int paramInt2, int paramInt3);
  
  private native MtpObjectInfo native_get_object_info(int paramInt);
  
  private native long native_get_object_size_long(int paramInt1, int paramInt2) throws IOException;
  
  private native int native_get_parent(int paramInt);
  
  private native long native_get_partial_object(int paramInt, long paramLong1, long paramLong2, byte[] paramArrayOfbyte) throws IOException;
  
  private native int native_get_partial_object_64(int paramInt, long paramLong1, long paramLong2, byte[] paramArrayOfbyte) throws IOException;
  
  private native int native_get_storage_id(int paramInt);
  
  private native int[] native_get_storage_ids();
  
  private native MtpStorageInfo native_get_storage_info(int paramInt);
  
  private native byte[] native_get_thumbnail(int paramInt);
  
  private native boolean native_import_file(int paramInt1, int paramInt2);
  
  private native boolean native_import_file(int paramInt, String paramString);
  
  private native boolean native_open(String paramString, int paramInt);
  
  private native MtpEvent native_reap_event_request(int paramInt) throws IOException;
  
  private native boolean native_send_object(int paramInt1, long paramLong, int paramInt2);
  
  private native MtpObjectInfo native_send_object_info(MtpObjectInfo paramMtpObjectInfo);
  
  private native int native_submit_event_request() throws IOException;
}
