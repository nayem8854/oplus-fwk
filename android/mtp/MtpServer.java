package android.mtp;

import android.content.Context;
import android.content.SharedPreferences;
import com.android.internal.util.Preconditions;
import java.io.FileDescriptor;
import java.util.Random;
import libcore.util.HexEncoding;

public class MtpServer implements Runnable {
  private static final int sID_LEN_BYTES = 16;
  
  private static final int sID_LEN_STR = 32;
  
  private final Context mContext;
  
  private final MtpDatabase mDatabase;
  
  private long mNativeContext;
  
  private final Runnable mOnTerminate;
  
  static {
    System.loadLibrary("media_jni");
  }
  
  public MtpServer(MtpDatabase paramMtpDatabase, FileDescriptor paramFileDescriptor, boolean paramBoolean, Runnable paramRunnable, String paramString1, String paramString2, String paramString3) {
    String str;
    this.mDatabase = (MtpDatabase)Preconditions.checkNotNull(paramMtpDatabase);
    this.mOnTerminate = (Runnable)Preconditions.checkNotNull(paramRunnable);
    Context context = this.mDatabase.getContext();
    paramRunnable = null;
    SharedPreferences sharedPreferences = context.getSharedPreferences("mtp-cfg", 0);
    if (sharedPreferences.contains("mtp-id")) {
      String str1 = sharedPreferences.getString("mtp-id", null);
      if (str1.length() != 32) {
        paramRunnable = null;
      } else {
        byte b = 0;
        while (true) {
          str = str1;
          if (b < str1.length()) {
            if (Character.digit(str1.charAt(b), 16) == -1) {
              str = null;
              break;
            } 
            b++;
            continue;
          } 
          break;
        } 
      } 
    } 
    if (str == null) {
      str = getRandId();
      sharedPreferences.edit().putString("mtp-id", str).apply();
    } 
    native_setup(paramMtpDatabase, paramFileDescriptor, paramBoolean, paramString1, paramString2, paramString3, str);
    paramMtpDatabase.setServer(this);
  }
  
  private String getRandId() {
    Random random = new Random();
    byte[] arrayOfByte = new byte[16];
    random.nextBytes(arrayOfByte);
    return HexEncoding.encodeToString(arrayOfByte);
  }
  
  public void start() {
    Thread thread = new Thread(this, "MtpServer");
    thread.start();
  }
  
  public void run() {
    native_run();
    native_cleanup();
    this.mDatabase.close();
    this.mOnTerminate.run();
  }
  
  public void sendObjectAdded(int paramInt) {
    native_send_object_added(paramInt);
  }
  
  public void sendObjectRemoved(int paramInt) {
    native_send_object_removed(paramInt);
  }
  
  public void sendObjectInfoChanged(int paramInt) {
    native_send_object_info_changed(paramInt);
  }
  
  public void sendDevicePropertyChanged(int paramInt) {
    native_send_device_property_changed(paramInt);
  }
  
  public void addStorage(MtpStorage paramMtpStorage) {
    native_add_storage(paramMtpStorage);
  }
  
  public void removeStorage(MtpStorage paramMtpStorage) {
    native_remove_storage(paramMtpStorage.getStorageId());
  }
  
  public static void configure(boolean paramBoolean) {
    native_configure(paramBoolean);
  }
  
  private final native void native_add_storage(MtpStorage paramMtpStorage);
  
  private final native void native_cleanup();
  
  public static final native void native_configure(boolean paramBoolean);
  
  private final native void native_remove_storage(int paramInt);
  
  private final native void native_run();
  
  private final native void native_send_device_property_changed(int paramInt);
  
  private final native void native_send_object_added(int paramInt);
  
  private final native void native_send_object_info_changed(int paramInt);
  
  private final native void native_send_object_removed(int paramInt);
  
  private final native void native_setup(MtpDatabase paramMtpDatabase, FileDescriptor paramFileDescriptor, boolean paramBoolean, String paramString1, String paramString2, String paramString3, String paramString4);
}
