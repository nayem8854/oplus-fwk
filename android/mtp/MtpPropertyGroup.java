package android.mtp;

import android.content.ContentProviderClient;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;
import android.util.Log;
import java.util.ArrayList;

class MtpPropertyGroup {
  private static final String PATH_WHERE = "_data=?";
  
  private static final String TAG = MtpPropertyGroup.class.getSimpleName();
  
  private String[] mColumns;
  
  private final Property[] mProperties;
  
  private class Property {
    int code;
    
    int column;
    
    final MtpPropertyGroup this$0;
    
    int type;
    
    Property(int param1Int1, int param1Int2, int param1Int3) {
      this.code = param1Int1;
      this.type = param1Int2;
      this.column = param1Int3;
    }
  }
  
  public MtpPropertyGroup(int[] paramArrayOfint) {
    int i = paramArrayOfint.length;
    ArrayList<String> arrayList = new ArrayList(i);
    arrayList.add("_id");
    this.mProperties = new Property[i];
    byte b;
    for (b = 0; b < i; b++)
      this.mProperties[b] = createProperty(paramArrayOfint[b], arrayList); 
    i = arrayList.size();
    this.mColumns = new String[i];
    for (b = 0; b < i; b++)
      this.mColumns[b] = arrayList.get(b); 
  }
  
  private Property createProperty(int paramInt, ArrayList<String> paramArrayList) {
    char c;
    String str2;
    StringBuilder stringBuilder;
    String str1 = null;
    switch (paramInt) {
      default:
        c = Character.MIN_VALUE;
        str2 = TAG;
        stringBuilder = new StringBuilder();
        stringBuilder.append("unsupported property ");
        stringBuilder.append(paramInt);
        Log.e(str2, stringBuilder.toString());
        break;
      case 56979:
      case 56985:
      case 56986:
        c = '\006';
        break;
      case 56978:
      case 56980:
        c = '\004';
        break;
      case 56544:
        c = '￿';
        break;
      case 56475:
        str1 = "album_artist";
        c = '￿';
        break;
      case 56474:
        str1 = "album";
        c = '￿';
        break;
      case 56473:
        str1 = "year";
        c = '￿';
        break;
      case 56470:
        str1 = "composer";
        c = '￿';
        break;
      case 56460:
        str1 = "genre";
        c = '￿';
        break;
      case 56459:
        str1 = "track";
        c = '\004';
        break;
      case 56457:
        str1 = "duration";
        c = '\006';
        break;
      case 56398:
        c = '￿';
        break;
      case 56392:
        str1 = "description";
        c = '￿';
        break;
      case 56390:
        str1 = "artist";
        c = '￿';
        break;
      case 56388:
        c = '￿';
        break;
      case 56385:
        c = '\n';
        break;
      case 56331:
        c = '\006';
        break;
      case 56329:
        c = '￿';
        break;
      case 56327:
        c = '￿';
        break;
      case 56324:
        c = '\b';
        break;
      case 56323:
        c = '\004';
        break;
      case 56322:
        c = '\004';
        break;
      case 56321:
        c = '\006';
        break;
    } 
    if (str1 != null) {
      paramArrayList.add(str1);
      return new Property(paramInt, c, paramArrayList.size() - 1);
    } 
    return new Property(paramInt, c, -1);
  }
  
  private native String format_date_time(long paramLong);
  
  public int getPropertyList(ContentProviderClient paramContentProviderClient, String paramString, MtpStorageManager.MtpObject paramMtpObject, MtpPropertyList paramMtpPropertyList) {
    int i = paramMtpObject.getId();
    String str = paramMtpObject.getPath().toString();
    Property[] arrayOfProperty = this.mProperties;
    int j = arrayOfProperty.length;
    Cursor cursor = null;
    byte b = 0;
    while (true) {
      Property property;
      int k;
      StringBuilder stringBuilder;
      String str1;
      long l1, l2;
      int m;
      if (b < j) {
        property = arrayOfProperty[b];
        if (property.column != -1 && cursor == null) {
          try {
            int n = paramMtpObject.getFormat();
            Cursor cursor1 = cursor;
            try {
              Uri uri = MtpDatabase.getObjectPropertiesUri(n, paramString);
              cursor1 = cursor;
              Cursor cursor2 = paramContentProviderClient.query(uri, this.mColumns, "_data=?", new String[] { str }, null, null);
              cursor = cursor2;
              if (cursor2 != null) {
                cursor = cursor2;
                cursor1 = cursor2;
                if (!cursor2.moveToNext()) {
                  cursor1 = cursor2;
                  cursor2.close();
                  cursor = null;
                } 
              } 
            } catch (IllegalArgumentException illegalArgumentException) {
              return 43009;
            } catch (RemoteException remoteException) {
              cursor = cursor1;
            } 
          } catch (IllegalArgumentException illegalArgumentException) {
          
          } catch (RemoteException remoteException) {
            Log.e(TAG, "Mediaprovider lookup failed");
          } 
          return 43009;
        } 
      } else {
        break;
      } 
      switch (property.code) {
        default:
          k = property.type;
          if (k != 0) {
            if (k != 65535) {
              long l;
              if (cursor != null) {
                l = cursor.getLong(property.column);
              } else {
                l = 0L;
              } 
              paramMtpPropertyList.append(i, property.code, property.type, l);
            } else {
              String str2 = "";
              if (cursor != null)
                str2 = cursor.getString(property.column); 
              paramMtpPropertyList.append(i, property.code, str2);
            } 
          } else {
            paramMtpPropertyList.append(i, property.code, property.type, 0L);
          } 
          b++;
          continue;
        case 56979:
        case 56985:
        case 56986:
          paramMtpPropertyList.append(i, property.code, 6, 0L);
          break;
        case 56978:
        case 56980:
          paramMtpPropertyList.append(i, property.code, 4, 0L);
          break;
        case 56473:
          k = 0;
          if (cursor != null)
            k = cursor.getInt(property.column); 
          stringBuilder = new StringBuilder();
          stringBuilder.append(Integer.toString(k));
          stringBuilder.append("0101T000000");
          str1 = stringBuilder.toString();
          paramMtpPropertyList.append(i, property.code, str1);
          break;
        case 56459:
          if (cursor != null) {
            k = cursor.getInt(property.column);
          } else {
            k = 0;
          } 
          paramMtpPropertyList.append(i, property.code, 4, (k % 1000));
          break;
        case 56385:
          l1 = (paramMtpObject.getPath().toString().hashCode() << 32);
          l2 = paramMtpObject.getModifiedTime();
          paramMtpPropertyList.append(i, property.code, property.type, l1 + l2);
          break;
        case 56331:
          k = property.code;
          m = property.type;
          if (paramMtpObject.getParent().isRoot()) {
            l1 = 0L;
          } else {
            l1 = paramMtpObject.getParent().getId();
          } 
          paramMtpPropertyList.append(i, k, m, l1);
          break;
        case 56329:
        case 56398:
          k = property.code;
          str1 = format_date_time(paramMtpObject.getModifiedTime());
          paramMtpPropertyList.append(i, k, str1);
          break;
        case 56327:
        case 56388:
        case 56544:
          paramMtpPropertyList.append(i, property.code, paramMtpObject.getName());
          break;
        case 56324:
          paramMtpPropertyList.append(i, property.code, property.type, paramMtpObject.getSize());
          break;
        case 56323:
          paramMtpPropertyList.append(i, property.code, property.type, 0L);
          break;
        case 56322:
          paramMtpPropertyList.append(i, property.code, property.type, paramMtpObject.getFormat());
          break;
        case 56321:
          paramMtpPropertyList.append(i, property.code, property.type, paramMtpObject.getStorageId());
          break;
      } 
      b++;
    } 
    if (cursor != null)
      cursor.close(); 
    return 8193;
  }
}
