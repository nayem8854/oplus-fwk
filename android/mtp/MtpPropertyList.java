package android.mtp;

import java.util.ArrayList;
import java.util.List;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;

class MtpPropertyList {
  private int mCode;
  
  private List<Integer> mDataTypes;
  
  private List<Long> mLongValues;
  
  private List<Integer> mObjectHandles;
  
  private List<Integer> mPropertyCodes;
  
  private List<String> mStringValues;
  
  public MtpPropertyList(int paramInt) {
    this.mCode = paramInt;
    this.mObjectHandles = new ArrayList<>();
    this.mPropertyCodes = new ArrayList<>();
    this.mDataTypes = new ArrayList<>();
    this.mLongValues = new ArrayList<>();
    this.mStringValues = new ArrayList<>();
  }
  
  public void append(int paramInt1, int paramInt2, int paramInt3, long paramLong) {
    this.mObjectHandles.add(Integer.valueOf(paramInt1));
    this.mPropertyCodes.add(Integer.valueOf(paramInt2));
    this.mDataTypes.add(Integer.valueOf(paramInt3));
    this.mLongValues.add(Long.valueOf(paramLong));
    this.mStringValues.add((String)null);
  }
  
  public void append(int paramInt1, int paramInt2, String paramString) {
    this.mObjectHandles.add(Integer.valueOf(paramInt1));
    this.mPropertyCodes.add(Integer.valueOf(paramInt2));
    this.mDataTypes.add(Integer.valueOf(65535));
    this.mStringValues.add(paramString);
    this.mLongValues.add(Long.valueOf(0L));
  }
  
  public int getCode() {
    return this.mCode;
  }
  
  public int getCount() {
    return this.mObjectHandles.size();
  }
  
  public int[] getObjectHandles() {
    return this.mObjectHandles.stream().mapToInt((ToIntFunction)_$$Lambda$UV1wDVoVlbcxpr8zevj_aMFtUGw.INSTANCE).toArray();
  }
  
  public int[] getPropertyCodes() {
    return this.mPropertyCodes.stream().mapToInt((ToIntFunction)_$$Lambda$UV1wDVoVlbcxpr8zevj_aMFtUGw.INSTANCE).toArray();
  }
  
  public int[] getDataTypes() {
    return this.mDataTypes.stream().mapToInt((ToIntFunction)_$$Lambda$UV1wDVoVlbcxpr8zevj_aMFtUGw.INSTANCE).toArray();
  }
  
  public long[] getLongValues() {
    return this.mLongValues.stream().mapToLong((ToLongFunction)_$$Lambda$ELHKvd8JMVRD8rbALqYPKbDX2mM.INSTANCE).toArray();
  }
  
  public String[] getStringValues() {
    return this.mStringValues.<String>toArray(new String[0]);
  }
}
