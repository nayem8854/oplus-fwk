package android.mtp;

import com.android.internal.util.Preconditions;
import dalvik.system.VMRuntime;

public final class MtpObjectInfo {
  private int mThumbPixWidth;
  
  private int mThumbPixHeight;
  
  private int mThumbFormat;
  
  private int mThumbCompressedSize;
  
  private int mStorageId;
  
  private int mSequenceNumber;
  
  private int mProtectionStatus;
  
  private int mParent;
  
  private String mName = "";
  
  private String mKeywords = "";
  
  private int mImagePixWidth;
  
  private int mImagePixHeight;
  
  private int mImagePixDepth;
  
  private int mHandle;
  
  private int mFormat;
  
  private long mDateModified;
  
  private long mDateCreated;
  
  private int mCompressedSize;
  
  private int mAssociationType;
  
  private int mAssociationDesc;
  
  public final int getObjectHandle() {
    return this.mHandle;
  }
  
  public final int getStorageId() {
    return this.mStorageId;
  }
  
  public final int getFormat() {
    return this.mFormat;
  }
  
  public final int getProtectionStatus() {
    return this.mProtectionStatus;
  }
  
  public final int getCompressedSize() {
    boolean bool;
    if (this.mCompressedSize >= 0) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkState(bool);
    return this.mCompressedSize;
  }
  
  public final long getCompressedSizeLong() {
    return uint32ToLong(this.mCompressedSize);
  }
  
  public final int getThumbFormat() {
    return this.mThumbFormat;
  }
  
  public final int getThumbCompressedSize() {
    boolean bool;
    if (this.mThumbCompressedSize >= 0) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkState(bool);
    return this.mThumbCompressedSize;
  }
  
  public final long getThumbCompressedSizeLong() {
    return uint32ToLong(this.mThumbCompressedSize);
  }
  
  public final int getThumbPixWidth() {
    boolean bool;
    if (this.mThumbPixWidth >= 0) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkState(bool);
    return this.mThumbPixWidth;
  }
  
  public final long getThumbPixWidthLong() {
    return uint32ToLong(this.mThumbPixWidth);
  }
  
  public final int getThumbPixHeight() {
    boolean bool;
    if (this.mThumbPixHeight >= 0) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkState(bool);
    return this.mThumbPixHeight;
  }
  
  public final long getThumbPixHeightLong() {
    return uint32ToLong(this.mThumbPixHeight);
  }
  
  public final int getImagePixWidth() {
    boolean bool;
    if (this.mImagePixWidth >= 0) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkState(bool);
    return this.mImagePixWidth;
  }
  
  public final long getImagePixWidthLong() {
    return uint32ToLong(this.mImagePixWidth);
  }
  
  public final int getImagePixHeight() {
    boolean bool;
    if (this.mImagePixHeight >= 0) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkState(bool);
    return this.mImagePixHeight;
  }
  
  public final long getImagePixHeightLong() {
    return uint32ToLong(this.mImagePixHeight);
  }
  
  public final int getImagePixDepth() {
    boolean bool;
    if (this.mImagePixDepth >= 0) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkState(bool);
    return this.mImagePixDepth;
  }
  
  public final long getImagePixDepthLong() {
    return uint32ToLong(this.mImagePixDepth);
  }
  
  public final int getParent() {
    return this.mParent;
  }
  
  public final int getAssociationType() {
    return this.mAssociationType;
  }
  
  public final int getAssociationDesc() {
    return this.mAssociationDesc;
  }
  
  public final int getSequenceNumber() {
    boolean bool;
    if (this.mSequenceNumber >= 0) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkState(bool);
    return this.mSequenceNumber;
  }
  
  public final long getSequenceNumberLong() {
    return uint32ToLong(this.mSequenceNumber);
  }
  
  public final String getName() {
    return this.mName;
  }
  
  public final long getDateCreated() {
    return this.mDateCreated;
  }
  
  public final long getDateModified() {
    return this.mDateModified;
  }
  
  public final String getKeywords() {
    return this.mKeywords;
  }
  
  public static class Builder {
    private MtpObjectInfo mObjectInfo;
    
    public Builder() {
      MtpObjectInfo mtpObjectInfo = new MtpObjectInfo();
      MtpObjectInfo.access$102(mtpObjectInfo, -1);
    }
    
    public Builder(MtpObjectInfo param1MtpObjectInfo) {
      MtpObjectInfo mtpObjectInfo = new MtpObjectInfo();
      MtpObjectInfo.access$102(mtpObjectInfo, -1);
      MtpObjectInfo.access$202(this.mObjectInfo, param1MtpObjectInfo.mAssociationDesc);
      MtpObjectInfo.access$302(this.mObjectInfo, param1MtpObjectInfo.mAssociationType);
      MtpObjectInfo.access$402(this.mObjectInfo, param1MtpObjectInfo.mCompressedSize);
      MtpObjectInfo.access$502(this.mObjectInfo, param1MtpObjectInfo.mDateCreated);
      MtpObjectInfo.access$602(this.mObjectInfo, param1MtpObjectInfo.mDateModified);
      MtpObjectInfo.access$702(this.mObjectInfo, param1MtpObjectInfo.mFormat);
      MtpObjectInfo.access$802(this.mObjectInfo, param1MtpObjectInfo.mImagePixDepth);
      MtpObjectInfo.access$902(this.mObjectInfo, param1MtpObjectInfo.mImagePixHeight);
      MtpObjectInfo.access$1002(this.mObjectInfo, param1MtpObjectInfo.mImagePixWidth);
      MtpObjectInfo.access$1102(this.mObjectInfo, param1MtpObjectInfo.mKeywords);
      MtpObjectInfo.access$1202(this.mObjectInfo, param1MtpObjectInfo.mName);
      MtpObjectInfo.access$1302(this.mObjectInfo, param1MtpObjectInfo.mParent);
      MtpObjectInfo.access$1402(this.mObjectInfo, param1MtpObjectInfo.mProtectionStatus);
      MtpObjectInfo.access$1502(this.mObjectInfo, param1MtpObjectInfo.mSequenceNumber);
      MtpObjectInfo.access$1602(this.mObjectInfo, param1MtpObjectInfo.mStorageId);
      MtpObjectInfo.access$1702(this.mObjectInfo, param1MtpObjectInfo.mThumbCompressedSize);
      MtpObjectInfo.access$1802(this.mObjectInfo, param1MtpObjectInfo.mThumbFormat);
      MtpObjectInfo.access$1902(this.mObjectInfo, param1MtpObjectInfo.mThumbPixHeight);
      MtpObjectInfo.access$2002(this.mObjectInfo, param1MtpObjectInfo.mThumbPixWidth);
    }
    
    public Builder setObjectHandle(int param1Int) {
      MtpObjectInfo.access$102(this.mObjectInfo, param1Int);
      return this;
    }
    
    public Builder setAssociationDesc(int param1Int) {
      MtpObjectInfo.access$202(this.mObjectInfo, param1Int);
      return this;
    }
    
    public Builder setAssociationType(int param1Int) {
      MtpObjectInfo.access$302(this.mObjectInfo, param1Int);
      return this;
    }
    
    public Builder setCompressedSize(long param1Long) {
      MtpObjectInfo.access$402(this.mObjectInfo, MtpObjectInfo.longToUint32(param1Long, "value"));
      return this;
    }
    
    public Builder setDateCreated(long param1Long) {
      MtpObjectInfo.access$502(this.mObjectInfo, param1Long);
      return this;
    }
    
    public Builder setDateModified(long param1Long) {
      MtpObjectInfo.access$602(this.mObjectInfo, param1Long);
      return this;
    }
    
    public Builder setFormat(int param1Int) {
      MtpObjectInfo.access$702(this.mObjectInfo, param1Int);
      return this;
    }
    
    public Builder setImagePixDepth(long param1Long) {
      MtpObjectInfo.access$802(this.mObjectInfo, MtpObjectInfo.longToUint32(param1Long, "value"));
      return this;
    }
    
    public Builder setImagePixHeight(long param1Long) {
      MtpObjectInfo.access$902(this.mObjectInfo, MtpObjectInfo.longToUint32(param1Long, "value"));
      return this;
    }
    
    public Builder setImagePixWidth(long param1Long) {
      MtpObjectInfo.access$1002(this.mObjectInfo, MtpObjectInfo.longToUint32(param1Long, "value"));
      return this;
    }
    
    public Builder setKeywords(String param1String) {
      String str;
      if (VMRuntime.getRuntime().getTargetSdkVersion() > 25) {
        Preconditions.checkNotNull(param1String);
        str = param1String;
      } else {
        str = param1String;
        if (param1String == null)
          str = ""; 
      } 
      MtpObjectInfo.access$1102(this.mObjectInfo, str);
      return this;
    }
    
    public Builder setName(String param1String) {
      Preconditions.checkNotNull(param1String);
      MtpObjectInfo.access$1202(this.mObjectInfo, param1String);
      return this;
    }
    
    public Builder setParent(int param1Int) {
      MtpObjectInfo.access$1302(this.mObjectInfo, param1Int);
      return this;
    }
    
    public Builder setProtectionStatus(int param1Int) {
      MtpObjectInfo.access$1402(this.mObjectInfo, param1Int);
      return this;
    }
    
    public Builder setSequenceNumber(long param1Long) {
      MtpObjectInfo.access$1502(this.mObjectInfo, MtpObjectInfo.longToUint32(param1Long, "value"));
      return this;
    }
    
    public Builder setStorageId(int param1Int) {
      MtpObjectInfo.access$1602(this.mObjectInfo, param1Int);
      return this;
    }
    
    public Builder setThumbCompressedSize(long param1Long) {
      MtpObjectInfo.access$1702(this.mObjectInfo, MtpObjectInfo.longToUint32(param1Long, "value"));
      return this;
    }
    
    public Builder setThumbFormat(int param1Int) {
      MtpObjectInfo.access$1802(this.mObjectInfo, param1Int);
      return this;
    }
    
    public Builder setThumbPixHeight(long param1Long) {
      MtpObjectInfo.access$1902(this.mObjectInfo, MtpObjectInfo.longToUint32(param1Long, "value"));
      return this;
    }
    
    public Builder setThumbPixWidth(long param1Long) {
      MtpObjectInfo.access$2002(this.mObjectInfo, MtpObjectInfo.longToUint32(param1Long, "value"));
      return this;
    }
    
    public MtpObjectInfo build() {
      MtpObjectInfo mtpObjectInfo = this.mObjectInfo;
      this.mObjectInfo = null;
      return mtpObjectInfo;
    }
  }
  
  private static long uint32ToLong(int paramInt) {
    long l;
    if (paramInt < 0) {
      l = paramInt + 4294967296L;
    } else {
      l = paramInt;
    } 
    return l;
  }
  
  private static int longToUint32(long paramLong, String paramString) {
    Preconditions.checkArgumentInRange(paramLong, 0L, 4294967295L, paramString);
    return (int)paramLong;
  }
  
  private MtpObjectInfo() {}
}
