package android.mtp;

import android.content.ContentResolver;
import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.os.SystemProperties;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import java.io.File;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;

public class OplusBaseMtpDatabase {
  private static final boolean DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  protected static final int EXIT_MESSAGE = 2;
  
  protected static final int HANDLE_MESSAGE = 1;
  
  protected static final int INIT_MESSAGE = 0;
  
  private static final String NO_MEDIA = ".nomedia";
  
  private static final String TAG = "OplusBaseMtpDatabase";
  
  protected final ScanHandler mHandler;
  
  protected final HandlerThread mHandlerThread;
  
  private final Context sContext;
  
  public OplusBaseMtpDatabase(Context paramContext) {
    Objects.requireNonNull(paramContext);
    this.sContext = paramContext;
    HandlerThread handlerThread = new HandlerThread("OplusBaseMtpDatabase", 10);
    handlerThread.start();
    this.mHandler = new ScanHandler(this.mHandlerThread.getLooper(), this.sContext);
  }
  
  public class HandlerParams {
    String path;
    
    final OplusBaseMtpDatabase this$0;
    
    HandlerParams(String param1String) {
      this.path = param1String;
    }
  }
  
  class ScanHandler extends Handler {
    private ArrayList<OplusBaseMtpDatabase.HandlerParams> mPendingInstalls = new ArrayList<>();
    
    private final Context scanContext;
    
    final OplusBaseMtpDatabase this$0;
    
    ScanHandler(Looper param1Looper, Context param1Context) {
      super(param1Looper);
      this.scanContext = param1Context;
    }
    
    public void handleMessage(Message param1Message) {
      try {
        doHandleMessage(param1Message);
        return;
      } finally {
        Process.setThreadPriority(10);
      } 
    }
    
    private void doHandleMessage(Message param1Message) {
      OplusBaseMtpDatabase.HandlerParams handlerParams;
      int i = param1Message.what;
      if (i != 0) {
        if (i != 1) {
          if (i == 2)
            if (this.mPendingInstalls.size() == 0) {
              if (OplusBaseMtpDatabase.this.mHandlerThread != null) {
                boolean bool = OplusBaseMtpDatabase.this.mHandlerThread.quitSafely();
                if (OplusBaseMtpDatabase.DEBUG) {
                  StringBuilder stringBuilder = new StringBuilder();
                  stringBuilder.append("mHandlerThread.quitSafely ?");
                  stringBuilder.append(bool);
                  Log.d("OplusBaseMtpDatabase", stringBuilder.toString());
                } 
              } 
            } else {
              if (OplusBaseMtpDatabase.DEBUG)
                Log.d("OplusBaseMtpDatabase", "sendEmptyMessageDelayed EXIT_MESSAGE"); 
              OplusBaseMtpDatabase.this.mHandler.sendEmptyMessageDelayed(2, 500L);
            }  
        } else if (this.mPendingInstalls.size() > 0) {
          handlerParams = this.mPendingInstalls.get(0);
          if (handlerParams != null) {
            OplusBaseMtpDatabase.updateMediaStore(this.scanContext, new File(handlerParams.path));
            if (this.mPendingInstalls.size() > 0)
              this.mPendingInstalls.remove(0); 
            if (this.mPendingInstalls.size() != 0)
              OplusBaseMtpDatabase.this.mHandler.sendEmptyMessage(1); 
          } 
        } 
      } else {
        handlerParams = (OplusBaseMtpDatabase.HandlerParams)((Message)handlerParams).obj;
        i = this.mPendingInstalls.size();
        this.mPendingInstalls.add(i, handlerParams);
        if (i == 0)
          OplusBaseMtpDatabase.this.mHandler.sendEmptyMessage(1); 
      } 
    }
  }
  
  public String getOplusMarketName(int paramInt1, int paramInt2) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("getDeviceProperty  length = ");
    stringBuilder.append(paramInt1);
    Log.i("OplusBaseMtpDatabase", stringBuilder.toString());
    if (paramInt2 == 54274) {
      String str = SystemProperties.get("ro.vendor.oplus.market.name", "");
      if (TextUtils.isEmpty(str))
        str = SystemProperties.get("ro.oppo.market.name", "OPPO MTP Device"); 
      paramInt2 = str.length();
      paramInt1 = paramInt2;
      if (paramInt2 > 255)
        paramInt1 = 255; 
      if (paramInt1 > 0) {
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append("getDeviceProperty  deviceName = ");
        stringBuilder2.append(str);
        stringBuilder2.append(", lengthDeviceName = ");
        stringBuilder2.append(paramInt1);
        Log.d("OplusBaseMtpDatabase", stringBuilder2.toString());
        return str;
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("getDeviceProperty  lengthDeviceName = ");
      stringBuilder1.append(paramInt1);
      Log.d("OplusBaseMtpDatabase", stringBuilder1.toString());
      return null;
    } 
    return null;
  }
  
  private static void updateMediaStore(Context paramContext, File paramFile) {
    ContentResolver contentResolver = paramContext.getContentResolver();
    if (!paramFile.isDirectory() && paramFile.getName().toLowerCase(Locale.ROOT).endsWith(".nomedia")) {
      MediaStore.scanFile(contentResolver, paramFile.getParentFile());
    } else {
      MediaStore.scanFile(contentResolver, paramFile);
    } 
  }
}
