package android.mtp;

public class MtpDeviceInfo {
  private int[] mEventsSupported;
  
  private String mManufacturer;
  
  private String mModel;
  
  private int[] mOperationsSupported;
  
  private String mSerialNumber;
  
  private String mVersion;
  
  public final String getManufacturer() {
    return this.mManufacturer;
  }
  
  public final String getModel() {
    return this.mModel;
  }
  
  public final String getVersion() {
    return this.mVersion;
  }
  
  public final String getSerialNumber() {
    return this.mSerialNumber;
  }
  
  public final int[] getOperationsSupported() {
    return this.mOperationsSupported;
  }
  
  public final int[] getEventsSupported() {
    return this.mEventsSupported;
  }
  
  public boolean isOperationSupported(int paramInt) {
    return isSupported(this.mOperationsSupported, paramInt);
  }
  
  public boolean isEventSupported(int paramInt) {
    return isSupported(this.mEventsSupported, paramInt);
  }
  
  private static boolean isSupported(int[] paramArrayOfint, int paramInt) {
    int i;
    byte b;
    for (i = paramArrayOfint.length, b = 0; b < i; ) {
      int j = paramArrayOfint[b];
      if (j == paramInt)
        return true; 
      b++;
    } 
    return false;
  }
}
