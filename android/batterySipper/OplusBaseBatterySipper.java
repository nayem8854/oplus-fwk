package android.batterySipper;

import android.os.Bundle;

public class OplusBaseBatterySipper {
  public static final String BundleCameraBgPowerMah = "cameraBgPowerMah";
  
  public static final String BundleCameraBgTimeMs = "cameraBgTimeMs";
  
  public static final String BundleCpuBgPowerMah = "cpuBgPowerMah";
  
  public static final String BundleGpsBgPowerMah = "gpsBgPowerMah";
  
  public static final String BundleGpsBgTimeMs = "gpsBgTimeMs";
  
  public static final String BundleIsSharedUid = "isSharedUid";
  
  public static final String BundleIsSharedUidHighestDrain = "isSharedUidHighestDrain";
  
  public static final String BundleMobileBgTtafficBytes = "mobileBgTtafficBytes";
  
  public static final String BundleMobileRadioBgPowerMah = "mobileRadioBgPowerMah";
  
  public static final String BundlePkgName = "pkgName";
  
  public static final String BundleScreenHoldTimeMs = "screenHoldTimeMs";
  
  public static final String BundleSensorBgPowerMah = "sensorBgPowerMah";
  
  public static final String BundleSensorBgTimeMs = "sensorBgTimeMs";
  
  public static final String BundleSensorTimeMs = "sensorTimeMs";
  
  public static final String BundleWifiBgPowerMah = "wifiBgPowerMah";
  
  public static final String BundleWifiBgTtafficBytes = "wifiBgTtafficBytes";
  
  public double cameraBgPowerMah;
  
  public long cameraBgTimeMs;
  
  public double cpuBgPowerMah;
  
  public double gpsBgPowerMah;
  
  public long gpsBgTimeMs;
  
  public boolean isSharedUid = false;
  
  public boolean isSharedUidHighestDrain = false;
  
  public Bundle mSipperInfo = new Bundle();
  
  public long mobileBgTtafficBytes;
  
  public double mobileRadioBgPowerMah;
  
  public String pkgName;
  
  public long screenHoldTimeMs;
  
  public double sensorBgPowerMah;
  
  public long sensorBgTimeMs;
  
  public long sensorTimeMs;
  
  public double wifiBgPowerMah;
  
  public long wifiBgTtafficBytes;
  
  public Bundle getBundleData() {
    this.mSipperInfo.putLong("gpsBgTimeMs", this.gpsBgTimeMs);
    this.mSipperInfo.putLong("sensorTimeMs", this.sensorTimeMs);
    this.mSipperInfo.putLong("sensorBgTimeMs", this.sensorBgTimeMs);
    this.mSipperInfo.putLong("screenHoldTimeMs", this.screenHoldTimeMs);
    this.mSipperInfo.putLong("cameraBgTimeMs", this.cameraBgTimeMs);
    this.mSipperInfo.putLong("wifiBgTtafficBytes", this.wifiBgTtafficBytes);
    this.mSipperInfo.putLong("mobileBgTtafficBytes", this.mobileBgTtafficBytes);
    this.mSipperInfo.putDouble("cpuBgPowerMah", this.cpuBgPowerMah);
    this.mSipperInfo.putDouble("wifiBgPowerMah", this.wifiBgPowerMah);
    this.mSipperInfo.putDouble("mobileRadioBgPowerMah", this.mobileRadioBgPowerMah);
    this.mSipperInfo.putDouble("gpsBgPowerMah", this.gpsBgPowerMah);
    this.mSipperInfo.putDouble("sensorBgPowerMah", this.sensorBgPowerMah);
    this.mSipperInfo.putDouble("cameraBgPowerMah", this.cameraBgPowerMah);
    this.mSipperInfo.putString("pkgName", this.pkgName);
    this.mSipperInfo.putBoolean("isSharedUid", this.isSharedUid);
    this.mSipperInfo.putBoolean("isSharedUidHighestDrain", this.isSharedUidHighestDrain);
    return this.mSipperInfo;
  }
  
  public void setBundleData(Bundle paramBundle) {
    if (paramBundle != null) {
      this.gpsBgTimeMs = paramBundle.getLong("gpsBgTimeMs");
      this.sensorTimeMs = paramBundle.getLong("sensorTimeMs");
      this.sensorBgTimeMs = paramBundle.getLong("sensorBgTimeMs");
      this.screenHoldTimeMs = paramBundle.getLong("screenHoldTimeMs");
      this.cameraBgTimeMs = paramBundle.getLong("cameraBgTimeMs");
      this.wifiBgTtafficBytes = paramBundle.getLong("wifiBgTtafficBytes");
      this.mobileBgTtafficBytes = paramBundle.getLong("mobileBgTtafficBytes");
      this.cpuBgPowerMah = paramBundle.getDouble("cpuBgPowerMah");
      this.wifiBgPowerMah = paramBundle.getDouble("wifiBgPowerMah");
      this.mobileRadioBgPowerMah = paramBundle.getDouble("mobileRadioBgPowerMah");
      this.gpsBgPowerMah = paramBundle.getDouble("gpsBgPowerMah");
      this.sensorBgPowerMah = paramBundle.getDouble("sensorBgPowerMah");
      this.cameraBgPowerMah = paramBundle.getDouble("cameraBgPowerMah");
      this.pkgName = paramBundle.getString("pkgName");
      this.isSharedUid = paramBundle.getBoolean("isSharedUid");
      this.isSharedUidHighestDrain = paramBundle.getBoolean("isSharedUidHighestDrain");
    } 
  }
}
