package vendor.oplus.hardware.performance.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class ProcReqItemHal {
  public String key = new String();
  
  public String label = new String();
  
  public int uploadType = 0;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != ProcReqItemHal.class)
      return false; 
    paramObject = paramObject;
    if (!HidlSupport.deepEquals(this.key, ((ProcReqItemHal)paramObject).key))
      return false; 
    if (!HidlSupport.deepEquals(this.label, ((ProcReqItemHal)paramObject).label))
      return false; 
    if (this.uploadType != ((ProcReqItemHal)paramObject).uploadType)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    String str = this.key;
    int i = HidlSupport.deepHashCode(str);
    str = this.label;
    int j = HidlSupport.deepHashCode(str), k = this.uploadType;
    k = HidlSupport.deepHashCode(Integer.valueOf(k));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".key = ");
    stringBuilder.append(this.key);
    stringBuilder.append(", .label = ");
    stringBuilder.append(this.label);
    stringBuilder.append(", .uploadType = ");
    stringBuilder.append(UploadType.toString(this.uploadType));
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(40L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<ProcReqItemHal> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<ProcReqItemHal> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 40);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      ProcReqItemHal procReqItemHal = new ProcReqItemHal();
      procReqItemHal.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 40));
      arrayList.add(procReqItemHal);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    String str = paramHwBlob.getString(paramLong + 0L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 0L + 0L, false);
    this.label = str = paramHwBlob.getString(paramLong + 16L);
    l2 = ((str.getBytes()).length + 1);
    l1 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l2, l1, paramLong + 16L + 0L, false);
    this.uploadType = paramHwBlob.getInt32(paramLong + 32L);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(40);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<ProcReqItemHal> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 40);
    for (byte b = 0; b < i; b++)
      ((ProcReqItemHal)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 40)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putString(0L + paramLong, this.key);
    paramHwBlob.putString(16L + paramLong, this.label);
    paramHwBlob.putInt32(32L + paramLong, this.uploadType);
  }
}
