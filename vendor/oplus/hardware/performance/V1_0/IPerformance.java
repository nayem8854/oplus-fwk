package vendor.oplus.hardware.performance.V1_0;

import android.internal.hidl.base.V1_0.DebugInfo;
import android.internal.hidl.base.V1_0.IBase;
import android.os.HidlSupport;
import android.os.HwBinder;
import android.os.HwBlob;
import android.os.HwParcel;
import android.os.IHwBinder;
import android.os.IHwInterface;
import android.os.NativeHandle;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public interface IPerformance extends IBase {
  public static final String kInterfaceName = "vendor.oplus.hardware.performance@1.0::IPerformance";
  
  static IPerformance asInterface(IHwBinder paramIHwBinder) {
    if (paramIHwBinder == null)
      return null; 
    IHwInterface iHwInterface = paramIHwBinder.queryLocalInterface("vendor.oplus.hardware.performance@1.0::IPerformance");
    if (iHwInterface != null && iHwInterface instanceof IPerformance)
      return (IPerformance)iHwInterface; 
    Proxy proxy = new Proxy(paramIHwBinder);
    try {
      for (String str : proxy.interfaceChain()) {
        boolean bool = str.equals("vendor.oplus.hardware.performance@1.0::IPerformance");
        if (bool)
          return proxy; 
      } 
    } catch (RemoteException remoteException) {}
    return null;
  }
  
  static IPerformance castFrom(IHwInterface paramIHwInterface) {
    IPerformance iPerformance;
    if (paramIHwInterface == null) {
      paramIHwInterface = null;
    } else {
      iPerformance = asInterface(paramIHwInterface.asBinder());
    } 
    return iPerformance;
  }
  
  static IPerformance getService(String paramString, boolean paramBoolean) throws RemoteException {
    return asInterface(HwBinder.getService("vendor.oplus.hardware.performance@1.0::IPerformance", paramString, paramBoolean));
  }
  
  static IPerformance getService(boolean paramBoolean) throws RemoteException {
    return getService("default", paramBoolean);
  }
  
  static IPerformance getService(String paramString) throws RemoteException {
    return asInterface(HwBinder.getService("vendor.oplus.hardware.performance@1.0::IPerformance", paramString));
  }
  
  static IPerformance getService() throws RemoteException {
    return getService("default");
  }
  
  IHwBinder asBinder();
  
  void debug(NativeHandle paramNativeHandle, ArrayList<String> paramArrayList) throws RemoteException;
  
  int disableMultiThreadOptimize() throws RemoteException;
  
  int disableTaskCpustats() throws RemoteException;
  
  int enableMultiThreadOptimize() throws RemoteException;
  
  int enableTaskCpustats() throws RemoteException;
  
  DebugInfo getDebugInfo() throws RemoteException;
  
  String getExt4FragScore(String paramString) throws RemoteException;
  
  String getExt4FreefragInfo(String paramString) throws RemoteException;
  
  String getF2fsMovedBlks() throws RemoteException;
  
  ProcReqHal getHIAllocWait() throws RemoteException;
  
  String getHICpuInfo() throws RemoteException;
  
  ProcReqHal getHICpuLoading() throws RemoteException;
  
  ProcReqHal getHIDState() throws RemoteException;
  
  ProcReqHal getHIEmcdrvIowait() throws RemoteException;
  
  ProcReqHal getHIFsyncWait() throws RemoteException;
  
  ProcReqHal getHIIonWait() throws RemoteException;
  
  ProcReqHal getHIIowait() throws RemoteException;
  
  ProcReqHal getHIIowaitHung() throws RemoteException;
  
  ProcReqHal getHIKswapdLoading() throws RemoteException;
  
  ProcReqHal getHISchedLatency() throws RemoteException;
  
  ProcReqHal getHIScmCall() throws RemoteException;
  
  ProcReqHal getHIUfsFeature() throws RemoteException;
  
  ArrayList<byte[]> getHashChain() throws RemoteException;
  
  ArrayList<String> interfaceChain() throws RemoteException;
  
  String interfaceDescriptor() throws RemoteException;
  
  boolean linkToDeath(IHwBinder.DeathRecipient paramDeathRecipient, long paramLong) throws RemoteException;
  
  void notifySyspropsChanged() throws RemoteException;
  
  void ping() throws RemoteException;
  
  String readCpuTaskstats() throws RemoteException;
  
  String readIomonitorInfo(String paramString) throws RemoteException;
  
  String readSgeInfo() throws RemoteException;
  
  void setHALInstrumentation() throws RemoteException;
  
  boolean unlinkToDeath(IHwBinder.DeathRecipient paramDeathRecipient) throws RemoteException;
  
  class Proxy implements IPerformance {
    private IHwBinder mRemote;
    
    public Proxy(IPerformance this$0) {
      Objects.requireNonNull(this$0);
      this.mRemote = (IHwBinder)this$0;
    }
    
    public IHwBinder asBinder() {
      return this.mRemote;
    }
    
    public String toString() {
      try {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append(interfaceDescriptor());
        stringBuilder.append("@Proxy");
        return stringBuilder.toString();
      } catch (RemoteException remoteException) {
        return "[class or subclass of vendor.oplus.hardware.performance@1.0::IPerformance]@Proxy";
      } 
    }
    
    public final boolean equals(Object param1Object) {
      return HidlSupport.interfacesEqual((IHwInterface)this, param1Object);
    }
    
    public final int hashCode() {
      return asBinder().hashCode();
    }
    
    public int enableMultiThreadOptimize() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("vendor.oplus.hardware.performance@1.0::IPerformance");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(1, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return hwParcel.readInt32();
      } finally {
        hwParcel.release();
      } 
    }
    
    public int disableMultiThreadOptimize() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("vendor.oplus.hardware.performance@1.0::IPerformance");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(2, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return hwParcel.readInt32();
      } finally {
        hwParcel.release();
      } 
    }
    
    public int enableTaskCpustats() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("vendor.oplus.hardware.performance@1.0::IPerformance");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(3, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return hwParcel.readInt32();
      } finally {
        hwParcel.release();
      } 
    }
    
    public int disableTaskCpustats() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("vendor.oplus.hardware.performance@1.0::IPerformance");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(4, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return hwParcel.readInt32();
      } finally {
        hwParcel.release();
      } 
    }
    
    public String readCpuTaskstats() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("vendor.oplus.hardware.performance@1.0::IPerformance");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(5, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return hwParcel.readString();
      } finally {
        hwParcel.release();
      } 
    }
    
    public String readSgeInfo() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("vendor.oplus.hardware.performance@1.0::IPerformance");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(6, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return hwParcel.readString();
      } finally {
        hwParcel.release();
      } 
    }
    
    public String readIomonitorInfo(String param1String) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("vendor.oplus.hardware.performance@1.0::IPerformance");
      null.writeString(param1String);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(7, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return hwParcel.readString();
      } finally {
        hwParcel.release();
      } 
    }
    
    public ProcReqHal getHICpuLoading() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("vendor.oplus.hardware.performance@1.0::IPerformance");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(8, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        ProcReqHal procReqHal = new ProcReqHal();
        this();
        procReqHal.readFromParcel(hwParcel);
        return procReqHal;
      } finally {
        hwParcel.release();
      } 
    }
    
    public ProcReqHal getHIEmcdrvIowait() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("vendor.oplus.hardware.performance@1.0::IPerformance");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(9, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        ProcReqHal procReqHal = new ProcReqHal();
        this();
        procReqHal.readFromParcel(hwParcel);
        return procReqHal;
      } finally {
        hwParcel.release();
      } 
    }
    
    public ProcReqHal getHIIowaitHung() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("vendor.oplus.hardware.performance@1.0::IPerformance");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(10, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        ProcReqHal procReqHal = new ProcReqHal();
        this();
        procReqHal.readFromParcel(hwParcel);
        return procReqHal;
      } finally {
        hwParcel.release();
      } 
    }
    
    public ProcReqHal getHISchedLatency() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("vendor.oplus.hardware.performance@1.0::IPerformance");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(11, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        ProcReqHal procReqHal = new ProcReqHal();
        this();
        procReqHal.readFromParcel(hwParcel);
        return procReqHal;
      } finally {
        hwParcel.release();
      } 
    }
    
    public ProcReqHal getHIFsyncWait() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("vendor.oplus.hardware.performance@1.0::IPerformance");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(12, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        ProcReqHal procReqHal = new ProcReqHal();
        this();
        procReqHal.readFromParcel(hwParcel);
        return procReqHal;
      } finally {
        hwParcel.release();
      } 
    }
    
    public ProcReqHal getHIIowait() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("vendor.oplus.hardware.performance@1.0::IPerformance");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(13, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        ProcReqHal procReqHal = new ProcReqHal();
        this();
        procReqHal.readFromParcel(hwParcel);
        return procReqHal;
      } finally {
        hwParcel.release();
      } 
    }
    
    public ProcReqHal getHIUfsFeature() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("vendor.oplus.hardware.performance@1.0::IPerformance");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(14, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        ProcReqHal procReqHal = new ProcReqHal();
        this();
        procReqHal.readFromParcel(hwParcel);
        return procReqHal;
      } finally {
        hwParcel.release();
      } 
    }
    
    public ProcReqHal getHIIonWait() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("vendor.oplus.hardware.performance@1.0::IPerformance");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(15, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        ProcReqHal procReqHal = new ProcReqHal();
        this();
        procReqHal.readFromParcel(hwParcel);
        return procReqHal;
      } finally {
        hwParcel.release();
      } 
    }
    
    public ProcReqHal getHIAllocWait() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("vendor.oplus.hardware.performance@1.0::IPerformance");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(16, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        ProcReqHal procReqHal = new ProcReqHal();
        this();
        procReqHal.readFromParcel(hwParcel);
        return procReqHal;
      } finally {
        hwParcel.release();
      } 
    }
    
    public ProcReqHal getHIDState() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("vendor.oplus.hardware.performance@1.0::IPerformance");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(17, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        ProcReqHal procReqHal = new ProcReqHal();
        this();
        procReqHal.readFromParcel(hwParcel);
        return procReqHal;
      } finally {
        hwParcel.release();
      } 
    }
    
    public ProcReqHal getHIKswapdLoading() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("vendor.oplus.hardware.performance@1.0::IPerformance");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(18, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        ProcReqHal procReqHal = new ProcReqHal();
        this();
        procReqHal.readFromParcel(hwParcel);
        return procReqHal;
      } finally {
        hwParcel.release();
      } 
    }
    
    public ProcReqHal getHIScmCall() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("vendor.oplus.hardware.performance@1.0::IPerformance");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(19, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        ProcReqHal procReqHal = new ProcReqHal();
        this();
        procReqHal.readFromParcel(hwParcel);
        return procReqHal;
      } finally {
        hwParcel.release();
      } 
    }
    
    public String getHICpuInfo() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("vendor.oplus.hardware.performance@1.0::IPerformance");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(20, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return hwParcel.readString();
      } finally {
        hwParcel.release();
      } 
    }
    
    public String getExt4FreefragInfo(String param1String) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("vendor.oplus.hardware.performance@1.0::IPerformance");
      null.writeString(param1String);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(21, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return hwParcel.readString();
      } finally {
        hwParcel.release();
      } 
    }
    
    public String getExt4FragScore(String param1String) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("vendor.oplus.hardware.performance@1.0::IPerformance");
      null.writeString(param1String);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(22, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return hwParcel.readString();
      } finally {
        hwParcel.release();
      } 
    }
    
    public String getF2fsMovedBlks() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("vendor.oplus.hardware.performance@1.0::IPerformance");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(23, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return hwParcel.readString();
      } finally {
        hwParcel.release();
      } 
    }
    
    public ArrayList<String> interfaceChain() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256067662, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return hwParcel.readStringVector();
      } finally {
        hwParcel.release();
      } 
    }
    
    public void debug(NativeHandle param1NativeHandle, ArrayList<String> param1ArrayList) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hidl.base@1.0::IBase");
      hwParcel2.writeNativeHandle(param1NativeHandle);
      hwParcel2.writeStringVector(param1ArrayList);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(256131655, hwParcel2, hwParcel1, 0);
        hwParcel1.verifySuccess();
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public String interfaceDescriptor() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256136003, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return hwParcel.readString();
      } finally {
        hwParcel.release();
      } 
    }
    
    public ArrayList<byte[]> getHashChain() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256398152, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        ArrayList<byte[]> arrayList = new ArrayList();
        this();
        HwBlob hwBlob = hwParcel.readBuffer(16L);
        int i = hwBlob.getInt32(8L);
        long l1 = (i * 32);
        long l2 = hwBlob.handle();
        hwBlob = hwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
        arrayList.clear();
        for (byte b = 0; b < i; b++) {
          byte[] arrayOfByte = new byte[32];
          l1 = (b * 32);
          hwBlob.copyToInt8Array(l1, arrayOfByte, 32);
          arrayList.add(arrayOfByte);
        } 
        return arrayList;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setHALInstrumentation() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256462420, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public boolean linkToDeath(IHwBinder.DeathRecipient param1DeathRecipient, long param1Long) throws RemoteException {
      return this.mRemote.linkToDeath(param1DeathRecipient, param1Long);
    }
    
    public void ping() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256921159, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public DebugInfo getDebugInfo() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(257049926, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        DebugInfo debugInfo = new DebugInfo();
        this();
        debugInfo.readFromParcel(hwParcel);
        return debugInfo;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void notifySyspropsChanged() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(257120595, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public boolean unlinkToDeath(IHwBinder.DeathRecipient param1DeathRecipient) throws RemoteException {
      return this.mRemote.unlinkToDeath(param1DeathRecipient);
    }
  }
  
  class Stub extends HwBinder implements IPerformance {
    public IHwBinder asBinder() {
      return (IHwBinder)this;
    }
    
    public final ArrayList<String> interfaceChain() {
      return new ArrayList<>(Arrays.asList(new String[] { "vendor.oplus.hardware.performance@1.0::IPerformance", "android.hidl.base@1.0::IBase" }));
    }
    
    public void debug(NativeHandle param1NativeHandle, ArrayList<String> param1ArrayList) {}
    
    public final String interfaceDescriptor() {
      return "vendor.oplus.hardware.performance@1.0::IPerformance";
    }
    
    public final ArrayList<byte[]> getHashChain() {
      return (ArrayList)new ArrayList<>(Arrays.asList((byte[])new byte[][] { { 
                54, -89, 40, 62, 21, -32, 68, -1, 61, -52, 
                84, -42, 12, 48, 28, 33, -95, -75, 116, 62, 
                73, -24, 34, -70, -88, -94, 90, 28, 71, -67, 
                31, -77 }, { 
                -20, Byte.MAX_VALUE, -41, -98, -48, 45, -6, -123, -68, 73, 
                -108, 38, -83, -82, 62, -66, 35, -17, 5, 36, 
                -13, -51, 105, 87, 19, -109, 36, -72, 59, 24, 
                -54, 76 } }));
    }
    
    public final void setHALInstrumentation() {}
    
    public final boolean linkToDeath(IHwBinder.DeathRecipient param1DeathRecipient, long param1Long) {
      return true;
    }
    
    public final void ping() {}
    
    public final DebugInfo getDebugInfo() {
      DebugInfo debugInfo = new DebugInfo();
      debugInfo.pid = HidlSupport.getPidIfSharable();
      debugInfo.ptr = 0L;
      debugInfo.arch = 0;
      return debugInfo;
    }
    
    public final void notifySyspropsChanged() {
      HwBinder.enableInstrumentation();
    }
    
    public final boolean unlinkToDeath(IHwBinder.DeathRecipient param1DeathRecipient) {
      return true;
    }
    
    public IHwInterface queryLocalInterface(String param1String) {
      if ("vendor.oplus.hardware.performance@1.0::IPerformance".equals(param1String))
        return (IHwInterface)this; 
      return null;
    }
    
    public void registerAsService(String param1String) throws RemoteException {
      registerService(param1String);
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(interfaceDescriptor());
      stringBuilder.append("@Stub");
      return stringBuilder.toString();
    }
    
    public void onTransact(int param1Int1, HwParcel param1HwParcel1, HwParcel param1HwParcel2, int param1Int2) throws RemoteException {
      DebugInfo debugInfo;
      HwBlob hwBlob1;
      String str3;
      ArrayList<String> arrayList;
      String str2;
      ProcReqHal procReqHal;
      String str1;
      ArrayList<byte[]> arrayList1;
      HwBlob hwBlob2;
      NativeHandle nativeHandle;
      switch (param1Int1) {
        default:
          switch (param1Int1) {
            default:
              return;
            case 257120595:
              param1HwParcel1.enforceInterface("android.hidl.base@1.0::IBase");
              notifySyspropsChanged();
            case 257049926:
              param1HwParcel1.enforceInterface("android.hidl.base@1.0::IBase");
              debugInfo = getDebugInfo();
              param1HwParcel2.writeStatus(0);
              debugInfo.writeToParcel(param1HwParcel2);
              param1HwParcel2.send();
            case 256921159:
              debugInfo.enforceInterface("android.hidl.base@1.0::IBase");
              ping();
              param1HwParcel2.writeStatus(0);
              param1HwParcel2.send();
            case 256462420:
              debugInfo.enforceInterface("android.hidl.base@1.0::IBase");
              setHALInstrumentation();
            case 256398152:
              debugInfo.enforceInterface("android.hidl.base@1.0::IBase");
              arrayList1 = getHashChain();
              param1HwParcel2.writeStatus(0);
              hwBlob2 = new HwBlob(16);
              param1Int2 = arrayList1.size();
              hwBlob2.putInt32(8L, param1Int2);
              hwBlob2.putBool(12L, false);
              hwBlob1 = new HwBlob(param1Int2 * 32);
              for (param1Int1 = 0; param1Int1 < param1Int2; ) {
                long l = (param1Int1 * 32);
                byte[] arrayOfByte = arrayList1.get(param1Int1);
                if (arrayOfByte != null && arrayOfByte.length == 32) {
                  hwBlob1.putInt8Array(l, arrayOfByte);
                  param1Int1++;
                } 
                throw new IllegalArgumentException("Array element is not of the expected length");
              } 
              hwBlob2.putBlob(0L, hwBlob1);
              param1HwParcel2.writeBuffer(hwBlob2);
              param1HwParcel2.send();
            case 256136003:
              hwBlob1.enforceInterface("android.hidl.base@1.0::IBase");
              str3 = interfaceDescriptor();
              param1HwParcel2.writeStatus(0);
              param1HwParcel2.writeString(str3);
              param1HwParcel2.send();
            case 256131655:
              str3.enforceInterface("android.hidl.base@1.0::IBase");
              nativeHandle = str3.readNativeHandle();
              arrayList = str3.readStringVector();
              debug(nativeHandle, arrayList);
              param1HwParcel2.writeStatus(0);
              param1HwParcel2.send();
            case 256067662:
              break;
          } 
          arrayList.enforceInterface("android.hidl.base@1.0::IBase");
          arrayList = interfaceChain();
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.writeStringVector(arrayList);
          param1HwParcel2.send();
        case 23:
          arrayList.enforceInterface("vendor.oplus.hardware.performance@1.0::IPerformance");
          str2 = getF2fsMovedBlks();
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.writeString(str2);
          param1HwParcel2.send();
        case 22:
          str2.enforceInterface("vendor.oplus.hardware.performance@1.0::IPerformance");
          str2 = str2.readString();
          str2 = getExt4FragScore(str2);
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.writeString(str2);
          param1HwParcel2.send();
        case 21:
          str2.enforceInterface("vendor.oplus.hardware.performance@1.0::IPerformance");
          str2 = str2.readString();
          str2 = getExt4FreefragInfo(str2);
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.writeString(str2);
          param1HwParcel2.send();
        case 20:
          str2.enforceInterface("vendor.oplus.hardware.performance@1.0::IPerformance");
          str2 = getHICpuInfo();
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.writeString(str2);
          param1HwParcel2.send();
        case 19:
          str2.enforceInterface("vendor.oplus.hardware.performance@1.0::IPerformance");
          procReqHal = getHIScmCall();
          param1HwParcel2.writeStatus(0);
          procReqHal.writeToParcel(param1HwParcel2);
          param1HwParcel2.send();
        case 18:
          procReqHal.enforceInterface("vendor.oplus.hardware.performance@1.0::IPerformance");
          procReqHal = getHIKswapdLoading();
          param1HwParcel2.writeStatus(0);
          procReqHal.writeToParcel(param1HwParcel2);
          param1HwParcel2.send();
        case 17:
          procReqHal.enforceInterface("vendor.oplus.hardware.performance@1.0::IPerformance");
          procReqHal = getHIDState();
          param1HwParcel2.writeStatus(0);
          procReqHal.writeToParcel(param1HwParcel2);
          param1HwParcel2.send();
        case 16:
          procReqHal.enforceInterface("vendor.oplus.hardware.performance@1.0::IPerformance");
          procReqHal = getHIAllocWait();
          param1HwParcel2.writeStatus(0);
          procReqHal.writeToParcel(param1HwParcel2);
          param1HwParcel2.send();
        case 15:
          procReqHal.enforceInterface("vendor.oplus.hardware.performance@1.0::IPerformance");
          procReqHal = getHIIonWait();
          param1HwParcel2.writeStatus(0);
          procReqHal.writeToParcel(param1HwParcel2);
          param1HwParcel2.send();
        case 14:
          procReqHal.enforceInterface("vendor.oplus.hardware.performance@1.0::IPerformance");
          procReqHal = getHIUfsFeature();
          param1HwParcel2.writeStatus(0);
          procReqHal.writeToParcel(param1HwParcel2);
          param1HwParcel2.send();
        case 13:
          procReqHal.enforceInterface("vendor.oplus.hardware.performance@1.0::IPerformance");
          procReqHal = getHIIowait();
          param1HwParcel2.writeStatus(0);
          procReqHal.writeToParcel(param1HwParcel2);
          param1HwParcel2.send();
        case 12:
          procReqHal.enforceInterface("vendor.oplus.hardware.performance@1.0::IPerformance");
          procReqHal = getHIFsyncWait();
          param1HwParcel2.writeStatus(0);
          procReqHal.writeToParcel(param1HwParcel2);
          param1HwParcel2.send();
        case 11:
          procReqHal.enforceInterface("vendor.oplus.hardware.performance@1.0::IPerformance");
          procReqHal = getHISchedLatency();
          param1HwParcel2.writeStatus(0);
          procReqHal.writeToParcel(param1HwParcel2);
          param1HwParcel2.send();
        case 10:
          procReqHal.enforceInterface("vendor.oplus.hardware.performance@1.0::IPerformance");
          procReqHal = getHIIowaitHung();
          param1HwParcel2.writeStatus(0);
          procReqHal.writeToParcel(param1HwParcel2);
          param1HwParcel2.send();
        case 9:
          procReqHal.enforceInterface("vendor.oplus.hardware.performance@1.0::IPerformance");
          procReqHal = getHIEmcdrvIowait();
          param1HwParcel2.writeStatus(0);
          procReqHal.writeToParcel(param1HwParcel2);
          param1HwParcel2.send();
        case 8:
          procReqHal.enforceInterface("vendor.oplus.hardware.performance@1.0::IPerformance");
          procReqHal = getHICpuLoading();
          param1HwParcel2.writeStatus(0);
          procReqHal.writeToParcel(param1HwParcel2);
          param1HwParcel2.send();
        case 7:
          procReqHal.enforceInterface("vendor.oplus.hardware.performance@1.0::IPerformance");
          str1 = procReqHal.readString();
          str1 = readIomonitorInfo(str1);
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.writeString(str1);
          param1HwParcel2.send();
        case 6:
          str1.enforceInterface("vendor.oplus.hardware.performance@1.0::IPerformance");
          str1 = readSgeInfo();
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.writeString(str1);
          param1HwParcel2.send();
        case 5:
          str1.enforceInterface("vendor.oplus.hardware.performance@1.0::IPerformance");
          str1 = readCpuTaskstats();
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.writeString(str1);
          param1HwParcel2.send();
        case 4:
          str1.enforceInterface("vendor.oplus.hardware.performance@1.0::IPerformance");
          param1Int1 = disableTaskCpustats();
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.writeInt32(param1Int1);
          param1HwParcel2.send();
        case 3:
          str1.enforceInterface("vendor.oplus.hardware.performance@1.0::IPerformance");
          param1Int1 = enableTaskCpustats();
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.writeInt32(param1Int1);
          param1HwParcel2.send();
        case 2:
          str1.enforceInterface("vendor.oplus.hardware.performance@1.0::IPerformance");
          param1Int1 = disableMultiThreadOptimize();
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.writeInt32(param1Int1);
          param1HwParcel2.send();
        case 1:
          break;
      } 
      str1.enforceInterface("vendor.oplus.hardware.performance@1.0::IPerformance");
      param1Int1 = enableMultiThreadOptimize();
      param1HwParcel2.writeStatus(0);
      param1HwParcel2.writeInt32(param1Int1);
      param1HwParcel2.send();
    }
  }
}
