package vendor.oplus.hardware.performance.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class ProcReqHal {
  public String proc = new String();
  
  public ArrayList<ProcReqItemHal> items = new ArrayList<>();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != ProcReqHal.class)
      return false; 
    paramObject = paramObject;
    if (!HidlSupport.deepEquals(this.proc, ((ProcReqHal)paramObject).proc))
      return false; 
    if (!HidlSupport.deepEquals(this.items, ((ProcReqHal)paramObject).items))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    String str = this.proc;
    int i = HidlSupport.deepHashCode(str);
    ArrayList<ProcReqItemHal> arrayList = this.items;
    int j = HidlSupport.deepHashCode(arrayList);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".proc = ");
    stringBuilder.append(this.proc);
    stringBuilder.append(", .items = ");
    stringBuilder.append(this.items);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(32L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<ProcReqHal> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<ProcReqHal> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 32);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      ProcReqHal procReqHal = new ProcReqHal();
      procReqHal.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 32));
      arrayList.add(procReqHal);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    String str = paramHwBlob.getString(paramLong + 0L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 0L + 0L, false);
    int i = paramHwBlob.getInt32(paramLong + 16L + 8L);
    l2 = (i * 40);
    l1 = paramHwBlob.handle();
    paramHwBlob = paramHwParcel.readEmbeddedBuffer(l2, l1, paramLong + 16L + 0L, true);
    this.items.clear();
    for (byte b = 0; b < i; b++) {
      ProcReqItemHal procReqItemHal = new ProcReqItemHal();
      procReqItemHal.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, (b * 40));
      this.items.add(procReqItemHal);
    } 
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(32);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<ProcReqHal> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 32);
    for (byte b = 0; b < i; b++)
      ((ProcReqHal)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 32)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putString(paramLong + 0L, this.proc);
    int i = this.items.size();
    paramHwBlob.putInt32(paramLong + 16L + 8L, i);
    paramHwBlob.putBool(paramLong + 16L + 12L, false);
    HwBlob hwBlob = new HwBlob(i * 40);
    for (byte b = 0; b < i; b++)
      ((ProcReqItemHal)this.items.get(b)).writeEmbeddedToBlob(hwBlob, (b * 40)); 
    paramHwBlob.putBlob(16L + paramLong + 0L, hwBlob);
  }
}
