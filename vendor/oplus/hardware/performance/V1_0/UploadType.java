package vendor.oplus.hardware.performance.V1_0;

import java.util.ArrayList;

public final class UploadType {
  public static final int DELTA = 1;
  
  public static final int PRIMITIVE = 0;
  
  public static final int SKIP = 2;
  
  public static final String toString(int paramInt) {
    if (paramInt == 0)
      return "PRIMITIVE"; 
    if (paramInt == 1)
      return "DELTA"; 
    if (paramInt == 2)
      return "SKIP"; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    return stringBuilder.toString();
  }
  
  public static final String dumpBitfield(int paramInt) {
    ArrayList<String> arrayList = new ArrayList();
    int i = 0;
    arrayList.add("PRIMITIVE");
    if ((paramInt & 0x1) == 1) {
      arrayList.add("DELTA");
      i = false | true;
    } 
    int j = i;
    if ((paramInt & 0x2) == 2) {
      arrayList.add("SKIP");
      j = i | 0x2;
    } 
    if (paramInt != j) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Integer.toHexString((j ^ 0xFFFFFFFF) & paramInt));
      arrayList.add(stringBuilder.toString());
    } 
    return String.join(" | ", (Iterable)arrayList);
  }
}
