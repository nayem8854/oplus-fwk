package vendor.oplus.hardware.stability.oplus_project.V1_0;

import android.internal.hidl.base.V1_0.DebugInfo;
import android.internal.hidl.base.V1_0.IBase;
import android.os.HidlSupport;
import android.os.HwBinder;
import android.os.HwBlob;
import android.os.HwParcel;
import android.os.IHwBinder;
import android.os.IHwInterface;
import android.os.NativeHandle;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public interface IOplusProject extends IBase {
  public static final String kInterfaceName = "vendor.oplus.hardware.stability.oplus_project@1.0::IOplusProject";
  
  static IOplusProject asInterface(IHwBinder paramIHwBinder) {
    if (paramIHwBinder == null)
      return null; 
    IHwInterface iHwInterface = paramIHwBinder.queryLocalInterface("vendor.oplus.hardware.stability.oplus_project@1.0::IOplusProject");
    if (iHwInterface != null && iHwInterface instanceof IOplusProject)
      return (IOplusProject)iHwInterface; 
    Proxy proxy = new Proxy(paramIHwBinder);
    try {
      for (String str : proxy.interfaceChain()) {
        boolean bool = str.equals("vendor.oplus.hardware.stability.oplus_project@1.0::IOplusProject");
        if (bool)
          return proxy; 
      } 
    } catch (RemoteException remoteException) {}
    return null;
  }
  
  static IOplusProject castFrom(IHwInterface paramIHwInterface) {
    IOplusProject iOplusProject;
    if (paramIHwInterface == null) {
      paramIHwInterface = null;
    } else {
      iOplusProject = asInterface(paramIHwInterface.asBinder());
    } 
    return iOplusProject;
  }
  
  static IOplusProject getService(String paramString, boolean paramBoolean) throws RemoteException {
    return asInterface(HwBinder.getService("vendor.oplus.hardware.stability.oplus_project@1.0::IOplusProject", paramString, paramBoolean));
  }
  
  static IOplusProject getService(boolean paramBoolean) throws RemoteException {
    return getService("default", paramBoolean);
  }
  
  static IOplusProject getService(String paramString) throws RemoteException {
    return asInterface(HwBinder.getService("vendor.oplus.hardware.stability.oplus_project@1.0::IOplusProject", paramString));
  }
  
  static IOplusProject getService() throws RemoteException {
    return getService("default");
  }
  
  IHwBinder asBinder();
  
  void debug(NativeHandle paramNativeHandle, ArrayList<String> paramArrayList) throws RemoteException;
  
  DebugInfo getDebugInfo() throws RemoteException;
  
  ArrayList<byte[]> getHashChain() throws RemoteException;
  
  int get_eng_version() throws RemoteException;
  
  int get_operator_name() throws RemoteException;
  
  int get_pcb_version() throws RemoteException;
  
  int get_project() throws RemoteException;
  
  int get_rf_type() throws RemoteException;
  
  String get_serial_ID() throws RemoteException;
  
  ArrayList<String> interfaceChain() throws RemoteException;
  
  String interfaceDescriptor() throws RemoteException;
  
  boolean linkToDeath(IHwBinder.DeathRecipient paramDeathRecipient, long paramLong) throws RemoteException;
  
  void notifySyspropsChanged() throws RemoteException;
  
  void ping() throws RemoteException;
  
  void setHALInstrumentation() throws RemoteException;
  
  boolean unlinkToDeath(IHwBinder.DeathRecipient paramDeathRecipient) throws RemoteException;
  
  class Proxy implements IOplusProject {
    private IHwBinder mRemote;
    
    public Proxy(IOplusProject this$0) {
      Objects.requireNonNull(this$0);
      this.mRemote = (IHwBinder)this$0;
    }
    
    public IHwBinder asBinder() {
      return this.mRemote;
    }
    
    public String toString() {
      try {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append(interfaceDescriptor());
        stringBuilder.append("@Proxy");
        return stringBuilder.toString();
      } catch (RemoteException remoteException) {
        return "[class or subclass of vendor.oplus.hardware.stability.oplus_project@1.0::IOplusProject]@Proxy";
      } 
    }
    
    public final boolean equals(Object param1Object) {
      return HidlSupport.interfacesEqual((IHwInterface)this, param1Object);
    }
    
    public final int hashCode() {
      return asBinder().hashCode();
    }
    
    public int get_project() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("vendor.oplus.hardware.stability.oplus_project@1.0::IOplusProject");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(1, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return hwParcel.readInt32();
      } finally {
        hwParcel.release();
      } 
    }
    
    public int get_pcb_version() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("vendor.oplus.hardware.stability.oplus_project@1.0::IOplusProject");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(2, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return hwParcel.readInt32();
      } finally {
        hwParcel.release();
      } 
    }
    
    public String get_serial_ID() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("vendor.oplus.hardware.stability.oplus_project@1.0::IOplusProject");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(3, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return hwParcel.readString();
      } finally {
        hwParcel.release();
      } 
    }
    
    public int get_operator_name() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("vendor.oplus.hardware.stability.oplus_project@1.0::IOplusProject");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(4, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return hwParcel.readInt32();
      } finally {
        hwParcel.release();
      } 
    }
    
    public int get_rf_type() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("vendor.oplus.hardware.stability.oplus_project@1.0::IOplusProject");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(5, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return hwParcel.readInt32();
      } finally {
        hwParcel.release();
      } 
    }
    
    public int get_eng_version() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("vendor.oplus.hardware.stability.oplus_project@1.0::IOplusProject");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(6, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return hwParcel.readInt32();
      } finally {
        hwParcel.release();
      } 
    }
    
    public ArrayList<String> interfaceChain() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256067662, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return hwParcel.readStringVector();
      } finally {
        hwParcel.release();
      } 
    }
    
    public void debug(NativeHandle param1NativeHandle, ArrayList<String> param1ArrayList) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hidl.base@1.0::IBase");
      hwParcel2.writeNativeHandle(param1NativeHandle);
      hwParcel2.writeStringVector(param1ArrayList);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(256131655, hwParcel2, hwParcel1, 0);
        hwParcel1.verifySuccess();
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public String interfaceDescriptor() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256136003, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return hwParcel.readString();
      } finally {
        hwParcel.release();
      } 
    }
    
    public ArrayList<byte[]> getHashChain() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256398152, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        ArrayList<byte[]> arrayList = new ArrayList();
        this();
        HwBlob hwBlob1 = hwParcel.readBuffer(16L);
        int i = hwBlob1.getInt32(8L);
        long l1 = (i * 32);
        long l2 = hwBlob1.handle();
        HwBlob hwBlob2 = hwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
        arrayList.clear();
        for (byte b = 0; b < i; b++) {
          byte[] arrayOfByte = new byte[32];
          l2 = (b * 32);
          hwBlob2.copyToInt8Array(l2, arrayOfByte, 32);
          arrayList.add(arrayOfByte);
        } 
        return arrayList;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setHALInstrumentation() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256462420, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public boolean linkToDeath(IHwBinder.DeathRecipient param1DeathRecipient, long param1Long) throws RemoteException {
      return this.mRemote.linkToDeath(param1DeathRecipient, param1Long);
    }
    
    public void ping() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256921159, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public DebugInfo getDebugInfo() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(257049926, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        DebugInfo debugInfo = new DebugInfo();
        this();
        debugInfo.readFromParcel(hwParcel);
        return debugInfo;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void notifySyspropsChanged() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(257120595, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public boolean unlinkToDeath(IHwBinder.DeathRecipient param1DeathRecipient) throws RemoteException {
      return this.mRemote.unlinkToDeath(param1DeathRecipient);
    }
  }
  
  class Stub extends HwBinder implements IOplusProject {
    public IHwBinder asBinder() {
      return (IHwBinder)this;
    }
    
    public final ArrayList<String> interfaceChain() {
      return new ArrayList<>(Arrays.asList(new String[] { "vendor.oplus.hardware.stability.oplus_project@1.0::IOplusProject", "android.hidl.base@1.0::IBase" }));
    }
    
    public void debug(NativeHandle param1NativeHandle, ArrayList<String> param1ArrayList) {}
    
    public final String interfaceDescriptor() {
      return "vendor.oplus.hardware.stability.oplus_project@1.0::IOplusProject";
    }
    
    public final ArrayList<byte[]> getHashChain() {
      byte[] arrayOfByte = { 
          -20, Byte.MAX_VALUE, -41, -98, -48, 45, -6, -123, -68, 73, 
          -108, 38, -83, -82, 62, -66, 35, -17, 5, 36, 
          -13, -51, 105, 87, 19, -109, 36, -72, 59, 24, 
          -54, 76 };
      return (ArrayList)new ArrayList<>(Arrays.asList((byte[])new byte[][] { { 
                -99, -127, -91, -78, 108, 13, -118, -105, 86, 16, 
                -16, 84, -74, 40, 11, 104, 91, -86, 48, 43, 
                25, 21, 55, 43, 6, 33, -17, 1, 106, 102, 
                60, -74 }, arrayOfByte }));
    }
    
    public final void setHALInstrumentation() {}
    
    public final boolean linkToDeath(IHwBinder.DeathRecipient param1DeathRecipient, long param1Long) {
      return true;
    }
    
    public final void ping() {}
    
    public final DebugInfo getDebugInfo() {
      DebugInfo debugInfo = new DebugInfo();
      debugInfo.pid = HidlSupport.getPidIfSharable();
      debugInfo.ptr = 0L;
      debugInfo.arch = 0;
      return debugInfo;
    }
    
    public final void notifySyspropsChanged() {
      HwBinder.enableInstrumentation();
    }
    
    public final boolean unlinkToDeath(IHwBinder.DeathRecipient param1DeathRecipient) {
      return true;
    }
    
    public IHwInterface queryLocalInterface(String param1String) {
      if ("vendor.oplus.hardware.stability.oplus_project@1.0::IOplusProject".equals(param1String))
        return (IHwInterface)this; 
      return null;
    }
    
    public void registerAsService(String param1String) throws RemoteException {
      registerService(param1String);
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(interfaceDescriptor());
      stringBuilder.append("@Stub");
      return stringBuilder.toString();
    }
    
    public void onTransact(int param1Int1, HwParcel param1HwParcel1, HwParcel param1HwParcel2, int param1Int2) throws RemoteException {
      DebugInfo debugInfo;
      byte[] arrayOfByte;
      String str2;
      ArrayList<String> arrayList;
      String str1;
      ArrayList<byte[]> arrayList1;
      NativeHandle nativeHandle;
      HwBlob hwBlob1, hwBlob2;
      switch (param1Int1) {
        default:
          switch (param1Int1) {
            default:
              return;
            case 257120595:
              param1HwParcel1.enforceInterface("android.hidl.base@1.0::IBase");
              notifySyspropsChanged();
            case 257049926:
              param1HwParcel1.enforceInterface("android.hidl.base@1.0::IBase");
              debugInfo = getDebugInfo();
              param1HwParcel2.writeStatus(0);
              debugInfo.writeToParcel(param1HwParcel2);
              param1HwParcel2.send();
            case 256921159:
              debugInfo.enforceInterface("android.hidl.base@1.0::IBase");
              ping();
              param1HwParcel2.writeStatus(0);
              param1HwParcel2.send();
            case 256462420:
              debugInfo.enforceInterface("android.hidl.base@1.0::IBase");
              setHALInstrumentation();
            case 256398152:
              debugInfo.enforceInterface("android.hidl.base@1.0::IBase");
              arrayList1 = getHashChain();
              param1HwParcel2.writeStatus(0);
              hwBlob1 = new HwBlob(16);
              param1Int2 = arrayList1.size();
              hwBlob1.putInt32(8L, param1Int2);
              hwBlob1.putBool(12L, false);
              hwBlob2 = new HwBlob(param1Int2 * 32);
              for (param1Int1 = 0; param1Int1 < param1Int2; ) {
                long l = (param1Int1 * 32);
                arrayOfByte = arrayList1.get(param1Int1);
                if (arrayOfByte != null && arrayOfByte.length == 32) {
                  hwBlob2.putInt8Array(l, arrayOfByte);
                  param1Int1++;
                } 
                throw new IllegalArgumentException("Array element is not of the expected length");
              } 
              hwBlob1.putBlob(0L, hwBlob2);
              param1HwParcel2.writeBuffer(hwBlob1);
              param1HwParcel2.send();
            case 256136003:
              arrayOfByte.enforceInterface("android.hidl.base@1.0::IBase");
              str2 = interfaceDescriptor();
              param1HwParcel2.writeStatus(0);
              param1HwParcel2.writeString(str2);
              param1HwParcel2.send();
            case 256131655:
              str2.enforceInterface("android.hidl.base@1.0::IBase");
              nativeHandle = str2.readNativeHandle();
              arrayList = str2.readStringVector();
              debug(nativeHandle, arrayList);
              param1HwParcel2.writeStatus(0);
              param1HwParcel2.send();
            case 256067662:
              break;
          } 
          arrayList.enforceInterface("android.hidl.base@1.0::IBase");
          arrayList = interfaceChain();
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.writeStringVector(arrayList);
          param1HwParcel2.send();
        case 6:
          arrayList.enforceInterface("vendor.oplus.hardware.stability.oplus_project@1.0::IOplusProject");
          param1Int1 = get_eng_version();
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.writeInt32(param1Int1);
          param1HwParcel2.send();
        case 5:
          arrayList.enforceInterface("vendor.oplus.hardware.stability.oplus_project@1.0::IOplusProject");
          param1Int1 = get_rf_type();
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.writeInt32(param1Int1);
          param1HwParcel2.send();
        case 4:
          arrayList.enforceInterface("vendor.oplus.hardware.stability.oplus_project@1.0::IOplusProject");
          param1Int1 = get_operator_name();
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.writeInt32(param1Int1);
          param1HwParcel2.send();
        case 3:
          arrayList.enforceInterface("vendor.oplus.hardware.stability.oplus_project@1.0::IOplusProject");
          str1 = get_serial_ID();
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.writeString(str1);
          param1HwParcel2.send();
        case 2:
          str1.enforceInterface("vendor.oplus.hardware.stability.oplus_project@1.0::IOplusProject");
          param1Int1 = get_pcb_version();
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.writeInt32(param1Int1);
          param1HwParcel2.send();
        case 1:
          break;
      } 
      str1.enforceInterface("vendor.oplus.hardware.stability.oplus_project@1.0::IOplusProject");
      param1Int1 = get_project();
      param1HwParcel2.writeStatus(0);
      param1HwParcel2.writeInt32(param1Int1);
      param1HwParcel2.send();
    }
  }
}
