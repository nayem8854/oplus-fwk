package core.java.com.android.internal.os;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;

public class SmartEndcStatus implements Parcelable {
  public boolean isSwitchOn() {
    return this.mSwitchOn;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public SmartEndcStatus(boolean paramBoolean, long paramLong1, long paramLong2, long paramLong3, long paramLong4, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, int paramInt10) {
    this.mSwitchOn = paramBoolean;
    this.mEndcBearDuration = paramLong1;
    this.mNoEndcBearDuration = paramLong2;
    this.mEnableEndcSettingTime = paramLong3;
    this.mDisableEndcSettingTime = paramLong4;
    this.mLteSpeedCntL0 = paramInt1;
    this.mLteSpeedCntL1 = paramInt2;
    this.mLteSpeedCntL2 = paramInt3;
    this.mLteSpeedCntL3 = paramInt4;
    this.mLteSpeedCntL4 = paramInt5;
    this.mEnEndcSpeedHighCnt = paramInt6;
    this.mEnEndcSwitchOffCnt = paramInt7;
    this.mEnEndcLtePoorCnt = paramInt8;
    this.mEnEndcLteJamCnt = paramInt9;
    this.mEnEndcProhibitCnt = paramInt10;
  }
  
  public SmartEndcStatus() {}
  
  public static final Parcelable.Creator<SmartEndcStatus> CREATOR = new Parcelable.Creator<SmartEndcStatus>() {
      public SmartEndcStatus createFromParcel(Parcel param1Parcel) {
        boolean bool = param1Parcel.readBoolean();
        long l1 = param1Parcel.readLong();
        long l2 = param1Parcel.readLong();
        long l3 = param1Parcel.readLong();
        long l4 = param1Parcel.readLong();
        int i = param1Parcel.readInt();
        int j = param1Parcel.readInt();
        int k = param1Parcel.readInt();
        int m = param1Parcel.readInt();
        int n = param1Parcel.readInt();
        int i1 = param1Parcel.readInt();
        int i2 = param1Parcel.readInt();
        int i3 = param1Parcel.readInt();
        int i4 = param1Parcel.readInt();
        int i5 = param1Parcel.readInt();
        return new SmartEndcStatus(bool, l1, l2, l3, l4, i, j, k, m, n, i1, i2, i3, i4, i5);
      }
      
      public SmartEndcStatus[] newArray(int param1Int) {
        return new SmartEndcStatus[param1Int];
      }
    };
  
  private long mDisableEndcSettingTime;
  
  private int mEnEndcLteJamCnt;
  
  private int mEnEndcLtePoorCnt;
  
  private int mEnEndcProhibitCnt;
  
  private int mEnEndcSpeedHighCnt;
  
  private int mEnEndcSwitchOffCnt;
  
  private long mEnableEndcSettingTime;
  
  private long mEndcBearDuration;
  
  private int mLteSpeedCntL0;
  
  private int mLteSpeedCntL1;
  
  private int mLteSpeedCntL2;
  
  private int mLteSpeedCntL3;
  
  private int mLteSpeedCntL4;
  
  private long mNoEndcBearDuration;
  
  private boolean mSwitchOn;
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeBoolean(this.mSwitchOn);
    paramParcel.writeLong(this.mEndcBearDuration);
    paramParcel.writeLong(this.mNoEndcBearDuration);
    paramParcel.writeLong(this.mEnableEndcSettingTime);
    paramParcel.writeLong(this.mDisableEndcSettingTime);
    paramParcel.writeInt(this.mLteSpeedCntL0);
    paramParcel.writeInt(this.mLteSpeedCntL1);
    paramParcel.writeInt(this.mLteSpeedCntL2);
    paramParcel.writeInt(this.mLteSpeedCntL3);
    paramParcel.writeInt(this.mLteSpeedCntL4);
    paramParcel.writeInt(this.mEnEndcSpeedHighCnt);
    paramParcel.writeInt(this.mEnEndcSwitchOffCnt);
    paramParcel.writeInt(this.mEnEndcLtePoorCnt);
    paramParcel.writeInt(this.mEnEndcLteJamCnt);
    paramParcel.writeInt(this.mEnEndcProhibitCnt);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{ sw:");
    stringBuilder.append(this.mSwitchOn);
    stringBuilder.append(", eddut:");
    stringBuilder.append(this.mEndcBearDuration);
    stringBuilder.append(", noeddut:");
    stringBuilder.append(this.mNoEndcBearDuration);
    stringBuilder.append(", et:");
    stringBuilder.append(this.mEnableEndcSettingTime);
    stringBuilder.append(", det:");
    stringBuilder.append(this.mDisableEndcSettingTime);
    stringBuilder.append(", ls0:");
    stringBuilder.append(this.mLteSpeedCntL0);
    stringBuilder.append(", ls1:");
    stringBuilder.append(this.mLteSpeedCntL1);
    stringBuilder.append(", ls2:");
    stringBuilder.append(this.mLteSpeedCntL2);
    stringBuilder.append(", ls3:");
    stringBuilder.append(this.mLteSpeedCntL3);
    stringBuilder.append(", ls4:");
    stringBuilder.append(this.mLteSpeedCntL4);
    stringBuilder.append(", edhct:");
    stringBuilder.append(this.mEnEndcSpeedHighCnt);
    stringBuilder.append(", edoffct:");
    stringBuilder.append(this.mEnEndcSwitchOffCnt);
    stringBuilder.append(", edlpct:");
    stringBuilder.append(this.mEnEndcLtePoorCnt);
    stringBuilder.append(", edljct:");
    stringBuilder.append(this.mEnEndcLteJamCnt);
    stringBuilder.append(", edphct:");
    stringBuilder.append(this.mEnEndcProhibitCnt);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public String toStringLite() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{ sw:");
    stringBuilder.append(this.mSwitchOn);
    stringBuilder.append(", eddut:");
    stringBuilder.append(this.mEndcBearDuration);
    stringBuilder.append(", noeddut:");
    stringBuilder.append(this.mNoEndcBearDuration);
    stringBuilder.append(", et:");
    stringBuilder.append(this.mEnableEndcSettingTime);
    stringBuilder.append(", det:");
    stringBuilder.append(this.mDisableEndcSettingTime);
    stringBuilder.append(", ls:{");
    stringBuilder.append(this.mLteSpeedCntL0);
    stringBuilder.append(",");
    stringBuilder.append(this.mLteSpeedCntL1);
    stringBuilder.append(",");
    stringBuilder.append(this.mLteSpeedCntL2);
    stringBuilder.append(",");
    stringBuilder.append(this.mLteSpeedCntL3);
    stringBuilder.append(",");
    stringBuilder.append(this.mLteSpeedCntL4);
    stringBuilder.append("}, edhct:");
    stringBuilder.append(this.mEnEndcSpeedHighCnt);
    stringBuilder.append(", edoffct:");
    stringBuilder.append(this.mEnEndcSwitchOffCnt);
    stringBuilder.append(", edlpct:");
    stringBuilder.append(this.mEnEndcLtePoorCnt);
    stringBuilder.append(", edljct:");
    stringBuilder.append(this.mEnEndcLteJamCnt);
    stringBuilder.append(", edphct:");
    stringBuilder.append(this.mEnEndcProhibitCnt);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public void setSwitchOn(boolean paramBoolean) {
    this.mSwitchOn = paramBoolean;
  }
  
  public long getEndcBearDuration() {
    return this.mEndcBearDuration;
  }
  
  public void setEndcBearDuration(long paramLong) {
    this.mEndcBearDuration = paramLong;
  }
  
  public long getNoEndcBearDuration() {
    return this.mNoEndcBearDuration;
  }
  
  public void setNoEndcBearDuration(long paramLong) {
    this.mNoEndcBearDuration = paramLong;
  }
  
  public long getEnableEndcSettingTime() {
    return this.mEnableEndcSettingTime;
  }
  
  public void setEnableEndcSettingTime(long paramLong) {
    this.mEnableEndcSettingTime = paramLong;
  }
  
  public long getDisableEndcSettingTime() {
    return this.mDisableEndcSettingTime;
  }
  
  public void setDisableEndcSettingTime(long paramLong) {
    this.mDisableEndcSettingTime = paramLong;
  }
  
  public int getLteSpeedCntL0() {
    return this.mLteSpeedCntL0;
  }
  
  public void setLteSpeedCntL0(int paramInt) {
    this.mLteSpeedCntL0 = paramInt;
  }
  
  public int getLteSpeedCntL1() {
    return this.mLteSpeedCntL1;
  }
  
  public void setLteSpeedCntL1(int paramInt) {
    this.mLteSpeedCntL1 = paramInt;
  }
  
  public int getLteSpeedCntL2() {
    return this.mLteSpeedCntL2;
  }
  
  public void setLteSpeedCntL2(int paramInt) {
    this.mLteSpeedCntL2 = paramInt;
  }
  
  public int getLteSpeedCntL3() {
    return this.mLteSpeedCntL3;
  }
  
  public void setLteSpeedCntL3(int paramInt) {
    this.mLteSpeedCntL3 = paramInt;
  }
  
  public int getLteSpeedCntL4() {
    return this.mLteSpeedCntL4;
  }
  
  public void setLteSpeedCntL4(int paramInt) {
    this.mLteSpeedCntL4 = paramInt;
  }
  
  public int getEnEndcSpeedHighCnt() {
    return this.mEnEndcSpeedHighCnt;
  }
  
  public void setEnEndcSpeedHighCnt(int paramInt) {
    this.mEnEndcSpeedHighCnt = paramInt;
  }
  
  public int getEnEndcSwitchOffCnt() {
    return this.mEnEndcSwitchOffCnt;
  }
  
  public void setEnEndcSwitchOffCnt(int paramInt) {
    this.mEnEndcSwitchOffCnt = paramInt;
  }
  
  public int getEnEndcLtePoorCnt() {
    return this.mEnEndcLtePoorCnt;
  }
  
  public void setEnEndcLtePoorCnt(int paramInt) {
    this.mEnEndcLtePoorCnt = paramInt;
  }
  
  public int getEnEndcLteJamCnt() {
    return this.mEnEndcLteJamCnt;
  }
  
  public void setEnEndcLteJamCnt(int paramInt) {
    this.mEnEndcLteJamCnt = paramInt;
  }
  
  public int getEnEndcProhibitCnt() {
    return this.mEnEndcProhibitCnt;
  }
  
  public void setEnEndcProhibitCnt(int paramInt) {
    this.mEnEndcProhibitCnt = paramInt;
  }
  
  public static SmartEndcStatus creatEndcStatusFormIntent(Intent paramIntent) {
    if (paramIntent == null)
      return null; 
    boolean bool = paramIntent.getBooleanExtra("Switch", false);
    long l1 = paramIntent.getLongExtra("EndcDura", 0L);
    long l2 = paramIntent.getLongExtra("NoEndcDura", 0L);
    long l3 = paramIntent.getLongExtra("EnEndcTime", 0L);
    long l4 = paramIntent.getLongExtra("DisEndcTime", 0L);
    int i = paramIntent.getIntExtra("LteSpeedCntL0", 0);
    int j = paramIntent.getIntExtra("LteSpeedCntL1", 0);
    int k = paramIntent.getIntExtra("LteSpeedCntL2", 0);
    int m = paramIntent.getIntExtra("LteSpeedCntL3", 0);
    int n = paramIntent.getIntExtra("LteSpeedCntL4", 0);
    int i1 = paramIntent.getIntExtra("EnEndcSpeedHighCnt", 0);
    int i2 = paramIntent.getIntExtra("EnEndcSwitchOffCnt", 0);
    int i3 = paramIntent.getIntExtra("EnEndcLtePoorCnt", 0);
    int i4 = paramIntent.getIntExtra("EnEndcLteJamCnt", 0);
    return new SmartEndcStatus(bool, l1, l2, l3, l4, i, j, k, m, n, i1, i2, i3, i4, paramIntent.getIntExtra("EnEndcProhibitCnt", 0));
  }
}
