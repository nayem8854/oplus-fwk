package org.apache.http.conn.ssl;

import javax.net.ssl.SSLException;

@Deprecated
public class StrictHostnameVerifier extends AbstractVerifier {
  public final void verify(String paramString, String[] paramArrayOfString1, String[] paramArrayOfString2) throws SSLException {
    verify(paramString, paramArrayOfString1, paramArrayOfString2, true);
  }
  
  public final String toString() {
    return "STRICT";
  }
}
