package org.apache.http.conn.ssl;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import org.apache.http.conn.scheme.HostNameResolver;
import org.apache.http.conn.scheme.LayeredSocketFactory;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

@Deprecated
public class SSLSocketFactory implements LayeredSocketFactory {
  public static final X509HostnameVerifier ALLOW_ALL_HOSTNAME_VERIFIER = new AllowAllHostnameVerifier();
  
  public static final X509HostnameVerifier BROWSER_COMPATIBLE_HOSTNAME_VERIFIER = new BrowserCompatHostnameVerifier();
  
  public static final String SSL = "SSL";
  
  public static final String SSLV2 = "SSLv2";
  
  public static final X509HostnameVerifier STRICT_HOSTNAME_VERIFIER = new StrictHostnameVerifier();
  
  public static final String TLS = "TLS";
  
  private X509HostnameVerifier hostnameVerifier;
  
  private final HostNameResolver nameResolver;
  
  private final javax.net.ssl.SSLSocketFactory socketfactory;
  
  private final SSLContext sslcontext;
  
  class NoPreloadHolder {
    private static final SSLSocketFactory DEFAULT_FACTORY = new SSLSocketFactory();
  }
  
  public static SSLSocketFactory getSocketFactory() {
    return NoPreloadHolder.DEFAULT_FACTORY;
  }
  
  public SSLSocketFactory(String paramString1, KeyStore paramKeyStore1, String paramString2, KeyStore paramKeyStore2, SecureRandom paramSecureRandom, HostNameResolver paramHostNameResolver) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
    KeyManager[] arrayOfKeyManager;
    TrustManager[] arrayOfTrustManager;
    this.hostnameVerifier = BROWSER_COMPATIBLE_HOSTNAME_VERIFIER;
    String str = paramString1;
    if (paramString1 == null)
      str = "TLS"; 
    paramString1 = null;
    if (paramKeyStore1 != null)
      arrayOfKeyManager = createKeyManagers(paramKeyStore1, paramString2); 
    paramKeyStore1 = null;
    if (paramKeyStore2 != null)
      arrayOfTrustManager = createTrustManagers(paramKeyStore2); 
    SSLContext sSLContext = SSLContext.getInstance(str);
    sSLContext.init(arrayOfKeyManager, arrayOfTrustManager, paramSecureRandom);
    this.socketfactory = this.sslcontext.getSocketFactory();
    this.nameResolver = paramHostNameResolver;
  }
  
  public SSLSocketFactory(KeyStore paramKeyStore1, String paramString, KeyStore paramKeyStore2) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
    this("TLS", paramKeyStore1, paramString, paramKeyStore2, null, null);
  }
  
  public SSLSocketFactory(KeyStore paramKeyStore, String paramString) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
    this("TLS", paramKeyStore, paramString, null, null, null);
  }
  
  public SSLSocketFactory(KeyStore paramKeyStore) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
    this("TLS", null, null, paramKeyStore, null, null);
  }
  
  public SSLSocketFactory(javax.net.ssl.SSLSocketFactory paramSSLSocketFactory) {
    this.hostnameVerifier = BROWSER_COMPATIBLE_HOSTNAME_VERIFIER;
    this.sslcontext = null;
    this.socketfactory = paramSSLSocketFactory;
    this.nameResolver = null;
  }
  
  private SSLSocketFactory() {
    this.hostnameVerifier = BROWSER_COMPATIBLE_HOSTNAME_VERIFIER;
    this.sslcontext = null;
    this.socketfactory = HttpsURLConnection.getDefaultSSLSocketFactory();
    this.nameResolver = null;
  }
  
  private static KeyManager[] createKeyManagers(KeyStore paramKeyStore, String paramString) throws KeyStoreException, NoSuchAlgorithmException, UnrecoverableKeyException {
    if (paramKeyStore != null) {
      String str = KeyManagerFactory.getDefaultAlgorithm();
      KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(str);
      if (paramString != null) {
        char[] arrayOfChar = paramString.toCharArray();
      } else {
        paramString = null;
      } 
      keyManagerFactory.init(paramKeyStore, (char[])paramString);
      return keyManagerFactory.getKeyManagers();
    } 
    throw new IllegalArgumentException("Keystore may not be null");
  }
  
  private static TrustManager[] createTrustManagers(KeyStore paramKeyStore) throws KeyStoreException, NoSuchAlgorithmException {
    if (paramKeyStore != null) {
      String str = TrustManagerFactory.getDefaultAlgorithm();
      TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(str);
      trustManagerFactory.init(paramKeyStore);
      return trustManagerFactory.getTrustManagers();
    } 
    throw new IllegalArgumentException("Keystore may not be null");
  }
  
  public Socket createSocket() throws IOException {
    return this.socketfactory.createSocket();
  }
  
  public Socket connectSocket(Socket paramSocket, String paramString, int paramInt1, InetAddress paramInetAddress, int paramInt2, HttpParams paramHttpParams) throws IOException {
    if (paramString != null) {
      if (paramHttpParams != null) {
        InetSocketAddress inetSocketAddress;
        if (paramSocket == null)
          paramSocket = createSocket(); 
        SSLSocket sSLSocket = (SSLSocket)paramSocket;
        if (paramInetAddress != null || paramInt2 > 0) {
          int j = paramInt2;
          if (paramInt2 < 0)
            j = 0; 
          inetSocketAddress = new InetSocketAddress(paramInetAddress, j);
          sSLSocket.bind(inetSocketAddress);
        } 
        paramInt2 = HttpConnectionParams.getConnectionTimeout(paramHttpParams);
        int i = HttpConnectionParams.getSoTimeout(paramHttpParams);
        if (this.nameResolver != null) {
          inetSocketAddress = new InetSocketAddress(this.nameResolver.resolve(paramString), paramInt1);
        } else {
          inetSocketAddress = new InetSocketAddress(paramString, paramInt1);
        } 
        sSLSocket.connect(inetSocketAddress, paramInt2);
        sSLSocket.setSoTimeout(i);
        try {
          sSLSocket.startHandshake();
          this.hostnameVerifier.verify(paramString, sSLSocket);
          return sSLSocket;
        } catch (IOException iOException) {
          try {
            sSLSocket.close();
          } catch (Exception exception) {}
          throw iOException;
        } 
      } 
      throw new IllegalArgumentException("Parameters may not be null.");
    } 
    throw new IllegalArgumentException("Target host may not be null.");
  }
  
  public boolean isSecure(Socket paramSocket) throws IllegalArgumentException {
    if (paramSocket != null) {
      if (paramSocket instanceof SSLSocket) {
        if (!paramSocket.isClosed())
          return true; 
        throw new IllegalArgumentException("Socket is closed.");
      } 
      throw new IllegalArgumentException("Socket not created by this factory.");
    } 
    throw new IllegalArgumentException("Socket may not be null.");
  }
  
  public Socket createSocket(Socket paramSocket, String paramString, int paramInt, boolean paramBoolean) throws IOException, UnknownHostException {
    paramSocket = this.socketfactory.createSocket(paramSocket, paramString, paramInt, paramBoolean);
    paramSocket.startHandshake();
    this.hostnameVerifier.verify(paramString, (SSLSocket)paramSocket);
    return paramSocket;
  }
  
  public void setHostnameVerifier(X509HostnameVerifier paramX509HostnameVerifier) {
    if (paramX509HostnameVerifier != null) {
      this.hostnameVerifier = paramX509HostnameVerifier;
      return;
    } 
    throw new IllegalArgumentException("Hostname verifier may not be null");
  }
  
  public X509HostnameVerifier getHostnameVerifier() {
    return this.hostnameVerifier;
  }
}
