package org.apache.http.conn.ssl;

import java.io.IOException;
import java.security.cert.Certificate;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;

@Deprecated
public abstract class AbstractVerifier implements X509HostnameVerifier {
  private static final String[] BAD_COUNTRY_2LDS;
  
  private static final Pattern IPV4_PATTERN = Pattern.compile("^(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)(\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)){3}$");
  
  static {
    String[] arrayOfString = new String[14];
    arrayOfString[0] = "ac";
    arrayOfString[1] = "co";
    arrayOfString[2] = "com";
    arrayOfString[3] = "ed";
    arrayOfString[4] = "edu";
    arrayOfString[5] = "go";
    arrayOfString[6] = "gouv";
    arrayOfString[7] = "gov";
    arrayOfString[8] = "info";
    arrayOfString[9] = "lg";
    arrayOfString[10] = "ne";
    arrayOfString[11] = "net";
    arrayOfString[12] = "or";
    arrayOfString[13] = "org";
    BAD_COUNTRY_2LDS = arrayOfString;
    Arrays.sort((Object[])arrayOfString);
  }
  
  public final void verify(String paramString, SSLSocket paramSSLSocket) throws IOException {
    if (paramString != null) {
      SSLSession sSLSession = paramSSLSocket.getSession();
      Certificate[] arrayOfCertificate = sSLSession.getPeerCertificates();
      X509Certificate x509Certificate = (X509Certificate)arrayOfCertificate[0];
      verify(paramString, x509Certificate);
      return;
    } 
    throw new NullPointerException("host to verify is null");
  }
  
  public final boolean verify(String paramString, SSLSession paramSSLSession) {
    try {
      Certificate[] arrayOfCertificate = paramSSLSession.getPeerCertificates();
      X509Certificate x509Certificate = (X509Certificate)arrayOfCertificate[0];
      verify(paramString, x509Certificate);
      return true;
    } catch (SSLException sSLException) {
      return false;
    } 
  }
  
  public final void verify(String paramString, X509Certificate paramX509Certificate) throws SSLException {
    String[] arrayOfString2 = getCNs(paramX509Certificate);
    String[] arrayOfString1 = getDNSSubjectAlts(paramX509Certificate);
    verify(paramString, arrayOfString2, arrayOfString1);
  }
  
  public final void verify(String paramString, String[] paramArrayOfString1, String[] paramArrayOfString2, boolean paramBoolean) throws SSLException {
    LinkedList<String> linkedList = new LinkedList();
    if (paramArrayOfString1 != null && paramArrayOfString1.length > 0 && paramArrayOfString1[0] != null)
      linkedList.add(paramArrayOfString1[0]); 
    if (paramArrayOfString2 != null) {
      int i;
      byte b;
      for (i = paramArrayOfString2.length, b = 0; b < i; ) {
        String str = paramArrayOfString2[b];
        if (str != null)
          linkedList.add(str); 
        b++;
      } 
    } 
    if (!linkedList.isEmpty()) {
      StringBuffer stringBuffer = new StringBuffer();
      String str = paramString.trim().toLowerCase(Locale.ENGLISH);
      boolean bool = false;
      for (Iterator<String> iterator = linkedList.iterator(); iterator.hasNext(); ) {
        boolean bool1;
        String str1 = iterator.next();
        str1 = str1.toLowerCase(Locale.ENGLISH);
        stringBuffer.append(" <");
        stringBuffer.append(str1);
        stringBuffer.append('>');
        if (iterator.hasNext())
          stringBuffer.append(" OR"); 
        bool = str1.startsWith("*.");
        boolean bool2 = true;
        if (bool && 
          str1.indexOf('.', 2) != -1 && 
          acceptableCountryWildcard(str1) && 
          !isIPv4Address(paramString)) {
          bool1 = true;
        } else {
          bool1 = false;
        } 
        if (bool1) {
          boolean bool3 = str.endsWith(str1.substring(1));
          bool = bool3;
          if (bool3) {
            bool = bool3;
            if (paramBoolean)
              if (countDots(str) == countDots(str1)) {
                bool = bool2;
              } else {
                bool = false;
              }  
          } 
        } else {
          bool = str.equals(str1);
        } 
        if (bool)
          break; 
      } 
      if (bool)
        return; 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("hostname in certificate didn't match: <");
      stringBuilder1.append(paramString);
      stringBuilder1.append("> !=");
      stringBuilder1.append(stringBuffer);
      throw new SSLException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Certificate for <");
    stringBuilder.append(paramString);
    stringBuilder.append("> doesn't contain CN or DNS subjectAlt");
    paramString = stringBuilder.toString();
    throw new SSLException(paramString);
  }
  
  public static boolean acceptableCountryWildcard(String paramString) {
    int i = paramString.length();
    boolean bool = true;
    if (i >= 7 && i <= 9)
      if (paramString.charAt(i - 3) == '.') {
        paramString = paramString.substring(2, i - 3);
        i = Arrays.binarySearch((Object[])BAD_COUNTRY_2LDS, paramString);
        if (i >= 0)
          bool = false; 
        return bool;
      }  
    return true;
  }
  
  public static String[] getCNs(X509Certificate paramX509Certificate) {
    AndroidDistinguishedNameParser androidDistinguishedNameParser = new AndroidDistinguishedNameParser(paramX509Certificate.getSubjectX500Principal());
    List<String> list = androidDistinguishedNameParser.getAllMostSpecificFirst("cn");
    if (!list.isEmpty()) {
      String[] arrayOfString = new String[list.size()];
      list.toArray(arrayOfString);
      return arrayOfString;
    } 
    return null;
  }
  
  public static String[] getDNSSubjectAlts(X509Certificate paramX509Certificate) {
    LinkedList<String> linkedList = new LinkedList();
    CertificateParsingException certificateParsingException2 = null;
    try {
      Collection<List<?>> collection = paramX509Certificate.getSubjectAlternativeNames();
    } catch (CertificateParsingException certificateParsingException1) {
      Logger logger = Logger.getLogger(AbstractVerifier.class.getName());
      Level level = Level.FINE;
      logger.log(level, "Error parsing certificate.", certificateParsingException1);
      certificateParsingException1 = certificateParsingException2;
    } 
    if (certificateParsingException1 != null)
      for (List<Integer> list : (Iterable<List<Integer>>)certificateParsingException1) {
        int i = ((Integer)list.get(0)).intValue();
        if (i == 2) {
          String str = (String)list.get(1);
          linkedList.add(str);
        } 
      }  
    if (!linkedList.isEmpty()) {
      String[] arrayOfString = new String[linkedList.size()];
      linkedList.toArray(arrayOfString);
      return arrayOfString;
    } 
    return null;
  }
  
  public static int countDots(String paramString) {
    int i = 0;
    for (byte b = 0; b < paramString.length(); b++, i = j) {
      int j = i;
      if (paramString.charAt(b) == '.')
        j = i + 1; 
    } 
    return i;
  }
  
  private static boolean isIPv4Address(String paramString) {
    return IPV4_PATTERN.matcher(paramString).matches();
  }
}
