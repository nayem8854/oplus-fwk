package org.apache.http.conn.ssl;

@Deprecated
public class AllowAllHostnameVerifier extends AbstractVerifier {
  public final void verify(String paramString, String[] paramArrayOfString1, String[] paramArrayOfString2) {}
  
  public final String toString() {
    return "ALLOW_ALL";
  }
}
